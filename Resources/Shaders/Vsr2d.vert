#version 450

layout(location = 0) in vec3  Position;
layout(location = 1) in vec4  Color;
layout(location = 2) in vec2  Uv;
layout(location = 3) in int Ind;
layout(location = 4) in int IsFont;
layout(location = 5) in int RectIndex;

layout(location = 0) out vec4 o_Color;
layout(location = 1) out vec2 o_Uv;
layout(location = 2) out flat int o_TextureInd;
layout(location = 3) out flat int o_IsFont;
layout(location = 4) out vec3 o_Position;
layout(location = 5) out flat int o_RectIndex;

layout(std140, set = 0, binding = 0) uniform CameraUbo
{
    mat4 ViewProjection;
} u_CameraUbo;

vec2 hp[4] = vec2[](
    vec2(-1.0, 1.0),
    vec2(-1.0, -1.0),
    vec2(1.0, -1.0),
    vec2(1.0, 1.0)
);

void main()
{
    gl_Position = u_CameraUbo.ViewProjection * vec4(Position, 1.0);
    o_Color = Color;
    o_Uv = Uv;
    o_TextureInd = Ind;
    o_IsFont = IsFont;
    o_RectIndex = RectIndex;
    // Need for circle
    int mInd = int(gl_VertexIndex - 4.0 * floor(gl_VertexIndex / 4));
    o_Position = vec3(hp[mInd], Position.z);//vec3(Position.x / 2000.0, Position.z / 1200.0, Position.z);
}
