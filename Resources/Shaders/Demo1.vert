#version 450

layout(location = 0) in vec3  Position;
layout(location = 1) in vec4  Color;
layout(location = 0) out vec4  o_Color;

vec2 positions[3] = vec2[](
    vec2(0.0, -0.5),
    vec2(0.5, 0.5),
    vec2(-0.5, 0.5)
);

void main()
{
    vec2 tpos = positions[gl_VertexIndex];
    gl_Position = vec4(tpos.x,tpos.y, 0.0, 1.0);
}
