#version 460

layout(location = 0) in vec3 Position;
layout(location = 1) in vec3 Normals;
layout(location = 2) in vec2 Uv;

layout(location = 0) out VertexData
{
    vec4 Normals;
    vec2 Uv;
    flat int Index;
} o_VertexData;

layout(set = 0, binding = 0, std140) uniform CameraUbo
{
    mat4 ViewProjection;
} u_CameraUbo;

layout(set = 0, binding = 1, std140) uniform TransformUbo
{
    mat4 Transforms[200];
} u_TransformUbo;

layout(set = 0, binding = 2, std140) uniform InstanceUbo
{
    // NOTE: 16 byte each = 3200 in sum
    int MaterialIndex[200];
} u_InstanceUbo;

void main()
{
    int index = gl_InstanceIndex;
    mat4 transformMat = u_TransformUbo.Transforms[index];
    vec4 transformed = transformMat * vec4(Position, 1.0);

    //o_VertexData.Position = transformed.xyz;
    o_VertexData.Normals = vec4(normalize(mat3(transpose(inverse(transformMat))) * Normals), 1.0);
    o_VertexData.Uv = Uv;
    o_VertexData.Index = u_InstanceUbo.MaterialIndex[index];

    gl_Position = u_CameraUbo.ViewProjection * transformed;
}
