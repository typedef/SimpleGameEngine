#version 450

layout(location = 0) in vec3 inPosition;

layout(location = 0) out VertexData
{
    flat int Id;
} o_VertexData;

layout(set = 0, binding = 0, std140) uniform UniformBufferObject {
    mat4 Value;
} u_Camera;

layout(set = 0, binding = 1, std140) uniform TransformUbo
{
    mat4 Transforms[200];
} u_TransformUbo;

layout(set = 0, binding = 2, std140) uniform InstanceUbo
{
    // NOTE: 16 byte each = 3200 in sum
    int Ids[200];
} u_InstanceUbo;

void main()
{
    int index = gl_InstanceIndex;

    o_VertexData.Id = u_InstanceUbo.Ids[index];
    mat4 transform = u_TransformUbo.Transforms[index];
    gl_Position = u_Camera.Value * transform * vec4(inPosition, 1.0);
}
