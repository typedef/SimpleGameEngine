#version 460

layout(location = 0) in vec3 Position;

layout(location = 0) out VertexData
{
    vec3 Position;
} o_VertexData;

layout(set = 0, binding = 0, std140) uniform CameraUbo
{
    mat4 View;
    mat4 Projection;
} u_CameraUbo;

void main()
{
    gl_Position = vec4(Position, 1.0);
#if 0
    mat3 view3 = mat3(u_CameraUbo.View);
    vec3 pos = (inverse(u_CameraUbo.Projection) * gl_Position).xyz;
    o_VertexData.Position = transpose(view3) * pos;
#else
    o_VertexData.Position = (transpose(u_CameraUbo.View) * (inverse(u_CameraUbo.Projection) * gl_Position)).xyz;
#endif
}
