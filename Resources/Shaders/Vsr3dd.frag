#version 460
#extension GL_GOOGLE_include_directive : enable

#include "Model/MaterialInfo.glsl"

layout(location = 0) out vec4 Color;

layout(location = 0) in VertexData
{
    vec4 Normals;
    vec2 Uv;
    flat int Index;
} i_VertexData;

layout(set = 0, binding = 3, std140) uniform MeshMaterialInfoUbo
{
    MeshMaterialInfo MaterialInfo[20];
} u_MeshMaterial;

layout(set = 0, binding = 4) uniform texture2D u_BaseTexture[20];
layout(set = 0, binding = 5) uniform sampler u_BaseSampler[20];

void main()
{
    vec3 lightColor = vec3(1);

    int ind = i_VertexData.Index.x;
    MeshMaterialInfo materialInfo = u_MeshMaterial.MaterialInfo[ind];
    int baseTextureInd = materialInfo.BaseColorTexture;

    vec4 textureColor = texture(sampler2D(u_BaseTexture[baseTextureInd], u_BaseSampler[baseTextureInd]), i_VertexData.Uv) * vec4(materialInfo.BaseColor.xyz, 1.0);
    vec4 modelColor = textureColor * materialInfo.BaseColor;
    Color = modelColor; //vec4(lightColor, 1.0) * modelColor;
    if (ind > 102000 && ind < 102200)
    {
	Color = Color * i_VertexData.Normals;
    }
}
