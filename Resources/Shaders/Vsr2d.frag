#version 450

layout(location = 0) in vec4 o_Color;
layout(location = 1) in vec2 o_Uv;
layout(location = 2) in flat int o_TextureInd;
layout(location = 3) in flat int o_IsFont;
layout(location = 4) in vec3 o_Position;
layout(location = 5) in flat int o_RectIndex;

layout(location = 0) out vec4 FragColor;

layout(std140, set = 0, binding = 1) uniform FontUbo
{
    float Width;
    float EdgeTransition;
    int IsScissorsDebug;
} u_FontUbo;

struct VisibleRect
{
    vec2 Min;
    vec2 Max;
};

layout(std140, set = 0, binding = 2) uniform VisibleRectUbo
{
    VisibleRect Rects[5000];
} u_VisibleRects;

layout(set = 0, binding = 3) uniform texture2D u_Textures[100];
layout(set = 0, binding = 4) uniform sampler u_Samplers[100];

float myPow(float x, int n) {
    float mul = x;

    for (int i = 1; i < n; ++i) {
	mul *= x;
    }

    return mul;
}

vec4 circle(vec2 uv, int multiplier, float thikness, float fade) {
    float xn = myPow(uv.x, multiplier);
    float yn = myPow(uv.y, multiplier);
    float d = 1.0 - (xn+yn);

    vec4 col = vec4(smoothstep(0.0, fade, d));
    float thicknessRatio = smoothstep(thikness, thikness - fade, d);
    col *= vec4(thicknessRatio);

    return col;
}

void main()
{
    VisibleRect rect = u_VisibleRects.Rects[o_RectIndex];
    vec2 min = rect.Min;
    vec2 max = rect.Max;

    float fx = gl_FragCoord.x;
    float fy = gl_FragCoord.y;
    bool isInside = (fx >= min.x) && (fx <= max.x) && (fy >= min.y) && (fy <= max.y);

    if (!isInside) {
	if (u_FontUbo.IsScissorsDebug == 1) {
	    FragColor = vec4(1, 0, 0, 1);
	}
	else {
	    discard;
	}
    }
    else if (o_TextureInd == -1) {
	FragColor = o_Color;
    }
    else if (o_TextureInd == -2) { // IsCircle
	FragColor = circle(o_Position.xy, o_IsFont, o_Uv.x, o_Uv.y) * o_Color;
    }
    else if (o_IsFont == 1) {
	vec4 text = texture(sampler2D(u_Textures[o_TextureInd], u_Samplers[o_TextureInd]), o_Uv);
	float dist = 1.0 - text.r;
	float alpha = 1.0 - smoothstep(u_FontUbo.Width, u_FontUbo.Width + u_FontUbo.EdgeTransition, dist);
	//o_Color.xyz
	FragColor = vec4(o_Color.xyz, alpha + o_Color.w);
    }
    else {
	vec4 text = texture(sampler2D(u_Textures[o_TextureInd], u_Samplers[o_TextureInd]), o_Uv);
	FragColor = vec4(o_Color.rgb * text.rgb, text.a * o_Color.a);
    }
}
