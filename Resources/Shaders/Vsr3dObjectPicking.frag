#version 450

// Attribute to output
layout(location = 0) out float outColor;

layout(location = 0) in VertexData
{
    flat int Id;
} i_VertexData;

layout(set = 0, binding = 3, std140) buffer ShaderStorageBufferObject
{
    uint SelectedId;
} ssbo;

void main()
{
    ssbo.SelectedId = i_VertexData.Id;

    //only needed for debugging to draw to color attachment
    outColor = i_VertexData.Id;
}
