#ifndef AUTO_TEST_SUI_H
#define AUTO_TEST_SUI_H

struct AutoTestRecord;
struct AutoTestFunction;
struct AutoTestFile;

void auto_test_sui_draw_record(struct AutoTestRecord* pRecord);
void auto_test_sui_draw_function(struct AutoTestFunction* pFunction);
int auto_test_sui_draw_file_list(struct AutoTestFile* pFile);

#endif // AUTO_TEST_SUI_H
