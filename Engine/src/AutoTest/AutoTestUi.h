#ifndef AUTO_TEST_UI_H
#define AUTO_TEST_UI_H

struct AutoTestRecord;
struct AutoTestFunction;
struct AutoTestFile;

void auto_test_ui_draw_record(struct AutoTestRecord* pRecord);
void auto_test_ui_draw_function(struct AutoTestFunction* pFunction);
int auto_test_ui_draw_file_list(struct AutoTestFile* pFile);

#endif // AUTO_TEST_UI_H
