#ifndef AUTO_TEST_H
#define AUTO_TEST_H

#include <Core/Types.h>

/*
  DOCS: Simple and easy to use api for testing

  Private function / not for direct use (begins with "_"):
  _*();

  AutoTest_File - setting current file for testing, call it one time at the start of ur test's
  AutoTest_Func - setting current function for testing, call it per each testing function
  Condition - check if some statement is `true or `false

  Usage Example:
  void testing_array() {
    some test code ...
    Condition(array_count(arr) > 0);
  }

  void array_test() {
   AutTest_File();
   AutoTest_Func(testing_array());
  }

*/

typedef struct AutoTestRecord
{
    i8 Code;
    char* Message;
} AutoTestRecord;

typedef struct AutoTestFunction
{
    char* Name;
    i32 Success;
    AutoTestRecord* aTestRecords;
} AutoTestFunction;

typedef struct AutoTestFile
{
    char* Name;
    i32 SelectedFunction;
    AutoTestFunction* aFunctions;
} AutoTestFile;

typedef struct TestInfo
{
    u32 Count;
    u32 ErrorsCount;
    u32 SuccessCount;
} TestInfo;

typedef struct AutoTestDependecy
{
    const char* Name;
    i64 IsInitialize;
    int (*OnInitialize)();
} AutoTestDependency;

typedef enum AutoTestDependStatusType
{
    AutoTestDependStatusType_Error_CauseNotRegistered = 0,
    AutoTestDependStatusType_Warn_AlreadyInit,
    AutoTestDependStatusType_Init,
} AutoTestDependStatusType;

typedef struct AutoTestGlobal
{
    SimpleArena* pArena;
    const char* FunctionName;
    AutoTestFile* pAutoTestFile;
    TestInfo Info;

    AutoTestFile* aFiles;
    struct { const char* Key; AutoTestDependency Value; }* pDependencies;
} AutoTestGlobal;

#define AutoTest_File()				\
    _auto_test_set_file(__FILE__)

#define AutoTest_Func(func)			\
    _auto_test_set_function(#func);		\
    func;

#define AutoTest_Depend(pName)			\
    _auto_test_depend(pName)

#define AutoTest_Depend_Valid(pName)			\
    _auto_test_depend_valid(pName, __FILE__, __LINE__)

#define AutoTest_Condition(condition)					\
    ({									\
        auto_test_condition(condition, #condition, __FILE__);		\
    })

#define AutoTest_I32_Value(i32v)					\
    ({									\
        auto_test_i32_value(i32v, #i32v": ", __FILE__);			\
    })

#define AutoTest_I32_Equal(iv0, iv1)				\
    ({								\
        auto_test_i32_equal(iv0, iv1, #iv0, #iv1, __FILE__);	\
    })

#define AutoTest_F32_Value(f32v)					\
    ({									\
        auto_test_f32_value(f32v, #f32v": ", __FILE__);			\
    })

#define AutoTest_F32_Equal(f32v0, f32v1)				\
    ({									\
        const char* cstr = #f32v0" == "#f32v1;				\
        auto_test_f32_equal(f32v0, f32v1, cstr, __FILE__);		\
    })

#define AutoTest_String_EqualBase(a, b, c)				\
    ({									\
        auto_test_string_equal(a, b, c, __FILE__);			\
    })
#define AutoTest_String_Equal(a, b)					\
    ({									\
        AutoTest_String_EqualBase(a, b, #a" == "#b);			\
    })
#define AutoTest_String_NotEqual(a, b)					\
    ({									\
        AutoTest_String_EqualBase(a, b, #a" != "#b);			\
    })
#define AutoTest_String_Value(s)					\
    ({									\
        auto_test_string_value(s, #s": ", __FILE__);			\
    })

#define AutoTest_V2_Value(v)			\
    ({						\
        auto_test_v2_value(v, #v, __FILE__);	\
    })
#define AutoTest_V2_Equal(v0, v1)			\
    ({							\
        auto_test_v2_equal(v0, v1, #v0, #v1, __FILE__);	\
    })
#define AutoTest_V3_Value(v)			\
    ({						\
        auto_test_v3_value(v, #v, __FILE__);	\
    })
#define AutoTest_V3_Equal(v0, v1)			\
    ({							\
        auto_test_v3_equal(v0, v1, #v0, #v1, __FILE__);	\
    })
#define AutoTest_V4_Value(v)			\
    ({						\
        auto_test_v4_value(v, #v, __FILE__);	\
                                                \
    })
#define AutoTest_V4_Equal(v0, v1)			\
    ({							\
        auto_test_v4_equal(v0, v1, #v0, #v1, __FILE__);	\
    })

#define AutoTest_M4_Value(m)				\
    ({							\
        auto_test_m4_value(m, #m, #m": ", __FILE__);	\
    })

#define AutoTest_M4_Equal(m0, m1)				\
    ({								\
        auto_test_m4_equal(m0, m1, #m0" == "#m1, __FILE__);	\
    })


void auto_test_init(size_t size);
AutoTestGlobal* auto_test_get();

void auto_test_condition(i32 condition, const char* cstr, const char* file);
void auto_test_i32_value(i32 v, const char* cstr, const char* file);
void auto_test_i32_equal(i32 v0, i32 v1, const char* cstr0, const char* cstr1, const char* file);
void auto_test_f32_value(f32 v, const char* cstr, const char* file);
void auto_test_f32_equal(f32 v0, f32 v1, const char* cstr, const char* file);

void auto_test_string_equal(char* s0, char* s1, const char* cstr, const char* file);
void auto_test_string_value(const char* s, const char* cstr, const char* file);

void auto_test_v2_equal(v2 v0, v2 v1, const char*v0s, const char*v1s, const char* pFile);
void auto_test_v2_value(v2 v, const char*vs, const char* pFile);
void auto_test_v3_equal(v3 v0, v3 v1, const char*v0s, const char*v1s, const char* pFile);
void auto_test_v3_value(v3 v, const char*vs, const char* pFile);
void auto_test_v4_equal(v4 v0, v4 v1, const char*v0s, const char*v1s, const char* pFile);
void auto_test_v4_value(v4 v, const char*vs, const char* pFile);

void auto_test_m4_value(m4 m, const char* cstr0, const char* cstr1, const char* file);
void auto_test_m4_equal(m4 m0, m4 m1, const char* cstr, const char* file);

void _auto_test_set_file(const char* file);
void _auto_test_set_function(const char* functionName);

i32 _auto_test_register_dependency(AutoTestDependency dependency);
AutoTestDependStatusType _auto_test_depend(const char* pName);
void _auto_test_depend_valid(AutoTestDependStatusType type, const char* pFile, i32 line);
//void _auto_test_add_record(i32 conditionResult, const char* file, const char* condition);



#endif // AUTO_TEST_H
