#include "AutoTestUi.h"

#include <AutoTest/AutoTest.h>
#include <Core/SimpleStandardLibrary.h>

#define SUI_BACKEND 0

#if SUI_BACKEND == 1
#include "AutoTestSui.h"
#else
#include "AutoTestTui.h"
#endif

void
auto_test_ui_draw_record(AutoTestRecord* pRecord)
{
    // TODO: Add identation and stuff later
#if SUI_BACKEND == 1
    auto_test_sui_draw_record(pRecord);
#else
#endif
}

void
auto_test_ui_draw_function(AutoTestFunction* pFunction)
{
#if SUI_BACKEND == 1
    auto_test_sui_draw_function(pFunction);
#else

#endif
}

i32
auto_test_ui_draw_file_list(AutoTestFile* aFiles)
{
#if SUI_BACKEND == 1
    return auto_test_sui_draw_file_list(aFile);
#else

    auto_test_tui_init(aFiles);
    simple_thread_create(auto_test_input, KB(5), NULL);
    simple_thread_create(auto_test_draw, KB(5), aFiles);

    return 0;
#endif
}
