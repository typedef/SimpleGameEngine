#include "AutoTestSui.h"

#include <AutoTest/AutoTest.h>
#include <Core/SimpleStandardLibrary.h>
#include <UI/Sui.h>

void
auto_test_sui_draw_record(AutoTestRecord* pRecord)
{
    sui_text("%s: %d", pRecord->Message, pRecord->Code);
}

void
auto_test_sui_draw_function(AutoTestFunction* pFunction)
{
    sui_text("");
    i64 cnt = array_count(pFunction->aTestRecords);
    for (i64 f = 0; f < cnt; ++f)
    {
        AutoTestRecord* pRecord = &pFunction->aTestRecords[f];
        auto_test_sui_draw_record(pRecord);
    }
}

i32
auto_test_sui_draw_file_list(AutoTestFile* pFile)
{
    i32 isSelected = -1;

    if (sui_panel_begin(pFile->Name, SuiPanelFlags_Movable | SuiPanelFlags_Scrollable))
    {
        i64 cnt = array_count(pFile->aFunctions);

        for (i64 f = 0; f < cnt; ++f)
        {
            AutoTestFunction autoFunc = pFile->aFunctions[f];
            if (sui_text("%d:%s", autoFunc.Success, autoFunc.Name) == SuiState_Clicked)
            {
                if (pFile->SelectedFunction == f)
                    pFile->SelectedFunction = -1;
                else
                    pFile->SelectedFunction = f;
            }
        }

        if (pFile->SelectedFunction != -1)
        {
            AutoTestFunction funcItem = pFile->aFunctions[pFile->SelectedFunction];

            auto_test_sui_draw_function(&funcItem);
        }

    }

    sui_panel_end();

    return isSelected;
}
