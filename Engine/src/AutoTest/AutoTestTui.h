#ifndef AUTO_TEST_TUI_H
#define AUTO_TEST_TUI_H

struct AutoTestRecord;
struct AutoTestFunction;
struct AutoTestFile;


void auto_test_tui_draw_record(struct AutoTestRecord* pRecord);
void auto_test_tui_draw_function(struct AutoTestFunction* pFunction);
int auto_test_tui_draw_file_list(struct AutoTestFile* pFile);
void auto_test_tui_draw_files_list(struct AutoTestFile* aTestFile);

void auto_test_tui_init(struct AutoTestFile* aTestFile);
void* auto_test_draw(void* pData);
void* auto_test_input(void* pData);

#endif // AUTO_TEST_TUI_H
