#include "SimpleWindow.h"

#include <Core/SimpleStandardLibrary.h>
#include <Deps/stb_image.h>
#include <Event/Event.h>


static void (*_window_on_event_function)(Event* event);
static SimpleWindow* CurrentWindow;

static void
_scroll_callback(GLFWwindow* window, f64 xoffset, f64 yoffset)
{
    MouseScrolledEvent event = {};
    event.Base.IsHandled = 0;
    event.Base.Type = EventType_MouseScrolled;
    event.Base.Category = EventCategory_Mouse;
    event.XOffset = xoffset;
    event.YOffset = yoffset;

    _window_on_event_function((Event*)&event);
}

static void
_window_size_callback(GLFWwindow* window, i32 width, i32 height)
{
    WindowResizedEvent event = {};
    event.Base.IsHandled = 0;
    event.Base.Type = EventType_WindowResized;
    event.Base.Category = EventCategory_Window;
    event.Width = (u32)width;
    event.Height = (u32)height;

    CurrentWindow->Width = width;
    CurrentWindow->Height = height;

    _window_on_event_function((Event*)&event);
}

static void
_key_callback(GLFWwindow* window, i32 key, i32 scancode, i32 action, i32 mods)
{
    KeyPressedEvent event = {};
    event.Base.IsHandled = 0;
    event.Base.Category = EventCategory_Key;

    event.KeyCode = key;
    event.ScanCode = scancode;
    event.Action = action;
    event.Modificator = mods;

    //GINFO("Code: %d Scancode: %d, IsRepeat: %d Mods: %d\n", key, scancode, action == GLFW_REPEAT, mods);

    if (action == GLFW_PRESS)
    {
        event.Base.Type = EventType_KeyPressed;
        event.RepeatCount = 0;
    }
    else if (action == GLFW_REPEAT)
    {
        event.Base.Type = EventType_KeyRepeatPressed;
        event.RepeatCount = 1;
    }
    else if (action == GLFW_RELEASE)
    {
        event.Base.Type = EventType_KeyRealeased;
        event.RepeatCount = 0;
    }

    _window_on_event_function((Event*)&event);
}

static void
_window_char_callback(GLFWwindow* window, u32 ch)
{
    CharEvent event = {};
    event.Base.IsHandled = 0;
    event.Base.Category = EventCategory_Key;
    event.Base.Type = EventType_CharTyped;
    event.Char = ch;

    _window_on_event_function((Event*)&event);
}

static void
_mouse_button_callback(GLFWwindow* window, i32 button, i32 action, i32 mods)
{
    MouseButtonEvent event = {};
    event.MouseCode = button;
    event.Action = action;
    event.Modificator = mods;
    event.Base.IsHandled = 0;
    event.Base.Category = EventCategory_Mouse;

    if (action == GLFW_PRESS)
    {
        event.Base.Type = EventType_MouseButtonPressed;
    }
    else if (action == GLFW_RELEASE)
    {
        event.Base.Type = EventType_MouseButtonReleased;
    }

    _window_on_event_function((Event*)&event);
}

static void
_mouse_cursor_position_callback(GLFWwindow* window, f64 x, f64 y)
{
    MouseMovedEvent event = {};
    event.X = x;
    event.Y = y;
    event.Base.IsHandled = 0;
    event.Base.Category = EventCategory_Mouse;
    event.Base.Type = EventType_MouseMoved;

    _window_on_event_function((Event*)&event);
}

static void
_window_minimized_callback(GLFWwindow* window, i32 isMinimized)
{
    if (isMinimized)
    {
        Event event = {
            .IsHandled = 0,
            .Type = EventType_WindowMinimized,
            .Category = EventCategory_Window
        };
        _window_on_event_function((Event*)&event);
    }
    else
    {
        Event event = {
            .IsHandled = 0,
            .Type = EventType_WindowRestored,
            .Category = EventCategory_Window
        };
        _window_on_event_function((Event*)&event);
    }
}

static void
_window_maximized_callback(GLFWwindow* window, i32 isMaximized)
{
    if (isMaximized)
    {
        Event event = {
            .IsHandled = 0,
            .Type = EventType_WindowMaximized,
            .Category = EventCategory_Window
        };
        _window_on_event_function((Event*)&event);
    }
}

static void
_window_close_callback(GLFWwindow* window)
{
    Event event = {
        .IsHandled = 0,
        .Type = EventType_WindowShouldBeClosed,
        .Category = EventCategory_Window
    };
    _window_on_event_function((Event*)&event);
}

static void
_window_refreshed(GLFWwindow* window)
{
    Event event = {
        .IsHandled = 0,
        .Type = EventType_WindowRestored,
        .Category = EventCategory_Window
    };
    _window_on_event_function((Event*)&event);
}


i32
window_create(SimpleWindow* window, SimpleWindowSettings* pWinSet, void (*onEvent)(Event* event))
{
    i32 major, minor, revision;

    static i32 windowsCreated = 0;
    if (windowsCreated)
    {
        vassert(0 && "Window already created!");
    }

    windowsCreated = 1;
    CurrentWindow = window;

    if (!glfwInit())
    {
        GERROR("GLFW is not initialized!\n");
        return -1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwGetVersion(&major, &minor, &revision);
    GLOG(MAGNETA("GLFW version: %d.%d.%d\n"), major, minor, revision);

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    window->Width  = pWinSet->Width;
    window->Height = pWinSet->Height;
    window->VsyncLevel = 1;
    window->Title = pWinSet->Name;
    window->GlfwWindow = glfwCreateWindow(pWinSet->Width, pWinSet->Height, pWinSet->Name, NULL, NULL);
    window->OnEvent = onEvent;
    window->AspectRatio = ((f32) pWinSet->Width) / pWinSet->Height;
    _window_on_event_function = onEvent;

    glfwSetKeyCallback(window->GlfwWindow, _key_callback);
    glfwSetCharCallback(window->GlfwWindow, _window_char_callback);
    glfwSetMouseButtonCallback(window->GlfwWindow, _mouse_button_callback);
    glfwSetCursorPosCallback(window->GlfwWindow, _mouse_cursor_position_callback);
    glfwSetScrollCallback(window->GlfwWindow, _scroll_callback);
    glfwSetWindowSizeCallback(window->GlfwWindow, _window_size_callback);
    glfwSetWindowIconifyCallback(window->GlfwWindow, _window_minimized_callback);
    glfwSetWindowMaximizeCallback(window->GlfwWindow, _window_maximized_callback);
    glfwSetWindowCloseCallback(window->GlfwWindow, _window_close_callback);

    return 1;
}

VkResult
window_create_surface(VkInstance instance, VkAllocationCallbacks* allocator, VkSurfaceKHR* surface)
{
    return glfwCreateWindowSurface(instance, CurrentWindow->GlfwWindow, allocator, surface);
}

SimpleWindow*
window_get()
{
    return CurrentWindow;
}

static i32 g_WindowPos[2];
static i32 g_WindowSize[2];

void
window_set_fullscreen(SimpleWindow* window)
{
    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* videomode = glfwGetVideoMode(monitor);

    if (glfwGetWindowMonitor(window->GlfwWindow) == NULL)
    {
        glfwGetWindowPos(window->GlfwWindow, &g_WindowPos[0], &g_WindowPos[1]);
        glfwGetWindowSize(window->GlfwWindow, &g_WindowSize[0], &g_WindowSize[1]);

        glfwSetWindowMonitor(window->GlfwWindow, monitor, 0, 0, videomode->width, videomode->height, 0);
    }
    else
    {
        glfwSetWindowMonitor(window->GlfwWindow, NULL, g_WindowPos[0], g_WindowPos[1], g_WindowSize[0], g_WindowSize[1], 0);
    }
}

i32
window_should_close(SimpleWindow* window)
{
    return glfwWindowShouldClose(window->GlfwWindow);
}

void
window_set_should_close(SimpleWindow* window, i8 shouldClose)
{
    glfwSetWindowShouldClose(window->GlfwWindow, shouldClose);
}

void
window_terminate()
{
    GSUCCESS("\n\n\n TERMINATING THE WINDOW !!! \n\n\n");
    glfwDestroyWindow(CurrentWindow->GlfwWindow);
    glfwTerminate();

    CurrentWindow = NULL;
}

void
window_set_title(SimpleWindow* window, const char* title)
{
    window->Title = title;
    glfwSetWindowTitle(window->GlfwWindow, title);
}

void
window_set_vsync(i32 isVsync)
{
    CurrentWindow->VsyncLevel = isVsync;
    glfwSwapInterval(isVsync);
}

void
window_on_update(SimpleWindow* window)
{
    //glfwSwapBuffers(window->GlfwWindow);
    glfwPollEvents();
}

void
window_get_size(SimpleWindow* window, i32* width, i32* height)
{
    glfwGetWindowSize(window->GlfwWindow, width, height);
}

v2
window_get_size_v2(SimpleWindow* window)
{
    i32 w, h;
    glfwGetWindowSize(window->GlfwWindow, &w, &h);

    v2 size = { .Width = w, .Height = h };
    return size;
}

void
window_get_frame_size(SimpleWindow* window, i32* left, i32* top, i32* right, i32* bottom)
{
    glfwGetWindowFrameSize(window->GlfwWindow, left, top, right, bottom);
}

void
window_minimize(SimpleWindow* window)
{
    glfwIconifyWindow(window->GlfwWindow);
}

void
window_restore(SimpleWindow* window)
{
    glfwRestoreWindow(window->GlfwWindow);
}

void
window_maximize(SimpleWindow* window)
{
    glfwMaximizeWindow(window->GlfwWindow);
}

force_inline GLFWimage
window_load_icon(const char* path)
{
    GLFWimage image;

    i32 width, height, channels;
    stbi_uc* data = stbi_load(path, &width, &height, &channels, 0);

    image.width = width;
    image.height = height;
    image.pixels = data;

    return image;
}

void
window_set_icon(SimpleWindow* window, const char* path)
{
    i32 isIconPathExist = path_is_file_exist(path);
    if (!isIconPathExist)
    {
        vassert(isIconPathExist && "Window: Icon path does not exist!");
    }
    GLFWimage image = window_load_icon(path);
    glfwSetWindowIcon(window->GlfwWindow, 1, &image);
}

f64
window_get_short_time()
{
    return glfwGetTime();
}

void
window_get_framebuffer_size(SimpleWindow* window, i32* w, i32* h)
{
    glfwGetFramebufferSize(window->GlfwWindow, w, h);
}

void
window_get_framebuffer_size_shared(i32* w, i32* h)
{
    window_get_framebuffer_size(CurrentWindow, w, h);
}

char**
window_get_required_exts(u32* extCount)
{
    char** exts = NULL;

    const char** requiredExts =
        glfwGetRequiredInstanceExtensions(extCount);
    i32 count = *extCount;
    array_reserve(exts, count);
    for (i32 i = 0; i < count; ++i)
    {
        array_push(exts, (char*)requiredExts[i]);
    }

    return exts;
}
