#include "GraphicsLayer.h"
#include "Graphics/Vulkan/VsrCompositor.h"

#include <Application/Application.h>
#include <AssetManager/AssetManager.h>
#include <InputSystem/SimpleInput.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Graphics/Vulkan/Vsr.h>
#include <UI/UserInterface.h>
#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>

static struct GraphicsLayerSettings gGraphicsLayerSettings = {};
static char* gGraphicsLayerName = "Graphics Layer";

void
graphics_layer_on_attach()
{

    SimpleWindow* pSimpleWindow = memory_allocate_type(SimpleWindow);
    application_set_window(pSimpleWindow);

    SimpleWindowSettings sws = {
        .Width = gGraphicsLayerSettings.Width,
        .Height = gGraphicsLayerSettings.Height,
        .Name = gGraphicsLayerSettings.Name
    };
    i32 isWindowCreated = window_create(pSimpleWindow, &sws, application_on_event);
    window_set_vsync(gGraphicsLayerSettings.IsVsync);
    if (isWindowCreated == -1)
    {
        GERROR("Can't create window!\n");
        return;
    }

    input_init(application_get_window()->GlfwWindow);

    VsaSettings vsaSettings = {
        .IsDebugEnabled = gGraphicsLayerSettings.IsDebug,
        .IsVSyncEnabled = gGraphicsLayerSettings.IsVsync,
        .SamplesCount = 8
    };
    vsa_core_init(vsaSettings);

    // DOCS: Create render pass'es for all graphics pipelines
    vsr_init();

    // DOCS(typedef): Use after vsa_core_init()
    asset_manager_create();

    ui_create();
}

void
graphics_layer_on_attach_finished()
{
}

void
graphics_layer_on_before_update()
{
}

void
graphics_layer_on_after_update()
{
    Application* pApplication = application_get();

    i32 layersCount = array_count(pApplication->Layers);
    i32 framesCnt = pApplication->Stats.FramesCount;
    u16 uiRatio = pApplication->Stats.UiFpsSyncRatio;
    if (uiRatio <= 1 || ((framesCnt % uiRatio) == 0))
    {
        ui_begin();
        for (i32 l = 0; l < layersCount; l++)
        {
            Layer layer = pApplication->Layers[l];

            if (layer.OnUIRender != NULL)
                layer.OnUIRender();
        }

        ui_end();
    }

    vsr_render_passes_execute();
}

void
graphics_layer_on_event(struct Event* pEvent)
{
    ui_event(pEvent);
}

void
graphics_layer_on_destroy()
{
    /*
      Mb is already done.
      NOTE/TODO/BUG():
      All Fonts and other vsa resources needs to be loaded by asset manager, so asset manager is responsible for it's destruction
    */

    vsr_compositor_destroy();

    ui_destroy();
    asset_manager_destroy();
    vsa_core_deinit();
    window_terminate();
}

void
graphics_layer_create(const struct GraphicsLayerSettings* pSettings)
{
    gGraphicsLayerSettings = *pSettings;

    Layer layer = {
        .Name = gGraphicsLayerName,
        .OnAttach = graphics_layer_on_attach,
        .OnAttachFinished = graphics_layer_on_attach_finished,
        .OnBeforeUpdate = graphics_layer_on_before_update,
        .OnAfterUpdate = graphics_layer_on_after_update,
        .OnEvent = graphics_layer_on_event,
        .OnDestoy = graphics_layer_on_destroy
    };

    application_push_layer(layer);

}
