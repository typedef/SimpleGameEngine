#ifndef GRAPHICS_LAYER_H
#define GRAPHICS_LAYER_H

struct Event;

struct GraphicsLayerSettings
{
    int IsDebug;
    int IsVsync;

    int Width;
    int Height;
    const char* Name;
};

void graphics_layer_create(const struct GraphicsLayerSettings* pSettings);
void graphics_layer_on_event(struct Event* pEvent);

#endif // GRAPHICS_LAYER_H
