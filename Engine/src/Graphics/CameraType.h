#ifndef CAMERA_TYPE_H
#define CAMERA_TYPE_H

#include <Core/Types.h>

typedef enum CameraType
{
    CameraType_Orthographic = 1 << 1,
    CameraType_Perspective  = 1 << 2,
    CameraType_Spectator    = 1 << 3,
    CameraType_Arcball      = 1 << 4,

    CameraType_BaseOnly     = 1 << 5,

    CameraType_OrthographicSpectator = CameraType_BaseOnly | CameraType_Orthographic | CameraType_Spectator,
    CameraType_OrthographicArcball   = CameraType_BaseOnly | CameraType_Orthographic | CameraType_Arcball,
    CameraType_PerspectiveSpectator  = CameraType_BaseOnly | CameraType_Perspective | CameraType_Spectator,
    CameraType_PerspectiveArcball    = CameraType_BaseOnly | CameraType_Perspective | CameraType_Arcball,

    CameraType_End          = 1 << 6
} CameraType;

const char* camera_type_to_string(CameraType type);
i32 camera_type_is_valid(CameraType cameraType);

#endif // CAMERA_TYPE_H
