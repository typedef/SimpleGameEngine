#ifndef LIGHT_H
#define LIGHT_H

#include <Core/Types.h>

typedef struct BaseLight
{
    v3 Ambient;
    v3 Diffuse;
    v3 Specular;
} BaseLight;

typedef struct DirectionalLight
{
    BaseLight Base;
    v3 Color;
    v3 Direction;
} DirectionalLight;

typedef struct PointLight
{
    BaseLight Base;
    v3 Color;
    v3 Position;
    f32 Constant;
    f32 Linear;
    f32 Quadratic;
} PointLight;

typedef struct FlashLight
{
    BaseLight Base;
    v3 Color;
    v3 Position;
    v3 Direction;
    f32 Constant;
    f32 Linear;
    f32 Quadratic;
    f32 CutOff;      // NOTE(typedef): cos(rad(angle))
    f32 OuterCutOff; // NOTE(typedef): cos(rad(angle))
} FlashLight;

#endif // LIGHT_H
