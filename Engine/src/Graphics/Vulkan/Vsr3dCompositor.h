#ifndef VSR_3D_COMPOSITOR_H
#define VSR_3D_COMPOSITOR_H

#include "Vsr3d.h"
#include "Vsr3dSky.h"

struct Camera;
struct StaticModel;
struct MeshMaterialInfo;
struct DynamicModel;

typedef struct Vsr3dCompositor
{
    Vsr3dS Render;
    Vsr3dD DynamicRender;
    Vsr3dSky SkyRender;
    struct Camera* pCamera;

    VsaTexture* aTextures;
    i64* aMaterialIndices;
    struct MeshMaterialInfo* aMeshMaterialInfos;
    VsrMeshRecord* aMeshRecords;
    VsrMeshRecord* aDynamicMeshRecords;
} Vsr3dCompositor;

void vsr_3d_compositor_create(Vsr3dCompositor* pCompositor, Vsr3dSSetting* pVsr3dsSet, Vsr3dDSetting* pVsr3ddSet, struct Camera* pCamera);
void vsr_3d_compositor_destroy(Vsr3dCompositor* pCompositor);
void vsr_3d_compositor_update_camera_ubo(Vsr3dCompositor* pCompositor);
void vsr_3d_compositor_submit_model(Vsr3dCompositor* pCompositor, struct StaticModel* pModel, m4 transform);
void vsr_3d_compositor_submit_dynamic_model(Vsr3dCompositor* pCompositor, struct DynamicModel* pModel, m4 transform);
void vsr_3d_compositor_flush(Vsr3dCompositor* pCompositor);



#define ENABLE_COMPOSITOR_CODE 0

#if ENABLE_COMPOSITOR_CODE == 1
typedef struct VsrCompositor
{
    // TODO: Move this into compositor
    VsaTexture* pTextures;
    struct Camera* pCamera;

    SimpleChunk* GpuChunks;
    SimpleChunk* PassedChunks;
    // DOCS(typedef): Passed Model Ids submit()
    i64* PassedModelIds;
    i64* UniqueMaterialIds;
    // DOCS: Only Transforms are unique
    m4* PassedTransforms;
    VsrRenderable* PassedRenderables;
    // DOCS(typedef): models on gpu flush(), needed for not passing whole models chunks to buffer
    i64* GpuModelIds;
    m4* GpuTransforms; //NOTE: array_count(Tranforms) == Base.ItemsCount
#if VSRF_SET_BUFFER_DEBUGGING == 1
    void* VertexBuffer;
    void* IndexBuffer;
#endif

} VsrCompositor;

#endif

#endif // VSR_3D_COMPOSITOR_H
