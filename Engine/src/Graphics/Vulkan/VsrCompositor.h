#ifndef VSR_COMPOSITOR_H
#define VSR_COMPOSITOR_H

#include <Core/Types.h>

/*

  array of pointer to VsrCompositor
  void**

 */

typedef void (*OnVsrDestroy)(void* pData);

typedef struct VsrCompositor
{
    char* pName;
    void* pData;
    OnVsrDestroy OnDestroy;
} VsrCompositor;


void* vsr_compositor_register(const char* pName, void* pData, size_t size, OnVsrDestroy onDestroy);
void* vsr_compositor_get(const char* pName);
void vsr_compositor_destroy();

#endif // VSR_COMPOSITOR_H
