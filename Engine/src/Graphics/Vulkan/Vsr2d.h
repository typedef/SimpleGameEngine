#ifndef VSR_2D_H
#define VSR_2D_H

#include "Vsr.h"
#include <Core/Types.h>

struct Camera;

VsaDrawCall* vsr_add_draw_call(VsrBase* pBase, void* pVertices, size_t verticesCount, size_t vertexSize, void* indices, size_t indicesCount);
void vsr_pipeline_renderable(VsrBase* pBase);

/*
  ############################
  #DOCS(typedef): 2d renderer#
  ############################
*/

typedef struct VsrVertex2d
{
    v3 Position;
    v4 Color;
    v2 Uv;
    /*
      TODO(typedef): TextureInd can be used for
      NoTexture = -1,
      IsCircle = -2
    */

    i32 TextureInd;
    i32 IsFont;
    i32 RectangleInd;
} VsrVertex2d;

typedef struct Vsr2dData
{
    i64 IndicesToDraw;
    VsrVertex2d* Vertices;
} Vsr2dData;

typedef struct Renderer2dSettings
{
    i64 MaxInstancesCount;
    i64 MaxVerticesCount;
    i64 MaxTexturesCount;
    const char* pName;
    VsaFont Font;
    struct Camera* pCamera;
} Vsr2dSettings;

typedef struct VisibleRect
{
    v2 Min;
    v2 Max;
} VsrVisibleRect;

typedef struct Vsr2d
{
    VsrBase Base;
    i8 IsDataSubmited;
    i8 IsScissorsDebug;
    VsrVisibleRect* Rects;
    u32* Indices;
    // NOTE: mb this is link to asset manager stuff
    VsaFont CurrentFontInfo;

    struct Camera* pCamera;
    VsaTexture* pTextures;

    Vsr2dData Current;
    Vsr2dData Prev;

    VsaShaderUniform* pCameraUbo;
    VsaShaderUniform* pFontUbo;
    VsaShaderUniform* pRectUbo;
} Vsr2d;

void vsr_2d_create(Vsr2d* renderer, const Vsr2dSettings* settings);
void vsr_2d_set_visible_rect(Vsr2d* pRenderer, VsrVisibleRect rect);
void vsr_2d_submit_items(Vsr2d* pRenderer, v3* positions, v2* uvs, size_t itemsCount, size_t indicesCount, i32 isFont, v4 color, VsaTexture vsaTexture);
void vsr_2d_submit_positions(Vsr2d* pRenderer, v3 p0, v3 p1, v3 p2, v3 p3, v2 uv0, v2 uv1, v2 uv2, v2 uv3, i32 isFont, v4 color, VsaTexture vsaTexture);
void vsr_2d_submit_text_ext(Vsr2d* pRenderer, void* buffer, size_t length, i32 scale, v3 position, v3 rotation, v2 drawable, v4 glyphColor);
void vsr_2d_flush(Vsr2d* renderer);
void vsr_2d_destroy(Vsr2d* renderer);


#endif // VSR_2D_H
