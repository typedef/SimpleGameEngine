#ifndef VSR_2D_COMPOSITOR_H
#define VSR_2D_COMPOSITOR_H

#include "Vsr2d.h"


/*
  2d compositor for 2d renderer is useless, unlike 3d compositor
 */
typedef struct Vsr2DCompositor
{
    Vsr2d ScreenRender;
    Vsr2d ScreenRender2;
    Vsr2d WorldRender;
} Vsr2dCompositor;

void vsr_2d_compositor_create_screen(Vsr2dCompositor* pCompositor, Vsr2dSettings* pUiSettings);
void vsr_2d_compositor_create_screen2(Vsr2dCompositor* pCompositor, Vsr2dSettings* pUiSettings);
void vsr_2d_compositor_create_world(Vsr2dCompositor* pCompositor, Vsr2dSettings* pWorldSettings);
void vsr_2d_compositor_destroy(Vsr2dCompositor* pCompositor);

void vsr_2d_compositor_set_visible_rect(Vsr2dCompositor* pCompositor, VsrVisibleRect rect);
void vsr_2d_compositor_submit_items(Vsr2dCompositor* pCompositor, v3* positions, v2* uvs, size_t itemsCount, size_t indicesCount, i32 isFont, v4 color, VsaTexture vsaTexture);
void vsr_2d_compositor_submit(Vsr2dCompositor* pCompositor, v3 position, v2 size, v4 color, VsaTexture vsaTexture);
void vsr_2d_compositor_submit_rect(Vsr2dCompositor* pCompositor, v3 position, v2 size, v4 color);
void vsr_2d_compositor_submit_line(Vsr2d* pRenderer, v3 start, v3 end, f32 thickness, v4 color);
void vsr_2d_compositor_world_submit_line(Vsr2d* pRenderer, v3 start, v3 end, f32 thickness, v4 color);
void vsr_2d_compositor_submit_empty_rect(Vsr2dCompositor* pCompositor, v3 position, v2 size, f32 thickness, v4 color);
void vsr_2d_compositor_submit_circle_ext(Vsr2dCompositor* pCompositor, v3 position, v2 size, v4 color, f32 thikness, f32 fade, i32 multiplier);

void vsr_2d_compositor_flush_screen(Vsr2dCompositor* pCompositor);
void vsr_2d_compositor_flush_screen2(Vsr2dCompositor* pCompositor);
void vsr_2d_compositor_flush_world(Vsr2dCompositor* pCompositor);

void vsr_2d_compositor_submit_text_ext(Vsr2dCompositor* pCompositor, void* buffer, size_t length, i32 scale, v3 position, v3 rotation, v2 drawable, v4 glyphColor);

/*
  ############################
  #DOCS(typedef): Tests      #
  ############################
*/
void vsr_test_submit_circle_and_line(Vsr2d* renderer, VsaTexture someTexture);

#endif
