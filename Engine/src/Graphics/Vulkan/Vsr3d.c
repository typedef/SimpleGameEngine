#include "Vsr3d.h"

#include "Graphics/Vulkan/Vsr.h"
#include "VulkanSimpleApi.h"
#include <Core/SimpleStandardLibrary.h>
#include <Model/StaticModel.h>
#include <Graphics/Camera.h>
#include <AssetManager/AssetManager.h>

/*
  DOCS: Ubos structures
*/

typedef struct CameraUbo
{
    m4 ViewProjection;
} CameraUbo;


typedef struct InstanceUbo
{
    i32 MaterialIndex;
    i32 TrashVariable0;
    i32 TrashVariable1;
    i32 TrashVariable2;
} InstanceUbo;

void
vsr_3d_s_create(Vsr3dS* pRenderer, Vsr3dSSetting* pSettings)
{
    vguard(pSettings->MaxTexture > 0 && "pSettings->MaxTexture should be bigger than 0!!!");
    vguard(pSettings->MaxVertices > 0 && "pSettings->MaxVertices should be bigger than 0!!!");
    vguard(pSettings->MaxInstances > 0 && "pSettings->MaxInstances should be bigger than 0!!!");
    vguard(pSettings->MaxMaterialsCount > 0 && "pSettings->MaxMaterialsCount should be bigger than 0!!!");

    size_t texturesCount  = pSettings->MaxTexture;
    size_t verticesCount  = pSettings->MaxVertices;
    size_t instancesCount = pSettings->MaxInstances;
    size_t materialsCount = pSettings->MaxMaterialsCount;

    IStringPool* pStringPool = istring_pool_create();

    // ===============================
    //     DOCS() Pipeline creation
    // ===============================

    VsaShaderStage stages[] = {
        [0] = {
            .Type = VsaShaderType_Vertex,
            .ShaderSourcePath = resource_shader("Vsr3ds.vert"),
            .Descriptors = {
                [0] = (VsaShaderDescriptor) {
                    .Name = istring_ext(pStringPool, "u_CameraUbo"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = 1,
                    .Size = sizeof(CameraUbo)
                },
                [1] = (VsaShaderDescriptor) {
                    .Name = istring_ext(pStringPool, "u_TransformUbo"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = instancesCount,
                    .Size = sizeof(m4)
                },
                [2] = (VsaShaderDescriptor) {
                    .Name = istring_ext(pStringPool, "u_InstanceUbo"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = instancesCount,
                    .Size = sizeof(InstanceUbo)
                }
            },
            .DescriptorsCount = 3
        },

        [1] = {
            .Type = VsaShaderType_Fragment,
            .ShaderSourcePath = resource_shader("Vsr3ds.frag"),
            .Descriptors = {
                [0] = {
                    .Name = istring_ext(pStringPool, "u_MeshMaterial"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = instancesCount,
                    .Size = sizeof(MeshMaterialInfo)
                },
                [1] = {
                    .Name = istring_ext(pStringPool, "u_BaseTexture"),
                    .Type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
                    .Count = instancesCount,
                    .Size = 0
                },
                [2] = {
                    .Name = istring_ext(pStringPool, "u_BaseSampler"),
                    .Type = VK_DESCRIPTOR_TYPE_SAMPLER,
                    .Count = instancesCount,
                    .Size = 0
                },

            },

            .DescriptorsCount = 3
        }
    };

    VsaShaderAttribute attributes[] = {
        [0] = {
            .Format = VK_FORMAT_R32G32B32_SFLOAT,
            .Offset = OffsetOf(StaticMeshVertex, Position)
        },
        [1] = {
            .Format = VK_FORMAT_R32G32B32_SFLOAT,
            .Offset = OffsetOf(StaticMeshVertex, Normal)
        },
        [2] = {
            .Format = VK_FORMAT_R32G32_SFLOAT,
            .Offset = OffsetOf(StaticMeshVertex, Uv)
        }
    };

    VsaPipelineDescription pipeDescr = {
        .Type = VsaPipelineType_Indirect,
        .RenderPassIndex = vsr_get_render_pass_id(),
        .MaxInstanceCount = instancesCount,
        .Stride = sizeof(StaticMeshVertex),
        .EnableDepthStencil = 1,
        .Stages = stages,
        .StagesCount = ARRAY_COUNT(stages),
        .Attributes = attributes,
        .AttributesCount = ARRAY_COUNT(attributes),
        .PolygonMode = VK_POLYGON_MODE_FILL, //VK_POLYGON_MODE_LINE,
    };

    VsaPipeline vsaPipeline = vsa_pipeline_create(pipeDescr);

    // TODO: last one is vsrbase creation
    vsr_base_create(&pRenderer->Base, (VsrBaseSettings) {
            .Name = "Vsr3ds",
            .pStringPool = pStringPool,
            .Pipeline = vsaPipeline,
            .MaxTexturesCount = pSettings->MaxTexture,
            .MaxVerticesCount = pSettings->MaxVertices,
            .MaxItemsCount = pSettings->MaxInstances,
            .MaxMaterialsCount = pSettings->MaxMaterialsCount,

            .Priority = 100,
        });

    pRenderer->pTransformUniform = vsr_base_get_uniform(&pRenderer->Base, "u_TransformUbo");
    pRenderer->pInstanceUniform = vsr_base_get_uniform(&pRenderer->Base, "u_InstanceUbo");
    pRenderer->pMaterialUniform = vsr_base_get_uniform(&pRenderer->Base, "u_MeshMaterial");
    pRenderer->pCameraUbo = vsr_base_get_uniform(&pRenderer->Base, "u_CameraUbo");
    // TODO/BUG: I dont like this design decision
    pRenderer->pMaterialUniform->IsUpdatedByFlag = 1;
}

void
vsr_3d_s_destroy(Vsr3dS* pRenderer)
{
    vsr_base_destroy(&pRenderer->Base);
}

void
vsr_3d_s_set_transform_ubo(Vsr3dS* pRenderer, m4* aTransforms)
{
    size_t size = array_count(aTransforms) * sizeof(m4);
    vsa_uniform_set_data(pRenderer->pTransformUniform, aTransforms, size);
}

void
vsr_3d_s_set_instance_ubo(Vsr3dS* pRenderer, i64* aMaterialIndices)
{
    vguard((sizeof(InstanceUbo) == 16) && "Instance ubo smaller than 16");

    array_foreach(aMaterialIndices,
                  InstanceUbo instanceUbo = {
                      .MaterialIndex = item
                  };

                  vsa_uniform_set_data(pRenderer->pInstanceUniform, &instanceUbo, sizeof(struct InstanceUbo));
        );

}

void
vsr_3d_s_set_material_ubo(Vsr3dS* pRenderer, MeshMaterial* pMaterials)
{
    size_t size = array_count(pMaterials) * sizeof(MeshMaterial);
    vsa_uniform_set_data(pRenderer->pMaterialUniform, pMaterials, size);
}

void
vsr_3d_s_set_camera_ubo(Vsr3dS* pRenderer, m4 viewProjection)
{
    vsa_uniform_set_data(pRenderer->pCameraUbo, &viewProjection, sizeof(m4));
    vsa_uniform_reset(pRenderer->pCameraUbo);
}

VsrMeshRecord*
vsr_3d_s_submit_model(Vsr3dS* pRenderer, StaticModel* pModel, m4 transform, MeshMaterialInfo* aMeshMaterialInfo)
{
    VsrMeshRecord* recs = NULL;

    for (i64 i = 0; i < array_count(pModel->aMeshes); ++i)
    {
        StaticMesh* pMesh = &pModel->aMeshes[i];
        MeshMaterialInfo* pMeshMaterialInfo = &aMeshMaterialInfo[i];
        VsrMeshRecord rec = vsr_3d_s_submit_mesh(pRenderer, pMesh, transform, pMeshMaterialInfo);
        array_push(recs, rec);
    }

    return recs;
}

// note: if we need use instance rendering we could send mesh=StaticMesh count=100
VsrMeshRecord
vsr_3d_s_submit_meshes(Vsr3dS* pRenderer, StaticMesh* pMesh, m4* aTransforms, size_t count, MeshMaterialInfo* pMeshMaterialInfo)
{
    /* static size_t vbufferOffset = 0; */
    /* static size_t ibufferOffset = 0; */
    /* static size_t verticesOffsetCnt = 0; */
    /* static size_t indicesOffsetCnt = 0; */

    i64 verticesCnt = array_count(pMesh->Vertices);
    i64 indicesCnt = array_count(pMesh->Indices);
    size_t vsize = verticesCnt * sizeof(StaticMeshVertex);
    size_t isize = indicesCnt * sizeof(u32);

    { // DOCS: setting vbuffer/ibuffer
        vsa_buffer_set_data(&pRenderer->Base.pPipeline->Vertex, pMesh->Vertices, pRenderer->VBufferOffset, vsize);
        vsa_buffer_set_data(&pRenderer->Base.pPipeline->Index, pMesh->Indices, pRenderer->IBufferOffset, isize);
    }

    VsrMeshRecord meshRecord = {
        .Count = count,
        .VerticesCountInd = pRenderer->VerticesOffsetCnt,
        .IndicesCountInd = pRenderer->IndicesOffsetCnt,
        .VBufferOffset = pRenderer->VBufferOffset,
        .IBufferOffset = pRenderer->IBufferOffset,
        .IndicesCount = indicesCnt,
    };

    { // DOCS: Set mesh material
        vsa_uniform_set_data(pRenderer->pMaterialUniform, pMeshMaterialInfo, sizeof(MeshMaterialInfo));
    }

    { //DOCS: Set transforms;
        size_t size = count * sizeof(m4);
        vsa_uniform_set_data(pRenderer->pTransformUniform, aTransforms, size);
    }

    pRenderer->VBufferOffset += vsize;
    pRenderer->IBufferOffset += isize;
    pRenderer->VerticesOffsetCnt += verticesCnt;
    pRenderer->IndicesOffsetCnt += indicesCnt;

    VsrStats* pStats = &pRenderer->Base.Stats;
    pStats->MaterialsCount += 1;
    pStats->ItemsCount += count;
    pStats->VertexBuffer.UsedSize += vsize;
    pStats->IndexBuffer.UsedSize  += isize;
    pStats->VerticesCount += verticesCnt;

    // TODO: Place inside vsr_3d_s_bind_textures()
    // pStats->TexturesCount

    return meshRecord;
}

VsrMeshRecord
vsr_3d_s_submit_mesh(Vsr3dS* pRenderer, StaticMesh* pMesh, m4 transform, MeshMaterialInfo* pMeshMaterialInfo)
{
    m4 aTransforms[] = { [0] = transform };
    VsrMeshRecord record = vsr_3d_s_submit_meshes(pRenderer, pMesh, aTransforms, 1, pMeshMaterialInfo);
    return record;
}

void
vsr_3d_s_flush(Vsr3dS* pRenderer, VsrMeshRecord* pMeshRecords, size_t meshRecordsCount)
{
    VkDrawIndexedIndirectCommand* cmds =
        vsr_mesh_records_to_idirect_cmds(pMeshRecords);

    vsa_buffer_set_data(&pRenderer->Base.pPipeline->IndirectCmd.IndirectBuffer,
                        cmds, 0,
                        array_count(cmds) * sizeof(VkDrawIndexedIndirectCommand));
    pRenderer->Base.pPipeline->IndirectCmd.InstancesCount = meshRecordsCount;
    array_free(cmds);

    // NOTE: Reset buffers
    pRenderer->VBufferOffset = 0;
    pRenderer->IBufferOffset = 0;
    pRenderer->VerticesOffsetCnt = 0;
    pRenderer->IndicesOffsetCnt = 0;

    VsrStats* pStats = &pRenderer->Base.Stats;
    pStats->MaterialsCount = 0;
    pStats->ItemsCount = 0;
    pStats->VertexBuffer.UsedSize = 0;
    pStats->IndexBuffer.UsedSize = 0;
    pStats->VerticesCount = 0;

    pRenderer->Base.pPipeline->IsPipelineRenderable = 1;
}

/*
  DOCS: 3d for dynamic models,
  skined meshes with animation
*/

void
vsr_3d_d_create(Vsr3dD* pRenderer, Vsr3dDSetting* pSetting)
{
    vguard(pSetting->MaxTexture > 0 && "pSettings->MaxTexture should be bigger than 0!!!");
    vguard(pSetting->MaxVertices > 0 && "pSettings->MaxVertices should be bigger than 0!!!");
    vguard(pSetting->MaxInstances > 0 && "pSettings->MaxInstances should be bigger than 0!!!");
    vguard(pSetting->MaxMaterialsCount > 0 && "pSettings->MaxMaterialsCount should be bigger than 0!!!");

    size_t texturesCount  = pSetting->MaxTexture;
    size_t verticesCount  = pSetting->MaxVertices;
    size_t instancesCount = pSetting->MaxInstances;
    size_t materialsCount = pSetting->MaxMaterialsCount;

    IStringPool* pStringPool = istring_pool_create();

    // ===============================
    //     DOCS() Pipeline creation
    // ===============================

    VsaShaderStage stages[] = {
        [0] = {
            .Type = VsaShaderType_Vertex,
            .ShaderSourcePath = resource_shader("Vsr3dd.vert"),
            .Descriptors = {
                [0] = (VsaShaderDescriptor) {
                    .Name = istring_ext(pStringPool, "u_CameraUbo"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = 1,
                    .Size = sizeof(CameraUbo)
                },
                [1] = (VsaShaderDescriptor) {
                    .Name = istring_ext(pStringPool, "u_TransformUbo"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = instancesCount,
                    .Size = sizeof(m4)
                },
                [2] = (VsaShaderDescriptor) {
                    .Name = istring_ext(pStringPool, "u_InstanceUbo"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = instancesCount,
                    .Size = sizeof(InstanceUbo)
                }
            },
            .DescriptorsCount = 3
        },

        [1] = {
            .Type = VsaShaderType_Fragment,
            .ShaderSourcePath = resource_shader("Vsr3dd.frag"),
            .Descriptors = {
                [0] = {
                    .Name = istring_ext(pStringPool, "u_MeshMaterial"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = instancesCount,
                    .Size = sizeof(MeshMaterialInfo)
                },
                [1] = {
                    .Name = istring_ext(pStringPool, "u_BaseTexture"),
                    .Type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
                    .Count = instancesCount,
                    .Size = 0
                },
                [2] = {
                    .Name = istring_ext(pStringPool, "u_BaseSampler"),
                    .Type = VK_DESCRIPTOR_TYPE_SAMPLER,
                    .Count = instancesCount,
                    .Size = 0
                },

            },

            .DescriptorsCount = 3
        }
    };

    VsaShaderAttribute attributes[] = {
        [0] = {
            .Format = VK_FORMAT_R32G32B32_SFLOAT,
            .Offset = OffsetOf(StaticMeshVertex, Position)
        },
        [1] = {
            .Format = VK_FORMAT_R32G32B32_SFLOAT,
            .Offset = OffsetOf(StaticMeshVertex, Normal)
        },
        [2] = {
            .Format = VK_FORMAT_R32G32_SFLOAT,
            .Offset = OffsetOf(StaticMeshVertex, Uv)
        }
    };

    VsaPipelineDescription pipeDescr = {
        .Type = VsaPipelineType_Indirect,
        .RenderPassIndex = vsr_get_render_pass_id(),
        .MaxInstanceCount = instancesCount,
        .Stride = sizeof(DynamicMeshVertex),
        .EnableDepthStencil = 1,
        .Stages = stages,
        .StagesCount = ARRAY_COUNT(stages),
        .Attributes = attributes,
        .AttributesCount = ARRAY_COUNT(attributes),
        .PolygonMode = VK_POLYGON_MODE_FILL, //VK_POLYGON_MODE_LINE,
    };

    VsaPipeline vsaPipeline = vsa_pipeline_create(pipeDescr);

    // TODO: last one is vsrbase creation
    vsr_base_create(&pRenderer->Base, (VsrBaseSettings) {
            .Name = "Vsr3dd",
            .pStringPool = pStringPool,
            .Pipeline = vsaPipeline,
            .MaxTexturesCount = pSetting->MaxTexture,
            .MaxVerticesCount = pSetting->MaxVertices,
            .MaxItemsCount = pSetting->MaxInstances,
            .MaxMaterialsCount = pSetting->MaxMaterialsCount,

            .Priority = 99,
        });

    pRenderer->pTransformUniform = vsr_base_get_uniform(&pRenderer->Base, "u_TransformUbo");
    pRenderer->pInstanceUniform = vsr_base_get_uniform(&pRenderer->Base, "u_InstanceUbo");
    pRenderer->pMaterialUniform = vsr_base_get_uniform(&pRenderer->Base, "u_MeshMaterial");
    pRenderer->pCameraUbo = vsr_base_get_uniform(&pRenderer->Base, "u_CameraUbo");
    // TODO/BUG: I dont like this design decision
    pRenderer->pMaterialUniform->IsUpdatedByFlag = 1;

}

void
vsr_3d_d_destroy(Vsr3dD* pRenderer)
{
    vsr_base_destroy(&pRenderer->Base);
}

void
vsr_3d_d_set_transform_ubo(Vsr3dD* pRenderer, m4* aTransforms)
{
    size_t size = array_count(aTransforms) * sizeof(m4);
    vsa_uniform_set_data(pRenderer->pTransformUniform, aTransforms, size);
}

void
vsr_3d_d_set_instance_ubo(Vsr3dD* pRenderer, i64* aMaterialIndices)
{
    vguard((sizeof(InstanceUbo) == 16) && "Instance ubo smaller than 16");

    array_foreach(aMaterialIndices,
                  InstanceUbo instanceUbo = {
                      .MaterialIndex = item
                  };

                  vsa_uniform_set_data(pRenderer->pInstanceUniform, &instanceUbo, sizeof(struct InstanceUbo));
        );

}

void
vsr_3d_d_set_material_ubo(Vsr3dD* pRenderer, MeshMaterial* pMaterials)
{
    size_t size = array_count(pMaterials) * sizeof(MeshMaterial);
    vsa_uniform_set_data(pRenderer->pMaterialUniform, pMaterials, size);
}

void
vsr_3d_d_set_camera_ubo(Vsr3dD* pRenderer, m4 viewProjection)
{
    vsa_uniform_set_data(pRenderer->pCameraUbo, &viewProjection, sizeof(m4));
    vsa_uniform_reset(pRenderer->pCameraUbo);
}

VsrMeshRecord*
vsr_3d_d_submit_model(Vsr3dD* pRenderer, DynamicModel* pModel, m4 transform, MeshMaterialInfo* aMeshMaterialInfo)
{
    VsrMeshRecord* recs = NULL;

    for (i64 i = 0; i < array_count(pModel->aMeshes); ++i)
    {
        DynamicMesh* pMesh = &pModel->aMeshes[i];
        MeshMaterialInfo* pMeshMaterialInfo = &aMeshMaterialInfo[i];
        VsrMeshRecord rec = vsr_3d_d_submit_mesh(pRenderer, pMesh, transform, pMeshMaterialInfo);
        array_push(recs, rec);
    }

    return recs;
}

// note: if we need use instance rendering we could send mesh=StaticMesh count=100
VsrMeshRecord
vsr_3d_d_submit_meshes(Vsr3dD* pRenderer, DynamicMesh* pMesh, m4* aTransforms, size_t count, MeshMaterialInfo* pMeshMaterialInfo)
{
    /* static size_t vbufferOffset = 0; */
    /* static size_t ibufferOffset = 0; */
    /* static size_t verticesOffsetCnt = 0; */
    /* static size_t indicesOffsetCnt = 0; */

    i64 verticesCnt = array_count(pMesh->Vertices);
    i64 indicesCnt = array_count(pMesh->Indices);
    size_t vsize = verticesCnt * sizeof(StaticMeshVertex);
    size_t isize = indicesCnt * sizeof(u32);

    { // DOCS: setting vbuffer/ibuffer
        vsa_buffer_set_data(&pRenderer->Base.pPipeline->Vertex, pMesh->Vertices, pRenderer->VBufferOffset, vsize);
        vsa_buffer_set_data(&pRenderer->Base.pPipeline->Index, pMesh->Indices, pRenderer->IBufferOffset, isize);
    }

    VsrMeshRecord meshRecord = {
        //.MeshId = pMesh->Id,
        .Count = count,
        .VerticesCountInd = pRenderer->VerticesOffsetCnt,
        .IndicesCountInd = pRenderer->IndicesOffsetCnt,
        .VBufferOffset = pRenderer->VBufferOffset,
        .IBufferOffset = pRenderer->IBufferOffset,
        .IndicesCount = indicesCnt,
    };

    { // DOCS: Set mesh material
        vsa_uniform_set_data(pRenderer->pMaterialUniform, pMeshMaterialInfo, sizeof(MeshMaterialInfo));
    }

    { //DOCS: Set transforms;
        size_t size = count * sizeof(m4);
        vsa_uniform_set_data(pRenderer->pTransformUniform, aTransforms, size);
    }

    pRenderer->VBufferOffset += vsize;
    pRenderer->IBufferOffset += isize;
    pRenderer->VerticesOffsetCnt += verticesCnt;
    pRenderer->IndicesOffsetCnt += indicesCnt;

    VsrStats* pStats = &pRenderer->Base.Stats;
    pStats->MaterialsCount += 1;
    pStats->ItemsCount += count;
    pStats->VertexBuffer.UsedSize += vsize;
    pStats->IndexBuffer.UsedSize  += isize;
    pStats->VerticesCount += verticesCnt;

    // TODO: Place inside vsr_3d_s_bind_textures()
    // pStats->TexturesCount

    return meshRecord;
}

VsrMeshRecord
vsr_3d_d_submit_mesh(Vsr3dD* pRenderer, DynamicMesh* pMesh, m4 transform, MeshMaterialInfo* pMeshMaterialInfo)
{
    m4 aTransforms[] = { [0] = transform };
    VsrMeshRecord record = vsr_3d_d_submit_meshes(pRenderer, pMesh, aTransforms, 1, pMeshMaterialInfo);
    return record;
}

void
vsr_3d_d_flush(Vsr3dD* pRenderer, VsrMeshRecord* pMeshRecords, size_t meshRecordsCount)
{
    VkDrawIndexedIndirectCommand* cmds =
        vsr_mesh_records_to_idirect_cmds(pMeshRecords);

    vsa_buffer_set_data(&pRenderer->Base.pPipeline->IndirectCmd.IndirectBuffer,
                        cmds, 0,
                        array_count(cmds) * sizeof(VkDrawIndexedIndirectCommand));
    pRenderer->Base.pPipeline->IndirectCmd.InstancesCount = meshRecordsCount;
    array_free(cmds);

    // NOTE: Reset buffers
    pRenderer->VBufferOffset = 0;
    pRenderer->IBufferOffset = 0;
    pRenderer->VerticesOffsetCnt = 0;
    pRenderer->IndicesOffsetCnt = 0;

    VsrStats* pStats = &pRenderer->Base.Stats;
    pStats->MaterialsCount = 0;
    pStats->ItemsCount = 0;
    pStats->VertexBuffer.UsedSize = 0;
    pStats->IndexBuffer.UsedSize = 0;
    pStats->VerticesCount = 0;

    pRenderer->Base.pPipeline->IsPipelineRenderable = 1;
}
