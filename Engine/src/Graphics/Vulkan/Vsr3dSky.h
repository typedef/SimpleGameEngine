#ifndef VSR_3D_SKY_H
#define VSR_3D_SKY_H

#include "Vsr.h"

struct Camera;

typedef struct Vsr3dSky
{
    VsrBase Base;

    VsaShaderUniform* pCameraUniform;
} Vsr3dSky;

void vsr_3d_sky_create(Vsr3dSky* pRenderer);
void vsr_3d_sky_update_camera(Vsr3dSky* pRenderer, struct Camera* pCamera);
void vsr_3d_sky_destroy(Vsr3dSky* pRenderer);


#endif // VSR_3D_SKY_H
