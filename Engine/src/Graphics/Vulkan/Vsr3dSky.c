#include "Vsr3dSky.h"
#include "Graphics/Vulkan/Vsr.h"

#include <Core/SimpleStandardLibrary.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Graphics/Camera.h>

typedef struct SkyCameraUbo
{
    m4 View;
    m4 Projection;
} SkyCameraUbo;

static VsaDrawCall* aVsaDrawCalls = NULL;
#define INSTANCES_COUNT 6

void
vsr_3d_sky_create(Vsr3dSky* pRenderer)
{
    IStringPool* pStringPool = istring_pool_create();

    // ===============================
    //     DOCS() Pipeline creation
    // ===============================

    VsaShaderStage stages[] = {
        [0] = {
            .Type = VsaShaderType_Vertex,
            .ShaderSourcePath = resource_shader("Vsr3dSky.vert"),
            .Descriptors = {
                [0] = (VsaShaderDescriptor) {
                    .Name = istring_ext(pStringPool, "u_SkyCameraUbo"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = 1,
                    .Size = sizeof(SkyCameraUbo)
                },
            },
            .DescriptorsCount = 1
        },

        [1] = {
            .Type = VsaShaderType_Fragment,
            .ShaderSourcePath = resource_shader("Vsr3dSky.frag"),
            .Descriptors = {},
            .DescriptorsCount = 0
        }
    };

    VsaShaderAttribute attributes[] = {
        [0] = {
            .Format = VK_FORMAT_R32G32B32_SFLOAT,
            .Offset = 0
        },
    };

    VsaPipelineDescription pipeDescr = {
        .Type = VsaPipelineType_Raw_NotIndexed,
        .RenderPassIndex = vsr_get_render_pass_id(),
        .EnableDepthStencil = 1,
        .MaxInstanceCount = INSTANCES_COUNT,
        .Stride = sizeof(v3),
        .Stages = stages,
        .StagesCount = ARRAY_COUNT(stages),
        .Attributes = attributes,
        .AttributesCount = ARRAY_COUNT(attributes),
        .PolygonMode = VK_POLYGON_MODE_FILL, //VK_POLYGON_MODE_LINE,
    };

    VsaPipeline vsaPipeline = vsa_pipeline_create(pipeDescr);

    // TODO: last one is vsrbase creation
    vsr_base_create(&pRenderer->Base, (VsrBaseSettings) {
            .Name = "Vsr3dSky",
            .pStringPool = pStringPool,
            .Pipeline = vsaPipeline,
            .MaxTexturesCount = 0,
            .MaxVerticesCount = INSTANCES_COUNT,
            .MaxItemsCount = 1,
            .MaxMaterialsCount = 0,

            .Priority = 0,
        });

    pRenderer->pCameraUniform = vsr_base_get_uniform(&pRenderer->Base, "u_SkyCameraUbo");

    // TODO/BUG: I dont like this design decision
    pRenderer->pCameraUniform->IsUpdatedByFlag = 1;

    static VsaDrawCall vsaDrawCall = {
        .VerticesOffset = 0,
        .VerticesSize = INSTANCES_COUNT * sizeof(v3),
        .IndicesOffset = 0,
        .IndicesCount = 0,
        .VerticesCount = INSTANCES_COUNT,
    };

    array_push(aVsaDrawCalls, vsaDrawCall);
    pRenderer->Base.pPipeline->DrawCalls = aVsaDrawCalls;

    /*
      -1,+1(0)        +1,+1(2)

      -1,-1(1)        +1,-1(3)

    */

    v3 vertices[INSTANCES_COUNT] = {
        [0] = (v3) { -1.0f, 1.0f },
        [1] = (v3) { 1.0f, 1.0f },
        [2] = (v3) { -1.0f, -1.0f },

        [3] = (v3) { 1.0f, 1.0f },
        [4] = (v3) { 1.0f, -1.0f },
        [5] = (v3) { -1.0f, -1.0f },
    };
    vsa_buffer_set_data(&pRenderer->Base.pPipeline->Vertex, vertices, 0, sizeof(vertices));
}

void
vsr_3d_sky_update_camera(Vsr3dSky* pRenderer, Camera* pCamera)
{

    // DOCS: Set camera uniform
    size_t size = 2 * sizeof(m4);
    struct { m4 View; m4 Projection; } camera = {};
    camera.View = pCamera->View;
    camera.Projection = pCamera->Projection;
    vsa_uniform_set_data(pRenderer->pCameraUniform, &camera, size);
    vsa_uniform_update(pRenderer->pCameraUniform, pRenderer->Base.pPipeline);
    vsa_uniform_reset(pRenderer->pCameraUniform);

    pRenderer->Base.pPipeline->IsPipelineRenderable = 1;
}

void
vsr_3d_sky_destroy(Vsr3dSky* pRenderer)
{
    vsr_base_destroy(&pRenderer->Base);
}
