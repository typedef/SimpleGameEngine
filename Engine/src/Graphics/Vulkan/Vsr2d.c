#include "Vsr2d.h"

#include "Graphics/Vulkan/Vsr.h"
#include "VulkanSimpleApi.h"

#include <AssetManager/AssetManager.h>
#include <Math/SimpleMath.h>
#include <Core/SimpleStandardLibrary.h>
#include <Graphics/Camera.h>


VsaDrawCall*
vsr_add_draw_call(VsrBase* pBase, void* pVertices, size_t verticesCount, size_t vertexSize, void* indices, size_t indicesCount)
{
    if (verticesCount > pBase->Stats.MaxVerticesCount)
    {
        GERROR("Can't render StaticModel max count exceeded!\n");
        vguard(0);
        return NULL;
    }

    // NOTE(typedef): For now we are not handling situation with "buffer exceeded event"
    size_t verticesSize = verticesCount * vertexSize;
    u64 verticesOffset = pBase->pPipeline->VerticesOffset;
    vsa_buffer_set_data(
        &pBase->pPipeline->Vertex,
        pVertices,
        verticesOffset,
        verticesSize);

    size_t indicesSize = indicesCount * sizeof(u32);
    u64 indicesOffset  = pBase->pPipeline->IndicesOffset;
    vsa_buffer_set_data(
        &pBase->pPipeline->Index,
        indices,
        indicesOffset,
        indicesSize);

    VsaDrawCall vsaDrawCall = {
        .VerticesOffset = verticesOffset,
        .VerticesSize = verticesSize,
        .IndicesOffset = indicesOffset,
        .IndicesCount = indicesCount,
    };

    VsaDrawCall* pVsaDrawCall = vsa_pipeline_add_draw_call(pBase->pPipeline, vsaDrawCall);
    return pVsaDrawCall;
}

void
vsr_pipeline_renderable(VsrBase* pBase)
{
    pBase->pPipeline->IsPipelineRenderable = 1;
}


typedef struct CameraUbo
{
    m4 ViewProjection;
} CameraUbo;

typedef struct FontUbo
{
    f32 Width;
    f32 EdgeTransition;
    i32 IsScissorsDebug;
} FontUbo;

i32
_vsr_2d_texture_submit(Vsr2d* pRenderer, VsaTexture vsaTexture)
{
    if (vsaTexture.Name == NULL)
        return -1;
    else if (string_compare(vsaTexture.Name, "-2"))
        return -2;

    vguard(vsa_texture_is_valid(vsaTexture));

    if (array_count(pRenderer->pTextures) > pRenderer->Base.Stats.MaxTexturesCount)
    {
        GERROR("Exceeded renderer texture max count!\n");
        vguard(0);
    }

    i64 baseTextureInd = array_index_of(pRenderer->pTextures, item.Id == vsaTexture.Id);
    if (baseTextureInd == -1)
    {
        baseTextureInd = array_count(pRenderer->pTextures);
        array_push(pRenderer->pTextures, vsaTexture);
    }

    return baseTextureInd;
}

void
vsr_2d_create(Vsr2d* pRenderer, const Vsr2dSettings* settings)
{
    i64 verticesCount  = settings->MaxVerticesCount;
    i64 instancesCount = settings->MaxInstancesCount;
    i64 texturesCount = settings->MaxTexturesCount;

    IStringPool* pStringPool = istring_pool_create();

    pRenderer->IsDataSubmited = 0;
    pRenderer->Current.IndicesToDraw = 0;
    pRenderer->Current.Vertices = NULL;
    pRenderer->CurrentFontInfo = settings->Font;
    pRenderer->pCamera = settings->pCamera;
    pRenderer->Rects = NULL;

    // ===============================
    //       DOCS() Demo PART
    // ===============================
    VsaShaderStage stages[] = {
        [0] = {
            .Type = VsaShaderType_Vertex,
            .ShaderSourcePath = resource_shader("Vsr2d.vert"),
            .Descriptors = {
                [0] = {
                    .Name = istring_ext(pStringPool, "u_CameraUbo"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = 1,
                    .Size = vsa_get_valid_uniform_size(sizeof(CameraUbo))
                }
            },
            .DescriptorsCount = 1
        },

        [1] = {
            .Type = VsaShaderType_Fragment,
            .ShaderSourcePath = resource_shader("Vsr2d.frag"),
            .Descriptors = {
                [0] = {
                    .Name = istring_ext(pStringPool, "u_FontUbo"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = 1,
                    .Size = vsa_get_valid_uniform_size(sizeof(FontUbo))
                },
                [1] = {
                    .Name = istring_ext(pStringPool, "u_VisibleRects"),
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = instancesCount,
                    .Size = vsa_get_valid_uniform_size(sizeof(VsrVisibleRect))
                },
                [2] = {
                    .Name = istring_ext(pStringPool, "u_Textures"),
                    .Type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
                    .Count = texturesCount,
                    .Size = 0
                },
                [3] = {
                    .Name = istring_ext(pStringPool, "u_Samplers"),
                    .Type = VK_DESCRIPTOR_TYPE_SAMPLER,
                    .Count = texturesCount,
                    .Size = 0
                },

            },
            .DescriptorsCount = 4
        }
    };

    VsaShaderAttribute attributes[] = {
        [0] = {
            .Format = VK_FORMAT_R32G32B32_SFLOAT,
            .Offset = OffsetOf(VsrVertex2d, Position)
        },
        [1] = {
            .Format = VK_FORMAT_R32G32B32A32_SFLOAT,
            .Offset = OffsetOf(VsrVertex2d, Color)
        },
        [2] = {
            .Format = VK_FORMAT_R32G32_SFLOAT,
            .Offset = OffsetOf(VsrVertex2d, Uv)
        },
        [3] = {
            .Format = VK_FORMAT_R32_SINT,
            .Offset = OffsetOf(VsrVertex2d, TextureInd)
        },
        [4] = {
            .Format = VK_FORMAT_R32_SINT,
            .Offset = OffsetOf(VsrVertex2d, IsFont)
        },
        [5] = {
            .Format = VK_FORMAT_R32_SINT,
            .Offset = OffsetOf(VsrVertex2d, RectangleInd)
        }
    };

    VsaPipelineDescription pipeDescr = {
        .Type = VsaPipelineType_Batched,
        .RenderPassIndex = vsr_get_render_pass_id(),

        .EnableDepthStencil = 1,
        .Stride = sizeof(VsrVertex2d),
        .Stages = stages,
        .StagesCount = ARRAY_COUNT(stages),
        .Attributes = attributes,
        .AttributesCount = ARRAY_COUNT(attributes)
    };
    VsaPipeline vsaPipeline = vsa_pipeline_create(pipeDescr);

    vsr_base_create(&pRenderer->Base, (VsrBaseSettings) {
            .Name = (char*) settings->pName,
            .pStringPool = pStringPool,
            .Pipeline = vsaPipeline,
            .MaxTexturesCount = settings->MaxTexturesCount,
            .MaxVerticesCount = settings->MaxVerticesCount,
            .MaxItemsCount = settings->MaxInstancesCount,
            .Priority = 80,
        });

    { // NOTE: setting uniforms for renderer
        pRenderer->pCameraUbo = vsr_base_get_uniform(&pRenderer->Base, "u_CameraUbo");
        pRenderer->pFontUbo = vsr_base_get_uniform(&pRenderer->Base, "u_FontUbo");
        pRenderer->pRectUbo = vsr_base_get_uniform(&pRenderer->Base, "u_VisibleRects");
        pRenderer->pRectUbo->IsUpdatedByFlag = 0;
    }

    { // NOTE: Push some default textures to renderer
        VsaTexture defTexture = asset_manager_get_texture(0);
        _vsr_2d_texture_submit(pRenderer, defTexture);
    }

    { // NOTE: Create indices

        i64 maxVerticesCount = pRenderer->Base.Stats.MaxVerticesCount;
        u32 indCnt = vsr_get_indices_size(maxVerticesCount);
        u32* indices = NULL;
        array_reserve(indices, indCnt);
        i32 i, temp, count = maxVerticesCount;
        for (i = 0, temp = 0; i < count; i += 6, temp += 4)
        {
            indices[i]     = 0 + temp;
            indices[i + 1] = 1 + temp;
            indices[i + 2] = 2 + temp;
            indices[i + 3] = 2 + temp;
            indices[i + 4] = 3 + temp;
            indices[i + 5] = 0 + temp;
        }
        pRenderer->Indices = indices;
    }

}

void
vsr_2d_set_visible_rect(Vsr2d* pRenderer, VsrVisibleRect rect)
{
    array_push(pRenderer->Rects, rect);
    vsa_uniform_set_data(pRenderer->pRectUbo, &rect, sizeof(VsrVisibleRect));
}

void
_vsr_2d_default_guard(Vsr2d* pRenderer)
{
    vassert_not_null(pRenderer);

    i64 verticesCount = array_count(pRenderer->Current.Vertices);
    i64 maxAcceptableVerticesCount = pRenderer->Base.Stats.MaxVerticesCount - 4;
    if (verticesCount > maxAcceptableVerticesCount)
    {
        GERROR("VerticesCnt:%d Max:%d\n", verticesCount, maxAcceptableVerticesCount);
        vassert(0 && "Out of memory!");
    }
}

void
_vsr_init_guard(VsrBase* pBase)
{
    if (!pBase->IsInitialized)
    {
        GERROR("Vulkan renderer base is not initilized [%s]\n", pBase->Name);
        vassert_break();
    }
}

#if ENGINE_DEBUG == 1
#define vsr_init_guard(pBase) _vsr_init_guard(pBase)
#define vsr_2d_default_guard(pBase) _vsr_2d_default_guard(pBase)
#else
#define vsr_init_guard(pBase)
#define vsr_2d_default_guard(pBase)
#endif

void
vsr_2d_submit_items(Vsr2d* pRenderer, v3* positions, v2* uvs, size_t itemsCount, size_t indicesCount, i32 isFont, v4 color, VsaTexture vsaTexture)
{
    vsr_init_guard(&pRenderer->Base);
    vsr_2d_default_guard(pRenderer);

    i32 textureInd = _vsr_2d_texture_submit(pRenderer, vsaTexture);
    i32 rectInd = Max((array_count(pRenderer->Rects) - 1), 0);

    for (size_t i = 0; i < itemsCount; ++i)
    {
        v3 pos = positions[i];
        v2 uv = uvs[i];

        VsrVertex2d vertex = {
            .Position = pos,
            .Color = color,
            .Uv = uv,
            .IsFont = isFont,
            .TextureInd = textureInd,
            .RectangleInd = rectInd
        };

        array_push(pRenderer->Current.Vertices, vertex);

    }

    pRenderer->IsDataSubmited = 1;
    pRenderer->Current.IndicesToDraw += indicesCount;//6;
}

/*
  DOCS: Submit rect, with this signature:
  p1 ------- p2
  |        / |
  |       /  |
  |      /   |
  |     /    |
  |    /     |
  |   /      |
  |  /       |
  | /        |
  |/         |
  p0 ------- p3

*/

void
vsr_2d_submit_positions_legacy(Vsr2d* pRenderer, v3 p0, v3 p1, v3 p2, v3 p3, v2 uv0, v2 uv1, v2 uv2, v2 uv3, i32 isFont, v4 color, VsaTexture vsaTexture)
{
    vsr_init_guard(&pRenderer->Base);
    vsr_2d_default_guard(pRenderer);

    i32 textureInd = _vsr_2d_texture_submit(pRenderer, vsaTexture);
    i32 rectInd = Max((array_count(pRenderer->Rects) - 1), 0);

    VsrVertex2d v0 = {
        .Position = p0,
        .Color = color,
        .Uv = uv0,//v2_new(0, 0),
        .IsFont = isFont,
        .TextureInd = textureInd,
        .RectangleInd = rectInd
    };
    VsrVertex2d v1 = {
        .Position = p1,
        .Color = color,
        .Uv = uv1,//v2_new(0, 1),
        .IsFont = isFont,
        .TextureInd = textureInd,
        .RectangleInd = rectInd
    };
    VsrVertex2d v2 = {
        .Position = p2,
        .Color = color,
        .Uv = uv2,//v2_new(1, 1),
        .IsFont = isFont,
        .TextureInd = textureInd,
        .RectangleInd = rectInd
    };
    VsrVertex2d v3 = {
        .Position = p3,
        .Color = color,
        .Uv = uv3,//v2_new(1, 0),
        .IsFont = isFont,
        .TextureInd = textureInd,
        .RectangleInd = rectInd
    };

    array_push(pRenderer->Current.Vertices, v0);
    array_push(pRenderer->Current.Vertices, v1);
    array_push(pRenderer->Current.Vertices, v2);
    array_push(pRenderer->Current.Vertices, v3);

    pRenderer->IsDataSubmited = 1;
    pRenderer->Current.IndicesToDraw += 6;
}

void
vsr_2d_submit_text_ext(Vsr2d* pRenderer, void* buffer, size_t length, i32 scale, v3 position, v3 rotation, v2 drawable, v4 glyphColor)
{
    vsr_init_guard(&pRenderer->Base);
    vsr_2d_default_guard(pRenderer);

    //BUG: Make this shit work, and then create working compositor here
#if 0
    m4 transform = m4_transform_wo_rotation(position, v3_new(1,1,1));
    m4 rotationMat = m4_rotate_x(transform, rad(rotation.X));
    rotationMat = m4_rotate_y(rotationMat, rad(rotation.Y));
    rotationMat = m4_rotate_z(rotationMat, rad(rotation.Z));
    quat quatRot = quat_new(rad(0), rotation);
    transform = quat_rotate_m4(quatRot, transform);

    p0 = m4_mul_v3(transform, p0);
    p1 = m4_mul_v3(transform, p1);
    p2 = m4_mul_v3(transform, p2);
    p3 = m4_mul_v3(transform, p3);

#endif
    //position = m4_mul_v3(transform, position);

    i32 textureInd = _vsr_2d_texture_submit(pRenderer, pRenderer->CurrentFontInfo.Atlas);
    //GINFO("INDEX: %d\n", textureInd);

    wchar* text = (wchar*) buffer;

    vguard(' ' == L' ' && "Fatal error");

    f32 offsetX = 0.0f;
    f32 firstCharHeight;
    for (i32 i = 0; i < length; ++i)
    {
        i32 charCode = (i32) text[i];

        v3 positions[4];
        f32 startX, startY, endX, endY, xhOffset = 0.0f;
        CharRecord* charTable = pRenderer->CurrentFontInfo.Font.CharTable;

        f32 x0 = 0.0f, x1 = 0.0f, y0 = 0.0f, y1 = 0.0f;

        if (charCode == ' ')
        {
            f32 spaceWidth = font_get_space_width(&pRenderer->CurrentFontInfo.Font);
            xhOffset = spaceWidth;
            offsetX += xhOffset;
            continue;
        }

        if (hash_geti(charTable, charCode) != -1)
        {
            CharChar charChar = hash_get(charTable, charCode);

            if (i == 0)
            {
                firstCharHeight = charChar.FullHeight;
            }

            f32 yCharOffset = scale * charChar.YOffset;
            f32 vOffset = scale * charChar.FullHeight - yCharOffset;
            xhOffset = scale * charChar.FullWidth;

            x0 = position.X + offsetX;
            x1 = x0 + xhOffset;

            y0 = position.Y
                - scale * (firstCharHeight + yCharOffset);
            y1 = y0 + scale * charChar.FullHeight;

            startX = charChar.UV.X;
            startY = charChar.UV.Y + charChar.Size.Height;
            endX   = charChar.UV.X + charChar.Size.Width;
            endY   = charChar.UV.Y;
        }
        else
        {
            DO_ONES(GERROR("Unknown font character %lc %d!\n", charCode, charCode));
        }

        offsetX += xhOffset;
        //GWARNING("OFFSETX: %f, %f\n", offsetX, drawable.X);
        if (offsetX >= drawable.X)
        {
            DO_ONES(GINFO("Hide!\n"););
            return;
        }

        v3 p0 = /* 0 0 */ v3_new(x0, y0, position.Z);
        v3 p1 = /* 0 1 */ v3_new(x0, y1, position.Z);
        v3 p2 = /* 1 1 */ v3_new(x1, y1, position.Z);
        v3 p3 = /* 1 0 */ v3_new(x1, y0, position.Z);

        v2 uv0 = v2_new(startX, startY);
        v2 uv1 = v2_new(startX, endY);
        v2 uv2 = v2_new(endX, endY);
        v2 uv3 = v2_new(endX, startY);

        //vsr_2d_submit_positions(pRenderer, p0, p1, p2, p3, uv0, uv1, uv2, uv3, 1, glyphColor, pRenderer->CurrentFontInfo.Atlas);

        v3 poss[4] = { p0, p1, p2, p3 };
        v2 uvs[4]  = { uv0, uv1, uv2, uv3 };
        const i32 itemsCount = 4;
        const i32 indicesCount = 6;
        vsr_2d_submit_items(pRenderer, poss, uvs, itemsCount, indicesCount, 1, glyphColor, pRenderer->CurrentFontInfo.Atlas);

    }

}

void
vsr_2d_flush(Vsr2d* pRenderer)
{
    vsr_init_guard(&pRenderer->Base);
    vsa_pipeline_reset(pRenderer->Base.pPipeline);

    pRenderer->Base.pPipeline->IsPipelineRenderable = 1;

    //if (!pRenderer->IsDataSubmited)
    //return;

    VsaDrawCall* pVsaDrawCall = vsr_add_draw_call(&pRenderer->Base, pRenderer->Current.Vertices, array_count(pRenderer->Current.Vertices), pRenderer->Base.pPipeline->Stride, pRenderer->Indices, pRenderer->Current.IndicesToDraw);

    vsa_pipeline_bind_textures(pRenderer->Base.pPipeline, pRenderer->pTextures, array_count(pRenderer->pTextures), pRenderer->Base.Stats.MaxTexturesCount);

    { // NOTE(typedef): Uniform updates
        CameraUbo cameraUbo = {
            .ViewProjection = pRenderer->pCamera->ViewProjection
        };
        vsa_uniform_set_data(pRenderer->pCameraUbo, &cameraUbo, sizeof(CameraUbo));

        // TODO(typedef): this should be configured w dependency on font size
        FontUbo fontUbo = {
            .Width = 0.6f,
            .EdgeTransition = 0.325f,
            .IsScissorsDebug = pRenderer->IsScissorsDebug
        };
        vsa_uniform_set_data(pRenderer->pFontUbo, &fontUbo, sizeof(FontUbo));
    }

    vsa_pipeline_update_uniforms(pRenderer->Base.pPipeline);
    vsa_pipeline_update_descriptors(pRenderer->Base.pPipeline);

    pRenderer->Base.Stats.VerticesCount = array_count(pRenderer->Current.Vertices);
    pRenderer->Base.Stats.TexturesCount = array_count(pRenderer->pTextures);
    pRenderer->Base.Stats.ItemsCount = pRenderer->Current.IndicesToDraw / 6;

    pRenderer->IsDataSubmited = 0;
    array_clear(pRenderer->Current.Vertices);
    array_clear(pRenderer->Rects);
    pRenderer->Current.IndicesToDraw = 0;
}

void
vsr_2d_destroy(Vsr2d* pRenderer)
{
    vsr_init_guard(&pRenderer->Base);

    vsr_base_destroy(&pRenderer->Base);

    array_free(pRenderer->pTextures);
    array_free(pRenderer->Rects);

    array_free(pRenderer->Current.Vertices);
    array_free(pRenderer->Prev.Vertices);
    array_free(pRenderer->Indices);
}
