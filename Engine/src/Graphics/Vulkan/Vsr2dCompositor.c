#include "Vsr2dCompositor.h"
#include "AssetManager/AssetManager.h"

#include <Graphics/Vulkan/Vsr2d.h>
#include <Math/SimpleMath.h>
#include <Core/SimpleStandardLibrary.h>

void
vsr_2d_compositor_create_screen(Vsr2dCompositor* pCompositor, Vsr2dSettings* pUiSettings)
{
    vguard_not_null(pUiSettings);

    pCompositor->ScreenRender.Base.IsInitialized = 1;
    vsr_2d_create(&pCompositor->ScreenRender, pUiSettings);
}

void
vsr_2d_compositor_create_screen2(Vsr2dCompositor* pCompositor, Vsr2dSettings* pUiSettings)
{
    vguard_not_null(pUiSettings);

    pCompositor->ScreenRender2.Base.IsInitialized = 1;
    vsr_2d_create(&pCompositor->ScreenRender2, pUiSettings);
}

void
vsr_2d_compositor_create_world(Vsr2dCompositor* pCompositor, Vsr2dSettings* pWorldSettings)
{
    vassert_not_null(pWorldSettings);

    pCompositor->WorldRender.Base.IsInitialized = 1;
    vsr_2d_create(&pCompositor->WorldRender, pWorldSettings);
}

void
vsr_2d_compositor_destroy(Vsr2dCompositor* pCompositor)
{
    if (pCompositor->ScreenRender.Base.IsInitialized)
    {
        GINFO("Destroy ScreenRender\n");
        vsr_2d_destroy(&pCompositor->ScreenRender);
    }

    if (pCompositor->ScreenRender2.Base.IsInitialized)
    {
        GINFO("Destroy ScreenRender2\n");
        vsr_2d_destroy(&pCompositor->ScreenRender2);
    }

    if (pCompositor->WorldRender.Base.IsInitialized)
    {
        GINFO("Destroy WorldRender\n");
        vsr_2d_destroy(&pCompositor->WorldRender);
    }
}

void
vsr_2d_compositor_set_visible_rect(Vsr2dCompositor* pCompositor, VsrVisibleRect rect)
{
    vsr_2d_set_visible_rect(&pCompositor->ScreenRender, rect);
}

void
vsr_2d_compositor_submit_items(Vsr2dCompositor* pCompositor, v3* positions, v2* uvs, size_t itemsCount, size_t indicesCount, i32 isFont, v4 color, VsaTexture vsaTexture)
{
    vsr_2d_submit_items(&pCompositor->ScreenRender, positions, uvs, itemsCount, indicesCount, isFont, color, vsaTexture);
}

void
vsr_2d_compositor_submit(Vsr2dCompositor* pCompositor, v3 position, v2 size, v4 color, VsaTexture vsaTexture)
{
    v3 p0 = v3_sub_y(position, size.Height);
    v3 p1 = position;
    v3 p2 = v3_new(position.X + size.Width, position.Y, position.Z);
    v3 p3 = v3_new(position.X + size.Width, position.Y - size.Height, position.Z);

    v2 uv0 = v2_new(0, 0);
    v2 uv1 = v2_new(0, 1);
    v2 uv2 = v2_new(1, 1);
    v2 uv3 = v2_new(1, 0);

    v3 poss[4] = { p0, p1, p2, p3 };
    v2 uvs[4]  = { uv0, uv1, uv2, uv3 };
    vsr_2d_compositor_submit_items(pCompositor, poss, uvs, 4, 6, 0, color, vsaTexture);
}

void
vsr_2d_compositor_submit_rect(Vsr2dCompositor* pCompositor, v3 position, v2 size, v4 color)
{
    VsaTexture empty = {};
    vsr_2d_compositor_submit(pCompositor, position, size, color, empty);
}

void
vsr_2d_compositor_submit_line(Vsr2d* pRenderer, v3 start, v3 end, f32 thickness, v4 color)
{
    f32 dx = f32_abs(start.X - end.X);
    f32 dyn = start.Y - end.Y;

    f32 dy = f32_abs(dyn);
    f32 dt = (dy > 0) ? (-thickness) : (+thickness);

    v3 poss[4] = {
        [0] = start,
        [1] = v3_new(start.X, start.Y + dt, start.Z),
        [2] = v3_new(end.X, end.Y + dt, end.Z),
        [3] = v3_new(end.X, end.Y, end.Z),
    };
    v2 uvs[4] = {};
    VsaTexture vsaTexture = {};

    vsr_2d_submit_items(pRenderer, poss, uvs, 4, 6, 0, color, vsaTexture);
}

void
vsr_2d_compositor_world_submit_line(Vsr2d* pRenderer, v3 start, v3 end, f32 thickness, v4 color)
{
    i32 dx = (f32_abs((start.X - end.X)) > F32_CUSTOM_EPSILON);
    i32 dy = (f32_abs((start.Y - end.Y)) > F32_CUSTOM_EPSILON);
    i32 dz = (f32_abs((start.Z - end.Z)) > F32_CUSTOM_EPSILON);

    //f32 dt = (dy > 0) ? (-thickness) : (+thickness);
    f32 dt = thickness;

    size_t verticesCount = 16;
    size_t indicesCount = 24;

    v3 poss[16] = {};

    if (dx)
    {
        // DOCS: X-Axis Horizontal lines
        poss[0 ] = start;
        poss[1 ] = v3_new(start.X, start.Y + dt, start.Z);
        poss[2 ] = v3_new(end.X, end.Y + dt, end.Z);
        poss[3 ] = v3_new(end.X, end.Y, end.Z);

        poss[4 ] = v3_new(start.X, start.Y, start.Z);
        poss[5 ] = v3_new(start.X, start.Y, start.Z - dt);
        poss[6 ] = v3_new(end.X, end.Y, end.Z - dt);
        poss[7 ] = v3_new(end.X, end.Y, end.Z);

        poss[8 ] = v3_new(start.X, start.Y, start.Z - dt);
        poss[9 ] = v3_new(start.X, start.Y + dt, start.Z - dt);
        poss[10] = v3_new(end.X, end.Y + dt, end.Z - dt);
        poss[11] = v3_new(end.X, end.Y, end.Z - dt);

        poss[12] = v3_new(start.X, start.Y + dt, start.Z - dt);
        poss[13] = v3_new(start.X, start.Y + dt, start.Z);
        poss[14] = v3_new(end.X, end.Y + dt, end.Z);
        poss[15] = v3_new(end.X, end.Y + dt, end.Z - dt);

    }
    else if (dy)
    {
        // DOCS: Y-Axis Vertical lines
        poss[0 ] = start;
        poss[1 ] = v3_new(start.X + dt, start.Y, start.Z);
        poss[2 ] = v3_new(end.X + dt, end.Y, end.Z);
        poss[3 ] = v3_new(end.X, end.Y, end.Z);

        poss[4 ] = start;
        poss[5 ] = v3_new(start.X, start.Y, start.Z - dt);
        poss[6 ] = v3_new(end.X, end.Y, end.Z - dt);
        poss[7 ] = end;

        poss[8 ] = v3_new(start.X, start.Y, start.Z - dt);
        poss[9 ] = v3_new(start.X + dt, start.Y, start.Z - dt);
        poss[10] = v3_new(end.X + dt, end.Y, end.Z - dt);
        poss[11] = v3_new(end.X, end.Y, end.Z - dt);

        poss[12] = v3_new(start.X + dt, start.Y, start.Z - dt);
        poss[13] = v3_new(start.X + dt, start.Y, start.Z);
        poss[14] = v3_new(end.X + dt, end.Y, end.Z);
        poss[15] = v3_new(end.X + dt, end.Y, end.Z - dt);

    }
    else if (dz)
    {
        // DOCS: Z-Axis Vertical lines
        poss[0 ] = start;
        poss[1 ] = v3_new(start.X, start.Y + dt, start.Z);
        poss[2 ] = v3_new(end.X, end.Y + dt, end.Z);
        poss[3 ] = v3_new(end.X, end.Y, end.Z);

        poss[4 ] = v3_new(start.X, start.Y, start.Z);
        poss[5 ] = v3_new(start.X + dt, start.Y, start.Z);
        poss[6 ] = v3_new(end.X + dt, end.Y, end.Z);
        poss[7 ] = v3_new(end.X, end.Y, end.Z);

        poss[8 ] = v3_new(start.X + dt, start.Y, start.Z);
        poss[9 ] = v3_new(start.X + dt, start.Y + dt, start.Z);
        poss[10] = v3_new(end.X + dt, end.Y + dt, end.Z);
        poss[11] = v3_new(end.X + dt, end.Y, end.Z);

        poss[12] = v3_new(start.X + dt, start.Y + dt, start.Z);
        poss[13] = v3_new(start.X, start.Y + dt, start.Z);
        poss[14] = v3_new(end.X, end.Y + dt, end.Z);
        poss[15] = v3_new(end.X + dt, end.Y + dt, end.Z);
    }

    v2 uvs[4] = {};
    VsaTexture vsaTexture = {};

    vsr_2d_submit_items(pRenderer, poss, uvs, verticesCount, indicesCount, 0, color, vsaTexture);
}

void
vsr_2d_compositor_submit_empty_rect(Vsr2dCompositor* pCompositor, v3 position, v2 size, f32 thickness, v4 color)
{
    // top line
    vsr_2d_compositor_submit_rect(pCompositor, position, v2_new(size.X, thickness), color);

    // bot line
    vsr_2d_compositor_submit_rect(pCompositor,
                       v3_new(position.X, position.Y - size.Y + thickness, position.Z),
                       v2_new(size.X, thickness),
                       color);

    // left
    vsr_2d_compositor_submit_rect(pCompositor,
                       v3_new(position.X - thickness, position.Y, position.Z),
                       v2_new(thickness, size.Y),
                       color);

    // right
    vsr_2d_compositor_submit_rect(pCompositor,
                       v3_new(position.X + size.X - thickness, position.Y, position.Z),
                       v2_new(thickness, size.Y),
                       color);

}

void
vsr_2d_compositor_submit_circle_ext(Vsr2dCompositor* pCompositor, v3 position, v2 size, v4 color, f32 thikness, f32 fade, i32 multiplier)
{

    // DOCS: Set default value
    if (fade == 0.0f)
    {
        fade = 0.049f;
    }

    if (multiplier == 0)
    {
        multiplier = 2.0f;
    }

    v3 poss[4] = {
        [0] = position,
        [1] = v3_sub_y(position, size.Height),
        [2] = v3_new(position.X + size.Width, position.Y - size.Height, position.Z),
        [3] = v3_new(position.X + size.Width, position.Y, position.Z),
    };

    v2 vuv = v2_new(thikness, fade);
    v2 uvs[4] = {
        [0] = vuv,
        [1] = vuv,
        [2] = vuv,
        [3] = vuv,
    };
    VsaTexture vsaTexture = { .Name = "-2" };

    vsr_2d_compositor_submit_items(pCompositor, poss, uvs, 4, 6, multiplier, color, vsaTexture);

}

void
vsr_2d_compositor_submit_text_ext(Vsr2dCompositor* pCompositor, void* buffer, size_t length, i32 scale, v3 position, v3 rotation, v2 drawable, v4 glyphColor)
{
    vsr_2d_submit_text_ext(&pCompositor->ScreenRender, buffer, length, scale, position, rotation, drawable, glyphColor);
}

void
vsr_2d_compositor_flush_screen(Vsr2dCompositor* pCompositor)
{
    vsr_2d_flush(&pCompositor->ScreenRender);
}
void
vsr_2d_compositor_flush_screen2(Vsr2dCompositor* pCompositor)
{
    vsr_2d_flush(&pCompositor->ScreenRender2);
}

void
vsr_2d_compositor_flush_world(Vsr2dCompositor* pCompositor)
{
    vsr_2d_flush(&pCompositor->WorldRender);
}

#if 0



/*
  DOCS(typedef): Tests
*/

void
vsr_test_submit_circle_and_line(Vsr2dCompositor* pCompositor, VsaTexture someTexture)
{
    // NOTE(typedef): Just check circle rendering
    f32 thikness, fade;
    vsr_2d_submit_circle_ext(renderer,
                             v3_new(100.0f, 500.0f, 1.1f),
                             v2_new(200.0f, 200.0f),
                             v4_new(0, 0.23f, 0.71f, 1.0f),
                             thikness = 1.35f,
                             fade = 0.049f,
                             32);

    vsr_2d_submit_circle_ext(
        renderer,
        v3_new(400.0f, 500.0f, 1.1f),
        v2_new(200.0f, 200.0f),
        v4_new(0, 0.23f, 0.71f, 1.0f),
        thikness = 1.35f,
        fade = 0.049f,
        4);

    vsr_2d_submit_circle_ext(
        renderer,
        v3_new(10, 400, 4),
        v2_new(20, 20),
        v4_new(0.4, 0.4, 0.4, 1.0),
        1.1f, 0, 2);

    //NOTE(): Line
    vsr_2d_submit_line(
        renderer,
        v3_new(150, 240, 4),
        v3_new(450, 260, 4),
        2.0f,
        v4_new(1,1,1,1));

    vsr_2d_submit_line(
        renderer,
        v3_new(250, 560, 4),
        v3_new(350, 160, 4),
        2.0f,
        v4_new(1,1,1,1));

    vsr_2d_submit_line(
        renderer,
        v3_new(250, 160, 4),
        v3_new(350, 66, 4),
        2.0f,
        v4_new(1,1,1,1));

    vsr_2d_submit_line(
        renderer,
        v3_new(150, 66, 4),
        v3_new(350, 160, 4),
        20.0f,
        v4_new(1,1,1,1));

    vsr_2d_submit(renderer, v3_new(600.0f, 1000.0f, 100.0f), v2_new(200,300), v4_new(1,1,1,1), someTexture);
    vsr_2d_submit_rect(renderer, v3_new(500, 950, 50), v2_new(100,100), v4_new(0.9541, 0.1, 0.541, 0.8));
    vsr_2d_submit_rect(renderer, v3_new(550, 1000, 49), v2_new(100,100), v4_new(0.2141, 0.1,0.7541, 0.78));
    vsr_2d_submit_rect(renderer, v3_new(250.0f, 250.0f, 0.5f), v2_new(100,100), v4_new(1.41,.6541,0.541,1));
    vsr_2d_submit_rect(renderer, v3_new(700.0f, 1100.0f, 1.0f), v2_new(100,40), v4_new(1.41,.6541,0.541,1));
    vsr_2d_submit_empty_rect(renderer, v3_new(100.0, 100.5, 0.5f), v2_new(50, 50), 2.0f, v4_new(0.43, 0.843, 0.234, 0.6));

    static WideString justSomeMessage = {
        .Buffer = L"Просто сообщение",
        .Length = 16
    };
    vsr_2d_submit_text_ext(
        renderer, justSomeMessage.Buffer, justSomeMessage.Length,
        4,
        v3_new(1220, 400, 1),
        v3_new(1, 1, 1),
        v2_new(1000, 1000),
        v4_new(1, 1, 1, 0.0));
}

#endif
