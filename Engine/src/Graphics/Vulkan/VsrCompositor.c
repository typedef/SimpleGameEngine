#include "VsrCompositor.h"

#include <Core/SimpleStandardLibrary.h>

static VsrCompositor* aCompositors = NULL;

void*
vsr_compositor_register(const char* pName, void* pData, size_t size, OnVsrDestroy onDestroy)
{
    void* allocatedData = memory_allocate(size);
    memcpy(allocatedData, pData, size);

    VsrCompositor vsrCompositor = {
        .pName = string(pName),
        .pData = allocatedData,
        .OnDestroy = onDestroy,
    };

    array_push(aCompositors, vsrCompositor);

    return allocatedData;
}

void*
vsr_compositor_get(const char* pName)
{
    i64 ind = array_index_of(aCompositors, string_compare(item.pName, pName));
    if (ind == -1)
        return NULL;

    return aCompositors[ind].pData;
}

void
vsr_compositor_destroy()
{
    i64 i, count = array_count(aCompositors);
    for (i = 0; i < count; ++i)
    {
        VsrCompositor vsrCompositor = aCompositors[i];
        vsrCompositor.OnDestroy(vsrCompositor.pData);

        memory_free(vsrCompositor.pName);
    }

    //array_foreach(aCompositors, item.OnDestroy(item.pData));
}
