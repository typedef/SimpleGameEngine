#include "Vsr3dCompositor.h"

#include <Core/SimpleStandardLibrary.h>
#include <AssetManager/AssetManager.h>
#include <Graphics/Camera.h>
#include <Model/StaticModel.h>
#include <Math/SimpleMath.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>

// DOCS: Forward Declarations
MeshMaterialInfo _get_mesh_info(Vsr3dCompositor* pBase, MeshMaterial* pMeshMaterial);

#define ENABLE_3D_SKY 0

void
vsr_3d_compositor_create(Vsr3dCompositor* pCompositor, Vsr3dSSetting* pVsr3dsSet, Vsr3dDSetting* pVsr3ddSet, Camera* pCamera)
{
    // DOCS: Create && Push sky with update of camera
#if ENABLE_3D_SKY == 1
    vsr_3d_sky_create(&pCompositor->SkyRender);
    vsr_3d_sky_update_camera(&pCompositor->SkyRender, pCamera);
#endif

    vsr_3d_s_create(&pCompositor->Render, pVsr3dsSet);
    vsr_3d_d_create(&pCompositor->DynamicRender, pVsr3ddSet);

    pCompositor->pCamera = pCamera;

    VsaFont vf = asset_manager_load_font(resource_font("NotoSans.ttf"));
    array_push(pCompositor->aTextures, vf.Atlas);
}

void
vsr_3d_compositor_destroy(Vsr3dCompositor* pCompositor)
{
#if ENABLE_3D_SKY == 1
    vsr_3d_sky_destroy(&pCompositor->SkyRender);
#endif

    vsr_3d_s_destroy(&pCompositor->Render);
    vsr_3d_d_destroy(&pCompositor->DynamicRender);
}

void
vsr_3d_compositor_update_camera_ubo(Vsr3dCompositor* pCompositor)
{
#if ENABLE_3D_SKY == 1
    vsr_3d_sky_update_camera(&pCompositor->SkyRender, pCompositor->pCamera);
#endif
    m4 viewProj = pCompositor->pCamera->ViewProjection;
    vsr_3d_s_set_camera_ubo(&pCompositor->Render, viewProj);
    vsr_3d_d_set_camera_ubo(&pCompositor->DynamicRender, viewProj);
}

void
vsr_3d_compositor_submit_model(Vsr3dCompositor* pCompositor, StaticModel* pModel, m4 transform)
{
    i64 meshesCount = array_count(pModel->aMeshes);
    for (i64 i = 0; i < meshesCount; ++i)
    {
        StaticMesh* pMesh = &pModel->aMeshes[i];
        i64 matId = pMesh->MaterialId;
        MeshMaterial mat = asset_manager_load_material_by_id(matId);
        VsaTexture vt = asset_manager_load_texture_by_id(mat.Metallic.BaseColorTexture);

        MeshMaterialInfo meshMatInfo = _get_mesh_info(pCompositor, &mat);

        i64 meshMaterialInfosCnt = array_count(pCompositor->aMeshMaterialInfos);
        array_push(pCompositor->aMeshMaterialInfos, meshMatInfo);

        array_push(pCompositor->aMaterialIndices, array_count(pCompositor->aMaterialIndices));

        //TODO: Reset vbuffer/ibuffer in flush
        VsrMeshRecord rec = vsr_3d_s_submit_mesh(&pCompositor->Render, pMesh, transform, &pCompositor->aMeshMaterialInfos[meshMaterialInfosCnt]);

        array_push(pCompositor->aMeshRecords, rec);
    }

}

void
vsr_3d_compositor_submit_dynamic_model(Vsr3dCompositor* pCompositor, DynamicModel* pModel, m4 transform)
{
    i64 meshesCount = array_count(pModel->aMeshes);
    for (i64 i = 0; i < meshesCount; ++i)
    {
        DynamicMesh* pMesh = &pModel->aMeshes[i];
        i64 matId = pMesh->MaterialId;
        MeshMaterial mat = asset_manager_load_material_by_id(matId);
        VsaTexture vt = asset_manager_load_texture_by_id(mat.Metallic.BaseColorTexture);

        MeshMaterialInfo meshMatInfo = _get_mesh_info(pCompositor, &mat);

        i64 meshMaterialInfosCnt = array_count(pCompositor->aMeshMaterialInfos);
        array_push(pCompositor->aMeshMaterialInfos, meshMatInfo);

        array_push(pCompositor->aMaterialIndices, array_count(pCompositor->aMaterialIndices));

        //TODO: Reset vbuffer/ibuffer in flush
        VsrMeshRecord rec = vsr_3d_d_submit_mesh(&pCompositor->DynamicRender, pMesh, transform, &pCompositor->aMeshMaterialInfos[meshMaterialInfosCnt]);

        array_push(pCompositor->aDynamicMeshRecords, rec);
    }

}

void
_vsr_3d_compositor_flush_static(Vsr3dCompositor* pCompositor)
{
    Vsr3dS* pRender = &pCompositor->Render;
    if (array_count(pCompositor->aMeshRecords) <= 0)
        return;

    vsr_3d_s_set_instance_ubo(&pCompositor->Render, pCompositor->aMaterialIndices);
    vsr_3d_s_set_camera_ubo(&pCompositor->Render, pCompositor->pCamera->ViewProjection);

    VsaPipeline* pVsaPipeline = pCompositor->Render.Base.pPipeline;
    vsa_pipeline_bind_textures(pVsaPipeline, pCompositor->aTextures, array_count(pCompositor->aTextures), pCompositor->Render.Base.Stats.MaxTexturesCount);
    vsa_pipeline_update_uniforms(pVsaPipeline);
    vsa_pipeline_update_descriptors(pVsaPipeline);

    vsr_3d_s_flush(&pCompositor->Render, pCompositor->aMeshRecords, array_count(pCompositor->aMeshRecords));
}

void
_vsr_3d_compositor_flush_dynamic(Vsr3dCompositor* pCompositor)
{
    Vsr3dD* pRender = &pCompositor->DynamicRender;
    if (array_count(pCompositor->aDynamicMeshRecords) <= 0)
        return;

    vsr_3d_d_set_instance_ubo(pRender, pCompositor->aMaterialIndices);
    vsr_3d_d_set_camera_ubo(pRender, pCompositor->pCamera->ViewProjection);

    VsaPipeline* pVsaPipeline = pRender->Base.pPipeline;
    vsa_pipeline_bind_textures(pVsaPipeline, pCompositor->aTextures, array_count(pCompositor->aTextures), pRender->Base.Stats.MaxTexturesCount);
    vsa_pipeline_update_uniforms(pVsaPipeline);
    vsa_pipeline_update_descriptors(pVsaPipeline);

    vsr_3d_d_flush(pRender, pCompositor->aDynamicMeshRecords, array_count(pCompositor->aDynamicMeshRecords));
}

void
vsr_3d_compositor_flush(Vsr3dCompositor* pCompositor)
{
    // DOCS: Indirect drawing render
    _vsr_3d_compositor_flush_static(pCompositor);
    _vsr_3d_compositor_flush_dynamic(pCompositor);

    array_free(pCompositor->aTextures);
    array_free(pCompositor->aMaterialIndices);
    array_free(pCompositor->aMeshMaterialInfos);
    array_free(pCompositor->aMeshRecords);
    array_free(pCompositor->aDynamicMeshRecords);

    vsa_uniform_reset(pCompositor->Render.pTransformUniform);
    vsa_uniform_reset(pCompositor->Render.pInstanceUniform);
    vsa_uniform_reset(pCompositor->Render.pMaterialUniform);
    vsa_uniform_reset(pCompositor->Render.pCameraUbo);
}


i64
_easy_load(Vsr3dCompositor* pCompositor, VsaTextureId textureId)
{
    VsaTexture vsaTexture = asset_manager_get_texture((textureId == -1) ? 0 : textureId);
    vguard(vsa_texture_is_valid(vsaTexture));

    if (array_count(pCompositor->aTextures) > pCompositor->Render.Base.Stats.MaxTexturesCount)
    {
        GERROR("Exceeded renderer texture max count!\n");
        vguard(0);
    }

    i64 baseTextureInd = array_index_of(pCompositor->aTextures, item.Id == textureId);
    if (baseTextureInd == -1)
    {
        baseTextureInd = array_count(pCompositor->aTextures);
        array_push(pCompositor->aTextures, vsaTexture);
    }

    return baseTextureInd;
}

MeshMaterialInfo
_get_mesh_info(Vsr3dCompositor* pBase, MeshMaterial* pMeshMaterial)
{
    i64 baseColorTextureInd = _easy_load(pBase, pMeshMaterial->Metallic.BaseColorTexture);
    i64 metallicRoughnessTextureInd = _easy_load(pBase, pMeshMaterial->Metallic.MetallicRoughnessTexture);

    i64 specularTextureInd = _easy_load(pBase, pMeshMaterial->Specular.Texture);
    i64 specularColorTextureInd = _easy_load(pBase, pMeshMaterial->Specular.ColorTexture);

    i64 specularDiffuseTextureInd = _easy_load(pBase, pMeshMaterial->SpecularGlossiness.DiffuseTexture);
    i64 specularGlossinessTextureInd = _easy_load(pBase, pMeshMaterial->SpecularGlossiness.SpecularGlossinessTexture);

    i64 clearCoatTextureInd = _easy_load(pBase, pMeshMaterial->ClearCoat.ClearcoatTexture);
    i64 clearCoatRoughnessTextureInd = _easy_load(pBase, pMeshMaterial->ClearCoat.ClearcoatRoughnessTexture);
    i64 clearCoatNormalTextureInd = _easy_load(pBase, pMeshMaterial->ClearCoat.ClearcoatNormalTexture);

    i64 transmissionTextureInd = _easy_load(pBase, pMeshMaterial->Transmission.TransmissionTexture);

    i64 volumeThicknessTextureInd = _easy_load(pBase, pMeshMaterial->Volume.ThicknessTexture);

    i64 sheenColorTextureInd = _easy_load(pBase, pMeshMaterial->Sheen.SheenColorTexture);
    i64 sheenRoughnessTextureInd = _easy_load(pBase, pMeshMaterial->Sheen.SheenRoughnessTexture);

    i64 emissiveTextureInd = _easy_load(pBase, pMeshMaterial->Emissive.Texture);

    i64 iridescenceTextureInd = _easy_load(pBase, pMeshMaterial->Iridescence.Texture);
    i64 iridescenceThicknessTextureInd = _easy_load(pBase, pMeshMaterial->Iridescence.ThicknessTexture);

    i64 normalTextureInd = _easy_load(pBase, pMeshMaterial->Normal);
    i64 occlusionTextureInd = _easy_load(pBase, pMeshMaterial->Occlusion);

    MeshMaterialInfo materialInfo = {
        .BaseColorTexture = baseColorTextureInd, // TODO THIS
        .MetallicRoughnessTexture = metallicRoughnessTextureInd,
        .SpecularTexture = specularTextureInd,
        .ColorTexture = specularColorTextureInd,

        .MetallicFactor = pMeshMaterial->Metallic.MetallicFactor,
        .RoughnessFactor = pMeshMaterial->Metallic.RoughnessFactor,
        .BaseColor = pMeshMaterial->Metallic.BaseColor,

        // PbrSpecular Specular
        .SpecularFactor = pMeshMaterial->Specular.Factor,
        .ColorFactor = v4_new_v3(pMeshMaterial->Specular.ColorFactor),

        // PbrSpecularGlossiness SpecularGlossiness
        .GlossinessFactor = pMeshMaterial->SpecularGlossiness.GlossinessFactor,
        .SpecularGlossinessFactor = pMeshMaterial->SpecularGlossiness.SpecularFactor,
        .DiffuseFactor = pMeshMaterial->SpecularGlossiness.DiffuseFactor,
        .DiffuseTexture = specularDiffuseTextureInd,
        .SpecularGlossinessTexture = specularGlossinessTextureInd,

        // PbrClearCoat ClearCoat
        .ClearcoatFactor = pMeshMaterial->ClearCoat.ClearcoatFactor,
        .ClearcoatRoughnessFactor = pMeshMaterial->ClearCoat.ClearcoatRoughnessFactor,
        .ClearcoatTexture = clearCoatTextureInd,
        .ClearcoatRoughnessTexture = clearCoatRoughnessTextureInd,
        .ClearcoatNormalTexture = clearCoatNormalTextureInd,

        // PbrTransmission Transmission
        .TransmissionFactor = pMeshMaterial->Transmission.TransmissionFactor,
        .TransmissionTexture = transmissionTextureInd,

        // PbrVolume Volume
        .ThicknessFactor = pMeshMaterial->Volume.ThicknessFactor,
        .AttenuationDistance = pMeshMaterial->Volume.AttenuationDistance,
        .AttenuationColor = pMeshMaterial->Volume.AttenuationColor,
        .VolumeThicknessTexture = volumeThicknessTextureInd,

        // PbrSheen Sheen
        .SheenRoughnessFactor = pMeshMaterial->Sheen.SheenRoughnessFactor,
        .SheenColorFactor = pMeshMaterial->Sheen.SheenColorFactor,
        .SheenColorTexture = sheenColorTextureInd,
        .SheenRoughnessTexture = sheenRoughnessTextureInd,

        // PbrEmissive Emissive
        .EmissiveStrength = pMeshMaterial->Emissive.Strength,
        .EmissiveFactor = pMeshMaterial->Emissive.Factor,
        .EmissiveTexture = emissiveTextureInd,

        // PbrIridescence Iridescence
        .IridescenceFactor = pMeshMaterial->Iridescence.Factor,
        .IridescenceIor = pMeshMaterial->Iridescence.Ior,
        .ThicknessMin = pMeshMaterial->Iridescence.ThicknessMin,
        .ThicknessMax = pMeshMaterial->Iridescence.ThicknessMax,
        .IridescenceTexture = iridescenceTextureInd,
        .IridescenceThicknessTexture = iridescenceThicknessTextureInd,

        //PbrAlpha Alpha
        .Mode = pMeshMaterial->Alpha.Mode,
        .CutOff = pMeshMaterial->Alpha.CutOff,

        .Normal = normalTextureInd,
        .Occlusion = occlusionTextureInd,

        .Ior = pMeshMaterial->Ior
    };

    return materialInfo;
}
































#if 0

i32
renderable_comparer(const void* a, const void* b)
{
    i64 av = ((VsrRenderable*) a)->MeshId;
    i64 bv = ((VsrRenderable*) b)->MeshId;

    if (av > bv)
        return 1;
    else if (av < bv)
        return -1;

    return 0;
}

void
vsrf_draw_chunks(Vsr3dRenderer* pRenderer)
{
    VkDrawIndexedIndirectCommand* cmds = NULL;
    i64 instancesCnt = 0;

    /*
      DOCS:
      Get uniques
      * Models (as Meshes)
      * Set it to buffer
      */
    i64 renderablesCount = array_count(pRenderer->PassedRenderables);
    qsort(pRenderer->PassedRenderables, renderablesCount, sizeof(VsrRenderable), renderable_comparer);

    ////////////////////////////////////////
    // NOTE: BEGIN OF _get_indirect_records //
    ////////////////////////////////////////
    MeshRecord* pMeshRecords = NULL;

    size_t vbufferOffset = 0;
    size_t ibufferOffset = 0;
    size_t verticesOffsetCnt = 0;
    size_t indicesOffsetCnt = 0;
    i64* meshIds = NULL;
    i64 renderablesCount = array_count(pRenderer->PassedRenderables);

    array_foreach(pRenderer->PassedRenderables,
                  _set_instance_for_renderable(pRenderer, item));

    for (i64 m = 0; m < renderablesCount; ++m)
    {
        VsrRenderable renderable = pRenderer->PassedRenderables[m];
        i64 meshId = renderable.MeshId;

        i64 cnt = 1;
        i64 j;

        for (j = (m + 1); j < renderablesCount; ++j)
        {
            VsrRenderable otherObj = pRenderer->PassedRenderables[j];
            if (otherObj.MeshId != renderable.MeshId)
                break;

            ++cnt;
        }

        if (cnt > 1)
        {
            m += cnt - 1;
        }

        i64 modelInd = collection_find_id(meshIds, meshId);
        if (modelInd != -1)
            continue;

        array_push(meshIds, meshId);

        MeshRecord modelRecord = {
            .MeshId = meshId,
            .Count = cnt,
            .VerticesCountInd = verticesOffsetCnt,
            .IndicesCountInd = indicesOffsetCnt,
            .VBufferOffset = vbufferOffset,
            .IBufferOffset = ibufferOffset,
        };
        array_push(pMeshRecords, modelRecord);

        StaticMesh mesh = *((StaticMesh*) asset_manager_get(AssetType_StaticMesh, meshId));
        i64 verticesCnt = array_count(pMesh->Vertices);
        i64 indicesCnt = array_count(pMesh->Indices);

        // NOTE: setting buffer
        size_t vsize = verticesCnt * sizeof(StaticMeshVertex);
        size_t isize = indicesCnt * sizeof(u32);
        vsa_buffer_set_data(&pRenderer->Base.Pipeline.Vertex, pMesh->Vertices, vbufferOffset, vsize);
        vsa_buffer_set_data(&pRenderer->Base.Pipeline.Index, pMesh->Indices, ibufferOffset, isize);

        vbufferOffset += vsize;
        ibufferOffset += isize;
        verticesOffsetCnt += verticesCnt;
        indicesOffsetCnt += indicesCnt;

    }

    GINFO(RED("instance:")" %ld\n", renderablesCount);
    GINFO(RED("modelsCount:")" %ld (doubled: %ld)\n", renderablesCount, renderablesCount*2);
    pRenderer->Base.Stats.ItemsCount = renderablesCount;
    ////////////////////////////////////////
    // NOTE: END OF _get_indirect_records //
    ////////////////////////////////////////

    i64 meshesCount = array_count(pMeshRecords);
    GINFO(RED("meshesCount:")" %ld\n", meshesCount);
    for (i64 i = 0; i < meshesCount; ++i)
    {
        MeshRecord meshRecord = pMeshRecords[i];

        StaticMesh* pMesh = asset_manager_get(AssetType_StaticMesh, meshRecord.MeshId);

        i64 indicesCnt = array_count(pMesh->Indices);

        VkDrawIndexedIndirectCommand idirectCmd = {
            .indexCount = indicesCnt, //NOTE: DONE
            .instanceCount = meshRecord.Count, // 1|multiple mesh to draw
            .firstIndex = meshRecord.IndicesCountInd,
            .vertexOffset = meshRecord.VerticesCountInd,
            .firstInstance = instancesCnt // wo feature enabled is always 0
        };

        array_push(cmds, idirectCmd);

        //++instancesCnt;
        instancesCnt += meshRecord.Count;
        //_vsrf_push_mesh(pRenderer, &cmds, meshRecord, &);
    }


    vsa_buffer_set_data(&pRenderer->Base.Pipeline.IndirectCmd.IndirectBuffer,
                        cmds, 0,
                        array_count(cmds) * sizeof(VkDrawIndexedIndirectCommand));
    pRenderer->Base.Pipeline.IndirectCmd.InstancesCount = meshesCount;
    array_free(cmds);

    // TODO: set cmds to buffer
    vsa_buffer_set_data(&pRenderer->Base.Pipeline.IndirectCmd.IndirectBuffer,
                        cmds, 0,
                        array_count(cmds) * sizeof(VkDrawIndexedIndirectCommand));
    pRenderer->Base.Pipeline.IndirectCmd.InstancesCount = meshesCount;
    array_free(cmds);

    // NOTE: set uniforms
    vsrf_update_camera_ubo(pRenderer);
    vsa_uniform_update(pRenderer->pTransformUniform, &pRenderer->Base.Pipeline);
    vsa_uniform_update(pRenderer->pMaterialUniform, &pRenderer->Base.Pipeline);

    i64 textCount = array_count(pRenderer->Base.pTextures);
    vsa_pipeline_bind_textures(&pRenderer->Base.Pipeline, pRenderer->Base.pTextures, textCount, pRenderer->Base.MaxTexturesCount);
    vsa_pipeline_update_descriptors(&pRenderer->Base.Pipeline);

    // Change Gpu chunks
    array_clear(pRenderer->GpuChunks);
    array_foreach(pRenderer->PassedChunks,
                  array_push(pRenderer->GpuChunks, item););

}

#endif // 0
