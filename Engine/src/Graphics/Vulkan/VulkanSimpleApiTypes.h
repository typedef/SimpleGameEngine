#ifndef VULKAN_SIMPLE_API_TYPES_H
#define VULKAN_SIMPLE_API_TYPES_H

//#include <vulkan/vulkan_core.h>
#include <Core/Types.h>
#include <Utils/Font.h>
#include <Graphics/SimpleWindow.h>

typedef struct VsaSettings
{
    i32 IsDebugEnabled;
    i32 IsVSyncEnabled;
    i32 SamplesCount;
} VsaSettings;

/*
  DOCS(typedef): Structs
*/
typedef struct VsaQueueFamily
{
    i32 GraphicsIndex;
    i32 PresentationIndex;
} VsaQueueFamily;

typedef struct VsaSwapChainSettings
{
    VkSurfaceFormatKHR SurfaceFormat;
    VkPresentModeKHR PresentationMode;
    v2 Size;
} VsaSwapChainSettings;

typedef struct VsaImageCreateExtSettings
{
    i32 Width;
    i32 Height;
    i32 Depth;
    i32 MipLevels;
    VkSampleCountFlagBits SamplesCount;
    VkFormat Format;
    VkImageTiling ImageTiling;
    VkImageUsageFlags ImageUsageFlags;
    VkMemoryPropertyFlags MemoryPropertyFlags;
} VsaImageCreateExtSettings;

typedef enum VsaImageViewType
{
    // DOCS: View Image created from code
    VsaImageViewType_Default = 0,

    // DOCS: View Image created from swapchain images
    VsaImageViewType_SwapChained,
} VsaImageViewType;

typedef struct VsaImageViewSettings
{
    VsaImageViewType Type;
    VsaImageCreateExtSettings ImageCreateExtSettings;
    VkFormat Format;
    VkImageAspectFlags ImageAspectFlags;
    i32 MipLevels;
} VsaImageViewSettings;

typedef struct VsaImageView
{
    VsaImageViewType Type;
    VkImage Image;
    VkImageView View;
    VkDeviceMemory Memory;
} VsaImageView;

typedef enum VsaBufferType
{
    VsaBufferType_Static = 0,
    VsaBufferType_Dynamic,
} VsaBufferType;

typedef struct VsaBuffer
{
    VsaBufferType Type;
    VkBuffer Staging;
    VkBuffer Gpu;
    VkDeviceMemory StagingMemory;
    VkDeviceMemory GpuMemory;
} VsaBuffer;


typedef struct VsaTextureImageSettings
{
    const char* Path;
    i32 MipLevels;
    i32 Height;
    i32 Width;
    i32 Channels;
    void* Data;
} VsaTextureImageSettings;

typedef struct VsaTexture
{
    /* DOCS(typedef): AssetManager Ind */
    i64 Id;
    VkImage Image;
    VkDeviceMemory ImageMemory;
    VkImageView ImageView;
    VkFormat Format;
    VkSampler Sampler;
    i32 MipLevels;
    char* Name;
} VsaTexture;

/*
  DOCS(typedef): 0 - no texture, all other number is valid texture Id
*/
typedef i64 VsaTextureId;

/*

  DOCS(typedef): SHADERS PART BEGIN

*/
typedef struct VsaShaderPaths
{
    const char* VertexPath;
    const char* FragmentPath;
    const char* GeometryPath;
    const char* ComputePath;
    const char* TesselationControlPath;
    const char* TesselationEvaluationPath;
} VsaShaderPaths;

typedef enum VsaShaderType
{
    VsaShaderType_Vertex = 0,
    VsaShaderType_Fragment,
    VsaShaderType_Geometry,
    VsaShaderType_Compute,
    VsaShaderType_TesselationControl,
    VsaShaderType_TesselationEvaluation,
    VsaShaderType_Count,
} VsaShaderType;

typedef struct VsaShaderGroupSetting
{
    const char* aPaths[VsaShaderType_Count];
} VsaShaderGroupSetting;

typedef struct VsaShaderBytecode
{
    void* ByteCode;
    size_t Size;
} VsaShaderBytecode;

typedef struct VsaShaderGroup
{
    VsaShaderBytecode saByteCodes[VsaShaderType_Count];
} VsaShaderGroup;


typedef enum VsaShaderCompilationResult
{
    VsaShaderCompilationResult_FileNotExist = 0,
    VsaShaderCompilationResult_AlreadyCompiled,
    VsaShaderCompilationResult_CompilationError,
    VsaShaderCompilationResult_InvalidStage,
    VsaShaderCompilationResult_InternalError,
    VsaShaderCompilationResult_NullResultObject,
    VsaShaderCompilationResult_ValidationError,
    VsaShaderCompilationResult_TransformationError,
    VsaShaderCompilationResult_ConfigurationError,
    VsaShaderCompilationResult_Successed
} VsaShaderCompilationResult;

typedef struct VsaShaderCompiledBytecode
{
    u32* Bytecode;
    size_t Size;
} VsaShaderCompiledBytecode;

typedef struct VsaShaderOutput
{
    VsaShaderCompiledBytecode Vertex;
    VsaShaderCompiledBytecode Fragment;
    VsaShaderCompiledBytecode Geometry;
    VsaShaderCompiledBytecode Compute;
    VsaShaderCompiledBytecode TesselationControl;
    VsaShaderCompiledBytecode TesselationEvaluation;
} VsaShaderOutput;

typedef struct VsaShaderAttribute
{
    VkFormat Format;
    i32 Offset;
    i32 GroupBinding;
} VsaShaderAttribute;

typedef struct VsaShaderDescriptor
{
    i8 ShouldBeUpdated;
    char* Name;
    VkDescriptorType Type;
    i32 Binding;
    i64 Count;
    size_t Size;
} VsaShaderDescriptor;

typedef struct VsaShaderUniform
{
    char* Name;
    i8 IsUpdatedByFlag;
    i8 ShouldBeUpdated;
    i32 Binding;
    i32 Count;
    size_t ElementSize;
    size_t Size;
    VkBuffer Buffer;
    VkDeviceMemory DeviceMemory;
    void* MappedMemory;
    size_t Offset;
} VsaShaderUniform;

typedef struct VsaShaderStorageBuffer
{
    i32 Binding;
    i32 Count;
    char* Name;
    size_t ElementSize;
    size_t Size;
    size_t Offset;
    void* MappedMemory;
    VkBuffer Buffer;
    VkDeviceMemory DeviceMemory;
} VsaShaderStorageBuffer;

typedef struct VsaShaderStage
{
    VsaShaderType Type;
    const char* ShaderSourcePath;
    VsaShaderDescriptor Descriptors[15];
    i32 DescriptorsCount;
} VsaShaderStage;

typedef struct VsaFont
{
    Font Font;
    VsaTexture Atlas;
} VsaFont;

typedef struct VsaPushConstant
{
    // VK_SHADER_STAGE_VERTEX_BIT
    VkShaderStageFlags ShaderStage;
} VsaPushConstant;

typedef struct VsaShaderBindingsCore
{
    //i32 TextureDescriptorsCount;
    VsaShaderDescriptor* Descriptors;
    VsaShaderUniform* Uniforms;
    VsaShaderStorageBuffer* Ssbos;
    //VsaPushConstant* PushContants;
    VkShaderStageFlags PushConstantStageFlag; //VK_SHADER_STAGE_VERTEX_BIT
    VkDescriptorSetLayoutBinding* Bindings; // not used
    VkDescriptorSet DescriptorSet;
    VkDescriptorSetLayout DescriptorSetLayout;
    VkDescriptorPool DescriptorPool;

    // DOCS(typedef): Texture related
    i32 MaxTexturesCount;
    VkDescriptorImageInfo* ImageInfos;
    VkDescriptorImageInfo* SamplerInfos;

} VsaShaderBindingsCore;

/*
  DOCS(typedef): SHADERS PART END
*/

/*###########################
  DOCS(typedef): Vsa Pipeline
  ###########################*/

typedef enum VsaPipelineType
{
    VsaPipelineType_Batched = 0,
    VsaPipelineType_Indirect,
    VsaPipelineType_Raw_NotIndexed,


    VsaPipelineType_Count
} VsaPipelineType;

typedef struct VsaPipelineDescription
{
    VsaPipelineType Type;
    // DOCS: Used RenderPass index
    i32 RenderPassIndex;

    // DOCS: Render pipeline description
    i32 Stride;
    i32 StagesCount;
    i32 AttributesCount;
    VkPolygonMode PolygonMode;
    VsaShaderStage* Stages;
    VsaShaderAttribute* Attributes;

    // DOCS: Some easy flags, maybe become structs in the future
    i8 EnableDepthStencil;
    i8 EnableBackFaceCulling;

    // DOCS: Push constant related
    size_t PustConstantSize;
    VkShaderStageFlags PushConstantStageFlag; // VK_SHADER_STAGE_VERTEX_BIT

    // DOCS: Some shader constants
    i64 MaxInstanceCount;

    // DOCS: Memory Stuff
    size_t ArenaDefaultSize;
} VsaPipelineDescription;

typedef struct VsaUnifomData
{
    void* Data;
    size_t Size;
} VsaUniformData;

typedef struct VsaPushConstantData
{
    void* Data;
    size_t Size;
} VsaPushConstantData;

typedef enum VsaDrawCallFlags
{
    VsaDrawCallFlags_None = 0,
    VsaDrawCallFlags_HaveScissors = 2 << 0,
    VsaDrawCallFlags_Count
} VsaDrawCallFlags;

typedef struct VsaDrawCall
{
    VsaDrawCallFlags Flags;
    v2 ScissorsMin;
    v2 ScissorsMax;

    /*
      DOCS(typedef): Vertex/Index buffers related
    */
    u64 VerticesOffset;
    u64 VerticesSize; // DOCS(typedef): Needed for pipeline_add_d*_c*() func

    u64 IndicesOffset;
    u64 IndicesCount;

    u64 VerticesCount;

    /*
      DOCS(typedef): Contains one/multiple push constant data for frame
    */
    VsaPushConstantData PushConstantData;

} VsaDrawCall;

typedef struct VsaIndirectCmd
{
    VsaBuffer IndirectBuffer;
    size_t InstancesCount;
} VsaIndirectCmd;

typedef struct VsaPipeline
{
    VsaPipelineType Type;
    i32 RenderPassIndex;

    i8 IsPipelineRenderable;
    i8 Priority;

    char* pName;
    size_t ArenaDefaultSize;
    SimpleArena** Arenas;

    VkPipeline Pipeline;
    VkPipelineLayout PipelineLayout;
    //VsaShaderDescriptor* Descriptors;
    VsaShaderBindingsCore BindingCore;

    u64 VerticesOffset;
    u64 IndicesOffset;
    u32 CurrentVertexId;
    u32 CurrentIndexId;
    // TODO: Remove it, we have Base.VertexSize
    i32 Stride;

    // TODO: Move to somewhere else
    struct { i32 IsSet; v2 Position; v2 Offset; } Scissor;
    // NOTE: For batched pipeline type
    VsaDrawCall* DrawCalls;

#if 1
    VsaBuffer Vertex;
    VsaBuffer Index;
#else
    VsaBuffer* VertexBuffers;
    VsaBuffer* IndexBuffers;
#endif

    /* Indirect Part */
    VsaIndirectCmd IndirectCmd;
    VsaBuffer IndirectBuffer;

    VkCommandBuffer CmdBuffer;
} VsaPipeline;


typedef struct VsaDrawIndirectIndexed
{
    VkCommandBuffer CommandBuffer;
    VkBuffer Buffer;
    VkDeviceSize Offset;
    uint32_t DrawCount;
    uint32_t Stride;
} VsaDrawIndirectIndexed;

/*###########################
  DOCS(typedef): Render Pass
  ###########################*/

typedef enum VsaAttachmentType
{
    VsaAttachmentType_Default = 0,
    VsaAttachmentType_ResolveMultisample,
    VsaAttachmentType_Depth,
    VsaAttachmentType_SwapChained,
} VsaAttachmentType;

typedef struct VsaAttachment
{
    VsaAttachmentType Type;
    VkAttachmentDescription Description;
    VkAttachmentReference Reference;
} VsaAttachment;

typedef enum VsaRenderPassType
{

    // DOCS: Prior, Previous Render Pass (Not final)
    VsaRenderPassType_Prior = 0,

    /*
      DOCS: RenderPass that use SwapChain images as image views for framebuffer.
      The main Render Target, something for rendering to the screen.
      Needs to be last render pass

      RenderScenePass (Prior) -> PostProccessPass(Prior) -> SwapChainedPass
    */
    VsaRenderPassType_SwapChained,

} VsaRenderPassType;

typedef struct VsaRenderPassSettings
{
    VsaRenderPassType Type;
    VkSubpassDependency Dependency;
    size_t AttachmentsCount;
    VsaAttachment Attachments[16];
} VsaRenderPassSettings;

typedef struct VsaRenderPass
{
    i32 IsDisabled;
    VkRenderPass RenderPass;
    VsaRenderPassType Type;
    VsaAttachment* aAttachments;

    //DOCS: CanBeUpdated: aFramebuffers, aVsaImageViews, aSwapChainedImageViews
    VkFramebuffer* aFramebuffers;
    VsaImageView* aVsaImageViews;

    VsaPipeline** apPipelines;
} VsaRenderPass;

#endif // VULKAN_SIMPLE_API_TYPES_H
