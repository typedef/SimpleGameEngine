#ifndef VULKAN_SIMPLE_API_H
#define VULKAN_SIMPLE_API_H

#include <Core/Types.h>
#include <Graphics/SimpleWindow.h>
#include "VulkanSimpleApiTypes.h"

/*########################
  DOCS(typedef): VsaBuffer
  ########################*/
VsaBuffer vsa_buffer_new(VsaBufferType type, void* data, size_t size, VkBufferUsageFlagBits usage);
void vsa_buffer_set_data(VsaBuffer* pVsaBuffer, void* data, size_t offset, size_t size);
void vsa_buffer_destroy(VsaBuffer vsaBuffer);

/*########################
  DOCS(typedef): VsaShader
  ########################*/
VkShaderStageFlagBits vsa_shader_type_to_stage_flag(VsaShaderType type);
const char* vsa_shader_type_to_string(VsaShaderType type);
void vsa_shader_bindings_destroy(VsaShaderBindingsCore vsaShaderBindingsCore);
void vsa_shader_output_free(VsaShaderOutput vsaOut);

/*
  #####################################
      DOCS(typedef): VsaFont
  #####################################
*/
VsaFont vsa_font_info_create(const char* path, i32 size, i32 isSaveBitmap);
void vsa_font_info_destroy(VsaFont vsaFontInfo);


/*#######################
  DOCS(typedef): Pipeline
  #######################*/

// NOTE: Should call it in order
SimpleArena* vsa_draw_call_get_arena(VsaPipeline* pVsaPipeline, size_t size);
void vsa_draw_call_add_push_constant(VsaPipeline* pVsaPipeline, VsaDrawCall* pVsaDrawCall, void* data, size_t size);
void vsa_draw_call_bind_push_constant(VsaPipeline* pVsaPipeline, VsaDrawCall vsaDrawCall);

VsaPipeline vsa_pipeline_create(VsaPipelineDescription descriptions);
void vsa_pipeline_set_buffers(VsaPipeline* vsaPipeline, VsaBuffer vertex, VsaBuffer index);
void vsa_pipeline_bind_texture(VsaPipeline* pVsaPipeline, VsaTexture vsaTexture, i32 binding);
void vsa_pipeline_bind_textures(VsaPipeline* vsaPipeline, VsaTexture* textures, i32 count, i32 maxCount);
void vsa_pipeline_update_uniforms(VsaPipeline* pVsaPipeline);
void vsa_pipeline_update_descriptors(VsaPipeline* pVsaPipeline);
VsaDrawCall* vsa_pipeline_add_draw_call(VsaPipeline* pVsaPipeline, VsaDrawCall vsaDrawCall);
void vsa_pipeline_draw_calls_reset(VsaPipeline* pVsaPipeline);
void vsa_pipeline_reset(VsaPipeline* pVsaPipeline);
VsaShaderUniform* vsa_pipeline_get_uniform(VsaPipeline* pVsaPipeline, char* name);
void vsa_pipeline_destroy(VsaPipeline* pVsaPipeline);

/*#######################
  DOCS(typedef): Textures
  #######################*/
VsaTexture vsa_texture_newf(const char* path);
VsaTexture vsa_texture_new(const char* path, void* data, i32 w, i32 h);
i32 vsa_texture_is_valid(VsaTexture vsaTexture);
void vsa_texture_destroy(VsaTexture vsaTexture);

/*#######################
  DOCS(typedef): Uniforms
  #######################*/
VsaShaderUniform vsa_uniform_create(size_t size, size_t elementSize, i32 binding, char* name);
void vsa_uniform_reset(VsaShaderUniform* pVsaUniform);
void vsa_uniform_update(VsaShaderUniform* pUniform, VsaPipeline* pVsaPipeline);
i32 vsa_uniform_set_data_w_offset(VsaShaderUniform* pVsaUniform, void* data, size_t offset, size_t size);
i32 vsa_uniform_set_data(VsaShaderUniform* pVsaUniform, void* data, size_t size);
void vsa_uniform_destroy(VsaShaderUniform vsaUniform);

/*#######################
  DOCS(typedef): SSBO (Shader Storage Buffer Object)
  #######################*/
VsaShaderStorageBuffer vsa_ssbo_create(size_t size, size_t elementSize, i32 bindingIndex, char* name);
i32 vsa_ssbo_set_data(VsaShaderStorageBuffer* pSsbo, void* data, size_t size);

/*###########################
  DOCS(typedef): Render Pass
  ###########################*/
i64 vsa_register_render_pass(VsaRenderPass vsaRenderPass);
VsaRenderPass* vsa_get_render_pass(i64 renderPassId);
void vsa_render_pass_proccess();
VsaRenderPass vsa_render_pass_new(VsaRenderPassSettings settings);
void vsa_render_pass_destroy(VsaRenderPass* pRenderPass);
void vsa_render_pass_update_swapchained(VsaRenderPass* pRenderPass);
void vsa_render_pass_update_images_and_framebuffers(VsaRenderPass* pRenderPass);

/*#######################
  DOCS(typedef): Vsa Api
  #######################*/
void vsa_core_init(VsaSettings vsaSettings);
void vsa_core_deinit();
void vsa_swapchain_objects_recreate();
void vsa_device_wait_idle();
i32 vsa_get_max_texture_count();
i32 vsa_get_min_uniform_size();
i32 vsa_get_max_uniform_buffer_range();
u32 vsa_get_max_vertex_binding_in_buffer();
size_t vsa_get_valid_uniform_size(size_t itemSize);
void vsa_draw_indirect_indexed_ext(VsaDrawIndirectIndexed cmd);
void vsa_draw_indirect_indexed(VsaDrawIndirectIndexed cmd);
VkExtent2D vsa_get_extent();
void vsa_set_window_resized();

/*##################################
  DOCS(typedef): Frame Api
  ##################################*/
void vsa_frame_start(VkCommandBuffer vkCommandBuffer, i32* pImageIndex);
void vsa_frame_end(VkCommandBuffer vkCommandBuffer, i32* pImageIndex);

/*##################################
  DOCS(typedef): Vsa Api Get Helpers
  ##################################*/
VkFormat vsa_get_surface_format();
VkSampleCountFlagBits vsa_get_samples_count();


#endif // VULKAN_SIMPLE_API_H
