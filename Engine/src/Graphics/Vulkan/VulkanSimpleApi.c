#include "VulkanSimpleApi.h"
#include "Graphics/Vulkan/VulkanSimpleApiTypes.h"
#include <vulkan/vulkan_core.h>

#include <math.h>

#include <Deps/stb_image.h>
#include <Core/SimpleStandardLibrary.h>

#pragma clang diagnostic ignored "-Wswitch"


/*
  #####################################
  ##  DOCS VULKAN SIMPLE API HELPER  ##
  #####################################
*/

/*
  DOCS(typedef): Structs
*/
typedef struct VkExtFunctions
{
    PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessengerEXT;
    PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT;

} VkExtFunctions;

typedef struct VkSwapChainSupportDetails
{
    VkSurfaceCapabilitiesKHR SurfaceCapabilities;
    VkSurfaceFormatKHR* SurfaceFormats;
    VkPresentModeKHR* PresentModes;
} VkSwapChainSupportDetails;

/*
  DOCS(typedef): Global variables|state
*/
static VkExtFunctions VkExts;

#define VsaNoTimeout U64_MAX
#define VsaNoFlags 0

/*
  DOCS(typdef): preprocessor part
*/
#if defined(ENGINE_DEBUG)
#define vkValidHandle(handle)						\
    ({									\
        if (handle == VK_NULL_HANDLE)					\
        {								\
            GERROR("Handle: %s is invalid!\n", #handle);		\
            vassert(handle != VK_NULL_HANDLE && "Invalid handle!");	\
        }								\
                                                                        \
    })
#else
#define vkValidHandle(handle)
#endif

/*
  DOCS(typedef): Functions part
*/

static void
vkPrintLayerProperty(VkLayerProperties layer)
{
    GINFO("[Layer] {\n\t.layerName = %s\n\t.Version = %d.%d.%d \n\t.implementationVersion = %d.%d.%d\n\t.description = %s\n}\n",
          layer.layerName,
          VK_API_VERSION_MAJOR(layer.specVersion),
          VK_API_VERSION_MINOR(layer.specVersion),
          VK_API_VERSION_PATCH(layer.specVersion),
          VK_API_VERSION_MAJOR(layer.implementationVersion),
          VK_API_VERSION_MINOR(layer.implementationVersion),
          VK_API_VERSION_PATCH(layer.implementationVersion),
          layer.description
        );
}

static void
vkPrintLayerProperties(i32 layersCount, VkLayerProperties* layersProperties)
{
    GINFO("Layers Count: %d\n", layersCount);
    for (i32 i = 0; i < layersCount; ++i)
    {
        VkLayerProperties layerProperty = layersProperties[i];
        vkPrintLayerProperty(layerProperty);
    }
}

/*
  NOTE(typedef): mb we'll need this for RenderDoc debugging
*/
static const char* const*
vkGetNeededLayers()
{
    u32 layersCount = 0;
    VkResult enumerateLayersResult =
        vkEnumerateInstanceLayerProperties(&layersCount, NULL);
    if (enumerateLayersResult != VK_SUCCESS)
    {
        GERROR("Can't get instance layer properties\n");
        vassert_break();
    }

    VkLayerProperties* layersProperties = NULL;
    array_reserve(layersProperties, layersCount);
    enumerateLayersResult =
        vkEnumerateInstanceLayerProperties(&layersCount, layersProperties);

    vkPrintLayerProperties(layersCount, layersProperties);

    char** enabledLayers = NULL;
    array_foreach(
        layersProperties,
        if (string_compare(item.layerName, "VK_LAYER_RENDERDOC_Capture"))
        {
            GINFO("Enable layer: %s\n", item.layerName);
            array_push(enabledLayers, item.layerName);
        });

    array_free(layersProperties);

    return (const char* const *)enabledLayers;
}

static const char* const*
vkGetExtensions(i32 isDebugEnabled)
{
    u32 requiredExtsCount;
    char** requiredExts = window_get_required_exts(&requiredExtsCount);
    if (isDebugEnabled)
        array_push(requiredExts, VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

    GINFO("Get exts count: %d\n", requiredExtsCount);

    char* sb = NULL;
    array_foreach(requiredExts, string_builder_appendf(sb, "  %s", item););
    GINFO("[Required Ext] %s\n", sb);
    string_builder_free(sb);

    return (const char* const*) requiredExts;
}

static void
vkCheckExtSupport(const char* const* requiredExts)
{
    //vkEnumerateInstanceLayerProperties
    u32 extsCount;
    vkEnumerateInstanceExtensionProperties(NULL, &extsCount, NULL);
    VkExtensionProperties* exts = NULL;
    array_reserve(exts, extsCount);
    vkEnumerateInstanceExtensionProperties(NULL, &extsCount, exts);
    array_header(exts)->Count = extsCount;

    // array_foreach(exts, GLOG("%s\n", item.extensionName););

    i32 i, count = array_count(requiredExts);
    for (i = 0; i < count; ++i)
    {
        const char* rext = requiredExts[i];
        i32 exist = 0;
        for (i32 e = 0; e < extsCount; ++e)
        {
            VkExtensionProperties ext = exts[e];
            //GLOG("Extension : %s\n", ext.extensionName);
            if (string_compare(rext, ext.extensionName))
            {
                exist = 1;
                break;
            }
        }

        if (!exist)
        {
            GERROR("Extension not supported: %s\n", rext);
            vassert(exist && "Extension not supported!");
        }
    }
}

static const char*
vkResultToString(VkResult result)
{
    switch (result)
    {

    case VK_ERROR_OUT_OF_HOST_MEMORY: return "VK_ERROR_OUT_OF_HOST_MEMORY";
    case VK_ERROR_OUT_OF_DEVICE_MEMORY: return "VK_ERROR_OUT_OF_DEVICE_MEMORY";

    case VK_SUCCESS: return "VK_SUCCESS";
    case VK_NOT_READY: return "VK_NOT_READY";
    case VK_TIMEOUT: return "VK_TIMEOUT";
    case VK_EVENT_SET: return "VK_EVENT_SET";
    case VK_EVENT_RESET: return "VK_EVENT_RESET";
    case VK_INCOMPLETE: return "VK_INCOMPLETE";
    case VK_ERROR_INITIALIZATION_FAILED: return "VK_ERROR_INITIALIZATION_FAILED";

    case VK_ERROR_DEVICE_LOST: return "VK_ERROR_DEVICE_LOST";
    case VK_ERROR_MEMORY_MAP_FAILED: return "VK_ERROR_MEMORY_MAP_FAILED";
    case VK_ERROR_LAYER_NOT_PRESENT: return "VK_ERROR_LAYER_NOT_PRESENT";
    case VK_ERROR_EXTENSION_NOT_PRESENT: return "VK_ERROR_EXTENSION_NOT_PRESENT";
    case VK_ERROR_FEATURE_NOT_PRESENT: return "VK_ERROR_FEATURE_NOT_PRESENT";
    case VK_ERROR_INCOMPATIBLE_DRIVER: return "VK_ERROR_INCOMPATIBLE_DRIVER";
    case VK_ERROR_TOO_MANY_OBJECTS: return "VK_ERROR_TOO_MANY_OBJECTS";
    case VK_ERROR_FORMAT_NOT_SUPPORTED: return "VK_ERROR_FORMAT_NOT_SUPPORTED";
    case VK_ERROR_FRAGMENTED_POOL: return "VK_ERROR_FRAGMENTED_POOL";
    case VK_ERROR_UNKNOWN: return "VK_ERROR_UNKNOWN";
    case VK_ERROR_OUT_OF_POOL_MEMORY: return "VK_ERROR_OUT_OF_POOL_MEMORY";
    case VK_ERROR_INVALID_EXTERNAL_HANDLE: return "VK_ERROR_INVALID_EXTERNAL_HANDLE";

    case VK_ERROR_FRAGMENTATION: return "VK_ERROR_FRAGMENTATION";
    case VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS: return "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS";
    case VK_PIPELINE_COMPILE_REQUIRED: return "VK_PIPELINE_COMPILE_REQUIRED";
    case VK_ERROR_SURFACE_LOST_KHR: return "VK_ERROR_SURFACE_LOST_KHR";
    case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR";
    case VK_SUBOPTIMAL_KHR: return "K_SUBOPTIMAL_KHR";
    case VK_ERROR_OUT_OF_DATE_KHR: return "VK_ERROR_OUT_OF_DATE_KHR";
    case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR";
    case VK_ERROR_VALIDATION_FAILED_EXT: return "VK_ERROR_VALIDATION_FAILED_EXT";
    case VK_ERROR_INVALID_SHADER_NV: return "VK_ERROR_INVALID_SHADER_NV";
    case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT: return "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT";
    case VK_ERROR_NOT_PERMITTED_KHR: return "VK_ERROR_NOT_PERMITTED_KHR";
    case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT: return "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT";

    case VK_THREAD_IDLE_KHR: return "VK_THREAD_IDLE_KHR";
    case VK_THREAD_DONE_KHR: return "VK_THREAD_DONE_KHR";
    case VK_OPERATION_DEFERRED_KHR: return "VK_OPERATION_DEFERRED_KHR";
    case VK_OPERATION_NOT_DEFERRED_KHR: return "VK_OPERATION_NOT_DEFERRED_KHR";
    case VK_ERROR_COMPRESSION_EXHAUSTED_EXT: return "VK_ERROR_COMPRESSION_EXHAUSTED_EXT";

    }

    return "Unknown result! Mb update is required for vkResultToString";
}

static VkBool32
vkDebugMessenger(VkDebugUtilsMessageSeverityFlagBitsEXT           messageSeverity, VkDebugUtilsMessageTypeFlagsEXT                  messageTypes, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData)
{
    if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
    {
        printf(MAGNETA("[VULKAN INFO]"));
    }
    else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
    {
        printf(YELLOW("[VULKAN WARNING]"));
    }
    else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
    {
        printf(RED("[VULKAN ERROR] "));
    }

    printf("%s \n", pCallbackData->pMessage);

    return VK_TRUE;
}

static void
vkPrintError(VkResult result, const char* message)
{
    GERROR("Error aquired: %s, message: %s\n", vkResultToString(result), message);
}

#define vkValidResult(result, message)          \
    ({                                          \
        if (result != VK_SUCCESS)               \
        {                                       \
            vkPrintError(result, message);      \
            vguard(result == VK_SUCCESS);       \
        }                                       \
    })

static VkDebugUtilsMessengerCreateInfoEXT
vkCreateDebugUtilsMessengerCreateInfo()
{
    VkDebugUtilsMessageSeverityFlagsEXT severity = /* VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | */
        VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    VkDebugUtilsMessageTypeFlagsEXT messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

    VkDebugUtilsMessengerCreateInfoEXT debugUtilsCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
        .flags = 0,
        .messageSeverity = severity,
        .messageType = messageType,
        .pfnUserCallback = vkDebugMessenger,
        .pUserData = NULL
    };

    return debugUtilsCreateInfo;
}

static VkDebugUtilsMessengerEXT
vkCreateDebugUtilsMessenger(VkInstance vkInstance, VkDebugUtilsMessengerCreateInfoEXT debugUtilsCreateInfo, VkAllocationCallbacks* alloc)
{
    vassert_not_null(VkExts.vkCreateDebugUtilsMessengerEXT);

    VkDebugUtilsMessengerEXT result;
    VkResult createDebugMessengerResult =
        VkExts.vkCreateDebugUtilsMessengerEXT(vkInstance, &debugUtilsCreateInfo, alloc, &result);
    if (createDebugMessengerResult != VK_SUCCESS)
    {
        vkPrintError(createDebugMessengerResult, "Can't create debug utils");
        vassert_break();
    }

    return result;
}
static void
vkDestroyDebugUtilsMessenger(VkInstance vkInstance, VkDebugUtilsMessengerEXT debugUtils, VkAllocationCallbacks* alloc)
{
    VkExts.vkDestroyDebugUtilsMessengerEXT(vkInstance, debugUtils, alloc);
}

static void
vkLoadExtsFunctions(VkInstance vkInstance)
{
    VkExts.vkCreateDebugUtilsMessengerEXT  =
        (PFN_vkCreateDebugUtilsMessengerEXT)
        vkGetInstanceProcAddr(vkInstance, "vkCreateDebugUtilsMessengerEXT");
    VkExts.vkDestroyDebugUtilsMessengerEXT = (PFN_vkDestroyDebugUtilsMessengerEXT)
        vkGetInstanceProcAddr(vkInstance, "vkDestroyDebugUtilsMessengerEXT");

    vassert(VkExts.vkCreateDebugUtilsMessengerEXT != NULL && "Can't load vkCreateDebugUtilsMessengerEXT");
    vassert(VkExts.vkDestroyDebugUtilsMessengerEXT != NULL && "Can't load vkDestroyDebugUtilsMessengerEXT");
}

static const char*
vkPhysicalDeviceTypeToString(VkPhysicalDeviceType type)
{
    switch (type)
    {
    case VK_PHYSICAL_DEVICE_TYPE_OTHER: return "Other";
    case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: return "Integrated GPU";
    case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: return "GPU";
    case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: return "Virtual GPU";
    case VK_PHYSICAL_DEVICE_TYPE_CPU: return "CPU";
    }

    vassert_break();
    return "";
}

static void
vkPrintPhysicalDeviceProperties(VkPhysicalDeviceProperties props)
{
    GINFO("[Physical Device] { \n\t"
          ".apiVersion = %d.%d.%d ,\n\t"
          ".driverVersion = %d.%d.%d,\n\t"
          ".vendorID = %d,\n\t"
          ".deviceID = %d,\n\t"
          ".deviceType = %s,\n\t"
          ".deviceName = %s\n}\n",

          // api version
          VK_API_VERSION_MAJOR(props.apiVersion),
          VK_API_VERSION_MINOR(props.apiVersion),
          VK_API_VERSION_PATCH(props.apiVersion),

          // driver version
          VK_API_VERSION_MAJOR(props.driverVersion),
          VK_API_VERSION_MINOR(props.driverVersion),
          VK_API_VERSION_PATCH(props.driverVersion),

          props.vendorID,
          props.deviceID,
          vkPhysicalDeviceTypeToString(props.deviceType),
          props.deviceName
        );

}

static char*
vkExtent2DToString(VkExtent2D ext)
{
    char buf[61];
    string_format(buf, "width: %d height: %d", ext.width, ext.height);
    return string(buf);
}

static void
vkPrintSurfaceCapabilities(VkSurfaceCapabilitiesKHR surfaceCapabilities)
{
    SimpleArena* arena = simple_arena_create_and_set(KB(1));

    char* sb = NULL;

    string_builder_appendf(
        sb,
        "\n{\n\t.minImageCount = %d,",
        surfaceCapabilities.minImageCount);
    string_builder_appendf(
        sb,
        "\n\t.maxImageCount = %d [%s],",
        surfaceCapabilities.maxImageCount,
        (surfaceCapabilities.maxImageCount == 0) ? "Unlimited" : "Limited");
    string_builder_appendf(
        sb,
        "\n\t.currentExtent = %s,",
        vkExtent2DToString(surfaceCapabilities.currentExtent));
    string_builder_appendf(
        sb,
        "\n\t.minImageExtent = %s",
        vkExtent2DToString(surfaceCapabilities.minImageExtent));
    string_builder_appendf(
        sb,
        "\n\t.maxImageExtent = %s",
        vkExtent2DToString(surfaceCapabilities.maxImageExtent));
    string_builder_appendf(
        sb,
        "\n\t.maxImageArrayLayers = %d",
        surfaceCapabilities.maxImageArrayLayers);

    GINFO("[VkSurfaceCapabilitiesKHR] %s\n", sb);

    /* VkSurfaceTransformFlagsKHR       supportedTransforms; */
    /* VkSurfaceTransformFlagBitsKHR    currentTransform; */
    /* VkCompositeAlphaFlagsKHR         supportedCompositeAlpha; */
    /* VkImageUsageFlags                supportedUsageFlags; */

    simple_arena_destroy(arena);
}

static void
vkPrintSurfaceCapabilitiesLine(VkSurfaceCapabilitiesKHR surfaceCapabilities)
{
    vkPrintSurfaceCapabilities(surfaceCapabilities);
    printf("\n");
}

static VkSwapChainSupportDetails
vkGetSwapChainSupportDetails(VkPhysicalDevice pDevice, VkSurfaceKHR surface)
{
    VkSwapChainSupportDetails supportDetails = {
        .SurfaceFormats = NULL,
        .PresentModes = NULL
    };

    VkResult getSurfaceCapabilitiesResult =
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
            pDevice,
            surface,
            &supportDetails.SurfaceCapabilities);
    vkPrintSurfaceCapabilitiesLine(supportDetails.SurfaceCapabilities);

    // NOTE(typdef): get surface formats
    VkSurfaceFormatKHR* SurfaceFormats = NULL;
    u32 formatsCount;
    VkResult getSurfaceFormatResult =
        vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice, surface, &formatsCount, NULL);
    array_reserve(supportDetails.SurfaceFormats, formatsCount);
    array_header(supportDetails.SurfaceFormats)->Count = formatsCount;
    getSurfaceFormatResult =
        vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice, surface, &formatsCount, supportDetails.SurfaceFormats);
    vkValidResult(getSurfaceFormatResult, "Failed to get Surface Format Result \n");

    // NOTE(typedef): get surface modes
    u32 surfaceModesCount;
    VkResult getSurfaceModeResult =
        vkGetPhysicalDeviceSurfacePresentModesKHR(
            pDevice,
            surface,
            &surfaceModesCount,
            NULL);
    array_reserve(supportDetails.PresentModes, surfaceModesCount);
    array_header(supportDetails.PresentModes)->Count = surfaceModesCount;
    getSurfaceModeResult =
        vkGetPhysicalDeviceSurfacePresentModesKHR(
            pDevice,
            surface,
            &surfaceModesCount,
            supportDetails.PresentModes);
    vkValidResult(getSurfaceModeResult, "Can;t get Surface Modes!");

    return supportDetails;
}

static void
vkFreeSwapChainSupportDetails(VkSwapChainSupportDetails details)
{
    array_free(details.PresentModes);
    array_free(details.SurfaceFormats);
}

VkImage*
vkGetSwapChainImages(VkDevice device, VkSwapchainKHR swapChain)
{
    VkImage* images = NULL;

    u32 imagesCount;
    VkResult getSwapChainImagesResult = vkGetSwapchainImagesKHR(device, swapChain, &imagesCount, NULL);
    array_reserve(images, imagesCount);
    array_header(images)->Count = imagesCount;
    getSwapChainImagesResult = vkGetSwapchainImagesKHR(device, swapChain, &imagesCount, images);
    vkValidResult(getSwapChainImagesResult, "Can't get swap chain images!");

    return images;
}

f32
vkGetMaxAnisotropy(VkPhysicalDevice vkPhysicalDevice)
{
    VkPhysicalDeviceProperties properties;
    vkGetPhysicalDeviceProperties(vkPhysicalDevice, &properties);
    return properties.limits.maxSamplerAnisotropy;
}

/*
  #########################################
  ##  DOCS END VULKAN SIMPLE API HELPER  ##
  #########################################
*/



/*
  NOTE(typedef): for now vsa (Vulkan Simple Api) have global state object
  maybe it is subject to change in the future, for now i don't have all
  necessary information and knowladge about Vulkan API for taking such serious
  decisions.

  Maybe this vars should be global:
  VkInstance, VkDebugUtilsMessengerExt, VkSurfaceKHR,
  VkPhysicalDevice, VkExtensionProperties, VkDevice

  [CONV]
  struct:
  typedef struct Vsa* {} Vsa*;

  func:
  vsa_{object}_{action}(...);

*/


/*
  DOCS(typedef): Vsa Core Api Objects
*/
static i32 gVsaCoreInitialized = 0;
static VkAllocationCallbacks* gAllocatorCallbacks = NULL;
static VsaSettings gVsaSetting;
static VkInstance gVulkanInstance;
// NOTE(typedef): Optional, only for debugging
static VkDebugUtilsMessengerEXT gVulkanDebugMessanger;
static VkSurfaceKHR gVulkanSurface;
static VkPhysicalDevice gVulkanPhysicalDevice;
static VkPhysicalDeviceLimits gVulkanPhysicalDeviceLimits;
static VkExtensionProperties* gPhysicalDeviceExts;
static VkDevice gVulkanDevice;
static VkQueue gVulkanGraphicsQueue;
static VkQueue gVulkanPresentationQueue;

// NOTE(typedef): Configuring SwapChain part (Infrastructure for presentation)
static VkSwapchainKHR gVulkanSwapChain = VK_NULL_HANDLE;
static VkSwapChainSupportDetails gVulkanSupportDetails;
static VsaQueueFamily gVulkanQueueFamily;
static VkSurfaceFormatKHR gVulkanSurfaceFormat;
static VkExtent2D gVulkanExtent;

// DOCS(typedef): Multisampling Related
static VkSampleCountFlagBits gVulkanSamplesCount = VK_SAMPLE_COUNT_1_BIT;

// DOCS: Main render pass (SwapChained)
static VsaRenderPass gRenderPass;
// NOTE: Just saving all render pass'es to this array var
static VsaRenderPass* aRenderPasses = NULL;

/*DOCS(typedef): Drawing commands*/
static VkCommandPool gVulkanCommandPool;
static VkCommandBuffer gVulkanCommandBuffer;

/*
  DOCS(typedef): Sync primitives for render loop
*/
static VkSemaphore gVulkanImageAvailableSemaphores;
static VkSemaphore gVulkanRenderFinishedSemaphores;
static VkFence gVulkanFrameFences;


VkExtent2D
vsa_get_extent()
{
    return gVulkanExtent;
}

typedef struct VsaSamplersTable
{
    i32 Key;
    VkSampler Value;
} VsaSamplersTable;
VsaSamplersTable* gVsaSamplers = NULL;

static void
vsa_samplers_init()
{
    gVsaSamplers = _table_new(gVsaSamplers, sizeof(VsaSamplersTable), -1);
    for (i32 i = 0; i < table_capacity(gVsaSamplers); ++i)
    {
        gVsaSamplers[i].Value = VK_NULL_HANDLE;
    }
}

static void
vsa_samplers_deinit()
{
    for (i32 i = 0; i < table_capacity(gVsaSamplers); ++i)
    {
        VsaSamplersTable item = gVsaSamplers[i];
        if (item.Value != VK_NULL_HANDLE)
        {
            vkDestroySampler(gVulkanDevice, item.Value, gAllocatorCallbacks);
        }
    }

    table_free(gVsaSamplers);
}

/*
  DOCS(typedef): [END] Vsa Core Api Objects
*/


/*
  DOCS: Forward declaration
*/
VkImageView vsa_image_view_create(VkImage vkImage, VkFormat vkFormat, VkImageAspectFlags vkImageAspectFlags, i32 mipLevels);


/*

  DOCS(typedef): Demo related variables

*/
i32 gIsWindowBeenResized = 0;

VkShaderStageFlagBits
vsa_shader_type_to_stage_flag(VsaShaderType type)
{
    switch (type)
    {

    case VsaShaderType_Vertex  : return VK_SHADER_STAGE_VERTEX_BIT;
    case VsaShaderType_Fragment: return VK_SHADER_STAGE_FRAGMENT_BIT;
    case VsaShaderType_Geometry: return VK_SHADER_STAGE_GEOMETRY_BIT;
    case VsaShaderType_Compute : return VK_SHADER_STAGE_COMPUTE_BIT;
    case VsaShaderType_TesselationControl:
        return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
    case VsaShaderType_TesselationEvaluation:
        return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
    }

    GERROR("Wrong VsaShaderType passed to vsa_shader_type_to_stage\n");
    vguard(0);
    return -1;
}

const char*
vsa_shader_type_to_string(VsaShaderType type)
{
    switch (type)
    {

    case VsaShaderType_Vertex: return "Vertex";
    case VsaShaderType_Fragment: return "Fragment";
    case VsaShaderType_Compute: return "Compute";
    case VsaShaderType_Geometry: return "Geometry";
    case VsaShaderType_TesselationControl: return "TesselationControl";
    case VsaShaderType_TesselationEvaluation: return "TesselationEvaluation";

    }

    vguard(0);
    return NULL;
}

void
vsa_shader_bindings_destroy(VsaShaderBindingsCore bindingCore)
{
    for (i32 i = 0; i < array_count(bindingCore.Uniforms); ++i)
    {
        VsaShaderUniform vsaUniform = bindingCore.Uniforms[i];
        vsa_uniform_destroy(vsaUniform);
    }

    array_free(bindingCore.Uniforms);

    vkDestroyDescriptorSetLayout(
        gVulkanDevice,
        bindingCore.DescriptorSetLayout,
        gAllocatorCallbacks);
    vkDestroyDescriptorPool(
        gVulkanDevice,
        bindingCore.DescriptorPool,
        gAllocatorCallbacks);
}

void
vsa_shader_output_free(VsaShaderOutput vsaOut)
{
    if (vsaOut.Vertex.Bytecode)
        memory_free(vsaOut.Vertex.Bytecode);

    if (vsaOut.Fragment.Bytecode)
        memory_free(vsaOut.Fragment.Bytecode);

    if (vsaOut.Geometry.Bytecode)
        memory_free(vsaOut.Geometry.Bytecode);

    if (vsaOut.Compute.Bytecode)
        memory_free(vsaOut.Compute.Bytecode);

    if (vsaOut.TesselationControl.Bytecode)
        memory_free(vsaOut.TesselationControl.Bytecode);

    if (vsaOut.TesselationEvaluation.Bytecode)
        memory_free(vsaOut.TesselationEvaluation.Bytecode);
}


VsaFont
vsa_font_info_create(const char* path, i32 size, i32 isSaveBitmap)
{
    // todo: refactor
    // 0x555555be34c7 "Resources/Fonts/NotoSans.ttf" [const char *]
    i32 ind = string_index_of(path, '/');
    char* rootDir = string_substring_range(path, 0, ind);

    char* texturePrefixPath = path_combine3(rootDir, "Textures", "Generated_");
    char* nameWoExt = path_get_name_wo_extension(path);
    char* finalTexturePath = string_concat3(texturePrefixPath, nameWoExt, ".png");

    memory_free(rootDir);
    memory_free(texturePrefixPath);
    memory_free(nameWoExt);

    FontInfoSettings fontInfoSet = {
        .Path = path,
        .FontSize = size,
        .IsSaveBitmap = isSaveBitmap,
        .BitmapSavePath = finalTexturePath
    };
    Font font = font_cirilic_create(fontInfoSet);

    VsaFont vsaFontInfo = {
        .Font = font
    };

    return vsaFontInfo;
}

void
vsa_font_info_destroy(VsaFont vsaFontInfo)
{
    font_destroy(vsaFontInfo.Font);
}

VkInstance
vsa_instance_create(VsaSettings* pVsaSettings)
{
    /*
      DOCS(typedef): Create VkInstance
    */
    u32 version;
    VkResult getVersionResult = vkEnumerateInstanceVersion(&version);
    if (getVersionResult != VK_SUCCESS)
    {
        GERROR("Error geting version!\n");
    }

    GINFO("Instance version: %d.%d.%d\n", VK_API_VERSION_MAJOR(version), VK_API_VERSION_MINOR(version), VK_API_VERSION_PATCH(version));

    VkApplicationInfo applicationInfo = {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pNext = NULL,
        .pApplicationName = "Simple Engine",
        .applicationVersion = VK_MAKE_API_VERSION(0, 1, 0, 0),
        .pEngineName = "Simple Engine",
        .engineVersion = VK_MAKE_API_VERSION(0, 1, 0, 0),
        .apiVersion = VK_API_VERSION_1_3
    };

    // NOTE(typedef): Get device exts
    // get rid off validation layers here
    const char* const* requiredExts = vkGetExtensions(pVsaSettings->IsDebugEnabled);
    vkCheckExtSupport(requiredExts);

    // NOTE(typedef): create validation layers
    VkLayerProperties* validationLayers = NULL;
    char** validationLayerNames = NULL;

    VkDebugUtilsMessengerCreateInfoEXT* pDebug = NULL;
    if (pVsaSettings->IsDebugEnabled)
    {
        array_push(validationLayerNames, "VK_LAYER_KHRONOS_validation");
        static VkDebugUtilsMessengerCreateInfoEXT debugUtilsCreateInfo;
        debugUtilsCreateInfo = vkCreateDebugUtilsMessengerCreateInfo();
        pDebug = (VkDebugUtilsMessengerCreateInfoEXT*)&debugUtilsCreateInfo;
    }

    VkInstanceCreateInfo instanceCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pNext = pDebug,
        .flags = 0,
        .pApplicationInfo = &applicationInfo,
        .enabledLayerCount = array_count(validationLayerNames),
        .ppEnabledLayerNames = (const char* const*)validationLayerNames,
        .enabledExtensionCount = array_count(requiredExts),
        .ppEnabledExtensionNames = requiredExts
    };
    VkResult createInstanceResult = vkCreateInstance(&instanceCreateInfo, gAllocatorCallbacks, &gVulkanInstance);
    if (createInstanceResult != VK_SUCCESS)
    {
        GERROR("Can't create instance!\n%s\n",
               vkResultToString(createInstanceResult));
    }

    if (pVsaSettings->IsDebugEnabled)
    {
        vkLoadExtsFunctions(gVulkanInstance);
        gVulkanDebugMessanger =
            vkCreateDebugUtilsMessenger(gVulkanInstance, *pDebug, gAllocatorCallbacks);
    }

    return gVulkanInstance;
}

/*
  DOCS(typedef): this thing is needed for on-screen rendering,
  it's just a connection between window and GPU Renderer.
*/
VkSurfaceKHR
vsa_surface_create()
{
    vkValidHandle(gVulkanInstance);

    VkResult windowCreateSurfaceResult =
        window_create_surface(gVulkanInstance, gAllocatorCallbacks, &gVulkanSurface);
    vkValidResult(windowCreateSurfaceResult, "Can't create vk surface!");

    return gVulkanSurface;
}

VkPhysicalDevice
vsa_physical_device_create()
{
    vkValidHandle(gVulkanInstance);

    /*
      DOCS(typedef):Pick physical device
    */
    u32 physicalDevicesCount;
    VkResult enumeratePhysicalDeviceResult =
        vkEnumeratePhysicalDevices(gVulkanInstance, &physicalDevicesCount, NULL);
    vassert(physicalDevicesCount != 0 && "Can't find any physical device!");

    if (enumeratePhysicalDeviceResult == VK_ERROR_INITIALIZATION_FAILED)
    {
        GERROR("Can't enemerate physical devices, maybe installable client driver (ICD) is not installed on your machine!\n");
        vguard(0);
    }

    VkPhysicalDevice* physicalDevices = NULL;
    array_reserve(physicalDevices, physicalDevicesCount);
    array_header(physicalDevices)->Count = physicalDevicesCount;
    enumeratePhysicalDeviceResult =
        vkEnumeratePhysicalDevices(gVulkanInstance, &physicalDevicesCount, physicalDevices);
    GINFO("Found %d devices\n", physicalDevicesCount);
    if (enumeratePhysicalDeviceResult != VK_SUCCESS)
    {
        vkPrintError(enumeratePhysicalDeviceResult, "Can't enumerate physical devices!\n");
        vassert_break();
    }

    i32 isPicked = 0;
    for (i32 i = 0; i < physicalDevicesCount; ++i)
    {
        VkPhysicalDeviceProperties physicalDeviceProperties;
        VkPhysicalDevice vkPhysicalDevice = physicalDevices[i];
        vkGetPhysicalDeviceProperties(vkPhysicalDevice, &physicalDeviceProperties);

        vkPrintPhysicalDeviceProperties(physicalDeviceProperties);

        VkPhysicalDeviceFeatures deviceFeatures;
        vkGetPhysicalDeviceFeatures(vkPhysicalDevice, &deviceFeatures);

        if ((physicalDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
            && deviceFeatures.samplerAnisotropy
            && deviceFeatures.multiDrawIndirect)
        {
            gVulkanPhysicalDevice = vkPhysicalDevice;
            gVulkanPhysicalDeviceLimits = physicalDeviceProperties.limits;
            GINFO("[Vulkan] maxDescriptorSetSampledImages: %d\n", gVulkanPhysicalDeviceLimits.maxDescriptorSetSampledImages);
            isPicked = 1;
            break;
        }
    }

    vassert(isPicked && "Can't pick physical device, you should have discrete gpu!");

    u32 physicalDeviceExtCount;
    VkResult getPhysicalDeviceExtResult = vkEnumerateDeviceExtensionProperties(gVulkanPhysicalDevice, NULL, &physicalDeviceExtCount, NULL);
    gPhysicalDeviceExts = NULL;
    array_reserve(gPhysicalDeviceExts, physicalDeviceExtCount);
    array_header(gPhysicalDeviceExts)->Count = physicalDeviceExtCount;
    getPhysicalDeviceExtResult = vkEnumerateDeviceExtensionProperties(gVulkanPhysicalDevice, NULL, &physicalDeviceExtCount, gPhysicalDeviceExts);
    if (getPhysicalDeviceExtResult != VK_SUCCESS)
    {
        vkPrintError(getPhysicalDeviceExtResult, "Can't get physical device exts!");
        vassert_break();
    }

    return gVulkanPhysicalDevice;
}

void
vsa_queue_family_create(VkSwapChainSupportDetails* pSupportDetails)
{
    vkValidHandle(gVulkanSurface);
    vkValidHandle(gVulkanPhysicalDevice);

    /*
      DOCS(typedef): Get Queue's
    */
    u32 familyPropertyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(gVulkanPhysicalDevice, &familyPropertyCount, NULL);
    VkQueueFamilyProperties* familyProperties = NULL;
    array_reserve(familyProperties, familyPropertyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(gVulkanPhysicalDevice, &familyPropertyCount, familyProperties);
    GINFO("Families count: %d\n", familyPropertyCount);
    i32 graphicsIndex = -1,
        presentationIndex = -1;
    VkSwapChainSupportDetails vkSwapChainSupportDetails;
    for (i32 i = 0; i < familyPropertyCount; ++i)
    {
        char* flagAsStr = NULL;
        VkQueueFamilyProperties familyProperty = familyProperties[i];

        VkBool32 isSurfaceKhrSupported;
        VkResult getSupportForKhrResult =
            vkGetPhysicalDeviceSurfaceSupportKHR(gVulkanPhysicalDevice, i, gVulkanSurface, &isSurfaceKhrSupported);
        if (getSupportForKhrResult != VK_SUCCESS)
        {
            vkPrintError(getSupportForKhrResult, "Can't get physical device surface khr!");
            vassert_break();
        }

        vkSwapChainSupportDetails =
            vkGetSwapChainSupportDetails(gVulkanPhysicalDevice, gVulkanSurface);
        i8 isSwapChainSupported =
            array_any(vkSwapChainSupportDetails.SurfaceFormats)
            && array_any(vkSwapChainSupportDetails.PresentModes);
        if (!isSwapChainSupported)
            continue;

        if (familyProperty.queueFlags & VK_QUEUE_GRAPHICS_BIT
            && familyProperty.queueFlags & VK_QUEUE_COMPUTE_BIT)
        {
            graphicsIndex = i;
        }

        if (isSurfaceKhrSupported == VK_TRUE)
        {
            presentationIndex = i;
        }

        if (graphicsIndex != -1 && presentationIndex != -1)
        {
            break;
        }
    }

    *pSupportDetails = vkSwapChainSupportDetails;

    /*
      DOCS(typedef): yes, on most gpu's this queue indices is the same
    */
    vassert(graphicsIndex > -1 && "Queue for graphics and compute not found!");
    vassert(presentationIndex > -1 && "Queue for presentation not found!");

    VsaQueueFamily vsaQueueFamily = {
        .GraphicsIndex = graphicsIndex,
        .PresentationIndex = presentationIndex
    };

    gVulkanQueueFamily = vsaQueueFamily;
}

VkDevice
vsa_device_create(VsaQueueFamily vsaQueueFamily)
{
    vkValidHandle(gVulkanInstance);
    vkValidHandle(gVulkanPhysicalDevice);

    /*
      DOCS(typedef): Create (Logical) Device
    */
    const char* deviceExts[] = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };
    for (i32 d = 0; d < ARRAY_COUNT(deviceExts); ++d)
    {
        const char* ext = deviceExts[d];
        i32 index = array_index_of(
            gPhysicalDeviceExts,
            string_compare(item.extensionName, ext));
        if (index == -1)
        {
            GERROR("Can't get ext for device: %s\n", ext);
            vassert_break();
        }
    }

    // TODO(typedef): Check if video card support it
    // TODO(typedef): Move before physical device creation
    VkPhysicalDeviceFeatures physcialDeviceFeatures = {
        /*
          DOCS(typedef): we need special features
        */

        // DOCS(typedef): For antisotropic filtering for textures x8, x16
        .samplerAnisotropy = VK_TRUE,

        // DOCS(typedef): For seting descriptors inside render pass, mb we can avoid this
        //.shaderSampledImageArrayDynamicIndexing = VK_TRUE,

        // DOCS(typedef): For enabling none VK_POLYGON_MODE_FILL fill mode
        .fillModeNonSolid = VK_TRUE,

        // DOCS(typedef): MultiDrawIndirect feature for modern renderer
        .multiDrawIndirect = VK_TRUE
    };

    /*
      NOTE(typedef): If we need more features each struct should contain pNext* to a some feature structure
    */
    /* VkPhysicalDeviceDescriptorIndexingFeatures physicalFeatures2 = { */
    /*	.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES, */
    /*	.descriptorBindingUniformBufferUpdateAfterBind = VK_TRUE, */
    /*	.descriptorBindingSampledImageUpdateAfterBind = VK_TRUE */
    /* }; */

    /*
      DOCS(typedef): Create info for creating Queue's.
      As Queue's created within VkDevice we need make CreateInfo
      structs for them as well, before creating VkDevice.
    */
    f32 queuePriority = 1.0f;
    VkDeviceQueueCreateInfo* queuesCreateInfos = NULL;
    VkDeviceQueueCreateInfo graphicsQueueCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .queueFamilyIndex = vsaQueueFamily.GraphicsIndex,
        .queueCount = 1,
        .pQueuePriorities = &queuePriority
    };
    array_push(queuesCreateInfos, graphicsQueueCreateInfo);
    /*
      DOCS(typedef): If .GraphicsIndex == .PresentationIndex then
      we need to pass it only once, if in app we have differentiation
    */
    if (vsaQueueFamily.GraphicsIndex != vsaQueueFamily.PresentationIndex)
    {
        VkDeviceQueueCreateInfo presentationQueueCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .pNext = NULL,
            .flags = 0,
            .queueFamilyIndex = vsaQueueFamily.PresentationIndex,
            .queueCount = 1,
            .pQueuePriorities = &queuePriority
        };
        array_push(queuesCreateInfos, presentationQueueCreateInfo);
    }

    VkDeviceCreateInfo deviceCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = NULL,//&physicalFeatures2,
        .flags = 0,
        .queueCreateInfoCount = array_count(queuesCreateInfos),
        .pQueueCreateInfos = queuesCreateInfos,
        .enabledExtensionCount = ARRAY_COUNT(deviceExts),
        .ppEnabledExtensionNames = deviceExts,
        .pEnabledFeatures = &physcialDeviceFeatures
    };

    VkResult deviceCreateResult = vkCreateDevice(gVulkanPhysicalDevice, &deviceCreateInfo, gAllocatorCallbacks, &gVulkanDevice);
    if (deviceCreateResult != VK_SUCCESS)
    {
        vkPrintError(deviceCreateResult, "Can't create logical device");
        vassert_break();
    }

    return gVulkanDevice;
}

VkSampleCountFlagBits
vsa_samples_get_max_count()
{
    vkValidHandle(gVulkanPhysicalDevice);

    vguard(VK_SAMPLE_COUNT_1_BIT  == 1  && "W1");
    vguard(VK_SAMPLE_COUNT_2_BIT  == 2  && "W2");
    vguard(VK_SAMPLE_COUNT_4_BIT  == 4  && "W4");
    vguard(VK_SAMPLE_COUNT_8_BIT  == 8  && "W8");
    vguard(VK_SAMPLE_COUNT_16_BIT == 16 && "W16");
    vguard(VK_SAMPLE_COUNT_32_BIT == 32 && "W32");
    vguard(VK_SAMPLE_COUNT_64_BIT == 64 && "W64");

    VkPhysicalDeviceProperties pdp;
    vkGetPhysicalDeviceProperties(gVulkanPhysicalDevice, &pdp);

    VkSampleCountFlags samples =
        pdp.limits.framebufferColorSampleCounts;

    if (samples & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
    if (samples & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
    if (samples & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
    if (samples & VK_SAMPLE_COUNT_8_BIT ) { return VK_SAMPLE_COUNT_8_BIT ; }
    if (samples & VK_SAMPLE_COUNT_4_BIT ) { return VK_SAMPLE_COUNT_4_BIT ; }
    if (samples & VK_SAMPLE_COUNT_2_BIT ) { return VK_SAMPLE_COUNT_2_BIT ; }

    return VK_SAMPLE_COUNT_1_BIT;
}

VkQueue
vsa_graphics_queue_create(i32 queueFamilyIndex)
{
    vkValidHandle(gVulkanDevice);
    vkGetDeviceQueue(gVulkanDevice, queueFamilyIndex, 0, &gVulkanGraphicsQueue);
    vkValidHandle(gVulkanGraphicsQueue);
    return gVulkanGraphicsQueue;
}

VkQueue
vsa_presentation_queue_create(i32 queueFamilyIndex)
{
    vkValidHandle(gVulkanDevice);
    vkGetDeviceQueue(gVulkanDevice, queueFamilyIndex, 0, &gVulkanPresentationQueue);
    vkValidHandle(gVulkanPresentationQueue);
    return gVulkanPresentationQueue;
}

VkSwapchainKHR
vsa_swapchain_create(VsaSwapChainSettings set, VkSwapChainSupportDetails supportDetails, VsaQueueFamily queueFamily)
{
    VkSurfaceFormatKHR pickedSurfaceFormat = supportDetails.SurfaceFormats[0];
    VkPresentModeKHR pickedPresentationMode = supportDetails.PresentModes[0];
    for (i32 f = 0; f < array_count(supportDetails.SurfaceFormats); ++f)
    {
        VkSurfaceFormatKHR surfaceFormat = supportDetails.SurfaceFormats[f];
        if (surfaceFormat.format == set.SurfaceFormat.format && surfaceFormat.colorSpace == set.SurfaceFormat.colorSpace)
        {
            pickedSurfaceFormat = surfaceFormat;
            //GINFO("Format is picked!\n");
            break;
        }
    }

    for (i32 m = 0; m < array_count(supportDetails.PresentModes); ++m)
    {
        VkPresentModeKHR presentMode = supportDetails.PresentModes[m];
        if (presentMode == set.PresentationMode)
        {
            pickedPresentationMode = presentMode;
            //GINFO("Presentation is picked!\n");
            break;
        }
    }

    VkSurfaceCapabilitiesKHR capabilities = supportDetails.SurfaceCapabilities;
    GLOG(RED5("min Image COUNT: %ld""\n"), capabilities.minImageCount);
    i32 imagesInSwapChain = capabilities.maxImageCount;
    if (capabilities.maxImageCount == 0)
    {
        imagesInSwapChain = capabilities.minImageCount + capabilities.minImageCount * 0.7f;
    }

    VkExtent2D min = capabilities.minImageExtent;
    VkExtent2D max = capabilities.maxImageExtent;
    GLOG(RED("Min(%d,%d) Max(%d,%d)") "\n", min.width, min.height, max.width, max.height);
    i32 w, h;
    window_get_framebuffer_size_shared(&w, &h);

#if ENGINE_DEBUG == 1
    // DOCS: Validation layers cache surface data, wo this
    // we ll get error Validation Error: pCreateInfo->imageExtent (1999, 1200), which is outside the bounds returned by vkGetPhysicalDeviceSurfaceCapabilitiesKHR(): currentExtent = (2000,1200), minImageExtent = (2000,1200), maxImageExtent = (2000,1200). (https://vulkan.lunarg.com/doc/view/1.3.268.0/linux/1.3-extensions/vkspec.html#VUID-VkSwapchainCreateInfoKHR-pNext-07781)
    VkResult getSurfaceCapabilitiesResult =
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
            gVulkanPhysicalDevice,
            gVulkanSurface,
            &supportDetails.SurfaceCapabilities);
    vkPrintSurfaceCapabilitiesLine(supportDetails.SurfaceCapabilities);
#endif

    gVulkanExtent = (VkExtent2D) {
        .width = w,
        .height = h,
    };

    gVulkanSurfaceFormat = pickedSurfaceFormat;

    VkSwapchainPresentScalingCreateInfoEXT scalingExt = {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_PRESENT_SCALING_CREATE_INFO_EXT,
        .pNext = NULL,
        /* VkPresentScalingFlagsEXT */
        .scalingBehavior = VK_PRESENT_SCALING_ASPECT_RATIO_STRETCH_BIT_EXT,
        //VK_PRESENT_SCALING_ONE_TO_ONE_BIT_EXT,
        /* VkPresentGravityFlagsEXT */
        .presentGravityX = VK_PRESENT_GRAVITY_CENTERED_BIT_EXT,
        /* VkPresentGravityFlagsEXT */
        .presentGravityY = VK_PRESENT_GRAVITY_CENTERED_BIT_EXT,
    };

    VkSwapchainCreateInfoKHR swapChainCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .pNext = &scalingExt,
        .flags = 0,
        .surface = gVulkanSurface,
        .minImageCount = imagesInSwapChain,
        .imageFormat = pickedSurfaceFormat.format,
        .imageColorSpace = pickedSurfaceFormat.colorSpace,
        .imageExtent = gVulkanExtent,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .preTransform = capabilities.currentTransform,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = pickedPresentationMode,
        .clipped = VK_TRUE,
        .oldSwapchain = gVulkanSwapChain, // <- it's a pointer
    };

    u32 queueIndices[2];
    queueIndices[0] = queueFamily.GraphicsIndex;
    queueIndices[1] = queueFamily.PresentationIndex;
    if (queueFamily.GraphicsIndex != queueFamily.PresentationIndex)
    {
        swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        swapChainCreateInfo.queueFamilyIndexCount = 2;
        swapChainCreateInfo.pQueueFamilyIndices = queueIndices;
    }
    else
    {
        swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    }

    VkSwapchainKHR swapChain = VK_NULL_HANDLE;
    VkResult swapChainCreateResult =
        vkCreateSwapchainKHR(gVulkanDevice, &swapChainCreateInfo, gAllocatorCallbacks, &swapChain);
    vkValidResult(swapChainCreateResult, "Failed Swap Chain creation!");
    vkValidHandle(swapChain);

    //GINFO("Get %d SwapChain images\n", array_count(gVulkanSwapChainImages));

    return swapChain;
}

static VkShaderModule
vsa_shader_module_create_ext(VkDevice device, u32* byteCode, size_t byteCodeSize)
{
    VkShaderModuleCreateInfo shaderModuleCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .codeSize = byteCodeSize,
        .pCode = byteCode
    };

    VkShaderModule shaderModule;
    VkResult createShaderModuleResult =
        vkCreateShaderModule(gVulkanDevice, &shaderModuleCreateInfo,  gAllocatorCallbacks, &shaderModule);
    vkValidResult(createShaderModuleResult, "Can't create shader!");

    return shaderModule;
}

static const char*
vsa_shader_type_to_spv_file_ending(VsaShaderType vsaShaderType)
{
    switch (vsaShaderType)
    {
    case VsaShaderType_Vertex  : return "Vert.spv";
    case VsaShaderType_Fragment: return "Frag.spv";
    case VsaShaderType_Geometry: return "Geom.spv";
    case VsaShaderType_Compute: return "Comp.spv";
    case VsaShaderType_TesselationControl: return "Tesc.spv";
    case VsaShaderType_TesselationEvaluation: return "Tese.spv";
    }

    GERROR("Shader type: %d\n", vsaShaderType);
    vguard(0 && "Wrong VsaShaderType!!!");
    return NULL;
}

static i32
_vsa_shader_is_compilation_needed(const char* path, const char* spvPath)
{
    size_t lastModificationTimeRaw =
        path_get_last_modification_time_raw(path);

    if (path_is_file_exist(spvPath) == 0)
    {
        return 1;
    }

    size_t lastCreationTimeRaw =
        path_get_last_modification_time_raw(spvPath);

    if (lastModificationTimeRaw > lastCreationTimeRaw)
    {
        return 1;
    }

    return 0;
}

const char*
vsa_shader_compilation_result_to_string(VsaShaderCompilationResult result)
{

    switch (result)
    {

    case VsaShaderCompilationResult_FileNotExist:
        return "File no exist";
    case VsaShaderCompilationResult_AlreadyCompiled:
        return "Already compiled";
    case VsaShaderCompilationResult_CompilationError:
        return "Compilation error";
    case VsaShaderCompilationResult_InvalidStage:
        return "Invalid stage";
    case VsaShaderCompilationResult_InternalError:
        return "Internal error";
    case VsaShaderCompilationResult_NullResultObject:
        return "Null result object";
    case VsaShaderCompilationResult_ValidationError:
        return "Validation error";
    case VsaShaderCompilationResult_TransformationError:
        return "Transformation error";
    case VsaShaderCompilationResult_ConfigurationError:
        return "Configuration error";
    case VsaShaderCompilationResult_Successed:
        return "Successed";

    }

    GERROR("Wrong Shader Result %d\n", result);
    vguard(0 && "Wrong Shader Result!");
    return "Null";
}

typedef enum VsaShaderDirectoryType
{
    VsaShaderDirectoryType_Source = 0,
    VsaShaderDirectoryType_Bin,
    VsaShaderDirectoryType_Count,
} VsaShaderDirectoryType;

static const char* gVsaShaderDirs[VsaShaderDirectoryType_Count] = {};

VsaShaderBytecode
vsa_shader_compile_single_unit_new(const char* pPath)
{
    // note: commit with shaderc inside code, not util d735f717589aa0c023c73b5b82946690844f826d

    VsaShaderBytecode byteCode = {};

    const char* binDir = gVsaShaderDirs[VsaShaderDirectoryType_Bin];
    const char* sourceDir = gVsaShaderDirs[VsaShaderDirectoryType_Source];

    char* sourceDirA = path_combine(sourceDir, path_get_name(pPath) /*Vsr3dd.frag*/);

    char* nameWoExt = path_get_name_wo_extension(pPath); // Vsr3dd
    char* ext = string(path_get_extension(pPath) + 1); // frag
    ext[0] = char_to_upper(ext[0]);
    char* ending = string_concat(ext, ".spv"); // Frag.spv
    char* fullNameWoDir = string_concat(nameWoExt, ending);
    char* spvPath = path_combine(binDir, fullNameWoDir); //full path spv


    i32 isRecompilationNeeded =
        _vsa_shader_is_compilation_needed(sourceDir, spvPath);
    if (!isRecompilationNeeded)
    {
        size_t size;
        void* bytes = file_read_bytes_ext(spvPath, &size);
        byteCode = (VsaShaderBytecode) {
            .ByteCode = bytes,
            .Size = size,
        };

        GINFO("Get bytecode from disk.\n");

        goto EndVsaShaderCompileSingleUnitLabel;
    }

    // glslc Vsr3dd.frag -O -o Vsr3ddFrag.spv --target-env=vulkan1.3 -w -x glsl
    // glslc Vsr3dd.vert -O -o Vsr3ddVert.spv --target-env=vulkan1.3 -w -x glsl

    const char* optimization = "";
    i32 isOptimizationNeeded = 1;
    if (isOptimizationNeeded)
    {
        optimization = "-O";
    }

    char cmd[1024] = {};
    string_format(cmd, "glslc %s %s -o %s --target-env=vulkan1.3 -w -x glsl", sourceDirA, optimization, spvPath);
    GINFO("Cmd: %s\n", cmd);
    system(cmd);

    vassert(path_is_file_exist(spvPath) && "Path for spv is not exist!");

    size_t size = 0;
    u8* pBytecode = file_read_bytes_ext(spvPath, &size);
    byteCode = (VsaShaderBytecode) {
        .ByteCode = pBytecode,
        .Size = size,
    };

EndVsaShaderCompileSingleUnitLabel:
    memory_free(sourceDirA);
    memory_free(fullNameWoDir);
    memory_free(nameWoExt);
    memory_free(ext);
    memory_free(ending);
    memory_free(spvPath);

    return byteCode;
}

//todo: here
void
vsa_shader_compile_new(VsaShaderGroupSetting groupSet, VsaShaderGroup* pGroup)
{
    i32 isAllNull = 1;
    for (i32 i = 0; i < VsaShaderType_Count; ++i)
    {
        if (groupSet.aPaths[i] != NULL)
        {
            isAllNull = 0;
            break;
        }
    }

    if (isAllNull)
    {
        GERROR("Compile Settings all paths for shader units is NULL!\n");
        vguard(0 && "Wrong parameters!");
    }

    const char* rootDir = path_get_current_directory();
    gVsaShaderDirs[VsaShaderDirectoryType_Source] = path_combine(rootDir, "Resources/Shaders");
    gVsaShaderDirs[VsaShaderDirectoryType_Bin] = path_combine(rootDir, "Resources/Shaders/Bin");

    for (i32 i = VsaShaderDirectoryType_Source; i < VsaShaderDirectoryType_Count; ++i)
    {
        const char* dir = gVsaShaderDirs[i];
        i32 isDirectoryExist = path_is_directory_exist(dir);
        if (!isDirectoryExist)
        {
            path_directory_create(dir);
        }
    }

    for (i32 i = 0; i < VsaShaderType_Count; ++i)
    {
        VsaShaderType type = (VsaShaderType) i;
        const char* pPath = groupSet.aPaths[type];
        if (pPath == NULL)
            continue;

        VsaShaderBytecode byte = vsa_shader_compile_single_unit_new(pPath);
        pGroup->saByteCodes[type] = byte;
    }
}

VkShaderModule
vsa_shader_module_create(VkDevice vkDevice, const char* path)
{
    size_t byteCodeSize;
    u32* byteCode = (u32*) file_read_bytes_ext(path, &byteCodeSize);
    vassert_not_null(byteCode);
    GINFO("Creating shader for: %s\n", path);

    VkShaderModule shaderModule =
        vsa_shader_module_create_ext(vkDevice, byteCode, byteCodeSize);

    return shaderModule;
}

i32
vsa_find_memory_type_index(u32 typeBit, VkMemoryPropertyFlags vkMemoryPropertyFlags)
{
    VkPhysicalDeviceMemoryProperties dMemoryProperties;
    vkGetPhysicalDeviceMemoryProperties(gVulkanPhysicalDevice, &dMemoryProperties);

    i32 index = -1;
    for (i32 i = 0; i < dMemoryProperties.memoryTypeCount; ++i)
    {
        VkMemoryType memoryType =
            dMemoryProperties.memoryTypes[i];
        if ((typeBit & (1 << i)) && (memoryType.propertyFlags & vkMemoryPropertyFlags))
        {
            return i;
        }
    }

    vguard(0 && "Can't find memory type index!");
    return -1;
}

VkBuffer
vsa_buffer_create(VkBufferUsageFlags usage, VkDeviceSize size, VkMemoryPropertyFlags memoryPropertyFlags, VkDeviceMemory* deviceMemory)
{
    VkBuffer vkBuffer;

    VkBufferCreateInfo vkCreateBufferCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .size = size,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = NULL
    };

    VkResult createBufferResult = vkCreateBuffer(gVulkanDevice, &vkCreateBufferCreateInfo, gAllocatorCallbacks, &vkBuffer);
    vkValidResult(createBufferResult, "Failed Vertex Buffer creation!");

    //NOTE(typedef): Allocating memory for VkBuffer
    VkMemoryRequirements vertexBufferMemoryRequirements;
    vkGetBufferMemoryRequirements(gVulkanDevice, vkBuffer, &vertexBufferMemoryRequirements);

    // NOTE(typedef): Find suitable memory type
    i32 index = vsa_find_memory_type_index(
        vertexBufferMemoryRequirements.memoryTypeBits, memoryPropertyFlags);

    VkMemoryAllocateInfo memoryAllocateInfo = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext = NULL,
        .allocationSize = vertexBufferMemoryRequirements.size,
        .memoryTypeIndex = index
    };

    VkResult allocateMemoryResult = vkAllocateMemory(gVulkanDevice, &memoryAllocateInfo, gAllocatorCallbacks, deviceMemory);
    vkValidResult(allocateMemoryResult, "Failed triangle deivce memory allocation!");

    VkResult bindBufferMemoryResult = vkBindBufferMemory(gVulkanDevice, vkBuffer, *deviceMemory, 0);
    vkValidResult(bindBufferMemoryResult, "Failed binding buffer memory with buffer!");

    return vkBuffer;
}

void
vsa_staging_memory_set_data(VkDeviceMemory deviceMemory, void* dataToCopy, size_t offset, size_t size)
{
    vguard_not_null(deviceMemory);
    vguard_not_null(dataToCopy);
    vguard(size > 0);

    void* data;
    VkResult vkMapResult = vkMapMemory(gVulkanDevice, deviceMemory, offset, size, 0, &data);
    vkValidResult(vkMapResult, "Failed mapping memory!");
    memcpy(data, dataToCopy, size);
    vkUnmapMemory(gVulkanDevice, deviceMemory);
}

VkCommandBuffer
vsa_command_buffer_begin(VkCommandPool vkCommandPool)
{
    vkValidHandle(vkCommandPool);

    VkCommandBufferAllocateInfo vkCmdBufferAllocatedInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = NULL,
        .commandPool = vkCommandPool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };

    VkCommandBuffer vkCmdBuffer;
    VkResult vkAllocateCmdBuffersResult =
        vkAllocateCommandBuffers(gVulkanDevice, &vkCmdBufferAllocatedInfo, &vkCmdBuffer);
    vkValidResult(vkAllocateCmdBuffersResult, "Failed cmd buffer allocation!\n");

    VkCommandBufferBeginInfo beginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = NULL,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = NULL
    };
    vkBeginCommandBuffer(vkCmdBuffer, &beginInfo);

    return vkCmdBuffer;
}

void
vsa_command_buffer_end(VkCommandPool vkCommandPool, VkCommandBuffer vkCommandBuffer)
{
    vkValidHandle(vkCommandPool);
    vkValidHandle(vkCommandBuffer);
    vkValidHandle(gVulkanGraphicsQueue);

    VkResult vkEndCmdBufferResult = vkEndCommandBuffer(vkCommandBuffer);
    vkValidResult(vkEndCmdBufferResult, "Failed end cmd buffer!");

    // Execute command
    VkSubmitInfo submitInfo = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext = NULL,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = NULL,
        .pWaitDstStageMask = NULL,
        .commandBufferCount = 1,
        .pCommandBuffers = &vkCommandBuffer,
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = NULL
    };

    vkQueueSubmit(gVulkanGraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(gVulkanGraphicsQueue);

    // Free command for buffer
    vkFreeCommandBuffers(gVulkanDevice, vkCommandPool, 1, &vkCommandBuffer);
}

void
vsa_buffer_copy(VkBuffer deviceBuffer, VkBuffer cpuStagingBuffer, VkDeviceSize offset, VkDeviceSize size)
{
    VkCommandBuffer cmdBuffer =
        vsa_command_buffer_begin(gVulkanCommandPool);

    VkBufferCopy copyRegion = {
        .srcOffset = offset,
        .dstOffset = offset,
        .size = size,
    };

    vkCmdCopyBuffer(cmdBuffer, cpuStagingBuffer, deviceBuffer, 1, &copyRegion);

    vsa_command_buffer_end(gVulkanCommandPool, cmdBuffer);
}

VkBuffer
vsa_full_buffer_create_ext(VkBufferUsageFlags usage, size_t size, VkDeviceMemory* deviceMemory, VkBuffer* stagingBuffer, VkDeviceMemory* stagingMemory)
{
    *stagingBuffer =
        vsa_buffer_create(VK_BUFFER_USAGE_TRANSFER_SRC_BIT, size, (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT), stagingMemory);

    VkBuffer gpuBuffer =
        vsa_buffer_create(
            (VK_BUFFER_USAGE_TRANSFER_DST_BIT | usage), size, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, deviceMemory);

    return gpuBuffer;
}

VkBuffer
vsa_static_buffer_create(VkBufferUsageFlags usage, void* data, size_t offset, size_t size, VkDeviceMemory* deviceMemory)
{
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;

    VkBuffer gpuBuffer =
        vsa_full_buffer_create_ext(
            usage,
            size,
            deviceMemory,
            &stagingBuffer,
            &stagingMemory);

    vsa_staging_memory_set_data(stagingMemory, data, offset, size);
    vsa_buffer_copy(gpuBuffer, stagingBuffer, offset, size);

    // NOTE(typedef): clean up
    vkDestroyBuffer(gVulkanDevice, stagingBuffer, gAllocatorCallbacks);
    vkFreeMemory(gVulkanDevice, stagingMemory, gAllocatorCallbacks);

    return gpuBuffer;
}

VsaBuffer
vsa_buffer_new(VsaBufferType type, void* data, size_t size, VkBufferUsageFlagBits usage)
{

    VsaBuffer vsaBuffer = {
        .Type = type
    };

    if (type == VsaBufferType_Static)
    {

        vsaBuffer.Gpu =
            vsa_full_buffer_create_ext(
                usage,//VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                size,
                &vsaBuffer.GpuMemory,
                &vsaBuffer.Staging,
                &vsaBuffer.StagingMemory);
        vsa_buffer_set_data(&vsaBuffer, data, 0, size);
        vkDestroyBuffer(gVulkanDevice, vsaBuffer.Staging, gAllocatorCallbacks);
        vkFreeMemory(gVulkanDevice, vsaBuffer.StagingMemory, gAllocatorCallbacks);
    }
    else
    {
        vguard_null(data);
        vguard(size > 0);
        vsaBuffer.Gpu =
            vsa_full_buffer_create_ext(
                usage,//VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                size,
                &vsaBuffer.GpuMemory,
                &vsaBuffer.Staging,
                &vsaBuffer.StagingMemory);

    }

    return vsaBuffer;
}

void
vsa_buffer_set_data(VsaBuffer* pVsaBuffer, void* data, size_t offset, size_t size)
{
    vsa_staging_memory_set_data(pVsaBuffer->StagingMemory, data, offset, size);
    vsa_buffer_copy(pVsaBuffer->Gpu, pVsaBuffer->Staging, offset, size);
}

void
vsa_buffer_destroy(VsaBuffer vsaBuffer)
{
    if (vsaBuffer.Type == VsaBufferType_Static)
    {
        vkDestroyBuffer(gVulkanDevice, vsaBuffer.Gpu, gAllocatorCallbacks);
        vkFreeMemory(gVulkanDevice, vsaBuffer.GpuMemory, gAllocatorCallbacks);
    }
    else
    {
        vkDestroyBuffer(gVulkanDevice, vsaBuffer.Staging, gAllocatorCallbacks);
        vkFreeMemory(gVulkanDevice, vsaBuffer.StagingMemory, gAllocatorCallbacks);
        vkDestroyBuffer(gVulkanDevice, vsaBuffer.Gpu, gAllocatorCallbacks);
        vkFreeMemory(gVulkanDevice, vsaBuffer.GpuMemory, gAllocatorCallbacks);
    }

}

void
vsa_image_create(VsaImageCreateExtSettings settings, VkImage* vkImage, VkDeviceMemory* vkDeviceMemory)
{
    vkValidHandle(gVulkanDevice);

    VkImageCreateInfo vkImageCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = settings.Format,
        .extent = {
            .width = settings.Width,
            .height = settings.Height,
            .depth = settings.Depth
        },
        .mipLevels = settings.MipLevels,
        .arrayLayers = 1,
        .samples = settings.SamplesCount,
        .tiling = settings.ImageTiling,
        .usage = settings.ImageUsageFlags,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0, // NOTE(): should we set this value
        .pQueueFamilyIndices = NULL,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };

    VkResult vkCreateImageResult = vkCreateImage(
        gVulkanDevice, &vkImageCreateInfo, gAllocatorCallbacks, vkImage);
    vkValidResult(vkCreateImageResult, "Failed image/texture creation!");

    VkImage tempVkImage = *vkImage;
    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(gVulkanDevice, tempVkImage, &memRequirements);

    i32 index = vsa_find_memory_type_index(
        memRequirements.memoryTypeBits, settings.MemoryPropertyFlags);
    VkMemoryAllocateInfo allocateInfo = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext = NULL,
        .allocationSize = memRequirements.size,
        .memoryTypeIndex = index
    };

    VkResult vkAllocateMemoryResult =
        vkAllocateMemory(gVulkanDevice, &allocateInfo, gAllocatorCallbacks, vkDeviceMemory);
    vkValidResult(vkAllocateMemoryResult, "Failed texture memory allocation!");

    VkResult vkBindImageMemoryResult =
        vkBindImageMemory(gVulkanDevice, tempVkImage, *vkDeviceMemory, 0);
    vkValidResult(vkBindImageMemoryResult, "Failed bind texture memory!");
}

void
vsa_image_change_layout(VkImage vkImage, VkImageLayout vkOldLayout, VkImageLayout vkNewLayout, VkFormat vkFormat, i32 mipLevels)
{
    VkCommandBuffer vkCommandBuffer =
        vsa_command_buffer_begin(gVulkanCommandPool);

    VkImageMemoryBarrier vkImageMemoryBarrier = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = NULL,
        .srcAccessMask = 0, // TODO(typedef): empty for now
        .dstAccessMask = 0, // TODO(typedef): empty for now
        .oldLayout = vkOldLayout,
        .newLayout = vkNewLayout,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = vkImage,
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = mipLevels,
            .baseArrayLayer = 0,
            .layerCount = 1,
        }
    };

    VkPipelineStageFlags vkSrcPipelineStageFlags;
    VkPipelineStageFlags vkDstPipelineStageFlags;
    if (vkOldLayout == VK_IMAGE_LAYOUT_UNDEFINED
        && vkNewLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        vkImageMemoryBarrier.srcAccessMask = 0;
        vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        vkSrcPipelineStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        vkDstPipelineStageFlags = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if (vkOldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
             && vkNewLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        vkSrcPipelineStageFlags = VK_PIPELINE_STAGE_TRANSFER_BIT;
        vkDstPipelineStageFlags = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else
    {
        vguard(0 && "Wrong new||old layout picked !!!");
    }

    vkCmdPipelineBarrier(vkCommandBuffer,
                         vkSrcPipelineStageFlags,
                         vkDstPipelineStageFlags,
                         VK_DEPENDENCY_BY_REGION_BIT,
                         0,NULL, 0,NULL,
                         1,
                         &vkImageMemoryBarrier
        );

    vsa_command_buffer_end(gVulkanCommandPool, vkCommandBuffer);
}

void
vsa_image_create_mipmaps(VkImage vkImage, i32 width, i32 height, i32 mipLevels)
{
    VkFormatProperties formatProperties;
    vkGetPhysicalDeviceFormatProperties(gVulkanPhysicalDevice, VK_FORMAT_R8G8B8A8_SRGB, &formatProperties);
    vguard(
        formatProperties.optimalTilingFeatures
        & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT
        && "Linear filtering not supported!");

    VkCommandBuffer vkCommandBuffer =
        vsa_command_buffer_begin(gVulkanCommandPool);

    VkImageMemoryBarrier vkImageMemoryBarrier = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = NULL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = vkImage,
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        }
    };

    i32 mipWidth = width;
    i32 mipHeight = height;
    for (i32 i = 1; i < mipLevels; ++i)
    {
        i32 dstWidth, dstHeight;
        if (mipWidth > 1)
        {
            dstWidth = mipWidth / 2;
        }
        else
        {
            dstWidth = 1;
        }

        if (mipHeight > 1)
        {
            dstHeight = mipHeight / 2;
        }
        else
        {
            dstHeight = 1;
        }

        vkImageMemoryBarrier.subresourceRange.baseMipLevel = i - 1;
        vkImageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        vkImageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

        vkCmdPipelineBarrier(
            vkCommandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0, NULL,
            0, NULL,
            1, &vkImageMemoryBarrier);

        VkImageBlit vkImageBlit = {
            .srcSubresource = {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .mipLevel = i - 1,
                .baseArrayLayer = 0,
                .layerCount = 1
            },
            .srcOffsets = {
                [0] = { 0, 0, 0 },
                [1] = { mipWidth, mipHeight, 1 },
            },
            .dstSubresource = {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .mipLevel = i,
                .baseArrayLayer = 0,
                .layerCount = 1
            },
            .dstOffsets = {
                [0] = { 0, 0, 0 },
                [1] = { dstWidth, dstHeight, 1 }
            }
        };

        vkCmdBlitImage(
            vkCommandBuffer,
            vkImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            vkImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1, &vkImageBlit,
            VK_FILTER_LINEAR);

        vkImageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        vkImageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
        vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

        vkCmdPipelineBarrier(
            vkCommandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            0,
            0, NULL,
            0, NULL,
            1, &vkImageMemoryBarrier);

        if (dstWidth > 1)
            mipWidth = dstWidth;
        if (dstHeight > 1)
            mipHeight = dstHeight;
    }

    vkImageMemoryBarrier.subresourceRange.baseMipLevel = mipLevels - 1;
    vkImageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    vkImageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    vkCmdPipelineBarrier(
        vkCommandBuffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        0,
        0, NULL,
        0, NULL,
        1, &vkImageMemoryBarrier);

    vsa_command_buffer_end(gVulkanCommandPool, vkCommandBuffer);
}

void
vsa_buffer_to_image(VkBuffer vkBuffer, VkImage vkImage, i32 width, i32 height)
{
    VkCommandBuffer vkCommandBuffer =
        vsa_command_buffer_begin(gVulkanCommandPool);

    VkBufferImageCopy vkBufferImageCopy = {
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1
        },
        .imageOffset = {
            .x = 0,
            .y = 0,
            .z = 0
        },
        .imageExtent = {
            .width = width,
            .height = height,
            .depth = 1
        }
    };

    vkCmdCopyBufferToImage(
        vkCommandBuffer, vkBuffer, vkImage,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &vkBufferImageCopy);

    vsa_command_buffer_end(gVulkanCommandPool, vkCommandBuffer);
}

VkImageView
vsa_image_view_create(VkImage vkImage, VkFormat vkFormat, VkImageAspectFlags vkImageAspectFlags, i32 mipLevels)
{
    VkImageView vkImageView;

    VkImageViewCreateInfo imageViewCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .image = vkImage,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = vkFormat,
        .components = {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY
        },
        .subresourceRange = {
            .aspectMask = vkImageAspectFlags,//VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = mipLevels,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };

    VkResult createImageViewResult =
        vkCreateImageView(
            gVulkanDevice, &imageViewCreateInfo,
            gAllocatorCallbacks, &vkImageView);
    vkValidResult(createImageViewResult, "Can't create vk image view!");

    return vkImageView;
}

VsaImageView
vsa_image_view_new(VsaImageViewSettings settings)
{
    VsaImageView vsaImageView = {
        .Type = settings.Type
    };

    vsa_image_create(settings.ImageCreateExtSettings, &vsaImageView.Image, &vsaImageView.Memory);

    vsaImageView.View =
        vsa_image_view_create(vsaImageView.Image,
                              settings.Format,
                              settings.ImageAspectFlags,
                              settings.MipLevels);

    return vsaImageView;
}

void
_vsa_texture_image_create_ext(VsaTextureImageSettings settings, VkImage* vkImage, VkDeviceMemory* vkDeviceMemory, VkFormat vkFormat)
{
    size_t imageSize = settings.Width * settings.Height * settings.Channels;
    VkDeviceMemory stagingBufferMemory;
    VkBuffer stagingBuffer =
        vsa_buffer_create(
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            imageSize,
            (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
            &stagingBufferMemory);

    void* bufferData;
    vkMapMemory(gVulkanDevice, stagingBufferMemory, 0, imageSize, 0, &bufferData);
    memcpy(bufferData, settings.Data, imageSize);
    vkUnmapMemory(gVulkanDevice, stagingBufferMemory);

    VsaImageCreateExtSettings vsaImageSettings = {
        .Width = settings.Width,
        .Height = settings.Height,
        .Depth = 1,
        .MipLevels = settings.MipLevels,
        .SamplesCount = VK_SAMPLE_COUNT_1_BIT,
        .Format = vkFormat,
        .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
        .ImageUsageFlags = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
    };
    vsa_image_create(vsaImageSettings, vkImage, vkDeviceMemory);

    /*
      NOTE(typedef): Make this thing faster with *_lite() function wo cmd buffers
    */
    VkImage tempImage = *vkImage;
    vsa_image_change_layout(tempImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, vkFormat, settings.MipLevels);
    vsa_buffer_to_image(stagingBuffer, tempImage, settings.Width, settings.Height);

#if DO_NOT_USE_MIP_LEVELS
    //DOCS(typedef): imageBlit cmd will transfer image to SHADER format
    // NOTE(typedef): This is for using image in shader
    vsa_image_change_layout(tempImage,
                            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                            vkFormat,
                            1);
#else
    vsa_image_create_mipmaps(tempImage, settings.Width, settings.Height, settings.MipLevels);
#endif

    vkDestroyBuffer(gVulkanDevice, stagingBuffer, gAllocatorCallbacks);
    vkFreeMemory(gVulkanDevice, stagingBufferMemory, gAllocatorCallbacks);
}

/*
  NOTE/TODO(typedef): We need instanced AssetManager->LoadTexture
  for loading texture
*/

void
_vsa_texture_image_create(const char* path, void* data, i32 w, i32 h, VkImage* vkImage, VkDeviceMemory* vkDeviceMemory, VkFormat* vkFormat, i32* mipLevels)
{
    vkValidHandle(gVulkanDevice);
    vguard_not_null(path);
    vguard(w > 0);
    vguard(h > 0);

    i32 minDim = Min(w, h);
    f32 logValue = log2f(minDim);
    i32 mipLevelsValue = Min(i32(logValue) + 1, 10);

    *vkFormat = VK_FORMAT_R8G8B8A8_SRGB;

    VsaTextureImageSettings settings = {
        .Path = path,
        .MipLevels = mipLevelsValue,
        .Height = h,
        .Width = w,
        .Channels = 4,
        .Data = data
    };
    _vsa_texture_image_create_ext(settings, vkImage, vkDeviceMemory, *vkFormat);

    *mipLevels = mipLevelsValue;
}

VkSampler
vsa_texture_sampler_create(i32 mipLevels)
{
    VkFilter magFilter = VK_FILTER_LINEAR; //VK_FILTER_NEAREST
    VkFilter minFilter = VK_FILTER_LINEAR;
    VkBool32 isAnisotropyFilterEnabled = VK_TRUE;
    f32 maxAnisotropy = vkGetMaxAnisotropy(gVulkanPhysicalDevice);
    //GINFO("Max anisatropy: %f\n", vkGetMaxAnisotropy(gVulkanPhysicalDevice));
    VkBool32 compareEnabled = VK_FALSE; // NOTE(typedef): used for PCF in shadow map technique
    VkCompareOp compareOp = VK_COMPARE_OP_ALWAYS; // NOTE(typedef): used for PCF in shadow map technique

    VkSamplerCreateInfo vkSamplerCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .magFilter = magFilter,
        .minFilter = minFilter,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .mipLodBias = 0.0f,
        .anisotropyEnable = isAnisotropyFilterEnabled,
        .maxAnisotropy = maxAnisotropy,
        .compareEnable = compareEnabled,
        .compareOp = compareOp,
        .minLod = 0.0f, //NOTE(typedef): Multisample trick (f32) (mipLevels)/8,
        .maxLod = (f32) mipLevels,
        .borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
        .unnormalizedCoordinates = VK_FALSE
    };

    VkSampler vkSampler;
    VkResult vkCreateSamplerResult =
        vkCreateSampler(gVulkanDevice, &vkSamplerCreateInfo, gAllocatorCallbacks, &vkSampler);

    return vkSampler;
}

VkSampler
vsa_texture_get_sampler(i32 mipLevels)
{
    i64 index = hash_geti(gVsaSamplers, mipLevels);
    VkSampler vkSampler;
    if (index == -1)
    {
        vkSampler = vsa_texture_sampler_create(mipLevels);
        hash_put(gVsaSamplers, mipLevels, vkSampler);
        index = table_index(gVsaSamplers);
        vguard(index > -1 && "gVsaSamplers error!");
    }

    vkSampler = gVsaSamplers[index].Value;

    return vkSampler;
}

void
vsa_texture_image_destroy(VkImage vkImage, VkImageView vkImageView, VkDeviceMemory vkDeviceMemory)
{
    vkDestroyImage(gVulkanDevice, vkImage, gAllocatorCallbacks);
    vkFreeMemory(gVulkanDevice, vkDeviceMemory, gAllocatorCallbacks);
    vkDestroyImageView(gVulkanDevice, vkImageView, gAllocatorCallbacks);
}

void
vsa_texture_image_destroy_w_sampler(VkImage vkImage, VkImageView vkImageView, VkDeviceMemory vkDeviceMemory, VkSampler vkSampler)
{
    vsa_texture_image_destroy(vkImage, vkImageView, vkDeviceMemory);
    vkDestroySampler(gVulkanDevice, vkSampler, gAllocatorCallbacks);
}

VsaTexture
vsa_texture_newf(const char* path)
{
    i32 w, h, c, d = 4;
    void* pData = stbi_load(path, &w, &h, &c, d);
    vassert_not_null(pData);
    VsaTexture vsaTexture = vsa_texture_new(path, pData, w, h);
    return vsaTexture;
}

VsaTexture
vsa_texture_new(const char* path, void* data, i32 w, i32 h)
{
    const char* pathName = path_get_name(path);

    VsaTexture vsaTexture = {
        .Name = string(pathName),
        .Id = -1
    };

    _vsa_texture_image_create(path, data, w, h,
                              &vsaTexture.Image,
                              &vsaTexture.ImageMemory,
                              &vsaTexture.Format,
                              &vsaTexture.MipLevels);

    vsaTexture.ImageView = vsa_image_view_create(
        vsaTexture.Image,
        vsaTexture.Format,
        VK_IMAGE_ASPECT_COLOR_BIT,
        vsaTexture.MipLevels);
    vsaTexture.Sampler = vsa_texture_get_sampler(vsaTexture.MipLevels);

    return vsaTexture;
}

i32
vsa_texture_is_valid(VsaTexture vsaTexture)
{
    if (vsaTexture.Image != NULL
        && vsaTexture.ImageMemory != NULL
        && vsaTexture.ImageView != NULL
        && vsaTexture.Sampler != NULL
        && vsaTexture.Id != -1)
    {
        return 1;
    }

    return 0;
}

void
vsa_texture_destroy(VsaTexture vsaTexture)
{
    if (!vsa_texture_is_valid(vsaTexture))
    {
        GWARNING("Not value texture!\n");
        return;
    }

    vsa_texture_image_destroy(vsaTexture.Image, vsaTexture.ImageView, vsaTexture.ImageMemory);

    memory_free(vsaTexture.Name);
}

VsaShaderBindingsCore
vsa_shader_bindings_create(VsaShaderStage* stages, i32 stagesCount, VsaShaderGroupSetting* pGroupSet, VkShaderStageFlags pushConstantStageFlag)
{
    vkValidHandle(gVulkanDevice);

    VsaShaderBindingsCore bindingCore = {
        .PushConstantStageFlag = pushConstantStageFlag,
    };

    /*
      DOCS(typedef): Descriptors creation
    */
    VkDescriptorSetLayoutBinding* bindings = NULL;
    VkDescriptorPoolSize* poolSizes = NULL;

    i32 bindingIndex = 0;
    for (i32 i = 0; i < stagesCount; ++i)
    {
        VsaShaderStage vsaShaderStage = stages[i];
        VkShaderStageFlagBits stageFlag =
            vsa_shader_type_to_stage_flag(vsaShaderStage.Type);

        for (i32 d = 0; d < vsaShaderStage.DescriptorsCount; ++d)
        {
            VsaShaderDescriptor descriptor = vsaShaderStage.Descriptors[d];
            descriptor.Binding = bindingIndex;
            GINFO("%s: %d\n", descriptor.Name, descriptor.Binding);

            i32 descrCount = 0;
            switch (descriptor.Type)
            {

            case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
            case VK_DESCRIPTOR_TYPE_SAMPLER:
                array_push(bindingCore.Descriptors, descriptor);
                bindingCore.MaxTexturesCount =
                    Max(bindingCore.MaxTexturesCount, descriptor.Count);

                descrCount = descriptor.Count;
                break;

            case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
            {
                VsaShaderUniform uniform =
                    vsa_uniform_create(descriptor.Count * descriptor.Size, descriptor.Size, bindingIndex, descriptor.Name);
                array_push(bindingCore.Uniforms, uniform);

                descrCount = 1;
                break;
            }

            case VK_DESCRIPTOR_TYPE_STORAGE_BUFFER:
            {
                VsaShaderStorageBuffer ssbo =
                    vsa_ssbo_create(descriptor.Count * descriptor.Size, descriptor.Size, bindingIndex, descriptor.Name);
                array_push(bindingCore.Ssbos, ssbo);

                descrCount = 1;
                break;
            }

            }

            VkDescriptorSetLayoutBinding binding = {
                .binding = bindingIndex,
                .descriptorType = descriptor.Type,
                .descriptorCount = descrCount,
                .stageFlags = stageFlag,
                .pImmutableSamplers = NULL
            };

            VkDescriptorPoolSize poolSize = {
                .type = descriptor.Type,
                .descriptorCount = descriptor.Count
            };

            array_push(bindings, binding);
            array_push(poolSizes, poolSize);

            ++bindingIndex;
        }
    }

    VkDescriptorSetLayoutCreateInfo descriptorCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext = NULL,
        .flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT,//0,
        .bindingCount = array_count(bindings),
        .pBindings = bindings
    };

    VkDescriptorSetLayout vkDescriptorSetLayout;
    VkResult creteDescriptorSetLayout =
        vkCreateDescriptorSetLayout(gVulkanDevice, &descriptorCreateInfo, gAllocatorCallbacks, &vkDescriptorSetLayout);
    vkValidResult(creteDescriptorSetLayout, "Failed descriptor set layout creation!");

    VkDescriptorPoolCreateInfo vkPoolCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext = NULL,
        .flags = VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT,
        .maxSets = 1,
        .poolSizeCount = array_count(poolSizes),
        .pPoolSizes = poolSizes,
    };

    VkDescriptorPool vkDescriptorPool;
    VkResult descriptorPoolCreateResult =
        vkCreateDescriptorPool(gVulkanDevice, &vkPoolCreateInfo, gAllocatorCallbacks, &vkDescriptorPool);

    vkValidResult(descriptorPoolCreateResult, "Failed Descriptor Pool Creation!!!");

    VkDescriptorSet vkDescriptorSet;
    VkDescriptorSetAllocateInfo vkDescriptorSetAllocateInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext = NULL,
        .descriptorPool = vkDescriptorPool,
        .descriptorSetCount = 1,
        .pSetLayouts = &vkDescriptorSetLayout,
    };

    VkResult allocateDescritorSetsResult =
        vkAllocateDescriptorSets(gVulkanDevice, &vkDescriptorSetAllocateInfo, &vkDescriptorSet);
    vkValidResult(allocateDescritorSetsResult, "Failed allocate descriptor sets!");

    array_free(bindings);
    array_free(poolSizes);

    for (i32 i = 0; i < stagesCount; ++i)
    {
        VsaShaderStage stage = stages[i];
        pGroupSet->aPaths[stage.Type] = stage.ShaderSourcePath;
    }

    bindingCore.DescriptorSet = vkDescriptorSet;
    bindingCore.DescriptorSetLayout = vkDescriptorSetLayout;
    bindingCore.DescriptorPool = vkDescriptorPool;

    return bindingCore;
}

VkFramebuffer
_vsa_render_pass_get_current_framebuffer(VsaRenderPass* pRenderPass, i32 imageIndex)
{
    if (pRenderPass->Type == VsaRenderPassType_SwapChained)
        return pRenderPass->aFramebuffers[imageIndex];

    // TODO/BUG: Can cause a bug later
    return pRenderPass->aFramebuffers[0];
}

void
_vsa_render_pass_proccess(VsaRenderPass* pRenderPass, i32 imageIndex)
{
    VsaPipeline** pPipelines = pRenderPass->apPipelines;
    i64 pipelinesCount = array_count(pPipelines);

    //TODO(typedef): set in pipeline
    VkClearValue clearValues[2] = {
        [0] = (VkClearValue) {
            .color = { 0.0034f, 0.0037f, 0.0039f, 1 }
            //.color = { 0.0934f, 0.0937f, 0.0939f, 1 }
        },
        [1] = (VkClearValue) {
            .depthStencil = { 1.0f, 0.0f }
        }
    };

    VkFramebuffer currentFramebuffer = _vsa_render_pass_get_current_framebuffer(pRenderPass, imageIndex);

    VkRenderPassBeginInfo renderPassBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .pNext = NULL,
        .renderPass = pRenderPass->RenderPass,
        .framebuffer = currentFramebuffer,
        .renderArea = {
            .offset = {	0, 0 },
            .extent = gVulkanExtent
        },
        .clearValueCount = 2,
        .pClearValues = clearValues
    };
    vkCmdBeginRenderPass(gVulkanCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

    //TODO(typedef): set in pipeline
    VkViewport viewport = {
        .x = 0,
        .y = 0,
        .width = gVulkanExtent.width,
        .height = gVulkanExtent.height,
        .minDepth = 0,
        .maxDepth = 1
    };
    vkCmdSetViewport(gVulkanCommandBuffer, 0, 1, &viewport);

    //TODO(typedef): set in pipeline
    VkRect2D scissor = {
        .offset = { 0, 0 },
        .extent = gVulkanExtent
    };
    vkCmdSetScissor(gVulkanCommandBuffer, 0, 1, &scissor);

    DO_ONES(array_foreach(pPipelines, GERROR("Pipeline: %s\n", item->pName)));

    for (i32 i = 0; i < pipelinesCount; ++i)
    {
        VsaPipeline* pVsaPipeline = pPipelines[i];

        if (!pVsaPipeline->IsPipelineRenderable)
        {
            //DO_MANY_TIME(GWARNING("Not rendere pipeline!\n"), 5);
            continue;
        }

        switch (pVsaPipeline->Type)
        {

        case VsaPipelineType_Batched:
        {
            i32 isPushConstantExist =
                (pVsaPipeline->BindingCore.PushConstantStageFlag > 0);
            /* vsa_pipeline_update_uniforms(pVsaPipeline); */
            /* vsa_pipeline_update_descriptors(pVsaPipeline); */

            vkCmdBindPipeline(gVulkanCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pVsaPipeline->Pipeline);
            vkCmdBindDescriptorSets(gVulkanCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pVsaPipeline->PipelineLayout, 0, 1, &pVsaPipeline->BindingCore.DescriptorSet, 0, NULL);

            i32 drawCallsCount = array_count(pVsaPipeline->DrawCalls);
            //GINFO("DrawCalls Count: %d\n", drawCallsCount);
            for (i32 d = 0; d < drawCallsCount; ++d)
            {
                VsaDrawCall vsaDrawCall = pVsaPipeline->DrawCalls[d];

                if (isPushConstantExist)
                    vkCmdPushConstants(
                        pVsaPipeline->CmdBuffer,
                        pVsaPipeline->PipelineLayout,
                        pVsaPipeline->BindingCore.PushConstantStageFlag,
                        0,
                        vsaDrawCall.PushConstantData.Size,
                        vsaDrawCall.PushConstantData.Data);

                VkDeviceSize offsets[1] = { vsaDrawCall.VerticesOffset };
                vkCmdBindVertexBuffers(gVulkanCommandBuffer, 0, 1, &pVsaPipeline->Vertex.Gpu, offsets);
                vkCmdBindIndexBuffer(gVulkanCommandBuffer, pVsaPipeline->Index.Gpu, vsaDrawCall.IndicesOffset, VK_INDEX_TYPE_UINT32);

                vkCmdDrawIndexed(gVulkanCommandBuffer, vsaDrawCall.IndicesCount, 1, 0, 0, 0);

                if (vsaDrawCall.Flags == VsaDrawCallFlags_HaveScissors)
                {
                    vkCmdSetScissor(gVulkanCommandBuffer, 0, 1, &scissor);
                }
            }

            break;
        }

        case VsaPipelineType_Indirect:
        {
            vkCmdBindPipeline(gVulkanCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pVsaPipeline->Pipeline);
            vkCmdBindDescriptorSets(gVulkanCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pVsaPipeline->PipelineLayout, 0, 1, &pVsaPipeline->BindingCore.DescriptorSet, 0, NULL);
            VkDeviceSize offsets[1] = { 0 };
            vkCmdBindVertexBuffers(gVulkanCommandBuffer, 0, 1, &pVsaPipeline->Vertex.Gpu, offsets);
            vkCmdBindIndexBuffer(gVulkanCommandBuffer, pVsaPipeline->Index.Gpu, 0, VK_INDEX_TYPE_UINT32);

            if (pVsaPipeline->Scissor.IsSet)
            {
                v2 offset = pVsaPipeline->Scissor.Offset;
                v2 position = pVsaPipeline->Scissor.Position;
                VkRect2D scissor = {
                    .offset = { offset.X, offset.Y },
                    .extent = { position.X, position.Y },
                };

                vkCmdSetScissor(gVulkanCommandBuffer, 0, 1, &scissor);
            }

            VsaIndirectCmd vsaIndirectCmd = pVsaPipeline->IndirectCmd;
            vkCmdDrawIndexedIndirect(gVulkanCommandBuffer,
                                     vsaIndirectCmd.IndirectBuffer.Gpu,
                                     0,
                                     vsaIndirectCmd.InstancesCount,
                                     sizeof(VkDrawIndexedIndirectCommand));
            break;
        }

        case VsaPipelineType_Raw_NotIndexed:
        {
            i32 isPushConstantExist = (pVsaPipeline->BindingCore.PushConstantStageFlag > 0);

            vkCmdBindPipeline(gVulkanCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pVsaPipeline->Pipeline);
            vkCmdBindDescriptorSets(gVulkanCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pVsaPipeline->PipelineLayout, 0, 1, &pVsaPipeline->BindingCore.DescriptorSet, 0, NULL);

            i64 drawCallsCount = array_count(pVsaPipeline->DrawCalls);
            for (i64 d = 0; d < drawCallsCount; ++d)
            {
                VsaDrawCall vsaDrawCall = pVsaPipeline->DrawCalls[d];

                if (isPushConstantExist)
                    vkCmdPushConstants(
                        pVsaPipeline->CmdBuffer,
                        pVsaPipeline->PipelineLayout,
                        pVsaPipeline->BindingCore.PushConstantStageFlag,
                        0,
                        vsaDrawCall.PushConstantData.Size,
                        vsaDrawCall.PushConstantData.Data);

                VkDeviceSize offsets[1] = { vsaDrawCall.VerticesOffset };
                vkCmdBindVertexBuffers(gVulkanCommandBuffer, 0, 1, &pVsaPipeline->Vertex.Gpu, offsets);
                vkCmdBindIndexBuffer(gVulkanCommandBuffer, pVsaPipeline->Index.Gpu, vsaDrawCall.IndicesOffset, VK_INDEX_TYPE_UINT32);

                vkCmdDraw(gVulkanCommandBuffer, vsaDrawCall.VerticesCount, 1, 0, 0);

                if (vsaDrawCall.Flags == VsaDrawCallFlags_HaveScissors)
                {
                    vkCmdSetScissor(gVulkanCommandBuffer, 0, 1, &scissor);
                }
            }

            break;
        }

        }

    }

    vkCmdEndRenderPass(gVulkanCommandBuffer);

}

VkPipelineShaderStageCreateInfo
vsa_shader_type_to_stage_create_info(VsaShaderType type, VkShaderModule vkShaderModule)
{
    VkShaderStageFlagBits flag = vsa_shader_type_to_stage_flag(type);

    VkPipelineShaderStageCreateInfo stageCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .stage = flag,
        .module = vkShaderModule,
        .pName = "main",
        /* NOTE(typedef): use this thing if we need constants in shader */
        .pSpecializationInfo = NULL
    };

    return stageCreateInfo;
}

SimpleArena*
vsa_draw_call_get_arena(VsaPipeline* pVsaPipeline, size_t size)
{
    // DOCS(typedef): Search for appropriate arena, get index from it
    i32 i, ind = -1, count = array_count(pVsaPipeline->Arenas);
    for (i = 0; i < count; ++i)
    {
        SimpleArena* pArena = pVsaPipeline->Arenas[i];
        if ((pArena->Offset + size) < pArena->Size)
        {
            ind = i;
            break;
        }
    }

    // DOCS(typedef): If arena not found, create new
    if (ind == -1)
    {
        ind = count;
        SimpleArena* pArena = simple_arena_create(pVsaPipeline->ArenaDefaultSize);
        array_push(pVsaPipeline->Arenas, pArena);
    }

    SimpleArena* currArena = pVsaPipeline->Arenas[ind];

    return currArena;
}

void
vsa_draw_call_add_push_constant(VsaPipeline* pVsaPipeline, VsaDrawCall* pVsaDrawCall, void* data, size_t size)
{
    SimpleArena* currArena = vsa_draw_call_get_arena(pVsaPipeline, size);
    memory_set_arena(currArena);

    void* aData = memory_allocate(size);
    memcpy(aData, data, size);

    pVsaDrawCall->PushConstantData = (VsaPushConstantData) {
        .Data = aData,
        .Size = size
    };

    memory_set_arena(NULL);
}

VsaPipeline
vsa_pipeline_create(VsaPipelineDescription descriptions)
{
    VkPipeline vkPipeline;
    VkPipelineLayout vkPipelineLayout;
    VsaShaderPaths paths = {};
    VsaPipeline vsaPipeline = {
        .Type = descriptions.Type,
        .RenderPassIndex = descriptions.RenderPassIndex,

        .ArenaDefaultSize = descriptions.ArenaDefaultSize == 0 ? MB(1) : descriptions.ArenaDefaultSize,
    };

    vsaPipeline.Stride = descriptions.Stride;

    //DOCS: Validate
    i64 renderPassCnt = array_count(aRenderPasses);
    vguard((vsaPipeline.RenderPassIndex < renderPassCnt) && (vsaPipeline.RenderPassIndex >= 0) && "RenderPass index if wrong !!!");

    VsaRenderPass* pVsaRenderPass = &aRenderPasses[vsaPipeline.RenderPassIndex];
    //array_push(pVsaRenderPass->aPipelines, vsaPipeline);
    VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT;
    array_foreach(pVsaRenderPass->aAttachments,
                  if (samples < item.Description.samples)
                  {
                      samples = item.Description.samples;
                  });
//descriptorBindingSampledImageUpdateAfter
    if (descriptions.Type == VsaPipelineType_Indirect)
    {
        VkBufferUsageFlagBits usageBits =
            VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT
            | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        size_t size = descriptions.MaxInstanceCount * sizeof(VkDrawIndexedIndirectCommand);
        vsaPipeline.IndirectCmd.IndirectBuffer = vsa_buffer_new(VsaBufferType_Dynamic, NULL, size, usageBits);
    }

    // NOTE(typedef): mb place bindings && poolSizes inside bot loop??
    VsaShaderGroupSetting groupSet = {};
    vsaPipeline.BindingCore = vsa_shader_bindings_create(descriptions.Stages, descriptions.StagesCount, &groupSet, descriptions.PushConstantStageFlag);

    VsaShaderOutput vsaShaderOutput = {};
    VsaShaderGroup shaderGroup = {};
    //vsa_shader_compile(paths, &vsaShaderOutput);
    vsa_shader_compile_new(groupSet, &shaderGroup);

    VkPipelineShaderStageCreateInfo* stages = NULL;
    for (i32 i = 0; i < descriptions.StagesCount; ++i)
    {
        VsaShaderStage vsaStage = descriptions.Stages[i];

        vassert(vsaStage.Type >= 0 && vsaStage.Type <= VsaShaderType_Count && "Error: VsaStage.Type is wrong!");

        VsaShaderBytecode byteCode = {};
        byteCode = shaderGroup.saByteCodes[vsaStage.Type];

        vguard_not_null(byteCode.ByteCode);
        vguard(byteCode.Size > 0);

        VkShaderModule vkShaderModule =
            vsa_shader_module_create_ext(gVulkanDevice, byteCode.ByteCode, byteCode.Size);

        VkPipelineShaderStageCreateInfo stage =
            vsa_shader_type_to_stage_create_info(
                vsaStage.Type,
                vkShaderModule);
        array_push(stages, stage);
    }

    // NOTE(typedef): Vertex Buffer description
    VkVertexInputBindingDescription vkVertexInputBindingDescription = {
        .binding = 0,
        .stride = descriptions.Stride,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX
    };

    VkVertexInputAttributeDescription* attributeDescrs = NULL;
    for (i32 i = 0; i < descriptions.AttributesCount; ++i)
    {
        VsaShaderAttribute vsaAttribute = descriptions.Attributes[i];

        VkVertexInputAttributeDescription descr = {
            .location = i,
            .binding = vsaAttribute.GroupBinding,
            .format = vsaAttribute.Format,
            .offset = vsaAttribute.Offset
        };

        array_push(attributeDescrs, descr);
    }

    VkPipelineVertexInputStateCreateInfo pipelineVertexInputCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &vkVertexInputBindingDescription,
        .vertexAttributeDescriptionCount = array_count(attributeDescrs),
        .pVertexAttributeDescriptions = attributeDescrs
    };

    VkDynamicState dynamicStates[2] = {
        [0] = VK_DYNAMIC_STATE_VIEWPORT,
        [1] = VK_DYNAMIC_STATE_SCISSOR
    };

    VkPipelineDynamicStateCreateInfo pipelineDynamicStateCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .dynamicStateCount = ARRAY_COUNT(dynamicStates),
        .pDynamicStates = dynamicStates,
    };

    VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE
    };

    // DOCS(typedef): passing viewport, scissor here make them immutable
    VkPipelineViewportStateCreateInfo pipelineViewportCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .viewportCount = 1,
        .scissorCount = 1
    };

    VkPipelineRasterizationStateCreateInfo pipelineRasterizationCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        // NOTE(typedef): enabling that requires gpu feature
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        // NOTE(typedef): requires enabling gpu feature
        .polygonMode = descriptions.PolygonMode,
        .cullMode =
#if 0 // NOTE(): best option
        VK_CULL_MODE_NONE,
#else // DOCS: Using this for now
        VK_CULL_MODE_BACK_BIT,
#endif

        .frontFace =
#if 1 // NOTE(): best option
        VK_FRONT_FACE_COUNTER_CLOCKWISE,
#else
        VK_FRONT_FACE_CLOCKWISE,
#endif
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0.0f,
        .depthBiasClamp = 0.0f,
        .depthBiasSlopeFactor = 0.0f,
        // NOTE(typedef): anything other then 1.0f requires enabling GPU feature
        .lineWidth = 1.0f
    };

    if (!descriptions.EnableBackFaceCulling)
    {
        pipelineRasterizationCreateInfo.cullMode = VK_CULL_MODE_NONE;
    }
    else{
        vassert_break();
    }

    VkPipelineMultisampleStateCreateInfo pipelineMultisampleCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .rasterizationSamples = samples,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 1.0f,
        .pSampleMask = NULL,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE
    };

    // NOTE(typedef): Depth Stencil code creation here
    VkPipelineDepthStencilStateCreateInfo depthStencilCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
        .front = 0.0f,
        .back = 0.0f,
        .minDepthBounds = -1.0f,
        .maxDepthBounds =  1.0f
    };

    if (!descriptions.EnableDepthStencil)
    {
        depthStencilCreateInfo = (VkPipelineDepthStencilStateCreateInfo) {};
        vguard(0 && "Wtf, you foget .EnableDepthStencil = 1 !!!");
    }

    // NOTE(typedef): Blending not configured now
    VkPipelineColorBlendAttachmentState pipelineColorBlendAttachment = {
        .blendEnable = VK_TRUE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp = VK_BLEND_OP_ADD,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
    };

    VkPipelineColorBlendStateCreateInfo pipelineColorBlendCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &pipelineColorBlendAttachment,
        .blendConstants = { 0, 0, 0, 0 }
    };

    VkPushConstantRange vkPushConstantRange = {};
    u32 pushConstantCount = 0;
    if (descriptions.PushConstantStageFlag != 0)
    {
        vkPushConstantRange = (VkPushConstantRange) {
            .stageFlags = descriptions.PushConstantStageFlag,
            .offset = 0,
            .size = descriptions.PustConstantSize
        };
        pushConstantCount = 1;
    }

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .setLayoutCount = 1,
        .pSetLayouts = &vsaPipeline.BindingCore.DescriptorSetLayout,
        .pushConstantRangeCount = pushConstantCount,
        .pPushConstantRanges = &vkPushConstantRange
    };

    VkResult createPipelineLayoutResult =
        vkCreatePipelineLayout(gVulkanDevice, &pipelineLayoutCreateInfo, gAllocatorCallbacks, &vkPipelineLayout);
    vkValidResult(createPipelineLayoutResult, "Can't create pipeline layout!");

    VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .stageCount = array_count(stages),
        .pStages = stages,
        .pVertexInputState = &pipelineVertexInputCreateInfo,
        .pInputAssemblyState = &pipelineInputAssemblyCreateInfo,
        .pTessellationState = NULL,
        .pViewportState = &pipelineViewportCreateInfo,
        .pRasterizationState = &pipelineRasterizationCreateInfo,
        .pMultisampleState = &pipelineMultisampleCreateInfo,
        .pDepthStencilState = &depthStencilCreateInfo,
        .pColorBlendState = &pipelineColorBlendCreateInfo,
        .pDynamicState = &pipelineDynamicStateCreateInfo,
        .layout = vkPipelineLayout,
        .renderPass = pVsaRenderPass->RenderPass,
        .subpass = 0,
        .basePipelineHandle = VK_NULL_HANDLE,
        .basePipelineIndex = -1
    };

    VkResult createGraphicsPipelinesResult =
        vkCreateGraphicsPipelines(gVulkanDevice, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, gAllocatorCallbacks, &vkPipeline);
    vkValidResult(createGraphicsPipelinesResult, "Can't crete Graphics Pipeline!");
    vsaPipeline.Pipeline = vkPipeline;
    vsaPipeline.PipelineLayout = vkPipelineLayout;

    // DOCS(typedef): Clean up
    array_free(attributeDescrs);
    vsa_shader_output_free(vsaShaderOutput);
    for (i32 i = 0; i < array_count(stages); ++i)
    {
        VkPipelineShaderStageCreateInfo stageCreateInfo = stages[i];
        vkDestroyShaderModule(gVulkanDevice, stageCreateInfo.module, gAllocatorCallbacks);
    }
    array_free(stages);

    return vsaPipeline;
}

void
vsa_pipeline_set_buffers(VsaPipeline* vsaPipeline, VsaBuffer vertex, VsaBuffer index)
{
    vsaPipeline->Vertex = vertex;
    vsaPipeline->Index = index;
    // NOTE(typedef): Not sure about command buffer
    vsaPipeline->CmdBuffer = gVulkanCommandBuffer;
}

void
vsa_pipeline_bind_texture(VsaPipeline* pVsaPipeline, VsaTexture vsaTexture, i32 binding)
{
    // note: we are not using single bind
    vguard(0);

    vguard(vsa_texture_is_valid(vsaTexture));
    vguard_not_null(pVsaPipeline);

    VkDescriptorImageInfo imageInfo = {
        .sampler = VK_NULL_HANDLE,
        .imageView = vsaTexture.ImageView,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    };
    VkDescriptorImageInfo samplerInfo = {
        .sampler = vsaTexture.Sampler
    };

    VkWriteDescriptorSet textureArray = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = NULL,
        .dstSet = pVsaPipeline->BindingCore.DescriptorSet,
        .dstBinding = binding,
        .dstArrayElement = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
        .pImageInfo = &imageInfo,
        .pBufferInfo = NULL,
        .pTexelBufferView = NULL
    };

    VkWriteDescriptorSet samplers = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = NULL,
        .dstSet = pVsaPipeline->BindingCore.DescriptorSet,
        .dstBinding = binding + 1,
        .dstArrayElement = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER,
        .pImageInfo = &samplerInfo,
        .pBufferInfo = NULL,
        .pTexelBufferView = NULL
    };

    VkWriteDescriptorSet sets[] = {
        textureArray,
        samplers
    };

    vkUpdateDescriptorSets(gVulkanDevice, 2, sets, 0, NULL);
}

void
vsa_pipeline_bind_textures(VsaPipeline* pVsaPipeline, VsaTexture* aTextures, i32 count, i32 maxCount)
{
    array_clear(pVsaPipeline->BindingCore.ImageInfos);
    array_clear(pVsaPipeline->BindingCore.SamplerInfos);

    VsaShaderDescriptor* descriptors = pVsaPipeline->BindingCore.Descriptors;
    i64 descrCnt = array_count(descriptors);

    for (i32 d = 0; d < descrCnt; ++d)
    {
        VsaShaderDescriptor* pDescription = &descriptors[d];
        pDescription->ShouldBeUpdated = 1;
    }

    for (i32 i = 0; i < maxCount; ++i)
    {
        VsaTexture texture = (i < count) ? aTextures[i] : aTextures[0];
        vguard(vsa_texture_is_valid(texture));

        VkDescriptorImageInfo imageInfo = {
            .sampler = VK_NULL_HANDLE,
            .imageView = texture.ImageView,
            .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        };
        VkDescriptorImageInfo samplerInfo = {
            .sampler = texture.Sampler
        };

        array_push(pVsaPipeline->BindingCore.ImageInfos, imageInfo);
        array_push(pVsaPipeline->BindingCore.SamplerInfos, samplerInfo);
    }

}

void
vsa_pipeline_update_uniforms(VsaPipeline* pVsaPipeline)
{
    i64 cnt = array_count(pVsaPipeline->BindingCore.Uniforms);
    for (i64 u = 0; u < cnt; ++u)
    {
        VsaShaderUniform vsaUniform = pVsaPipeline->BindingCore.Uniforms[u];
        i32 waitFlag = (vsaUniform.IsUpdatedByFlag && !vsaUniform.ShouldBeUpdated);
        if (waitFlag)
            continue;

        i64 allCount = vsaUniform.Size / vsaUniform.ElementSize;
        //vguard((allCount == 1 || allCount == 20000) && "20000 min!");

        VkDescriptorBufferInfo bufferInfo = {
            .buffer = vsaUniform.Buffer,
            .offset = 0,
            .range = VK_WHOLE_SIZE
        };

        // NOTE(FEATURE0): VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT VkDescriptorSetLayoutBindingFlagsCreateInfo
        VkWriteDescriptorSet vkWriteDescriptorSet = {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .pNext = NULL,
            .dstSet = pVsaPipeline->BindingCore.DescriptorSet,
            .dstBinding = vsaUniform.Binding,
            .dstArrayElement = 0,
            .descriptorCount = 1,//allCount,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .pImageInfo = NULL,
            .pBufferInfo = &bufferInfo, //bufferInfos,
            .pTexelBufferView = NULL
        };

        // NOTE/TODO(): Can update everything in one call
        VkWriteDescriptorSet vkWriteDescriptorSets[] = {
            [0] = vkWriteDescriptorSet
        };

        vkUpdateDescriptorSets(gVulkanDevice, 1, vkWriteDescriptorSets, 0, NULL);

        VsaShaderUniform* pUniform =
            &pVsaPipeline->BindingCore.Uniforms[u];
        pUniform->Offset = 0;
    }

}

void
vsa_pipeline_update_descriptors(VsaPipeline* pVsaPipeline)
{
    VkWriteDescriptorSet* sets = NULL;
    VsaShaderDescriptor* descriptors = pVsaPipeline->BindingCore.Descriptors;
    i32 descrCnt = array_count(descriptors);

    //GSUCCESS("Max: %d\n", pVsaPipeline->BindingCore.MaxTexturesCount);
    for (i32 d = 0; d < descrCnt; ++d)
    {
        VsaShaderDescriptor descr = descriptors[d];
        if (!descr.ShouldBeUpdated)
            continue;

        if (array_count(pVsaPipeline->BindingCore.ImageInfos) > 500)
        {
            GERROR(RED5("Updating textures frame: %ld\n"),
                   array_count(pVsaPipeline->BindingCore.ImageInfos));
        }

        VkDescriptorImageInfo* pImageInfo = NULL;
        switch (descr.Type)
        {

        case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
            pImageInfo = pVsaPipeline->BindingCore.ImageInfos;
            break;

        case VK_DESCRIPTOR_TYPE_SAMPLER:
            pImageInfo = pVsaPipeline->BindingCore.SamplerInfos;
            break;

        default:
            GERROR("Mistake in pipeline creation!\n");
            vguard(0);
            break;

        }

        VkWriteDescriptorSet set = {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .pNext = NULL,
            .dstSet = pVsaPipeline->BindingCore.DescriptorSet,
            .dstBinding = descr.Binding,
            .dstArrayElement = 0,
            .descriptorCount = pVsaPipeline->BindingCore.MaxTexturesCount,
            .descriptorType = descr.Type,
            .pImageInfo = pImageInfo,
            .pBufferInfo = NULL,
            .pTexelBufferView = NULL
        };

        array_push(sets, set);

        VsaShaderDescriptor* pDescription = &descriptors[d];
        pDescription->ShouldBeUpdated = 0;
    }

    vkUpdateDescriptorSets(gVulkanDevice, array_count(sets), sets, 0, NULL);

    array_free(sets);
}

VsaDrawCall*
vsa_pipeline_add_draw_call(VsaPipeline* pVsaPipeline, VsaDrawCall vsaDrawCall)
{
    vguard(vsaDrawCall.VerticesOffset >= 0);
    vguard(vsaDrawCall.VerticesSize > 0);
    vguard(vsaDrawCall.IndicesOffset >= 0);
    vguard(vsaDrawCall.IndicesCount > 0);

    //GINFO("\n\n\n\n\nvsaDrawCall.VerticesSize: %d\n", vsaDrawCall.VerticesSize);
    pVsaPipeline->VerticesOffset += vsaDrawCall.VerticesSize;
    pVsaPipeline->IndicesOffset  += vsaDrawCall.IndicesCount * sizeof(u32);

    i64 ind = array_count(pVsaPipeline->DrawCalls);
    array_push(pVsaPipeline->DrawCalls, vsaDrawCall);
    return &pVsaPipeline->DrawCalls[ind];
}

void
vsa_pipeline_draw_calls_reset(VsaPipeline* pVsaPipeline)
{
    pVsaPipeline->VerticesOffset = 0;
    pVsaPipeline->IndicesOffset  = 0;
    array_clear(pVsaPipeline->DrawCalls);
}

void
vsa_pipeline_reset(VsaPipeline* pVsaPipeline)
{
    pVsaPipeline->IsPipelineRenderable = 0;

    vsa_pipeline_draw_calls_reset(pVsaPipeline);

    /* pVsaPipeline->VerticesOffset = 0; */
    /* pVsaPipeline->IndicesOffset  = 0; */
    /* pVsaPipeline->IsPipelineRenderable = 0; */
    /* array_clear(pVsaPipeline->DrawCalls); */

    i32 uniformsCnt = array_count(pVsaPipeline->BindingCore.Uniforms);
    for (i32 u = 0; u < uniformsCnt; ++u)
    {
        VsaShaderUniform* vui = &pVsaPipeline->BindingCore.Uniforms[u];
        vsa_uniform_reset(vui);
    }

}

VsaShaderUniform*
vsa_pipeline_get_uniform(VsaPipeline* pVsaPipeline, char* name)
{
    VsaShaderUniform* uniforms = pVsaPipeline->BindingCore.Uniforms;
    i32 ind = array_index_of(uniforms, item.Name == name);
    if (ind != -1)
        return &uniforms[ind];

    ind = array_index_of(uniforms, string_compare(item.Name, name));
    if (ind != -1)
        return &uniforms[ind];

    return NULL;
}

void
vsa_pipeline_destroy(VsaPipeline* pVsaPipeline)
{
    VsaPipeline vsaPipeline = *pVsaPipeline;

    vsa_shader_bindings_destroy(vsaPipeline.BindingCore);

    vsa_buffer_destroy(vsaPipeline.Vertex);
    vsa_buffer_destroy(vsaPipeline.Index);

    if (vsaPipeline.Type == VsaPipelineType_Indirect)
    {
        vsa_buffer_destroy(vsaPipeline.IndirectCmd.IndirectBuffer);
    }

    vkDestroyPipeline(gVulkanDevice, vsaPipeline.Pipeline, gAllocatorCallbacks);
    vkDestroyPipelineLayout(gVulkanDevice, vsaPipeline.PipelineLayout, gAllocatorCallbacks);

    memory_free(pVsaPipeline);
}

void
vsa_sync_create()
{
    vkValidHandle(gVulkanDevice);

    VkSemaphoreCreateInfo semaphoreCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0
    };

    VkFenceCreateInfo frameFenceCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .pNext = NULL,
        //NOTE(typedef): this will become unsignaled at the 2 line (vkResetFences) of the render loop
        .flags = VK_FENCE_CREATE_SIGNALED_BIT
    };

    VkResult createImageSemaphoreResult =
        vkCreateSemaphore(gVulkanDevice, &semaphoreCreateInfo, gAllocatorCallbacks, &gVulkanImageAvailableSemaphores);
    vkValidResult(createImageSemaphoreResult, "Failed image available semaphore creation!");

    VkResult createRenderSemaphoreResult =
        vkCreateSemaphore(gVulkanDevice, &semaphoreCreateInfo, gAllocatorCallbacks, &gVulkanRenderFinishedSemaphores);
    vkValidResult(createRenderSemaphoreResult, "Failed image available semaphore creation!");

    VkResult createFrameFenceResult =
        vkCreateFence(gVulkanDevice, &frameFenceCreateInfo, gAllocatorCallbacks, &gVulkanFrameFences);
    vkValidResult(createFrameFenceResult, "Failed frame fence creation!");

}

VkCommandPool
vsa_command_pool_create(VsaQueueFamily vsaQueueFamily)
{
    vkValidHandle(gVulkanDevice);

    VkCommandPool vkCommandPool;
    VkCommandPoolCreateInfo commandPoolCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext = NULL,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = vsaQueueFamily.GraphicsIndex
    };
    VkResult createCommandPoolResult =
        vkCreateCommandPool(gVulkanDevice, &commandPoolCreateInfo, gAllocatorCallbacks, &vkCommandPool);
    vkValidResult(createCommandPoolResult, "Can't create Command Pool!");

    return vkCommandPool;
}

VkCommandBuffer
vsa_command_buffer_create(VkCommandPool vkCommandPool)
{
    VkCommandBuffer vkCommandBuffer;

    VkCommandBufferAllocateInfo commandBufferAllocatedInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = NULL,
        .commandPool = vkCommandPool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };
    VkResult allocatingCommandBufferResult =
        vkAllocateCommandBuffers(gVulkanDevice, &commandBufferAllocatedInfo, &vkCommandBuffer);
    vkValidResult(allocatingCommandBufferResult, "Can't allocate Command Buffer!");

    return vkCommandBuffer;
}

VsaShaderUniform
vsa_uniform_create(size_t size, size_t elementSize, i32 bindingIndex, char* name)
{
    vkValidHandle(gVulkanDevice);

    VsaShaderUniform vsaUniform = {
        .Name = name,
        .Binding = bindingIndex,
        .ElementSize = elementSize,
        .Size = size,
        .Offset = 0,
        .Count = 0
    };

    vsaUniform.Buffer =
        vsa_buffer_create(
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            size,
            (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
            &vsaUniform.DeviceMemory);
    // DOCS(typedef): persistence mapping
    VkResult mapMemoryResult =
        vkMapMemory(
            gVulkanDevice, vsaUniform.DeviceMemory,
            0, size, 0, &vsaUniform.MappedMemory);
    vkValidResult(mapMemoryResult,
                  "Failed memory mapping for uniform buffer!");

    memset(vsaUniform.MappedMemory, 0, size);

    return vsaUniform;
}

void
vsa_uniform_reset(VsaShaderUniform* pVsaUniform)
{
    pVsaUniform->Offset = 0;
    pVsaUniform->Count = 0;
    pVsaUniform->ShouldBeUpdated = 0;
}

i32
vsa_uniform_set_data_w_offset(VsaShaderUniform* pVsaUniform, void* data, size_t offset, size_t size)
{
    vassert_not_null(pVsaUniform);
    vassert_not_null(data);
    vassert(size > 0 && "uniform size is 0!");

    size_t newOffset = offset + size;
    if (newOffset > pVsaUniform->Size)
    {
        GWARNING("Uniform overflow!\n");
        return 0;
    }

    memcpy(pVsaUniform->MappedMemory + offset, data, size);
    pVsaUniform->ShouldBeUpdated = 1;

    return 1;
}

i32
vsa_uniform_set_data(VsaShaderUniform* pVsaUniform, void* data, size_t size)
{
    i32 isUpdated = vsa_uniform_set_data_w_offset(pVsaUniform, data, pVsaUniform->Offset, size);

    pVsaUniform->Offset += size;
    pVsaUniform->Count += 1;

    return isUpdated;
}

void
vsa_uniform_update(VsaShaderUniform* pUniform, VsaPipeline* pVsaPipeline)
{
    i64 allCount = pUniform->Size / pUniform->ElementSize;
    //vguard((allCount == 1 || allCount == 20000) && "20000 min!");

    VkDescriptorBufferInfo bufferInfo = {
        .buffer = pUniform->Buffer,
        .offset = 0,
        .range = VK_WHOLE_SIZE
    };

    // NOTE(FEATURE0): VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT VkDescriptorSetLayoutBindingFlagsCreateInfo
    VkWriteDescriptorSet vkWriteDescriptorSet = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = NULL,
        .dstSet = pVsaPipeline->BindingCore.DescriptorSet,
        .dstBinding = pUniform->Binding,
        .dstArrayElement = 0,
        .descriptorCount = 1,//allCount,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .pImageInfo = NULL,
        .pBufferInfo = &bufferInfo, //bufferInfos,
        .pTexelBufferView = NULL
    };

    // NOTE/TODO(): Can update everything in one call
    VkWriteDescriptorSet vkWriteDescriptorSets[] = {
        [0] = vkWriteDescriptorSet
    };

    vkUpdateDescriptorSets(gVulkanDevice, 1, vkWriteDescriptorSets, 0, NULL);

    pUniform->Offset = 0;
}

void
vsa_uniform_destroy(VsaShaderUniform vsaUniform)
{
    vkDestroyBuffer(gVulkanDevice, vsaUniform.Buffer, gAllocatorCallbacks);
    vkFreeMemory(gVulkanDevice, vsaUniform.DeviceMemory, gAllocatorCallbacks);
}

VsaShaderStorageBuffer
vsa_ssbo_create(size_t size, size_t elementSize, i32 bindingIndex, char* name)
{
    vkValidHandle(gVulkanDevice);

    VsaShaderStorageBuffer vsaSsbo = {
        .Name = name,
        .Binding = bindingIndex,
        .ElementSize = elementSize,
        .Size = size,
        .Offset = 0,
        .Count = 0
    };

    vsaSsbo.Buffer =
        vsa_buffer_create(
            VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
            size,
            (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
            &vsaSsbo.DeviceMemory);
    // DOCS(typedef): persistence mapping
    VkResult mapMemoryResult =
        vkMapMemory(
            gVulkanDevice, vsaSsbo.DeviceMemory,
            0, size, 0, &vsaSsbo.MappedMemory);
    vkValidResult(mapMemoryResult,
                  "Failed memory mapping for uniform buffer!");

    memset(vsaSsbo.MappedMemory, 0, size);

    return vsaSsbo;
}

i32
vsa_ssbo_set_data(VsaShaderStorageBuffer* pSsbo, void* data, size_t size)
{
    vassert_not_null(pSsbo);
    vassert_not_null(data);
    vassert(size > 0 && "uniform size is 0!");

    if ((pSsbo->Offset + size) > pSsbo->Size)
    {
        GWARNING("Uniform overflow!\n");
        return 0;
    }

    memcpy(pSsbo->MappedMemory + pSsbo->Offset, data, size);
    pSsbo->Offset += pSsbo->ElementSize;

    //ERROR
    i32 isOffsetError = pSsbo->Offset % vsa_get_min_uniform_size();
    if (isOffsetError)
    {
        /*i32 modVal = pVsaUniform->Offset % vsa_get_min_uniform_size();
          i32 needsToAdd = vsa_get_min_uniform_size() - modVal;
          GERROR("OffsetFromUniform: %d MinUniformSize (For curr GPU): %d, Mod: %d NeedsToAdd: %d\n",
          pVsaUniform->Offset, vsa_get_min_uniform_size(), modVal, needsToAdd);*/
        //vassert(isOffsetError == 0 && "Alignment error!");
    }

    pSsbo->Count += 1;

    return 1;
}

void
vsa_frame_start(VkCommandBuffer vkCommandBuffer, i32* pImageIndex)
{
    /*
      NOTE(typedef): Render Loop
      * Wait for the previous frame to finish
      * Acquire an image from the swap chain
      * Reset cmd buffer
      */

    vkWaitForFences(gVulkanDevice, 1, &gVulkanFrameFences, VK_TRUE, VsaNoTimeout);

    VkResult accuireNextImageResult =
        vkAcquireNextImageKHR(gVulkanDevice, gVulkanSwapChain, VsaNoTimeout, gVulkanImageAvailableSemaphores, VK_NULL_HANDLE, (u32*) pImageIndex);
    if (accuireNextImageResult == VK_ERROR_OUT_OF_DATE_KHR)
    {
        GINFO("Recreate!\n");
        vsa_swapchain_objects_recreate();
        return;
    }
    else if (accuireNextImageResult == VK_SUBOPTIMAL_KHR)
    {
        // DOCS(typedef): this thing is not an error or smth, just continue there
    }
    else
    {
        vkValidResult(accuireNextImageResult, "Can't acquire next image khr!");
    }

    vkResetFences(gVulkanDevice, 1, &gVulkanFrameFences);

    vkResetCommandBuffer(vkCommandBuffer, VsaNoFlags);
}

void
vsa_frame_end(VkCommandBuffer vkCommandBuffer, i32* pImageIndex)
{
    /*
      NOTE(typedef):
      * Submit the recorded command buffer
      * Present the swap chain image
      */

    VkPipelineStageFlags pipelineStageFlag = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext = NULL,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &gVulkanImageAvailableSemaphores,
        .pWaitDstStageMask = &pipelineStageFlag,
        .commandBufferCount = 1,
        .pCommandBuffers = &vkCommandBuffer,
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = &gVulkanRenderFinishedSemaphores
    };

    // NOTE(typedef): Submiting Recorded Commands
    VkResult queueSubmitResult =
        vkQueueSubmit(gVulkanGraphicsQueue, 1, &submitInfo, gVulkanFrameFences);
    vkValidResult(queueSubmitResult, "Can't Submit Queue!");

    VkPresentInfoKHR presentInfoKhr = {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .pNext = NULL,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &gVulkanRenderFinishedSemaphores,
        .swapchainCount = 1,
        .pSwapchains = &gVulkanSwapChain,
        .pImageIndices = (const u32*) pImageIndex,
        .pResults = NULL
    };

    VkResult queuePresentKhr =
        vkQueuePresentKHR(gVulkanPresentationQueue, &presentInfoKhr);
    if (queuePresentKhr == VK_ERROR_OUT_OF_DATE_KHR || queuePresentKhr == VK_SUBOPTIMAL_KHR || gIsWindowBeenResized)
    {
        GINFO("Recreate!\n");
        gIsWindowBeenResized = 0;
        vsa_swapchain_objects_recreate();
        return;
    }
    else
    {
        vkValidResult(queuePresentKhr, "Queue Present KHR!\n");
    }

}

i32
vsa_get_max_texture_count()
{
    return gVulkanPhysicalDeviceLimits.maxDescriptorSetSampledImages;
}
i32
vsa_get_min_uniform_size()
{
    return gVulkanPhysicalDeviceLimits.minUniformBufferOffsetAlignment;
}
i32
vsa_get_max_uniform_buffer_range()
{
    return gVulkanPhysicalDeviceLimits.maxUniformBufferRange;
}
u32
vsa_get_max_vertex_binding_in_buffer()
{
    return gVulkanPhysicalDeviceLimits.maxVertexInputBindings;
}

size_t
vsa_get_valid_uniform_size(size_t itemSize)
{
    i32 minUniSize = vsa_get_min_uniform_size();

    if ((itemSize % minUniSize) == 0)
    {
        return itemSize;
    }

    size_t toAdd = minUniSize - (itemSize % minUniSize);
    i32 res = (itemSize + toAdd);
    return res;
}

// TODO: REfactor this
// BUG
// FIXME
void
vsa_swapchain_objects_cleanup()
{
    if (gVulkanSwapChain == VK_NULL_HANDLE)
    {
        GWARNING("SwapChain is NULL HANDLE!\n");
        vassert_break();
        return;
    }

    vkDestroySwapchainKHR(gVulkanDevice, gVulkanSwapChain, gAllocatorCallbacks);


}

VsaSwapChainSettings
vsa_get_swapchain_settings(VsaSettings settings)
{
    VsaSwapChainSettings swapChainSettings = {
        .SurfaceFormat = {
            .format = VK_FORMAT_B8G8R8A8_SRGB,
            .colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
        }
    };
    if (settings.IsVSyncEnabled)
    {
        swapChainSettings.PresentationMode = VK_PRESENT_MODE_FIFO_KHR;
    }
    else
    {
        swapChainSettings.PresentationMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
    }

    return swapChainSettings;
}

// TODO: WIP
// BUG/FIXME: Not working yet
void
vsa_swapchain_objects_recreate()
{
    //vguard(0 && "SwapChain objects recreate!!!");
    vkDeviceWaitIdle(gVulkanDevice);

    VsaSwapChainSettings swapChainSettings =
        vsa_get_swapchain_settings(gVsaSetting);
    VkSwapchainKHR swapChain = vsa_swapchain_create(swapChainSettings, gVulkanSupportDetails, gVulkanQueueFamily);

    // DOCS: Destroy swapChain before assing
    vsa_swapchain_objects_cleanup();
    gVulkanSwapChain = swapChain;

    i64 i, count = array_count(aRenderPasses);
    for (i = 0; i < count; ++i)
    {
        VsaRenderPass* pRenderPass = &aRenderPasses[i];
        vsa_render_pass_update_images_and_framebuffers(pRenderPass);
    }

    // GINFO("Extent: %d %d\n", gVulkanExtent.width, gVulkanExtent.height);
}

VsaImageView*
_vsa_get_image_views_from_swapchain(VkFormat vkFormat)
{
    VsaImageView* aVsaImageViews = NULL;

    VkImage* aImages = vkGetSwapChainImages(gVulkanDevice, gVulkanSwapChain);
    i64 imagesCount = array_count(aImages);
    vguard((imagesCount > 0) && "Images Count should be > 0 !!!");

    for (i64 i = 0; i < imagesCount; ++i)
    {
        VkImageView imageView = vsa_image_view_create(
            aImages[i],
            vkFormat,
            VK_IMAGE_ASPECT_COLOR_BIT,
            1);

        VsaImageView vsaImageView = {
            .Type = VsaImageViewType_SwapChained,
            .Image = aImages[i],
            .View = imageView,
        };

        array_push(aVsaImageViews, vsaImageView);
    }

    return aVsaImageViews;
}

VsaRenderPass*
vsa_get_render_pass(i64 renderPassId)
{
    i64 renderPassCnt = array_count(aRenderPasses);
    vguard((renderPassId < renderPassCnt) && (renderPassId >= 0) && "RenderPass index if wrong !!!");

    return &aRenderPasses[renderPassId];
}

/*
  TODO: Should render pass be created inside api itself, or we should create it on application side

  app() {
   renderPass = render_pass_new(...);
   renderPassId = push_render_pass_to_stack(renderPass)

   ...

   for_each(renderPass in gRenderPasses) {
    _vsa_render_pass_proccess(renderPass);
   }
  }
*/

void
vsa_render_pass_proccess()
{
    static i32 imageIndex = 0;
    vsa_frame_start(gVulkanCommandBuffer, &imageIndex);

    i64 renderPassesCount = array_count(aRenderPasses);

    /*
      DOCS(typedef): Recording Commands BEGIN
    */
    VkCommandBufferBeginInfo commandBufferBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = NULL,
        .flags = 0,
        .pInheritanceInfo = NULL,
    };
    VkResult beginCommandBufferResult =
        vkBeginCommandBuffer(gVulkanCommandBuffer, &commandBufferBeginInfo);
    vkValidResult(beginCommandBufferResult, "Begin command buffer!");

    for (i64 i = 0; i < renderPassesCount; ++i)
    {
        VsaRenderPass* pRenderPass = &aRenderPasses[i];
        if (pRenderPass->IsDisabled)
            continue;
        _vsa_render_pass_proccess(pRenderPass, imageIndex);
    }

    /*
      DOCS(typedef): Recording Commands END
    */

    VkResult endCommandBufferResult = vkEndCommandBuffer(gVulkanCommandBuffer);
    vkValidResult(endCommandBufferResult, "Can't end command buffer!");

    vsa_frame_end(gVulkanCommandBuffer, &imageIndex);

}

void
vsa_render_pass_create()
{
    VsaRenderPassSettings settings = {

        // NOTE(typedef): This makes sure that writes to the
        // depth image are done before we try to write to it again
        .Dependency = (VkSubpassDependency) {
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
            .dependencyFlags = 0,
        },

        .AttachmentsCount = 3,
        .Attachments = {
            [0] = (VsaAttachment) {
                .Type = VsaAttachmentType_Default,
                .Description = {
                    .format = gVulkanSurfaceFormat.format,
                    .samples = gVulkanSamplesCount,
                    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                    .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                    .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                },
                .Reference = {
                    //.attachment = 0, AUTO DUE TO INDEX IN ARRAY
                    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                }
            },

            [1] = (VsaAttachment) {
                .Type = VsaAttachmentType_Depth,
                .Description = {
                    .format = VK_FORMAT_D32_SFLOAT,
                    .samples = gVulkanSamplesCount,
                    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                    .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
                },
                .Reference = {
                    .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
                }
            },

            [2] = (VsaAttachment) {
                .Type = VsaAttachmentType_SwapChained,
                .Description = {
                    .format = gVulkanSurfaceFormat.format,
                    .samples = VK_SAMPLE_COUNT_1_BIT,
                    .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
                },
                .Reference = {
                    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                }
            }
        },

    };

    gRenderPass = vsa_render_pass_new(settings);
}

i64
vsa_register_render_pass(VsaRenderPass vsaRenderPass)
{
    i64 id = array_count(aRenderPasses);
    array_push(aRenderPasses, vsaRenderPass);
    return id;
}

// NOTE: WIP
// TODO: remove push to aRenderPasses
VsaRenderPass
vsa_render_pass_new(VsaRenderPassSettings settings)
{
    /*
      DEPENDS:
      * gVulkanExtent
      * gVulkanSurfaceFormat
      * gVulkanSamplesCount
      * gVulkanDevice
    */

    vkValidHandle(gVulkanDevice);

    VkAttachmentDescription* allDescrs = NULL;
    VkAttachmentReference* aColorRefs = NULL;
    VkAttachmentReference* pResolvedRef = NULL;
    VkAttachmentReference* pDepthRef = NULL;

    VsaAttachment* aAttachments = NULL;

    /* NOTE(typedef): this is layout(location=0) out vec4 Color; in .frag */
    // TODO: Split Descr's/Ref's assingments AND imageViewCreation
    for (i64 i = 0; i < settings.AttachmentsCount; ++i)
    {
        settings.Attachments[i].Reference.attachment = i;
        VsaAttachment attach = settings.Attachments[i];
        array_push(aAttachments, attach);

        switch (attach.Type)
        {

        case VsaAttachmentType_Default:
            array_push(aColorRefs, settings.Attachments[i].Reference);
            break;

        case VsaAttachmentType_Depth:
            pDepthRef = &settings.Attachments[i].Reference;
            break;

        case VsaAttachmentType_ResolveMultisample:
            vassert_null(pResolvedRef);
            pResolvedRef = &settings.Attachments[i].Reference;
            break;

        case VsaAttachmentType_SwapChained:
            vassert_null(pResolvedRef);
            pResolvedRef = &settings.Attachments[i].Reference;
            break;

        }

        array_push(allDescrs, attach.Description);
    }

        // NOTE: Validation stuff
#if ENGINE_DEBUG == 1
#warning "Runing in debug/slow mode"

    i32 attachmentSwapChainedExist = 0;
    i32 attachmentMultisampleResolveExist = 0;
    array_foreach(aAttachments,
                  if (item.Type == VsaAttachmentType_SwapChained)
                  {
                      attachmentSwapChainedExist = 1;
                  }

                  if (item.Type == VsaAttachmentType_ResolveMultisample)
                  {
                      attachmentMultisampleResolveExist = 1;
                  }
        );

    if (attachmentSwapChainedExist && attachmentMultisampleResolveExist)
    {
        GERROR("For some weird reason u have Multisample Resolve Attachment and Swap Chained Attachment, it's not allowed!\n");
        vassert_break();
    }

#endif

    VkSubpassDescription subpassDescription = {
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = array_count(aColorRefs),
        .pColorAttachments = aColorRefs,
        .pResolveAttachments = pResolvedRef,
        .pDepthStencilAttachment = pDepthRef,

        // NOTE: Just for doc
        .inputAttachmentCount = 0,
        .pInputAttachments = NULL,
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = NULL
    };

    VkRenderPassCreateInfo renderPassCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        // NOTE: Maybe in some cases it may cause a problem
        .attachmentCount = array_count(allDescrs),
        .pAttachments = allDescrs,
        .subpassCount = 1,
        .pSubpasses = &subpassDescription,
        .dependencyCount = 1,
        .pDependencies = &settings.Dependency
    };

    // TODO: Render pass create vsa_render_pass_create()
    VkRenderPass vkRenderPass;
    VkResult createRenderPassResult =
        vkCreateRenderPass(gVulkanDevice, &renderPassCreateInfo, gAllocatorCallbacks, &vkRenderPass);
    vkValidResult(createRenderPassResult, "Can't create RenderPass!");

    // DOCS: Finished RenderPass
    VsaRenderPass renderPass = {
        .Type = settings.Type,
        .RenderPass = vkRenderPass,
        .aAttachments = aAttachments,
    };

    // DOCS: Create image views and framebuffers
    vsa_render_pass_update_images_and_framebuffers(&renderPass);

    array_free(allDescrs);
    array_free(aColorRefs);

    return renderPass;
}

void
vsa_render_pass_destroy(VsaRenderPass* pRenderPass)
{
    array_free(pRenderPass->aAttachments);

    // DOCS: Needs to be destroyed before ImageViews and RenderPass
    // DEPENDS: ImageView, RenderPass
    i64 framebufferCount = array_count(pRenderPass->aFramebuffers);
    for (i64 f = 0; f < framebufferCount; ++f)
    {
        VkFramebuffer vkFramebuffer = pRenderPass->aFramebuffers[f];
        vkDestroyFramebuffer(gVulkanDevice, vkFramebuffer, gAllocatorCallbacks);
    }

    array_foreach(pRenderPass->aVsaImageViews,
                  if (item.Type != VsaImageViewType_SwapChained)
                  {
                      vsa_texture_image_destroy(item.Image, item.View, item.Memory);
                  }
                  else
                  {
                      vkDestroyImageView(gVulkanDevice, item.View, gAllocatorCallbacks);
                  });

    // BUG: Idk: double free bug
    // array_free(pRenderPass->aFramebuffers);

    vkDestroyRenderPass(gVulkanDevice, pRenderPass->RenderPass, gAllocatorCallbacks);
}

void
_vsa_render_pass_add_framebuffer(VsaRenderPass* pRenderPass, VkImageView* aAttachments, i32 attachmentsCount)
{
    VkFramebufferCreateInfo frabufferCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .renderPass = pRenderPass->RenderPass,
        .attachmentCount = attachmentsCount,
        .pAttachments = aAttachments,
        .width = gVulkanExtent.width,
        .height = gVulkanExtent.height,
        .layers = 1
    };

    VkFramebuffer vkFramebuffer;
    VkResult createFramebufferResult =
        vkCreateFramebuffer(gVulkanDevice, &frabufferCreateInfo, gAllocatorCallbacks, &vkFramebuffer);
    vkValidResult(createFramebufferResult, "Can't create framebuffer!");

    array_push(pRenderPass->aFramebuffers, vkFramebuffer);
}

void
vsa_render_pass_update_images_and_framebuffers(VsaRenderPass* pRenderPass)
{

    // DOCS: Clean Up, delete old image views and framebuffers
    i32 firstRun = (pRenderPass->aVsaImageViews != NULL) ? 0 : 1;
    if (!firstRun)
    {
        // TODO: Create function for destroying framebuffers, imageviews
        array_foreach(pRenderPass->aFramebuffers, vkDestroyFramebuffer(gVulkanDevice, item, gAllocatorCallbacks););

        array_foreach(pRenderPass->aVsaImageViews,
                      if (item.Type == VsaImageViewType_SwapChained)
                      {
                          vkDestroyImageView(gVulkanDevice, item.View, gAllocatorCallbacks);
                      }
                      else
                      {
                          vsa_texture_image_destroy(item.Image, item.View, item.Memory);
                      });

        array_free(pRenderPass->aVsaImageViews);
        array_free(pRenderPass->aFramebuffers);
        vassert(pRenderPass->aFramebuffers == NULL && "pRenderPass->aFramebuffers == NULL!");
    }

    // DOCS: Create images
    VsaImageView* aVsaImageViews = NULL;

    for (i64 i = 0; i < array_count(pRenderPass->aAttachments); ++i)
    {
        VsaAttachment attach = pRenderPass->aAttachments[i];

        switch (attach.Type)
        {

        case VsaAttachmentType_Default:
        {
            VsaImageViewSettings settings = {
                .Type = VsaImageViewType_Default,
                .ImageCreateExtSettings = (VsaImageCreateExtSettings) {
                    .Width = gVulkanExtent.width,
                    .Height = gVulkanExtent.height,
                    .Depth = 1.0f,
                    .MipLevels = 1,
                    .SamplesCount = attach.Description.samples,
                    .Format = attach.Description.format,
                    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
                    .ImageUsageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
                },
                .Format = attach.Description.format,
                .ImageAspectFlags = VK_IMAGE_ASPECT_COLOR_BIT,
                .MipLevels = 1,
            };

            VsaImageView vsaColorImageView = vsa_image_view_new(settings);
            array_push(aVsaImageViews, vsaColorImageView);

            break;
        }

        case VsaAttachmentType_Depth:
        {
            // TODO: Write find_format for depth instead of using concrete type
            VkFormat depthAttachmentFormat = VK_FORMAT_D32_SFLOAT;

            VsaImageViewSettings settings = {
                .Type = VsaImageViewType_Default,
                .ImageCreateExtSettings = (VsaImageCreateExtSettings) {
                    .Width = gVulkanExtent.width,
                    .Height = gVulkanExtent.height,
                    .Depth = 1.0f,
                    .MipLevels = 1,
                    .SamplesCount = attach.Description.samples,
                    .Format = depthAttachmentFormat,
                    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
                    .ImageUsageFlags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
                },
                .Format = depthAttachmentFormat,
                .ImageAspectFlags = VK_IMAGE_ASPECT_DEPTH_BIT,
                .MipLevels = 1,
            };

            VsaImageView vsaDepthImageView = vsa_image_view_new(settings);
            array_push(aVsaImageViews, vsaDepthImageView);

            break;
        }

        case VsaAttachmentType_ResolveMultisample:
        {
            VsaImageViewSettings settings = {
                .Type = VsaImageViewType_Default,
                .ImageCreateExtSettings = (VsaImageCreateExtSettings) {
                    .Width = gVulkanExtent.width,
                    .Height = gVulkanExtent.height,
                    .Depth = 1.0f,
                    .MipLevels = 1,
                    .SamplesCount = attach.Description.samples,
                    .Format = attach.Description.format,
                    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
                    .ImageUsageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
                },
                .Format = attach.Description.format,
                .ImageAspectFlags = VK_IMAGE_ASPECT_COLOR_BIT,
                .MipLevels = 1,
            };

            VsaImageView vsaColorImageView = vsa_image_view_new(settings);
            array_push(aVsaImageViews, vsaColorImageView);

            break;
        }

        case VsaAttachmentType_SwapChained:
        {
            VsaImageView* aSwapChainedImageViews =
                _vsa_get_image_views_from_swapchain(
                    attach.Description.format);

            for (i32 si = 0; si < array_count(aSwapChainedImageViews); ++si)
            {
                VsaImageView vsaImageView = aSwapChainedImageViews[si];
                array_push(aVsaImageViews, vsaImageView);
            }

            array_free(aSwapChainedImageViews);

            break;
        }

        }

    }

    // DOCS: Update Stage
    pRenderPass->aVsaImageViews = aVsaImageViews;

    i32 isSwapChained = (array_index_of(pRenderPass->aVsaImageViews, item.Type == VsaImageViewType_SwapChained) != -1);
    if (isSwapChained)
    {
        VkImageView* aSwapChained = NULL;
        array_foreach(pRenderPass->aVsaImageViews,
                      if (item.Type == VsaImageViewType_SwapChained)
                          array_push(aSwapChained, item.View));


        i32 framebuffersCount = array_count(aSwapChained);
        for (i32 f = 0; f < framebuffersCount; ++f)
        {
            VkImageView* aAttachments = NULL;
            array_foreach(pRenderPass->aVsaImageViews,
                          if (item.Type != VsaImageViewType_SwapChained)
                              array_push(aAttachments, item.View);
                );

            array_push(aAttachments, aSwapChained[f]);
            _vsa_render_pass_add_framebuffer(pRenderPass, aAttachments, array_count(aAttachments));

            array_free(aAttachments);
        }

    }
    else
    {
        VkImageView* aImageViews = NULL;
        array_foreach(pRenderPass->aVsaImageViews,
                      array_push(aImageViews, item.View);
            );
        _vsa_render_pass_add_framebuffer(pRenderPass, aImageViews, array_count(aImageViews));
    }

}

void
vsa_core_init(VsaSettings vsaSettings)
{
    if (gVsaCoreInitialized == 1)
    {
        return;
    }
    gVsaCoreInitialized = 1;
    GINFO("Init Vulkan Core\n");

    gVsaSetting = vsaSettings;

    /*
      DOCS(typedef): Core
    */

    vsa_instance_create(&gVsaSetting);
    vsa_surface_create();
    vsa_physical_device_create();
    // DEPENDS(typedef): On Physical Device
    // TODO(typedef): Make this configurable
    gVulkanSamplesCount = Min(gVsaSetting.SamplesCount, vsa_samples_get_max_count());
    vsa_queue_family_create(&gVulkanSupportDetails);
    vsa_device_create(gVulkanQueueFamily);
    vsa_graphics_queue_create(gVulkanQueueFamily.GraphicsIndex);
    vsa_presentation_queue_create(gVulkanQueueFamily.PresentationIndex);

    vsa_sync_create();

    /*
      NOTE(typedef): Create "Infrastructure"|SwapChains for managing ALL
      Buffers (Framebuffer, VkBuffers, etc..)
      SwapChain is essentially array of images for presentation on the screen

      Creates: gVulkanExtent
    */
    VsaSwapChainSettings swapChainSettings =
        vsa_get_swapchain_settings(gVsaSetting);
    gVulkanSwapChain = vsa_swapchain_create(swapChainSettings, gVulkanSupportDetails, gVulkanQueueFamily);

    /*
      DEPENDS:
      * gVulkanExtent
      * gVulkanSurfaceFormat
      * gVulkanSamplesCount
      * gVulkanDevice
    */
    //vsa_render_pass_create();

    // DOCS: Object picking RenderPass, Framebuffer and Pipeline mb

    // NOTE(typedef): For now it's part of Core Api
    gVulkanCommandPool = vsa_command_pool_create(gVulkanQueueFamily);
    gVulkanCommandBuffer = vsa_command_buffer_create(gVulkanCommandPool);

    vsa_samplers_init();
}

void
vsa_set_window_resized()
{
    gIsWindowBeenResized = 1;
}

void
vsa_device_wait_idle()
{
    vkDeviceWaitIdle(gVulkanDevice);
}

void
vsa_core_deinit()
{
    vsa_samplers_deinit();

    vsa_swapchain_objects_cleanup();

    // TODO: Destroy pipelines here
    array_foreach(aRenderPasses, vsa_render_pass_destroy(&item));

    vkDestroySemaphore(gVulkanDevice, gVulkanImageAvailableSemaphores, gAllocatorCallbacks);
    vkDestroySemaphore(gVulkanDevice, gVulkanRenderFinishedSemaphores, gAllocatorCallbacks);
    vkDestroyFence(gVulkanDevice, gVulkanFrameFences, gAllocatorCallbacks);
    vkDestroyCommandPool(gVulkanDevice, gVulkanCommandPool, gAllocatorCallbacks);

    vkDestroyDevice(gVulkanDevice, gAllocatorCallbacks);
    if (gVulkanDebugMessanger)
        vkDestroyDebugUtilsMessenger(gVulkanInstance, gVulkanDebugMessanger, gAllocatorCallbacks);
    vkDestroySurfaceKHR(gVulkanInstance, gVulkanSurface, gAllocatorCallbacks);

    vkDestroyInstance(gVulkanInstance, gAllocatorCallbacks);

}


/*##################################
  DOCS(typedef): Vsa Api Get Helpers
  ##################################*/
VkFormat
vsa_get_surface_format()
{
    return gVulkanSurfaceFormat.format;
}

VkSampleCountFlagBits
vsa_get_samples_count()
{
    return gVulkanSamplesCount;
}
