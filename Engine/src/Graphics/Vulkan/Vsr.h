#ifndef VSR_H
#define VSR_H

#include <Core/Types.h>
#include "VulkanSimpleApiTypes.h"

struct VsaPipeline;
struct VsrBase;

void vsr_init();
i64 vsr_get_render_pass_id();

/* DOCS: Vsr as base structure for all renderers */

typedef struct VsrBufferUsage
{
    size_t TotalSize;
    size_t UsedSize;
} VsrBufferUsage;

/*
  NOTE: We should set this struct inside renderer calls
*/
typedef struct VsrStats
{
    size_t VerticesCount;
    size_t TexturesCount;
    size_t ItemsCount;
    size_t MaterialsCount;

    size_t MaxVerticesCount;
    size_t MaxTexturesCount;
    size_t MaxItemsCount;
    size_t MaxMaterialsCount;

    u16 SingleVertexSize;
    u16 SingleItemSize;
    u16 SingleMaterialSize;

    VsrBufferUsage VertexBuffer;
    VsrBufferUsage IndexBuffer;
} VsrStats;

typedef struct VsrBaseSettings
{
    char* Name;
    IStringPool* pStringPool;
    VsaPipeline Pipeline;

    size_t MaxVerticesCount;
    size_t MaxTexturesCount;
    size_t MaxItemsCount;
    size_t MaxMaterialsCount;

    i8 Priority;
} VsrBaseSettings;

typedef struct VsrBase
{
    i8 IsInitialized;
    char* Name;
    IStringPool* pStringPool;
    VsaPipeline* pPipeline;
    VsrStats Stats;
} VsrBase;

void vsr_base_create(VsrBase* pBase, VsrBaseSettings settings);
VsaShaderUniform* vsr_base_get_uniform(VsrBase* pBase, char* pName);
void vsr_base_destroy(VsrBase* pBase);

/* DOCS: Vsr as platform */
typedef struct IVsrRenderer
{
    char* Name;
    void* Renderer;
    void (*Destroy)(void* data);
} IVsrRenderer;

void vsr_add_pipeline(struct VsaPipeline* pVsaPipeline);
void vsr_sort_pipeline();
// TODO: remove
void vsr_add_renderer(IVsrRenderer renderer);
/* void vsr_frame_begin(); */
/* void vsr_draw_all(); */
/* void vsr_frame_end(); */

void vsr_render_passes_execute();

i32 vsr_get_image_index();
size_t vsr_get_indices_size(size_t maxVerticesCount);
void vsr_destroy();


typedef struct VsrMeshRecord
{
    size_t MeshId;
    size_t Count;

    size_t VerticesCountInd;
    size_t IndicesCountInd;

    size_t VBufferOffset;
    size_t IBufferOffset;

    size_t IndicesCount;
} VsrMeshRecord;

VkDrawIndexedIndirectCommand* vsr_mesh_records_to_idirect_cmds(VsrMeshRecord* aMeshRecords);

#endif // VSR_H
