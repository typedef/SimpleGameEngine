#include "Vsr.h"

#include <stdlib.h>

#include "Graphics/Vulkan/VulkanSimpleApiTypes.h"
#include "VulkanSimpleApi.h"
#include "vulkan_core.h"
#include <Core/SimpleStandardLibrary.h>

typedef struct VsrGlobalData
{
    i64 RenderPassId;
} VsrGlobalData;

static VsrGlobalData gVsrGlobalData = {};

void
vsr_init()
{
    VkFormat surfaceFormat = vsa_get_surface_format();
    VkSampleCountFlagBits samplesCount = vsa_get_samples_count();

    VsaRenderPassSettings settings = {
        .Type = VsaRenderPassType_SwapChained,

        // NOTE(typedef): This makes sure that writes to the
        // depth image are done before we try to write to it again
        .Dependency = (VkSubpassDependency) {
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
            .dependencyFlags = 0,
        },

        .AttachmentsCount = 3,
        .Attachments = {
            [0] = (VsaAttachment) {
                .Type = VsaAttachmentType_Default,
                .Description = {
                    .format = surfaceFormat,
                    .samples = samplesCount,
                    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                    .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                    .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                },
                .Reference = {
                    //.attachment = 0, AUTO DUE TO INDEX IN ARRAY
                    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                }
            },

            [1] = (VsaAttachment) {
                .Type = VsaAttachmentType_Depth,
                .Description = {
                    .format = VK_FORMAT_D32_SFLOAT,
                    .samples = samplesCount,
                    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                    .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
                },
                .Reference = {
                    .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
                }
            },

            [2] = (VsaAttachment) {
                .Type = VsaAttachmentType_SwapChained,
                .Description = {
                    .format = surfaceFormat,
                    .samples = VK_SAMPLE_COUNT_1_BIT,
                    .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
                },
                .Reference = {
                    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                }
            },

        },

    };

    VsaRenderPass mainVsrRenderPass = vsa_render_pass_new(settings);
    i64 renderPassId = vsa_register_render_pass(mainVsrRenderPass);

    gVsrGlobalData.RenderPassId = renderPassId;
}

i64
vsr_get_render_pass_id()
{
    return gVsrGlobalData.RenderPassId;
}

void
vsr_base_create(VsrBase* pBase, VsrBaseSettings settings)
{
    // TODO: Create instance if settings is null in the future
    // for now use temp desicion of not allowing nulls at all
    vguard_not_null(settings.Name);
    vguard_not_null(settings.pStringPool);

    size_t pipelineSize = sizeof(VsaPipeline);
    VsaPipeline* pVsaPipeline = memory_allocate(pipelineSize);
    memcpy(pVsaPipeline, &settings.Pipeline, pipelineSize);

    VsrBase base = {
        .IsInitialized = 1,
        .Name = string(settings.Name),
        .pStringPool = settings.pStringPool,
        .pPipeline = pVsaPipeline,
    };
    base.pPipeline->pName = base.Name;
    base.pPipeline->Priority = settings.Priority;

    // DOCS(typedef): Allocating buffers
    i64 maxVerticesCount = settings.MaxVerticesCount;
    u32 indCnt = vsr_get_indices_size(maxVerticesCount);

    size_t vSize = maxVerticesCount * base.pPipeline->Stride;
    size_t iSize = indCnt * sizeof(u32);
    VsaBuffer vertex =
        vsa_buffer_new(VsaBufferType_Dynamic, NULL,
                       vSize,
                       VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
    VsaBuffer index =
        vsa_buffer_new(VsaBufferType_Dynamic, NULL,
                       iSize,
                       VK_BUFFER_USAGE_INDEX_BUFFER_BIT);

    VsrStats* pStats = &base.Stats;
    pStats->MaxVerticesCount = settings.MaxVerticesCount;
    pStats->MaxTexturesCount = settings.MaxTexturesCount;
    pStats->MaxItemsCount = settings.MaxItemsCount;
    pStats->MaxMaterialsCount = settings.MaxMaterialsCount;
    pStats->VertexBuffer.TotalSize = vSize;
    pStats->IndexBuffer.TotalSize = iSize;

    GINFO(RED5("V: %ld I: %ld\n"), maxVerticesCount, indCnt);
    GSUCCESS("VerticesCnt: %ld, SingleSize: %d\n", maxVerticesCount, base.pPipeline->Stride);

    vsa_pipeline_set_buffers(base.pPipeline, vertex, index);

    *pBase = base;

    vsr_add_pipeline(pBase->pPipeline);
}

VsaShaderUniform*
vsr_base_get_uniform(VsrBase* pBase, char* pName)
{
    char* ipName = istring_ext(pBase->pStringPool, pName);
    VsaShaderUniform* pVsaUniformInternal =
        vsa_pipeline_get_uniform(pBase->pPipeline, ipName);

    if (pVsaUniformInternal == NULL)
    {
        GERROR("Uniform %s not found!\n", pName);
        vguard_not_null(pVsaUniformInternal);
    }

    return pVsaUniformInternal;
}

void
vsr_base_destroy(VsrBase* pBase)
{
    memory_free(pBase->Name);
    istring_pool_destroy(pBase->pStringPool);
    vsa_pipeline_destroy(pBase->pPipeline);
}

typedef struct VsrFrameData
{
    i32 ImageIndex;
} VsrFrameData;

static VsrFrameData gVsrFrameData = {};

void
vsr_add_pipeline(VsaPipeline* pVsaPipeline)
{
    VsaRenderPass* pRenderPass = vsa_get_render_pass(pVsaPipeline->RenderPassIndex);

    array_push(pRenderPass->apPipelines, pVsaPipeline);
}

i32
_pipeline_sort_function(const void* pA, const void* pB)
{
    VsaPipeline* pAPipeline = (VsaPipeline*) pA;
    VsaPipeline* pBPipeline = (VsaPipeline*) pB;

    if (pAPipeline->Priority > pBPipeline->Priority)
    {
        return 1;
    }
    else if (pAPipeline->Priority < pBPipeline->Priority)
    {
        return -1;
    }

    return 0;
}

void
vsr_render_passes_execute()
{
    vsa_render_pass_proccess();
}

i32
vsr_get_image_index()
{
    return gVsrFrameData.ImageIndex;
}

size_t
vsr_get_indices_size(size_t maxVerticesCount)
{
    size_t indicesCount = (size_t) (1.5f * maxVerticesCount + 1);
    return indicesCount;
}

VkDrawIndexedIndirectCommand*
vsr_mesh_records_to_idirect_cmds(VsrMeshRecord* aMeshRecords)
{
    size_t instancesCnt = 0;
    VkDrawIndexedIndirectCommand* cmds = NULL;
    i64 meshRecordsCount = array_count(aMeshRecords);

    for (i64 i = 0; i < meshRecordsCount; ++i)
    {
        VsrMeshRecord meshRecord = aMeshRecords[i];

        VkDrawIndexedIndirectCommand idirectCmd = {
            .indexCount = meshRecord.IndicesCount, //NOTE: DONE
            .instanceCount = meshRecord.Count, // 1|multiple mesh to draw
            .firstIndex = meshRecord.IndicesCountInd,
            .vertexOffset = meshRecord.VerticesCountInd,
            .firstInstance = instancesCnt // wo feature enabled is always 0
        };

        array_push(cmds, idirectCmd);

        instancesCnt += meshRecord.Count;
    }

    return cmds;
}
