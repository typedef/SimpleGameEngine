#ifndef VSR_3D_H
#define VSR_3D_H

#include "Vsr.h"

struct MeshMaterial;
struct MeshMaterialInfo;
struct StaticMesh;
struct StaticModel;
struct DynamicMesh;
struct DynamicModel;

/*
  NOTE:
  Требования к рендеру:
  * Удобный рендеринг модели в одном числе
  * Удобный рендеринг массива моделей, возможно переданный в виде SimpleChunk
  * Удобное обновление материалов,, индексов материалов, трансформов

  Возможно разделить работу по сохранению уже добавленных "чанков" и структурирование этих чанков переложить на RenderCompositor, разгрузив тем самым рендер.

 */

/*
  DOCS: Renderer for static geometry
*/

typedef struct Vsr3dSSetting
{
    i64 MaxTexture;
    i64 MaxVertices;
    i64 MaxInstances;
    i64 MaxMaterialsCount;
} Vsr3dSSetting;

typedef struct Vsr3dS
{
    // TODO: Fill Stats in submit func
    VsrBase Base;

    VsaShaderUniform* pTransformUniform;
    VsaShaderUniform* pInstanceUniform;
    VsaShaderUniform* pMaterialUniform;
    VsaShaderUniform* pCameraUbo;

    size_t VBufferOffset;
    size_t IBufferOffset;
    size_t VerticesOffsetCnt;
    size_t IndicesOffsetCnt;

} Vsr3dS;

void vsr_3d_s_create(Vsr3dS* pRenderer, Vsr3dSSetting* pSettings);
void vsr_3d_s_destroy(Vsr3dS* pRenderer);

void vsr_3d_s_set_transform_ubo(Vsr3dS* pRenderer, m4* aTransform);
void vsr_3d_s_set_instance_ubo(Vsr3dS* pRenderer, i64* pMaterialIndices);
void vsr_3d_s_set_material_ubo(Vsr3dS* pRenderer, struct MeshMaterial* pMaterials);
void vsr_3d_s_set_camera_ubo(Vsr3dS* pRenderer, m4 viewProjection);
VsrMeshRecord* vsr_3d_s_submit_model(Vsr3dS* pRenderer, struct StaticModel* pModel, m4 transform, struct MeshMaterialInfo* aMeshMaterialInfo);
VsrMeshRecord vsr_3d_s_submit_meshes(Vsr3dS* pRenderer, struct StaticMesh* pMesh, m4* aTransforms, size_t count, struct MeshMaterialInfo* pMeshMaterialInfo);
VsrMeshRecord vsr_3d_s_submit_mesh(Vsr3dS* pRenderer, struct StaticMesh* pMesh, m4 transform, struct MeshMaterialInfo* pMeshMaterialInfo);
void vsr_3d_s_flush(Vsr3dS* pRenderer, VsrMeshRecord* pMeshRecords, size_t meshRecordsCount);

/*
  DOCS: 3d for dynamic models,
  skined meshes with animation
*/

typedef struct Vsr3dDSetting
{
    i64 MaxTexture;
    i64 MaxVertices;
    i64 MaxInstances;
    i64 MaxMaterialsCount;
} Vsr3dDSetting;

typedef struct Vsr3dD
{
    VsrBase Base;

    VsaShaderUniform* pTransformUniform;
    VsaShaderUniform* pInstanceUniform;
    VsaShaderUniform* pMaterialUniform;
    VsaShaderUniform* pCameraUbo;

    size_t VBufferOffset;
    size_t IBufferOffset;
    size_t VerticesOffsetCnt;
    size_t IndicesOffsetCnt;

} Vsr3dD;

void vsr_3d_d_create(Vsr3dD* pRenderer, Vsr3dDSetting* pSetting);
void vsr_3d_d_destroy(Vsr3dD* pRenderer);

void vsr_3d_d_set_transform_ubo(Vsr3dD* pRenderer, m4* aTransforms);
void vsr_3d_d_set_instance_ubo(Vsr3dD* pRenderer, i64* aMaterialIndices);
void vsr_3d_d_set_material_ubo(Vsr3dD* pRenderer, struct MeshMaterial* pMaterials);
void vsr_3d_d_set_camera_ubo(Vsr3dD* pRenderer, m4 viewProjection);
VsrMeshRecord* vsr_3d_d_submit_model(Vsr3dD* pRenderer, struct DynamicModel* pModel, m4 transform, struct MeshMaterialInfo* aMeshMaterialInfo);
VsrMeshRecord vsr_3d_d_submit_meshes(Vsr3dD* pRenderer, struct DynamicMesh* pMesh, m4* aTransforms, size_t count, struct MeshMaterialInfo* pMeshMaterialInfo);
VsrMeshRecord vsr_3d_d_submit_mesh(Vsr3dD* pRenderer, struct DynamicMesh* pMesh, m4 transform, struct MeshMaterialInfo* pMeshMaterialInfo);
void vsr_3d_d_flush(Vsr3dD* pRenderer, VsrMeshRecord* pMeshRecords, size_t meshRecordsCount);

#endif // VSR_3D_H
