#include "SimpleSmallEcs.h"

#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>

static sse_i64 gWorldCount = 0;

#if defined(SSE_DEFINE_SIMPLIFIED_API)
static SseWorld* gpCachedWorld = {};
#endif // SSE_DEFINE_SIMPLIFIED_API

SseWorld*
sse_world_create()
{
    SseWorld sseWorld = {
        .Id = gWorldCount,

        .EntityStorage = {
            .NextId = SseIdRangeType_Entity_Begin,
        },

        .ComponentStorage = {
            .NextId = SseIdRangeType_Component_Begin,
        },

    };

    SseWorld* pSseWorld = memory_allocate(sizeof(SseWorld));
    *pSseWorld = sseWorld;

    ++gWorldCount;

#if defined(SSE_DEFINE_SIMPLIFIED_API)
    gpCachedWorld = pSseWorld;
#endif // SSE_DEFINE_SIMPLIFIED_API

    return pSseWorld;
}

void
sse_world_destroy(SseWorld* pSseWorld)
{
    array_free(pSseWorld->ComponentStorage.aComponentRecords);
    table_free(pSseWorld->ComponentStorage.hNameToId);

    array_free(pSseWorld->EntityStorage.aDeletedEntities);
    array_free(pSseWorld->EntityStorage.aEntityRecords);

    memory_free(pSseWorld);
}

sse_i32
sse_world_component_register(SseWorld* pSseWorld, const char* pComponentName, sse_size componentSize)
{
    sse_i32 isRegistered = sse_world_is_component_registered_by_name(
        pSseWorld, pComponentName);
    if (isRegistered)
    {
        return 0;
    }

    SseComponentId nextId = pSseWorld->ComponentStorage.NextId;
    shash_put(pSseWorld->ComponentStorage.hNameToId, pComponentName, nextId);

    SseComponentRecord componentRecord = {
        .pName = pComponentName,
        .Size = componentSize,
        .pData = NULL,
    };

    sse_i64 cnt = array_count(pSseWorld->ComponentStorage.aComponentRecords);
    if (cnt != sse_component_id_to_index(nextId))
    {
        GWARNING("[SimpleSmallEcs] cnt != nextId for component registration!\n");
        return 0;
    }

    array_push(pSseWorld->ComponentStorage.aComponentRecords, componentRecord);

    ++pSseWorld->ComponentStorage.NextId;

    return 1;
}

sse_i32
sse_world_is_component_registered_by_name(SseWorld* pSseWorld, const char* pComponentName)
{
    sse_i64 ind = shash_geti(pSseWorld->ComponentStorage.hNameToId, pComponentName);
    if (ind == -1)
        return 0;
    return 1;
}

sse_i32
sse_world_is_component_registered_by_id(SseWorld* pSseWorld, SseComponentId id)
{
    sse_i64 ind = sse_component_id_to_index(id);
    SseComponentId nextId = pSseWorld->ComponentStorage.NextId;
    if (id >= nextId)
    {
        return 0;
    }

    SseComponentRecord* aRecs = pSseWorld->ComponentStorage.aComponentRecords;
    if (ind >= array_count(aRecs) || ind < 0)
        return 0;

    if (aRecs[ind].pName == NULL)
        return 0;

    return 1;
}

SseComponentId
sse_component_name_to_id(SseWorld* pWorld, const char* pName)
{
    sse_i64 ind = shash_geti(pWorld->ComponentStorage.hNameToId, pName);
    if (ind == -1)
    {
        GWARNING("Component(%s) not registered!\n", pName);
        return SseIdRangeType_Component_Invalid;
    }

    ind = table_index(pWorld->ComponentStorage.hNameToId);

    SseComponentId id = pWorld->ComponentStorage.hNameToId[ind].Value;

    return id;
}

sse_i32
sse_component_validate_id(SseComponentId id)
{
    return ((id >= SseIdRangeType_Component_Begin) && (id <= SseIdRangeType_Component_End));
}

sse_i32
sse_entity_validate_id(SseEntityId id)
{
    return ((id >= SseIdRangeType_Entity_Begin) && (id <= SseIdRangeType_Entity_End));
}

sse_i64
sse_component_id_to_index(SseComponentId id)
{
#if SSE_FLAG_DEBUG == 1
    vassert(sse_component_validate_id(id) && "Component Id is not valid.");
#endif

    return id - SseIdRangeType_Component_Begin;
}

sse_i64
sse_entity_id_to_index(SseEntityId id)
{
#if SSE_FLAG_DEBUG == 1
    vassert(sse_entity_validate_id(id) && "Entity Id is not valid.");
#endif

    return id - SseIdRangeType_Entity_Begin;
}

SseComponentId
sse_component_index_to_id(sse_i64 id)
{
#if SSE_FLAG_DEBUG == 1
    vassert(sse_component_validate_id(id) && "Component Id is not valid.");
#endif

    return id + SseIdRangeType_Component_Begin;
}

SseEntityId
sse_entity_index_to_id(sse_i64 id)
{
#if SSE_FLAG_DEBUG == 1
    vassert(sse_entity_validate_id(id) && "Entity Id is not valid.");
#endif

    return id + SseIdRangeType_Entity_Begin;
}


SseEntity
sse_entity_create(SseWorld* pWorld)
{
    SseEntityId nextId = pWorld->EntityStorage.NextId;

    SseEntity sseEntity = {
        .Id = nextId,
    };

    SseEntityRecord emptyRec = {};
    sse_i64 cnt = array_count(pWorld->EntityStorage.aEntityRecords);
    if (cnt != sse_entity_id_to_index(nextId))
    {
        GWARNING("Entity id is not correct.\n");
        return (SseEntity) {-1};
    }

    array_push(pWorld->EntityStorage.aEntityRecords, emptyRec);

    ++pWorld->EntityStorage.NextId;

    return sseEntity;
}

void*
sse_entity_get_component_by_id(SseWorld* pWorld, SseEntity entity, SseComponentId compId)
{
    sse_size offset = 1;
    SseEntityRecord eRec = pWorld->EntityStorage.aEntityRecords[sse_entity_id_to_index(entity.Id)];
    array_foreach(eRec.aComponents,
                  if (item.ComponentId == compId)
                  {
                      offset = item.Offset;
                  }
        );

    if (offset == 1)
    {
#if SSE_FLAG_DEBUG == 1
        GWARNING("sse_entity_get_component: Failed to run %ld\n", compId);
#endif
        return NULL;
    }

    SseComponentRecord cRec = pWorld->ComponentStorage
        .aComponentRecords[sse_component_id_to_index(compId)];
    void* pComponentData = cRec.pData + offset;

    return pComponentData;
}

void*
sse_entity_get_component(SseWorld* pWorld, SseEntity entity, const char* pComponentName)
{
    sse_i64 ind = shash_geti(pWorld->ComponentStorage.hNameToId, pComponentName);
    if (ind == -1)
    {
#if SSE_FLAG_DEBUG == 1
        GWARNING("sse_entity_get_component: No component with name %s registered\n", pComponentName);
#endif
        return 0;
    }

    SseComponentId compId = pWorld->ComponentStorage.hNameToId[ind].Value;

    void* pComponentData = sse_entity_get_component_by_id(pWorld, entity, compId);

    return pComponentData;
}

sse_i32
sse_entity_set_component_by_id(SseWorld* pWorld, SseEntity entity, SseComponentId compId, void* pComponentData, sse_size componentSize)
{
#if SSE_FLAG_DEBUG == 1
    {
        sse_i32 ev = sse_entity_validate_id(entity.Id);
        SseComponentId cv = sse_component_validate_id(compId);
        if (!ev || !cv)
        {
            GWARNING("sse_entity_set_component_by_id: Id validation was failed e:%ld c:%ld!\n", entity.Id, compId);
            return 0;
        }

        if (pComponentData == NULL || componentSize == 0)
        {
            GWARNING("sse_entity_set_component_by_id: CompData is NULL or have size 0\n");
            return 0;
        }
    }

#endif

    // DOCS: Add data to component record array
    sse_i64 componentInd = sse_component_id_to_index(compId);
    SseComponentRecord* pCompRec = &pWorld->ComponentStorage.aComponentRecords[componentInd];
    sse_i64 componentsCnt = array_count(pCompRec->pData);
    array_push_void(pCompRec->pData, pComponentData, pCompRec->Size);

    // DOCS: Add new data to entity record
    sse_i64 entityInd = sse_entity_id_to_index(entity.Id);
    SseEntityRecord* pEntityRec = &pWorld->EntityStorage.aEntityRecords[entityInd];
    ComponentTuple tuple = {
        .ComponentId = compId,
        .Offset = componentsCnt * pCompRec->Size,
    };
    array_push(pEntityRec->aComponents, tuple);

    return 1;
}

sse_i32
sse_entity_set_component(SseWorld* pWorld, SseEntity entity, const char* pComponentName, void* pComponentData, sse_size componentSize)
{
    sse_i64 ind = shash_geti(pWorld->ComponentStorage.hNameToId, pComponentName);
    if (ind == -1)
    {
#if SSE_FLAG_DEBUG == 1
        GWARNING("No component with name %s registered\n", pComponentName);
#endif
        return 0;
    }

    SseComponentId compId = pWorld->ComponentStorage.hNameToId[ind].Value;

    sse_i32 isAdded = sse_entity_set_component_by_id(pWorld, entity, compId, pComponentData, componentSize);

    return isAdded;
}
