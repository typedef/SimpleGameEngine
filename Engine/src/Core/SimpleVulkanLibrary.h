#ifndef SIMPLE_VULKAN_LIBRARY_H
#define SIMPLE_VULKAN_LIBRARY_H

#include "vulkan_core.h"
#include <vulkan.h>

/*
  DOCS:
  For usage:
  #define SVL_CRASH_ON_ERROR 1
  #define SVL_X11 1
  #define SIMPLE_VULKAN_LIBRARY_IMPLEMENTATION
  #include "SimpleVulkanLibrary.h"

*/

#ifndef svl_i32
//DOCS: No Types declared, so declare one
#define svl_i8 char
#define svl_i32 int
#define svl_i64 long long int
#define svl_u8 unsigned char
#define svl_u32 unsigned int
#define svl_u64 unsigned long
#define svl_f32 float
#define svl_f64 double
#define svl_size svl_u64
#endif

#if !defined(svl_i64) || !defined(svl_u8) || !defined(svl_u32) || !defined(svl_u64) || !defined(svl_f32) || !defined(svl_f64) || !defined(svl_size)
#error "Some stupid lib include svl_i32 and now we don't have types, fix it!"
#endif

#define SvlTerminalRed(x) "\x1B[31m"x"\033[0m"
#define SvlTerminalMagneta(x) "\x1B[35m"x"\033[0m"
#define SvlTerminalYellow(x) "\x1B[93m"x"\033[0m"

typedef struct svl_v2i
{
    svl_i32 X;
    svl_i32 Y;
} svl_v2i;

typedef enum SvlErrorType
{
    SvlErrorType_None = 0,
    SvlErrorType_Instance_CantCreate,
    SvlErrorType_Instance_CantGetExts,
    SvlErrorType_Instance_CantCreateInstance,
    SvlErrorType_Instance_CantCreateDebugUtils,

    SvlErrorType_Surface_CantLoadCreate,
    SvlErrorType_Surface_CantCreate,

    SvlErrorType_PhysicalDevice_CantFind,
    SvlErrorType_SwapChainSupport_CantGet,

    SvlErrorType_Device,

    SvlErrorType_Queue,

    SvlErrorType_RenderPass,
    SvlErrorType_Pipeline,
    SvlErrorType_Pipeline_Layout,

    SvlErrorType_Image,
    SvlErrorType_ImageView,
    SvlErrorType_SwapImageView,

    SvlErrorType_NoRenderPass,

    SvlErrorType_Shader_NoPath,
    SvlErrorType_Shader_BytecodeInvalid,

    SvlErrorType_Count,
} SvlErrorType;


typedef struct _VkInstance
{
    svl_i8 IsDebugMode;
    SvlErrorType Error;
    svl_u32 VersionMajor;
    svl_u32 VersionMinor;
    svl_u32 VersionPatch;
    VkInstance Instance;
    VkDebugUtilsMessengerEXT DebugMessenger;
    VkAllocationCallbacks* pAllocator;
    void* pWindowBackend;
} _VkInstance;

typedef struct _VkPhysicalDevice
{
    SvlErrorType Error;
    VkPhysicalDevice PhysicalDevice;
    VkPhysicalDeviceProperties Properties;
    VkExtensionProperties* aExts;
} _VkPhysicalDevice;


typedef struct SvlExtent
{
    svl_i32 Width;
    svl_i32 Height;
} SvlExtent;

typedef struct SvlPaths
{
    const char* pShaderDirectory;
    const char* pRootDirectory;
} SvlPaths;

typedef struct SvlSettings
{
    svl_i8 IsDebug;
    svl_i8 IsVsync;
    // DOCS: Nothing Important
    svl_i8 IsDevicePropsPrintable;
    svl_u32 VulkanVersion;
    const char* pName;
    void* pWindowBackend;
    SvlExtent Extent;
    SvlPaths Paths;
} SvlSettings;

typedef struct SvlGraphSupport
{
    SvlErrorType Error;
    svl_i32 SurfaceFormatsCount;
    svl_i32 PresentModesCount;
    VkSurfaceCapabilitiesKHR SurfaceCapabilities;
    VkSurfaceFormatKHR* SurfaceFormats;
    VkPresentModeKHR* PresentModes;

    svl_i32 GraphicsIndex;
    svl_i32 PresentationIndex;
    // Compute?
} SvlGraphSupport;

typedef struct SvlDevice
{
    SvlErrorType Error;
    VkDevice Device;
} SvlDevice;

typedef struct SvlSync
{
    SvlErrorType Error;
    VkSemaphore ImageAvailableSemaphore;
    VkSemaphore RenderFinishedSemaphore;
    VkFence FrameFence;
} SvlSync;

typedef struct SvlSwapchainSettings
{
    svl_i8 IsVsync;
    svl_i8 IsDebug;
    VkSurfaceFormatKHR SurfaceFormat;
    SvlGraphSupport* pSupport;
    VkExtent2D Resolution;
    VkSwapchainKHR PrevSwapchain;
    _VkInstance _VkInstance;
    svl_i32 GraphicsQueueIndex;
    svl_i32 PresentQueueIndex;
    VkSurfaceKHR Surface;
    SvlDevice SvlDevice;
    _VkPhysicalDevice PhysicalDevice;
} SvlSwapchainSettings;

typedef struct SvlSwapchain
{
    SvlErrorType Error;
    VkSwapchainKHR Swapchain;
    VkSurfaceFormatKHR SurfaceFormat;
    VkExtent2D Resolution;
} SvlSwapchain;

typedef struct SvlCmd
{
    VkCommandPool Pool;
    VkCommandBuffer Buffer;
} SvlCmd;

typedef enum SvlAttachmentType
{
    SvlAttachmentType_Default = 0,
    SvlAttachmentType_ResolveMultisample,
    SvlAttachmentType_Depth,
    SvlAttachmentType_SwapChained,
} SvlAttachmentType;

typedef struct SvlAttachment
{
    SvlAttachmentType Type;
    VkAttachmentDescription Description;
    VkAttachmentReference Reference;
} SvlAttachment;

typedef struct SvlImageSettings
{
    svl_i32 Width;
    svl_i32 Height;
    svl_i32 Depth;
    svl_i32 MipLevels;
    VkSampleCountFlagBits SamplesCount;
    VkFormat Format;
    VkImageTiling ImageTiling;
    VkImageUsageFlags ImageUsageFlags;
    VkMemoryPropertyFlags MemoryPropertyFlags;
} SvlImageSettings;

typedef struct SvlSwapImageView
{
    VkImage Image;
    VkImageView View;
} SvlSwapImageView;
typedef struct SvlImageViewSettings
{
    SvlImageSettings ImageSettings;
    VkFormat Format;
    VkImageAspectFlags ImageAspectFlags;
    svl_i32 MipLevels;
} SvlImageViewSettings;

typedef struct SvlImageView
{
    VkImage Image;
    VkImageView View;
    VkDeviceMemory Memory;
} SvlImageView;


typedef enum SvlShaderType
{
    SvlShaderType_Vertex = 0,
    SvlShaderType_Fragment,
    SvlShaderType_Geometry,
    SvlShaderType_Compute,
    SvlShaderType_TesselationControl,
    SvlShaderType_TesselationEvaluation,
    SvlShaderType_Count,
} SvlShaderType;

static VkShaderStageFlagBits
svl_shader_type_to_flag(SvlShaderType type)
{
    switch (type)
    {

    case SvlShaderType_Vertex: return VK_SHADER_STAGE_VERTEX_BIT;
    case SvlShaderType_Fragment: return VK_SHADER_STAGE_FRAGMENT_BIT;
    case SvlShaderType_Geometry: return VK_SHADER_STAGE_GEOMETRY_BIT;
    case SvlShaderType_Compute: return VK_SHADER_STAGE_COMPUTE_BIT;
    case SvlShaderType_TesselationControl: return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
    case SvlShaderType_TesselationEvaluation: return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
    case SvlShaderType_Count: break;

    }

    return (VkShaderStageFlagBits) 0;
}

typedef struct SvlFrame
{
    svl_u32 ImageIndex;
} SvlFrame;

typedef struct SvlDescriptor
{
    char* pName;
    VkDescriptorType Type;
    svl_i64 Count;
    size_t Size;

    // DOCS: Private: Setup Internally In Shader
    svl_i32 Binding;
    // DOCS: Can be sampler or image
    VkDescriptorImageInfo* aImageInfos;
} SvlDescriptor;

typedef struct SvlShaderStage
{
    SvlShaderType Type;
    const char* ShaderSourcePath;
    SvlDescriptor aDescriptors[15];
    svl_i32 DescriptorsCount;
} SvlShaderStage;

typedef struct SvlShaderBytecode
{
    void* Bytecode;
    size_t Size;
} SvlShaderBytecode;

typedef struct SvlShaderGroupSettings
{
    const char* aPaths[SvlShaderType_Count];
} SvlShaderGroupSettings;

typedef struct SvlShaderGroup
{
    SvlShaderBytecode aBytecodes[SvlShaderType_Count];
} SvlShaderGroup;

typedef struct SvlBufferSettings
{
    svl_size Size;
    VkBufferUsageFlagBits Usage;
} SvlBufferSettings;

typedef struct SvlBuffer
{
    VkBuffer Staging;
    VkBuffer Gpu;
    VkDeviceMemory StagingMemory;
    VkDeviceMemory GpuMemory;
} SvlBuffer;


typedef enum SvlPipelineFlags
{
    SvlPipelineFlags_DepthStencil    = 1 << 1,
    SvlPipelineFlags_BackFaceCulling = 1 << 2,
    SvlPipelineFlags_IndirectDraw    = 1 << 3,
    //SvlPipelineFlags_Batched    = 1 << 4,
} SvlPipelineFlags;

typedef struct SvlShaderAttribute
{
    VkFormat Format;
    svl_i32 Offset;
    svl_i32 GroupBinding;
} SvlShaderAttribute;

typedef struct SvlUniformSettings
{
    svl_size Size;
    svl_size ItemSize;
    svl_i32 BindIndex;
    const char* pName;
    VkBufferUsageFlagBits Usage;
} SvlUniformSettings;

typedef struct SvlUniform
{
    svl_size Size;
    svl_size ItemSize;
    // note: we dont need this
    //svl_size Offset;
    svl_size Count;
    const char* pName;
    svl_i32 BindIndex;

    SvlBuffer Buffer;
} SvlUniform;


typedef struct SvlPipelineSettings
{
    // DOCS: Some easy flags
    SvlPipelineFlags Flags;

    // DOCS: Used RenderPass index
    svl_i32 RenderPassGroupIndex;

    // DOCS: Render pipeline description
    svl_i32 Stride;

    svl_i32 StagesCount;
    SvlShaderStage* aStages;

    svl_i32 AttributesCount;
    SvlShaderAttribute* aAttributes;

    VkPolygonMode PolygonMode;

    // DOCS: Some shader constants
    svl_i64 MaxInstanceCount;
} SvlPipelineSettings;

typedef struct SvlSsbo
{
    svl_size Size;
    svl_size ItemSize;
    svl_size Count;
    const char* pName;
    svl_i32 BindIndex;

    SvlBuffer Buffer;
} SvlSsbo;

typedef struct SvlBinding
{
    SvlUniform* aUniforms;
    SvlDescriptor* aDescriptors;
    SvlSsbo* aSsbos;

    VkDescriptorSet DescriptorSet;
    VkDescriptorSetLayout DescriptorSetLayout;
    VkDescriptorPool DescriptorPool;
} SvlBinding;

typedef struct SvlPipeline
{
    SvlBinding Binding;
    VkPipeline Pipeline;
    VkPipelineLayout PipelineLayout;

    SvlBuffer Vertex;
    SvlBuffer Index;
    svl_i64 IndicesCount;

    // DOCS: Dynamic/Private
    VkViewport Viewport;
    VkRect2D Scissor;
} SvlPipeline;

typedef enum SvlRenderPassType
{

    // DOCS: Prior, Previous Render Pass (Not final)
    SvlRenderPassType_Prior = 0,

    /*
      DOCS: RenderPass that use SwapChain images as image views for framebuffer.
      The main Render Target, something for rendering to the screen.
      Needs to be last render pass

      RenderScenePass (Prior) -> PostProccessPass(Prior) -> SwapChainedPass
    */
    SvlRenderPassType_SwapChained,

} SvlRenderPassType;

typedef struct SvlRenderPassSettings
{
    SvlRenderPassType Type;
    VkSubpassDependency Dependency;
    size_t AttachmentsCount;
    SvlAttachment aAttachments[16];
} SvlRenderPassSettings;

typedef struct SvlRenderPass
{
    svl_i32 IsDisabled;
    VkRenderPass RenderPass;
    SvlRenderPassType Type;
    VkSampleCountFlagBits Samples;
    SvlAttachment* aAttachments;

    // DOCS: CanBeUpdated: aFramebuffers, aVsaImageViews, aSwapChainedImageViews
    VkFramebuffer* aFramebuffers;
    //SvlImageView* aVsaImageViews;
    SvlSwapImageView* aSwapImageViews;
    SvlImageView* aImageViews;

    SvlPipeline** apPipelines;

    // DOCS: Dynamic/Private
    VkClearColorValue ClearColor;
} SvlRenderPass;

typedef struct SvlInstance
{
    svl_i32 IsInitialized;

    // DOCS: Internal Part
    _VkInstance VkInstance;
    VkSurfaceKHR VkSurface;
    _VkPhysicalDevice VkPhysicalDevice;
    SvlGraphSupport GraphSupport;
    // TODO: Remove struct, keep it raw
    SvlDevice Device;
    VkQueue PresentationQueue;
    VkQueue GraphicsQueue;
    // note: Some presentation stuff
    SvlSync Sync;
    SvlSwapchain Swapchain;
    SvlCmd Cmd;
    SvlPaths Paths;

    // DOCS: Dynamic
    SvlRenderPass* aRenderPasses;
} SvlInstance;

/*#######################
  DOCS(typedef): Svl Api
  #######################*/

SvlInstance svl_create(SvlSettings* pSettings);
void svl_destroy(SvlInstance* pInstance);


/*#######################
  DOCS(typedef): Buffer
  #######################*/

SvlBuffer svl_buffer_create(SvlInstance* pInstance, SvlBufferSettings set, SvlErrorType* pError);
void svl_buffer_set_data(SvlInstance* pInstance, SvlBuffer* pBuffer, void* pData, svl_size offset, svl_size size);
void svl_buffer_destroy(SvlInstance* pInstance, SvlBuffer* pBuffer);


/*#######################
  DOCS(typedef): Uniform
  #######################*/

SvlUniform svl_uniform_create(SvlInstance* pInstance, SvlUniformSettings set);
void svl_uniform_reset(SvlUniform* pUniform);
void svl_uniform_destroy(SvlInstance* pInstance, SvlUniform* pUniform);
void svl_uniform_set(SvlInstance* pInstance, SvlPipeline* pPipeline, SvlUniform* pUniform, void* pData, svl_size offset, svl_size size);

/*#######################
  DOCS(typedef): RenderPass
  #######################*/
SvlRenderPass svl_render_pass_create(SvlInstance* pInstance, SvlRenderPassSettings settings, SvlErrorType* pError);
void svl_render_pass_update(SvlInstance* pInstance, SvlRenderPass* pRenderPass, SvlFrame* pFrame, SvlErrorType* pError);

/*#######################
  DOCS(typedef): Pipeline
  #######################*/
SvlPipeline* svl_pipeline_create(SvlInstance* pInstance, SvlPipelineSettings settings, SvlErrorType* pError);

/*#######################
  DOCS(typedef): Svl Public Api
  #######################*/

void svl_set_clear_color(SvlRenderPass* pRenderPass, svl_f32 r, svl_f32 g, svl_f32 b, svl_f32 a);
void svl_set_viewport(SvlPipeline* pPipeline, svl_v2i from, svl_v2i size);
void svl_set_scissor(SvlPipeline* pPipeline, svl_v2i from, svl_v2i to);


/*#######################
  DOCS(typedef): Cmd
  #######################*/

VkCommandBuffer svl_cmd_begin(SvlInstance* pInstance);
void svl_cmd_end(SvlInstance* pInstance, VkCommandBuffer vkCmdBuffer);


/*#######################
  DOCS(typedef): Frame
  #######################*/

void svl_frame_start(SvlInstance* pInstance, SvlFrame* pFrame);
void svl_frame_end(SvlInstance* pInstance, SvlFrame* pFrame);

/*#######################
  DOCS(typedef): Helpers Api
  #######################*/

const char* svl_result_to_string(VkResult vkResult);
svl_i32 svl_result_check(VkResult vkResult, const char* pMessage);

// ????
// ?note: where to place
SvlImageView svl_image_view_create(SvlInstance* pInstance, SvlImageViewSettings settings, SvlErrorType* pError);



#if defined(SIMPLE_VULKAN_LIBRARY_IMPLEMENTATION)

#if !defined (SVL_X11)
 #define SVL_X11 0
#endif

#if !defined (SVL_XCB)
 #define SVL_XCB 0
#endif

#if !defined (SVL_WIN32)
 #define SVL_WIN32 0
#endif

#if SVL_X11 == 1
 #define VK_USE_PLATFORM_XLIB_KHR
 #define SVL_LINUX_PLATFORM
#elif SVL_XCB == 1
 #error "Not impl"
#elif SVL_WIN32 == 1
 #error "Not impl"
 #define SVL_WINDOWS_PLATFORM
#else
//#error "Not supported platform!!!"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#if defined(SVL_LINUX_PLATFORM)
#include <sys/stat.h>
#elif defined(SVL_WINDOWS_PLATFORM)
#endif

#if SVL_X11 == 1
#include <X11/Xlib.h>

typedef struct SvlX11Backend
{
    Display* pDisplay;
    Window Window;
} SvlX11Backend;

#elif SVL_XCB == 1
#error "Not impl"
#elif SVL_WIN32 == 1
#error "Not impl"
#else
#error "Platform not supported, or you fogot to declare platform flag, like #define SVL_X11 1!"
#endif


#if SVL_CRASH_ON_ERROR == 1
#define svl_guard(cond) _svl_guard(cond, #cond, __LINE__, __FILE__)
#else
#define svl_guard(cond) ((void)(cond))
#endif
void _svl_guard(svl_i32 condition, const char* pCond, svl_i32 line, const char* pFile);
#define SvlArrayCount(arr) (sizeof(arr) / sizeof(*arr))

svl_i32 _svl_string_compare(const char* s0, const char* s1);
svl_i64 _svl_string_length(const char* p);
char* _svl_path_combine(const char* left, const char* right);
const char* _svl_path_get_name(const char* p);
char* _svl_path_get_name_wo_ext(const char* p);
const char* _svl_path_get_ext(const char* p);
char _svl_char_to_upper(char c);
char* _svl_string(const char* s);
char* _svl_string_concat(const char* p0, const char* p1);
void* _svl_file_read_bytes(const char* pPath, svl_i64* pLength);
svl_i32 _svl_path_file_exist(const char* path);
svl_i32 _svl_path_directory_exist(const char* pPath);
svl_i32 _svl_directory_create(const char* pPath);
svl_size _svl_path_get_last_mod_time(const char* pPath);
char svl_char_to_upper(char c);


/*#######################
  DOCS(typedef): Some utils/structures like svl_array_push
  #######################*/
typedef struct SvlArrayHeader
{
    svl_size ItemSize;
    svl_i64 Count;
    svl_i64 Capacity;
    void* pBuffer;
} SvlArrayHeader;

#define svl_array_header(b) ((SvlArrayHeader*) (((char*)b) - sizeof(SvlArrayHeader)))
#define svl_array_count(b) ((b != NULL) ? svl_array_header(b)->Count : 0)
#define svl_array_capacity(b) ((b != NULL) ? svl_array_header(b)->Capacity : 0)

static void*
svl_array_grow(const void* pBuffer, svl_size itemSize, svl_size cnt)
{
    if (pBuffer != NULL)
    {
        svl_size newCapacity = 2 * svl_array_capacity(pBuffer) + 1;
        svl_size newSize = newCapacity * itemSize + sizeof(SvlArrayHeader);
        SvlArrayHeader* pOld = svl_array_header(pBuffer);

        SvlArrayHeader* pHdr = NULL;
        pHdr = (SvlArrayHeader*) malloc(newSize);
        pHdr->pBuffer = ((char*)pHdr) + sizeof(SvlArrayHeader);
        pHdr->ItemSize = itemSize;
        pHdr->Count = pOld->Count;
        pHdr->Capacity = newCapacity;

        size_t copySize = pOld->Count * itemSize;
        memcpy(pHdr->pBuffer, pBuffer, copySize);

        free(pOld);

        return pHdr->pBuffer;
    }

    SvlArrayHeader* pHdr = (SvlArrayHeader*) malloc(cnt * itemSize + sizeof(SvlArrayHeader));
    pHdr->pBuffer = (((void*)pHdr) + sizeof(SvlArrayHeader));
    pHdr->Count = 0;
    pHdr->Capacity = cnt;
    pHdr->ItemSize = itemSize;

    return pHdr->pBuffer;
}

#define svl_array_free(b) if (b) free(svl_array_header(b));

#define svl_array_push(b, ...)						\
    ({									\
        if ((b) == NULL || svl_array_count(b) >= svl_array_capacity(b))	\
        {								\
            (b) = svl_array_grow((const void*)b, sizeof(__typeof__(b[0])), 1); \
        }								\
                                                                        \
        b[svl_array_count(b)] = (__VA_ARGS__);				\
        ++svl_array_header(b)->Count;					\
    })
#define svl_array_reserve(b, cnt) ({b = svl_array_grow((const void*)b, sizeof(__typeof__(b[0])), cnt);})

/*#######################
  DOCS(typedef): Internal Structures
  #######################*/

typedef struct _VkInstanceCreateInfo
{
    const char* pName;
    svl_u32 VulkanVersion;
    svl_i32 IsDebug;
    void* pWindowBackend;
} _VkInstanceCreateInfo;

typedef struct _VkSurface
{
    SvlErrorType Error;
    VkSurfaceKHR Surface;
} _VkSurface;


/*#######################
  DOCS(typedef): Forward Declaration's
  #######################*/

void _svl_image_view_create(SvlInstance* pInstance, VkImage vkImage, VkFormat vkFormat, VkImageAspectFlags vkImageAspectFlags, svl_i32 mipLevels, VkImageView* pImageView, SvlErrorType* pError);
svl_i32 svl_find_memory_type_index(SvlInstance* pInstance, svl_u32 typeBit, VkMemoryPropertyFlags vkMemoryPropertyFlags);


/*#######################
  DOCS(typedef): Platform dependent stuff
  #######################*/

static char**
_svl_get_required_extensions(svl_i32 isDebug)
{
#if 0
    u32 extCount = 5;
    char** result = NULL;
    char** aExts = window_get_required_exts(&extCount);

    svl_array_push(result, VK_EXT_DEBUG_UTILS_EXTENSION_NAME);

    for (svl_i32 i = 0; i < extCount; ++i)
    {
        char*item=aExts[i];

        printf("Ext: %s\n", item);
        svl_array_push(result, item);
    }

    return result;
#else

    char** aExts = NULL;

    const char* pSurfaceExtName = NULL;

#if SVL_X11 == 1
    //pSurfaceExtName = "VK_KHR_xcb_surface";
    pSurfaceExtName = "VK_KHR_xlib_surface";
#elif SVL_XCB == 1
    pSurfaceExtName = "VK_KHR_xcb_surface";
#elif SVL_WIN32 == 1
    pSurfaceExtName = "VK_KHR_win32_surface";
#else
#error "Platform not supported, or you fogot to declare platform flag, like #define SVL_X11 1!"
#endif

    svl_u32 count;
    vkEnumerateInstanceExtensionProperties(NULL, &count, NULL);
    VkExtensionProperties* aExtProperties = malloc(sizeof(VkExtensionProperties)*count);

    VkResult enumResult = vkEnumerateInstanceExtensionProperties(NULL, &count, aExtProperties);
    if (enumResult != VK_SUCCESS)
    {
        free(aExtProperties);
        return NULL;
    }

    for (svl_i32 i = 0; i < count; ++i)
    {
        VkExtensionProperties prop = aExtProperties[i];
        if (strcmp(prop.extensionName, "VK_KHR_surface") == 0)
        {
            svl_array_push(aExts, (char*)"VK_KHR_surface");
        }
        // DOCS: include only if exist in extProps
        else if (strcmp(prop.extensionName, pSurfaceExtName) == 0)
        {
            svl_array_push(aExts, (char*)pSurfaceExtName);
        }
    }

    if (isDebug)
    {
        svl_array_push(aExts, VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }

    free(aExtProperties);

    return aExts;
#endif
}

/*
  DOCS: Internal functions, client are should *NOT* be interersted in this
*/

static VkBool32
vk_validation_layers_error_callback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageTypes,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData)
{
    if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
    {
        printf(SvlTerminalMagneta("[VULKAN INFO]"));
    }
    else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
    {
        printf(SvlTerminalYellow("[VULKAN WARNING]"));
    }
    else if (messageSeverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
    {
        printf(SvlTerminalRed("[VULKAN ERROR] "));
    }

    printf("%s \n", pCallbackData->pMessage);

    return VK_TRUE;
}

_VkInstance
_svl_instance_create(_VkInstanceCreateInfo createInfo)
{
    if (createInfo.pName == NULL || createInfo.pWindowBackend == NULL)
    {
        svl_guard(createInfo.pName != NULL);
        svl_guard(createInfo.pWindowBackend != NULL);
        return (_VkInstance) { .Error = SvlErrorType_Instance_CantCreate };
    }

    svl_u32 version;
    VkResult getVersionResult = vkEnumerateInstanceVersion(&version);

    if (svl_result_check(getVersionResult, "Failed vkEnumerateInstanceVersion"))
    {
        return (_VkInstance) {
            .Error = SvlErrorType_Instance_CantCreate,
        };
    }

    svl_u32 vulkanVersion = createInfo.VulkanVersion;
    if (vulkanVersion == 0)
    {
        vulkanVersion = VK_MAKE_API_VERSION(0, 1, 3, 0);
    }

    char** aRequiredExts = _svl_get_required_extensions(createInfo.IsDebug);
    if (aRequiredExts == NULL)
    {
        return (_VkInstance) {
            .Error = SvlErrorType_Instance_CantGetExts,
        };
    }

    // DOCS: Allocators callbacks
    // TODO: Provide interface for setting callback from user space
    VkAllocationCallbacks* pAllocatorCallbacks = NULL;

    /* // NOTE(typedef): create validation layers */
    char** validationLayerNames = NULL;
    VkDebugUtilsMessengerCreateInfoEXT debug = {};
    if (createInfo.IsDebug)
    {
        svl_array_push(validationLayerNames, "VK_LAYER_KHRONOS_validation");

        VkDebugUtilsMessageSeverityFlagsEXT severity = /* VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | */
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        VkDebugUtilsMessageTypeFlagsEXT messageType =
            VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;

        debug = (VkDebugUtilsMessengerCreateInfoEXT) {
            .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
            .flags = 0,
            .messageSeverity = severity,
            .messageType = messageType,
            .pfnUserCallback = vk_validation_layers_error_callback,
            .pUserData = NULL
        };
    }

    VkDebugUtilsMessengerCreateInfoEXT* pDebug = NULL;
    if (createInfo.IsDebug)
        pDebug = &debug;

    VkApplicationInfo applicationInfo = {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pNext = NULL,
        .pApplicationName = createInfo.pName,
        .applicationVersion = VK_MAKE_API_VERSION(0, 1, 0, 0),
        .pEngineName = createInfo.pName,
        .engineVersion = VK_MAKE_API_VERSION(0, 1, 0, 0),
        .apiVersion = vulkanVersion
    };

    VkInstanceCreateInfo instanceCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pNext = pDebug,
        .flags = 0,
        .pApplicationInfo = &applicationInfo,
        .enabledLayerCount = svl_array_count(validationLayerNames),
        .ppEnabledLayerNames = (const char* const*)validationLayerNames,
        .enabledExtensionCount = svl_array_count(aRequiredExts),
        .ppEnabledExtensionNames = (const char* const*)aRequiredExts
    };

    VkInstance vkInstance;
    VkResult createInstanceResult = vkCreateInstance(&instanceCreateInfo, pAllocatorCallbacks, &vkInstance);
    if (svl_result_check(createInstanceResult, "Can't create instance"))
    {
        return (_VkInstance) {
            .Error = SvlErrorType_Instance_CantCreateInstance,
        };
    }

    VkDebugUtilsMessengerEXT messenger;

    if (createInfo.IsDebug)
    {
        PFN_vkCreateDebugUtilsMessengerEXT vkCreateDebugUtilsMessenger =
            (PFN_vkCreateDebugUtilsMessengerEXT)
            vkGetInstanceProcAddr(vkInstance,
                                  "vkCreateDebugUtilsMessengerEXT");

        VkResult createResult = vkCreateDebugUtilsMessenger(vkInstance, pDebug, pAllocatorCallbacks, &messenger);

        if (svl_result_check(createResult, "Can't create debug utils"))
        {
            return (_VkInstance) {
                .Error = SvlErrorType_Instance_CantCreateDebugUtils,
            };
        }
    }

    _VkInstance _vkInstance = (_VkInstance) {
        .IsDebugMode = createInfo.IsDebug,
        .Error = SvlErrorType_None,
        .VersionMajor = VK_API_VERSION_MAJOR(version),
        .VersionMinor = VK_API_VERSION_MINOR(version),
        .VersionPatch = VK_API_VERSION_PATCH(version),
        .Instance = vkInstance,
        .pAllocator = pAllocatorCallbacks,
        .DebugMessenger = messenger,
        .pWindowBackend = createInfo.pWindowBackend,
    };

    return _vkInstance;

}

#if SVL_X11 == 1
typedef VkFlags VkXlibSurfaceCreateFlagsKHR;

typedef struct VkXlibSurfaceCreateInfoKHR
{
    VkStructureType             sType;
    const void*                 pNext;
    VkXlibSurfaceCreateFlagsKHR flags;
    Display*                    dpy;
    Window                      window;
} VkXlibSurfaceCreateInfoKHR;

typedef VkResult (*PFN_vkCreateXlibSurfaceKHR)(VkInstance, const VkXlibSurfaceCreateInfoKHR*, const VkAllocationCallbacks*, VkSurfaceKHR*);

#elif SVL_WIN32 == 1

#endif // SVL_X11; SVL_WIN32

_VkSurface
_svl_surface_create(_VkInstance* pVkInstance)
{
    VkSurfaceKHR vkSurface;

#if SVL_X11 == 1234
    window_create_surface(pVkInstance->Instance, NULL, &vkSurface);
    return (_VkSurface) {.Surface = vkSurface};

#elif SVL_X11 == 1

    SvlX11Backend* pWindowBackend = (SvlX11Backend*) pVkInstance->pWindowBackend;

    VkXlibSurfaceCreateInfoKHR surfaceCI = {
        .sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR,
        .dpy = pWindowBackend->pDisplay,
        .window = pWindowBackend->Window,
    };

    // vkCreateXlibSurfaceKHR();

    PFN_vkCreateXlibSurfaceKHR vkCreateXlibSurfaceKHR =
        (PFN_vkCreateXlibSurfaceKHR)
        vkGetInstanceProcAddr(pVkInstance->Instance, "vkCreateXlibSurfaceKHR");
    if (!vkCreateXlibSurfaceKHR)
    {
        return (_VkSurface) { .Error = SvlErrorType_Surface_CantLoadCreate };
    }

    VkResult createSurfaceResult = vkCreateXlibSurfaceKHR(pVkInstance->Instance, &surfaceCI, pVkInstance->pAllocator, &vkSurface);
    if (svl_result_check(createSurfaceResult, "Can't create surface!"))
    {
        return (_VkSurface) { .Error = SvlErrorType_Surface_CantCreate };
    }

    return (_VkSurface) {.Surface = vkSurface};

#elif SVL_XCB == 1

    return (_VkSurface) {};

#elif SVL_WIN32 == 1

    return (_VkSurface) {};

#else
#error "Platform not supported, or you fogot to declare platform flag, like #define SVL_X11 1!"
#endif

}

_VkPhysicalDevice
_svl_physical_device_create(_VkInstance vkInstance)
{
    VkInstance instance = vkInstance.Instance;

    /*
      DOCS(typedef):Pick physical device
    */
    svl_u32 physicalDevicesCount;
    VkResult enumeratePhysicalDeviceResult =
        vkEnumeratePhysicalDevices(instance, &physicalDevicesCount, NULL);

    if (svl_result_check(enumeratePhysicalDeviceResult, "Physical device enumeration error") || physicalDevicesCount <= 0)
    {
        return (_VkPhysicalDevice) { .Error = SvlErrorType_PhysicalDevice_CantFind };
    }

    if (enumeratePhysicalDeviceResult == VK_ERROR_INITIALIZATION_FAILED)
    {
        printf("Can't enemerate physical devices, maybe installable client driver (ICD) is not installed on your machine!\n");
        return (_VkPhysicalDevice) { .Error = SvlErrorType_PhysicalDevice_CantFind };
    }

    VkPhysicalDevice* aPhysicalDevices = malloc(physicalDevicesCount * sizeof(VkPhysicalDevice));
    enumeratePhysicalDeviceResult =
        vkEnumeratePhysicalDevices(instance, &physicalDevicesCount, aPhysicalDevices);
    if (svl_result_check(enumeratePhysicalDeviceResult, "Problem with getting physical device throught vkEnumeratePhysicalDevices"))
    {
        return (_VkPhysicalDevice) { .Error = SvlErrorType_PhysicalDevice_CantFind };
    }

    VkPhysicalDevice vkPhysicalDevice = VK_NULL_HANDLE;
    VkPhysicalDeviceProperties vkProperties;

    svl_i32 isPicked = 0;
    for (svl_i32 i = 0; i < physicalDevicesCount; ++i)
    {
        VkPhysicalDeviceProperties physicalDeviceProperties;
        VkPhysicalDevice device = aPhysicalDevices[i];
        vkGetPhysicalDeviceProperties(device, &physicalDeviceProperties);

        VkPhysicalDeviceFeatures deviceFeatures;
        vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

        if ((physicalDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
            && deviceFeatures.samplerAnisotropy
            && deviceFeatures.multiDrawIndirect)
        {
            vkPhysicalDevice = device;
            vkProperties = physicalDeviceProperties;
            isPicked = 1;
            break;
        }
    }

    if (!isPicked || vkPhysicalDevice == VK_NULL_HANDLE)
    {
        return (_VkPhysicalDevice) { .Error = SvlErrorType_PhysicalDevice_CantFind };
    }

    svl_u32 physicalDeviceExtCount;
    VkResult getPhysicalDeviceExtResult = vkEnumerateDeviceExtensionProperties(vkPhysicalDevice, NULL, &physicalDeviceExtCount, NULL);
    if (svl_result_check(getPhysicalDeviceExtResult, "Can't enumerate vkEnumerateDeviceExtensionProperties"))
    {
        return (_VkPhysicalDevice) { .Error = SvlErrorType_PhysicalDevice_CantFind };
    }

    VkExtensionProperties* aExts = NULL;
    svl_array_reserve(aExts, physicalDeviceExtCount);

    getPhysicalDeviceExtResult = vkEnumerateDeviceExtensionProperties(vkPhysicalDevice, NULL, &physicalDeviceExtCount, aExts);
    if (svl_result_check(getPhysicalDeviceExtResult, "Can't enumerate vkEnumerateDeviceExtensionProperties"))
    {
        return (_VkPhysicalDevice) { .Error = SvlErrorType_PhysicalDevice_CantFind };
    }

    svl_array_header(aExts)->Count = physicalDeviceExtCount;

    free(aPhysicalDevices);

    _VkPhysicalDevice phDev = {
        .PhysicalDevice = vkPhysicalDevice,
        .Properties = vkProperties,
        .aExts = aExts,
    };

    return phDev;
}

static const char*
_svl_physical_device_to_string(VkPhysicalDeviceType type)
{
    switch (type)
    {
    case VK_PHYSICAL_DEVICE_TYPE_OTHER: return "Other";
    case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: return "Integrated GPU";
    case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: return "GPU";
    case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: return "Virtual GPU";
    case VK_PHYSICAL_DEVICE_TYPE_CPU: return "CPU";
    default: return "";
    }
}

void
_svl_physical_device_print(VkPhysicalDeviceProperties props)
{
    printf("[Physical Device] { \n\t"
           ".apiVersion = %d.%d.%d ,\n\t"
           ".driverVersion = %d.%d.%d,\n\t"
           ".vendorID = %d,\n\t"
           ".deviceID = %d,\n\t"
           ".deviceType = %s,\n\t"
           ".deviceName = %s\n}\n",

           // api version
           VK_API_VERSION_MAJOR(props.apiVersion),
           VK_API_VERSION_MINOR(props.apiVersion),
           VK_API_VERSION_PATCH(props.apiVersion),

           // driver version
           VK_API_VERSION_MAJOR(props.driverVersion),
           VK_API_VERSION_MINOR(props.driverVersion),
           VK_API_VERSION_PATCH(props.driverVersion),

           props.vendorID,
           props.deviceID,
           _svl_physical_device_to_string(props.deviceType),
           props.deviceName
        );

}


static SvlGraphSupport
_svl_swap_chain_support_details(VkPhysicalDevice pDevice,
                                VkSurfaceKHR surface)
{
    SvlGraphSupport svlSupport = { };

    VkResult getSurfaceCapabilitiesResult =
        vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
            pDevice,
            surface,
            &svlSupport.SurfaceCapabilities);
    //vkPrintSurfaceCapabilitiesLine(supportDetails.SurfaceCapabilities);

    // NOTE(typdef): get surface formats
    svl_u32 formatsCount;
    VkResult getSurfaceFormatResult =
        vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice, surface, &formatsCount, NULL);
    svlSupport.SurfaceFormats = malloc(formatsCount * sizeof(VkSurfaceFormatKHR));

    getSurfaceFormatResult =
        vkGetPhysicalDeviceSurfaceFormatsKHR(pDevice, surface, &formatsCount, svlSupport.SurfaceFormats);

    if (svl_result_check(getSurfaceFormatResult, "Failed to get Surface Format Result"))
    {
        return (SvlGraphSupport) { .Error = SvlErrorType_SwapChainSupport_CantGet };
    }

    // NOTE(typedef): get surface modes
    svl_u32 presentModesCount;
    VkResult getSurfaceModeResult =
        vkGetPhysicalDeviceSurfacePresentModesKHR(
            pDevice,
            surface,
            &presentModesCount,
            NULL);
    if (svl_result_check(getSurfaceFormatResult, "Failed get PresentMode"))
    {
        return (SvlGraphSupport) { .Error = SvlErrorType_SwapChainSupport_CantGet };
    }

    svlSupport.PresentModes = malloc(sizeof(VkPresentModeKHR) * presentModesCount);

    getSurfaceModeResult =
        vkGetPhysicalDeviceSurfacePresentModesKHR(
            pDevice,
            surface,
            &presentModesCount,
            svlSupport.PresentModes);
    if (svl_result_check(getSurfaceFormatResult, "Failed get PresentMode"))
    {
        return (SvlGraphSupport) { .Error = SvlErrorType_SwapChainSupport_CantGet };
    }

    svlSupport.SurfaceFormatsCount = formatsCount;
    svlSupport.PresentModesCount = presentModesCount;

    return svlSupport;
}

void
_svl_swap_chain_support_details_destroy(SvlGraphSupport support)
{
    free(support.PresentModes);
    free(support.SurfaceFormats);
}

SvlGraphSupport
svl_queue_indices(VkSurfaceKHR surface, _VkPhysicalDevice physicalDevice)
{
    VkPhysicalDevice phDev = physicalDevice.PhysicalDevice;

    /*
      DOCS(typedef): Get Queue's
    */
    svl_u32 familyPropertyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(phDev, &familyPropertyCount, NULL);
    VkQueueFamilyProperties* familyProperties = NULL;
    svl_array_reserve(familyProperties, familyPropertyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(phDev, &familyPropertyCount, familyProperties);

    printf("Families count: %d\n", familyPropertyCount);

    svl_i32 graphicsIndex = -1,
        presentationIndex = -1;

    SvlGraphSupport support;
    for (svl_i32 i = 0; i < familyPropertyCount; ++i)
    {
        char* flagAsStr = NULL;
        VkQueueFamilyProperties familyProperty = familyProperties[i];

        VkBool32 isSurfaceKhrSupported;
        VkResult getSupportForKhrResult =
            vkGetPhysicalDeviceSurfaceSupportKHR(phDev, i, surface, &isSurfaceKhrSupported);
        if (svl_result_check(getSupportForKhrResult, "vkGetPhysicalDeviceSurfaceSupportKHR"))
        {
            return (SvlGraphSupport) { .Error = SvlErrorType_SwapChainSupport_CantGet };
        }

        support =
            _svl_swap_chain_support_details(phDev, surface);

        svl_i8 isSwapChainSupported = support.SurfaceFormatsCount
            && support.PresentModesCount;
        if (!isSwapChainSupported)
        {
            _svl_swap_chain_support_details_destroy(support);
            continue;
        }

        if (familyProperty.queueFlags & VK_QUEUE_GRAPHICS_BIT
            && familyProperty.queueFlags & VK_QUEUE_COMPUTE_BIT)
        {
            graphicsIndex = i;
        }

        if (isSurfaceKhrSupported == VK_TRUE)
        {
            presentationIndex = i;
        }

        if (graphicsIndex != -1 && presentationIndex != -1)
        {
            break;
        }
    }

    /*
      DOCS(typedef): yes, on most gpu's this queue indices is the same
    */
    if (graphicsIndex < 0 || presentationIndex < 0)
    {
        _svl_swap_chain_support_details_destroy(support);
        return (SvlGraphSupport) { .Error = SvlErrorType_SwapChainSupport_CantGet };
    }

    support.GraphicsIndex = graphicsIndex;
    support.GraphicsIndex = presentationIndex;

    return support;
}

SvlDevice
_svl_device_create(_VkInstance _vkInstance, _VkPhysicalDevice physicalDevice, SvlGraphSupport graphSupport)
{
    VkExtensionProperties* aExts = physicalDevice.aExts;
    VkPhysicalDevice phDevice = physicalDevice.PhysicalDevice;
    svl_i32 graphicsIndex = graphSupport.GraphicsIndex;
    svl_i32 presentIndex = graphSupport.PresentationIndex;
    VkAllocationCallbacks* pAllocator = _vkInstance.pAllocator;

    /*
      DOCS(typedef): Create (Logical) Device
    */
    const char* deviceExts[] = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };

    for (svl_i32 d = 0; d < SvlArrayCount(deviceExts); ++d)
    {
        const char* ext = deviceExts[d];

        svl_i32 ind = -1, extCnt = svl_array_count(aExts);
        for (svl_i32 e = 0; e < extCnt; ++e)
        {
            VkExtensionProperties prop = aExts[e];

            if (_svl_string_compare(prop.extensionName, ext))
            {
                ind = e;
                break;
            }
        }

        // note: We need it all
        if (ind == -1)
        {
            return (SvlDevice) { .Error = SvlErrorType_Device };
        }
    }

    // TODO(typedef): Check if video card support it
    // TODO(typedef): Move before physical device creation
    VkPhysicalDeviceFeatures physcialDeviceFeatures = {
        /*
          DOCS(typedef): we need special features
        */

        // DOCS(typedef): For antisotropic filtering for textures x8, x16
        .samplerAnisotropy = VK_TRUE,

        // DOCS(typedef): For seting descriptors inside render pass, mb we can avoid this
        //.shaderSampledImageArrayDynamicIndexing = VK_TRUE,

        // DOCS(typedef): For enabling none VK_POLYGON_MODE_FILL fill mode
        .fillModeNonSolid = VK_TRUE,

        // DOCS(typedef): MultiDrawIndirect feature for modern renderer
        .multiDrawIndirect = VK_TRUE
    };

    /*
      DOCS(typedef): Create info for creating Queue's.
      As Queue's created within VkDevice we need make CreateInfo
      structs for them as well, before creating VkDevice.
    */
    svl_f32 queuePriority = 1.0f;
    VkDeviceQueueCreateInfo* queuesCreateInfos = NULL;
    VkDeviceQueueCreateInfo graphicsQueueCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .queueFamilyIndex = graphicsIndex,
        .queueCount = 1,
        .pQueuePriorities = &queuePriority
    };
    svl_array_push(queuesCreateInfos, graphicsQueueCreateInfo);

    /*
      DOCS(typedef): If .GraphicsIndex == .PresentationIndex then
      we need to pass it only once, if in app we have differentiation
    */
    if (graphicsIndex != presentIndex)
    {
        VkDeviceQueueCreateInfo presentationQueueCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .pNext = NULL,
            .flags = 0,
            .queueFamilyIndex = presentIndex,
            .queueCount = 1,
            .pQueuePriorities = &queuePriority
        };
        svl_array_push(queuesCreateInfos, presentationQueueCreateInfo);
    }

    VkDeviceCreateInfo deviceCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = NULL,//&physicalFeatures2,
        .flags = 0,
        .queueCreateInfoCount = svl_array_count(queuesCreateInfos),
        .pQueueCreateInfos = queuesCreateInfos,
        .enabledExtensionCount = SvlArrayCount(deviceExts),
        .ppEnabledExtensionNames = deviceExts,
        .pEnabledFeatures = &physcialDeviceFeatures
    };

    VkDevice vkDevice;
    VkResult deviceCreateResult = vkCreateDevice(phDevice, &deviceCreateInfo, pAllocator, &vkDevice);
    if (svl_result_check(deviceCreateResult, "// Failed Device Creation //")
        || vkDevice == VK_NULL_HANDLE)
    {
        return (SvlDevice) { .Error = SvlErrorType_Device };
    }

    SvlDevice svlDevice = {
        .Device = vkDevice
    };

    svl_array_free(queuesCreateInfos);

    return svlDevice;
}

SvlErrorType
_svl_queue_create(SvlDevice svlDevice, svl_i32 queueFamilyIndex, VkQueue* pQueue)
{
    VkDevice device = svlDevice.Device;

    vkGetDeviceQueue(device, queueFamilyIndex, 0, pQueue);
    if (pQueue != VK_NULL_HANDLE)
    {
        return SvlErrorType_None;
    }

    return SvlErrorType_Queue;
}

SvlSync
svl_sync_create(_VkInstance _vkInstance, SvlDevice svlDevice)
{
    SvlSync svlSync = {};

    VkDevice device = svlDevice.Device;
    VkAllocationCallbacks* pAllocator = _vkInstance.pAllocator;

    VkSemaphoreCreateInfo semaphoreCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0
    };

    VkFenceCreateInfo frameFenceCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .pNext = NULL,
        //NOTE(typedef): this will become unsignaled at the 2 line (vkResetFences) of the render loop
        .flags = VK_FENCE_CREATE_SIGNALED_BIT
    };

    VkResult createImageSemaphoreResult =
        vkCreateSemaphore(device, &semaphoreCreateInfo, pAllocator, &svlSync.ImageAvailableSemaphore);
    if (svl_result_check(createImageSemaphoreResult, "Failed to create semaphore!"))
    {
        return (SvlSync) {.Error = 1};
    }

    VkResult createRenderSemaphoreResult =
        vkCreateSemaphore(device, &semaphoreCreateInfo, pAllocator, &svlSync.RenderFinishedSemaphore);
    if (svl_result_check(createRenderSemaphoreResult, "Failed to create semaphore!"))
    {
        return (SvlSync) {.Error = 1};
    }

    VkResult createFrameFenceResult =
        vkCreateFence(device, &frameFenceCreateInfo, pAllocator, &svlSync.FrameFence);
    if (svl_result_check(createFrameFenceResult, "Failed to create fence!"))
    {
        return (SvlSync) {.Error = 1};
    }

    return svlSync;
}

SvlSwapchain
svl_swapchain_create(SvlSwapchainSettings settings)
{
    VkPresentModeKHR prefPresentMode;
    if (settings.IsVsync)
        prefPresentMode = VK_PRESENT_MODE_FIFO_KHR;
    else
        prefPresentMode = VK_PRESENT_MODE_IMMEDIATE_KHR;

    SvlGraphSupport* pSupport = settings.pSupport;
    VkSurfaceCapabilitiesKHR capabilities = pSupport->SurfaceCapabilities;
    VkSurfaceFormatKHR prefFormat = settings.SurfaceFormat;
    VkSurfaceFormatKHR* aSurfaceFormats = pSupport->SurfaceFormats;
    VkPresentModeKHR* aPresentModes = pSupport->PresentModes;
    svl_i32 surfaceFormatsCount = pSupport->SurfaceFormatsCount;
    svl_i32 presentModeCount = pSupport->PresentModesCount;
    VkExtent2D resolution = settings.Resolution;
    svl_i8 isDebug = settings.IsDebug;
    VkAllocationCallbacks* pAllocator = settings._VkInstance.pAllocator;
    svl_u32 queueIndices[] = {
        settings.GraphicsQueueIndex,
        settings.PresentQueueIndex,
    };
    VkSurfaceKHR surface = settings.Surface;
    VkSwapchainKHR prevSwapchain = settings.PrevSwapchain;
    VkDevice device = settings.SvlDevice.Device;
    VkPhysicalDevice physicalDevice = settings.PhysicalDevice.PhysicalDevice;

    /* VkSwapchainKHR */
/* vsa_swapchain_create(VsaSwapChainSettings set, VkSwapChainSupportDetails supportDetails, VsaQueueFamily queueFamily) */

    VkSurfaceFormatKHR pickedSurfaceFormat = aSurfaceFormats[0];
    VkPresentModeKHR pickedPresentationMode = aPresentModes[0];

    for (svl_i32 f = 0; f < surfaceFormatsCount; ++f)
    {
        VkSurfaceFormatKHR surfaceFormat = aSurfaceFormats[f];
        if (surfaceFormat.format == prefFormat.format
            && surfaceFormat.colorSpace == prefFormat.colorSpace)
        {
            pickedSurfaceFormat = surfaceFormat;
            break;
        }
    }

    for (svl_i32 m = 0; m < presentModeCount; ++m)
    {
        VkPresentModeKHR presentMode = aPresentModes[m];
        if (presentMode == prefPresentMode)
        {
            pickedPresentationMode = presentMode;
            break;
        }
    }

    // printf(SvlTerminalRed("min Image COUNT: %u""\n"), capabilities.minImageCount);

    svl_i32 imagesInSwapChain = capabilities.maxImageCount;
    if (capabilities.maxImageCount == 0)
    {
        imagesInSwapChain = capabilities.minImageCount + capabilities.minImageCount * 0.7f;
    }

    VkExtent2D min = capabilities.minImageExtent;
    VkExtent2D max = capabilities.maxImageExtent;

// printf("Min(%d,%d) Max(%d,%d)\n", min.width, min.height, max.width, max.height);

    if (isDebug)
    {
        // DOCS: Validation layers cache surface data, wo this
        // we ll get error Validation Error: pCreateInfo->imageExtent (1999, 1200), which is outside the bounds returned by vkGetPhysicalDeviceSurfaceCapabilitiesKHR(): currentExtent = (2000,1200), minImageExtent = (2000,1200), maxImageExtent = (2000,1200). (https://vulkan.lunarg.com/doc/view/1.3.268.0/linux/1.3-extensions/vkspec.html#VUID-VkSwapchainCreateInfoKHR-pNext-07781)
        VkResult getSurfaceCapabilitiesResult =
            vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
                physicalDevice,
                surface,
                &pSupport->SurfaceCapabilities);
        //vkPrintSurfaceCapabilitiesLine(supportDetails.SurfaceCapabilities);
    }

    VkSwapchainPresentScalingCreateInfoEXT scalingExt = {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_PRESENT_SCALING_CREATE_INFO_EXT,
        .pNext = NULL,
        /* VkPresentScalingFlagsEXT */
        .scalingBehavior = VK_PRESENT_SCALING_ASPECT_RATIO_STRETCH_BIT_EXT,
        //VK_PRESENT_SCALING_ONE_TO_ONE_BIT_EXT,
        /* VkPresentGravityFlagsEXT */
        .presentGravityX = VK_PRESENT_GRAVITY_CENTERED_BIT_EXT,
        /* VkPresentGravityFlagsEXT */
        .presentGravityY = VK_PRESENT_GRAVITY_CENTERED_BIT_EXT,
    };

    VkSwapchainCreateInfoKHR swapChainCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .pNext = &scalingExt,
        .flags = 0,
        .surface = surface,
        .minImageCount = imagesInSwapChain,
        .imageFormat = pickedSurfaceFormat.format,
        .imageColorSpace = pickedSurfaceFormat.colorSpace,
        .imageExtent = resolution,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .preTransform = pSupport->SurfaceCapabilities.currentTransform,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = pickedPresentationMode,
        .clipped = VK_TRUE,
        .oldSwapchain = prevSwapchain,
    };

    if (queueIndices[0] != queueIndices[1])
    {
        swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        swapChainCreateInfo.queueFamilyIndexCount = 2;
        swapChainCreateInfo.pQueueFamilyIndices = queueIndices;
    }
    else
    {
        swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    }

    VkSwapchainKHR swapchain = VK_NULL_HANDLE;
    VkResult swapChainCreateResult =
        vkCreateSwapchainKHR(device, &swapChainCreateInfo, pAllocator, &swapchain);
    if (svl_result_check(swapChainCreateResult, "Failed Swapchain creation!"))
    {
        return (SvlSwapchain) { .Error = 1 };
    }

    SvlSwapchain svlSwapchain = {
        .Swapchain = swapchain,
        .SurfaceFormat = pickedSurfaceFormat,
        .Resolution = resolution,
    };

    //GINFO("Get %d SwapChain images\n", array_count(gVulkanSwapChainImages));

    return svlSwapchain;

}

SvlCmd
svl_cmd_create(SvlDevice svlDevice, _VkInstance _vkInstance, SvlGraphSupport graphSupport)
{
    VkDevice device = svlDevice.Device;
    VkAllocationCallbacks* pAllocator = _vkInstance.pAllocator;
    svl_i32 graphicsIndex = graphSupport.GraphicsIndex;

    VkCommandPool vkCommandPool;
    VkCommandPoolCreateInfo commandPoolCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext = NULL,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = graphicsIndex
    };
    VkResult createCommandPoolResult =
        vkCreateCommandPool(device, &commandPoolCreateInfo, pAllocator, &vkCommandPool);
    if (svl_result_check(createCommandPoolResult, "Can't create Command Pool!"))
    {
        return (SvlCmd) {};
    }

    VkCommandBuffer vkCommandBuffer;

    VkCommandBufferAllocateInfo commandBufferAllocatedInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = NULL,
        .commandPool = vkCommandPool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };
    VkResult allocatingCommandBufferResult =
        vkAllocateCommandBuffers(device, &commandBufferAllocatedInfo, &vkCommandBuffer);
    if (svl_result_check(allocatingCommandBufferResult, "Can't allocate Command Buffer!"))
    {
        return (SvlCmd) {};
    }

    SvlCmd svlCmd = {
        .Pool = vkCommandPool,
        .Buffer = vkCommandBuffer
    };

    return svlCmd;
}

/*#######################
  DOCS(typedef): Svl Api
  #######################*/

SvlInstance
svl_create(SvlSettings* pSettings)
{
    if (pSettings->Paths.pRootDirectory == NULL)
    {
        svl_result_check(1, "Root directory not set.");
        return (SvlInstance) {};
    }

    _VkInstance _vkInstance = _svl_instance_create(
        (_VkInstanceCreateInfo) {
            .pName = pSettings->pName,
            .VulkanVersion = pSettings->VulkanVersion,
            .IsDebug = pSettings->IsDebug,
            .pWindowBackend = pSettings->pWindowBackend
        });
    if (_vkInstance.Error != SvlErrorType_None)
    {
        return (SvlInstance) {};
    }

    _VkSurface _vkSurface = _svl_surface_create(&_vkInstance);
    if (_vkSurface.Error != SvlErrorType_None)
    {
        return (SvlInstance) {};
    }

    _VkPhysicalDevice _vkPhysicalDevice = _svl_physical_device_create(_vkInstance);
    if (_vkPhysicalDevice.Error != SvlErrorType_None)
    {
        return (SvlInstance) {};
    }

    if (pSettings->IsDevicePropsPrintable)
        _svl_physical_device_print(_vkPhysicalDevice.Properties);

    /* vsa_queue_family_create(&gVulkanSupportDetails); */
    SvlGraphSupport svlGraphSupport = svl_queue_indices(_vkSurface.Surface, _vkPhysicalDevice);

    SvlDevice svlDevice = _svl_device_create(_vkInstance, _vkPhysicalDevice, svlGraphSupport);
    if (svlDevice.Device == VK_NULL_HANDLE)
    {
        return (SvlInstance) {};
    }

    VkQueue graphicsQueue;
    SvlErrorType type = _svl_queue_create(svlDevice, svlGraphSupport.GraphicsIndex, &graphicsQueue);
    if (type != SvlErrorType_None)
    {
        return (SvlInstance) {};
    }

    VkQueue presentationQueue;
    type = _svl_queue_create(svlDevice, svlGraphSupport.PresentationIndex, &presentationQueue);
    if (type != SvlErrorType_None)
    {
        return (SvlInstance) {};
    }

    SvlSync svlSync = svl_sync_create(_vkInstance, svlDevice);
    if (svlSync.Error != SvlErrorType_None)
    {
        return (SvlInstance) {};
    }

    SvlSwapchainSettings svlSettings = {
        .IsVsync = pSettings->IsVsync,
        .IsDebug = pSettings->IsDebug,
        .SurfaceFormat = (VkSurfaceFormatKHR) {
            .format = VK_FORMAT_B8G8R8A8_SRGB,
            .colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
        },

        .pSupport = &svlGraphSupport,
        .Resolution = (VkExtent2D) { .width = pSettings->Extent.Width, .height = pSettings->Extent.Height },

        ._VkInstance = _vkInstance,
        .GraphicsQueueIndex = svlGraphSupport.GraphicsIndex,
        .PresentQueueIndex = svlGraphSupport.PresentationIndex,
        .Surface = _vkSurface.Surface,
        .SvlDevice = svlDevice,
        .PhysicalDevice = _vkPhysicalDevice,
    };

    SvlSwapchain svlSwapchain = svl_swapchain_create(svlSettings);

    SvlCmd svlCmd = svl_cmd_create(svlDevice, _vkInstance, svlGraphSupport);
    if (svlCmd.Pool == VK_NULL_HANDLE || svlCmd.Buffer == VK_NULL_HANDLE)
    {
        return (SvlInstance) {};
    }

    const char* pShaderDirectory = "Resources/Shaders";
    if (pSettings->Paths.pShaderDirectory != NULL)
    {
        pShaderDirectory = pSettings->Paths.pShaderDirectory;
    }
    const char* pRootDirectory = pSettings->Paths.pRootDirectory;

    SvlInstance svlInstance = {
        .IsInitialized = 1,
        .VkInstance = _vkInstance,
        .VkSurface = _vkSurface.Surface,
        .VkPhysicalDevice = _vkPhysicalDevice,
        .GraphSupport = svlGraphSupport,
        .Device = svlDevice,

        .PresentationQueue = presentationQueue,
        .GraphicsQueue = graphicsQueue,

        // NOTE: Graphics related
        .Sync = svlSync,
        .Swapchain = svlSwapchain,
        .Cmd = svlCmd,

        .Paths = {
            .pShaderDirectory = pShaderDirectory,
            .pRootDirectory = pRootDirectory,
        },

    };

    return svlInstance;
}

void
svl_destroy(SvlInstance* pInstance)
{
    _VkInstance vkInstance = pInstance->VkInstance;

    if (vkInstance.IsDebugMode)
    {
        PFN_vkDestroyDebugUtilsMessengerEXT vkDestroyDebugUtilsMessengerEXT =
            (PFN_vkDestroyDebugUtilsMessengerEXT)
            vkGetInstanceProcAddr(vkInstance.Instance, "vkDestroyDebugUtilsMessengerEXT");

        vkDestroyDebugUtilsMessengerEXT(vkInstance.Instance, vkInstance.DebugMessenger, vkInstance.pAllocator);
    }

#if 0
    vsa_swapchain_objects_cleanup();

    // TODO: Destroy pipelines here
    array_foreach(aRenderPasses, vsa_render_pass_destroy(&item));

    vkDestroySemaphore(gVulkanDevice, gVulkanImageAvailableSemaphores, pInstance->VkInstance.pAllocator);
    vkDestroySemaphore(gVulkanDevice, gVulkanRenderFinishedSemaphores, pInstance->VkInstance.pAllocator);
    vkDestroyFence(gVulkanDevice, gVulkanFrameFences, pInstance->VkInstance.pAllocator);
    vkDestroyCommandPool(gVulkanDevice, gVulkanCommandPool, pInstance->VkInstance.pAllocator);

    vkDestroyDevice(gVulkanDevice, pInstance->VkInstance.pAllocator);
    if (gVulkanDebugMessanger)
        vkDestroyDebugUtilsMessenger(gVulkanInstance, gVulkanDebugMessanger, pInstance->VkInstance.pAllocator);
    vkDestroySurfaceKHR(gVulkanInstance, gVulkanSurface, pInstance->VkInstance.pAllocator);

    vkDestroyInstance(gVulkanInstance, pInstance->VkInstance.pAllocator);
#endif
}

static VkShaderModule
_svl_shader_module_create_ext(VkDevice device, svl_u32* byteCode, size_t byteCodeSize)
{
    VkShaderModuleCreateInfo shaderModuleCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .codeSize = byteCodeSize,
        .pCode = byteCode
    };

    VkShaderModule shaderModule;
    VkResult createShaderModuleResult =
        vkCreateShaderModule(device, &shaderModuleCreateInfo,  NULL /* gAllocatorCallbacks */, &shaderModule);
    //vkValidResult(createShaderModuleResult, "Can't create shader!");
    if (svl_result_check(createShaderModuleResult, "Can't create shader"))
    {
        // return error;
        return shaderModule;
    }

    return shaderModule;
}

SvlRenderPass*
_svl_get_render_pass(SvlInstance* pInstance, svl_i32 index, SvlErrorType* pError)
{
    svl_i32 cnt = svl_array_count(pInstance->aRenderPasses);
    if (index >= cnt || index < 0)
    {
        *pError = SvlErrorType_NoRenderPass;
        return NULL;
    }

    SvlRenderPass* pRenderPass = &pInstance->aRenderPasses[index];
    return pRenderPass;
}

SvlShaderGroupSettings
_svl_get_shader_group_settings(SvlShaderStage* aStages, svl_i32 stagesCount)
{
    SvlShaderGroupSettings groupSettings = {};

    for (svl_i32 i = 0; i < stagesCount; ++i)
    {
        SvlShaderStage stage = aStages[i];
        svl_guard(stage.Type != SvlShaderType_Count && "SvlShaderType_Count used in pipe settings.");
        groupSettings.aPaths[stage.Type] = stage.ShaderSourcePath;
    }

    return groupSettings;
}

svl_i32
_svl_string_compare(const char* s0, const char* s1)
{
    char* p0 = (char*) s0;
    char* p1 = (char*) s1;

    if (s0 == NULL || s1 == NULL)
        return 0;

    char c0, c1;
    while (1)
    {
        c0 = *p0;
        c1 = *p1;

        if (c0 != c1)
            return 0;
        else if (c0 == '\0' || c1 == '\0')
            return 1;

        p0++;
        p1++;
    }

}

svl_i64
_svl_string_length(const char* p)
{
    char* ptr;
    for (ptr = (char*) p; *ptr != '\0'; ++ptr);

    return (svl_i64) ((svl_size)(ptr - p));
}

char*
_svl_path_combine(const char* left, const char* right)
{
    svl_guard(left != NULL);
    svl_guard(right != NULL);

    /*
      1. Adding paths:
      * path_combine("Image/", "a.png");
      * path_combine("Image/", "/a.png");
      * path_combine("Image/", "./a.png");
      */

    svl_i64 leftLength  = _svl_string_length(left);

    char leftLastChar = left[leftLength-1];
    svl_i32 isLeftSlashed = (leftLastChar == '/' || leftLastChar == '\\');

    char* rightMod = NULL;
    svl_i64 leftModLen;
    svl_i64 rightModLen;

    if (isLeftSlashed)
        leftModLen = leftLength - 1;
    else
        leftModLen = leftLength;

    { // DOCS(typedef): set rightMod
        char* ptr = (char*) right;
        char c = *ptr;
        while (c == '.' || c == '/' || c == '\\' || c == ' ' || c == '\n' || c == '\t' || c == '\r')
        {
            ++ptr;
            c = *ptr;
        }

        // DOCS(typedef): right is empty string for us
        if (*ptr == '\0')
        {
            return _svl_string(left);
        }

        rightModLen = _svl_string_length(ptr);
        rightMod = ptr;
    }

    svl_i64 resultLength = leftModLen + rightModLen + 1 /* slash between */ + 1 /*\0*/;
    char* result = (char*) malloc(resultLength);

    char* ptr = result;
    memcpy(ptr, left, leftModLen);

    ptr += leftModLen;
    *ptr = '/';

    ++ptr;
    memcpy(ptr, rightMod, rightModLen);

    ptr += rightModLen;
    *ptr = '\0';

    return result;
}

const char*
_svl_path_get_name(const char* p)
{
    if (p == NULL)
        return 0;

    svl_i64 plen = _svl_string_length(p);
    svl_i64 last = plen - 1;

    char* ptr = (char*) p;
    char* pEnd = (char*) (ptr + plen - 1);

    for (svl_i64 i = last; i >= 0; --i)
    {
        char c = *pEnd;
        if (c == '/' || c == '\\')
        {
            return (const char*) (pEnd + 1);
        }

        --pEnd;
    }

    return p;
}

char*
_svl_path_get_name_wo_ext(const char* p)
{
    if (p == NULL)
        return 0;

    svl_i64 plen = _svl_string_length(p);
    svl_i64 last = plen - 1;
    svl_i64 dotIndex = -1;
    char* ptr = (char*) p;

    char* pEnd = (char*) (ptr + plen - 1);

    for (svl_i64 i = last; i >= 0; --i)
    {
        char c = *pEnd;
        if (c == '.')
        {
            dotIndex = i;
        }
        else if (c == '/' || c == '\\')
        {
            // 0123456789
            //     456
            // /pat/a.txt
            svl_i64 nameInd = (i + 1);
            svl_i64 nameWoExtLen = dotIndex - nameInd;
            char* res = (char*) malloc(nameWoExtLen+1);
            res[nameWoExtLen] = '\0';
            memcpy(res, pEnd + 1, nameWoExtLen);

            return (char*) res;
        }

        --pEnd;
    }

    return _svl_string(p);
}

const char*
_svl_path_get_ext(const char* p)
{
    if (p == NULL)
        return 0;

    svl_i64 plen = _svl_string_length(p);
    svl_i64 last = plen - 1;
    char* ptr = (char*) p;

    char* pEnd = (char*) (ptr + plen - 1);

    for (svl_i64 i = last; i >= 0; --i)
    {
        char c = *pEnd;
        if (c == '.')
        {
            return (const char*) pEnd;
        }
        --pEnd;
    }

    return p;
}

char
_svl_char_to_upper(char c)
{
    if (c >= 'a' && c <= 'z')
        return 'A' + (c - 'a');
    return c;
}

char*
_svl_string(const char* s)
{
    svl_i64 len = _svl_string_length(s) + 1;
    char* str = malloc(len);
    memcpy(str, s, len);
    return str;
}

char*
_svl_string_concat(const char* p0, const char* p1)
{
    svl_i64 leftLength = _svl_string_length(p0);
    svl_i64 rightLength = _svl_string_length(p1);
    svl_i64 bothLength = leftLength + rightLength;

    char* newString = (char*) malloc(bothLength + 1);

    memcpy(newString, p0, leftLength);
    memcpy(newString + leftLength, p1, rightLength);
    newString[bothLength] = '\0';

    return newString;
}

void*
_svl_file_read_bytes(const char* pPath, svl_i64* pLength)
{
    FILE* file;
    void* pResult;
    svl_i64 size;

    file = fopen(pPath, "rb");
    if (file)
    {
        fseek(file, 0, SEEK_END);
        size = (svl_i64) ftell(file);
        fseek(file, 0, SEEK_SET);
        pResult = malloc(size);

        fread(pResult, 1, size, file);
        *pLength = size;

        fclose(file);
        return pResult;
    }

    return NULL;
}

svl_i32
_svl_path_file_exist(const char* path)
{
#if defined(SVL_LINUX_PLATFORM)
    struct stat buf;
    svl_i32 result = stat(path, &buf);
    return (result != -1);
#elif defined(SVL_WINDOWS_PLATFORM)
    BOOL isFileExist = PathFileExistsA(path);
    return isFileExist;
#endif
}

svl_i32
_svl_path_directory_exist(const char* pPath)
{
#if defined(SVL_LINUX_PLATFORM)
    struct stat buf;
    svl_i32 result = stat(pPath, &buf);
    return (result != -1);
#elif defined(SVL_WINDOWS_PLATFORM)
    DWORD result = GetFileAttributesA(path);
    return (result != INVALID_FILE_ATTRIBUTES && (result & FILE_ATTRIBUTE_DIRECTORY));
#endif
}

svl_i32
_svl_directory_create(const char* pPath)
{
#if defined(SVL_LINUX_PLATFORM)

    /*
      #define S_IRWXU 0000700    RWX mask for owner
      #define S_IRUSR 0000400    R for owner
      #define S_IWUSR 0000200    W for owner
      #define S_IXUSR 0000100    X for owner

      #define S_IRWXG 0000070    RWX mask for group
      #define S_IRGRP 0000040    R for group
      #define S_IWGRP 0000020    W for group
      #define S_IXGRP 0000010    X for group

      #define S_IRWXO 0000007    RWX mask for other
      #define S_IROTH 0000004    R for other
      #define S_IWOTH 0000002    W for other
      #define S_IXOTH 0000001    X for other

      #define S_ISUID 0004000    set user id on execution
      #define S_ISGID 0002000    set group id on execution
      #define S_ISVTX 0001000    save swapped text even after use
    */
    svl_i32 result = mkdir(pPath, S_IRWXU);
    if (result != 0)
        return 0;
    return 1;

#elif defined(SVL_WINDOW_PLATFORM)

    if (!CreateDirectoryA(pPath, NULL))
        return 0;
    return 1;

#endif
}

svl_size
_svl_path_get_last_mod_time(const char* pPath)
{
#if defined(SVL_LINUX_PLATFORM)

    struct stat fileItemInfo;
    stat(pPath, &fileItemInfo);
    return fileItemInfo.st_mtime;

#elif defined(SVL_WINDOWS_PLATFORM)

    svl_i64 accessTime, creationTime, writeTime;
    io_get_file_times_ms(path, &at, &ct, &wt);

    HANDLE file = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (file == INVALID_HANDLE_VALUE)
    {
        GERROR("Can't open file: %s\n", path);
        vassert_break();
    }
    FILETIME ct, at, wt;
    BOOL result = GetFileTime(file, &ct, &at, &wt);
    if (result == FALSE)
    {
        GERROR("Can't get file time: %s\n", path);
        vassert_break();
    }

    SYSTEMTIME systemAccessTime;
    FileTimeToSystemTime(&at, &systemAccessTime);
    SYSTEMTIME systemCreationTime;
    FileTimeToSystemTime(&ct, &systemCreationTime);
    SYSTEMTIME systemWriteTime;
    FileTimeToSystemTime(&wt, &systemWriteTime);

    *accessTime = systemAccessTime.wMilliseconds;
    *creationTime = systemCreationTime.wMilliseconds;
    *writeTime = systemWriteTime.wMilliseconds;

    return wt;

#endif
}

char
svl_char_to_upper(char c)
{
    if (c >= 'a' && c <= 'z')
        return (c - 'a' + 'A');
    else
        return c;
}

static svl_i32
_svl_is_shader_compilation_needed(SvlInstance* pInstance, const char* path, const char* spvPath)
{
    if (_svl_path_file_exist(spvPath) == 0)
        return 1;

    svl_size lastModificationTimeRaw = _svl_path_get_last_mod_time(path);
    svl_size lastCreationTimeRaw = _svl_path_get_last_mod_time(spvPath);

    if (lastModificationTimeRaw > lastCreationTimeRaw)
        return 1;

    return 0;
}


SvlShaderBytecode
svl_shader_compile_single_unit_new(SvlInstance* pInstance,
                                   const char* pPath,
                                   const char* pSourceDir,
                                   const char* pBinDir)
{
    // note: commit with shaderc inside code, not util d735f717589aa0c023c73b5b82946690844f826d

    SvlShaderBytecode byteCode = {};

    const char* binDir = pBinDir;
    const char* sourceDir = pSourceDir;

    const char* pName = _svl_path_get_name(pPath);/*Vsr3dd.frag*/
    char* sourceDirA = _svl_path_combine(sourceDir, pName);

    const char* nameWoExt = _svl_path_get_name_wo_ext(pPath); // Vsr3dd
    char* ext = _svl_string(_svl_path_get_ext(pPath) + 1); //frag
    ext[0] = _svl_char_to_upper(ext[0]);
    char* ending = _svl_string_concat(ext, ".spv"); // Frag.spv
    char* fullNameWoDir = _svl_string_concat(nameWoExt, ending);
    char* spvPath = _svl_path_combine(binDir, fullNameWoDir); //full path spv

    svl_i32 isRecompilationNeeded =
        _svl_is_shader_compilation_needed(pInstance, sourceDirA, spvPath);
    if (!isRecompilationNeeded)
    {
        svl_i64 size;
        void* bytes = _svl_file_read_bytes(spvPath, &size);
        byteCode = (SvlShaderBytecode) {
            .Bytecode = bytes,
            .Size = size,
        };

        goto EndSvlShaderCompileSingleUnitLabel;
    }

    // glslc Vsr3dd.frag -O -o Vsr3ddFrag.spv --target-env=vulkan1.3 -w -x glsl
    // glslc Vsr3dd.vert -O -o Vsr3ddVert.spv --target-env=vulkan1.3 -w -x glsl

    const char* optimization = "";
    svl_i32 isOptimizationNeeded = 1;
    if (isOptimizationNeeded)
    {
        optimization = "-O";
    }

    printf("sourceDirA: %s\n", sourceDirA);
    printf("optimization: %s\n", optimization);
    printf("spvPath: %s\n", spvPath);

    char cmd[1024] = {};
    sprintf(cmd, "glslc %s %s -o %s --target-env=vulkan1.3 -w -x glsl", sourceDirA, optimization, spvPath);
    system(cmd);

    //vassert(path_is_file_exist(spvPath) && "Path for spv is not exist!");

    svl_i64 size = 0;
    void* pBytecode = _svl_file_read_bytes(spvPath, &size);
    byteCode = (SvlShaderBytecode) {
        .Bytecode = pBytecode,
        .Size = size,
    };

EndSvlShaderCompileSingleUnitLabel:
    free(sourceDirA);
    free(fullNameWoDir);
    free((void*)nameWoExt);
    free(ext);
    free(ending);
    free(spvPath);

    return byteCode;

}

SvlShaderGroup
_svl_shader_group_create(SvlInstance* pInstance, SvlShaderGroupSettings* pGroupSettings, SvlErrorType* pError)
{
    const char* pShaderDir = pInstance->Paths.pShaderDirectory;
    const char* pRootDir = pInstance->Paths.pRootDirectory;

    SvlShaderGroup group = {};

    if (pInstance->VkInstance.IsDebugMode)
    {
        svl_i32 isAllNull = 1;
        for (svl_i32 i = 0; i < SvlShaderType_Count; ++i)
        {
            if (pGroupSettings->aPaths[i] != NULL)
            {
                isAllNull = 0;
                break;
            }
        }

        if (isAllNull)
        {
            svl_result_check(isAllNull, "Compile Settings all paths for shader units is NULL!\n");
            *pError = SvlErrorType_Shader_NoPath;
        }
    }

    char* pSourceDir = _svl_path_combine(pRootDir, pShaderDir);
    const char* dirs[] = {
        [0] = pSourceDir,
        [1] = _svl_path_combine(pSourceDir, "Bin"),
    };

    for (svl_i32 i = 0; i < SvlArrayCount(dirs); ++i)
    {
        const char* dir = dirs[i];
        svl_i32 isDirectoryExist = _svl_path_directory_exist(dir);
        if (!isDirectoryExist)
        {
            _svl_directory_create(dir);
        }
    }

    for (svl_i32 i = 0; i < SvlShaderType_Count; ++i)
    {
        SvlShaderType type = (SvlShaderType) i;
        const char* pPath = pGroupSettings->aPaths[type];
        if (pPath == NULL)
            continue;

        SvlShaderBytecode byte =
            svl_shader_compile_single_unit_new(pInstance, pPath, dirs[0], dirs[1]);
        group.aBytecodes[type] = byte;

        if (byte.Bytecode == NULL || byte.Size == 0)
        {
            *pError = SvlErrorType_Shader_BytecodeInvalid;
        }
    }

    for (svl_i32 i = 0; i < SvlArrayCount(dirs); ++i)
    {
        free((void*)dirs[i]);
    }

    return group;
}

SvlBinding
_svl_binding_create(SvlInstance* pInstance, SvlShaderStage* aStages, svl_i32 stagesCount)
{
    svl_i32 bindingIndex;
    VkDescriptorSetLayoutBinding* aBindings = NULL;
    VkDescriptorPoolSize* aPoolSizes = NULL;
    SvlBinding svlBinding = {};

    for (svl_i32 i = 0; i < stagesCount; ++i)
    {
        SvlShaderStage stage = aStages[i];
        VkShaderStageFlagBits stageFlag = svl_shader_type_to_flag(stage.Type);

        bindingIndex = 0;

        for (svl_i32 d = 0; d < stage.DescriptorsCount; ++d)
        {
            SvlDescriptor svlDescr = stage.aDescriptors[d];
            svl_guard(svlDescr.Count > 0);
            svlDescr.Binding = bindingIndex;
            svl_i32 descrCnt = 1;

            switch (svlDescr.Type)
            {

            case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
            {
                SvlUniformSettings uniformSet = {
                    .Size = svlDescr.Size * svlDescr.Count,
                    .ItemSize = svlDescr.Size,
                    .BindIndex = svlDescr.Binding,
                    .pName = svlDescr.pName,
                };

                SvlUniform uniform =
                    svl_uniform_create(pInstance, uniformSet);
                svl_array_push(svlBinding.aUniforms, uniform);
                break;
            }

            case VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
            { // DOCS: Probably a texture
                svl_guard(svlDescr.aImageInfos == NULL);

                svl_array_reserve(svlDescr.aImageInfos, svlDescr.Count);
                svl_array_header(svlDescr.aImageInfos)->Count = svlDescr.Count;
                descrCnt = svlDescr.Count;

                svl_array_push(svlBinding.aDescriptors, svlDescr);
                break;
            }

            case VK_DESCRIPTOR_TYPE_SAMPLER:
            {
                svl_guard(svlDescr.aImageInfos == NULL);

                svl_array_reserve(svlDescr.aImageInfos, svlDescr.Count);
                svl_array_header(svlDescr.aImageInfos)->Count = svlDescr.Count;

                svl_array_push(svlBinding.aDescriptors, svlDescr);
                break;
            }

            default:
                svl_guard(0);
                break;

            }

            /* VkDescriptorSetLayoutBinding* aBindings = NULL; */
            /* VkDescriptorPoolSize* aPoolSizes = NULL; */
            VkDescriptorSetLayoutBinding binding = {
                .binding = bindingIndex,
                .descriptorType = svlDescr.Type,
                .descriptorCount = descrCnt,
                .stageFlags = stageFlag,
                .pImmutableSamplers = NULL
            };

            VkDescriptorPoolSize poolSize = {
                .type = svlDescr.Type,
                .descriptorCount = svlDescr.Count
            };

            svl_array_push(aBindings, binding);
            svl_array_push(aPoolSizes, poolSize);

            ++bindingIndex;

        }
    }

    VkDescriptorSetLayoutCreateInfo descriptorCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext = NULL,
        .flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT,//0,
        .bindingCount = svl_array_count(aBindings),
        .pBindings = aBindings
    };

    VkDescriptorSetLayout vkDescriptorSetLayout;
    VkResult creteDescriptorSetLayout =
        vkCreateDescriptorSetLayout(pInstance->Device.Device, &descriptorCreateInfo, pInstance->VkInstance.pAllocator, &vkDescriptorSetLayout);
    if (svl_result_check(creteDescriptorSetLayout, "Failed descriptor set layout creation!"))
    {
    }

    VkDescriptorPoolCreateInfo vkPoolCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext = NULL,
        .flags = VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT,
        .maxSets = 1,
        .poolSizeCount = svl_array_count(aPoolSizes),
        .pPoolSizes = aPoolSizes,
    };

    VkDescriptorPool vkDescriptorPool;
    VkResult descriptorPoolCreateResult =
        vkCreateDescriptorPool(pInstance->Device.Device, &vkPoolCreateInfo, pInstance->VkInstance.pAllocator, &vkDescriptorPool);

    if (svl_result_check(descriptorPoolCreateResult, "Failed Descriptor Pool Creation!!!"))
    {
    }

    VkDescriptorSet vkDescriptorSet;
    VkDescriptorSetAllocateInfo vkDescriptorSetAllocateInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext = NULL,
        .descriptorPool = vkDescriptorPool,
        .descriptorSetCount = 1,
        .pSetLayouts = &vkDescriptorSetLayout,
    };

    VkResult allocateDescritorSetsResult =
        vkAllocateDescriptorSets(pInstance->Device.Device, &vkDescriptorSetAllocateInfo, &vkDescriptorSet);
    if (svl_result_check(allocateDescritorSetsResult, "Failed allocate descriptor sets!"))
    {
    }

    svl_array_free(aBindings);
    svl_array_free(aPoolSizes);

    svlBinding.DescriptorSet = vkDescriptorSet;
    svlBinding.DescriptorSetLayout = vkDescriptorSetLayout;
    svlBinding.DescriptorPool = vkDescriptorPool;

    return svlBinding;
}

void
svl_binding_destroy(SvlInstance* pInstance, SvlBinding binding)
{
    for (svl_i32 i = 0; i < svl_array_count(binding.aUniforms); ++i)
    {
        svl_uniform_destroy(pInstance, &binding.aUniforms[i]);
    }

    svl_array_free(binding.aUniforms);

    vkDestroyDescriptorSetLayout(
        pInstance->Device.Device,
        binding.DescriptorSetLayout,
        pInstance->VkInstance.pAllocator);
    vkDestroyDescriptorPool(
        pInstance->Device.Device,
        binding.DescriptorPool,
        pInstance->VkInstance.pAllocator);
}

SvlPipeline*
svl_pipeline_create(SvlInstance* pInstance, SvlPipelineSettings settings, SvlErrorType* pError)
{
    VkPipeline vkPipeline;
    VkPipelineLayout vkPipelineLayout;

#if SVL_DEBUG
    if (settings.StagesCount == 0 && settings.aStages != NULL)
    {
        GWARNING("Weird param settings.StagesCount == 0 when settings.aStages != NULL!\n");
    }
    if (settings.AttributesCount == 0 && settings.aAttributes != NULL)
    {
        GWARNING("Weird param settings.AttributesCount == 0 when settings.aAttributes != NULL!\n");
    }
#endif

    SvlErrorType renderPassError = 0;
    SvlRenderPass* pRenderPass =
        _svl_get_render_pass(pInstance, settings.RenderPassGroupIndex, &renderPassError);

    SvlBinding svlBinding = _svl_binding_create(pInstance, settings.aStages, settings.StagesCount);

    SvlShaderGroupSettings groupSet =
        _svl_get_shader_group_settings(settings.aStages,
                                       settings.StagesCount);
    //todo: shader compilation error
    SvlErrorType error = 0;
    SvlShaderGroup shaderGroup = _svl_shader_group_create(pInstance, &groupSet, &error);
    // todo:
    svl_guard(error == SvlErrorType_None);

    VkPipelineShaderStageCreateInfo* aStageCreateInfos = NULL;
    for (svl_i32 si = 0; si < settings.StagesCount; ++si)
    {
        SvlShaderStage stage = settings.aStages[si];

        svl_guard(stage.Type >= 0 && stage.Type <= SvlShaderType_Count);

        SvlShaderBytecode byteCode = shaderGroup.aBytecodes[stage.Type];

        VkShaderModule vkShaderModule =
            _svl_shader_module_create_ext(pInstance->Device.Device, byteCode.Bytecode, byteCode.Size);

        VkPipelineShaderStageCreateInfo stageCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .pNext = NULL,
            .flags = 0,
            .stage = svl_shader_type_to_flag(stage.Type),
            .module = vkShaderModule,
            .pName = "main",
            /* NOTE(typedef): use this thing if we need constants in shader */
            .pSpecializationInfo = NULL
        };

        svl_array_push(aStageCreateInfos, stageCreateInfo);
    }

    // NOTE(typedef): Vertex Buffer description
    VkVertexInputBindingDescription vkVertexInputBindingDescription = {
        .binding = 0,
        .stride = settings.Stride,
        .inputRate = VK_VERTEX_INPUT_RATE_VERTEX
    };

    VkVertexInputAttributeDescription* aAttributeDescrs = NULL;
    for (svl_i32 i = 0; i < settings.AttributesCount; ++i)
    {
        SvlShaderAttribute attr = settings.aAttributes[i];

        VkVertexInputAttributeDescription descr = {
            .location = i,
            .binding = attr.GroupBinding,
            .format = attr.Format,
            .offset = attr.Offset
        };

        svl_array_push(aAttributeDescrs, descr);
    }

    VkPipelineVertexInputStateCreateInfo pipelineVertexInputCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &vkVertexInputBindingDescription,
        .vertexAttributeDescriptionCount = svl_array_count(aAttributeDescrs),
        .pVertexAttributeDescriptions = aAttributeDescrs
    };

    VkDynamicState dynamicStates[2] = {
        [0] = VK_DYNAMIC_STATE_VIEWPORT,
        [1] = VK_DYNAMIC_STATE_SCISSOR
    };

    VkPipelineDynamicStateCreateInfo pipelineDynamicStateCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .dynamicStateCount = SvlArrayCount(dynamicStates),
        .pDynamicStates = dynamicStates,
    };

    VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE
    };

    // DOCS(typedef): passing viewport, scissor here make them immutable
    VkPipelineViewportStateCreateInfo pipelineViewportCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .viewportCount = 1,
        .scissorCount = 1
    };

    VkPipelineRasterizationStateCreateInfo pipelineRasterizationCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        // NOTE(typedef): enabling that requires gpu feature
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        // NOTE(typedef): requires enabling gpu feature
        .polygonMode = settings.PolygonMode,
        .cullMode =
#if 1 // NOTE(): best option
        VK_CULL_MODE_NONE,
#else // DOCS: Using this for now
        VK_CULL_MODE_BACK_BIT,
#endif

        .frontFace =
#if 1 // NOTE(): best option
        VK_FRONT_FACE_COUNTER_CLOCKWISE,
#else
        VK_FRONT_FACE_CLOCKWISE,
#endif

        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0.0f,
        .depthBiasClamp = 0.0f,
        .depthBiasSlopeFactor = 0.0f,
        // NOTE(typedef): anything other then 1.0f requires enabling GPU feature
        .lineWidth = 1.0f
    };

    /* if (!settings.EnableBackFaceCulling) */
    /* { */
    /*	pipelineRasterizationCreateInfo.cullMode = VK_CULL_MODE_NONE; */
    /* } */

    VkPipelineMultisampleStateCreateInfo pipelineMultisampleCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .rasterizationSamples = pRenderPass->Samples,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 1.0f,
        .pSampleMask = NULL,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = VK_FALSE
    };

    // NOTE(typedef): Depth Stencil code creation here
    VkPipelineDepthStencilStateCreateInfo depthStencilCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .depthTestEnable = VK_TRUE,
        .depthWriteEnable = VK_TRUE,
        .depthCompareOp = VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
        .front = 0.0f,
        .back = 0.0f,
        .minDepthBounds = -1.0f,
        .maxDepthBounds =  1.0f
    };

    /* if (!settings.EnableDepthStencil) */
    /* { */
    /*	depthStencilCreateInfo = (VkPipelineDepthStencilStateCreateInfo) {}; */
    /*	vguard(0 && "Wtf, you foget .EnableDepthStencil = 1 !!!"); */
    /* } */

    // NOTE(typedef): Blending not configured now
    VkPipelineColorBlendAttachmentState pipelineColorBlendAttachment = {
        .blendEnable = VK_TRUE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp = VK_BLEND_OP_ADD,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
    };

    VkPipelineColorBlendStateCreateInfo pipelineColorBlendCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &pipelineColorBlendAttachment,
        .blendConstants = { 0, 0, 0, 0 }
    };

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .setLayoutCount = 1,
        .pSetLayouts = &svlBinding.DescriptorSetLayout,
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = NULL
    };

    VkResult createPipelineLayoutResult =
        vkCreatePipelineLayout(pInstance->Device.Device, &pipelineLayoutCreateInfo, pInstance->VkInstance.pAllocator, &vkPipelineLayout);
    if (svl_result_check(createPipelineLayoutResult, "Can't create pipeline layout!"))
    {
        *pError = SvlErrorType_Pipeline_Layout;
        return NULL;
    }

    VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .stageCount = svl_array_count(aStageCreateInfos),
        .pStages = aStageCreateInfos,
        .pVertexInputState = &pipelineVertexInputCreateInfo,
        .pInputAssemblyState = &pipelineInputAssemblyCreateInfo,
        .pTessellationState = NULL,
        .pViewportState = &pipelineViewportCreateInfo,
        .pRasterizationState = &pipelineRasterizationCreateInfo,
        .pMultisampleState = &pipelineMultisampleCreateInfo,
        .pDepthStencilState = &depthStencilCreateInfo,
        .pColorBlendState = &pipelineColorBlendCreateInfo,
        .pDynamicState = &pipelineDynamicStateCreateInfo,
        .layout = vkPipelineLayout,
        .renderPass = pRenderPass->RenderPass,
        .subpass = 0,
        .basePipelineHandle = VK_NULL_HANDLE,
        .basePipelineIndex = -1
    };

    VkResult createGraphicsPipelinesResult =
        vkCreateGraphicsPipelines(pInstance->Device.Device, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, pInstance->VkInstance.pAllocator, &vkPipeline);
    if (svl_result_check(createGraphicsPipelinesResult, "Can't create Graphics Pipeline!"))
    {
        *pError = SvlErrorType_Pipeline;
        return NULL;
    }

    SvlPipeline* pPipe = NULL;
    {
        SvlPipeline svlPipeline = {
            .Binding = svlBinding,
            .Pipeline = vkPipeline,
            .PipelineLayout = vkPipelineLayout
        };

        pPipe = malloc(sizeof(SvlPipeline));
        memcpy(pPipe, &svlPipeline, sizeof(SvlPipeline));
        svl_array_push(pRenderPass->apPipelines, pPipe);
    }

    // DOCS(typedef): Clean up
    svl_array_free(aAttributeDescrs);

    // note: free bytecode mem
    for (SvlShaderType i = SvlShaderType_Vertex; i < SvlShaderType_Count; ++i)
    {
        SvlShaderBytecode svlBytecode = shaderGroup.aBytecodes[i];
        free(svlBytecode.Bytecode);
    }

    for (svl_i32 i = 0; i < svl_array_count(aStageCreateInfos); ++i)
    {
        VkPipelineShaderStageCreateInfo stageCreateInfo = aStageCreateInfos[i];
        vkDestroyShaderModule(pInstance->Device.Device, stageCreateInfo.module, pInstance->VkInstance.pAllocator);
    }

    svl_array_free(aStageCreateInfos);

    return pPipe;
}

/*#######################
  DOCS(typedef): Svl Public Api
  #######################*/

void
svl_set_clear_color(SvlRenderPass* pRenderPass, svl_f32 r, svl_f32 g, svl_f32 b, svl_f32 a)
{
    VkClearColorValue clearColor = { r, g, b, a };

    pRenderPass->ClearColor = clearColor;
}

void
svl_set_viewport(SvlPipeline* pPipeline, svl_v2i from, svl_v2i size)
{
    VkViewport viewport = {
        .x = (svl_f32) from.X,
        .y = (svl_f32) from.Y,
        .width = (svl_f32) size.X,
        .height = (svl_f32) size.Y,
        .minDepth = 0,
        .maxDepth = 1
    };

    pPipeline->Viewport = viewport;
}

void
svl_set_scissor(SvlPipeline* pPipeline, svl_v2i from, svl_v2i to)
{
    VkRect2D scissor = {
        .offset = {
            .x = from.X,
            .y = from.Y,
        },
        .extent = {
            .width  = (svl_u32) to.X,
            .height = (svl_u32) to.Y,
        }
    };

    pPipeline->Scissor = scissor;
}

SvlSwapImageView*
_svl_swapchain_image_views_create(SvlInstance* pInstance, VkFormat vkFormat, SvlErrorType* pError)
{
    VkDevice device = pInstance->Device.Device;
    VkSwapchainKHR swapchain = pInstance->Swapchain.Swapchain;

    VkImage* aImages = NULL;

    svl_u32 imagesCount;
    VkResult getSwapChainImagesResult = vkGetSwapchainImagesKHR(device, swapchain, &imagesCount, NULL);
    if (svl_result_check(getSwapChainImagesResult, "Can't get swap chain images!"))
    {
        *pError = SvlErrorType_SwapImageView;
        return NULL;
    }

    svl_array_reserve(aImages, imagesCount);
    svl_array_header(aImages)->Count = imagesCount;

    getSwapChainImagesResult = vkGetSwapchainImagesKHR(device, swapchain, &imagesCount, aImages);
    if (svl_result_check(getSwapChainImagesResult, "Can't get swap chain images!"))
    {
        *pError = SvlErrorType_SwapImageView;
        return NULL;
    }

    svl_guard((imagesCount > 0) && "Images Count should be > 0 !!!");

    SvlSwapImageView* aSwapImageViews = NULL;

    for (svl_i64 i = 0; i < imagesCount; ++i)
    {
        VkImageView vkImageView;
        SvlErrorType error = 0;
        _svl_image_view_create(pInstance,
                               aImages[i],
                               vkFormat,
                               VK_IMAGE_ASPECT_COLOR_BIT,
                               1,
                               &vkImageView, &error);

        SvlSwapImageView vsaImageView = {
            .Image = aImages[i],
            .View = vkImageView,
        };

        svl_array_push(aSwapImageViews, vsaImageView);
    }

    return aSwapImageViews;
}

svl_i32
svl_find_memory_type_index(SvlInstance* pInstance, svl_u32 typeBit, VkMemoryPropertyFlags vkMemoryPropertyFlags)
{
    VkPhysicalDeviceMemoryProperties dMemoryProperties;
    vkGetPhysicalDeviceMemoryProperties(pInstance->VkPhysicalDevice.PhysicalDevice, &dMemoryProperties);

    for (svl_i32 i = 0; i < dMemoryProperties.memoryTypeCount; ++i)
    {
        VkMemoryType memoryType =
            dMemoryProperties.memoryTypes[i];
        if ((typeBit & (1 << i)) && (memoryType.propertyFlags & vkMemoryPropertyFlags))
        {
            return i;
        }
    }

    svl_guard(0 && "Can't find memory type index!");
    return -1;
}


void
_svl_image_create(SvlInstance* pInstance, SvlImageSettings settings, VkImage* pVkImage, VkDeviceMemory* pDeviceMemory, SvlErrorType* pError)
{
    VkDevice vkDevice = pInstance->Device.Device;
    VkAllocationCallbacks* pAllocator = pInstance->VkInstance.pAllocator;
    VkImageCreateInfo vkImageCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = settings.Format,
        .extent = {
            .width = settings.Width,
            .height = settings.Height,
            .depth = settings.Depth
        },
        .mipLevels = settings.MipLevels,
        .arrayLayers = 1,
        .samples = settings.SamplesCount,
        .tiling = settings.ImageTiling,
        .usage = settings.ImageUsageFlags,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0, // NOTE(): should we set this value
        .pQueueFamilyIndices = NULL,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };

    VkResult vkCreateImageResult = vkCreateImage(
        vkDevice, &vkImageCreateInfo, pAllocator, pVkImage);
    if (svl_result_check(vkCreateImageResult, "Failed image/texture creation!"))
    {
        *pError = SvlErrorType_Image;
    }

    VkImage tempVkImage = *pVkImage;
    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(vkDevice, tempVkImage, &memRequirements);

    svl_i32 index = svl_find_memory_type_index(
        pInstance,
        memRequirements.memoryTypeBits,
        settings.MemoryPropertyFlags);

    VkMemoryAllocateInfo allocateInfo = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext = NULL,
        .allocationSize = memRequirements.size,
        .memoryTypeIndex = index
    };

    VkResult vkAllocateMemoryResult =
        vkAllocateMemory(vkDevice, &allocateInfo, pAllocator, pDeviceMemory);
    if (svl_result_check(vkAllocateMemoryResult, "Failed texture memory allocation!"))
    {
        *pError = SvlErrorType_Image;
    }

    VkResult vkBindImageMemoryResult =
        vkBindImageMemory(vkDevice, tempVkImage, *pDeviceMemory, 0);
    if (svl_result_check(vkBindImageMemoryResult, "Failed bind texture memory!"))
    {
        *pError = SvlErrorType_Image;
    }
}

void
_svl_image_view_create(SvlInstance* pInstance, VkImage vkImage, VkFormat vkFormat, VkImageAspectFlags vkImageAspectFlags, svl_i32 mipLevels, VkImageView* pImageView, SvlErrorType* pError)
{
    VkImageView vkImageView;

    VkImageViewCreateInfo imageViewCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .image = vkImage,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = vkFormat,
        .components = {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY
        },
        .subresourceRange = {
            .aspectMask = vkImageAspectFlags, //VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = mipLevels,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };

    VkResult createImageViewResult =
        vkCreateImageView(
            pInstance->Device.Device, &imageViewCreateInfo,
            pInstance->VkInstance.pAllocator, &vkImageView);
    if (svl_result_check(createImageViewResult, "Can't create vk image view!"))
    {
        *pError = SvlErrorType_ImageView;
    }

    *pImageView = vkImageView;
}

SvlImageView
svl_image_view_create(SvlInstance* pInstance, SvlImageViewSettings settings, SvlErrorType* pError)
{
    VkImage vkImage;
    VkDeviceMemory vkDeviceMemory;
    SvlErrorType error = 0;

    _svl_image_create(pInstance, settings.ImageSettings, &vkImage, &vkDeviceMemory, &error);
    if (error != SvlErrorType_None)
    {
        *pError = error;
        return (SvlImageView) {};
    }

    VkImageView vkImageView;

    _svl_image_view_create(pInstance, vkImage,
                           settings.Format,
                           settings.ImageAspectFlags,
                           settings.MipLevels,
                           &vkImageView,
                           &error);
    if (error != SvlErrorType_None)
    {
        *pError = error;
        return (SvlImageView) {};
    }

    SvlImageView imageView = {
        .Image = vkImage,
        .View = vkImageView,
        .Memory = vkDeviceMemory,
    };

    return imageView;
}

void
_svl_render_pass_add_framebuffer(SvlInstance* pInstance, SvlRenderPass* pRenderPass, VkImageView* aAttachments)
{
    VkExtent2D extent = pInstance->Swapchain.Resolution;
    VkDevice device = pInstance->Device.Device;
    VkAllocationCallbacks* pAllocator = pInstance->VkInstance.pAllocator;

    VkFramebufferCreateInfo frabufferCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .renderPass = pRenderPass->RenderPass,
        .attachmentCount = svl_array_count(aAttachments),
        .pAttachments = aAttachments,
        .width = extent.width,
        .height = extent.height,
        .layers = 1
    };

    VkFramebuffer vkFramebuffer;
    VkResult createFramebufferResult =
        vkCreateFramebuffer(device, &frabufferCreateInfo, pAllocator, &vkFramebuffer);
    if (svl_result_check(createFramebufferResult, "Can't create framebuffer!"))
    {

    }

    svl_array_push(pRenderPass->aFramebuffers, vkFramebuffer);
}

void
svl_render_pass_update_images_framebuffers(SvlInstance* pInstance, SvlRenderPass* pRenderPass)
{
    VkAllocationCallbacks* pAllocator = pInstance->VkInstance.pAllocator;
    VkDevice vkDevice = pInstance->Device.Device;
    VkExtent2D extent = pInstance->Swapchain.Resolution;

    svl_i32 notFirstRun = (pRenderPass->aImageViews != NULL || pRenderPass->aSwapImageViews != NULL);

    if (notFirstRun)
    {
        // TODO: Create function for destroying framebuffers, imageviews

        for (svl_i32 i = 0; i < svl_array_count(pRenderPass->aFramebuffers); ++i)
        {
            VkFramebuffer framebuffer = pRenderPass->aFramebuffers[i];
            vkDestroyFramebuffer(vkDevice, framebuffer, pAllocator);
        }

        svl_i32 swapCnt = svl_array_count(pRenderPass->aSwapImageViews);
        for (svl_i32 i = 0; i < swapCnt; ++i)
        {
            SvlSwapImageView image = pRenderPass->aSwapImageViews[i];
            vkDestroyImageView(vkDevice, image.View, pAllocator);
        }

        for (svl_i32 i = 0; i < svl_array_count(pRenderPass->aImageViews); ++i)
        {
            SvlImageView imageView = pRenderPass->aImageViews[i];
            //vsa_texture_image_destroy(item.Image, item.View, item.Memory);
        }

        svl_array_free(pRenderPass->aSwapImageViews);
        svl_array_free(pRenderPass->aImageViews);
        svl_array_free(pRenderPass->aFramebuffers);
    }

    SvlImageView* aImageViews = NULL;
    SvlSwapImageView* aSwapImageViews = NULL;

    svl_i32 attachCount = svl_array_count(pRenderPass->aAttachments);
    for (svl_i32 i = 0; i < attachCount; ++i)
    {
        SvlAttachment attach = pRenderPass->aAttachments[i];

        switch (attach.Type)
        {

        case SvlAttachmentType_Default:
        {
            // NOTE: Mb this cause bug
            SvlImageViewSettings settings = {
                .ImageSettings = {
                    .Width = extent.width,
                    .Height = extent.height,
                    .Depth = 1.0f,
                    .MipLevels = 1,
                    .SamplesCount = attach.Description.samples,
                    .Format = attach.Description.format,
                    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
                    .ImageUsageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
                },
                .Format = attach.Description.format,
                .ImageAspectFlags = VK_IMAGE_ASPECT_COLOR_BIT,
                .MipLevels = 1,
            };

            // note: color image view
            SvlErrorType error = 0;
            SvlImageView imageView = svl_image_view_create(pInstance, settings, &error);
            if (error)
            {
                svl_guard(0);
            }

            svl_array_push(aImageViews, imageView);

            break;
        }

        case SvlAttachmentType_ResolveMultisample:
        {
            SvlImageViewSettings settings = {
                .ImageSettings = {
                    .Width = extent.width,
                    .Height = extent.height,
                    .Depth = 1.0f,
                    .MipLevels = 1,
                    .SamplesCount = attach.Description.samples,
                    .Format = attach.Description.format,
                    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
                    .ImageUsageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
                },
                .Format = attach.Description.format,
                .ImageAspectFlags = VK_IMAGE_ASPECT_COLOR_BIT,
                .MipLevels = 1,
            };

            SvlErrorType error = 0;
            SvlImageView imageView = svl_image_view_create(pInstance, settings, &error);
            if (error)
            {
                svl_guard(0);
            }

            svl_array_push(aImageViews, imageView);
            break;
        }

        case SvlAttachmentType_Depth:
        {
            // TODO: Write find_format for depth instead of using concrete type
            VkFormat depthAttachmentFormat = VK_FORMAT_D32_SFLOAT;

            SvlImageViewSettings settings = {
                .ImageSettings = {
                    .Width = extent.width,
                    .Height = extent.height,
                    .Depth = 1.0f,
                    .MipLevels = 1,
                    .SamplesCount = attach.Description.samples,
                    .Format = depthAttachmentFormat,
                    .ImageTiling = VK_IMAGE_TILING_OPTIMAL,
                    .ImageUsageFlags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                    .MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
                },
                .Format = depthAttachmentFormat,
                .ImageAspectFlags = VK_IMAGE_ASPECT_DEPTH_BIT,
                .MipLevels = 1,
            };

            SvlErrorType error = 0;
            SvlImageView imageView = svl_image_view_create(pInstance, settings, &error);
            if (error)
            {
                svl_guard(0);
            }

            svl_array_push(aImageViews, imageView);

            break;
        }

        case SvlAttachmentType_SwapChained:
        {
            VkFormat vkFormat = attach.Description.format;
            SvlErrorType error;
            aSwapImageViews = _svl_swapchain_image_views_create(pInstance, vkFormat, &error);

            break;
        }

        }
    }

    pRenderPass->aImageViews = aImageViews;
    pRenderPass->aSwapImageViews = aSwapImageViews;

    if (pRenderPass->aSwapImageViews)
    {
        svl_i32 framebuffersCount = svl_array_count(pRenderPass->aSwapImageViews);
        for (svl_i32 f = 0; f < framebuffersCount; ++f)
        {
            SvlSwapImageView swapImageView = pRenderPass->aSwapImageViews[f];

            VkImageView* aAttachments = NULL;
            {
                svl_i64 i, count = svl_array_count(pRenderPass->aImageViews);
                for (i = 0; i < count; ++i)
                {
                    SvlImageView imageView = pRenderPass->aImageViews[i];
                    svl_array_push(aAttachments, imageView.View);
                }

                svl_array_push(aAttachments, swapImageView.View);
            }

            _svl_render_pass_add_framebuffer(pInstance, pRenderPass, aAttachments);

            svl_array_free(aAttachments);
        }

        return;
    }

    VkImageView* aAttachments = NULL;
    svl_i64 i, count = svl_array_count(pRenderPass->aImageViews);
    for (i = 0; i < count; ++i)
    {
        SvlImageView imageView = pRenderPass->aImageViews[i];
        svl_array_push(aAttachments, imageView.View);
    }

    _svl_render_pass_add_framebuffer(pInstance, pRenderPass, aAttachments);

}

SvlRenderPass
svl_render_pass_create(SvlInstance* pInstance, SvlRenderPassSettings settings, SvlErrorType* pError)
{
    // VsaRenderPass vsa_render_pass_new(VsaRenderPassSettings settings)

    VkDevice vkDevice = pInstance->Device.Device;
    VkAllocationCallbacks* pAllocator = pInstance->VkInstance.pAllocator;

    /*
      DEPENDS:
      * gVulkanExtent
      * gVulkanSurfaceFormat
      * gVulkanSamplesCount
      * pInstance->Device.Device
      */

    VkAttachmentDescription* allDescrs = NULL;
    VkAttachmentReference* aColorRefs = NULL;
    VkAttachmentReference* pResolvedRef = NULL;
    VkAttachmentReference* pDepthRef = NULL;

    SvlAttachment* aAttachments = NULL;
    VkSampleCountFlagBits samples = VK_SAMPLE_COUNT_1_BIT;

    for (svl_i64 i = 0; i < settings.AttachmentsCount; ++i)
    {
        settings.aAttachments[i].Reference.attachment = i;
        SvlAttachment attach = settings.aAttachments[i];
        svl_array_push(aAttachments, attach);

        if (settings.aAttachments[i].Description.samples > samples)
            samples = settings.aAttachments[i].Description.samples;

        switch (attach.Type)
        {

        case SvlAttachmentType_Default:
            svl_array_push(aColorRefs, settings.aAttachments[i].Reference);
            break;

        case SvlAttachmentType_Depth:
            svl_guard(pDepthRef == NULL);
            pDepthRef = &settings.aAttachments[i].Reference;
            break;

        case SvlAttachmentType_ResolveMultisample:
            svl_guard(pResolvedRef == NULL);
            pResolvedRef = &settings.aAttachments[i].Reference;
            break;

        case SvlAttachmentType_SwapChained:
            svl_guard(pResolvedRef == NULL);
            pResolvedRef = &settings.aAttachments[i].Reference;
            break;

        }

        svl_array_push(allDescrs, attach.Description);
    }

    if (pInstance->VkInstance.IsDebugMode)
    {
        svl_i32 attachmentSwapChainedExist = 0;
        svl_i32 attachmentMultisampleResolveExist = 0;

        for (svl_i32 i = 0; i < svl_array_count(aAttachments); ++i)
        {
            SvlAttachment item = aAttachments[i];
            if (item.Type == SvlAttachmentType_SwapChained)
            {
                attachmentSwapChainedExist = 1;
            }

            if (item.Type == SvlAttachmentType_ResolveMultisample)
            {
                attachmentMultisampleResolveExist = 1;
            }
        }

        if (attachmentSwapChainedExist && attachmentMultisampleResolveExist)
        {
            // GERROR("For some weird reason u have Multisample Resolve Attachment and Swap Chained Attachment, it's not allowed!\n");
            *pError = SvlErrorType_RenderPass;
            return (SvlRenderPass) {};
        }
    }

    VkSubpassDescription subpassDescription = {
        .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
        .colorAttachmentCount = svl_array_count(aColorRefs),
        .pColorAttachments = aColorRefs,
        .pResolveAttachments = pResolvedRef,
        .pDepthStencilAttachment = pDepthRef,

        // NOTE: Just for doc
        .inputAttachmentCount = 0,
        .pInputAttachments = NULL,
        .preserveAttachmentCount = 0,
        .pPreserveAttachments = NULL
    };

    VkRenderPassCreateInfo renderPassCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .pNext = NULL,
        // NOTE: Maybe in some cases it may cause a problem
        .attachmentCount = svl_array_count(allDescrs),
        .pAttachments = allDescrs,
        .subpassCount = 1,
        .pSubpasses = &subpassDescription,
        .dependencyCount = 1,
        .pDependencies = &settings.Dependency
    };

    VkRenderPass vkRenderPass;
    VkResult createRenderPassResult =
        vkCreateRenderPass(vkDevice, &renderPassCreateInfo, pAllocator, &vkRenderPass);
    if (svl_result_check(createRenderPassResult, "Can't create RenderPass!"))
    {
        *pError = SvlErrorType_RenderPass;
        return (SvlRenderPass) {};
    }

    SvlRenderPass renderPass = {
        .Type = settings.Type,
        .RenderPass = vkRenderPass,
        .aAttachments = aAttachments,
        .Samples = samples,
    };

    // DOCS: Create image views and framebuffers
    svl_render_pass_update_images_framebuffers(pInstance, &renderPass);
    //vsa_render_pass_update_images_and_framebuffers(&renderPass);

    svl_array_free(allDescrs);
    svl_array_free(aColorRefs);

    svl_array_push(pInstance->aRenderPasses, renderPass);

    return renderPass;
}

void
svl_render_pass_update(SvlInstance* pInstance, SvlRenderPass* pRenderPass, SvlFrame* pFrame, SvlErrorType* pError)
{
    SvlPipeline** apPipelines = pRenderPass->apPipelines;
    svl_i64 pipelinesCount = svl_array_count(apPipelines);

    VkCommandBuffer cmd = pInstance->Cmd.Buffer;

    VkClearValue aClearValues[2] = {
        [0] = (VkClearValue) {
            .color = pRenderPass->ClearColor, //{ 0.0034f, 0.0037f, 0.0039f, 1 }
            //.color = { 0.0934f, 0.0937f, 0.0939f, 1 }
        },
        [1] = (VkClearValue) {
            .depthStencil = { 1.0f, 0.0f }
        }
    };

    VkFramebuffer currentFramebuffer = pRenderPass->aFramebuffers[pFrame->ImageIndex];
    VkExtent2D vkExtent = pInstance->Swapchain.Resolution;

    VkRenderPassBeginInfo renderPassBeginInfo = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .pNext = NULL,
        .renderPass = pRenderPass->RenderPass,
        .framebuffer = currentFramebuffer,
        .renderArea = {
            .offset = {	0, 0 },
            .extent = vkExtent
        },
        .clearValueCount = 2,
        .pClearValues = aClearValues
    };
    vkCmdBeginRenderPass(cmd, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

    //TODO(typedef): set in pipeline
    VkViewport viewport = {
        .x = 0,
        .y = 0,
        .width = vkExtent.width,
        .height = vkExtent.height,
        .minDepth = 0,
        .maxDepth = 1
    };
    //printf("%d %d\n", vkExtent.width, vkExtent.height);
    vkCmdSetViewport(cmd, 0, 1, &viewport);

    //TODO(typedef): set in pipeline
    VkRect2D scissor = {
        .offset = { 0, 0 },
        .extent = vkExtent
    };
    vkCmdSetScissor(cmd, 0, 1, &scissor);

    /* DO_ONES(array_foreach(apPipelines, GERROR("Pipeline: %s\n", item->pName))); */

    svl_guard(pipelinesCount > 0);
    for (svl_i32 i = 0; i < pipelinesCount; ++i)
    {
        SvlPipeline* pSvlPipeline = apPipelines[i];

        /* if (!pSvlPipeline->IsPipelineRenderable) */
        /* { */
        /*     //DO_MANY_TIME(GWARNING("Not rendere pipeline!\n"), 5); */
        /*     continue; */
        /* } */

        /* vsa_pipeline_update_uniforms(pSvlPipeline); */
        /* vsa_pipeline_update_descriptors(pSvlPipeline); */

        vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pSvlPipeline->Pipeline);
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pSvlPipeline->PipelineLayout, 0, 1, &pSvlPipeline->Binding.DescriptorSet, 0, NULL);

        //svl_i32 drawCallsCount = array_count(pSvlPipeline->DrawCalls);
        //GINFO("DrawCalls Count: %d\n", drawCallsCount);
        //for (svl_i32 d = 0; d < drawCallsCount; ++d)
        {
            //VsaDrawCall vsaDrawCall = pSvlPipeline->DrawCalls[d];


            VkDeviceSize offsets[1] = { 0 };
            vkCmdBindVertexBuffers(cmd, 0, 1, &pSvlPipeline->Vertex.Gpu, offsets);
            vkCmdBindIndexBuffer(cmd, pSvlPipeline->Index.Gpu, 0, VK_INDEX_TYPE_UINT32);

            vkCmdDrawIndexed(cmd, pSvlPipeline->IndicesCount, 1, 0, 0, 0);

            /* if (vsaDrawCall.Flags == VsaDrawCallFlags_HaveScissors) */
            /* { */
            /*	vkCmdSetScissor(cmd, 0, 1, &scissor); */
            /* } */
        }

    }

    vkCmdEndRenderPass(cmd);

}

/*#######################
  DOCS(typedef): Buffer
  #######################*/

VkBuffer
_svl_buffer_create(SvlInstance* pInstance, VkBufferUsageFlags usage, VkDeviceSize size, VkMemoryPropertyFlags memoryPropertyFlags, VkDeviceMemory* deviceMemory)
{
    VkBuffer vkBuffer;

    VkBufferCreateInfo vkCreateBufferCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = NULL,
        .flags = 0,
        .size = size,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = NULL
    };

    VkResult createBufferResult = vkCreateBuffer(pInstance->Device.Device, &vkCreateBufferCreateInfo, pInstance->VkInstance.pAllocator, &vkBuffer);
    if (svl_result_check(createBufferResult, "Failed Vertex Buffer creation!"))
    {
    }

    //NOTE(typedef): Allocating memory for VkBuffer
    VkMemoryRequirements vertexBufferMemoryRequirements;
    vkGetBufferMemoryRequirements(pInstance->Device.Device, vkBuffer, &vertexBufferMemoryRequirements);

    // NOTE(typedef): Find suitable memory type
    svl_i32 index = svl_find_memory_type_index(
        pInstance,
        vertexBufferMemoryRequirements.memoryTypeBits,
        memoryPropertyFlags);

    VkMemoryAllocateInfo memoryAllocateInfo = {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .pNext = NULL,
        .allocationSize = vertexBufferMemoryRequirements.size,
        .memoryTypeIndex = index
    };

    VkResult allocateMemoryResult = vkAllocateMemory(pInstance->Device.Device, &memoryAllocateInfo, pInstance->VkInstance.pAllocator, deviceMemory);
    if (svl_result_check(allocateMemoryResult, "Failed triangle deivce memory allocation!"))
    {
    }

    VkResult bindBufferMemoryResult = vkBindBufferMemory(pInstance->Device.Device, vkBuffer, *deviceMemory, 0);
    if (svl_result_check(bindBufferMemoryResult, "Failed binding buffer memory with buffer!"))
    {
    }

    return vkBuffer;

}

void
svl_buffer_stage_and_gpu_create(SvlInstance* pInstance, VkBufferUsageFlags usage, size_t size, VkBuffer* pGpuBuffer, VkDeviceMemory* pDeviceMemory, VkBuffer* pStagingBuffer, VkDeviceMemory* pStagingMemory, SvlErrorType* pError)
{
    VkBuffer stagingBuffer = _svl_buffer_create(
        pInstance,
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        size,
        (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
        pStagingMemory);

    VkBuffer gpuBuffer = _svl_buffer_create(
        pInstance,
        (VK_BUFFER_USAGE_TRANSFER_DST_BIT | usage),
        size,
        (VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT),
        pDeviceMemory);

    *pGpuBuffer = gpuBuffer;
    *pStagingBuffer = stagingBuffer;
}

SvlBuffer
svl_buffer_create(SvlInstance* pInstance, SvlBufferSettings set, SvlErrorType* pError)
{
    VkBuffer stage, gpu;
    VkDeviceMemory stageMem, gpuMem;

    svl_buffer_stage_and_gpu_create(pInstance,
                                    set.Usage, set.Size,
                                    &gpu, &gpuMem,
                                    &stage, &stageMem,
                                    pError);

    SvlBuffer buffer = {
        .Staging = stage,
        .Gpu = gpu,
        .StagingMemory = stageMem,
        .GpuMemory = gpuMem,
    };

    return buffer;
}

void
svl_buffer_set_data(SvlInstance* pInstance, SvlBuffer* pBuffer, void* pData, svl_size offset, svl_size size)
{
    svl_guard(pData != NULL);
    svl_guard(size > 0);

    VkDevice device = pInstance->Device.Device;
    VkCommandPool pool = pInstance->Cmd.Pool;

    void* data;
    VkResult vkMapResult = vkMapMemory(device, pBuffer->StagingMemory, offset, size, 0, &data);
    if (svl_result_check(vkMapResult, "Failed mapping memory!"))
    {
    }

    memcpy(data, pData, size);
    vkUnmapMemory(device, pBuffer->StagingMemory);

    VkCommandBuffer cmdBuffer = svl_cmd_begin(pInstance);

    VkBufferCopy copyRegion = {
        .srcOffset = offset,
        .dstOffset = offset,
        .size = size,
    };

    vkCmdCopyBuffer(cmdBuffer, pBuffer->Staging, pBuffer->Gpu, 1, &copyRegion);

    svl_cmd_end(pInstance, cmdBuffer);
}

void
svl_buffer_destroy(SvlInstance* pInstance, SvlBuffer* pBuffer)
{
    VkDevice device = pInstance->Device.Device;
    VkAllocationCallbacks* pAllocator = pInstance->VkInstance.pAllocator;

    vkDestroyBuffer(device, pBuffer->Staging, pAllocator);
    vkFreeMemory(device, pBuffer->StagingMemory, pAllocator);
    vkDestroyBuffer(device, pBuffer->Gpu, pAllocator);
    vkFreeMemory(device, pBuffer->GpuMemory, pAllocator);
}


/*#######################
  DOCS(typedef): Uniform
  #######################*/

SvlUniform
svl_uniform_create(SvlInstance* pInstance, SvlUniformSettings set)
{
    SvlErrorType error = 0;

    SvlBuffer svlBuffer = svl_buffer_create(
        pInstance,
        (SvlBufferSettings) {
            .Size = set.Size,
            .Usage =  VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        }, &error);

    svl_guard(error == SvlErrorType_None);

    SvlUniform svlUniform = {
        .Size = set.Size,
        .ItemSize = set.ItemSize,
        .pName = set.pName,
        .BindIndex = set.BindIndex,
        .Buffer = svlBuffer,
    };

    return svlUniform;
}


void
svl_uniform_destroy(SvlInstance* pInstance, SvlUniform* pUniform)
{
    svl_buffer_destroy(pInstance, &pUniform->Buffer);
}

/*#######################
  DOCS(typedef): Pipeline
  #######################*/

void
svl_uniform_set(SvlInstance* pInstance, SvlPipeline* pPipeline, SvlUniform* pUniform, void* pData, svl_size offset, svl_size size)
{
    if ((offset + size) > pUniform->Size)
        return;

    VkDevice vkDevice = pInstance->Device.Device;

    svl_buffer_set_data(pInstance, &pUniform->Buffer, pData, offset, size);

    svl_i64 allCount = pUniform->Size / pUniform->ItemSize;

    VkDescriptorBufferInfo bufferInfo = {
        .buffer = pUniform->Buffer.Gpu,
        .offset = offset,
        .range = size
    };

    // NOTE(FEATURE0): VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT VkDescriptorSetLayoutBindingFlagsCreateInfo
    VkWriteDescriptorSet vkWriteDescriptorSet = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = NULL,
        .dstSet = pPipeline->Binding.DescriptorSet,
        .dstBinding = pUniform->BindIndex,
        .dstArrayElement = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        .pImageInfo = NULL,
        .pBufferInfo = &bufferInfo, //bufferInfos,
        .pTexelBufferView = NULL
    };

    // NOTE/TODO(): Can update everything in one call
    VkWriteDescriptorSet vkWriteDescriptorSets[] = {
        [0] = vkWriteDescriptorSet
    };

    vkUpdateDescriptorSets(vkDevice, 1, vkWriteDescriptorSets, 0, NULL);

}


/*#######################
  DOCS(typedef): Cmd
  #######################*/

VkCommandBuffer
svl_cmd_begin(SvlInstance* pInstance)
{
    VkCommandBufferAllocateInfo vkCmdBufferAllocatedInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = NULL,
        .commandPool = pInstance->Cmd.Pool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };

    VkCommandBuffer vkCmdBuffer;
    VkResult vkAllocateCmdBuffersResult =
        vkAllocateCommandBuffers(pInstance->Device.Device, &vkCmdBufferAllocatedInfo, &vkCmdBuffer);
    if (svl_result_check(vkAllocateCmdBuffersResult, "Failed cmd buffer allocation!\n"))
    {
    }

    VkCommandBufferBeginInfo beginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = NULL,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = NULL
    };
    vkBeginCommandBuffer(vkCmdBuffer, &beginInfo);

    return vkCmdBuffer;
}

void
svl_cmd_end(SvlInstance* pInstance, VkCommandBuffer vkCmdBuffer)
{
    VkResult vkEndCmdBufferResult = vkEndCommandBuffer(vkCmdBuffer);
    if (svl_result_check(vkEndCmdBufferResult, "Failed end cmd buffer!"))
    {
    }

    // Execute command
    VkSubmitInfo submitInfo = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext = NULL,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = NULL,
        .pWaitDstStageMask = NULL,
        .commandBufferCount = 1,
        .pCommandBuffers = &vkCmdBuffer,
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = NULL
    };

    vkQueueSubmit(pInstance->GraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(pInstance->GraphicsQueue);

    // Free command for buffer
    vkFreeCommandBuffers(pInstance->Device.Device, pInstance->Cmd.Pool, 1, &vkCmdBuffer);
}

/*#######################
  DOCS(typedef): Frame
  #######################*/

void
svl_frame_start(SvlInstance* pInstance, SvlFrame* pFrame)
{
    /*
      NOTE(typedef): Render Loop
      * Wait for the previous frame to finish
      * Acquire an image from the swap chain
      * Reset cmd buffer
      */

    VkDevice device = pInstance->Device.Device;
    VkSwapchainKHR swapchain = pInstance->Swapchain.Swapchain;
    VkSemaphore imageAvailable = pInstance->Sync.ImageAvailableSemaphore;
    VkFence* pFrameFence = &pInstance->Sync.FrameFence;
    VkCommandBuffer cmdBuf = pInstance->Cmd.Buffer;

    static svl_u64 noTime = (18446744073709551615ULL);
    vkWaitForFences(device, 1, pFrameFence, VK_TRUE, noTime);

    VkResult accuireNextImageResult =
        vkAcquireNextImageKHR(device, swapchain, noTime, imageAvailable, VK_NULL_HANDLE, &pFrame->ImageIndex);
    if (accuireNextImageResult == VK_ERROR_OUT_OF_DATE_KHR)
    {
        // todo: impl
        // GINFO("Recreate!\n");
        // vsa_swapchain_objects_recreate();
        return;
    }
    else if (accuireNextImageResult == VK_SUBOPTIMAL_KHR)
    {
        // DOCS(typedef): this thing is not an error or smth, just continue there
    }
    else
    {
        // vkValidResult(accuireNextImageResult, "Can't acquire next image khr!");
    }

    vkResetFences(device, 1, pFrameFence);

    const svl_i32 no_flag = 0;
    vkResetCommandBuffer(cmdBuf, no_flag);
}

void
svl_frame_end(SvlInstance* pInstance, SvlFrame* pFrame)
{
    /*
      NOTE(typedef):
      * Submit the recorded command buffer
      * Present the swap chain image
      */

    VkQueue graphQueue = pInstance->GraphicsQueue;
    VkSwapchainKHR swapchain = pInstance->Swapchain.Swapchain;
    VkSemaphore* pImageAvailable = &pInstance->Sync.ImageAvailableSemaphore;
    VkSemaphore* pRenderFinished = &pInstance->Sync.RenderFinishedSemaphore;
    VkFence frameFence = pInstance->Sync.FrameFence;
    VkCommandBuffer* pCmdBuf = &pInstance->Cmd.Buffer;

    VkPipelineStageFlags pipelineStageFlag = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext = NULL,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = pImageAvailable,
        .pWaitDstStageMask = &pipelineStageFlag,
        .commandBufferCount = 1,
        .pCommandBuffers = pCmdBuf,
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = pRenderFinished
    };

    // NOTE(typedef): Submiting Recorded Commands
    VkResult queueSubmitResult =
        vkQueueSubmit(graphQueue, 1, &submitInfo, frameFence);
    //vkValidResult(queueSubmitResult, "Can't Submit Queue!");

    VkPresentInfoKHR presentInfoKhr = {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .pNext = NULL,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = pRenderFinished,
        .swapchainCount = 1,
        .pSwapchains = &swapchain,
        .pImageIndices = (const svl_u32*) &pFrame->ImageIndex,
        .pResults = NULL
    };

    VkResult queuePresentKhr =
        vkQueuePresentKHR(graphQueue, &presentInfoKhr);
    if (queuePresentKhr == VK_ERROR_OUT_OF_DATE_KHR || queuePresentKhr == VK_SUBOPTIMAL_KHR /* || gIsWindowBeenResized */)
    {
        //GINFO("Recreate!\n");
        //gIsWindowBeenResized = 0;
        //vsa_swapchain_objects_recreate();
        return;
    }
    else
    {
        // vkValidResult(queuePresentKhr, "Queue Present KHR!\n");
    }

}

/*#######################
  DOCS(typedef): Helpers Api
  #######################*/

const char*
svl_result_to_string(VkResult vkResult)
{
    switch (vkResult)
    {

    case VK_ERROR_OUT_OF_HOST_MEMORY: return "VK_ERROR_OUT_OF_HOST_MEMORY";
    case VK_ERROR_OUT_OF_DEVICE_MEMORY: return "VK_ERROR_OUT_OF_DEVICE_MEMORY";

    case VK_SUCCESS: return "VK_SUCCESS";
    case VK_NOT_READY: return "VK_NOT_READY";
    case VK_TIMEOUT: return "VK_TIMEOUT";
    case VK_EVENT_SET: return "VK_EVENT_SET";
    case VK_EVENT_RESET: return "VK_EVENT_RESET";
    case VK_INCOMPLETE: return "VK_INCOMPLETE";
    case VK_ERROR_INITIALIZATION_FAILED: return "VK_ERROR_INITIALIZATION_FAILED";

    case VK_ERROR_DEVICE_LOST: return "VK_ERROR_DEVICE_LOST";
    case VK_ERROR_MEMORY_MAP_FAILED: return "VK_ERROR_MEMORY_MAP_FAILED";
    case VK_ERROR_LAYER_NOT_PRESENT: return "VK_ERROR_LAYER_NOT_PRESENT";
    case VK_ERROR_EXTENSION_NOT_PRESENT: return "VK_ERROR_EXTENSION_NOT_PRESENT";
    case VK_ERROR_FEATURE_NOT_PRESENT: return "VK_ERROR_FEATURE_NOT_PRESENT";
    case VK_ERROR_INCOMPATIBLE_DRIVER: return "VK_ERROR_INCOMPATIBLE_DRIVER";
    case VK_ERROR_TOO_MANY_OBJECTS: return "VK_ERROR_TOO_MANY_OBJECTS";
    case VK_ERROR_FORMAT_NOT_SUPPORTED: return "VK_ERROR_FORMAT_NOT_SUPPORTED";
    case VK_ERROR_FRAGMENTED_POOL: return "VK_ERROR_FRAGMENTED_POOL";
    case VK_ERROR_UNKNOWN: return "VK_ERROR_UNKNOWN";
    case VK_ERROR_OUT_OF_POOL_MEMORY: return "VK_ERROR_OUT_OF_POOL_MEMORY";
    case VK_ERROR_INVALID_EXTERNAL_HANDLE: return "VK_ERROR_INVALID_EXTERNAL_HANDLE";

    case VK_ERROR_FRAGMENTATION: return "VK_ERROR_FRAGMENTATION";
    case VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS: return "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS";
    case VK_PIPELINE_COMPILE_REQUIRED: return "VK_PIPELINE_COMPILE_REQUIRED";
    case VK_ERROR_SURFACE_LOST_KHR: return "VK_ERROR_SURFACE_LOST_KHR";
    case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR";
    case VK_SUBOPTIMAL_KHR: return "K_SUBOPTIMAL_KHR";
    case VK_ERROR_OUT_OF_DATE_KHR: return "VK_ERROR_OUT_OF_DATE_KHR";
    case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR";
    case VK_ERROR_VALIDATION_FAILED_EXT: return "VK_ERROR_VALIDATION_FAILED_EXT";
    case VK_ERROR_INVALID_SHADER_NV: return "VK_ERROR_INVALID_SHADER_NV";
    case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT: return "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT";
    case VK_ERROR_NOT_PERMITTED_KHR: return "VK_ERROR_NOT_PERMITTED_KHR";
    case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT: return "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT";

    case VK_THREAD_IDLE_KHR: return "VK_THREAD_IDLE_KHR";
    case VK_THREAD_DONE_KHR: return "VK_THREAD_DONE_KHR";
    case VK_OPERATION_DEFERRED_KHR: return "VK_OPERATION_DEFERRED_KHR";
    case VK_OPERATION_NOT_DEFERRED_KHR: return "VK_OPERATION_NOT_DEFERRED_KHR";
    case VK_ERROR_COMPRESSION_EXHAUSTED_EXT: return "VK_ERROR_COMPRESSION_EXHAUSTED_EXT";

    case VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR";
    case VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR";
    case VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR";
    case VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR";
    case VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR";
    case VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR: return "VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR";
    case VK_ERROR_INCOMPATIBLE_SHADER_BINARY_EXT: return "VK_ERROR_INCOMPATIBLE_SHADER_BINARY_EXT";
    case VK_RESULT_MAX_ENUM: return "VK_RESULT_MAX_ENUM";

    }

    return "Unknown result! Mb update is required for vkResultToString";
}

void
_svl_guard(svl_i32 condition, const char* pCond, svl_i32 line, const char* pFile)
{
    if (condition)
        return;

    printf(SvlTerminalRed("Error")" %s on line %d in file %s\n", pCond, line, pFile);
    svl_i32* i = (void*)0;
    *i = 1337;
}

svl_i32
svl_result_check(VkResult vkResult, const char* pMessage)
{
    if (vkResult == VK_SUCCESS)
        return 0;

    const char* pDescr = svl_result_to_string(vkResult);
    printf(SvlTerminalRed("Svl-Error: %s, message: %s")"\n", pDescr, pMessage);
    svl_guard(vkResult == VK_SUCCESS);

    return 1;
}

#endif // VULKAN_SIMPLE_LIBRARY_IMPLEMENTATION


#endif // SIMPLE_VULKAN_LIBRARY_H
