#include "SimpleAudioEngine.h"

#include <Core/SimpleStandardLibrary.h>

SimpleAudioEngine
sae_create()
{
    ma_engine* pMaEngine = (ma_engine*) memory_allocate(sizeof(ma_engine));

    ma_engine_config conf = ma_engine_config_init();
    ma_result result = ma_engine_init(&conf, pMaEngine);
    if (result != MA_SUCCESS)
    {
        GERROR("Failed create SimpleAudioEngine!\n");
        return (SimpleAudioEngine) {};
    }

    SimpleAudioEngine saEngine = {
        .IsInitialized = 1,
        .pInternalEngine = pMaEngine,
    };

    return saEngine;
}

void
sae_destroy(SimpleAudioEngine* pSaEngine)
{
    pSaEngine->IsInitialized = 0;
    ma_engine_uninit(pSaEngine->pInternalEngine);
    memory_free(pSaEngine->pInternalEngine);
}

// NOTE: call it from asset_manager
LoadSimpleSoundResult
sae_load_sound(SimpleAudioEngine* pSaEngine, SimpleSoundSetting setting)
{
    if (!pSaEngine->IsInitialized)
    {
        GERROR("SimpleAudioEngine is not initilized!\n");
        return (LoadSimpleSoundResult){.IsSuccess = 0};
    }

    // todo: learn more about ma_sound_group / ma_sound_group_init(
    // note: we can load everything async and then wait until loading is completed
    // todo: MA_SOUND_FLAG_STREAM for soundtrack
    ma_sound sound;
    ma_result result = ma_sound_init_from_file(pSaEngine->pInternalEngine, setting.pPath, 0, NULL, NULL, &sound);
    if (result != MA_SUCCESS)
    {
        return (LoadSimpleSoundResult){.IsSuccess = 0};
    }

    LoadSimpleSoundResult res = {
        .IsSuccess = 1,
        .Sound = (SimpleSound){
            .InternalSound = sound,
        },
    };

    return res;
}

void
sae_unload_sound(SimpleSound* pSound)
{
    ma_sound_uninit(&pSound->InternalSound);
}

// listener
void
sae_listener_set_position(SimpleAudioEngine* pSaEngine, v3 position)
{
    i32 listenerIndex = 0; // do i need more then one listener???
    ma_engine_listener_set_position(pSaEngine->pInternalEngine, listenerIndex, position.X, position.Y, position.Z);
}
