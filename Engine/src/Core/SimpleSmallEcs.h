#ifndef SIMPLE_SMALL_ECS
#define SIMPLE_SMALL_ECS


// DOCS:
// SSE_IS_EASY_API

/*#####################################
  ##        SimpleSmallEcs           ##
  #####################################

  SimpleSmallEcs - small c99 impl of ecs, entities and components
  programmed inside lib, but system is user-space problem, user
  can get group to build system on top of it.

  You can define SSE_DEFINE_SIMPLIFIED_API in header file for easy api.
  It will store current world instance and give you ability for
  calling simplified methods:

  ```C
  #define SSE_DEFINE_SIMPLIFIED_API
  ```

  Usage example:
  ```C
  TransformComponent tc = {};

  SseWorld* pWorld = sse_world_create();
  sse_i32 isRegistered = sse_world_component_register(pWorld, sse_ctos(TransformComponent), sizeof(TransformComponent));
  if (!isRegistered)
  {
      // handle error
  }

  SseEntity sseEntity = sse_entity_create(pWorld);
  sse_entity_set_component(pWolrd, sseEntity, sse_ctos(TransformComponent), &tc, sizeof(tc));
  TransformComponent* pTc = sse_entity_get_component(pWorld, sseEntity, sse_ctos(TransformComponent));
  ```



  ######## Call functions in type-safe way
  Use sse_ctos when made call of a function, that uses const char*
  pComponentName as an argument, that function saves you from typo error,
  like this:
  ```C
  sse_ctos(TranformComponent)
  // instead of
  sse_ctos(TransformComponent)
  ```
  It give you compile time error




  // NOTE: /// not ready part ///
  // NOTE: /// not ready part ///
  // NOTE: /// not ready part ///

  // todo: move to documentation
    // DOCS: Offset in Component raw data
    // so we can get component data by using this formula:
    // RealComponent* pComp = (RealComponent*)(ComponentRawData + .Offset)

   todo: move to documentation
   NOTE: for each_entity we have:
   size:
   * 8 byte - for index
   * 16 byte * componentsCount - for storing component data


   // todo: add back-end for function, do not depend on anything

*/

#define SSE_FLAG_DEBUG 1

/*#####################################
  ##            Types.h              ##
  #####################################*/

typedef enum SseIdRangeType
{
    SseIdRangeType_Component_Invalid = -2,
    SseIdRangeType_Entity_Invalid = -1,

    SseIdRangeType_Component_Begin = 16,
    SseIdRangeType_Component_End = 512,

    SseIdRangeType_Entity_Begin,
    SseIdRangeType_Entity_End = 9223372036854775807LL,
} SseIdRangeType;


#if !defined(sse_i32)
typedef int sse_i32;
#endif

#if !defined(sse_i64)
typedef long long int sse_i64;
#endif

#if !defined(sse_size)
typedef unsigned long sse_size;
#endif

typedef sse_i64 SseEntityId;
typedef sse_i64 SseComponentId;
typedef sse_i64 SseWorldId;

#define sse_ctos(componentStruct) ({sizeof(componentStruct); #componentStruct;})

typedef struct SseEntity
{
    SseEntityId Id;
    // some stuf for records, like what components entity contains
} SseEntity;

typedef struct ComponentTuple
{
    SseComponentId ComponentId;
    sse_size Offset;
} ComponentTuple;

typedef struct SseEntityRecord
{
    ComponentTuple* aComponents;
} SseEntityRecord;

typedef struct SseEntityStorage
{
    SseEntityId NextId;
    SseEntityId* aDeletedEntities;
    SseEntityRecord* aEntityRecords;
} SseEntityStorage;

typedef struct SseComponentRecord
{
    const char* pName;
    sse_size Size;
    void* pData;
} SseComponentRecord;

typedef struct SseComponentStorage
{
    SseComponentId NextId;
    struct {const char* Key; SseComponentId Value;}* hNameToId;
    SseComponentRecord* aComponentRecords;
} SseComponentStorage;

typedef struct SseWorld
{
    SseWorldId Id;

    SseEntityStorage EntityStorage;
    SseComponentStorage ComponentStorage;
} SseWorld;


/*#####################################
  ##         Verbose api.h           ##
  #####################################*/

SseWorld* sse_world_create();
void sse_world_destroy(SseWorld* pSseWorld);
sse_i32 sse_world_component_register(SseWorld* pSseWorld, const char* pComponentName, sse_size componentSize);
sse_i32 sse_world_is_component_registered_by_name(SseWorld* pSseWorld, const char* pComponentName);
sse_i32 sse_world_is_component_registered_by_id(SseWorld* pSseWorld, SseComponentId id);

//                Components
SseComponentId sse_component_name_to_id(SseWorld* pWorld, const char* pName);

//            Components / Entity Ids
sse_i32 sse_component_validate_id(SseComponentId id);
sse_i32 sse_entity_validate_id(SseEntityId id);
sse_i64 sse_component_id_to_index(SseComponentId id);
sse_i64 sse_entity_id_to_index(SseEntityId id);
SseComponentId sse_component_index_to_id(sse_i64 id);
SseEntityId sse_entity_index_to_id(sse_i64 id);


//                   Entity
SseEntity sse_entity_create(SseWorld* pWorld);
void* sse_entity_get_component_by_id(SseWorld* pWorld, SseEntity entity, SseComponentId compId);
void* sse_entity_get_component(SseWorld* pWorld, SseEntity entity, const char* pComponentName);
sse_i32 sse_entity_set_component_by_id(SseWorld* pWorld, SseEntity entity, SseComponentId compId, void* pComponentData, sse_size componentSize);
sse_i32 sse_entity_set_component(SseWorld* pWorld, SseEntity entity, const char* pComponentName, void* pComponentData, sse_size componentSize);

#endif // SIMPLE_SMALL_ECS
