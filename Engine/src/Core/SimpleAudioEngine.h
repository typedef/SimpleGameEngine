#ifndef SIMPLE_AUDIO_ENGINE_H
#define SIMPLE_AUDIO_ENGINE_H

#include <Core/Types.h>

// todo: get rid off this header in .h
#include <Deps/miniaudio.h>

/*
  for sound manipulation use directly miniaudio api:

  1111

  sound:

  listener:
  ma_engine_listener_set_position(&)

*/

typedef struct SimpleAudioEngine
{
    i8 IsInitialized;
    ma_engine* pInternalEngine;
} SimpleAudioEngine;

typedef struct SimpleSoundSetting
{
    const char* pPath;
} SimpleSoundSetting;

typedef struct SimpleSound
{
    v3 Position;
    v3 Direction;
    v3 Velocity;
    ma_sound InternalSound;
} SimpleSound;

typedef struct LoadSimpleSoundResult
{
    i32 IsSuccess;
    SimpleSound Sound;
} LoadSimpleSoundResult;


SimpleAudioEngine sae_create();
void sae_destroy(SimpleAudioEngine* pSaEngine);

LoadSimpleSoundResult sae_load_sound(SimpleAudioEngine* pSaEngine, SimpleSoundSetting setting);
void sae_unload_sound(SimpleSound* pSound);


#define sae_sound_play(pSound) ({ma_sound_start(&pSound->InternalSound);})
#define sae_sound_stop(pSound) ({ma_sound_stop(&pSound->InternalSound);})
#define sae_sound_is_playind(pSound) ({ma_sound_is_playing(&pSound->InternalSound);})
#define sae_sound_set_looping(pSound) ({ma_sound_set_looping(&pSound->InternalSound, 1);})

#define sae_sound_set_volume(pSound, vol) ({ma_sound_set_volume(&pSound->InternalSound, vol);})
#define sae_sound_get_volume(pSound, vol) ({ma_sound_get_volume(&pSound->InternalSound);})

#define sae_sound_set_position(pSound, pos) ({pSound->Position = pos; ma_sound_set_position(&pSound->InternalSound, pos.X, pos.Y, pos.Z);})
#define sae_sound_set_direction(pSound, dir) ({pSound->Direction = dir; ma_sound_set_direction(&pSound->InternalSound, dir.X, dir.Y, dir.Z);})
#define sae_sound_set_velocity(pSound, vel) ({pSound->Velocity = vel; ma_sound_set_velocity(&pSound->InternalSound, vel.X, vel.Y, vel.Z);})




#endif // SIMPLE_AUDIO_ENGINE_H
