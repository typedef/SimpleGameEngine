// stb_truetype.h - v1.26 - public domain
// authored from 2009-2021 by Sean Barrett / RAD Game Tools

#ifdef STB_TRUETYPE_IMPLEMENTATION
   // #define your own (u)stbtt_int8/16/32 before including to override this

   // NOTE: We need this
   #ifndef swl_stbtt_uint8
   typedef unsigned char   swl_stbtt_uint8;
   typedef signed   char   swl_stbtt_int8;
   typedef unsigned short  swl_stbtt_uint16;
   typedef signed   short  swl_stbtt_int16;
   typedef unsigned int    swl_stbtt_uint32;
   typedef signed   int    swl_stbtt_int32;
   #endif

   typedef char stbtt__check_size32[sizeof(swl_stbtt_int32)==4 ? 1 : -1];
   typedef char stbtt__check_size16[sizeof(swl_stbtt_int16)==2 ? 1 : -1];

   // NOTE: We need this
   // e.g. #define your own STBTT_ifloor/STBTT_iceil() to avoid math.h
   #ifndef SWL_STBTT_ifloor
   #include <math.h>
   #define SWL_STBTT_ifloor(x)   ((int) floor(x))
   #define SWL_STBTT_iceil(x)    ((int) ceil(x))
   #endif

   // NOTE: We need this
   #ifndef SWL_STBTT_sqrt
   #include <math.h>
   #define SWL_STBTT_sqrt(x)      sqrt(x)
   #define SWL_STBTT_pow(x,y)     pow(x,y)
   #endif

   #ifndef SWL_STBTT_fmod
   #include <math.h>
   #define SWL_STBTT_fmod(x,y)    fmod(x,y)
   #endif

   #ifndef SWL_STBTT_cos
   #include <math.h>
   #define SWL_STBTT_cos(x)       cos(x)
   #define SWL_STBTT_acos(x)      acos(x)
   #endif

   #ifndef SWL_STBTT_fabs
   #include <math.h>
   #define SWL_STBTT_fabs(x)      fabs(x)
   #endif

// NOTE: We need this
   // #define your own functions "STBTT_malloc" / "STBTT_free" to avoid malloc.h
   #ifndef SWL_STBTT_malloc
   #include <stdlib.h>
   #define SWL_STBTT_malloc(x,u)  ((void)(u),malloc(x))
   #define SWL_STBTT_free(x,u)    ((void)(u),free(x))
   #endif

// NOTE: We need this
   #ifndef SWL_STBTT_assert
   #include <assert.h>
   #define SWL_STBTT_assert(x)    assert(x)
   #endif

   #ifndef SWL_STBTT_strlen
   #include <string.h>
   #define SWL_STBTT_strlen(x)    strlen(x)
   #endif

// NOTE: We need this
   #ifndef SWL_STBTT_memcpy
   #include <string.h>
   #define SWL_STBTT_memcpy       memcpy
   #define SWL_STBTT_memset       memset
   #endif
#endif

#ifndef __STB_INCLUDE_STB_TRUETYPE_H__
#define __STB_INCLUDE_STB_TRUETYPE_H__

#ifdef STBTT_STATIC
#define SWL_STBTT_DEF static
#else
#define SWL_STBTT_DEF extern
#endif

#ifdef __cplusplus
extern "C" {
#endif


typedef struct
{
   unsigned short x0,y0,x1,y1; // coordinates of bbox in bitmap
   float xoff,yoff,xadvance;
   float xoff2,yoff2;
} swl_stbtt_packedchar;

typedef struct swl_stbtt_pack_context swl_stbtt_pack_context;
typedef struct swl_stbtt_fontinfo swl_stbtt_fontinfo;
#ifndef STB_RECT_PACK_VERSION
typedef struct stbrp_rect stbrp_rect;
#endif

    // NOTE: We need this
    // private structure
typedef struct
{
   unsigned char *data;
   int cursor;
   int size;
} swl_stbtt__buf;

// NOTE: We need this
struct swl_stbtt_fontinfo
{
    void           * userdata;
    unsigned char  * data;              // pointer to .ttf file
    int              fontstart;         // offset of start of font

    int numGlyphs;                     // number of glyphs, needed for range checking

    int loca,head,glyf,hhea,hmtx,kern,gpos,svg; // table locations as offset from start of .ttf
    int index_map;                     // a cmap mapping for our chosen character encoding
    int indexToLocFormat;              // format needed to map from glyph index to glyph

    swl_stbtt__buf cff;                    // cff font data
    swl_stbtt__buf charstrings;            // the charstring index
    swl_stbtt__buf gsubrs;                 // global charstring subroutines index
    swl_stbtt__buf subrs;                  // private charstring subroutines index
    swl_stbtt__buf fontdicts;              // array of font dicts
    swl_stbtt__buf fdselect;               // map from glyph to fontdict
};

// NOTE: We need this
SWL_STBTT_DEF int swl_stbtt_InitFont(swl_stbtt_fontinfo *info, const unsigned char *data, int offset);

// NOTE: We need this
SWL_STBTT_DEF int swl_stbtt_FindGlyphIndex(const swl_stbtt_fontinfo *info, int unicode_codepoint);

// NOTE: We need this
SWL_STBTT_DEF float swl_stbtt_ScaleForPixelHeight(const swl_stbtt_fontinfo *info, float pixels);


// NOTE: We need this
SWL_STBTT_DEF int swl_stbtt_GetGlyphBox(const swl_stbtt_fontinfo *info, int glyph_index, int *x0, int *y0, int *x1, int *y1);

// NOTE: We need this
#ifndef SWL_STBTT_vmove // you can predefine these to use different values (but why?)
   enum {
      SWL_STBTT_vmove=1,
      SWL_STBTT_vline,
      SWL_STBTT_vcurve,
      SWL_STBTT_vcubic
   };
#endif

// NOTE: We need this
#ifndef swl_stbtt_vertex // you can predefine this to use different values
                   // (we share this with other code at RAD)
   #define swl_stbtt_vertex_type short // can't use swl_stbtt_int16 because that's not visible in the header file
   typedef struct
   {
      swl_stbtt_vertex_type x,y,cx,cy,cx1,cy1;
      unsigned char type,padding;
   } swl_stbtt_vertex;
#endif

// NOTE: We need this
SWL_STBTT_DEF int swl_stbtt_GetGlyphShape(const swl_stbtt_fontinfo *info, int glyph_index, swl_stbtt_vertex **vertices);

// NOTE: We need this
SWL_STBTT_DEF void swl_stbtt_GetGlyphBitmapBoxSubpixel(const swl_stbtt_fontinfo *font, int glyph, float scale_x, float scale_y,float shift_x, float shift_y, int *ix0, int *iy0, int *ix1, int *iy1);

// NOTE: We need this
SWL_STBTT_DEF void swl_stbtt_FreeSDF(unsigned char *bitmap, void *userdata);
// frees the SDF bitmap allocated below

// NOTE: We need this
SWL_STBTT_DEF unsigned char* swl_stbtt_GetGlyphSDF(const swl_stbtt_fontinfo *info, float scale, int glyph, int padding, unsigned char onedge_value, float pixel_dist_scale, int *width, int *height, int *xoff, int *yoff);

// NOTE: We need this
SWL_STBTT_DEF unsigned char* swl_stbtt_GetCodepointSDF(const swl_stbtt_fontinfo *info, float scale, int codepoint, int padding, unsigned char onedge_value, float pixel_dist_scale, int *width, int *height, int *xoff, int *yoff);


#define SWL_STBTT_MACSTYLE_DONTCARE     0
#define SWL_STBTT_MACSTYLE_BOLD         1
#define SWL_STBTT_MACSTYLE_ITALIC       2
#define SWL_STBTT_MACSTYLE_UNDERSCORE   4
#define SWL_STBTT_MACSTYLE_NONE         8   // <= not same as 0, this makes us check the bitfield is 0


enum { // platformID
   SWL_STBTT_PLATFORM_ID_UNICODE   =0,
   SWL_STBTT_PLATFORM_ID_MAC       =1,
   SWL_STBTT_PLATFORM_ID_ISO       =2,
   SWL_STBTT_PLATFORM_ID_MICROSOFT =3
};

enum { // encodingID for SWL_STBTT_PLATFORM_ID_UNICODE
   SWL_STBTT_UNICODE_EID_UNICODE_1_0    =0,
   SWL_STBTT_UNICODE_EID_UNICODE_1_1    =1,
   SWL_STBTT_UNICODE_EID_ISO_10646      =2,
   SWL_STBTT_UNICODE_EID_UNICODE_2_0_BMP=3,
   SWL_STBTT_UNICODE_EID_UNICODE_2_0_FULL=4
};

enum { // encodingID for SWL_STBTT_PLATFORM_ID_MICROSOFT
   SWL_STBTT_MS_EID_SYMBOL        =0,
   SWL_STBTT_MS_EID_UNICODE_BMP   =1,
   SWL_STBTT_MS_EID_SHIFTJIS      =2,
   SWL_STBTT_MS_EID_UNICODE_FULL  =10
};

enum { // encodingID for SWL_STBTT_PLATFORM_ID_MAC; same as Script Manager codes
   SWL_STBTT_MAC_EID_ROMAN        =0,   SWL_STBTT_MAC_EID_ARABIC       =4,
   SWL_STBTT_MAC_EID_JAPANESE     =1,   SWL_STBTT_MAC_EID_HEBREW       =5,
   SWL_STBTT_MAC_EID_CHINESE_TRAD =2,   SWL_STBTT_MAC_EID_GREEK        =6,
   SWL_STBTT_MAC_EID_KOREAN       =3,   SWL_STBTT_MAC_EID_RUSSIAN      =7
};

enum { // languageID for SWL_STBTT_PLATFORM_ID_MICROSOFT; same as LCID...
       // problematic because there are e.g. 16 english LCIDs and 16 arabic LCIDs
   SWL_STBTT_MS_LANG_ENGLISH     =0x0409,   SWL_STBTT_MS_LANG_ITALIAN     =0x0410,
   SWL_STBTT_MS_LANG_CHINESE     =0x0804,   SWL_STBTT_MS_LANG_JAPANESE    =0x0411,
   SWL_STBTT_MS_LANG_DUTCH       =0x0413,   SWL_STBTT_MS_LANG_KOREAN      =0x0412,
   SWL_STBTT_MS_LANG_FRENCH      =0x040c,   SWL_STBTT_MS_LANG_RUSSIAN     =0x0419,
   SWL_STBTT_MS_LANG_GERMAN      =0x0407,   SWL_STBTT_MS_LANG_SPANISH     =0x0409,
   SWL_STBTT_MS_LANG_HEBREW      =0x040d,   SWL_STBTT_MS_LANG_SWEDISH     =0x041D
};

enum { // languageID for SWL_STBTT_PLATFORM_ID_MAC
   SWL_STBTT_MAC_LANG_ENGLISH      =0 ,   SWL_STBTT_MAC_LANG_JAPANESE     =11,
   SWL_STBTT_MAC_LANG_ARABIC       =12,   SWL_STBTT_MAC_LANG_KOREAN       =23,
   SWL_STBTT_MAC_LANG_DUTCH        =4 ,   SWL_STBTT_MAC_LANG_RUSSIAN      =32,
   SWL_STBTT_MAC_LANG_FRENCH       =1 ,   SWL_STBTT_MAC_LANG_SPANISH      =6 ,
   SWL_STBTT_MAC_LANG_GERMAN       =2 ,   SWL_STBTT_MAC_LANG_SWEDISH      =5 ,
   SWL_STBTT_MAC_LANG_HEBREW       =10,   SWL_STBTT_MAC_LANG_CHINESE_SIMPLIFIED =33,
   SWL_STBTT_MAC_LANG_ITALIAN      =3 ,   SWL_STBTT_MAC_LANG_CHINESE_TRAD =19
};

#ifdef __cplusplus
}
#endif

#endif // __STB_INCLUDE_STB_TRUETYPE_H__

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////
////   IMPLEMENTATION
////
////

#ifdef STB_TRUETYPE_IMPLEMENTATION

#ifndef SWL_STBTT_MAX_OVERSAMPLE
#define SWL_STBTT_MAX_OVERSAMPLE   8
#endif

#if SWL_STBTT_MAX_OVERSAMPLE > 255
#error "SWL_STBTT_MAX_OVERSAMPLE cannot be > 255"
#endif

typedef int swl_stbtt__test_oversample_pow2[(SWL_STBTT_MAX_OVERSAMPLE & (SWL_STBTT_MAX_OVERSAMPLE-1)) == 0 ? 1 : -1];

#ifndef SWL_STBTT_RASTERIZER_VERSION
#define SWL_STBTT_RASTERIZER_VERSION 2
#endif

#ifdef _MSC_VER
#define SWL_STBTT__NOTUSED(v)  (void)(v)
#else
#define SWL_STBTT__NOTUSED(v)  (void)sizeof(v)
#endif

// NOTE: We need this
static swl_stbtt_uint8
swl_stbtt__buf_get8(swl_stbtt__buf *b)
{
   if (b->cursor >= b->size)
      return 0;
   return b->data[b->cursor++];
}

// NOTE: We need this
static swl_stbtt_uint8
swl_stbtt__buf_peek8(swl_stbtt__buf *b)
{
   if (b->cursor >= b->size)
      return 0;
   return b->data[b->cursor];
}

// NOTE: We need this
static void
swl_stbtt__buf_seek(swl_stbtt__buf *b, int o)
{
    SWL_STBTT_assert(!(o > b->size || o < 0));
    b->cursor = (o > b->size || o < 0) ? b->size : o;
}

// NOTE: We need this
static void
swl_stbtt__buf_skip(swl_stbtt__buf *b, int o)
{
    swl_stbtt__buf_seek(b, b->cursor + o);
}

// NOTE: We need this
static swl_stbtt_uint32
swl_stbtt__buf_get(swl_stbtt__buf *b, int n)
{
   swl_stbtt_uint32 v = 0;
   int i;
   SWL_STBTT_assert(n >= 1 && n <= 4);

   for (i = 0; i < n; i++)
      v = (v << 8) | swl_stbtt__buf_get8(b);

   return v;
}

// NOTE: We need this
static swl_stbtt__buf
swl_stbtt__new_buf(const void *p, size_t size)
{
    swl_stbtt__buf r;
    SWL_STBTT_assert(size < 0x40000000);
    r.data = (swl_stbtt_uint8*) p;
    r.size = (int) size;
    r.cursor = 0;
    return r;
}

// NOTE: We need this
#define swl_stbtt__buf_get16(b)  swl_stbtt__buf_get((b), 2)
// NOTE: We need this
#define swl_stbtt__buf_get32(b)  swl_stbtt__buf_get((b), 4)

// NOTE: We need this
static swl_stbtt__buf
swl_stbtt__buf_range(const swl_stbtt__buf *b, int o, int s)
{
   swl_stbtt__buf r = swl_stbtt__new_buf(NULL, 0);
   if (o < 0 || s < 0 || o > b->size || s > b->size - o)
       return r;
   r.data = b->data + o;
   r.size = s;
   return r;
}

// NOTE: We need this
static swl_stbtt__buf
swl_stbtt__cff_get_index(swl_stbtt__buf *b)
{
   int count, start, offsize;
   start = b->cursor;
   count = swl_stbtt__buf_get16(b);
   if (count)
   {
      offsize = swl_stbtt__buf_get8(b);
      SWL_STBTT_assert(offsize >= 1 && offsize <= 4);
      swl_stbtt__buf_skip(b, offsize * count);
      swl_stbtt__buf_skip(b, swl_stbtt__buf_get(b, offsize) - 1);
   }

   swl_stbtt__buf bufRange = swl_stbtt__buf_range(b, start, b->cursor - start);
   return bufRange;
}

// NOTE: We need this
static swl_stbtt_uint32
swl_stbtt__cff_int(swl_stbtt__buf *b)
{
    int b0 = swl_stbtt__buf_get8(b);
    if (b0 >= 32 && b0 <= 246)       return b0 - 139;
    else if (b0 >= 247 && b0 <= 250) return (b0 - 247)*256 + swl_stbtt__buf_get8(b) + 108;
    else if (b0 >= 251 && b0 <= 254) return -(b0 - 251)*256 - swl_stbtt__buf_get8(b) - 108;
    else if (b0 == 28)               return swl_stbtt__buf_get16(b);
    else if (b0 == 29)               return swl_stbtt__buf_get32(b);
    SWL_STBTT_assert(0);
    return 0;
}

// NOTE: We need this
static void
swl_stbtt__cff_skip_operand(swl_stbtt__buf *b)
{
    // NOTE: We need this
    int v, b0 = swl_stbtt__buf_peek8(b);
    SWL_STBTT_assert(b0 >= 28);
    if (b0 == 30)
    {
        // NOTE: We need this
        swl_stbtt__buf_skip(b, 1);
        while (b->cursor < b->size)
        {
            // NOTE: We need this
            v = swl_stbtt__buf_get8(b);
            if ((v & 0xF) == 0xF || (v >> 4) == 0xF)
            {
                break;
            }
        }
    }
    else
    {
        // NOTE: We need this
        swl_stbtt__cff_int(b);
    }
}

// NOTE: We need this
static swl_stbtt__buf
swl_stbtt__dict_get(swl_stbtt__buf *b, int key)
{
    // NOTE: We need this
    swl_stbtt__buf_seek(b, 0);
    while (b->cursor < b->size)
    {
        int start = b->cursor, end, op;
        // NOTE: We need this
        while (swl_stbtt__buf_peek8(b) >= 28)
        {
            // NOTE: We need this
            swl_stbtt__cff_skip_operand(b);
        }

        end = b->cursor;
        // NOTE: We need this
        op = swl_stbtt__buf_get8(b);
        if (op == 12)  op = swl_stbtt__buf_get8(b) | 0x100;
        if (op == key) return swl_stbtt__buf_range(b, start, end-start);
    }

    // NOTE: We need this
    return swl_stbtt__buf_range(b, 0, 0);
}

// NOTE: We need this
static void
swl_stbtt__dict_get_ints(swl_stbtt__buf *b, int key, int outcount, swl_stbtt_uint32 *out)
{
    int i;
    // NOTE: We need this
    swl_stbtt__buf operands = swl_stbtt__dict_get(b, key);
    for (i = 0; i < outcount && operands.cursor < operands.size; ++i)
        out[i] = swl_stbtt__cff_int(&operands); // NOTE: We need this
}

// NOTE: We need this
static int
swl_stbtt__cff_index_count(swl_stbtt__buf *b)
{
   swl_stbtt__buf_seek(b, 0);
   return swl_stbtt__buf_get16(b);
}

// NOTE: We need this
static swl_stbtt__buf
swl_stbtt__cff_index_get(swl_stbtt__buf b, int i)
{
   int count, offsize, start, end;
   swl_stbtt__buf_seek(&b, 0);
   count = swl_stbtt__buf_get16(&b);
   offsize = swl_stbtt__buf_get8(&b);
   SWL_STBTT_assert(i >= 0 && i < count);
   SWL_STBTT_assert(offsize >= 1 && offsize <= 4);
   swl_stbtt__buf_skip(&b, i*offsize);
   start = swl_stbtt__buf_get(&b, offsize);
   end = swl_stbtt__buf_get(&b, offsize);
   swl_stbtt__buf bufRange = swl_stbtt__buf_range(&b, 2+(count+1)*offsize+start, end - start);
   return bufRange;
}

// NOTE: We need this
#define ttBYTE(p)     (* (swl_stbtt_uint8 *) (p))
#define ttCHAR(p)     (* (swl_stbtt_int8 *) (p))
#define ttFixed(p)    ttLONG(p)

// NOTE: We need this
static swl_stbtt_uint16 ttUSHORT(swl_stbtt_uint8 *p) { return p[0]*256 + p[1]; }
// NOTE: We need this
static swl_stbtt_int16 ttSHORT(swl_stbtt_uint8 *p)   { return p[0]*256 + p[1]; }
// NOTE: We need this
static swl_stbtt_uint32 ttULONG(swl_stbtt_uint8 *p)  { return (p[0]<<24) + (p[1]<<16) + (p[2]<<8) + p[3]; }
static swl_stbtt_int32 ttLONG(swl_stbtt_uint8 *p)    { return (p[0]<<24) + (p[1]<<16) + (p[2]<<8) + p[3]; }

// NOTE: We need this
#define swl_stbtt_tag4(p,c0,c1,c2,c3) ((p)[0] == (c0) && (p)[1] == (c1) && (p)[2] == (c2) && (p)[3] == (c3))
// NOTE: We need this
#define swl_stbtt_tag(p,str)           swl_stbtt_tag4(p,str[0],str[1],str[2],str[3])

// @OPTIMIZE: binary search
// NOTE: We need this
static swl_stbtt_uint32
swl_stbtt__find_table(swl_stbtt_uint8 *data, swl_stbtt_uint32 fontstart, const char *tag)
{
    swl_stbtt_int32 num_tables = ttUSHORT(data+fontstart+4);
    swl_stbtt_uint32 tabledir = fontstart + 12;
    swl_stbtt_int32 i;
    for (i=0; i < num_tables; ++i)
    {
        swl_stbtt_uint32 loc = tabledir + 16*i;
        if (swl_stbtt_tag(data+loc+0, tag))
            return ttULONG(data+loc+8);
    }
    return 0;
}

// NOTE: We need this
static swl_stbtt__buf
swl_stbtt__get_subrs(swl_stbtt__buf cff, swl_stbtt__buf fontdict)
{
    swl_stbtt_uint32 subrsoff = 0, private_loc[2] = { 0, 0 };
    swl_stbtt__buf pdict;
    // NOTE: We need this
    swl_stbtt__dict_get_ints(&fontdict, 18, 2, private_loc);
    if (!private_loc[1] || !private_loc[0])
        return swl_stbtt__new_buf(NULL, 0); // NOTE: We need this
    pdict = swl_stbtt__buf_range(&cff, private_loc[1], private_loc[0]);
    swl_stbtt__dict_get_ints(&pdict, 19, 1, &subrsoff);
    if (!subrsoff)
        return swl_stbtt__new_buf(NULL, 0);
    swl_stbtt__buf_seek(&cff, private_loc[1]+subrsoff);
    return swl_stbtt__cff_get_index(&cff);
}

// NOTE: We need this
static int
swl_stbtt_InitFont_internal(swl_stbtt_fontinfo *info, unsigned char *data, int fontstart)
{
    swl_stbtt_uint32 cmap, t;
    swl_stbtt_int32 i,numTables;

    info->data = data;
    info->fontstart = fontstart;
    // NOTE: DONE
    info->cff = swl_stbtt__new_buf(NULL, 0);

    // NOTE: DONE
    cmap = swl_stbtt__find_table(data, fontstart, "cmap");       // required
    info->loca = swl_stbtt__find_table(data, fontstart, "loca"); // required
    info->head = swl_stbtt__find_table(data, fontstart, "head"); // required
    info->glyf = swl_stbtt__find_table(data, fontstart, "glyf"); // required
    info->hhea = swl_stbtt__find_table(data, fontstart, "hhea"); // required
    info->hmtx = swl_stbtt__find_table(data, fontstart, "hmtx"); // required
    info->kern = swl_stbtt__find_table(data, fontstart, "kern"); // not required
    info->gpos = swl_stbtt__find_table(data, fontstart, "GPOS"); // not required

    if (!cmap || !info->head || !info->hhea || !info->hmtx)
        return 0;

    if (info->glyf)
    {
        // required for truetype
        if (!info->loca)
            return 0;
    }
    else
    {
        // initialization for CFF / Type2 fonts (OTF)
        swl_stbtt__buf b, topdict, topdictidx;
        swl_stbtt_uint32 cstype = 2, charstrings = 0, fdarrayoff = 0, fdselectoff = 0;
        swl_stbtt_uint32 cff;

        // NOTE: DONE
        cff = swl_stbtt__find_table(data, fontstart, "CFF ");
        if (!cff) return 0;

        // NOTE: DONE
        info->fontdicts = swl_stbtt__new_buf(NULL, 0);
        info->fdselect = swl_stbtt__new_buf(NULL, 0);

        // NOTE: DONE
        // @TODO this should use size from table (not 512MB)
        info->cff = swl_stbtt__new_buf(data+cff, 512*1024*1024);
        b = info->cff;

        // NOTE: DONE
        // read the header
        swl_stbtt__buf_skip(&b, 2);
        // NOTE: DONE
        swl_stbtt__buf_seek(&b, swl_stbtt__buf_get8(&b)); // hdrsize

        // NOTE: DONE
        // @TODO the name INDEX could list multiple fonts,
        // but we just use the first one.
        swl_stbtt__cff_get_index(&b);  // name INDEX
        // NOTE: DONE
        topdictidx = swl_stbtt__cff_get_index(&b);
        // NOTE: DONE
        topdict = swl_stbtt__cff_index_get(topdictidx, 0);
        // NOTE: DONE
        swl_stbtt__cff_get_index(&b);  // string INDEX
        // NOTE: DONE
        info->gsubrs = swl_stbtt__cff_get_index(&b);

        // NOTE: DONE
        swl_stbtt__dict_get_ints(&topdict, 17, 1, &charstrings);
        swl_stbtt__dict_get_ints(&topdict, 0x100 | 6, 1, &cstype);
        swl_stbtt__dict_get_ints(&topdict, 0x100 | 36, 1, &fdarrayoff);
        swl_stbtt__dict_get_ints(&topdict, 0x100 | 37, 1, &fdselectoff);
        // NOTE: DONE
        info->subrs = swl_stbtt__get_subrs(b, topdict);

        // we only support Type 2 charstrings
        if (cstype != 2) return 0;
        if (charstrings == 0) return 0;

        if (fdarrayoff)
        {
            // looks like a CID font
            if (!fdselectoff) return 0;
            // NOTE: DONE
            swl_stbtt__buf_seek(&b, fdarrayoff);
            // NOTE: DONE
            info->fontdicts = swl_stbtt__cff_get_index(&b);
            // NOTE: DONE
            info->fdselect = swl_stbtt__buf_range(&b, fdselectoff, b.size-fdselectoff);
        }

        // NOTE: DONE
        swl_stbtt__buf_seek(&b, charstrings);
        // NOTE: DONE
        info->charstrings = swl_stbtt__cff_get_index(&b);
    }

    // NOTE: DONE
    t = swl_stbtt__find_table(data, fontstart, "maxp");
    if (t)
        info->numGlyphs = ttUSHORT(data+t+4);
    else
        info->numGlyphs = 0xffff;

    info->svg = -1;

    // find a cmap encoding table we understand *now* to avoid searching
    // later. (todo: could make this installable)
    // the same regardless of glyph.
    numTables = ttUSHORT(data + cmap + 2);
    info->index_map = 0;
    for (i=0; i < numTables; ++i)
    {
        swl_stbtt_uint32 encoding_record = cmap + 4 + 8 * i;
        // find an encoding we understand:
        switch(ttUSHORT(data+encoding_record))
        {
        case SWL_STBTT_PLATFORM_ID_MICROSOFT:
            switch (ttUSHORT(data+encoding_record+2))
            {
            case SWL_STBTT_MS_EID_UNICODE_BMP:
            case SWL_STBTT_MS_EID_UNICODE_FULL:
                // MS/Unicode
                info->index_map = cmap + ttULONG(data+encoding_record+4);
                break;
            }
            break;
        case SWL_STBTT_PLATFORM_ID_UNICODE:
            // Mac/iOS has these
            // all the encodingIDs are unicode, so we don't bother to check it
            info->index_map = cmap + ttULONG(data+encoding_record+4);
            break;
        }
    }

    if (info->index_map == 0)
        return 0;

    info->indexToLocFormat = ttUSHORT(data+info->head + 50);
    return 1;
}

// NOTE: We need this
SWL_STBTT_DEF int
swl_stbtt_FindGlyphIndex(const swl_stbtt_fontinfo *info, int unicode_codepoint)
{
    swl_stbtt_uint8 *data = info->data;
    swl_stbtt_uint32 index_map = info->index_map;

    swl_stbtt_uint16 format = ttUSHORT(data + index_map + 0);
    if (format == 0)
    { // apple byte encoding
        swl_stbtt_int32 bytes = ttUSHORT(data + index_map + 2);
        if (unicode_codepoint < bytes-6)
            return ttBYTE(data + index_map + 6 + unicode_codepoint);
        return 0;
    }
    else if (format == 6)
    {
        swl_stbtt_uint32 first = ttUSHORT(data + index_map + 6);
        swl_stbtt_uint32 count = ttUSHORT(data + index_map + 8);
        if ((swl_stbtt_uint32) unicode_codepoint >= first && (swl_stbtt_uint32) unicode_codepoint < first+count)
            return ttUSHORT(data + index_map + 10 + (unicode_codepoint - first)*2);
        return 0;
    }
    else if (format == 2)
    {
        SWL_STBTT_assert(0); // @TODO: high-byte mapping for japanese/chinese/korean
        return 0;
    }
    else if (format == 4)
    { // standard mapping for windows fonts: binary search collection of ranges
        swl_stbtt_uint16 segcount = ttUSHORT(data+index_map+6) >> 1;
        swl_stbtt_uint16 searchRange = ttUSHORT(data+index_map+8) >> 1;
        swl_stbtt_uint16 entrySelector = ttUSHORT(data+index_map+10);
        swl_stbtt_uint16 rangeShift = ttUSHORT(data+index_map+12) >> 1;

        // do a binary search of the segments
        swl_stbtt_uint32 endCount = index_map + 14;
        swl_stbtt_uint32 search = endCount;

        if (unicode_codepoint > 0xffff)
            return 0;

        // they lie from endCount .. endCount + segCount
        // but searchRange is the nearest power of two, so...
        if (unicode_codepoint >= ttUSHORT(data + search + rangeShift*2))
            search += rangeShift*2;

        // now decrement to bias correctly to find smallest
        search -= 2;
        while (entrySelector)
        {
            swl_stbtt_uint16 end;
            searchRange >>= 1;
            end = ttUSHORT(data + search + searchRange*2);
            if (unicode_codepoint > end)
                search += searchRange*2;
            --entrySelector;
        }
        search += 2;

        {
            swl_stbtt_uint16 offset, start, last;
            swl_stbtt_uint16 item = (swl_stbtt_uint16) ((search - endCount) >> 1);

            start = ttUSHORT(data + index_map + 14 + segcount*2 + 2 + 2*item);
            last = ttUSHORT(data + endCount + 2*item);
            if (unicode_codepoint < start || unicode_codepoint > last)
                return 0;

            offset = ttUSHORT(data + index_map + 14 + segcount*6 + 2 + 2*item);
            if (offset == 0)
                return (swl_stbtt_uint16) (unicode_codepoint + ttSHORT(data + index_map + 14 + segcount*4 + 2 + 2*item));

            return ttUSHORT(data + offset + (unicode_codepoint-start)*2 + index_map + 14 + segcount*6 + 2 + 2*item);
        }
    }
    else if (format == 12 || format == 13)
    {
        swl_stbtt_uint32 ngroups = ttULONG(data+index_map+12);
        swl_stbtt_int32 low,high;
        low = 0; high = (swl_stbtt_int32)ngroups;
        // Binary search the right group.
        while (low < high)
        {
            swl_stbtt_int32 mid = low + ((high-low) >> 1); // rounds down, so low <= mid < high
            swl_stbtt_uint32 start_char = ttULONG(data+index_map+16+mid*12);
            swl_stbtt_uint32 end_char = ttULONG(data+index_map+16+mid*12+4);

            if ((swl_stbtt_uint32) unicode_codepoint < start_char)
                high = mid;
            else if ((swl_stbtt_uint32) unicode_codepoint > end_char)
                low = mid+1;
            else
            {
                swl_stbtt_uint32 start_glyph = ttULONG(data+index_map+16+mid*12+8);

                if (format == 12)
                    return start_glyph + unicode_codepoint-start_char;
                else // format == 13
                    return start_glyph;
            }
        }

        // not found
        return 0;
    }

    // @TODO
    SWL_STBTT_assert(0);
    return 0;
}


// NOTE: We need this
static void
swl_stbtt_setvertex(swl_stbtt_vertex *v, swl_stbtt_uint8 type, swl_stbtt_int32 x, swl_stbtt_int32 y, swl_stbtt_int32 cx, swl_stbtt_int32 cy)
{
   v->type = type;
   v->x = (swl_stbtt_int16) x;
   v->y = (swl_stbtt_int16) y;
   v->cx = (swl_stbtt_int16) cx;
   v->cy = (swl_stbtt_int16) cy;
}

// NOTE: We need this
static int
swl_stbtt__GetGlyfOffset(const swl_stbtt_fontinfo *info, int glyph_index)
{
   int g1,g2;

   SWL_STBTT_assert(!info->cff.size);

   if (glyph_index >= info->numGlyphs)
       return -1; // glyph index out of range
   if (info->indexToLocFormat >= 2)
       return -1; // unknown index->glyph map format

   if (info->indexToLocFormat == 0)
   {
      g1 = info->glyf + ttUSHORT(info->data + info->loca + glyph_index * 2) * 2;
      g2 = info->glyf + ttUSHORT(info->data + info->loca + glyph_index * 2 + 2) * 2;
   }
   else
   {
      g1 = info->glyf + ttULONG (info->data + info->loca + glyph_index * 4);
      g2 = info->glyf + ttULONG (info->data + info->loca + glyph_index * 4 + 4);
   }

   return g1==g2 ? -1 : g1; // if length is 0, return -1
}

// NOTE: We need this
static int swl_stbtt__GetGlyphInfoT2(const swl_stbtt_fontinfo *info, int glyph_index, int *x0, int *y0, int *x1, int *y1);

// NOTE: We need this
SWL_STBTT_DEF int
swl_stbtt_GetGlyphBox(const swl_stbtt_fontinfo *info, int glyph_index, int *x0, int *y0, int *x1, int *y1)
{
    if (info->cff.size)
    {
        // NOTE: We need this
        swl_stbtt__GetGlyphInfoT2(info, glyph_index, x0, y0, x1, y1);
    }
    else
    {
        // NOTE: We need this
        int g = swl_stbtt__GetGlyfOffset(info, glyph_index);
        if (g < 0) return 0;

        if (x0) *x0 = ttSHORT(info->data + g + 2);
        if (y0) *y0 = ttSHORT(info->data + g + 4);
        if (x1) *x1 = ttSHORT(info->data + g + 6);
        if (y1) *y1 = ttSHORT(info->data + g + 8);
    }
    return 1;
}

// NOTE: We need this
static int
swl_stbtt__close_shape(swl_stbtt_vertex *vertices, int num_vertices, int was_off, int start_off,
                   swl_stbtt_int32 sx, swl_stbtt_int32 sy, swl_stbtt_int32 scx, swl_stbtt_int32 scy, swl_stbtt_int32 cx, swl_stbtt_int32 cy)
{
    if (start_off)
    {
        if (was_off)
            swl_stbtt_setvertex(&vertices[num_vertices++], SWL_STBTT_vcurve, (cx+scx)>>1, (cy+scy)>>1, cx,cy);
        swl_stbtt_setvertex(&vertices[num_vertices++], SWL_STBTT_vcurve, sx,sy,scx,scy);
    }
    else
    {
        if (was_off)
            swl_stbtt_setvertex(&vertices[num_vertices++], SWL_STBTT_vcurve,sx,sy,cx,cy);
        else
            swl_stbtt_setvertex(&vertices[num_vertices++], SWL_STBTT_vline,sx,sy,0,0);
    }

    return num_vertices;
}

// NOTE: We need this
static int
swl_stbtt__GetGlyphShapeTT(const swl_stbtt_fontinfo *info, int glyph_index, swl_stbtt_vertex **pvertices)
{
    swl_stbtt_int16 numberOfContours;
    swl_stbtt_uint8 *endPtsOfContours;
    swl_stbtt_uint8 *data = info->data;
    swl_stbtt_vertex *vertices=0;
    int num_vertices=0;
    // NOTE: We need this
    int g = swl_stbtt__GetGlyfOffset(info, glyph_index);

    *pvertices = NULL;

    if (g < 0) return 0;

    numberOfContours = ttSHORT(data + g);

    if (numberOfContours > 0)
    {
        swl_stbtt_uint8 flags=0,flagcount;
        swl_stbtt_int32 ins, i,j=0,m,n, next_move, was_off=0, off, start_off=0;
        swl_stbtt_int32 x,y,cx,cy,sx,sy, scx,scy;
        swl_stbtt_uint8 *points;
        endPtsOfContours = (data + g + 10);
        ins = ttUSHORT(data + g + 10 + numberOfContours * 2);
        points = data + g + 10 + numberOfContours * 2 + 2 + ins;

        n = 1+ttUSHORT(endPtsOfContours + numberOfContours*2-2);

        m = n + 2*numberOfContours;  // a loose bound on how many vertices we might need
        vertices = (swl_stbtt_vertex *) SWL_STBTT_malloc(m * sizeof(vertices[0]), info->userdata);
        if (vertices == 0)
            return 0;

        next_move = 0;
        flagcount=0;

        // in first pass, we load uninterpreted data into the allocated array
        // above, shifted to the end of the array so we won't overwrite it when
        // we create our final data starting from the front

        off = m - n; // starting offset for uninterpreted data, regardless of how m ends up being calculated

        // first load flags

        for (i=0; i < n; ++i)
        {
            if (flagcount == 0)
            {
                flags = *points++;
                if (flags & 8)
                    flagcount = *points++;
            }
            else
            {
                --flagcount;
            }
            vertices[off+i].type = flags;
        }

        // now load x coordinates
        x=0;
        for (i=0; i < n; ++i)
        {
            flags = vertices[off+i].type;
            if (flags & 2)
            {
                swl_stbtt_int16 dx = *points++;
                x += (flags & 16) ? dx : -dx; // ???
            }
            else
            {
                if (!(flags & 16))
                {
                    x = x + (swl_stbtt_int16) (points[0]*256 + points[1]);
                    points += 2;
                }
            }
            vertices[off+i].x = (swl_stbtt_int16) x;
        }

        // now load y coordinates
        y=0;
        for (i=0; i < n; ++i)
        {
            flags = vertices[off+i].type;
            if (flags & 4)
            {
                swl_stbtt_int16 dy = *points++;
                y += (flags & 32) ? dy : -dy; // ???
            }
            else
            {
                if (!(flags & 32))
                {
                    y = y + (swl_stbtt_int16) (points[0]*256 + points[1]);
                    points += 2;
                }
            }
            vertices[off+i].y = (swl_stbtt_int16) y;
        }

        // now convert them to our format
        num_vertices=0;
        sx = sy = cx = cy = scx = scy = 0;
        for (i=0; i < n; ++i)
        {
            flags = vertices[off+i].type;
            x     = (swl_stbtt_int16) vertices[off+i].x;
            y     = (swl_stbtt_int16) vertices[off+i].y;

            if (next_move == i)
            {
                if (i != 0)
                    num_vertices = swl_stbtt__close_shape(vertices, num_vertices, was_off, start_off, sx,sy,scx,scy,cx,cy);

                // now start the new one
                start_off = !(flags & 1);
                if (start_off)
                {
                    // if we start off with an off-curve point, then when we need to find a point on the curve
                    // where we can start, and we need to save some state for when we wraparound.
                    scx = x;
                    scy = y;
                    if (!(vertices[off+i+1].type & 1))
                    {
                        // next point is also a curve point, so interpolate an on-point curve
                        sx = (x + (swl_stbtt_int32) vertices[off+i+1].x) >> 1;
                        sy = (y + (swl_stbtt_int32) vertices[off+i+1].y) >> 1;
                    }
                    else
                    {
                        // otherwise just use the next point as our start point
                        sx = (swl_stbtt_int32) vertices[off+i+1].x;
                        sy = (swl_stbtt_int32) vertices[off+i+1].y;
                        ++i; // we're using point i+1 as the starting point, so skip it
                    }
                }
                else
                {
                    sx = x;
                    sy = y;
                }
                swl_stbtt_setvertex(&vertices[num_vertices++], SWL_STBTT_vmove,sx,sy,0,0);
                was_off = 0;
                next_move = 1 + ttUSHORT(endPtsOfContours+j*2);
                ++j;
            }
            else
            {
                if (!(flags & 1)) { // if it's a curve
                    if (was_off) // two off-curve control points in a row means interpolate an on-curve midpoint
                        swl_stbtt_setvertex(&vertices[num_vertices++], SWL_STBTT_vcurve, (cx+x)>>1, (cy+y)>>1, cx, cy);
                    cx = x;
                    cy = y;
                    was_off = 1;
                } else {
                    if (was_off)
                        swl_stbtt_setvertex(&vertices[num_vertices++], SWL_STBTT_vcurve, x,y, cx, cy);
                    else
                        swl_stbtt_setvertex(&vertices[num_vertices++], SWL_STBTT_vline, x,y,0,0);
                    was_off = 0;
                }
            }
        }

        num_vertices = swl_stbtt__close_shape(vertices, num_vertices, was_off, start_off, sx,sy,scx,scy,cx,cy);
    }
    else if (numberOfContours < 0)
    {
        // Compound shapes.
        int more = 1;
        swl_stbtt_uint8 *comp = data + g + 10;
        num_vertices = 0;
        vertices = 0;
        while (more)
        {
            swl_stbtt_uint16 flags, gidx;
            int comp_num_verts = 0, i;
            swl_stbtt_vertex *comp_verts = 0, *tmp = 0;
            float mtx[6] = {1,0,0,1,0,0}, m, n;

            flags = ttSHORT(comp); comp+=2;
            gidx = ttSHORT(comp); comp+=2;

            if (flags & 2)
            { // XY values
                if (flags & 1)
                { // shorts
                    mtx[4] = ttSHORT(comp); comp+=2;
                    mtx[5] = ttSHORT(comp); comp+=2;
                }
                else
                {
                    mtx[4] = ttCHAR(comp); comp+=1;
                    mtx[5] = ttCHAR(comp); comp+=1;
                }
            }
            else
            {
                // @TODO handle matching point
                SWL_STBTT_assert(0);
            }
            if (flags & (1<<3))
            { // WE_HAVE_A_SCALE
                mtx[0] = mtx[3] = ttSHORT(comp)/16384.0f; comp+=2;
                mtx[1] = mtx[2] = 0;
            }
            else if (flags & (1<<6))
            { // WE_HAVE_AN_X_AND_YSCALE
                mtx[0] = ttSHORT(comp)/16384.0f; comp+=2;
                mtx[1] = mtx[2] = 0;
                mtx[3] = ttSHORT(comp)/16384.0f; comp+=2;
            }
            else if (flags & (1<<7))
            { // WE_HAVE_A_TWO_BY_TWO
                mtx[0] = ttSHORT(comp)/16384.0f; comp+=2;
                mtx[1] = ttSHORT(comp)/16384.0f; comp+=2;
                mtx[2] = ttSHORT(comp)/16384.0f; comp+=2;
                mtx[3] = ttSHORT(comp)/16384.0f; comp+=2;
            }

            // Find transformation scales.
            m = (float) SWL_STBTT_sqrt(mtx[0]*mtx[0] + mtx[1]*mtx[1]);
            n = (float) SWL_STBTT_sqrt(mtx[2]*mtx[2] + mtx[3]*mtx[3]);

            // Get indexed glyph.
            comp_num_verts = swl_stbtt_GetGlyphShape(info, gidx, &comp_verts);
            if (comp_num_verts > 0)
            {
                // Transform vertices.
                for (i = 0; i < comp_num_verts; ++i)
                {
                    swl_stbtt_vertex* v = &comp_verts[i];
                    swl_stbtt_vertex_type x,y;
                    x=v->x; y=v->y;
                    v->x = (swl_stbtt_vertex_type)(m * (mtx[0]*x + mtx[2]*y + mtx[4]));
                    v->y = (swl_stbtt_vertex_type)(n * (mtx[1]*x + mtx[3]*y + mtx[5]));
                    x=v->cx; y=v->cy;
                    v->cx = (swl_stbtt_vertex_type)(m * (mtx[0]*x + mtx[2]*y + mtx[4]));
                    v->cy = (swl_stbtt_vertex_type)(n * (mtx[1]*x + mtx[3]*y + mtx[5]));
                }

                // Append vertices.
                tmp = (swl_stbtt_vertex*)SWL_STBTT_malloc((num_vertices+comp_num_verts)*sizeof(swl_stbtt_vertex), info->userdata);
                if (!tmp)
                {
                    if (vertices) SWL_STBTT_free(vertices, info->userdata);
                    if (comp_verts) SWL_STBTT_free(comp_verts, info->userdata);
                    return 0;
                }

                if (num_vertices > 0 && vertices)
                    SWL_STBTT_memcpy(tmp, vertices, num_vertices*sizeof(swl_stbtt_vertex));
                SWL_STBTT_memcpy(tmp+num_vertices, comp_verts, comp_num_verts*sizeof(swl_stbtt_vertex));
                if (vertices)
                    SWL_STBTT_free(vertices, info->userdata);
                vertices = tmp;
                SWL_STBTT_free(comp_verts, info->userdata);
                num_vertices += comp_num_verts;
            }
            // More components ?
            more = flags & (1<<5);
        }
    }
    else
    {
        // numberOfCounters == 0, do nothing
    }

    *pvertices = vertices;
    return num_vertices;
}

// NOTE: We need this
typedef struct
{
   int bounds;
   int started;
   float first_x, first_y;
   float x, y;
   swl_stbtt_int32 min_x, max_x, min_y, max_y;

   swl_stbtt_vertex *pvertices;
   int num_vertices;
} swl_stbtt__csctx;

// NOTE: We need this
#define SWL_STBTT__CSCTX_INIT(bounds) {bounds,0, 0,0, 0,0, 0,0,0,0, NULL, 0}

// NOTE: We need this
static void
swl_stbtt__track_vertex(swl_stbtt__csctx *c, swl_stbtt_int32 x, swl_stbtt_int32 y)
{
   if (x > c->max_x || !c->started) c->max_x = x;
   if (y > c->max_y || !c->started) c->max_y = y;
   if (x < c->min_x || !c->started) c->min_x = x;
   if (y < c->min_y || !c->started) c->min_y = y;
   c->started = 1;
}

// NOTE: We need this
static void
swl_stbtt__csctx_v(swl_stbtt__csctx *c, swl_stbtt_uint8 type, swl_stbtt_int32 x, swl_stbtt_int32 y, swl_stbtt_int32 cx, swl_stbtt_int32 cy, swl_stbtt_int32 cx1, swl_stbtt_int32 cy1)
{
    if (c->bounds)
    {
        swl_stbtt__track_vertex(c, x, y);
        if (type == SWL_STBTT_vcubic)
        {
            swl_stbtt__track_vertex(c, cx, cy);
            swl_stbtt__track_vertex(c, cx1, cy1);
        }
    }
    else
    {
        swl_stbtt_setvertex(&c->pvertices[c->num_vertices], type, x, y, cx, cy);
        c->pvertices[c->num_vertices].cx1 = (swl_stbtt_int16) cx1;
        c->pvertices[c->num_vertices].cy1 = (swl_stbtt_int16) cy1;
    }
    c->num_vertices++;
}

// NOTE: We need this
static void
swl_stbtt__csctx_close_shape(swl_stbtt__csctx *ctx)
{
    if (ctx->first_x != ctx->x || ctx->first_y != ctx->y)
        swl_stbtt__csctx_v(ctx, SWL_STBTT_vline, (int)ctx->first_x, (int)ctx->first_y, 0, 0, 0, 0);
}

// NOTE: We need this
static void
swl_stbtt__csctx_rmove_to(swl_stbtt__csctx *ctx, float dx, float dy)
{
    swl_stbtt__csctx_close_shape(ctx);
    ctx->first_x = ctx->x = ctx->x + dx;
    ctx->first_y = ctx->y = ctx->y + dy;
    swl_stbtt__csctx_v(ctx, SWL_STBTT_vmove, (int)ctx->x, (int)ctx->y, 0, 0, 0, 0);
}

// NOTE: We need this
static void
swl_stbtt__csctx_rline_to(swl_stbtt__csctx *ctx, float dx, float dy)
{
   ctx->x += dx;
   ctx->y += dy;
   swl_stbtt__csctx_v(ctx, SWL_STBTT_vline, (int)ctx->x, (int)ctx->y, 0, 0, 0, 0);
}

// NOTE: We need this
static void
swl_stbtt__csctx_rccurve_to(swl_stbtt__csctx *ctx, float dx1, float dy1, float dx2, float dy2, float dx3, float dy3)
{
    float cx1 = ctx->x + dx1;
    float cy1 = ctx->y + dy1;
    float cx2 = cx1 + dx2;
    float cy2 = cy1 + dy2;
    ctx->x = cx2 + dx3;
    ctx->y = cy2 + dy3;
    swl_stbtt__csctx_v(ctx, SWL_STBTT_vcubic, (int)ctx->x, (int)ctx->y, (int)cx1, (int)cy1, (int)cx2, (int)cy2);
}

// NOTE: We need this
static swl_stbtt__buf
swl_stbtt__get_subr(swl_stbtt__buf idx, int n)
{
   int count = swl_stbtt__cff_index_count(&idx);
   int bias = 107;
   if (count >= 33900)
      bias = 32768;
   else if (count >= 1240)
      bias = 1131;
   n += bias;
   if (n < 0 || n >= count)
      return swl_stbtt__new_buf(NULL, 0);
   return swl_stbtt__cff_index_get(idx, n);
}

// NOTE: We need this
static swl_stbtt__buf
swl_stbtt__cid_get_glyph_subrs(const swl_stbtt_fontinfo *info, int glyph_index)
{
   swl_stbtt__buf fdselect = info->fdselect;
   int nranges, start, end, v, fmt, fdselector = -1, i;

   swl_stbtt__buf_seek(&fdselect, 0);
   fmt = swl_stbtt__buf_get8(&fdselect);
   if (fmt == 0) {
      // untested
      swl_stbtt__buf_skip(&fdselect, glyph_index);
      fdselector = swl_stbtt__buf_get8(&fdselect);
   } else if (fmt == 3) {
      nranges = swl_stbtt__buf_get16(&fdselect);
      start = swl_stbtt__buf_get16(&fdselect);
      for (i = 0; i < nranges; i++) {
         v = swl_stbtt__buf_get8(&fdselect);
         end = swl_stbtt__buf_get16(&fdselect);
         if (glyph_index >= start && glyph_index < end) {
            fdselector = v;
            break;
         }
         start = end;
      }
   }
   if (fdselector == -1) swl_stbtt__new_buf(NULL, 0);
   return swl_stbtt__get_subrs(info->cff, swl_stbtt__cff_index_get(info->fontdicts, fdselector));
}

// NOTE: We need this
static int
swl_stbtt__run_charstring(const swl_stbtt_fontinfo *info, int glyph_index, swl_stbtt__csctx *c)
{
    int in_header = 1, maskbits = 0, subr_stack_height = 0, sp = 0, v, i, b0;
    int has_subrs = 0, clear_stack;
    float s[48];
    swl_stbtt__buf subr_stack[10], subrs = info->subrs, b;
    float f;

#define SWL_STBTT__CSERR(s) (0)

    // this currently ignores the initial width value, which isn't needed if we have hmtx
    b = swl_stbtt__cff_index_get(info->charstrings, glyph_index);
    while (b.cursor < b.size)
    {
        i = 0;
        clear_stack = 1;
        b0 = swl_stbtt__buf_get8(&b);
        switch (b0)
        {
            // @TODO implement hinting
        case 0x13: // hintmask
        case 0x14: // cntrmask
            if (in_header)
                maskbits += (sp / 2); // implicit "vstem"
            in_header = 0;
            swl_stbtt__buf_skip(&b, (maskbits + 7) / 8);
            break;

        case 0x01: // hstem
        case 0x03: // vstem
        case 0x12: // hstemhm
        case 0x17: // vstemhm
            maskbits += (sp / 2);
            break;

        case 0x15: // rmoveto
            in_header = 0;
            if (sp < 2) return SWL_STBTT__CSERR("rmoveto stack");
            swl_stbtt__csctx_rmove_to(c, s[sp-2], s[sp-1]);
            break;
        case 0x04: // vmoveto
            in_header = 0;
            if (sp < 1) return SWL_STBTT__CSERR("vmoveto stack");
            swl_stbtt__csctx_rmove_to(c, 0, s[sp-1]);
            break;
        case 0x16: // hmoveto
            in_header = 0;
            if (sp < 1) return SWL_STBTT__CSERR("hmoveto stack");
            swl_stbtt__csctx_rmove_to(c, s[sp-1], 0);
            break;

        case 0x05: // rlineto
            if (sp < 2) return SWL_STBTT__CSERR("rlineto stack");
            for (; i + 1 < sp; i += 2)
                swl_stbtt__csctx_rline_to(c, s[i], s[i+1]);
            break;

            // hlineto/vlineto and vhcurveto/hvcurveto alternate horizontal and vertical
            // starting from a different place.

        case 0x07: // vlineto
            if (sp < 1) return SWL_STBTT__CSERR("vlineto stack");
            goto vlineto;
        case 0x06: // hlineto
            if (sp < 1) return SWL_STBTT__CSERR("hlineto stack");
            for (;;)
            {
                if (i >= sp) break;
                swl_stbtt__csctx_rline_to(c, s[i], 0);
                i++;
            vlineto:
                if (i >= sp) break;
                swl_stbtt__csctx_rline_to(c, 0, s[i]);
                i++;
            }
            break;

        case 0x1F: // hvcurveto
            if (sp < 4) return SWL_STBTT__CSERR("hvcurveto stack");
            goto hvcurveto;
        case 0x1E: // vhcurveto
            if (sp < 4) return SWL_STBTT__CSERR("vhcurveto stack");
            for (;;)
            {
                if (i + 3 >= sp) break;
                // NOTE: We need this
                swl_stbtt__csctx_rccurve_to(c, 0, s[i], s[i+1], s[i+2], s[i+3], (sp - i == 5) ? s[i + 4] : 0.0f);
                i += 4;
            hvcurveto:
                if (i + 3 >= sp) break;
                swl_stbtt__csctx_rccurve_to(c, s[i], 0, s[i+1], s[i+2], (sp - i == 5) ? s[i+4] : 0.0f, s[i+3]);
                i += 4;
            }
            break;

        case 0x08: // rrcurveto
            if (sp < 6) return SWL_STBTT__CSERR("rcurveline stack");
            for (; i + 5 < sp; i += 6)
                swl_stbtt__csctx_rccurve_to(c, s[i], s[i+1], s[i+2], s[i+3], s[i+4], s[i+5]);
            break;

        case 0x18: // rcurveline
            if (sp < 8) return SWL_STBTT__CSERR("rcurveline stack");
            for (; i + 5 < sp - 2; i += 6)
                swl_stbtt__csctx_rccurve_to(c, s[i], s[i+1], s[i+2], s[i+3], s[i+4], s[i+5]);
            if (i + 1 >= sp) return SWL_STBTT__CSERR("rcurveline stack");
            swl_stbtt__csctx_rline_to(c, s[i], s[i+1]);
            break;

        case 0x19: // rlinecurve
            if (sp < 8) return SWL_STBTT__CSERR("rlinecurve stack");
            for (; i + 1 < sp - 6; i += 2)
                swl_stbtt__csctx_rline_to(c, s[i], s[i+1]);
            if (i + 5 >= sp) return SWL_STBTT__CSERR("rlinecurve stack");
            swl_stbtt__csctx_rccurve_to(c, s[i], s[i+1], s[i+2], s[i+3], s[i+4], s[i+5]);
            break;

        case 0x1A: // vvcurveto
        case 0x1B: // hhcurveto
            if (sp < 4) return SWL_STBTT__CSERR("(vv|hh)curveto stack");
            f = 0.0;
            if (sp & 1) { f = s[i]; i++; }
            for (; i + 3 < sp; i += 4) {
                if (b0 == 0x1B)
                    swl_stbtt__csctx_rccurve_to(c, s[i], f, s[i+1], s[i+2], s[i+3], 0.0);
                else
                    swl_stbtt__csctx_rccurve_to(c, f, s[i], s[i+1], s[i+2], 0.0, s[i+3]);
                f = 0.0;
            }
            break;

        case 0x0A: // callsubr
            if (!has_subrs)
            {
                if (info->fdselect.size)
                {
                    // NOTE: We need this
                    subrs = swl_stbtt__cid_get_glyph_subrs(info, glyph_index);
                }
                has_subrs = 1;
            }
            // FALLTHROUGH
        case 0x1D: // callgsubr
            if (sp < 1) return SWL_STBTT__CSERR("call(g|)subr stack");
            v = (int) s[--sp];
            if (subr_stack_height >= 10) return SWL_STBTT__CSERR("recursion limit");
            subr_stack[subr_stack_height++] = b;
            // NOTE: We need this
            b = swl_stbtt__get_subr(b0 == 0x0A ? subrs : info->gsubrs, v);
            if (b.size == 0) return SWL_STBTT__CSERR("subr not found");
            b.cursor = 0;
            clear_stack = 0;
            break;

        case 0x0B: // return
            if (subr_stack_height <= 0) return SWL_STBTT__CSERR("return outside subr");
            b = subr_stack[--subr_stack_height];
            clear_stack = 0;
            break;

        case 0x0E: // endchar
            swl_stbtt__csctx_close_shape(c);
            return 1;

        case 0x0C:
        { // two-byte escape
            float dx1, dx2, dx3, dx4, dx5, dx6, dy1, dy2, dy3, dy4, dy5, dy6;
            float dx, dy;
            int b1 = swl_stbtt__buf_get8(&b);
            switch (b1)
            {
                // @TODO These "flex" implementations ignore the flex-depth and resolution,
                // and always draw beziers.
            case 0x22: // hflex
                if (sp < 7) return SWL_STBTT__CSERR("hflex stack");
                dx1 = s[0];
                dx2 = s[1];
                dy2 = s[2];
                dx3 = s[3];
                dx4 = s[4];
                dx5 = s[5];
                dx6 = s[6];
                swl_stbtt__csctx_rccurve_to(c, dx1, 0, dx2, dy2, dx3, 0);
                swl_stbtt__csctx_rccurve_to(c, dx4, 0, dx5, -dy2, dx6, 0);
                break;

            case 0x23: // flex
                if (sp < 13) return SWL_STBTT__CSERR("flex stack");
                dx1 = s[0];
                dy1 = s[1];
                dx2 = s[2];
                dy2 = s[3];
                dx3 = s[4];
                dy3 = s[5];
                dx4 = s[6];
                dy4 = s[7];
                dx5 = s[8];
                dy5 = s[9];
                dx6 = s[10];
                dy6 = s[11];
                //fd is s[12]
                swl_stbtt__csctx_rccurve_to(c, dx1, dy1, dx2, dy2, dx3, dy3);
                swl_stbtt__csctx_rccurve_to(c, dx4, dy4, dx5, dy5, dx6, dy6);
                break;

            case 0x24: // hflex1
                if (sp < 9) return SWL_STBTT__CSERR("hflex1 stack");
                dx1 = s[0];
                dy1 = s[1];
                dx2 = s[2];
                dy2 = s[3];
                dx3 = s[4];
                dx4 = s[5];
                dx5 = s[6];
                dy5 = s[7];
                dx6 = s[8];
                swl_stbtt__csctx_rccurve_to(c, dx1, dy1, dx2, dy2, dx3, 0);
                swl_stbtt__csctx_rccurve_to(c, dx4, 0, dx5, dy5, dx6, -(dy1+dy2+dy5));
                break;

            case 0x25: // flex1
                if (sp < 11) return SWL_STBTT__CSERR("flex1 stack");
                dx1 = s[0];
                dy1 = s[1];
                dx2 = s[2];
                dy2 = s[3];
                dx3 = s[4];
                dy3 = s[5];
                dx4 = s[6];
                dy4 = s[7];
                dx5 = s[8];
                dy5 = s[9];
                dx6 = dy6 = s[10];
                dx = dx1+dx2+dx3+dx4+dx5;
                dy = dy1+dy2+dy3+dy4+dy5;
                if (SWL_STBTT_fabs(dx) > SWL_STBTT_fabs(dy))
                    dy6 = -dy;
                else
                    dx6 = -dx;
                swl_stbtt__csctx_rccurve_to(c, dx1, dy1, dx2, dy2, dx3, dy3);
                swl_stbtt__csctx_rccurve_to(c, dx4, dy4, dx5, dy5, dx6, dy6);
                break;

            default:
                return SWL_STBTT__CSERR("unimplemented");
            }
        } break;

        default:
            if (b0 != 255 && b0 != 28 && b0 < 32)
                return SWL_STBTT__CSERR("reserved operator");

            // push immediate
            if (b0 == 255)
            {
                f = (float)(swl_stbtt_int32)swl_stbtt__buf_get32(&b) / 0x10000;
            }
            else
            {
                swl_stbtt__buf_skip(&b, -1);
                f = (float)(swl_stbtt_int16)swl_stbtt__cff_int(&b);
            }
            if (sp >= 48) return SWL_STBTT__CSERR("push stack overflow");
            s[sp++] = f;
            clear_stack = 0;
            break;
        }
        if (clear_stack) sp = 0;
    }

    return SWL_STBTT__CSERR("no endchar");

#undef SWL_STBTT__CSERR
}

// NOTE: We need this
static int
swl_stbtt__GetGlyphShapeT2(const swl_stbtt_fontinfo *info, int glyph_index, swl_stbtt_vertex **pvertices)
{
    // runs the charstring twice, once to count and once to output (to avoid realloc)
    swl_stbtt__csctx count_ctx = SWL_STBTT__CSCTX_INIT(1);
    swl_stbtt__csctx output_ctx = SWL_STBTT__CSCTX_INIT(0);

    // NOTE: We need this
    if (swl_stbtt__run_charstring(info, glyph_index, &count_ctx))
    {
        *pvertices = (swl_stbtt_vertex*)SWL_STBTT_malloc(count_ctx.num_vertices*sizeof(swl_stbtt_vertex), info->userdata);

        output_ctx.pvertices = *pvertices;

        // NOTE: We need this
        if (swl_stbtt__run_charstring(info, glyph_index, &output_ctx))
        {
            SWL_STBTT_assert(output_ctx.num_vertices == count_ctx.num_vertices);
            return output_ctx.num_vertices;
        }
    }

    *pvertices = NULL;
    return 0;
}

// NOTE: We need this
static int
swl_stbtt__GetGlyphInfoT2(const swl_stbtt_fontinfo *info, int glyph_index, int *x0, int *y0, int *x1, int *y1)
{
    // NOTE: We need this
    swl_stbtt__csctx c = SWL_STBTT__CSCTX_INIT(1);
    int r = swl_stbtt__run_charstring(info, glyph_index, &c);
    if (x0)  *x0 = r ? c.min_x : 0;
    if (y0)  *y0 = r ? c.min_y : 0;
    if (x1)  *x1 = r ? c.max_x : 0;
    if (y1)  *y1 = r ? c.max_y : 0;
    return r ? c.num_vertices : 0;
}

// NOTE: We need this  swl_stbtt_GetFontVMetrics
SWL_STBTT_DEF int
swl_stbtt_GetGlyphShape(const swl_stbtt_fontinfo *info, int glyph_index, swl_stbtt_vertex **pvertices)
{
   if (!info->cff.size)
      return swl_stbtt__GetGlyphShapeTT(info, glyph_index, pvertices);
   else
      return swl_stbtt__GetGlyphShapeT2(info, glyph_index, pvertices);
}

// NOTE: We need this
SWL_STBTT_DEF void
swl_stbtt_GetFontVMetrics(const swl_stbtt_fontinfo *info, int *ascent, int *descent, int *lineGap)
{
   if (ascent ) *ascent  = ttSHORT(info->data+info->hhea + 4);
   if (descent) *descent = ttSHORT(info->data+info->hhea + 6);
   if (lineGap) *lineGap = ttSHORT(info->data+info->hhea + 8);
}

// NOTE: We need this swl_stbtt_GetCodepointSDF swl_stbtt_GetCodepointSDF
SWL_STBTT_DEF float
swl_stbtt_ScaleForPixelHeight(const swl_stbtt_fontinfo *info, float height)
{
    int fheight = ttSHORT(info->data + info->hhea + 4) - ttSHORT(info->data + info->hhea + 6);
    return (float) height / fheight;
}

//////////////////////////////////////////////////////////////////////////////
//
// antialiasing software rasterizer
//

// NOTE: We need this
SWL_STBTT_DEF void
swl_stbtt_GetGlyphBitmapBoxSubpixel(const swl_stbtt_fontinfo *font, int glyph, float scale_x, float scale_y,float shift_x, float shift_y, int *ix0, int *iy0, int *ix1, int *iy1)
{
    int x0=0,y0=0,x1,y1; // =0 suppresses compiler warning

    // NOTE: We need this
    if (!swl_stbtt_GetGlyphBox(font, glyph, &x0,&y0,&x1,&y1))
    {
        // e.g. space character
        if (ix0) *ix0 = 0;
        if (iy0) *iy0 = 0;
        if (ix1) *ix1 = 0;
        if (iy1) *iy1 = 0;
    }
    else
    {
        // NOTE: We need this
        // move to integral bboxes (treating pixels as little squares, what pixels get touched)?
        if (ix0) *ix0 = SWL_STBTT_ifloor( x0 * scale_x + shift_x);
        if (iy0) *iy0 = SWL_STBTT_ifloor(-y1 * scale_y + shift_y);
        if (ix1) *ix1 = SWL_STBTT_iceil ( x1 * scale_x + shift_x);
        if (iy1) *iy1 = SWL_STBTT_iceil (-y0 * scale_y + shift_y);
    }
}

//////////////////////////////////////////////////////////////////////////////
//
//  Rasterizer


typedef struct swl_stbtt__edge {
   float x0,y0, x1,y1;
   int invert;
} swl_stbtt__edge;


typedef struct swl_stbtt__active_edge
{
   struct swl_stbtt__active_edge *next;
   #if SWL_STBTT_RASTERIZER_VERSION==1
   int x,dx;
   float ey;
   int direction;
   #elif SWL_STBTT_RASTERIZER_VERSION==2
   float fx,fdx,fdy;
   float direction;
   float sy;
   float ey;
   #else
   #error "Unrecognized value of SWL_STBTT_RASTERIZER_VERSION"
   #endif
} swl_stbtt__active_edge;

//////////////////////////////////////////////////////////////////////////////
//
// bitmap baking
//
// This is SUPER-AWESOME (tm Ryan Gordon) packing using stb_rect_pack.h. If
// stb_rect_pack.h isn't available, it uses the BakeFontBitmap strategy.

#define SWL_STBTT__OVER_MASK  (SWL_STBTT_MAX_OVERSAMPLE-1)

//////////////////////////////////////////////////////////////////////////////
//
// sdf computation
//

#define SWL_STBTT_min(a,b)  ((a) < (b) ? (a) : (b))
#define SWL_STBTT_max(a,b)  ((a) < (b) ? (b) : (a))

// NOTE: We need this
static float
swl_stbtt__cuberoot( float x )
{
    if (x<0)
        return -(float) SWL_STBTT_pow(-x,1.0f/3.0f);
    else
        return  (float) SWL_STBTT_pow( x,1.0f/3.0f);
}

// NOTE: We need this
static int
swl_stbtt__solve_cubic(float a, float b, float c, float* r)
{
   float s = -a / 3;
   float p = b - a*a / 3;
   float q = a * (2*a*a - 9*b) / 27 + c;
   float p3 = p*p*p;
   float d = q*q + 4*p3 / 27;
   if (d >= 0) {
      float z = (float) SWL_STBTT_sqrt(d);
      float u = (-q + z) / 2;
      float v = (-q - z) / 2;
      u = swl_stbtt__cuberoot(u);
      v = swl_stbtt__cuberoot(v);
      r[0] = s + u + v;
      return 1;
   } else {
      float u = (float) SWL_STBTT_sqrt(-p/3);
      float v = (float) SWL_STBTT_acos(-SWL_STBTT_sqrt(-27/p3) * q / 2) / 3; // p3 must be negative, since d is negative
      float m = (float) SWL_STBTT_cos(v);
      float n = (float) SWL_STBTT_cos(v-3.141592/2)*1.732050808f;
      r[0] = s + u * 2 * m;
      r[1] = s - u * (m + n);
      r[2] = s - u * (m - n);

      //SWL_STBTT_assert( SWL_STBTT_fabs(((r[0]+a)*r[0]+b)*r[0]+c) < 0.05f);  // these asserts may not be safe at all scales, though they're in bezier t parameter units so maybe?
      //SWL_STBTT_assert( SWL_STBTT_fabs(((r[1]+a)*r[1]+b)*r[1]+c) < 0.05f);
      //SWL_STBTT_assert( SWL_STBTT_fabs(((r[2]+a)*r[2]+b)*r[2]+c) < 0.05f);
      return 3;
   }
}

// NOTE: We need this
static int
equal(float *a, float *b)
{
    return (a[0] == b[0] && a[1] == b[1]);
}

// NOTE: We need this
static int
swl_stbtt__ray_intersect_bezier(float orig[2], float ray[2], float q0[2], float q1[2], float q2[2], float hits[2][2])
{
    float q0perp = q0[1]*ray[0] - q0[0]*ray[1];
    float q1perp = q1[1]*ray[0] - q1[0]*ray[1];
    float q2perp = q2[1]*ray[0] - q2[0]*ray[1];
    float roperp = orig[1]*ray[0] - orig[0]*ray[1];

    float a = q0perp - 2*q1perp + q2perp;
    float b = q1perp - q0perp;
    float c = q0perp - roperp;

    float s0 = 0., s1 = 0.;
    int num_s = 0;

    if (a != 0.0)
    {
        float discr = b*b - a*c;
        if (discr > 0.0)
        {
            float rcpna = -1 / a;
            float d = (float) SWL_STBTT_sqrt(discr);
            s0 = (b+d) * rcpna;
            s1 = (b-d) * rcpna;
            if (s0 >= 0.0 && s0 <= 1.0)
            {
                num_s = 1;
            }
            if (d > 0.0 && s1 >= 0.0 && s1 <= 1.0)
            {
                if (num_s == 0) s0 = s1;
                ++num_s;
            }
        }
    }
    else
    {
        // 2*b*s + c = 0
        // s = -c / (2*b)
        s0 = c / (-2 * b);
        if (s0 >= 0.0 && s0 <= 1.0)
            num_s = 1;
    }

    if (num_s == 0)
        return 0;
    else
    {
        float rcp_len2 = 1 / (ray[0]*ray[0] + ray[1]*ray[1]);
        float rayn_x = ray[0] * rcp_len2, rayn_y = ray[1] * rcp_len2;

        float q0d =   q0[0]*rayn_x +   q0[1]*rayn_y;
        float q1d =   q1[0]*rayn_x +   q1[1]*rayn_y;
        float q2d =   q2[0]*rayn_x +   q2[1]*rayn_y;
        float rod = orig[0]*rayn_x + orig[1]*rayn_y;

        float q10d = q1d - q0d;
        float q20d = q2d - q0d;
        float q0rd = q0d - rod;

        hits[0][0] = q0rd + s0*(2.0f - 2.0f*s0)*q10d + s0*s0*q20d;
        hits[0][1] = a*s0+b;

        if (num_s > 1)
        {
            hits[1][0] = q0rd + s1*(2.0f - 2.0f*s1)*q10d + s1*s1*q20d;
            hits[1][1] = a*s1+b;
            return 2;
        }
        else
        {
            return 1;
        }
    }
}

// NOTE: We need this
static int
swl_stbtt__compute_crossings_x(float x, float y, int nverts, swl_stbtt_vertex *verts)
{
    int i;
    float orig[2], ray[2] = { 1, 0 };
    float y_frac;
    int winding = 0;

    // make sure y never passes through a vertex of the shape
    y_frac = (float) SWL_STBTT_fmod(y, 1.0f);
    if (y_frac < 0.01f)
        y += 0.01f;
    else if (y_frac > 0.99f)
        y -= 0.01f;

    orig[0] = x;
    orig[1] = y;

    // test a ray from (-infinity,y) to (x,y)
    for (i=0; i < nverts; ++i)
    {
        if (verts[i].type == SWL_STBTT_vline)
        {
            int x0 = (int) verts[i-1].x, y0 = (int) verts[i-1].y;
            int x1 = (int) verts[i  ].x, y1 = (int) verts[i  ].y;
            if (y > SWL_STBTT_min(y0,y1) && y < SWL_STBTT_max(y0,y1) && x > SWL_STBTT_min(x0,x1)) {
                float x_inter = (y - y0) / (y1 - y0) * (x1-x0) + x0;
                if (x_inter < x)
                    winding += (y0 < y1) ? 1 : -1;
            }
        }
        if (verts[i].type == SWL_STBTT_vcurve)
        {
            int x0 = (int) verts[i-1].x , y0 = (int) verts[i-1].y ;
            int x1 = (int) verts[i  ].cx, y1 = (int) verts[i  ].cy;
            int x2 = (int) verts[i  ].x , y2 = (int) verts[i  ].y ;
            int ax = SWL_STBTT_min(x0,SWL_STBTT_min(x1,x2)), ay = SWL_STBTT_min(y0,SWL_STBTT_min(y1,y2));
            int by = SWL_STBTT_max(y0,SWL_STBTT_max(y1,y2));
            if (y > ay && y < by && x > ax)
            {
                float q0[2],q1[2],q2[2];
                float hits[2][2];
                q0[0] = (float)x0;
                q0[1] = (float)y0;
                q1[0] = (float)x1;
                q1[1] = (float)y1;
                q2[0] = (float)x2;
                q2[1] = (float)y2;
                if (equal(q0,q1) || equal(q1,q2))
                {
                    x0 = (int)verts[i-1].x;
                    y0 = (int)verts[i-1].y;
                    x1 = (int)verts[i  ].x;
                    y1 = (int)verts[i  ].y;
                    if (y > SWL_STBTT_min(y0,y1) && y < SWL_STBTT_max(y0,y1) && x > SWL_STBTT_min(x0,x1)) {
                        float x_inter = (y - y0) / (y1 - y0) * (x1-x0) + x0;
                        if (x_inter < x)
                            winding += (y0 < y1) ? 1 : -1;
                    }
                } else {
                    int num_hits = swl_stbtt__ray_intersect_bezier(orig, ray, q0, q1, q2, hits);
                    if (num_hits >= 1)
                        if (hits[0][0] < 0)
                            winding += (hits[0][1] < 0 ? -1 : 1);
                    if (num_hits >= 2)
                        if (hits[1][0] < 0)
                            winding += (hits[1][1] < 0 ? -1 : 1);
                }
            }
        }
    }
    return winding;
}

// NOTE: We need this
SWL_STBTT_DEF unsigned char*
swl_stbtt_GetGlyphSDF(const swl_stbtt_fontinfo *info, float scale, int glyph, int padding, unsigned char onedge_value, float pixel_dist_scale, int *width, int *height, int *xoff, int *yoff)
{
    float scale_x = scale, scale_y = scale;
    int ix0,iy0,ix1,iy1;
    int w,h;
    unsigned char *data;

    if (scale == 0) return NULL;

    swl_stbtt_GetGlyphBitmapBoxSubpixel(info, glyph, scale, scale, 0.0f,0.0f, &ix0,&iy0,&ix1,&iy1);

    // if empty, return NULL
    if (ix0 == ix1 || iy0 == iy1)
        return NULL;

    ix0 -= padding;
    iy0 -= padding;
    ix1 += padding;
    iy1 += padding;

    w = (ix1 - ix0);
    h = (iy1 - iy0);

    if (width ) *width  = w;
    if (height) *height = h;
    if (xoff  ) *xoff   = ix0;
    if (yoff  ) *yoff   = iy0;

    // invert for y-downwards bitmaps
    scale_y = -scale_y;

    {
        int x,y,i,j;
        float *precompute;
        swl_stbtt_vertex *verts;
        int num_verts = swl_stbtt_GetGlyphShape(info, glyph, &verts);
        data = (unsigned char *) SWL_STBTT_malloc(w * h, info->userdata);
        precompute = (float *) SWL_STBTT_malloc(num_verts * sizeof(float), info->userdata);

        for (i=0,j=num_verts-1; i < num_verts; j=i++)
        {
            if (verts[i].type == SWL_STBTT_vline)
            {
                float x0 = verts[i].x*scale_x, y0 = verts[i].y*scale_y;
                float x1 = verts[j].x*scale_x, y1 = verts[j].y*scale_y;
                float dist = (float) SWL_STBTT_sqrt((x1-x0)*(x1-x0) + (y1-y0)*(y1-y0));
                precompute[i] = (dist == 0) ? 0.0f : 1.0f / dist;
            }
            else if (verts[i].type == SWL_STBTT_vcurve)
            {
                float x2 = verts[j].x *scale_x, y2 = verts[j].y *scale_y;
                float x1 = verts[i].cx*scale_x, y1 = verts[i].cy*scale_y;
                float x0 = verts[i].x *scale_x, y0 = verts[i].y *scale_y;
                float bx = x0 - 2*x1 + x2, by = y0 - 2*y1 + y2;
                float len2 = bx*bx + by*by;
                if (len2 != 0.0f)
                    precompute[i] = 1.0f / (bx*bx + by*by);
                else
                    precompute[i] = 0.0f;
            } else
                precompute[i] = 0.0f;
        }

        for (y=iy0; y < iy1; ++y)
        {
            for (x=ix0; x < ix1; ++x)
            {
                float val;
                float min_dist = 999999.0f;
                float sx = (float) x + 0.5f;
                float sy = (float) y + 0.5f;
                float x_gspace = (sx / scale_x);
                float y_gspace = (sy / scale_y);

                int winding = swl_stbtt__compute_crossings_x(x_gspace, y_gspace, num_verts, verts); // @OPTIMIZE: this could just be a rasterization, but needs to be line vs. non-tesselated curves so a new path

                for (i=0; i < num_verts; ++i) {
                    float x0 = verts[i].x*scale_x, y0 = verts[i].y*scale_y;

                    if (verts[i].type == SWL_STBTT_vline && precompute[i] != 0.0f) {
                        float x1 = verts[i-1].x*scale_x, y1 = verts[i-1].y*scale_y;

                        float dist,dist2 = (x0-sx)*(x0-sx) + (y0-sy)*(y0-sy);
                        if (dist2 < min_dist*min_dist)
                            min_dist = (float) SWL_STBTT_sqrt(dist2);

                        // coarse culling against bbox
                        //if (sx > SWL_STBTT_min(x0,x1)-min_dist && sx < SWL_STBTT_max(x0,x1)+min_dist &&
                        //    sy > SWL_STBTT_min(y0,y1)-min_dist && sy < SWL_STBTT_max(y0,y1)+min_dist)
                        dist = (float) SWL_STBTT_fabs((x1-x0)*(y0-sy) - (y1-y0)*(x0-sx)) * precompute[i];
                        SWL_STBTT_assert(i != 0);
                        if (dist < min_dist) {
                            // check position along line
                            // x' = x0 + t*(x1-x0), y' = y0 + t*(y1-y0)
                            // minimize (x'-sx)*(x'-sx)+(y'-sy)*(y'-sy)
                            float dx = x1-x0, dy = y1-y0;
                            float px = x0-sx, py = y0-sy;
                            // minimize (px+t*dx)^2 + (py+t*dy)^2 = px*px + 2*px*dx*t + t^2*dx*dx + py*py + 2*py*dy*t + t^2*dy*dy
                            // derivative: 2*px*dx + 2*py*dy + (2*dx*dx+2*dy*dy)*t, set to 0 and solve
                            float t = -(px*dx + py*dy) / (dx*dx + dy*dy);
                            if (t >= 0.0f && t <= 1.0f)
                                min_dist = dist;
                        }
                    } else if (verts[i].type == SWL_STBTT_vcurve) {
                        float x2 = verts[i-1].x *scale_x, y2 = verts[i-1].y *scale_y;
                        float x1 = verts[i  ].cx*scale_x, y1 = verts[i  ].cy*scale_y;
                        float box_x0 = SWL_STBTT_min(SWL_STBTT_min(x0,x1),x2);
                        float box_y0 = SWL_STBTT_min(SWL_STBTT_min(y0,y1),y2);
                        float box_x1 = SWL_STBTT_max(SWL_STBTT_max(x0,x1),x2);
                        float box_y1 = SWL_STBTT_max(SWL_STBTT_max(y0,y1),y2);
                        // coarse culling against bbox to avoid computing cubic unnecessarily
                        if (sx > box_x0-min_dist && sx < box_x1+min_dist && sy > box_y0-min_dist && sy < box_y1+min_dist) {
                            int num=0;
                            float ax = x1-x0, ay = y1-y0;
                            float bx = x0 - 2*x1 + x2, by = y0 - 2*y1 + y2;
                            float mx = x0 - sx, my = y0 - sy;
                            float res[3] = {0.f,0.f,0.f};
                            float px,py,t,it,dist2;
                            float a_inv = precompute[i];
                            if (a_inv == 0.0) { // if a_inv is 0, it's 2nd degree so use quadratic formula
                                float a = 3*(ax*bx + ay*by);
                                float b = 2*(ax*ax + ay*ay) + (mx*bx+my*by);
                                float c = mx*ax+my*ay;
                                if (a == 0.0) { // if a is 0, it's linear
                                    if (b != 0.0) {
                                        res[num++] = -c/b;
                                    }
                                } else {
                                    float discriminant = b*b - 4*a*c;
                                    if (discriminant < 0)
                                        num = 0;
                                    else {
                                        float root = (float) SWL_STBTT_sqrt(discriminant);
                                        res[0] = (-b - root)/(2*a);
                                        res[1] = (-b + root)/(2*a);
                                        num = 2; // don't bother distinguishing 1-solution case, as code below will still work
                                    }
                                }
                            } else {
                                float b = 3*(ax*bx + ay*by) * a_inv; // could precompute this as it doesn't depend on sample point
                                float c = (2*(ax*ax + ay*ay) + (mx*bx+my*by)) * a_inv;
                                float d = (mx*ax+my*ay) * a_inv;
                                num = swl_stbtt__solve_cubic(b, c, d, res);
                            }
                            dist2 = (x0-sx)*(x0-sx) + (y0-sy)*(y0-sy);
                            if (dist2 < min_dist*min_dist)
                                min_dist = (float) SWL_STBTT_sqrt(dist2);

                            if (num >= 1 && res[0] >= 0.0f && res[0] <= 1.0f) {
                                t = res[0], it = 1.0f - t;
                                px = it*it*x0 + 2*t*it*x1 + t*t*x2;
                                py = it*it*y0 + 2*t*it*y1 + t*t*y2;
                                dist2 = (px-sx)*(px-sx) + (py-sy)*(py-sy);
                                if (dist2 < min_dist * min_dist)
                                    min_dist = (float) SWL_STBTT_sqrt(dist2);
                            }
                            if (num >= 2 && res[1] >= 0.0f && res[1] <= 1.0f) {
                                t = res[1], it = 1.0f - t;
                                px = it*it*x0 + 2*t*it*x1 + t*t*x2;
                                py = it*it*y0 + 2*t*it*y1 + t*t*y2;
                                dist2 = (px-sx)*(px-sx) + (py-sy)*(py-sy);
                                if (dist2 < min_dist * min_dist)
                                    min_dist = (float) SWL_STBTT_sqrt(dist2);
                            }
                            if (num >= 3 && res[2] >= 0.0f && res[2] <= 1.0f) {
                                t = res[2], it = 1.0f - t;
                                px = it*it*x0 + 2*t*it*x1 + t*t*x2;
                                py = it*it*y0 + 2*t*it*y1 + t*t*y2;
                                dist2 = (px-sx)*(px-sx) + (py-sy)*(py-sy);
                                if (dist2 < min_dist * min_dist)
                                    min_dist = (float) SWL_STBTT_sqrt(dist2);
                            }
                        }
                    }
                }
                if (winding == 0)
                    min_dist = -min_dist;  // if outside the shape, value is negative
                val = onedge_value + pixel_dist_scale * min_dist;
                if (val < 0)
                    val = 0;
                else if (val > 255)
                    val = 255;
                data[(y-iy0)*w+(x-ix0)] = (unsigned char) val;
            }
        }
        SWL_STBTT_free(precompute, info->userdata);
        SWL_STBTT_free(verts, info->userdata);
    }
    return data;
}

// NOTE: We need this
SWL_STBTT_DEF unsigned char*
swl_stbtt_GetCodepointSDF(const swl_stbtt_fontinfo *info, float scale, int codepoint, int padding, unsigned char onedge_value, float pixel_dist_scale, int *width, int *height, int *xoff, int *yoff)
{
    // NOTE: We need this
    int glyphIndex = swl_stbtt_FindGlyphIndex(info, codepoint);
    // NOTE: We need this
    unsigned char* pData = swl_stbtt_GetGlyphSDF(info, scale, glyphIndex, padding, onedge_value, pixel_dist_scale, width, height, xoff, yoff);
    return pData;
}

// NOTE: We need this
SWL_STBTT_DEF void
swl_stbtt_FreeSDF(unsigned char *bitmap, void *userdata)
{
    // NOTE: We need this
    SWL_STBTT_free(bitmap, userdata);
}

//////////////////////////////////////////////////////////////////////////////
//
// font name matching -- recommended not to use this
//

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
#endif

// NOTE: We need this swl_stbtt_ScaleForPixelHeight swl_stbtt_ScaleForPixelHeight
SWL_STBTT_DEF int
swl_stbtt_InitFont(swl_stbtt_fontinfo *info, const unsigned char *data, int offset)
{
   return swl_stbtt_InitFont_internal(info, (unsigned char *) data, offset);
}

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic pop
#endif


#endif // STB_TRUETYPE_IMPLEMENTATION


/*
------------------------------------------------------------------------------
ALTERNATIVE B - Public Domain (www.unlicense.org)
This is free and unencumbered software released into the public domain.
Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
software, either in source code form or as a compiled binary, for any purpose,
commercial or non-commercial, and by any means.
In jurisdictions that recognize copyright laws, the author or authors of this
software dedicate any and all copyright interest in the software to the public
domain. We make this dedication for the benefit of the public at large and to
the detriment of our heirs and successors. We intend this dedication to be an
overt act of relinquishment in perpetuity of all present and future rights to
this software under copyright law.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
------------------------------------------------------------------------------
*/
