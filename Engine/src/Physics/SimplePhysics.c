#include "SimplePhysics.h"

#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>

void
_describe_pipeline_suslik()
{
    /*
      DOCS

      ~1. DoSmthInPhysicalWorld
      2. UpdatePhysics
         1. DetectCollision
         2. SolveCollision
         3. Integrating
      ~3. Rendere

      [DetectCollision]:

      Physical body:
      1. Velocity
      2. Mass
      3. Momentum
      4. MassCenterPosition

      Geometry:
      1. Box
      2. MultiBox
      3. Primitive
      4. Sphere
      5. None (etc..)

      Two steps:
      1. GetNormalOfEachCollision()
      2. GetDepthOfEachCollision()


      [SolveCollision]:


     */
}

void
_describe_engine_book()
{
    /*
      DOCS():
      Rigid-Body Iterative Impulse-based Physics Engine
    */
}

void
simple_physics_test()
{
    GINFO("Physics study started !!!\n");
}
