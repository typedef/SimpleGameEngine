#include "SuiDemo.h"

#include <Application/Application.h>
#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>
#include "AssetManager/AssetManager.h"
#include "Graphics/Vulkan/VulkanSimpleApi.h"
#include "Graphics/Vulkan/Vsr2d.h"
#include "Sui.h"
#include "UI/SuifBackend.h"
#include <Math/SimpleMath.h>
#include <Math/SimpleMathIO.h>
#include <stdio.h>


#define IS_ENABLE_DONE_PART(x) (1 && x)

#define IS_DROP_DOWN_DEMO IS_ENABLE_DONE_PART(0)
#define IS_SCROLL_TEST_DEMO IS_ENABLE_DONE_PART(0)
#define IS_SUI_DEBUG_PANEL IS_ENABLE_DONE_PART(1)
#define IS_SUI_INTERNATIONAL_DEMO IS_ENABLE_DONE_PART(0)
#define IS_SUI_PANEL_ADD_DEMO IS_ENABLE_DONE_PART(0)
#define IS_SUI_ALIGN_DEMO IS_ENABLE_DONE_PART(0)
#define IS_SHORT_PANEL_CONTENT_DEMO IS_ENABLE_DONE_PART(0)
#define IS_SUI_DYNAMIC_BTNS_DEMO IS_ENABLE_DONE_PART(0)

#define IS_SUI_LAYOUT_DEMO 1 //IS_ENABLE_DONE_PART(0)

void
sui_demo_complicated_layout()
{
    if (sui_panel_begin("Layout", SuiPanelFlags_Movable | SuiPanelFlags_LayoutExist | SuiPanelFlags_Scrollable))
    {
        SuiPanel* pCurrentPanel = sui_get_current_panel();
        sui_text("Current panel size: %f %f", pCurrentPanel->Size.Width, pCurrentPanel->Size.Height);

        /*
          NOTE(): Все non-Root children имеют проблемы с линковкой, кроме h
        */
        SuiPanel* pSuiPanel = sui_get_current_panel();

        static i32 flag = 1;

        // note: VS
        sui_panel_push_layout(SuiWidgetLayoutType_HorizontalStack);
        sui_button("a", v2_0());
        sui_panel_push_layout(SuiWidgetLayoutType_VerticalStack);
        sui_button("b", v2_0());
        sui_button("c", v2_0());
        sui_panel_pop_layout();
        sui_panel_pop_layout();

        sui_button("d", v2_0());
        sui_panel_push_layout(SuiWidgetLayoutType_HorizontalStack);
        sui_button("e", v2_0());
        sui_panel_pop_layout();

        sui_button("f", v2_0());
        sui_button("h", v2_0());

        sui_panel_push_layout(SuiWidgetLayoutType_HorizontalStack);
        sui_button("i", v2_0());
        /* {SuiWidget* pWidget = sui_get_current_widget(); */
        /*     sui_text("uuid: %ld", pWidget->Id);} */
        if (sui_button("j", v2_0()) == SuiState_Clicked)
        {
            GINFO("CLicked on j!\n");
        }
        sui_panel_pop_layout();

        //flag = 0;

    }
    sui_panel_end();
}

void
sui_demo_easy_layout()
{
    if (sui_panel_begin("Layout", SuiPanelFlags_Movable | SuiPanelFlags_LayoutExist | SuiPanelFlags_Scrollable))
    {

        SuiPanel* pCurrentPanel = sui_get_current_panel();

        if (application_get()->Stats.FramesCount > 1)
        {
            static i32 doones = 1;
            if (doones)
            {
                doones = 0;
                // _sui_tree_print(pSuiPanel);
                sui_new_tree_print(pCurrentPanel);
            }
        }


        sui_text("Current panel size: %f %f", pCurrentPanel->Size.Width, pCurrentPanel->Size.Height);

        SuiPanel* pSuiPanel = sui_get_current_panel();

        sui_panel_push_layout(SuiWidgetLayoutType_HorizontalStack);
        sui_button("a", v2_0());
        sui_button("b", v2_0());
        sui_button("c", v2_0());
        sui_panel_pop_layout();

        sui_button("d", v2_0());
        sui_button("e", v2_0());
    }
    sui_panel_end();
}


void
sui_demo_of_frontend_usage()
{

#if IS_DROP_DOWN_DEMO == 1
    if (sui_panel_begin("Dropdown testing", SuiPanelFlags_Movable))
    {
        /* if (sui_popup_begin("some popup", SuiPopupFlags_None)) */
        /* { */
        /*     if (sui_button("Some btn inside popup", v2_0())) */
        /*     { */
        /*     } */
        /* } */
        /* sui_popup_end(); */

        /* char* texts[5] = { */
        /*     "Вариант 0", */
        /*     "Вариант 1", */
        /*     "Вариант 2", */
        /*     "Вариант 3", */
        /*     "Вариант 4" */
        /* }; */
        /* static i32 selected = 0; */
        /* static i32 isOpened = 0; */
        /* if (sui_dropdown("Выберите вариант", texts, 5, &selected, &isOpened) == SuiState_Clicked) */
        /* { */
        /*     GINFO("Choose %s\n", texts[selected]); */
        /* } */

        SuiPanel* pPanel = sui_get_current_panel();

        sui_button("smdw241de", v2_0());
        sui_button("dasdas1de", v2_0());
        sui_button("smdhfhfde", v2_0());
        sui_button("styutyude", v2_0());

        //GINFO("cnt: %ld\n", array_count(pPanel->aWidgets));

    }
    sui_panel_end();
#endif

#if IS_SCROLL_TEST_DEMO == 1

    if (sui_panel_begin("ScrollTest", SuiPanelFlags_Movable))
    {
        SuiPanel* pSearchedPanel = sui_get_panel_by_label("ForSuiDebug");
        if (pSearchedPanel != NULL)
        {
            sui_text("ASize: %0.0f %0.0f", pSearchedPanel->Size.X, pSearchedPanel->Size.Y);
            sui_text("NSize: %0.0f %0.0f", pSearchedPanel->NeededSize.X, pSearchedPanel->NeededSize.Y);

            v2 scrollSize = sui_panel_get_y_scroll_size(pSearchedPanel);
            v2 scrollBtnSize = sui_panel_get_y_scroll_button_size(pSearchedPanel);
            sui_text("ScrollSize: %f %f", scrollSize.X, scrollSize.Y);
            sui_text("ScrollBtnSize: %f %f", scrollBtnSize.X, scrollBtnSize.Y);
            f32 offsetRemain = pSearchedPanel->NeededSize.Height - pSearchedPanel->Size.Height;
            sui_text("Offset remain: %f", offsetRemain);

            v2 scrollOffset = pSearchedPanel->ScrollOffset;

            if (sui_slider_f32("Layout.ScrollX", &scrollOffset.X, v2_new(-pSearchedPanel->Size.Width, 0)) == SuiState_Changed)
            {
                pSearchedPanel->ScrollOffset.X = scrollOffset.X;
            }

            if (sui_slider_f32("Layout.ScrollY", &scrollOffset.Y, v2_new(0, pSearchedPanel->Size.Height)) == SuiState_Changed)
            {
                pSearchedPanel->ScrollOffset.Y = scrollOffset.Y;
            }

        }
    }
    sui_panel_end();

#endif

#if IS_SUI_DEBUG_PANEL == 1
    if (sui_panel_begin("ForSuiDebug", SuiPanelFlags_Movable | SuiPanelFlags_Scrollable))
    {
        if (sui_text("FontDebug: %d", sui_backend_get_font_debug()) == SuiState_Clicked)
        {
            sui_backend_set_font_debug();
        }

        if (sui_text("ScissordDebug: %d", sui_backend_get_scissors_debug()) == SuiState_Clicked)
        {
            sui_backend_set_scissors_debug();
        }

        Application* pApp = application_get();

        static i32 fps;
        static f32 tsMs;
        static i32 uiFps;
        static f32 uiTs;
        static SimpleTimerData timerData = { .WaitSeconds = 0.2f };
        if (simple_timer_interval(&timerData, pApp->Stats.Timestep))
        {
            fps = 1 / pApp->Stats.Timestep;
            tsMs = 1000 * pApp->Stats.Timestep;
            uiFps = fps / pApp->Stats.UiFpsSyncRatio;
            uiTs = 1000.0f / uiFps;
        }

        sui_text("Fps: %d (ts %f ms)", fps, tsMs);
        sui_text("UiFps: %d (%f ms)", uiFps, uiTs);
        sui_text("UiFpsSyncRatio: %d", pApp->Stats.UiFpsSyncRatio);
        sui_text("Frame Index: %d", pApp->Stats.FramesCount);
        sui_text("Time: %f", pApp->Stats.CurrentTime);

        SuiInputData* pSuiInputData = sui_get_input_data();
        v2 mp = pSuiInputData->MousePosition;
        sui_text("Мышь %0.0f %0.0f (Координаты в шейдере) %0.0f %0.0f", mp.X, mp.Y, mp.X, sui_get_instance()->Monitor.DisplaySize.Height - mp.Y);

        VsrStats stats = {};
        sui_backend_get_renderer_statistics(&stats);
        sui_text("Вершины: %d / %d", stats.VerticesCount, stats.MaxVerticesCount);
        sui_text("Текстуры: %d / %d", stats.TexturesCount, stats.MaxTexturesCount);
        sui_text("Прямоугольники: %d / %d", stats.ItemsCount, stats.MaxItemsCount);

        SuiPanel* pActivePanel = sui_get_active_panel();
        if (pActivePanel != NULL)
        {
            sui_text("Активная панель: %.*ls", pActivePanel->Label->Length, pActivePanel->Label->Buffer);
        }

        SuiDrawData* pDrawData = sui_get_draw_data();
        if (pDrawData != NULL)
        {
            sui_text("DrawData .ActiveId=%d .HotId=%d", pDrawData->ActiveID, pDrawData->HotID);
        }

        i32 btnDrawn = sui_backend_get_button_draw();
        if (sui_checkbox("Рендерить кнопки", &btnDrawn) == SuiState_Clicked)
        {
            sui_backend_set_button_draw();
        }

        i32 textDrawn = sui_backend_get_text_draw();
        if (sui_checkbox("Рендерить текст", &textDrawn) == SuiState_Clicked)
        {
            GINFO("TextDrawn: %d\n", textDrawn);
            sui_backend_set_text_draw();
        }

        i32 checkboxDrawn = sui_backend_get_checkbox_draw();
        if (sui_checkbox("Рендерить чекбокс", &checkboxDrawn) == SuiState_Clicked)
        {
            sui_backend_set_checkbox_draw();
        }

        i32 imageDrawn = sui_backend_get_image_draw();
        if (sui_checkbox("Рендерить картинки", &imageDrawn) == SuiState_Clicked)
        {
            sui_backend_set_image_draw();
        }

        i32 sliderDrawn = sui_backend_get_slider_draw();
        if (sui_checkbox("Рендерить сладер", &sliderDrawn) == SuiState_Clicked)
        {
            sui_backend_set_slider_draw();
        }

    }
    sui_panel_end();

#endif // IS_SUI_DEBUG_PANEL

#if IS_SUI_INTERNATIONAL_DEMO == 1

    if (sui_panel_begin("Русский язык", SuiPanelFlags_Movable | SuiPanelFlags_Scrollable))
    {
        SuiPanel* pCurrentPanel = sui_get_current_panel();
        sui_text("Current panel size: %f %f", pCurrentPanel->Size.Width, pCurrentPanel->Size.Height);
        SuiPanelFlags flags = pCurrentPanel->Flags;
        //GINFO("Flag: %d %d\n", (flags & SuiPanelFlags_WithOutHeader), (flags & SuiPanelFlags_Movable));

        if (sui_button("Очень длинное *сообщение*", v2_new(0,0)) == SuiState_Clicked)
        {
            static i32 clickCounter = 0;
            GINFO("It works %d!\n", clickCounter);
            ++clickCounter;
        }

        static f32 sliderF32Value = 3.14f;
        SuiState sliderState = sui_slider_f32("Лалала", &sliderF32Value, v2_new(0, 100));

        static f32 sliderNot100 = 45.0f;
        sui_slider_f32("Лалала2", &sliderNot100, v2_new(35, 155));

        static i32 someBoolValue = 0;
        SuiState checkBoxState = sui_checkbox("Чекбокс", &someBoolValue);

        SuiState textState = sui_text("Формат строка f:%0.2f d:%d t:%ls %ls", 3.14f, 1020, L"Новый текст", L"English text");

        if (sui_text("Число ПИ равно %f", 3.14f) == SuiState_Clicked)
        {
            GINFO("SuiText as Button!\n");
        }
        sui_button("Lalsl921swaw", (v2){0,0});
        sui_button("Lalsl921swaw21", (v2){0,0});
        sui_button("Lalsl921swaw23", (v2){0,0});

        static i32 isHdrActive = 0;
        sui_collapse_header("Эта hdr collapse", &isHdrActive);
        if (isHdrActive)
        {
            static i32 isOtherHdrActive = 0;
            sui_collapse_header("Эта штука", &isOtherHdrActive);
            if (isOtherHdrActive)
            {
                sui_text("Число ПИ 2равно %f", 3.14f);
                sui_text("Число ПИ 3равно %f", 3.14f);
            }

            sui_text("Ч %f", 3.14f);
            sui_text("Чравно %f", 3.14f);
            sui_text("Чвно %f", 3.14f);
        }

    }
    sui_panel_end();

#endif // IS_SUI_INTERNATIONAL_DEMO

#if IS_SUI_PANEL_ADD_DEMO == 1

    // NOTE(typedef): Панель с одинаковым Label добавляет содержимое к уже существующей панели
    if (sui_panel_begin("Панель с добавлением", SuiPanelFlags_WithOutHeader | SuiPanelFlags_Movable))
    {
        SuiPanel* pCurrentPanel = sui_get_current_panel();
        sui_text("Current panel size: %f %f", pCurrentPanel->Size.Width, pCurrentPanel->Size.Height);

        if (sui_button("Раз", v2_0()) == SuiState_Clicked)
        {
            GINFO("ONe\n");
        }

        sui_button("Раз два", v2_0());
        sui_button("Раз два три", v2_0());
    }
    sui_panel_end();

    // BUG: Weird resize beh
    if (sui_panel_begin("Панель с добавлением", SuiPanelFlags_WithOutHeader | SuiPanelFlags_Movable))
    {
        static i64 imageId = -1;
        if (imageId == -1)
        {
            imageId = asset_manager_load_texture_id(
                asset_texture("other/grass.png"));
        }
        sui_image("NewImage", imageId, v2_new(50, 50), v4_new(1,1,1,1));

        if (sui_button("Раз два три четыре", v2_0()) == SuiState_Clicked)
        {
            GINFO("Clicked!\n");
        }
    }
    sui_panel_end();

#endif // IS_SUI_PANEL_ADD_DEMO

#if IS_SUI_ALIGN_DEMO == 1

    if (sui_panel_begin("Btn align", SuiPanelFlags_WithOutHeader | SuiPanelFlags_Movable))
    {
        SuiPanel* pCurrentPanel = sui_get_current_panel();
        sui_text("Current panel size: %f %f", pCurrentPanel->Size.Width, pCurrentPanel->Size.Height);

        if (sui_button("Раз", v2_0()) == SuiState_Clicked)
        {
            GINFO("Clicked!\n");
        }



    }
    sui_panel_end();

#endif // IS_SUI_ALIGN_DEMO

#if IS_SHORT_PANEL_CONTENT_DEMO == 1
    if (sui_panel_begin("Hello Panel", SuiPanelFlags_Movable))
    {
        sui_button("Do sh", v2_0());
    }
    sui_panel_end();
#endif


#if IS_SUI_DYNAMIC_BTNS_DEMO == 1
    if (sui_panel_begin("Dynamic btns testing", SuiPanelFlags_Movable | SuiPanelFlags_Scrollable_Y))
    {
        //static wchar buf[128] = L"Введите значение";
        //SuiState inputState = sui_input_string("Поле для ввода", buf, wcslen(buf), 128);

        static i32 counter = 1;
        static i32 isFlag = 0;
        if (sui_button("Добавить и", v2_new(190, 50)) == SuiState_Clicked)
        {
            GINFO("Button Plus: %d\n", counter);
            ++counter;
            isFlag = !isFlag;
        }

        if (sui_button("Уменьшить", v2_new(190, 50)) == SuiState_Clicked)
        {
            --counter;
        }

        for (i32 i = 0; i < counter; ++i)
        {
            char btnName[64] = {};
            snprintf(btnName, 64, "Текст для кнопки %d", i);
            if (sui_button(btnName, v2_0()) == SuiState_Clicked)
            {
                GINFO("Btn[%d] is clicked\n", i);
            }
        }
    }
    sui_panel_end();
#endif

#if IS_SUI_LAYOUT_DEMO == 1
    //sui_demo_complicated_layout();
    sui_demo_easy_layout();
#endif

}
