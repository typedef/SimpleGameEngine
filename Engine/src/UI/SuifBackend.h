#ifndef SUIF_BACKEND_H
#define SUIF_BACKEND_H

#include <Core/Types.h>


struct Event;
struct VsrStats;
struct Vsr2dCompositor;


void sui_backend_create();
void sui_backend_destroy();
void sui_backend_new_frame();
void sui_backend_draw();
void sui_backend_event(struct Event* event);
void sui_backend_window_resized();

void sui_backend_set_font_debug();
int  sui_backend_get_font_debug();
void sui_backend_set_scissors_debug();
int sui_backend_get_scissors_debug();
void sui_backend_set_button_draw();
void sui_backend_set_text_draw();
void sui_backend_set_checkbox_draw();
void sui_backend_set_slider_draw();
void sui_backend_set_image_draw();

i8 sui_backend_get_button_draw();
i8 sui_backend_get_text_draw();
i8 sui_backend_get_checkbox_draw();
i8 sui_backend_get_image_draw();
i8 sui_backend_get_slider_draw();

void sui_backend_get_renderer_statistics(struct VsrStats*);

#endif // SUIF_BACKEND_H
