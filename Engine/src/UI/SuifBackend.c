#include "SuifBackend.h"
#include <Graphics/Vulkan/Vsr2d.h>
#include <Graphics/Vulkan/Vsr3dCompositor.h>

#include <Deps/stb_sprintf.h>

#include <Application/Application.h>
#include <AssetManager/AssetManager.h>
#include <Core/SimpleStandardLibrary.h>
#include <Event/Event.h>
#include <EntitySystem/Components/Base/CameraComponent.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Graphics/Vulkan/Vsr2dCompositor.h>
#include <Graphics/Vulkan/VsrCompositor.h>
#include <InputSystem/SimpleInput.h>
#include <Math/SimpleMath.h>
#include <UI/Sui.h>

#pragma clang diagnostic ignored "-Wswitch"


static Vsr2dCompositor* pVsr2dCompositor = NULL;
static CameraComponent gCameraComponent = {};
static SuiFunctionBackend gSuiBackend = {};


void
sui_backend_draw_rect(v3 position, v2 size, v4 color)
{
    vsr_2d_compositor_submit_rect(pVsr2dCompositor, position, size, color);
}

void
sui_backend_draw_text(void* buffer, size_t length, i32 scale, v3 position, v3 rotation, v2 drawable, v4 glyphColor)
{
    vsr_2d_compositor_submit_text_ext(pVsr2dCompositor, buffer, length, scale, position, rotation, drawable, glyphColor);
}

void
sui_backend_draw_image(v3 position, v2 size, v4 color, i32 textureId)
{
    VsaTexture vsaTexture = asset_manager_get_texture(textureId);
    // vsr_2d_compositor_submit(pVsr2dCompositor, position, size, color, vsaTexture);

    v3 p0 = v3_sub_y(position, size.Height);
    v3 p1 = position;
    v3 p2 = v3_new(position.X + size.Width, position.Y, position.Z);
    v3 p3 = v3_new(position.X + size.Width, position.Y - size.Height, position.Z);

    v2 uv0 = v2_new(1, 1);
    v2 uv1 = v2_new(1, 0);
    v2 uv2 = v2_new(0, 0);
    v2 uv3 = v2_new(0, 1);

    v3 poss[4] = { p0, p1, p2, p3 };
    v2 uvs[4]  = { uv0, uv1, uv2, uv3 };

    vsr_2d_compositor_submit_items(pVsr2dCompositor, poss, uvs, 4, 6, 0, color, vsaTexture);
}

void
sui_backend_draw_items(v3* positions, v2* uvs, size_t itemsCount, size_t indicesCount, i32 isFont, v4 color, i32 textureId)
{
    VsaTexture vsaTexture = asset_manager_get_texture(textureId);
    vsr_2d_compositor_submit_items(pVsr2dCompositor, positions, uvs, itemsCount, indicesCount, isFont, color, vsaTexture);
}

void
sui_backend_set_visible_rect(v2 min, v2 max)
{
    VsrVisibleRect vRect = { .Min = min, .Max = max };
    vsr_2d_compositor_set_visible_rect(pVsr2dCompositor, vRect);
}

void
sui_backend_draw_empty_rect(v3 position, v2 size, f32 thickness, v4 color)
{
    vsr_2d_compositor_submit_empty_rect(pVsr2dCompositor, position, size, thickness, color);
}

void
sui_backend_draw_circle(v3 position, v2 size, v4 color, f32 thikness, f32 fade, i32 multiplier)
{
    vsr_2d_compositor_submit_circle_ext(pVsr2dCompositor, position, size, color, thikness, fade, multiplier);
}

void
sui_backend_draw_line(v3 start, v3 end, f32 thickness, v4 color)
{
    vsr_2d_compositor_submit_line(&pVsr2dCompositor->ScreenRender, start, end, thickness, color);
}

void
sui_backend_flush()
{
    vsr_2d_compositor_flush_screen(pVsr2dCompositor);
}

#include <Math/SimpleMathIO.h>

void
sui_backend_create()
{
    struct SimpleWindow* pWindow = application_get_window();
    CameraComponentCreateInfo createInfo = {
        .IsCameraMoved = 1,
        .Base = 0,

        .Settings = {
            .Type = CameraType_OrthographicSpectator,
            .Position = v3_new(0, 0, 1.0f),
            .Front = v3_new(0, 0, -1),
            .Up    = v3_new(0, 1,  0),
            .Right = v3_new(1, 0,  0),
            .Yaw = 0,
            .Pitch = 0,
            .PrevX = 0,
            .PrevY = 0,
            .SensitivityHorizontal = 0.35f,
            .SensitivityVertical = 0.45f
        },

        .Orthographic = {
            .Near = 0.1f,
            .Far = 1000.0f,
            .Left = 0,
            .Right = (f32) pWindow->Width,
            .Bot = 0,
            .Top = (f32) pWindow->Height
        },

        .Spectator = {
            .Speed = 5.0f,
            .Zoom = 0
        }
    };

    gCameraComponent =
        camera_component_new_ext(createInfo);
    camera_component_update(&gCameraComponent);

    static VsaFont vf = {};
    vf = asset_manager_load_font(resource_font("NotoSans.ttf"));

    Vsr2dSettings screenRender = {
        .MaxVerticesCount = I32T(15, 000),
        .MaxInstancesCount = 5000,
        .MaxTexturesCount = 100,
        .pName = "Ui Renderer",
        .Font = vf,
        .pCamera = &gCameraComponent.Base
    };

    m4_print(gCameraComponent.Base.ViewProjection, "viewproj");
    m4_print(m4_t(gCameraComponent.Base.ViewProjection), "viewproj");
    //vassert_break();


    Vsr2dCompositor compositor = {};
    vsr_2d_compositor_create_screen(&compositor, &screenRender);

    Vsr2dSettings screenRender2 = {
        .MaxVerticesCount = I32T(5, 000),
        .MaxInstancesCount = 500,
        .MaxTexturesCount = 10,
        .pName = "Debug Renderer",
        .Font = vf,
        .pCamera = &gCameraComponent.Base
    };

    vsr_2d_compositor_create_screen2(&compositor, &screenRender);

    pVsr2dCompositor = vsr_compositor_register("Compositor2d", &compositor, sizeof(Vsr2dCompositor), (OnVsrDestroy) vsr_2d_compositor_destroy);

    SuiInstanceCreateInfo suiInstanceCreateInfo = {
        .DisplaySize = window_get_size_v2(pWindow),
        .pFont = &vf.Font
    };
    sui_init(suiInstanceCreateInfo);

    SuiBackendCreateInfo backCi = {
        .Functions = (SuiFunctionBackend) {
            .DrawRect = sui_backend_draw_rect,
            .DrawImage = sui_backend_draw_image,
            .DrawText = sui_backend_draw_text,
            .DrawItems = sui_backend_draw_items,
            .DrawEmptyRect = sui_backend_draw_empty_rect,
            .DrawCircle = sui_backend_draw_circle,
            .DrawLine = sui_backend_draw_line,
            .Flush = sui_backend_flush,
            .SetVisibleRect = sui_backend_set_visible_rect,
        },

        .DebugConfig = {
            .IsButtonDrawn = 1,
            .IsTextDrawn = 1,
            .IsCheckboxDrawn = 1,
            .IsSliderDrawn = 1,
            .IsImageDrawn = 1,
            .IsCollapseHeaderDebug = 1,
            .IsFontDebug = 0,
        }
    };

    sui_backend_init(backCi);

}

void
sui_backend_destroy()
{
    sui_destroy();
}

void
sui_backend_new_frame()
{
    SuiInstance* suiInstance = sui_get_instance();
    suiInstance->Monitor.DisplaySize = window_get_size_v2(application_get()->Window);

    // DOCS(typedef): We need convert coordinates to OpenGL friendly
    suiInstance->InputData.MousePositionChanged = 0;
    v2 mousePosition = input_get_cursor_position_v2();
    v2 newMousePosition =
#if 0
        mousePosition;
#else
    v2_new(mousePosition.X, suiInstance->Monitor.DisplaySize.Height - mousePosition.Y);
#endif
    if (v2_not_equal_epsilon(newMousePosition, mousePosition, 0.00001f))
    {
        suiInstance->InputData.PrevMousePosition =
            suiInstance->InputData.MousePosition;
        suiInstance->InputData.MousePosition = newMousePosition;
        suiInstance->InputData.MousePositionChanged = 1;
    }
    else
    {
        //v2_print(mousePosition);
        //v2_print(suiInstance->InputData.PrevMousePosition);
    }
}


#include <Math/SimpleMathIO.h>

void
sui_backend_draw()
{
    Vsr2dCompositor* pCompositor = pVsr2dCompositor;

    sui_backend_draw_ui();
}

void
sui_backend_event(struct Event* pEvent)
{
    SuiInputData* pInput = sui_get_input_data();

    SuiKeyState suiKeyState;
    switch (pEvent->Type)
    {
    case EventType_KeyRealeased:
    case EventType_MouseButtonReleased:
        suiKeyState = SuiKeyState_Released;
        break;

    case EventType_KeyPressed:
    case EventType_MouseButtonPressed:
        suiKeyState = SuiKeyState_Pressed;
        break;

    case EventType_WindowResized:
    {
        WindowResizedEvent* wre = (WindowResizedEvent*) pEvent;
        gCameraComponent.Orthographic.Right = wre->Width;
        gCameraComponent.Orthographic.Top   = wre->Height;
        camera_component_set_orthograhic(&gCameraComponent);
        break;
    }

    default:
        suiKeyState = SuiKeyState_None;
        break;
    }

    switch (pEvent->Category)
    {

    case EventCategory_Key:
    {
        KeyReleasedEvent* kre = (KeyReleasedEvent*) pEvent;
        KeyPressedEvent* kpe = (KeyPressedEvent*) pEvent;

        if (pEvent->Type == EventType_CharTyped)
        {
            CharEvent* ce = (CharEvent*) pEvent;
            pInput->CharTyped = ce->Char;
            break;
        }

        switch (kre->KeyCode)
        {
        case KeyType_Tab:
        {
            pInput->KeyData[SuiKeys_Tab].State = suiKeyState;

            //TODO(typedef): Itegrate it inside Suif.c
            SuiInstance* pSuiInstance = sui_get_instance();
            i32* panels = pSuiInstance->PrevFrameCache.Panels;
            i32 cnt = array_count(panels);
            for (i32 i = 0; i < cnt; ++i)
            {
                SuiPanel* pSuiPanel = sui_panel_get_by_i(i);
                if (pSuiPanel == NULL)
                    continue;

                if (pSuiPanel->Id == pSuiInstance->PrevFrameCache.ActivePanelId)
                {
                    if (i != (cnt - 1))
                    {
                        pSuiInstance->PrevFrameCache.ActivePanelId = i + 1;
                    }
                    else
                    {
                        pSuiInstance->PrevFrameCache.ActivePanelId = 0;
                    }

                    break;
                }
            }

            pEvent->IsHandled = 1;

            break;
        }

        case KeyType_Backspace:
            pInput->KeyData[SuiKeys_Backspace].State = suiKeyState;
            break;
        case KeyType_Delete:
            pInput->KeyData[SuiKeys_Delete].State = suiKeyState;
            break;

        case KeyType_Left:
            pInput->KeyData[SuiKeys_LeftArrow].State = suiKeyState;
            break;
        case KeyType_Right:
            pInput->KeyData[SuiKeys_RightArrow].State = suiKeyState;
            break;
        case KeyType_Up:
            pInput->KeyData[SuiKeys_UpArrow].State = suiKeyState;
            break;
        case KeyType_Down:
            pInput->KeyData[SuiKeys_DownArrow].State = suiKeyState;
            break;
        case KeyType_Home:
            pInput->KeyData[SuiKeys_Home].State = suiKeyState;
            break;
        case KeyType_End:
            pInput->KeyData[SuiKeys_End].State = suiKeyState;
            break;

        }

        break;
    }

    case EventCategory_Mouse:
    {
        if (pEvent->Type == EventType_MouseScrolled)
        {
            MouseScrolledEvent* pMse = (MouseScrolledEvent*) pEvent;
            pInput->ScrollY = -pMse->YOffset;
        }
        else if (pEvent->Type == EventType_MouseButtonPressed
                 || pEvent->Type == EventType_MouseButtonReleased)
        {
            MouseButtonEvent* mbe = (MouseButtonEvent*) pEvent;
            switch (mbe->MouseCode)
            {
            case MouseButtonType_Left:
                pInput->KeyData[SuiKeys_Mouse_Left_Button].State = suiKeyState;
                break;

            case MouseButtonType_Right:
                pInput->KeyData[SuiKeys_Mouse_Right_Button].State = suiKeyState;
                break;

            case MouseButtonType_Middle:
                pInput->KeyData[SuiKeys_Mouse_Middle_Button].State = suiKeyState;
                break;
            }
            break;
        }
        else if (pEvent->Type == EventType_MouseMoved)
        {
            //suiInputData->MousePositionChanged = 1;
            break;
        }



    }

    case EventCategory_Window:
    {
        if (pEvent->Type == EventType_WindowResized)
        {
            GINFO("[sui] Resized!\n");
            sui_backend_window_resized();
        }

        break;
    }

    break;
    }

}

void
sui_backend_window_resized()
{
    SuiInstance* suiInstance = sui_get_instance();
    suiInstance->Monitor.PrevDisplaySize = suiInstance->Monitor.DisplaySize;
    suiInstance->Monitor.DisplaySize = window_get_size_v2(application_get()->Window);
}

void
sui_backend_set_font_debug()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    pCfg->IsFontDebug = !pCfg->IsFontDebug;
}

i32
sui_backend_get_font_debug()
{
    return sui_backend_get_debug_config_pointer()->IsFontDebug;
}


void
sui_backend_set_scissors_debug()
{
    pVsr2dCompositor->ScreenRender.IsScissorsDebug = !pVsr2dCompositor->ScreenRender.IsScissorsDebug;
}

i32
sui_backend_get_scissors_debug()
{
    return pVsr2dCompositor->ScreenRender.IsScissorsDebug;
}

void
sui_backend_get_renderer_statistics(VsrStats* pStats)
{
    *pStats = pVsr2dCompositor->ScreenRender.Base.Stats;
}

void
sui_backend_set_button_draw()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    pCfg->IsButtonDrawn = !pCfg->IsButtonDrawn;
}

void
sui_backend_set_text_draw()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    pCfg->IsTextDrawn = !pCfg->IsTextDrawn;
}

void
sui_backend_set_checkbox_draw()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    pCfg->IsCheckboxDrawn = !pCfg->IsCheckboxDrawn;
}

void
sui_backend_set_image_draw()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    pCfg->IsImageDrawn = !pCfg->IsImageDrawn;
}

void
sui_backend_set_slider_draw()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    pCfg->IsSliderDrawn = !pCfg->IsSliderDrawn;
}

i8
sui_backend_get_button_draw()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    return pCfg->IsButtonDrawn;
}

i8
sui_backend_get_text_draw()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    return pCfg->IsTextDrawn;
}

i8
sui_backend_get_checkbox_draw()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    return pCfg->IsCheckboxDrawn;
}

i8
sui_backend_get_slider_draw()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    return pCfg->IsSliderDrawn;
}

i8
sui_backend_get_image_draw()
{
    SuiDebugConfig* pCfg = sui_backend_get_debug_config_pointer();
    return pCfg->IsImageDrawn;
}


/*

  float myPow(float x, int n) {
  float mul = x;

  for (int i = 1; i < n; ++i) {
  mul *= x;
  }

  return mul;
  }

  vec4 circle(vec2 uv, int multiplier, float thikness, vec4 color) {
  float xn = myPow(uv.x, multiplier);
  float yn = myPow(uv.y, multiplier);
  float d = 1.0 - (xn+yn);
  float fade = 0.009;

  vec4 col = vec4(smoothstep(0.0, fade, d));
  float thicknessRatio = smoothstep(thikness, thikness - fade, d);
  col *= vec4(thicknessRatio) * color;

  return col;
  }

  void mainImage( out vec4 fragColor, in vec2 fragCoord )
  {
  vec2 uv = fragCoord/iResolution.xy * 2.0 - 1.0; // <-1,1>
  uv.x *= (iResolution.x / iResolution.y); // <-1.69, 1.69>

  vec4 col = circle(uv, 14, 1.1999922, vec4(0.0, 1.0, 1.0, 1.0));

  fragColor = col;
  }*/
