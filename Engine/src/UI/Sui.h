#ifndef SUI_H
#define SUI_H

#include <Core/Types.h>
#include <Utils/Font.h>
#include <Event/Event.h>

/*
  ####################################################
  DOCS(typedef): Base Sui Types && Functions
  ####################################################
*/
typedef i64 SuiPanelId;
typedef i64 SuiPopupId;
typedef i64 SuiWidgetId;
#define GetSuiPanelId(label) ((SuiPanelId) ((u64)label))
#define GetSuiWidgetId(label) ((SuiWidgetId) ((u64)label))

typedef enum SuiState
{
    SuiState_None      = 0     ,
    SuiState_Hovered   = 1 << 0,
    SuiState_Clicked   = 1 << 1,
    SuiState_Grabbed   = 1 << 2,
    SuiState_Changed   = 1 << 3,
} SuiState;

const char* sui_state_to_string(SuiState suiState);
void sui_state_print(SuiState suiState);
f32 sui_apply_offset(f32 fullSize, f32 marginValue, f32 value);
v3 sui_apply_offset_v3(v3 value, v2 fullSize, v2 offset);
f32 sui_get_text_width(WideString wstr);
void sui_get_text_metrics_ext(wchar* buffer, i32 length, f32* width, f32* height);
f32 sui_get_char_xoffset(wchar wc);
void sui_get_text_metrics(WideString wstr, f32* width, f32* height);

/*
  #####################################
  DOCS(typedef): Sui Layout
  #####################################
*/
typedef enum SuiPanelLayoutType
{
    SuiLayoutType_None = 0,
    SuiLayoutType_OccupyAvailableSpace,
    SuiLayoutType_Count
} SuiPanelLayoutType;

const char* sui_layout_type_to_string(SuiPanelLayoutType type);
i32 sui_layout_type_is_valid(SuiPanelLayoutType type);

// LEGACY(typedef): SuiLayoutItem
typedef struct SuiLayoutData
{
    SuiPanelLayoutType Type;
    v3 NextPosition;
    v2 NextSize;
    //f32 ZOrder;
} SuiLayoutData;

void sui_set_layout(SuiPanelLayoutType layoutType);
SuiLayoutData sui_get_layout_data(v3 pos);
v3 sui_layout_get_next_position(SuiLayoutData* pLayoutData);

typedef struct SuiPanelLayoutData
{
    SuiPanelLayoutType Type;
    v2 Occupy;
} SuiPanelLayoutData;

// todo: refactor this
typedef enum SuiWidgetLayoutType
{
    SuiWidgetLayoutType_None = 0,

    SuiWidgetLayoutType_VerticalStack,
    SuiWidgetLayoutType_HorizontalStack,

    SuiWidgetLayoutType_Count,
} SuiWidgetLayoutType;

static const char*
sui_widget_layout_type_to_string(SuiWidgetLayoutType type)
{
    switch (type)
    {
    case SuiWidgetLayoutType_None: return "None";
    case SuiWidgetLayoutType_VerticalStack: return "VerticalStack";
    case SuiWidgetLayoutType_HorizontalStack: return "HorizontalStack";
    case SuiWidgetLayoutType_Count: return "Count";
    }

    vguard(0);
    return NULL;
}

static i32
sui_widget_layout_type_is_valid(SuiWidgetLayoutType type)
{
    return ((type >= SuiWidgetLayoutType_None) && (type <= SuiWidgetLayoutType_Count));
}


typedef struct SuiWidgetLayoutData
{
    SuiWidgetLayoutType Type;
    v3 NextPosition;
    v2 Occupy;
} SuiWidgetLayoutData;

struct SuiPanel;
v3 sui_layout_panel_data_get_next_position(SuiWidgetLayoutData* pData, struct SuiPanel* pPanel);

/*
  #####################################
  DOCS(typedef): Sui Input
  #####################################
*/
typedef enum SuiKeys
{
    SuiKeys_Start = 0,

    SuiKeys_Mouse_Left_Button,
    SuiKeys_Mouse_Right_Button,
    SuiKeys_Mouse_Middle_Button,

    SuiKeys_Tab,
    SuiKeys_Backspace,

    SuiKeys_LeftArrow,
    SuiKeys_RightArrow,
    SuiKeys_UpArrow,
    SuiKeys_DownArrow,
    SuiKeys_Home,
    SuiKeys_End,

    SuiKeys_Delete,

    SuiKeys_Count
} SuiKeys;

typedef enum SuiKeyState
{
    SuiKeyState_None = 0,
    SuiKeyState_Pressed,
    SuiKeyState_Released,
} SuiKeyState;

typedef struct SuiKeyData
{
    SuiKeyState State;
    i8 IsHandled;
    /* NOTE(typedef): Maybe later try analog values here */
} SuiKeyData;

typedef enum SuiModeType
{
    SuiModeType_Insert = 0,
    SuiModeType_Overwrite
} SuiModeType;

typedef struct SuiInputData
{
    v2 MousePosition;
    v2 PrevMousePosition;
    v2 GrabPosition;
    v2 GrabMousePosition;
    i8 MousePositionChanged;
    i8 LeftKeyWasPressed;
    SuiKeyData KeyData[SuiKeys_Count];
    i32 CharTyped;

    f32 ScrollY;

    i8 IsInputAlreadyHandled;
    i8 IsInGrabState;

    /* SuiWidgetId LastFocusedID; */
    SuiWidgetId FocusID;

    i32 TextIndexPosition;
    SuiModeType Mode;

    /* /\* Input *\/ */

} SuiInputData;

/*
  #####################################
  DOCS(typedef): Sui Widgets
  #####################################
*/
typedef enum SuiWidgetType
{
    SuiWidgetType_Button = 0,
    SuiWidgetType_Slider_F32,
    SuiWidgetType_Checkbox,
    SuiWidgetType_Text,
    SuiWidgetType_InputString,
    SuiWidgetType_Dropdown,
    SuiWidgetType_Image,
    SuiWidgetType_CollapseHeader,


    SuiWidgetType_SelectList,
    SuiWidgetType_ColorPicker,
    SuiWidgetType_Count
} SuiWidgetType;

typedef struct SuiDropDown
{
    i8 VisibleCount;
    i8 SelectedIndex;
    i8 ActiveIndex;
    i8 IsOpened;
    f32 ItemHeight;
    WideString* SelectOptions;
} SuiDropDown;

typedef struct SuiWidget
{
    SuiWidgetId Id;
    SuiWidgetType Type;
    WideString* Label;
    SuiState State;
    SuiState SubState;
    v2 Size;
    v3 Position;
    i32 StillExist;

    // TODO: fix it
    WideString* TempBuffer;

    void* Data;

    union
    {
        v4 FloatValues;

        struct
        {
            i32 Flag0;
            i32 Flag1;
            i32 Flag2;
            i32 Flag3;
        };

        struct
        {
            i32 TextIndexPosition;
            i32 Flag11;
            i32 Flag22;
            i32 Flag33;
        };
    };

    union
    {
        i32 TextureId;
    };

} SuiWidget;

typedef struct SuiDrawData
{
    i32 FontMaxHeight;

    i64 ActiveID;
    i64 HotID;

    SuiPanelId ActivePanelId;
    SuiPanelId HotPanelId;

    i32 LastFocusedID;
    v2 LastFocusedPosition;
    v2 LastFocusedSize;
    i32 LeftKeyWasPressed;

    /* i32 FocusID; */
    //i32 TextIndexPosition;
    //SuiModeType Mode;

    /* Input */
    //SuiKeyData KeyData[SuiKeys_Count];
    //i32 CharTyped;
//#define SUI_MAX_CHAR_TO_SAVE 20
//    i32 CharData[SUI_MAX_CHAR_TO_SAVE];
} SuiDrawData;

/*
  #####################################
  DOCS(typedef): Sui Style
  #####################################
*/

typedef struct SuiStyleColorItem
{
    v4 Background;
    v4 Hovered;
    v4 Clicked;
} SuiStyleColorItem;

typedef struct SuiStyleSizeItem
{
    v2 Min;
    v2 Max;
    v2 Hovered;
    v2 Clicked;
} SuiStyleSizeItem;

typedef enum SuiVerticalAlignType
{
    SuiVerticalAlignType_None = 0,
    SuiVerticalAlignType_Top,
    SuiVerticalAlignType_Center,
    SuiVerticalAlignType_Bot
} SuiVerticalAlignType;

typedef enum SuiHorizontalAlignType
{
    SuiHorizontalAlignType_None = 0,
    SuiHorizontalAlignType_Left,
    SuiHorizontalAlignType_Center,
    SuiHorizontalAlignType_Right
} SuiHorizontalAlignType;

v3 sui_get_text_position_inside(WideString* text, v3 widgetPosition, v2 widgetSize, v2 padding);
v3 sui_align_get_text_position(WideString* text, v3 widgetPosition, v2 widgetSize, SuiVerticalAlignType verticalAlign, SuiHorizontalAlignType horizontalAlign, f32 marginX);


typedef struct SuiStyleOffsetItem
{
    v2 Margin;
    SuiVerticalAlignType VerticalAlign;
    SuiHorizontalAlignType HorizontalAlign;
} SuiStyleOffsetItem;

typedef struct SuiStyleItem
{
    SuiStyleColorItem Color;
    SuiStyleSizeItem Size;
    SuiStyleOffsetItem Offset;
} SuiStyleItem;

typedef enum SuiStyleItemType
{
    //DOCS(typedef): Panel
    SuiStyleItemType_Panel = 0,
    SuiStyleItemType_Panel_Header,
    SuiStyleItemType_Panel_Header_Text,
    SuiStyleItemType_Panel_Header_Close_Button,
    SuiStyleItemType_Panel_Header_Close_Button_X,
    SuiStyleItemType_Panel_Border,
    SuiStyleItemType_Panel_Scroll_X,
    SuiStyleItemType_Panel_Scroll_Y,
    SuiStyleItemType_Panel_Scroll_Button_X,
    SuiStyleItemType_Panel_Scroll_Button_Y,

    SuiStyleItemType_Widget_Button,

    SuiStyleItemType_Widget_Slider_F32,
    SuiStyleItemType_Widget_Slider_F32_Slider,

    SuiStyleItemType_Widget_Checkbox,

    SuiStyleItemType_Widget_Text,

    SuiStyleItemType_Widget_Input_String,

    SuiStyleItemType_Widget_Dropdown,

    SuiStyleItemType_Widget_Image,
    SuiStyleItemType_Widget_CollapseHeader,
    SuiStyleItemType_Widget_CollapseHeader_Text,

    SuiStyleItemType_LayoutType_VerticalStack,

    SuiStyleItemType_Count
} SuiStyleItemType;

typedef struct SuiStyle
{
    SuiStyleItem Items[SuiStyleItemType_Count];
} SuiStyle;

void sui_style_init(SuiStyle* style, Font* pFontInfo);
v4 sui_style_get_color_by_state(SuiStyle* style, SuiStyleItemType itemType, SuiState state);
SuiStyleSizeItem sui_style_get_size(SuiStyle* style, SuiStyleItemType itemType);
SuiStyleOffsetItem sui_style_get_offset(SuiStyle* style, SuiStyleItemType itemType);
SuiStyle* sui_get_style();


/*
  #####################################
  DOCS(typedef): Sui Monitor
  #####################################
*/
typedef struct SuiMonitor
{
    v2 DisplaySize;
    v2 PrevDisplaySize;
} SuiMonitor;

/*
  #####################################
  DOCS(typedef): Sui Frame Info
  #####################################
*/
typedef struct SuiFrameInfo
{
    f32 Timestep;
} SuiFrameInfo;

/*
  #####################################
  DOCS(typedef): Infrastructure
  #####################################
*/
i32 sui_frame_begin(SuiFrameInfo suiFrameInfo);
void sui_frame_end();
void sui_auto_layout();

/*
  #####################################
  DOCS(typedef): Sui Panels
  #####################################
*/
typedef enum SuiPanelFlags
{
    SuiPanelFlags_None          = 0,
    SuiPanelFlags_WithOutHeader = 1 << 0,
    SuiPanelFlags_Movable       = 1 << 1,
    SuiPanelFlags_Scrollable_X  = 1 << 2,
    SuiPanelFlags_Scrollable_Y  = 1 << 3,
    SuiPanelFlags_Scrollable    = (SuiPanelFlags_Scrollable_X | SuiPanelFlags_Scrollable_Y),

    // TODO/BUG(typedef): Only for layout development
    SuiPanelFlags_LayoutExist   = 1 << 3,
} SuiPanelFlags;

typedef struct SuiLayoutNode
{
    i32 IsExist;
    i32 Parent;
    i32* Children;
    SuiWidgetLayoutType Type;
    SuiWidgetId WidgetId;
} SuiLayoutNode;

typedef struct SuiLayout
{
    SuiLayoutNode* aNodes;
    SuiWidgetLayoutType* LayoutStack;
} SuiLayout;

SuiLayout sui_layout_new(SuiPanelLayoutType* layoutTypes);


typedef enum SuiPanelState
{
    SuiPanelState_None = 0,
    SuiPanelState_Opened,
    SuiPanelState_Closed,
    SuiPanelState_Collapsed,
} SuiPanelState;

typedef struct SuiPanel
{
    SuiPanelId Id;
    SuiPanelState PanelState;

    WideString* Label;
    SuiPanelFlags Flags;

    //SuiLayoutData LayoutData;
    // todo: not clear this
    SuiWidget* aWidgets;

    SuiState State;
    SuiState HeaderState;
    SuiState CloseButtonState;

    SuiState ScrollYState;
    SuiState ScrollXState;

    // DOCS: Full panel state, need to draw it in back-end
    SuiState FullScrollYState;
    SuiState FullScrollXState;

    /*
      DOCS(typedef): Layout related
    */
    //struct SuiLayoutNode Root;
    SuiLayoutNode* aNodes;
    SuiWidgetLayoutType* LayoutStack;

    v3 Position;
    /* DOCS(typedef): If Size == {0, 0}, then auto size */
    v2 Size;
    v2 NeededSize;

    i8 IsBlockedByOther;
    i8 IsNotFirstCall;
    i8 IsLayoutChanged;
    // todo: wip
    i8 IsWidgetsModified;
    i8 IsLayoutTreeNeedUpdate;

    // DOCS: Use this for rendering scroll on back-end side
    v2 ScrollOffset;
    // DOCS: Use this for widget offset inside panel
    v2 ScrollContentOffset;

} SuiPanel;

i32 sui_panel_begin_l(WideString* label, SuiPanelFlags flags);
i32 sui_panel_begin(char* label, SuiPanelFlags flags);
void sui_panel_end();
void sui_panel_auto_layout_widget(SuiPanel* pPanel);

SuiWidget* sui_panel_get_widget_by_label(SuiPanel* pPanel, WideString* pLabel);
SuiWidget* sui_panel_get_widget_by_id(SuiPanel* pPanel, SuiWidgetId id);

void sui_panel_calc_size(SuiPanel* pSuiPanel);
i32 sui_panel_is_scroll_visible(SuiPanel* pSuiPanel);
v2 sui_panel_get_header_size(SuiPanel* suiPanel);
v3 sui_panel_get_header_position(SuiPanel* pSuiPanel);
v3 sui_panel_get_header_text_position(SuiPanel* pSuiPanel);
v2 sui_panel_get_close_button_size(v2 hdrSize);
v3 sui_panel_get_close_button_position(SuiPanel* suiPanel, v2 closeButtonSize);
void sui_panel_get_close_button_x_coordinates(v2 fullSize, v3 startPos, v2 xMinSize, f32* psx, f32* psy, f32* pex, f32* pey, f32* pzv);
// DOCS: Y-Scroll
v2 sui_panel_get_y_scroll_size(SuiPanel* pSuiPanel);
v3 sui_panel_get_y_scroll_position(SuiPanel* pSuiPanel);
v2 sui_panel_get_y_scroll_button_size(SuiPanel* pSuiPanel);
f32 sui_panel_get_y_scroll_button_free_space(f32 scrollSizeDim, f32 marginDim);
v3 sui_panel_get_y_scroll_button_position(SuiPanel* pSuiPanel);
// DOCS: X-Scroll
v2 sui_panel_get_x_scroll_size(SuiPanel* pSuiPanel);
v3 sui_panel_get_x_scroll_position(SuiPanel* pSuiPanel);
v2 sui_panel_get_x_scroll_button_size(SuiPanel* pSuiPanel);
f32 sui_panel_get_x_scroll_button_free_space(f32 scrollSizeDim, f32 marginDim);
v3 sui_panel_get_x_scroll_button_position(SuiPanel* pSuiPanel);

v3 sui_layout_process_next_position(SuiLayoutData* pSuiLayoutData, v2 widgetSize);
void sui_panel_reset_layout(SuiPanel* pSuiPanel);
v3 sui_panel_get_widget_offset(v3 panelPos, v2 headerSize, v2 padding);
i32 sui_panel_is_active(SuiPanel* pSuiPanel);
i32 sui_panel_is_active_or_focus(SuiPanel* pSuiPanel);
SuiPanel* sui_panel_get_by_i(i32 i);
SuiPanel sui_panel_get_by_i_value(i32 i);
void sui_panel_item_make_active_by_state(SuiState state, SuiPanelId id);
SuiPanel* sui_panel_get_next_tab();
void sui_is_panel_blocked_by_other_panels(SuiPanel* pCurrentPanel);
i32 sui_panel_is_overflow(SuiPanel* pSuiPanel);

void sui_panel_push_layout(SuiWidgetLayoutType layout);
void sui_panel_pop_layout();
//todo: remove this one
void _sui_tree_print(SuiPanel* pPanel);
void sui_new_tree_print(SuiPanel* pPanel);

/*
  #####################################
  DOCS(typedef): Sui Popups
  #####################################
*/

typedef enum SuiPopupFlags
{
    SuiPopupFlags_None = 0
} SuiPopupFlags;

typedef struct SuiPopup
{
    SuiPopupId Id;
    WideString* Label;
    SuiPopupFlags Flags;
    SuiWidget* Widgets;
} SuiPopup;

i32 sui_popup_begin_l(WideString* label, SuiPopupFlags flags);
i32 sui_popup_begin(char* label, SuiPopupFlags flags);
void sui_popup_end();


/*
  #####################################
  DOCS(typedef): Sui Frame Cache
  #####################################
*/
typedef struct SuiPanelKeyValue
{
    WideString Key;
    SuiPanel Value;
} SuiPanelKeyValue;

typedef struct SuiPopupKeyValue
{
    WideString Key;
    SuiPopup Value;
} SuiPopupKeyValue;

typedef struct SuiFrameCache
{
    SuiPanelId ActivePanelId;
    SuiPanelId FocusPanelInd;
    i32 CurrentPanelInd;
    // DOCS: It's panel indices from PanelsTable
    i32* Panels;
    i32 WidgetsCount;
    // DOCS: We store all panels Here as { Label, SuiPanel }
    SuiPanelKeyValue* PanelsTable;
    SuiPopupKeyValue* PopupsTable;
} SuiFrameCache;

/*
  #####################################
  DOCS(typedef): Sui Instance
  #####################################
*/
typedef struct SuiInstanceCreateInfo
{
    v2 DisplaySize;
    Font* pFont;
} SuiInstanceCreateInfo;

typedef struct GlobalStates
{
    v2 NextPosition;
} GlobalStates;

struct SimpleArena;

typedef struct SuiInstance
{
    // DOCS: FLags
    i8 IsInitialized;
    i8 IsFirstFrame;

    Font* pFont;
    IWideStringPool IPool;
    SuiFrameInfo FrameInfo;
    struct SimpleArena* Arena;

    SuiDrawData DrawData;
    SuiInputData InputData;
    SuiWidgetLayoutData WidgetLayoutData;

    SuiStyle Style;
    SuiMonitor Monitor;

    // NOTE(typedef): mb safe this in file like imgui does
    SuiFrameCache PrevFrameCache;

    // todo: remove this
    GlobalStates States;

} SuiInstance;

void sui_init(SuiInstanceCreateInfo createInfo);
i32 sui_is_initialized();
void sui_destroy();

/*
  #####################################
  DOCS(typedef): Sui Widgets
  #####################################
*/
SuiState sui_button_l(WideString* label, v2 definedSize);
SuiState sui_button(char* label, v2 definedSize);
SuiState sui_slider_f32_l(WideString* label, f32* valuePtr, v2 minMax);
SuiState sui_slider_f32(char* label, f32* valuePtr, v2 minMax);
SuiState sui_checkbox_l(WideString* label, i32* valuePtr);
SuiState sui_checkbox(char* label, i32* valuePtr);
SuiState sui_text_l(WideString* format, ...);
SuiState sui_text(char* format, ...);
SuiState sui_input_string(WideString* label, wchar* buf, i32 len, i32 maxLength);
SuiState sui_dropdown_l(WideString* label, WideString* texts, i32 textsCount, i32* selected, i32* isOpened);
SuiState sui_dropdown(char* label, char** texts, i32 textsCount, i32* selected, i32* isOpened);
SuiState sui_image_l(WideString* label, i64 textureId, v2 size, v4 color);
SuiState sui_image(char* label, i64 textureId, v2 size, v4 color);
SuiState sui_collapse_header_l(WideString* label, i32* isOpen);
SuiState sui_collapse_header(char* label, i32* isOpen);

/*
  void* data, void* (*getValue)(void* data), void* (*getText)(void* data)
*/

f32 sui_slider_get_slide_offset_x(f32 value, v2 minMax);
v3 sui_slider_get_slide_position(v3 position, f32 offsetX);
v2 sui_slider_get_slide_size(v2 sliderSize);
void sui_checkbox_get_metrics(v3 widgetPos, v2 widgetSize, v3* checkPos, v2* checkSize);
v3 sui_input_get_text_position(WideString* text, v3 backgroundPos, v2 widgetSize);
v2 sui_input_get_text_size(v2 backgroundSize);
f32 sui_get_full_width(SuiWidget widget);

/*
  #####################################
  DOCS(typedef): Public
  #####################################
*/
SuiInstance* sui_get_instance();
SuiDrawData* sui_get_draw_data();
SuiInputData* sui_get_input_data();
//SuiLayoutData* sui_get_layout_data();
SuiPanel* sui_get_current_panel();
void sui_set_current_panel(i64 ind);
SuiPanel* sui_get_active_panel();
SuiPanel* sui_get_panel_by_label(char* label);
void sui_get_current_panel_aabb(v2* min, v2* max);
void sui_get_panel_aabb(SuiPanel* pSuiPanel, v2* min, v2* max);
void sui_get_panel_aabb_w_hdr(SuiPanel* pSuiPanel, v2* min, v2* max);
void sui_get_panel_aabb_w_sb(SuiPanel* pSuiPanel, v2* min, v2* max);
void sui_get_panel_aabb_w_hdr_sb(SuiPanel* pSuiPanel, v2* min, v2* max);
SuiWidget* sui_get_current_widget();
void sui_set_next_position(v2 position);


/*
  #####################################
  DOCS(typedef): Sui back-end
  #####################################
*/

typedef struct SuiFunctionBackend
{
    void (*DrawRect)(v3 position, v2 size, v4 color);
    void (*DrawImage)(v3 position, v2 size, v4 color, i32 textureId);
    void (*DrawText)(void* buffer, size_t length, i32 scale, v3 position, v3 rotation, v2 drawable, v4 glyphColor);
    void (*DrawItems)(v3* positions, v2* uvs, size_t itemsCount, size_t indicesCount, i32 isFont, v4 color, i32 textureId);
    void (*DrawEmptyRect)(v3 position, v2 size, f32 thickness, v4 color);
    void (*DrawCircle)(v3 position, v2 size, v4 color, f32 thikness, f32 fade, i32 multiplier);
    void (*DrawLine)(v3 start, v3 end, f32 thickness, v4 color);

    void (*Flush)();

    void (*SetVisibleRect)(v2 min, v2 max);
} SuiFunctionBackend;

typedef struct SuiDebugConfig
{
    // DOCS: Drawing Flags
    i8 IsButtonDrawn;
    i8 IsTextDrawn;
    i8 IsCheckboxDrawn;
    i8 IsSliderDrawn;
    i8 IsImageDrawn;
    i8 IsCollapseHeaderDebug;
    i8 IsFontDebug;
} SuiDebugConfig;

typedef struct SuiBackendCreateInfo
{
    SuiFunctionBackend Functions;
    SuiDebugConfig DebugConfig;
} SuiBackendCreateInfo;

void sui_backend_init(SuiBackendCreateInfo backendCreateInfo);
SuiDebugConfig* sui_backend_get_debug_config_pointer();
void sui_backend_validate_and_set(SuiFunctionBackend backend);
void sui_backend_draw_ui();

#endif // SUI_H
