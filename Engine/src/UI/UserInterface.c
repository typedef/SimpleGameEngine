#include "UserInterface.h"

#include <Application/Application.h>
#include <Graphics/SimpleWindow.h>
#include <Core/Types.h>

#include "Sui.h"
#include "SuifBackend.h"

#pragma clang diagnostic ignored "-Wincompatible-function-pointer-types"


void
ui_create()
{
    sui_backend_create();
}

void
ui_event(struct Event* pEvent)
{
    sui_backend_event(pEvent);
}

void
ui_destroy()
{
    sui_backend_destroy();
}

void
ui_begin()
{
    sui_backend_new_frame();

    sui_frame_begin((SuiFrameInfo) {.Timestep = application_get()->Stats.Timestep });
}

void
ui_end()
{
    sui_auto_layout();
    sui_backend_draw();
    sui_frame_end();
}
