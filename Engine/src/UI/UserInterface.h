#ifndef USER_INTERFACE_H
#define USER_INTERFACE_H

struct Event;

void ui_create();
void ui_event(struct Event* pEvent);
void ui_destroy();
void ui_begin();
void ui_end();

#endif // USER_INTERFACE_H
