#include "Sui.h"

#include <stdlib.h>
#include <wchar.h>
#include <stdarg.h>

#include <Application/Application.h>
#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>
#include <InputSystem/SimpleInput.h>
#include <Math/SimpleMathIO.h>


/*
  #####################################
  DOCS(typedef): Global vars
  #####################################
*/

SuiInstance gSuiInstance = {};


/*
  #####################################
  DOCS(typedef): Base Preprocessors
  #####################################
*/

#define GetDrawData() (&gSuiInstance.DrawData)
#define GetInputData() (&gSuiInstance.InputData)
#define GetStyle() (&gSuiInstance.Style)
#define GetLayoutData() (&gSuiInstance.LayoutData)

#define DefaultGuard(name)                      \
    ({                                          \
        if (!name)                              \
        {                                       \
            vguard_not_null(name);              \
        }                                       \
                                                \
        if (gSuiInstance.IsInitialized)         \
        {                                       \
            vguard(gSuiInstance.IsInitialized); \
        }                                       \
    })
#define WidgetGuard(name)                                               \
    ({                                                                  \
        DefaultGuard(name);                                             \
                                                                        \
        vguard((array_count(gSuiInstance.PrevFrameCache.Panels) > 0) || gSuiInstance.PrevFrameCache.ActivePanelId != -1); \
        vguard(gSuiInstance.PrevFrameCache.CurrentPanelInd != -1);      \
    })

#define IsLayoutSet(layout) ({ layout != SuiLayoutType_None && layout != SuiLayoutType_Count; })
#define IsLayoutNotSet(layout) ({ !IsLayoutSet(layout); })

#define IsKeyPressed(key)                                               \
    ({                                                                  \
        SuiKeyData keyData = GetInputData()->KeyData[key];              \
        ((keyData.IsHandled!=1) && (keyData.State == SuiKeyState_Pressed)); \
    })
#define IsKeyReleased(key)                                              \
    ({                                                                  \
        SuiKeyData keyData = GetInputData()->KeyData[key];              \
        ((keyData.IsHandled!=1) && (keyData.State==SuiKeyState_Released)); \
    })
#define HandleKey(key)					\
    ({							\
        GetInputData()->KeyData[key].IsHandled = 1;	\
    })
#define UnHandleKey(key)				\
    ({							\
        GetInputData()->KeyData[key].IsHandled = 0;	\
    })


/*
  #####################################
  DOCS(typedef): Sui Internal
  #####################################
*/

WideString* sui_wide_string_raw(wchar* str, i32 length);
i32 _sui_panel_compare(const void* p1, const void* p2);
void _sui_click_and_grab_state_machine(SuiInputData* pSuiInputData, SuiPanelId id, SuiState* pState, v3 pos, v2 size);
struct SuiLayoutNode* _sui_tree_get_children(struct SuiLayoutNode* current);
void _sui_new_tree_append_node(SuiPanel* pPanel, SuiWidget* pWidget);
void _sui_tree_reset_existance(struct SuiLayoutNode* current);
void _sui_tree_remove_old(struct SuiLayoutNode* current);
struct SuiLayoutNode* _node_get(SuiPanel* pPanel, i32 ind);
SuiWidget* _sui_panel_add_widget(SuiPanel* pSuiPanel, SuiWidget suiWidget);
SuiWidgetId _sui_widget_get_id(WideString* label, SuiPanel* pSuiPanel);


/*
  ####################################################
  DOCS(typedef): Base Sui Types && Functions
  ####################################################
*/
const char*
sui_state_to_string(SuiState suiState)
{
    switch (suiState)
    {
    case SuiState_None: return "None";
    case SuiState_Hovered: return "Hovered";
    case SuiState_Clicked: return "Clicked";
    case SuiState_Grabbed: return "Grabbed";
    case SuiState_Changed: return "Changed";
    }

    vguard(0);
    return "";
}


void
sui_state_print(SuiState suiState)
{
#if 0
    if (suiState == SuiState_None)
    {
        //GSUCCESS("State: 0 0 0 0\n");
        return;
    }

    GSUCCESS("State: H: %d C: %d G: %d\n",
             (suiState & SuiState_Hovered),
             (suiState & SuiState_Clicked),
             (suiState & SuiState_Grabbed));
#endif
}


/*
  DOCS(typedef): Usage
  sui_apply_offset(100.0, 0.1 | 15.0f, 100.0f)
*/
f32
sui_apply_offset(f32 fullSize, f32 marginValue, f32 value)
{
    f32 result = 0.0f;
    f32 marginValueAbs = f32_abs(marginValue);
    if (marginValueAbs < 1.0f)
    {
        return value + fullSize * marginValue;
    }
    else
    {
        return value + marginValue;
    }
}

/*
  DOCS(typedef):
  size   - Reference size if offset not in absolute values but reference
  offset - Margin or Padding
*/
v2
_sui_apply_offset(v2 size, v2 offset)
{
    if (offset.X > 1.1f)
    {
        size.Width = offset.X;
    }
    else
    {
        size.Width = size.Width * offset.X;
    }

    if (offset.Y > 1.1f)
    {
        size.Height = offset.Y;
    }
    else
    {
        size.Height = size.Height * offset.Y;
    }

    return size;
}

f32
sui_get_text_width(WideString wstr)
{
    f32 textWidth = font_get_string_width(gSuiInstance.pFont, wstr);
    return textWidth;
}

void
sui_get_text_metrics_ext(wchar* buffer, i32 length, f32* width, f32* height)
{
    font_get_string_metrics_ext(gSuiInstance.pFont, buffer, length, width, height);
}

f32
sui_get_char_xoffset(wchar wc)
{
    CharChar value = hash_get(gSuiInstance.pFont->CharTable, (i32) wc);
    return value.XOffset;
}

void
sui_get_text_metrics(WideString wstr, f32* width, f32* height)
{
    font_get_string_metrics(gSuiInstance.pFont, wstr, width, height);
}

v2
sui_get_text_metrics_v2(WideString wstr)
{
    f32 width;
    f32 height;
    font_get_string_metrics(gSuiInstance.pFont, wstr, &width, &height);

    return v2_new(width, height);
}

void
sui_get_utf8_text_metrics(char* utf8Str, f32* width, f32* height)
{
    font_get_utf8_string_metrics(gSuiInstance.pFont, utf8Str, width, height);
}

const char*
sui_layout_type_to_string(SuiPanelLayoutType type)
{
    switch (type)
    {
    case SuiLayoutType_None: return "None";
    case SuiLayoutType_OccupyAvailableSpace: return "OccupyAvailableSpace";
    case SuiLayoutType_Count: return "Count";

    }

    vguard(0);
    return NULL;
}

i32
sui_layout_type_is_valid(SuiPanelLayoutType type)
{
    return type >= SuiLayoutType_None && type <= SuiLayoutType_Count;
}

f32
sui_get_full_width(SuiWidget widget)
{
    f32 fullWidth;

    switch (widget.Type)
    {

    case SuiWidgetType_Button:
    case SuiWidgetType_Text:
    case SuiWidgetType_InputString:
    case SuiWidgetType_Dropdown:
    case SuiWidgetType_Image:
    case SuiWidgetType_CollapseHeader:
        return widget.Size.X;

    case SuiWidgetType_Slider_F32:
    case SuiWidgetType_Checkbox:
        return widget.Size.X + sui_get_text_width(*widget.Label);

    default:
        vguard_not_impl();
        break;

    }

    GERROR("You probably fogot register some widget type!\n");
    vguard(0 && "Widget type not registered!");
    return 0;
}

void
sui_set_next_position(v2 position)
{
    gSuiInstance.States.NextPosition = position;
}
v2
sui_get_next_position()
{
    v2 res = gSuiInstance.States.NextPosition;
    gSuiInstance.States.NextPosition = v2_new(0, 0);
    return res;
}


/*
  #####################################
  DOCS(typedef): Sui Layout
  #####################################
*/

void
sui_widget_layout_data_reset(SuiWidgetLayoutData* pData)
{
    *pData = (SuiWidgetLayoutData) {
        .NextPosition = v3_new(15.0f, gSuiInstance.Monitor.DisplaySize.Height - 5.0f, 1.0f),
        .Type = SuiWidgetLayoutType_None
    };
}

v3
sui_layout_panel_data_get_next_position(SuiWidgetLayoutData* pData, SuiPanel* pPanel)
{
    v3 pos = v3_addz(pData->NextPosition, 1.0f);
    f32 dheight = sui_get_instance()->Monitor.DisplaySize.Height;

    if ((pData->Occupy.Y + pPanel->Size.Width) >= dheight)
    {
        //recalc everything
        f32 maxX = pData->Occupy.X;
        pData->NextPosition = v3_new(pos.X + maxX + 15.0f, dheight - 5.0f, pos.Z);
        pData->Occupy = v2_new(1.5f * pData->Occupy.X + pPanel->Size.X, pPanel->Size.Y);
    }
    else
    {
        pData->NextPosition = v3_new(pos.X, pos.Y - pPanel->Size.Height - 15.0f, pos.Z);
        pData->Occupy.X = Max(pData->Occupy.X, pPanel->Size.Width);
        pData->Occupy.Y += pPanel->Size.Height;
    }

    v3_print(pData->NextPosition);

    return pos;
}

v3
sui_layout_get_next_position(SuiLayoutData* pLayoutData)
{
    //TODO/BUG(typedef): Refactor all panel layout
    v3 pos = v3_addz(pLayoutData->NextPosition, 1.0f);
    pLayoutData->NextPosition = v3_new(pos.X, pos.Y - 170.0f, pos.Z);

    return pos;
}


void
sui_layout_reset(SuiLayoutData* pLayoutData)
{
    pLayoutData->Type = SuiLayoutType_None;
    pLayoutData->NextPosition = v3_new(15.0f, gSuiInstance.Monitor.DisplaySize.Height, 1.0f);
}

v3
sui_align_get_position(v2 fullSize, v3 widgetPosition, v2 widgetSize, SuiVerticalAlignType verticalAlign, SuiHorizontalAlignType horizontalAlign, v2 offset)
{
    v3 finalPos = v3_v2v(widgetPosition.XY, widgetPosition.Z - 0.1f);
    finalPos.X += offset.X;
    //finalPos.Y += offset.Y;

    f32 freeSpaceX = fullSize.X - widgetSize.X;
    f32 freeSpaceY = fullSize.Y - widgetSize.Y;

    switch (horizontalAlign)
    {

    case SuiHorizontalAlignType_Left:
        // DOCS(typedef): By default it already alinged by left side
        break;

    case SuiHorizontalAlignType_Center:
    {
        f32 freeSpaceXFromEachSide = freeSpaceX / 2;
        finalPos.X += freeSpaceXFromEachSide;
        break;
    }

    case SuiHorizontalAlignType_Right:
    {
        finalPos.X += freeSpaceX;
        break;
    }

    default:
        break;

    }

    switch (verticalAlign)
    {

    case SuiVerticalAlignType_Top:
        // DOCS(typedef): By default it already alinged by top side
        break;

    case SuiVerticalAlignType_Center:
    {
        f32 freeSpaceYFromEachSide = freeSpaceY * 0.5;
        finalPos.Y -= freeSpaceYFromEachSide;
        break;
    }

    case SuiVerticalAlignType_Bot:
    {
        finalPos.Y -= freeSpaceY;
        break;
    }

    default:
        vguard(0);
        break;

    }

    return finalPos;
}

v3
sui_get_text_position_inside(WideString* text, v3 widgetPosition, v2 widgetSize, v2 padding)
{
    WideString textValue = *text;
    f32 w, h;
    font_get_string_metrics(gSuiInstance.pFont, textValue, &w, &h);

    f32 firstYOffset = font_get_first_char_y_offset(gSuiInstance.pFont, textValue);

    v3 finalPos = v3_v2v(widgetPosition.XY, widgetPosition.Z - 0.1f);
    finalPos.X += padding.X;
    finalPos.Y += padding.Y + firstYOffset - ((widgetSize.Y - h) * 0.5f);

#if 0
    f32 freeSpaceX = fullSize.X - w;
    f32 freeSpaceXFromEachSide = widgetSize.X - w;
    finalPos.X += freeSpaceXFromEachSide;

    f32 freeSpaceY = ;
    f32 freeSpaceYFromEachSide = freeSpaceY * 0.5;
    finalPos.Y -= freeSpaceYFromEachSide;
#endif

    return finalPos;
}

v3
sui_align_get_text_position(WideString* text, v3 widgetPosition, v2 widgetSize, SuiVerticalAlignType verticalAlign, SuiHorizontalAlignType horizontalAlign, f32 marginX)
{
    WideString textValue = *text;
    f32 w, h;
    font_get_string_metrics(gSuiInstance.pFont, textValue, &w, &h);
    v2 textSize = sui_get_text_metrics_v2(textValue);

    f32 firstYOffset = font_get_first_char_y_offset(gSuiInstance.pFont, textValue);

    if ((textSize.X > widgetSize.X) && (horizontalAlign != SuiHorizontalAlignType_None))
    {
        GERROR("Some weird widget, with width less then text inside of it!\n");
    }

    if ((textSize.Y > widgetSize.Y) && (verticalAlign != SuiVerticalAlignType_None))
    {
        GERROR("Some weird widget, with height less then text inside of it!\n");
    }

    v3 finalPos = sui_align_get_position(widgetSize, widgetPosition,
                                         textSize,
                                         verticalAlign, horizontalAlign,
                                         v2_new(marginX, firstYOffset));

    return finalPos;
}

/*
  NOTE(typedef): el could be not only widget but a panel itself
*/
v3
sui_panel_apply_scroll_offset(v3 pos, v2 offset)
{
    return v3_v2v(v2_add(pos.XY, offset), pos.Z);
}

void
sui_panel_reset_layout(SuiPanel* pSuiPanel)
{
    SuiStyle* pStyle = GetStyle();

    // vertical layout stack
    SuiStyleItem item =
        pStyle->Items[SuiStyleItemType_LayoutType_VerticalStack];

    //NOTE: HERE
    v3 pos = pSuiPanel->Position;

}


/*
  #####################################
  DOCS(typedef): Sui Style
  #####################################
*/

void
sui_style_init(SuiStyle* pStyle, Font* pFontInfo)
{
#define PanelSize v2_new(30, 30)

    v4 grayColor = v4_new(0.11, 0.11, 0.11, 1);
    v4 darkerGrayColor = v4_new(0.09, 0.09, 0.09, 1);
    v4 ultraDarkColor = v4_new(0.01f, 0.01f, 0.01f, 1);
    v4 assDarkColor = v4_new(0.001f, 0.001f, 0.001f, 1);

    SuiStyleItem panel = {
        .Color = {
            .Background = ultraDarkColor,
            .Hovered = ultraDarkColor,
            .Clicked = ultraDarkColor
        },
        .Size = {
            .Min = PanelSize,
            .Max = v2_new(650.0f, 450.0f),
            .Hovered = PanelSize,
            .Clicked = PanelSize
        },
        .Offset = {
            .Margin = v2_new(5.0f, 15.0f)
        }
    };
    pStyle->Items[SuiStyleItemType_Panel] = panel;

    //GERROR("Font.Size: %d\n", pFontInfo->MaxHeight);
    v2 panelHdrPadding = v2_new(0.015, 5);
    SuiStyleItem panelHeader = {
        .Color = {
            .Background = v4_new(0.02, 0.03, 0.02, 1),
            .Hovered = v4_new(0.04, 0.06, 0.04, 1),
            .Clicked = v4_new(0.06, 0.09, 0.06, 1)
        },
        .Size = {
            .Min = v2_new(1.0f, pFontInfo->MaxHeight + panelHdrPadding.Y),
            .Hovered = v2_new(1.0f, 0.1f),
            .Clicked = v2_new(1.0f, 0.1f)
        },
        .Offset = {
            .Margin = v2_new(5, 0.1f),
        }
    };
    pStyle->Items[SuiStyleItemType_Panel_Header] = panelHeader;

    SuiStyleItem panelHdrTxt = {
        .Color = {
            .Background = v4_new(1, 1, 1, 0)
        },
        .Offset = { .Margin = v2_new(5.0f, 0.0f) },
    };
    pStyle->Items[SuiStyleItemType_Panel_Header_Text] = panelHdrTxt;

    SuiStyleItem panelHeaderClsBtn = {
        .Color = {
            .Background = v4_new(0.70f, 0.20f, 0.20f, 1.0f),
            .Hovered = v4_new(0.70f, 0.20f, 0.20f, 1.0f),
            .Clicked = v4_new(0.09, 0.06, 0.06, 1.0f)
        },
        .Size = {
            .Min = v2_new(0.1f, 0.7f),
        },
        .Offset = {
            .Margin = v2_new(5.0f, panelHdrPadding.Y),
            .VerticalAlign = SuiVerticalAlignType_Center,
            .HorizontalAlign = SuiHorizontalAlignType_Right
        }
    };
    pStyle->Items[SuiStyleItemType_Panel_Header_Close_Button] = panelHeaderClsBtn;

    SuiStyleItem panelHeaderClsBtnX = {
        .Color = {
            .Background = v4_new(0, 0, 0, 1),
            .Hovered = v4_new(1, 1, 1, 1),
            .Clicked = v4_new(1, 1, 1, 1)
        },
        .Size = {
            .Min = v2_new(0.6f, 0.6f),
        },
        .Offset = {
            .Margin = v2_new(5.0f, 0.07f),
            .VerticalAlign = SuiVerticalAlignType_Center
        }
    };
    pStyle->Items[SuiStyleItemType_Panel_Header_Close_Button_X] = panelHeaderClsBtnX;

    SuiStyleItem panelBorder = {
        .Color = {
            .Background = v4_new(0.06, 0.09, 0.06, 1),
            .Clicked = v4_new(0.36, 0.48, 0.36, 1)
        },
        .Size = {
            .Min = v2_new(0.5f, 0.5f),
        }
    };
    pStyle->Items[SuiStyleItemType_Panel_Border] = panelBorder;

    SuiStyleItem panelScrollY = {
        .Color = {
            .Background = v4_mulv(assDarkColor, 5),
            .Hovered = v4_mulv(assDarkColor, 7),
            .Clicked = v4_mulv(assDarkColor, 7)
        },
        .Size = {
            // DOCS(): How many percent of PanelSize it occupies
            .Min = v2_new(30.0f, 1.0f),
        },
        .Offset = {
            .Margin = v2_new(1.5f, 1.0f)
        }
    };
    pStyle->Items[SuiStyleItemType_Panel_Scroll_Y] = panelScrollY;

    SuiStyleItem panelScrollButtonY = {
        .Color = {
            .Background = v4_new(0.02, 0.03, 0.02, 1),
            .Hovered = v4_new(0.04, 0.06, 0.04, 1),
            .Clicked = v4_new(0.06, 0.09, 0.06, 1)
        },
        .Size = {
            .Min = v2_new(0.65f, 25.0f),
        },
        .Offset = {
            .Margin = v2_new(0.0f, 5.0f)
        }
    };
    pStyle->Items[SuiStyleItemType_Panel_Scroll_Button_Y] = panelScrollButtonY;

    SuiStyleItem panelScrollX = {
        .Color = {
            .Background = v4_mulv(assDarkColor, 5),
            .Hovered = v4_mulv(assDarkColor, 7),
            .Clicked = v4_mulv(assDarkColor, 7)
        },
        .Size = {
            // DOCS(): How many percent of PanelSize it occupies
            .Min = v2_new(1.0f, 30.0f),
        },
        .Offset = {
            .Margin = v2_new(1.5f, 1.0f)
        }
    };
    pStyle->Items[SuiStyleItemType_Panel_Scroll_X] = panelScrollX;

    SuiStyleItem panelScrollButtonX = {
        .Color = {
            .Background = v4_new(0.02, 0.03, 0.02, 1),
            .Hovered = v4_new(0.04, 0.06, 0.04, 1),
            .Clicked = v4_new(0.06, 0.09, 0.06, 1)
        },
        .Size = {
            .Min = v2_new(25.0f, 0.65f),
        },
        .Offset = {
            .Margin = v2_new(5.0f, 0.0f)
        }
    };
    pStyle->Items[SuiStyleItemType_Panel_Scroll_Button_X] = panelScrollButtonX;

    /*
      DOCS(typedef): Sui Widgets
    */
    SuiStyleItem btnWidget = {
        .Color = {
            .Background = v4_new(0.1, 0.153, 0.134, 1),
            .Hovered = v4_new(0.1, 0.203, 0.134, 1),
            .Clicked = v4_new(0.1, 0.253, 0.134, 1)
        },
        .Size = {
            .Min = v2_new(0.5f, 0.5f),
        },
        .Offset = {
            .VerticalAlign = SuiVerticalAlignType_Top,
            .Margin = v2_new(0.0f, 0.0f)
        }
    };
    pStyle->Items[SuiStyleItemType_Widget_Button] = btnWidget;

    SuiStyleItem sliderF32Widget = {
        .Color = {
            .Background = v4_new(0.1, 0.153, 0.134, 1),
            .Hovered = v4_new(0.1, 0.203, 0.134, 1),
            .Clicked = v4_new(0.1, 0.253, 0.134, 1)
        },
        .Size = {
            .Min = v2_new(150, 35),
        },
        .Offset = {
            .HorizontalAlign = SuiHorizontalAlignType_Center
        }
    };
    pStyle->Items[SuiStyleItemType_Widget_Slider_F32] = sliderF32Widget;

    SuiStyleItem sliderF32Slider = {
        .Color = {
            .Background = v4_new(0.02, 0.03, 0.02, 1),
            .Hovered = v4_new(0.04, 0.06, 0.04, 1),
            .Clicked = v4_new(0.06, 0.09, 0.06, 1)
        },
        .Size = {
            .Min = v2_new(0.1f, 1.0f),
        }
    };
    pStyle->Items[SuiStyleItemType_Widget_Slider_F32_Slider] = sliderF32Slider;

    f32 checkboxRatio = pFontInfo->MaxHeight;
    v2 checkboxRectSize = v2_new(checkboxRatio, checkboxRatio);
    SuiStyleItem checkboxItem = {
        .Color = {
            .Background = v4_new(0.02, 0.03, 0.02, 1),
            .Hovered = v4_new(0.04, 0.06, 0.04, 1),
            .Clicked = v4_new(0.1, 0.253, 0.134, 1)
        },
        .Size = {
            .Min = checkboxRectSize,
        }
    };
    pStyle->Items[SuiStyleItemType_Widget_Checkbox] = checkboxItem;

    v2 inputSize = v2_new(10 * pFontInfo->MaxHeight, 1.125f * pFontInfo->MaxHeight);
    SuiStyleItem inputItem = {
        .Color = {
            .Background = v4_new(0.02, 0.03, 0.02, 1),
            .Hovered = v4_new(0.04, 0.06, 0.04, 1),
            .Clicked = v4_new(0.1, 0.253, 0.134, 1)
        },
        .Size = {
            .Min = inputSize,
        }
    };
    pStyle->Items[SuiStyleItemType_Widget_Input_String] = inputItem;

    v2 dropdownSize = v2_new(10 * pFontInfo->MaxHeight, 1.125f * pFontInfo->MaxHeight);
    SuiStyleItem dropdown = {
        .Color = {
            .Background = v4_new(0.02, 0.03, 0.02, 1),
            .Hovered = v4_new(0.04, 0.06, 0.04, 1),
            .Clicked = v4_new(0.1, 0.253, 0.134, 1)
        },
        .Size = {
            .Min = inputSize,
        }
    };
    pStyle->Items[SuiStyleItemType_Widget_Dropdown] = dropdown;

    /* SuiStyleItem dropdownItem = { */
    /*	.Color = { */
    /*	    .Background = v4_new(0.02, 0.03, 0.02, 1), */
    /*	    .Hovered = v4_new(0.04, 0.06, 0.04, 1), */
    /*	    .Clicked = v4_new(0.1, 0.253, 0.134, 1) */
    /*	}, */
    /*	.Size = { */
    /*	    .Min = inputSize, */
    /*	} */
    /* }; */
    /* pStyle->Items[SuiStyleItemType_Widget_Dropdown] = dropdownItem; */

    SuiStyleItem collapseWidget = {
        .Color = {
            .Background = v4_new(0.1, 0.153, 0.134, 1),
            .Hovered = v4_new(0.1, 0.203, 0.134, 1),
            .Clicked = v4_new(0.1, 0.253, 0.134, 1)
        },
        .Size = {
            .Min = v2_new(0.5f, 0.5f),
        },
        .Offset = {
            .VerticalAlign = SuiVerticalAlignType_Top,
            .Margin = v2_new(0.0f, 0.0f)
        }
    };
    pStyle->Items[SuiStyleItemType_Widget_CollapseHeader] = collapseWidget;

    SuiStyleItem collapseTxtWidget = {
        .Color = {
            .Background = v4_new(1, 1, 1, 0),
        },
        .Offset = {
            .Margin = v2_new(5.0f, 0.0f)
        }
    };
    pStyle->Items[SuiStyleItemType_Widget_CollapseHeader_Text] = collapseTxtWidget;

    SuiStyleItem layoutTypeVerticalStack = {
        .Offset = {
            .Margin = v2_new(15, 15)
        }
    };
    pStyle->Items[SuiStyleItemType_LayoutType_VerticalStack] = layoutTypeVerticalStack;

}

v4
sui_style_get_color_by_state(SuiStyle* style, SuiStyleItemType itemType, SuiState state)
{
    SuiStyleColorItem colorItem = style->Items[itemType].Color;

    switch (state)
    {
    case SuiState_None   : return colorItem.Background;
    case SuiState_Hovered: return colorItem.Hovered;
    case SuiState_Clicked: return colorItem.Clicked;
    case SuiState_Grabbed: return colorItem.Clicked;
    case SuiState_Changed: return colorItem.Clicked;
    }

    vguard(0);
    return v4_new(0,0,0,0);
}

SuiStyleSizeItem
sui_style_get_size(SuiStyle* style, SuiStyleItemType itemType)
{
    SuiStyleSizeItem sizeItem = style->Items[itemType].Size;
    return sizeItem;
}

SuiStyleOffsetItem
sui_style_get_offset(SuiStyle* style, SuiStyleItemType itemType)
{
    SuiStyleOffsetItem sizeItem = style->Items[itemType].Offset;
    return sizeItem;
}

void
sui_input_init(SuiInputData* pSuiInputData)
{
    pSuiInputData->MousePositionChanged = 0;
    pSuiInputData->LeftKeyWasPressed = 0;
    pSuiInputData->IsInputAlreadyHandled = 0;
    pSuiInputData->Mode = SuiModeType_Insert;
    pSuiInputData->CharTyped = 0;

    pSuiInputData->ScrollY = 0;

    memset(pSuiInputData->KeyData, 0, SuiKeys_Count * sizeof(SuiKeyData));
}

/*
  #############################
  DOCS(typedef): Infrastructure
  #############################
*/
void
sui_init(SuiInstanceCreateInfo createInfo)
{
    vguard_not_null(createInfo.pFont);

    gSuiInstance.IsInitialized = 1;
    gSuiInstance.pFont = createInfo.pFont;
    gSuiInstance.Monitor.DisplaySize = createInfo.DisplaySize;

    { // NOTE(typedef): Init panels and widgets, frame cache
        gSuiInstance.PrevFrameCache = (SuiFrameCache) {
            .FocusPanelInd = -1,
            .ActivePanelId = -1,
        };
    }

    { // NOTE(typedef): Init layout
        // NOTE: Set sui panel layout
        sui_widget_layout_data_reset(&gSuiInstance.WidgetLayoutData);
    }

    sui_style_init(&gSuiInstance.Style, gSuiInstance.pFont);
    sui_input_init(GetInputData());

    gSuiInstance.Arena = simple_arena_create(KB(100));

    //NOTE BUG NOT SURE WE NEED THIS
    gSuiInstance.IPool = (IWideStringPool) {
        .Pool = NULL,
    };

}

i32
sui_is_initialized()
{
    return gSuiInstance.IsInitialized;
}

void
sui_destroy()
{
    simple_arena_destroy(gSuiInstance.Arena);
}

i32
sui_frame_begin(SuiFrameInfo suiFrameInfo)
{
    gSuiInstance.FrameInfo = suiFrameInfo;
    gSuiInstance.PrevFrameCache.WidgetsCount = 0;

    SuiDrawData* drawData = GetDrawData();
    drawData->HotID = 0;
    drawData->HotPanelId = 0;

    i64 panelsCnt = array_count(gSuiInstance.PrevFrameCache.Panels);

    i32 isLayoutingNeeded = 0;
    if (isLayoutingNeeded)
    {
        // BUG:
        static i32 doOnes = 1;
        static SimpleTimerData timer = {
            .WaitSeconds = .3f
        };

        i32 doIt = 0;
        if (doOnes && simple_timer_interval(&timer, gSuiInstance.FrameInfo.Timestep))
        {
            GERROR("It's done!\n");
            doIt = 1;
        }

        if (doIt)
        {
            doOnes = 0;
            doIt = 0;

            sui_widget_layout_data_reset(&gSuiInstance.WidgetLayoutData);

            for (i32 p = 0; p < panelsCnt; ++p)
            {
                SuiPanel* pPanel = sui_panel_get_by_i(p);

                pPanel->Position = sui_layout_panel_data_get_next_position(&gSuiInstance.WidgetLayoutData, pPanel);
                //pPanel->LayoutData = sui_get_layout_data(pPanel->Position);
            }

        }
    }

    for (i32 p = 0; p < panelsCnt; ++p)
    {
        SuiPanel* pPanel = sui_panel_get_by_i(p);
        sui_panel_reset_layout(pPanel);
    }

    simple_arena_clear(gSuiInstance.Arena);
    memory_set_arena(gSuiInstance.Arena);
    memory_unbind_current_arena();

    return 1;
}

//todo: apply this action on panel_clicked or panel_tabed event
void
sui_panel_active_behaviour()
{
    // DOCS(typedef): Active panel behaviour (z-sorting)
    // bug: because activepanelid is -1 most of the time
    if (gSuiInstance.PrevFrameCache.ActivePanelId == -1)
    {
        GERROR("Panel not active");
        return;
    }

    i32 panelsCount = array_count(gSuiInstance.PrevFrameCache.Panels);

    const f32 start_z_order_for_panels = 2.0f;
    f32 zorder = start_z_order_for_panels;

    for (i32 i = 0; i < panelsCount; ++i)
    {
        SuiPanel* pSuiPanel = sui_panel_get_by_i(i);

        if (sui_panel_is_active(pSuiPanel))
        {
            pSuiPanel->Position.Z = 1.0f;
        }
        else
        {
            pSuiPanel->Position.Z = zorder;
            ++zorder;
        }

        //GINFO("[%d] - PanelZ(%lld): %f\n", i, pSuiPanel->Id, pSuiPanel->Position.Z);
    }

    qsort(gSuiInstance.PrevFrameCache.Panels, panelsCount, sizeof(i32), _sui_panel_compare);
}

void
sui_frame_end()
{
    // DOCS(typedef): Clean Draw data active & hot ids
    SuiDrawData* drawData = GetDrawData();
    if (IsKeyReleased(SuiKeys_Mouse_Left_Button))
    {
        drawData->ActiveID = 0;
        //NOTE(typedef): We can ignore this assign but i want this to be 0
        drawData->HotID = 0;

        drawData->ActivePanelId = 0;
        drawData->HotPanelId = 0;
    }

    /*
      DOCS(typedef): Update panels cache
    */
    //GINFO("Panels->Count: %d\n", array_count(gSuiInstance.PrevFrameCache.Panels));
    //array_clear(gSuiInstance.PrevFrameCache.Panels);

    {
        // DOCS(typedef): Process global input
        if (IsKeyPressed(SuiKeys_Tab))
        {
            SuiPanel* pPanel = sui_panel_get_next_tab();
            gSuiInstance.PrevFrameCache.ActivePanelId = pPanel->Id;
            sui_panel_active_behaviour();
        }


        // DOCS: Clean panel data
        for (i32 i = 0; i < array_count(gSuiInstance.PrevFrameCache.Panels); ++i)
        {
            SuiPanel* pSuiPanel = sui_panel_get_by_i(i);
            pSuiPanel->IsNotFirstCall = 0;
            //pSuiPanel->IsLayoutChanged = 0;

            if (pSuiPanel->IsLayoutTreeNeedUpdate)
            {
                pSuiPanel->IsLayoutTreeNeedUpdate = 0;
            }

            for (i32 w = 0; w < array_count(pSuiPanel->aWidgets); ++w)
            {
                SuiWidget widget = pSuiPanel->aWidgets[w];
                if (!widget.StillExist)
                {
                    array_remove_at(pSuiPanel->aWidgets, w);
                    --w;

                    pSuiPanel->IsWidgetsModified = 1;
                }
            }

            array_foreach_ptr(pSuiPanel->aWidgets, item->StillExist = 0);

            // note: too complicated
            if (pSuiPanel->IsWidgetsModified)
            {
                // todo: update layout tree
                pSuiPanel->IsWidgetsModified = 0;
                pSuiPanel->IsLayoutTreeNeedUpdate = 1;

                // sui_panel_process_layout(pSuiPanel);
            }

            array_clear(pSuiPanel->LayoutStack);
        }

    }

    // DOCS(typedef): Clean Input Data
    SuiInputData* pSuiInputData = GetInputData();
    sui_input_init(pSuiInputData);

    memory_set_arena(NULL);
}

// todo: place into sui_frame_end()
void
sui_auto_layout()
{

    /*
      [INFO] P[None:0] C[VerticalStack:742C228] WId[-1]
      [INFO] P[VerticalStack:742C228] C[W:10D22A10] WId[a]
      [INFO] P[VerticalStack:742C228] C[W:10D22A30] WId[b]
      [INFO] P[VerticalStack:742C228] C[VerticalStack:10D22A50] WId[-1]
      [INFO] P[VerticalStack:742C228] C[HorizontalStack:10D22A70] WId[-1]
      [INFO] P[VerticalStack:742C228] C[W:10D22A90] WId[i]
      [INFO] P[VerticalStack:742C228] C[W:10D22AB0] WId[j]
      [INFO] P[None:46F5BE0] C[W:10D22B70] WId[c]
      [INFO] P[None:46F5BE0] C[W:10D22B90] WId[d]
      [INFO] P[None:46F5BE0] C[W:10D22BB0] WId[e]
      [INFO] P[HorizontalStack:10D22390] C[VerticalStack:10D22BD0] WId[-1]
      [INFO] P[HorizontalStack:10D22390] C[W:10D22BF0] WId[h]
      [INFO] P[None:4794C80] C[W:47943C0] WId[f]
    */

    i64 panelsCnt = array_count(gSuiInstance.PrevFrameCache.Panels);
    for (i32 p = 0; p < panelsCnt; ++p)
    {
        SuiPanel* pSuiPanel = sui_panel_get_by_i(p);
        //vguard(IsLayoutSet(pSuiPanel->LayoutData.Type) && "Layout not set");

        SuiStyle* pSuiStyle = GetStyle();
        SuiStyleItem panelHdrStyle =
            pSuiStyle->Items[SuiStyleItemType_Panel_Header];
        SuiStyleItem panelStyle =
            pSuiStyle->Items[SuiStyleItemType_Panel];
        SuiStyleItem verticalStackItem =
            pSuiStyle->Items[SuiStyleItemType_LayoutType_VerticalStack];

        // DOCS(typedef): Recalc panel size, it should always fit widgets (or it shouldn't maybe make this flag dependent ~~~)
        sui_panel_calc_size(pSuiPanel);

        // DOCS(typedef): Recalc
        if (pSuiPanel->Flags & SuiPanelFlags_LayoutExist || pSuiPanel->IsLayoutChanged)
        {
            pSuiPanel->IsLayoutChanged = 0;


#if 0
            struct SuiLayoutNode* children = _sui_tree_get_children(&pSuiPanel->Root);

            for (i32 i = 0; i < array_count(children); ++i)
            {
                struct SuiLayoutNode child = children[i];
                // to do smth ...

            }
#endif

        }


        //v2_print(pSuiPanel->NeededSize);

    }

}

i32
sui_is_item_hovered(v3 pos, v2 size)
{
    v2 mousePos = GetInputData()->MousePosition;
    if (mousePos.X >= pos.X
        && mousePos.X <= (pos.X + size.Width)
        && mousePos.Y <= pos.Y
        && mousePos.Y >= (pos.Y - size.Height))
    {
        return 1;
    }

    return 0;
}

static SuiState
sui_is_item_pressed(v3 position, v2 size, SuiWidgetId id)
{
    SuiDrawData* pDrawData = GetDrawData();
    if (pDrawData->ActiveID == 0
        && pDrawData->HotID == 0
        && sui_is_item_hovered(position, size))
    {
        //GERROR("HotId: %lld\n", pDrawData->HotID);
        pDrawData->HotID = id;

        if (IsKeyPressed(SuiKeys_Mouse_Left_Button))
        {
            pDrawData->ActiveID = id;
            pDrawData->LeftKeyWasPressed = 1;

            return SuiState_Clicked;
        }

        return SuiState_Hovered;
    }

    return SuiState_None;
}

/*
  ##############################
  DOCS(typedef): Sui Widgets
  ##############################
*/

WideString*
_sui_convert_utf8_to_wide_string(char* pLabel)
{
    SuiInstance* pSuiInstance = sui_get_instance();
    vassert(pSuiInstance->IsInitialized);
    WideString* pWideLabel = iwide_string_new_utf8(&pSuiInstance->IPool, pLabel);
    return pWideLabel;
}

SuiPanel*
_sui_panel_create(WideString* pLabel, SuiPanelFlags flags)
{
    // DOCS(typedef): Register new Panel
    //GINFO("Register new panel!\n");
    //vguard(table_count(gSuiInstance.PrevFrameCache.PanelsTable) < 4);

    WideString label = *pLabel;

    SuiPanel newPanel = {
        .PanelState = SuiPanelState_Opened,
        .Label = pLabel,
        .Flags = flags,
        .aNodes = NULL
    };

    /// note: this part is bad one
    // sui_panel_calc_size(&newPanel);

    static f32 zorder = 2.0f;
    newPanel.Position = v3_new(0, sui_get_instance()->Monitor.DisplaySize.Height, zorder);
    ++zorder;
    /// note: this part is bad one

    whash_put(gSuiInstance.PrevFrameCache.PanelsTable, label, newPanel);
    SuiPanelId id = (SuiPanelId) table_header(gSuiInstance.PrevFrameCache.PanelsTable)->Index;
    vguard(id != -1);

    i32 anyInd = whash_geti(gSuiInstance.PrevFrameCache.PanelsTable, label);
    if (id != anyInd)
    {
        GERROR("Indices not equal!\n");
        vguard(id == anyInd && "Indices not equal!");
    }

    SuiPanel* pPanel = &gSuiInstance.PrevFrameCache.PanelsTable[id].Value;
    pPanel->Id = id;

    i64 ind = table_index(gSuiInstance.PrevFrameCache.PanelsTable);
    sui_set_current_panel(ind);

    // DOCS(typedef): Save Panel For Future Use / Pushed panel
    i32 notExist;
    notExist = (array_index_of(gSuiInstance.PrevFrameCache.Panels, item == ind) == -1);
    vguard(notExist && "Weird behavior with existing rec:.Panels and !existing rec:.PanelsTable !!!");

    i32 cnt = array_count(gSuiInstance.PrevFrameCache.Panels);
    array_push(gSuiInstance.PrevFrameCache.Panels, ind);

    // DOCS: Set focus
    gSuiInstance.PrevFrameCache.FocusPanelInd = cnt;

    return pPanel;
}

void
_sui_panel_begin_add_to_existing_behaviour(SuiPanel* pCurrentPanel)
{
    /*
      DOCS(typedef): Still not sure we need this double
      panel_begin() call feature for appending new widgets
      to already existing panel
    */

    if (!pCurrentPanel->IsNotFirstCall)
    {
        pCurrentPanel->IsNotFirstCall = 1;
        return;
    }

    // DOCS: disable active hot beh for panels hovered by new widgets
    GetInputData()->IsInputAlreadyHandled = 0;
    SuiDrawData* pDrawData = GetDrawData();

    i32 shouldReset = 0;
    for (i32 p = 0; p < array_count(gSuiInstance.PrevFrameCache.Panels); ++p)
    {
        SuiPanel* pPanel = sui_panel_get_by_i(p);
        if (pDrawData->ActiveID == pPanel->Id || pDrawData->HotID == pPanel->Id)
        {
            shouldReset = 1;
            break;
        }
    }

    if (shouldReset)
    {
        pDrawData->ActiveID = 0;
        pDrawData->HotID = 0;
    }

}

i32
sui_panel_begin_l(WideString* pLabel, SuiPanelFlags flags)
{
    DefaultGuard(pLabel);

    SuiPanel* pCurrentPanel = NULL;

    // DOCS(typedef): Check panel registration in cache
    WideString label = *pLabel;
    i32 ind = whash_geti(gSuiInstance.PrevFrameCache.PanelsTable, label);

    if (ind != -1)
    {
        sui_set_current_panel(ind);

        // DOCS(typedef): Get panel from prev cached data
        pCurrentPanel = sui_get_current_panel();

        // DOCS: If panel been closed by button exit here
        if (pCurrentPanel->PanelState == SuiPanelState_Closed)
            return 0;
    }
    else
    {
        pCurrentPanel = _sui_panel_create(pLabel, flags);
    }

    // DOCS: Layout logic
    if (pCurrentPanel->Flags & SuiPanelFlags_LayoutExist)
        sui_panel_push_layout(SuiWidgetLayoutType_VerticalStack);

    _sui_panel_begin_add_to_existing_behaviour(pCurrentPanel);

    sui_is_panel_blocked_by_other_panels(pCurrentPanel);

    return 1;
}

i32
sui_panel_begin(char* label, SuiPanelFlags flags)
{
    // Arena clear cause the bug
    WideString* gWideLabel = _sui_convert_utf8_to_wide_string(label);
    i32 state = sui_panel_begin_l(gWideLabel, flags);
    return state;
}

void
_panel_scroll_y_behaviour(SuiPanel* pCurrentPanel)
{
    v2 scrollBtnSize = sui_panel_get_y_scroll_button_size(pCurrentPanel);
    v3 scrollBtnPos = sui_panel_get_y_scroll_button_position(pCurrentPanel);

    // NOTE: For debugging
    SuiInputData* pInput = sui_get_input_data();
    _sui_click_and_grab_state_machine(pInput,
                                      pCurrentPanel->Id,
                                      &pCurrentPanel->ScrollYState,
                                      scrollBtnPos,
                                      scrollBtnSize);

    if (pCurrentPanel->ScrollYState != SuiState_None)
    {
        pCurrentPanel->FullScrollYState = pCurrentPanel->ScrollYState;
    }
    else
    {
        v3 p = sui_panel_get_y_scroll_position(pCurrentPanel);
        v2 s = sui_panel_get_y_scroll_size(pCurrentPanel);

        _sui_click_and_grab_state_machine(pInput,
                                          pCurrentPanel->Id,
                                          &pCurrentPanel->FullScrollYState,
                                          p, s);
    }

    SuiDrawData* pDrawData = GetDrawData();

    if (pCurrentPanel->ScrollYState == SuiState_Grabbed)
    {
        SuiStyleItem scrollBtnStyle =
            GetStyle()->Items[SuiStyleItemType_Panel_Scroll_Button_Y];

        v2 sbs = sui_panel_get_y_scroll_button_size(pCurrentPanel);
        v2 scrollSize = sui_panel_get_y_scroll_size(pCurrentPanel);
        v3 scrollPos = sui_panel_get_y_scroll_position(pCurrentPanel);
        f32 marginY = scrollBtnStyle.Offset.Margin.Y;
        v2 dmp = v2_sub(pInput->PrevMousePosition, pInput->MousePosition);

        f32 newValue = pCurrentPanel->ScrollOffset.Y + dmp.Y;
        // NOTE: sbpe - scroll btn position end
        // NOTE: maximum offset scroll
        f32 mos = scrollSize.Y - 2*marginY - sbs.Y;
        pCurrentPanel->ScrollOffset.Y = MinMax(newValue, 0, mos);

        // NOTE: rm - ratio multipier, multiply this on pCurrentPanel->ScrollOffset.Y
        f32 rm = (pCurrentPanel->NeededSize.Height - pCurrentPanel->Size.Height) / mos;
        //GINFO("RM: %f %f %f\n", rm, mos, marginY);
        pCurrentPanel->ScrollContentOffset.Y = rm * pCurrentPanel->ScrollOffset.Y;

    }
}

void
_panel_scroll_x_behaviour(SuiPanel* pCurrentPanel)
{
    v2 scrollBtnSize = sui_panel_get_x_scroll_button_size(pCurrentPanel);
    v3 scrollBtnPos = sui_panel_get_x_scroll_button_position(pCurrentPanel);

    // NOTE: For debugging
    SuiInputData* pInput = sui_get_input_data();
    _sui_click_and_grab_state_machine(pInput,
                                      pCurrentPanel->Id,
                                      &pCurrentPanel->ScrollXState,
                                      scrollBtnPos,
                                      scrollBtnSize);

    SuiDrawData* pDrawData = GetDrawData();

    if (pCurrentPanel->ScrollXState == SuiState_Grabbed)
    {
        SuiStyleItem scrollBtnStyle =
            GetStyle()->Items[SuiStyleItemType_Panel_Scroll_Button_X];

        v2 sbs = sui_panel_get_x_scroll_button_size(pCurrentPanel);
        v2 scrollSize = sui_panel_get_x_scroll_size(pCurrentPanel);
        v3 scrollPos = sui_panel_get_x_scroll_position(pCurrentPanel);
        f32 marginX = scrollBtnStyle.Offset.Margin.X;
        v2 dmp = v2_sub(pInput->PrevMousePosition, pInput->MousePosition);

        f32 newValue = pCurrentPanel->ScrollOffset.X + dmp.X;
        f32 mos = scrollSize.X - 2*marginX - sbs.X;
        pCurrentPanel->ScrollOffset.X = MinMax(newValue, 0, mos);

        f32 rm = (pCurrentPanel->NeededSize.Width - pCurrentPanel->Size.Width) / mos;
        pCurrentPanel->ScrollContentOffset.X = rm * pCurrentPanel->ScrollOffset.X;

    }
}

void
sui_panel_end()
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();
    vguard_not_null(pCurrentPanel);

    if (pCurrentPanel == NULL || pCurrentPanel->PanelState == SuiPanelState_Closed)
    {
        gSuiInstance.PrevFrameCache.CurrentPanelInd = -1;
        //GINFO("Skip panel!\n");
        return;
    }

    sui_panel_auto_layout_widget(pCurrentPanel);
    sui_panel_calc_size(pCurrentPanel);

    if (pCurrentPanel->IsBlockedByOther)
    {
        //GINFO("Blocked by others!\n");
        return;
    }

    // DOCS(typedef): If widget is (hovered || clicked) ignore panel (hovered || clicked) mech
    for (i32 i = 0; i < array_count(pCurrentPanel->aWidgets); ++i)
    {
        SuiWidget suiWidget = pCurrentPanel->aWidgets[i];
        if (suiWidget.State == SuiState_Hovered
            || suiWidget.State == SuiState_Clicked)
        {
            //GWARNING("Ignore!\n");
            return;
        }
    }

    SuiInputData* pInput = GetInputData();

    // DOCS: Header logic
    v2 hdrSize = sui_panel_get_header_size(pCurrentPanel);
    if ((pCurrentPanel->Flags & SuiPanelFlags_WithOutHeader) == 0)
    {
        // DOCS(typedef): Close btn mech
        sui_state_print(pCurrentPanel->HeaderState);

        // DOCS(typedef): Header close button
        v2 closeBtnSize = sui_panel_get_close_button_size(hdrSize);
        v3 closeBtnPos =
            sui_panel_get_close_button_position(pCurrentPanel, closeBtnSize);
        pCurrentPanel->CloseButtonState = sui_is_item_pressed(closeBtnPos, closeBtnSize, pCurrentPanel->Id);
        if (pCurrentPanel->CloseButtonState == SuiState_Clicked)
        {
            pCurrentPanel->PanelState = SuiPanelState_Closed;
            return;

            //array_remove(gSuiInstance.PrevFrameCache.Panels, &gSuiInstance.PrevFrameCache.CurrentPanelInd);
        }

        // DOCS: Header logic && Make active by click on hdr
        v2 hdrSize = sui_panel_get_header_size(pCurrentPanel);
        _sui_click_and_grab_state_machine(
            pInput,
            pCurrentPanel->Id,
            &pCurrentPanel->HeaderState,
            pCurrentPanel->Position, hdrSize);

        // NOTE: We need this because ActiveID && HotID is set by cls btn
        if (pCurrentPanel->CloseButtonState == SuiState_Hovered)
        {
            pCurrentPanel->HeaderState = SuiState_Hovered;
        }

        sui_panel_item_make_active_by_state(pCurrentPanel->HeaderState, pCurrentPanel->Id);
    }

    // DOCS: Scroll stuff
    if (pCurrentPanel->Flags & SuiPanelFlags_Scrollable_Y)
    {
        _panel_scroll_y_behaviour(pCurrentPanel);
    }
    if (pCurrentPanel->Flags & SuiPanelFlags_Scrollable_X)
    {
        _panel_scroll_x_behaviour(pCurrentPanel);
    }

    //DOCS() Panel Mech Grab Logic
    if (pCurrentPanel->ScrollYState == SuiState_None && pCurrentPanel->HeaderState == SuiState_None)
    {
        _sui_click_and_grab_state_machine(
            pInput,
            pCurrentPanel->Id,
            &pCurrentPanel->State,
            pCurrentPanel->Position, pCurrentPanel->Size);

        sui_panel_item_make_active_by_state(pCurrentPanel->State, pCurrentPanel->Id);
    }
    else
    {
        //GERROR("Hdr state: %s\n", sui_state_to_string(pCurrentPanel->HeaderState));
        pCurrentPanel->State = pCurrentPanel->HeaderState;
    }

    // DOCS(typedef): Panel movable logic (Grab is Available for Header/Panel itself)
    // todo: place this into function, to operate on callback, mb
    // maybe we need to readraw a ui frame on input event if neccessart
    if (pInput->MousePositionChanged && ((pCurrentPanel->Flags & SuiPanelFlags_Movable) != 0))
    {
        i32 isHeaderGrabbed = (pCurrentPanel->HeaderState == SuiState_Grabbed);
        i32 isHeaderDisabledAndPanelGrabbed = ((pCurrentPanel->Flags & SuiPanelFlags_WithOutHeader) && pCurrentPanel->State == SuiState_Grabbed);
        if (isHeaderGrabbed || isHeaderDisabledAndPanelGrabbed)
        {
            v2 mp = pInput->MousePosition;
            v2 gp = pInput->GrabPosition;
            v2 gmp = pInput->GrabMousePosition;
            v2 delta = v2_sub(mp, gmp);
            pCurrentPanel->Position = v3_v2v(v2_add(gp, delta), pCurrentPanel->Position.Z);

            sui_panel_reset_layout(pCurrentPanel);

            sui_panel_auto_layout_widget(pCurrentPanel);

            //SuiStyle* pStyle = GetStyle();
            //v2 padding = sui_style_get_offset(pStyle, SuiStyleItemType_Panel).Padding;
        }
    }

    // DOCS: For Panel's layering
    if (pCurrentPanel->State == SuiState_Clicked
        || pCurrentPanel->State == SuiState_Hovered
        || pCurrentPanel->HeaderState == SuiState_Clicked
        || pCurrentPanel->HeaderState == SuiState_Hovered)
    {
        //HandleKey(key)
        GetInputData()->IsInputAlreadyHandled = 1;
        //GINFO("Input is already handled [%d %d]!\n", aTm->tm_min, aTm->tm_sec);
    }

    gSuiInstance.PrevFrameCache.CurrentPanelInd = -1;
}

void
sui_panel_auto_layout_widget(SuiPanel* pPanel)
{
    static WideString ws = {};
    if (ws.Buffer == NULL)
    {
        ws = wide_string(L"Layout");
    }

    i32 isEqual = wide_string_equals(*pPanel->Label, ws);
    if (!isEqual)
    {
        SuiStyleItem item =
            GetStyle()->Items[SuiStyleItemType_LayoutType_VerticalStack];

        v3 pos = pPanel->Position;

        pos.X += item.Offset.Margin.X;
        pos.Z = pos.Z - 0.1f;

        if ((pPanel->Flags & SuiPanelFlags_WithOutHeader) == 0)
        {
            pos.Y -= sui_panel_get_header_size(pPanel).Height;
        }

        f32 my = GetStyle()->Items[SuiStyleItemType_Panel].Offset.Margin.Y;
        pos.Y -= my;

        pos.Y += pPanel->ScrollContentOffset.Y;
        pos.Y -= pPanel->ScrollContentOffset.X;

        i64 i, count = array_count(pPanel->aWidgets);
        for (i = 0; i < count; ++i)
        {
            SuiWidget* pSuiWidget = &pPanel->aWidgets[i];
            pSuiWidget->Position = pos;

            pos.Y -= (pSuiWidget->Size.Height + item.Offset.Margin.Y);
        }

        return;
    }



    // todo: all layout margin should be set inside layout
    // so styles for widgets/panels needs global refactoring

    v3 pos = pPanel->Position;
    if ((pPanel->Flags & SuiPanelFlags_WithOutHeader) == 0)
    {
        pos.Y -= sui_panel_get_header_size(pPanel).Height;
    }
    pos.Z = pos.Z - 0.1f;

    pos.Y += pPanel->ScrollContentOffset.Y;

    SuiLayoutNode* pRoot = &pPanel->aNodes[0];

    // note: mod pos            1 - 100%, 0 - 0%
    f32 xRootProportion = 0.0f;
    f32 yRootProportion = 0.0f;

    if (pRoot->Type == SuiWidgetLayoutType_VerticalStack)
    {
        yRootProportion = 1 / array_count(pRoot->Children);
    }
    else
    {
        xRootProportion = 1 / array_count(pRoot->Children);
    }

    // note: proccess first child
    for (i64 wind = 0; wind < array_count(pRoot->Children); ++wind)
    {
        i32 ind = pRoot->Children[wind];
        SuiLayoutNode* pNode = _node_get(pPanel, ind);
        while (pNode->WidgetId == -1 && array_count(pNode->Children) > 0)
        {
            if (pRoot->Type == SuiWidgetLayoutType_VerticalStack)
            {
                if (yRootProportion == 0.0f) yRootProportion = 1.0f;
                yRootProportion /= array_count(pRoot->Children);
            }
            else
            {
                if (xRootProportion == 0.0f) xRootProportion = 1.0f;
                xRootProportion /=  array_count(pRoot->Children);
            }

            pNode = _node_get(pPanel, pNode->Children[0]);
        }

        v2 ns = pPanel->NeededSize;
        f32 width  = xRootProportion * ns.Width;
        f32 height = yRootProportion * ns.Height;


        pos.X += (wind>0?1:0) * width;
        pos.Y -= (wind>0?1:0) * height;

        v3 calculatedPos = pos;
        SuiWidget* pWidget = sui_panel_get_widget_by_id(pPanel, pNode->WidgetId);
        pWidget->Position = calculatedPos;

    }
}

// todo: refactor,
// resulting panel size is too big
void
sui_panel_calc_size(SuiPanel* pSuiPanel)
{
    SuiStyle* pStyle = GetStyle();

    SuiStyleItem panelStyle = pStyle->Items[SuiStyleItemType_Panel];
    SuiStyleItem verticalStackItem =
        pStyle->Items[SuiStyleItemType_LayoutType_VerticalStack];
    SuiStyleItem clsBtnStyle = pStyle->Items[SuiStyleItemType_Panel_Header_Close_Button];

    v2 panelMinSize = panelStyle.Size.Min;
    v2 panelMaxSize = panelStyle.Size.Max;

    v2 neededSize = {};

    // NOTE: Calc size for hdr only
    if ((pSuiPanel->Flags & SuiPanelFlags_WithOutHeader) == 0)
    {
        v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
        v2 clsBtnSize = sui_panel_get_close_button_size(hdrSize);
        clsBtnSize = v2_add(clsBtnSize, v2_mulv(clsBtnStyle.Offset.Margin, 2));
        neededSize.Width += clsBtnSize.Width;

        v2 panelTextSize = sui_get_text_metrics_v2(*pSuiPanel->Label);
        v2 panelBaseSize = v2_add(panelTextSize, panelStyle.Offset.Margin);
        neededSize = v2_add(neededSize, panelBaseSize);

        // NOTE: calc size with scrollbar
        if (pSuiPanel->Flags & SuiPanelFlags_Scrollable)
        {
            // BUG: This gives us weird resize behaviour
            //neededSize.Width += 2*scrollSize.Width;
        }
    }

    v2 layoutMargin = verticalStackItem.Offset.Margin;
    i32 widgetsCount = array_count(pSuiPanel->aWidgets);
    for (i32 w = 0; w < widgetsCount; ++w)
    {
        SuiWidget* pSuiWidget = &pSuiPanel->aWidgets[w];
        f32 widgetWidth = sui_get_full_width(*pSuiWidget);
        f32 wwidth = widgetWidth + 2 * layoutMargin.X;
        if (wwidth > neededSize.Width)
        {
            neededSize.Width = wwidth;
        }
        neededSize.Height += pSuiWidget->Size.Height;
    }

    neededSize.Height += (widgetsCount + 1) * layoutMargin.Y;

    //GINFO("Width: %f\n", panelMaxSize.Width);
    pSuiPanel->Size.Width = MinMax(neededSize.Width,
                                   panelMinSize.Width,
                                   panelMaxSize.Width);
    pSuiPanel->Size.Height = MinMax(neededSize.Height,
                                    panelMinSize.Height,
                                    panelMaxSize.Height);

    pSuiPanel->NeededSize = neededSize;
}

void
sui_panel_border_get_all(SuiPanel* pPanel, i32 isActive, v3* pPos, v2* pSize, f32* pThickness, v4* pColor)
{
    v3 pos  = pPanel->Position;
    v2 size = pPanel->Size;

    SuiStyleItem borderStyle = GetStyle()->Items[SuiStyleItemType_Panel_Border];

    v4 borderColor;
    if (isActive)
    {
        borderColor = borderStyle.Color.Clicked;
    }
    else
    {
        borderColor = borderStyle.Color.Background;
    }

    *pPos = v3_new(pos.X - borderStyle.Size.Min.X,
                   pos.Y - borderStyle.Size.Min.Y,
                   pos.Z - 0.6f);
    *pSize = v2_new(size.X + borderStyle.Size.Min.X, size.Y - borderStyle.Size.Min.Y);
    *pThickness = borderStyle.Size.Min.X;
    *pColor = borderColor;
}

SuiWidget*
sui_panel_get_widget_by_label(SuiPanel* pPanel, WideString* pLabel)
{
    WideString label = *pLabel;

    i64 i, count = array_count(pPanel->aWidgets);
    for (i = 0; i < count; ++i)
    {
        SuiWidget* pWidget = &pPanel->aWidgets[i];
        if (wide_string_equals(*pWidget->Label, label))
        {
            return pWidget;
        }
    }

    return NULL;
}

SuiWidget*
sui_panel_get_widget_by_id(SuiPanel* pPanel, SuiWidgetId id)
{
    i64 i, count = array_count(pPanel->aWidgets);
    for (i = 0; i < count; ++i)
    {
        SuiWidget* pWidget = &pPanel->aWidgets[i];
        if (pWidget->Id == id)
        {
            return pWidget;
        }
    }

    return NULL;
}

i32
sui_panel_is_scroll_visible(SuiPanel* pSuiPanel)
{
    if ((pSuiPanel->Flags & SuiPanelFlags_Scrollable) == 0)
        return 0;

    v2 s = pSuiPanel->Size;
    v2 ns = pSuiPanel->NeededSize;

    if (ns.X > s.X || ns.Y > s.Y)
        return 1;

    return 0;
}

v2
sui_panel_get_header_size(SuiPanel* pSuiPanel)
{
    SuiStyle* pSuiStyle = GetStyle();
    v2 hdrMin = pSuiStyle->Items[SuiStyleItemType_Panel_Header].Size.Min;
    v2 headerSize = v2_new(hdrMin.Width * pSuiPanel->Size.Width,
                           hdrMin.Height);

    return headerSize;
}

v3
sui_panel_get_header_position(SuiPanel* pSuiPanel)
{
    v3 hdrPos = v3_v2v(pSuiPanel->Position.XY, pSuiPanel->Position.Z - 0.1f);
    return hdrPos;
}

v3
sui_panel_get_header_text_position(SuiPanel* pSuiPanel)
{
    SuiStyleOffsetItem offsetItem = sui_style_get_offset(GetStyle(), SuiStyleItemType_Panel_Header_Text);

    SuiHorizontalAlignType horizontalAlign = SuiHorizontalAlignType_Left;
    SuiVerticalAlignType verticalAlign = SuiVerticalAlignType_Center;

    v3 hdrPos = sui_panel_get_header_position(pSuiPanel);
    v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
    v3 hdrTxtPos = sui_align_get_text_position(
        pSuiPanel->Label,
        hdrPos, hdrSize,
        verticalAlign, horizontalAlign,
        offsetItem.Margin.X);

    return hdrTxtPos;
}

v2
sui_panel_get_close_button_size(v2 hdrSize)
{
    v2 minSize = sui_style_get_size(
        GetStyle(),
        SuiStyleItemType_Panel_Header_Close_Button).Min;

    v2 closeBtnSize = v2_mul(hdrSize, minSize);
    f32 value = Min(closeBtnSize.X, closeBtnSize.Y);
    return v2_new(value, value);
}

v3
sui_panel_get_close_button_position(SuiPanel* pSuiPanel, v2 clsBtnSize)
{
    SuiStyle* pSuiStyle = GetStyle();
    SuiStyleOffsetItem offsetItem =
        sui_style_get_offset(pSuiStyle,
                             SuiStyleItemType_Panel_Header_Close_Button);

    v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
    v3 hdrPosition = sui_panel_get_header_position(pSuiPanel);

    f32 x = hdrPosition.X + hdrSize.Width - clsBtnSize.Width - offsetItem.Margin.X;
    f32 y = hdrPosition.Y - (hdrSize.Height - clsBtnSize.Height) / 2;
    f32 z = hdrPosition.Z - 0.1f;

    v3 clsBtnPos = v3_new(x, y, z);

    return clsBtnPos;
}

void
sui_panel_get_close_button_x_coordinates(v2 fullSize, v3 startPos, v2 xMinSize, f32* psx, f32* psy, f32* pex, f32* pey, f32* pzv)
{
    f32 ratio = Min(xMinSize.X, xMinSize.Y);
    f32 reverseRatio = 1.0f - ratio;

    f32 sxd = fullSize.X * (reverseRatio / 2.0f); //45 * 0.1
    f32 syd = fullSize.Y * (reverseRatio / 2.0f); //38 * 0.1

    f32 sx = startPos.X + sxd;
    f32 sy = startPos.Y - syd;
    f32 ex = sx + ratio * fullSize.X;
    f32 ey = sy - ratio * fullSize.Y;
    f32 zv = startPos.Z - 0.5f;

    *psx = sx;
    *psy = sy;
    *pex = ex;
    *pey = ey;
    *pzv = zv;

}

v2
sui_panel_get_y_scroll_size(SuiPanel* pSuiPanel)
{
    SuiStyleItem scrollStyle =
        GetStyle()->Items[SuiStyleItemType_Panel_Scroll_Y];

    v2 size = _sui_apply_offset(pSuiPanel->Size, scrollStyle.Size.Min);
    if ((pSuiPanel->Flags & SuiPanelFlags_WithOutHeader) == 1)
    {
        return v2_new(size.Width, size.Height - scrollStyle.Offset.Margin.Y);
    }

    v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
    v2 clsBtnSize = sui_panel_get_close_button_size(hdrSize);
    return v2_new(clsBtnSize.Width, //size.Width,
                  size.Height - hdrSize.Height - scrollStyle.Offset.Margin.Y);
}

v3
sui_panel_get_y_scroll_position(SuiPanel* pSuiPanel)
{
    SuiStyleItem scrollStyle =
        GetStyle()->Items[SuiStyleItemType_Panel_Scroll_Y];

    const f32 z_scroll_offset_from_panel = 0.5f;

    v3 pos = pSuiPanel->Position;
    pos.Z -= z_scroll_offset_from_panel;
    f32 marginY = scrollStyle.Offset.Margin.Y;
    f32 xOffsetFromRight = pSuiPanel->Size.Width
        - sui_panel_get_y_scroll_size(pSuiPanel).Width
        - scrollStyle.Offset.Margin.X;

    if ((pSuiPanel->Flags & SuiPanelFlags_WithOutHeader) == 1)
    {
        return v3_new(pos.X + xOffsetFromRight,
                      pos.Y - marginY,
                      pos.Z);
    }


    v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
    v3 clsBtnPos = sui_panel_get_close_button_position(
        pSuiPanel,
        sui_panel_get_close_button_size(hdrSize));
    return v3_new(pos.X + xOffsetFromRight,
                  pos.Y - marginY - hdrSize.Height,
                  pos.Z);
}

v2
sui_panel_get_y_scroll_button_size(SuiPanel* pSuiPanel)
{
    SuiStyleItem scrollBtnStyle =
        GetStyle()->Items[SuiStyleItemType_Panel_Scroll_Button_Y];
    v2 scrollSize = sui_panel_get_y_scroll_size(pSuiPanel);
    v2 minSize = scrollBtnStyle.Size.Min;
    f32 marginY = scrollBtnStyle.Offset.Margin.Y;

    f32 availableSpace = scrollSize.Height - 2 * marginY;
    f32 btnHeight = (pSuiPanel->Size.Height / pSuiPanel->NeededSize.Y) * availableSpace;
    btnHeight = Max(btnHeight, minSize.Height);
    v2 size = v2_new(scrollSize.Width * minSize.Width,
                     btnHeight);

    return size;
}

// NOTE: Only for vertical scroll button
// NOTE: Do we really need horizontal scroll button?
v3
sui_panel_get_y_scroll_button_position(SuiPanel* pSuiPanel)
{
    SuiStyleItem scrollBtnStyle =
        GetStyle()->Items[SuiStyleItemType_Panel_Scroll_Button_Y];

    // NOTE: scrollPos
    v3 scrollPos = sui_panel_get_y_scroll_position(pSuiPanel);
    v2 scrollSize = sui_panel_get_y_scroll_size(pSuiPanel);

    // NOTE: sbs - scroll button size
    v2 sbs = sui_panel_get_y_scroll_button_size(pSuiPanel);

    f32 marginY = scrollBtnStyle.Offset.Margin.Y;
    f32 marginX = (scrollSize.Width - sbs.Width) / 2;

    v3 sbp = v3_new(scrollPos.X + marginX,
                    scrollPos.Y - pSuiPanel->ScrollOffset.Y - marginY,
                    scrollPos.Z - 0.1f);

    //v3_print(sbp);

    return sbp;
}

// todo: wip
v2
sui_panel_get_x_scroll_size(SuiPanel* pSuiPanel)
{
    SuiStyleItem scrollStyle =
        GetStyle()->Items[SuiStyleItemType_Panel_Scroll_X];

    v2 size = _sui_apply_offset(pSuiPanel->Size, scrollStyle.Size.Min);
    if ((pSuiPanel->Flags & SuiPanelFlags_WithOutHeader) == 1)
    {
        return v2_new(size.Width, size.Height - scrollStyle.Offset.Margin.Y);
    }

    v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
    v2 clsBtnSize = sui_panel_get_close_button_size(hdrSize);
    v2 scrollYSize = sui_panel_get_y_scroll_size(pSuiPanel);
    return v2_new(hdrSize.Width - scrollYSize.Width,
                  size.Height);
}

// todo: wip
v3
sui_panel_get_x_scroll_position(SuiPanel* pSuiPanel)
{
    SuiStyleItem scrollStyle =
        GetStyle()->Items[SuiStyleItemType_Panel_Scroll_X];

    const f32 z_scroll_offset_from_panel = 0.5f;

    v3 pos = pSuiPanel->Position;
    pos.Z -= z_scroll_offset_from_panel;

    f32 marginY = scrollStyle.Offset.Margin.Y;
    f32 width = pSuiPanel->Size.Width;
    v2 scrollSize = sui_panel_get_x_scroll_size(pSuiPanel);

    // todo: refactor this
    if ((pSuiPanel->Flags & SuiPanelFlags_WithOutHeader) == 1)
    {
        return v3_new(pos.X,
                      pos.Y + marginY - scrollSize.Height,
                      pos.Z);
    }


    v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
    v3 clsBtnPos = sui_panel_get_close_button_position(
        pSuiPanel,
        sui_panel_get_close_button_size(hdrSize));
    pos.Y -= pSuiPanel->Size.Height;
    pos.Y += scrollSize.Height;

    return pos;
}

// todo: wip
v2
sui_panel_get_x_scroll_button_size(SuiPanel* pSuiPanel)
{
    SuiStyleItem scrollBtnStyle =
        GetStyle()->Items[SuiStyleItemType_Panel_Scroll_Button_X];
    v2 scrollSize = sui_panel_get_x_scroll_size(pSuiPanel);

    f32 marginX = scrollBtnStyle.Offset.Margin.X;
    f32 availableSpace = scrollSize.Width - 2 * marginX;
    f32 btnWidth = (pSuiPanel->Size.Width / pSuiPanel->NeededSize.X) * availableSpace;
    btnWidth = Max(btnWidth, scrollBtnStyle.Size.Min.Width);
    v2 size = v2_new(btnWidth * availableSpace, 30);



#if 0
    SuiStyleItem scrollBtnStyle =
        GetStyle()->Items[SuiStyleItemType_Panel_Scroll_Button_Y];
    v2 scrollSize = sui_panel_get_y_scroll_size(pSuiPanel);
    v2 minSize = scrollBtnStyle.Size.Min;
    f32 marginY = scrollBtnStyle.Offset.Margin.Y;

    f32 availableSpace = scrollSize.Height - 2 * marginY;
    f32 btnHeight = (pSuiPanel->Size.Height / pSuiPanel->NeededSize.Y) * availableSpace;
    btnHeight = Max(btnHeight, minSize.Height);
    v2 size = v2_new(scrollSize.Width * minSize.Width,
                     btnHeight);

#endif


    return size;
}

// todo: wip
v3
sui_panel_get_x_scroll_button_position(SuiPanel* pSuiPanel)
{
    SuiStyleItem scrollBtnStyle =
        GetStyle()->Items[SuiStyleItemType_Panel_Scroll_Button_X];

    // NOTE: scrollPos
    v3 scrollPos = sui_panel_get_x_scroll_position(pSuiPanel);
    v2 scrollSize = sui_panel_get_x_scroll_size(pSuiPanel);

    // NOTE: sbs - scroll button size
    v2 sbs = sui_panel_get_y_scroll_button_size(pSuiPanel);

    f32 centerYOffset = (scrollSize.Height - sbs.Height) / 2;

    scrollPos.X += pSuiPanel->ScrollOffset.X;
    //scrollPos.Y;
    scrollPos.Z -= 0.1f;

    v3 sbp = v3_new(scrollPos.X + pSuiPanel->ScrollOffset.X,
                    scrollPos.Y,
                    scrollPos.Z - 0.1f);

    //v3_print(sbp);

    return sbp;
}

v3
sui_panel_get_widget_offset(v3 panelPos, v2 headerSize, v2 padding)
{
    v3 pos = v3_new(panelPos.X + padding.X, panelPos.Y - headerSize.Y - padding.Y, panelPos.Z);
    return pos;
}

i32
sui_panel_is_active(SuiPanel* pSuiPanel)
{
    i32 isCurrentPanelActive =
        (pSuiPanel->Id == gSuiInstance.PrevFrameCache.ActivePanelId);
    return isCurrentPanelActive;
}

i32
sui_panel_is_active_or_focus(SuiPanel* pSuiPanel)
{
    i32 isTrue = (
        (pSuiPanel->Id == gSuiInstance.PrevFrameCache.ActivePanelId)
        ||
        (pSuiPanel->Id == gSuiInstance.PrevFrameCache.FocusPanelInd)
        );
    return isTrue;
}

SuiPanel*
sui_panel_get_by_i(i32 i)
{
    i32 ind = gSuiInstance.PrevFrameCache.Panels[i];
    SuiPanel* pSuiPanel = &gSuiInstance.PrevFrameCache.PanelsTable[ind].Value;
    return pSuiPanel;
}

SuiPanel
sui_panel_get_by_i_value(i32 i)
{
    i32 ind = gSuiInstance.PrevFrameCache.Panels[i];
    SuiPanel suiPanel = gSuiInstance.PrevFrameCache.PanelsTable[ind].Value;
    return suiPanel;
}

void
sui_panel_item_make_active_by_state(SuiState state, SuiPanelId id)
{
    if (state == SuiState_Clicked || state == SuiState_Grabbed)
    {
        gSuiInstance.PrevFrameCache.ActivePanelId = id;
        //GINFO("Active: %d\n", id);
        sui_panel_active_behaviour();
    }
}

SuiPanel*
sui_panel_get_next_tab()
{
    SuiPanelId activeId = gSuiInstance.PrevFrameCache.ActivePanelId;
    i32 focusInd = gSuiInstance.PrevFrameCache.FocusPanelInd;
    SuiInstance* pSuiInstance = sui_get_instance();
    i32 cnt = array_count(gSuiInstance.PrevFrameCache.Panels);


#if defined(REVERSE_TAB_BEH)
    ++focusInd;
    if (focusInd >= cnt)
    {
        focusInd = cnt - 1;
    }
#else
    // note: remove fix for cnt == 1
    i32 nextFocusInd = focusInd + 1;
    if (nextFocusInd >= cnt)
    {
focusInd = cnt - 1;
    }

#endif

    gSuiInstance.PrevFrameCache.FocusPanelInd = focusInd;
    //GINFO("Focus index=%d\n", gSuiInstance.PrevFrameCache.FocusPanelInd);

    SuiPanel* pSuiPanel = sui_panel_get_by_i(focusInd);
    return pSuiPanel;
}

void
sui_is_panel_blocked_by_other_panels(SuiPanel* pCurrentPanel)
{
    // DOCS(typedef): Check if panel is blocked by another panel with smaller zorder value
    i32 cnt = array_count(gSuiInstance.PrevFrameCache.Panels);
    //GINFO("cnt: %d\n", cnt);
    for (i32 i = 0; i < cnt; ++i)
    {
        SuiPanel* pOtherPanel = sui_panel_get_by_i(i);
        if (pOtherPanel == NULL)
            continue;

        v3 pos = pOtherPanel->Position;

        if (pOtherPanel->Id == pCurrentPanel->Id
            || f32_equal(pos.Z, pCurrentPanel->Position.Z)
            || pos.Z > pCurrentPanel->Position.Z)
            continue;

        SuiState state;
        if (sui_is_item_hovered(pOtherPanel->Position, pOtherPanel->Size))
        {
            pCurrentPanel->State = SuiState_None;
            pCurrentPanel->HeaderState = SuiState_None;
            pCurrentPanel->IsBlockedByOther = 1;
            return;
        }
    }

    pCurrentPanel->IsBlockedByOther = 0;
}

i32
sui_panel_is_overflow(SuiPanel* pSuiPanel)
{
    v2 diff = v2_sub(pSuiPanel->Size, pSuiPanel->NeededSize);
    return ((diff.X < 0) || (diff.Y < 0));
}

/*
  #####################################
  DOCS(typedef): Sui Popups
  #####################################
*/

i32
sui_popup_begin_l(WideString* label, SuiPopupFlags flags)
{

    SuiInstance* pInstance = sui_get_instance();
    WideString key = *label;

    i64 popupIndex = whash_geti(pInstance->PrevFrameCache.PopupsTable, key);
    if (popupIndex == -1)
    {
        SuiPopup value = {};
        whash_put(pInstance->PrevFrameCache.PopupsTable, key, value);
    }

    i64 index = table_header(pInstance->PrevFrameCache.PopupsTable)->Index;
    SuiPopup* pSuiPopup = &pInstance->PrevFrameCache.PopupsTable[index].Value;

    //pSuiPopup->;

    return 0;
}

i32
sui_popup_begin(char* label, SuiPopupFlags flags)
{
    SuiInstance* pSuiInstance = sui_get_instance();
    vassert(pSuiInstance->IsInitialized);
    WideString* gWideLabel = iwide_string_new_utf8(&pSuiInstance->IPool, label);
    i32 result = sui_popup_begin_l(gWideLabel, flags);
    return result;
}

void
sui_popup_end()
{

}


void
_very_temporary_printing_stack(SuiPanel* pSuiPanel, const char* message, SuiWidgetLayoutType layout)
{
    char msg[62] = {};
    snprintf(msg, 62, "%s %s", message, sui_widget_layout_type_to_string(layout));

    GINFO("%s | Types:", msg);
    array_foreach(pSuiPanel->LayoutStack,
                  printf(" %s", sui_widget_layout_type_to_string(item)));

    printf("\n");
}

void
sui_panel_push_layout(SuiWidgetLayoutType layout)
{
    SuiPanel* pSuiPanel = sui_get_current_panel();
    array_push(pSuiPanel->LayoutStack, layout);

    //_very_temporary_printing_stack(pSuiPanel, "[Push Layout]", layout);
}

void
sui_panel_pop_layout()
{
    SuiPanel* pSuiPanel = sui_get_current_panel();
    SuiWidgetLayoutType layout = array_pop(pSuiPanel->LayoutStack);

    //_very_temporary_printing_stack(pSuiPanel, "[Pop Layout]", layout);
}

i32
sui_is_click_outside_visible_viewport(SuiWidget* pWidget)
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();

    v2 mp = sui_get_instance()->InputData.MousePosition;
    f32 px = mp.X;
    f32 py = mp.Y;

    v2 min, max;
    SuiPanelFlags flags = pCurrentPanel->Flags;
    i32 hasHdr = ((flags & SuiPanelFlags_WithOutHeader) == 0);
    i32 hasScrollBar = (flags & SuiPanelFlags_Scrollable);

    if (!hasHdr && !hasScrollBar)
    {
        sui_get_panel_aabb(pCurrentPanel, &min, &max);
    }
    else
    {
        sui_get_panel_aabb_w_hdr(pCurrentPanel, &min, &max);
    }

    v2 xScrollSize = sui_panel_get_x_scroll_size(pCurrentPanel);
    v3 xScrollPos = sui_panel_get_x_scroll_position(pCurrentPanel);
    v2 yScrollSize = sui_panel_get_y_scroll_size(pCurrentPanel);
    v3 yScrollPos = sui_panel_get_y_scroll_position(pCurrentPanel);

    if (sui_is_item_hovered(xScrollPos, xScrollSize))
        return 1;
    if (sui_is_item_hovered(yScrollPos, yScrollSize))
        return 1;

    if (px >= min.X && px <= max.X && py >= min.Y && py <= max.Y)
        return 0;

    return 1;
}

i32
_sui_panel_header_is_hovered()
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();
    v3 pos = sui_panel_get_header_position(pCurrentPanel);
    v2 size = sui_panel_get_header_size(pCurrentPanel);
    return sui_is_item_hovered(pos, size);
}

#define sui_widget_ignore_input(pCurrentPanel, pSuiWidget)		\
    {                                                                   \
        SuiInputData* pInput = GetInputData();				\
                                                                        \
        i32 isOutsideV = sui_is_click_outside_visible_viewport(pSuiWidget); \
        if (isOutsideV)							\
        {								\
            pSuiWidget->State = SuiState_None;				\
            return SuiState_None;					\
        }								\
                                                                        \
        SuiPanel* i_pCurrentPanel = sui_get_current_panel();		\
                                                                        \
        if (pInput->IsInputAlreadyHandled				\
            || (i_pCurrentPanel->ScrollYState == SuiState_Grabbed)	\
            || (i_pCurrentPanel->ScrollXState == SuiState_Grabbed)	\
            || pCurrentPanel->IsBlockedByOther				\
            || (!GetInputData()->IsInGrabState && isOutsideV)		\
            || _sui_panel_header_is_hovered())				\
        {                                                               \
            pSuiWidget->State = SuiState_None;                          \
            return SuiState_None;                                       \
        }                                                               \
    }

v2
sui_get_text_background_size(v2 size, v2 margin)
{
    v2 r = v2_new(1.2 * size.X + 2 * margin.X,
                  2 * size.Y + 2 * margin.Y);
    return r;
}

void
_sui_widget_set_existance_flag(SuiWidget* pWidget)
{
    pWidget->StillExist = 1;
}

SuiWidget*
_sui_button_create(SuiPanel* pCurrentPanel, WideString* pLabel, v2 definedSize)
{
    WidgetGuard(pLabel);

    WideString labelValue = *pLabel;

    // DOCS(): CREATION stage
    v2 size;
    if (definedSize.Width > 0.0f && definedSize.Height > 0.0f)
    {
        size = definedSize;
    }
    else
    {
        size = sui_get_text_metrics_v2(labelValue);
        SuiStyle* pStyle = GetStyle();
        v2 margin = pStyle->Items[SuiStyleItemType_Widget_Button].Offset.Margin;
        size = sui_get_text_background_size(size, margin);
        //size = v2_new(w + 2 * margin.X, 1.5f * h + 2 * margin.Y);
    }

    SuiWidget newBtn = {
        .Type = SuiWidgetType_Button,
        .Label = pLabel,
        .Size = size,
        .State = SuiState_None,
    };

    SuiWidget* pBtnWidget =
        _sui_panel_add_widget(pCurrentPanel, newBtn);

    if (pCurrentPanel->Flags & SuiPanelFlags_LayoutExist)
    {
        _sui_new_tree_append_node(pCurrentPanel, pBtnWidget);
    }

    return pBtnWidget;
}

SuiState
_sui_base_widget_process_input(SuiPanel* pCurrentPanel, SuiWidget* pWidget)
{
    sui_widget_ignore_input(pCurrentPanel, pWidget);

    SuiState state = sui_is_item_pressed(pWidget->Position, pWidget->Size, pWidget->Id);
    pWidget->State = state;

    if (IsKeyReleased(SuiKeys_Mouse_Left_Button))
    {
        pWidget->State = SuiState_None;
    }

    return pWidget->State;
}

SuiState
sui_button_l(WideString* pLabel, v2 definedSize)
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();

    SuiWidget* pWidget = sui_panel_get_widget_by_label(pCurrentPanel, pLabel);
    if (pWidget == NULL)
    {
        pWidget = _sui_button_create(pCurrentPanel, pLabel, definedSize);
    }

    SuiState state = _sui_base_widget_process_input(pCurrentPanel, pWidget);
    _sui_widget_set_existance_flag(pWidget);

    return state;
}

SuiState
sui_button(char* pLabel, v2 definedSize)
{
    WideString* pWideLabel = _sui_convert_utf8_to_wide_string(pLabel);
    SuiState state = sui_button_l(pWideLabel, definedSize);
    return state;
}

SuiWidget*
_sui_slider_create(SuiPanel*pCurrentPanel, WideString* pLabel, f32* pValue, v2 range)
{
    WidgetGuard(pLabel);

    SuiStyle* pSuiStyle = GetStyle();
    SuiStyleItem sliderStyle =
        pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32];
    SuiStyleItem slideStyle =
        pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32_Slider];

    WideString labelValue = *pLabel;
    SuiDrawData* pDrawData = GetDrawData();

    v2 sliderSize = sliderStyle.Size.Min;
    f32 textWidth, textHeight;
    sui_get_text_metrics(labelValue, &textWidth, &textHeight);

    SuiWidget newSlider = {
        .Type = SuiWidgetType_Slider_F32,
        .Label = pLabel,
        .Size = sliderSize,
        .State = SuiState_None,
        .FloatValues = v4_new(*pValue, range.X, range.Y, 0)
    };
    // END OF NEW

    SuiWidget* pSliderWidget = _sui_panel_add_widget(pCurrentPanel, newSlider);

    return pSliderWidget;
}

SuiState
_sui_slider_process_input(SuiPanel* pCurrentPanel, SuiWidget* pSliderWidget, f32* pValue, v2 range)
{
    sui_widget_ignore_input(pCurrentPanel, pSliderWidget);

    SuiDrawData* pDrawData = GetDrawData();

    SuiState state = SuiState_None;
    f32 offsetX = sui_slider_get_slide_offset_x(*pValue, range);
    v2 slideSize = sui_slider_get_slide_size(pSliderWidget->Size);
    v3 slidePosition = sui_slider_get_slide_position(pSliderWidget->Position, offsetX);
    if (sui_is_item_hovered(pSliderWidget->Position, pSliderWidget->Size))
    {
        pSliderWidget->SubState = SuiState_Hovered;

        state = sui_is_item_pressed(slidePosition, slideSize, pSliderWidget->Id);
        pSliderWidget->State = state;
        pDrawData->HotID = pSliderWidget->Id;
    }

    if (pDrawData->ActiveID == pSliderWidget->Id)
    {
        if (state == SuiState_None)
        {
            pSliderWidget->State = SuiState_Grabbed;
            //GINFO("SubState: %s\n", sui_state_to_string(pSliderWidget->SubState));
            //GINFO("State: %s\n", sui_state_to_string(pSliderWidget->State));
        }

        SuiInputData* pInputData = GetInputData();
        f32 ratioX = (pInputData->MousePosition.X - pSliderWidget->Position.X) / pSliderWidget->Size.Width;

        f32 newValue = MinMaxV2(ratioX * (range.Max - range.Min) + range.Min, range);
        if (newValue != *pValue)
        {
            *pValue = newValue;
            return SuiState_Changed;
        }
    }

    if (IsKeyReleased(SuiKeys_Mouse_Left_Button))
    {
        pSliderWidget->State = SuiState_None;
        return SuiState_Clicked;
    }

    return SuiState_None;
}

SuiState
sui_slider_f32_l(WideString* pLabel, f32* pValue, v2 range)
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();

    SuiWidget* pWidget = sui_panel_get_widget_by_label(pCurrentPanel, pLabel);
    if (pWidget == NULL)
        pWidget = _sui_slider_create(pCurrentPanel, pLabel, pValue, range);

    wchar buf[256] = {};
    size_t bufLength = swprintf(buf, 256, L"%0.2f", *pValue);

    /*
      bug: should:
      * use arena
      * been update constantly every frame
    */
    pWidget->TempBuffer = sui_wide_string_raw(buf, bufLength);
    pWidget->FloatValues.X = *pValue;

    SuiState state = _sui_slider_process_input(pCurrentPanel, pWidget, pValue, range);

    _sui_widget_set_existance_flag(pWidget);

    return state;
} // sui_slider_f32_l

SuiState
sui_slider_f32(char* pLabel, f32* pValue, v2 range)
{
    WideString* pWideLabel = _sui_convert_utf8_to_wide_string(pLabel);
    SuiState state = sui_slider_f32_l(pWideLabel, pValue, range);
    return state;
}

SuiWidget*
_sui_checkbox_create(SuiPanel* pCurrentPanel, WideString* pLabel, i32* pValue)
{
    WidgetGuard(pLabel);

    SuiStyle* pSuiStyle = GetStyle();
    SuiStyleItem styleItem = pSuiStyle->Items[SuiStyleItemType_Widget_Checkbox];

    i32 value = *pValue;
    v2 size = styleItem.Size.Min;

    SuiWidget newCheckbox = {
        .Type = SuiWidgetType_Checkbox,
        .Label = pLabel,
        .Size = size,
        .State = SuiState_None,
        .Flag0 = *pValue,
    };
    // END OF NEW

    SuiWidget* pCheckboxWidget = _sui_panel_add_widget(pCurrentPanel, newCheckbox);

    return pCheckboxWidget;
}

SuiState
_sui_checkbox_process_input(SuiPanel* pCurrentPanel, SuiWidget* pCheckboxWidget, WideString* pLabel, i32* pValue)
{
    sui_widget_ignore_input(pCurrentPanel, pCheckboxWidget);

    v3 pos = pCheckboxWidget->Position;
    v2 size = pCheckboxWidget->Size;

    if (sui_is_item_hovered(pos, size))
    {
        pCheckboxWidget->State = sui_is_item_pressed(pos, size, pCheckboxWidget->Id);
        if (pCheckboxWidget->State == SuiState_Clicked)
        {
            i32 val = !(*pValue);
            *pValue = val;
            pCheckboxWidget->Flag0 = val;
        }
    }

    return pCheckboxWidget->State;
}

SuiState
sui_checkbox_l(WideString* pLabel, i32* pValue)
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();

    SuiWidget* pWidget = sui_panel_get_widget_by_label(pCurrentPanel, pLabel);
    if (pWidget == NULL)
        pWidget = _sui_checkbox_create(pCurrentPanel, pLabel, pValue);

    SuiState state = _sui_checkbox_process_input(pCurrentPanel, pWidget, pLabel, pValue);

    _sui_widget_set_existance_flag(pWidget);

    return state;
} // sui_ckeckbox

SuiState
sui_checkbox(char* pLabel, i32* pValue)
{
    WideString* pWideLabel = _sui_convert_utf8_to_wide_string(pLabel);
    SuiState state = sui_checkbox_l(pWideLabel, pValue);
    return state;
}

v3
sui_text_widget_get_position(v3 pos, v2 size, WideString str)
{
    f32 y0, y1;
    font_get_first_char_y(gSuiInstance.pFont, str, pos, &y0, &y1);
    f32 fheight = y1 - y0;

    f32 freeYSpace = size.Y - fheight;
    freeYSpace /= 2;
    //GINFO("FreeYSpace: %f [%f]\n", freeYSpace, fheight);
    v3 resPos = v3_new(pos.X, pos.Y-freeYSpace, pos.Z);
    return resPos;
}

SuiWidget*
_sui_text_create(SuiPanel* pCurrentPanel, WideString* pFormat, va_list vardicList)
{
    WidgetGuard(pFormat);

    SuiStyle* pSuiStyle = GetStyle();
    WideString formatValue = *pFormat;

    SuiWidget newText = {
        .Label = pFormat,
        .Type = SuiWidgetType_Text,
        .State = SuiState_None,
    };

    SuiWidget* pTextWidget = _sui_panel_add_widget(pCurrentPanel, newText);
    return pTextWidget;
}

SuiState
_sui_text(WideString* pFormat, va_list vardicList)
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();

    SuiWidget* pWidget = sui_panel_get_widget_by_label(pCurrentPanel, pFormat);
    if (pWidget == NULL)
        pWidget = _sui_text_create(pCurrentPanel, pFormat, vardicList);

    wchar buf[1024] = {0};
    wchar fbuf[1024] = {0};
    memcpy(fbuf, pFormat->Buffer, pFormat->Length * sizeof(wchar));
    i32 len = vswprintf(buf, 1024, fbuf, vardicList);
    vguard(len != -1);
    WideString* pNewString = sui_wide_string_raw(buf, len);
    //memory_unbind_current_arena();
    v2 size = sui_get_text_metrics_v2(*pNewString);
    pWidget->Size = size;
    pWidget->TempBuffer = pNewString;

    SuiState state = _sui_base_widget_process_input(pCurrentPanel, pWidget);

    _sui_widget_set_existance_flag(pWidget);

    return state;
}

SuiState
sui_text_l(WideString* format, ...)
{
    va_list vl;
    va_start(vl, format);
    SuiState state = _sui_text(format, vl);
    va_end(vl);
    return state;
} // sui_text

SuiState
sui_text(char* pFormat, ...)
{
    WideString* pWideLabel = _sui_convert_utf8_to_wide_string(pFormat);

    va_list vl;
    va_start(vl, pFormat);
    SuiState state = _sui_text(pWideLabel, vl);
    va_end(vl);

    return state;
}

SuiWidget*
_sui_input_create(SuiPanel* pCurrentPanel, WideString* label, wchar* buf, i32 len, i32 maxLength)
{
    WidgetGuard(label);
    WidgetGuard(buf);

    SuiStyle* pSuiStyle = GetStyle();
    SuiDrawData* pDrawData = GetDrawData();
    SuiInputData* pInputData = GetInputData();
    SuiStyleSizeItem inputStyleItem = sui_style_get_size(GetStyle(), SuiStyleItemType_Widget_Input_String);

    f32 w, h;
    sui_get_text_metrics_ext(buf, len, &w, &h);
    v2 size = inputStyleItem.Min;
    SuiWidget newInput = {
        .Type = SuiWidgetType_InputString,
        .Size = size,
        .State = SuiState_None,
    };
    // END OF NEW

    SuiWidget* pInputWidget = _sui_panel_add_widget(pCurrentPanel, newInput);
    return pInputWidget;
}

SuiState
_sui_input_process_input(SuiPanel* pCurrentPanel, SuiWidget* pInputWidget, wchar* buf, i32 len, i32 maxLength)
{
    SuiInputData* pInputData = GetInputData();
    SuiDrawData* pDrawData = GetDrawData();

    SuiState state = SuiState_None;
    if (pInputData->IsInputAlreadyHandled || pCurrentPanel->IsBlockedByOther)
    {
        pInputWidget->State = SuiState_None;
        goto inputStringEndLabel;
    }

    state = sui_is_item_pressed(pInputWidget->Position, pInputWidget->Size, pInputWidget->Id);

    i32 textInd = pInputData->TextIndexPosition;

    if (pInputWidget->Id == pDrawData->ActiveID)
    {
        pInputData->FocusID = pInputWidget->Id;
    }

    //GINFO("ActiveID: %d HotID: %d\n", pDrawData->ActiveID, pDrawData->HotID);
    if (pInputWidget->Id == pInputData->FocusID)
    {
        if (IsKeyPressed(SuiKeys_Backspace))
        {
            if (len > 0 && pInputData->TextIndexPosition > 0)
            {
                i32 copyLength = len - pInputData->TextIndexPosition;
                for (i32 i = 0; i < copyLength; ++i)
                {
                    i32 curPos = i + pInputData->TextIndexPosition;
                    buf[curPos - 1] = buf[curPos];
                }

                buf[len - 1] = 0;
                --pInputData->TextIndexPosition;
            }
        }
        else if (IsKeyPressed(SuiKeys_LeftArrow))
        {
            pInputData->TextIndexPosition = MinMax(pInputData->TextIndexPosition - 1, 0, len);
        }
        else if (IsKeyPressed(SuiKeys_RightArrow))
        {
            pInputData->TextIndexPosition = MinMax(pInputData->TextIndexPosition + 1, 0, len);
        }
        else if (IsKeyPressed(SuiKeys_Home))
        {
            pInputData->TextIndexPosition = 0;
        }
        else if (IsKeyPressed(SuiKeys_End))
        {
            pInputData->TextIndexPosition = len;
        }
        else if (pInputData->CharTyped != 0 && (pInputData->TextIndexPosition < (maxLength - 1)) && (len < maxLength))
        {
            if (pInputData->Mode == SuiModeType_Insert)
            {
                i32 temp = buf[textInd + 1];
                for (i32 i = len; i > textInd; --i)
                {
                    buf[i] = buf[i - 1];
                }

                buf[textInd] = pInputData->CharTyped;
                ++pInputData->TextIndexPosition;
            }
            else if (pInputData->Mode == SuiModeType_Overwrite)
            {
                buf[pInputData->TextIndexPosition] = pInputData->CharTyped;
                ++pInputData->TextIndexPosition;
            }
        }

    }

inputStringEndLabel:
    // TODO(): Create correct WideString
    //pInputWidget->TempBuffer = sui_wide_string_raw(buf, );
    return state;
}

//BUG(typedef): empty [wchar* buf] -> seg fault
SuiState
sui_input_string(WideString* pLabel, wchar* buf, i32 len, i32 maxLength)
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();

    SuiWidget* pWidget = sui_panel_get_widget_by_label(pCurrentPanel, pLabel);
    if (pWidget == NULL)
        pWidget = _sui_input_create(pCurrentPanel, pLabel, buf, len, maxLength);

    SuiState state = _sui_input_process_input(pCurrentPanel, pWidget, buf, len, maxLength);

    _sui_widget_set_existance_flag(pWidget);

    return state;
} // sui_input

SuiWidget*
_sui_dropdown_create(SuiPanel* pCurrentPanel, WideString* pLabel, WideString* pTexts, i32 textsCount, i32* pSelected, i32* pIsOpened)
{
    WidgetGuard(pLabel);
    vguard_not_null(pTexts);

    SuiStyle* pSuiStyle = GetStyle();
    SuiDrawData* pDrawData = GetDrawData();
    SuiInputData* pInputData = GetInputData();
    SuiStyleSizeItem dropdownStyleItem = sui_style_get_size(GetStyle(), SuiStyleItemType_Widget_Dropdown);

    // TODO: Place inside configuration as default VisibleCount
    // TODO: Create functions for setting it on application side
    const i32 HARDCODED_VISIBLE_COUNT = 5;

    SuiDropDown* pSuiDropDown = memory_allocate_type(SuiDropDown);
    pSuiDropDown->VisibleCount = HARDCODED_VISIBLE_COUNT;
    pSuiDropDown->SelectedIndex = *pSelected;
    pSuiDropDown->IsOpened = *pIsOpened;
    pSuiDropDown->SelectOptions = pTexts;

    v2 size = dropdownStyleItem.Min;
    SuiWidget newDropDown = {
        .Label = pLabel,
        .Type = SuiWidgetType_Dropdown,
        .Size = size,
        .Data = (void*) pSuiDropDown
    };

    SuiWidget* pDdWidget = _sui_panel_add_widget(pCurrentPanel, newDropDown);
    return pDdWidget;
}

SuiState
_sui_dropdown_process_input(SuiPanel* pCurrentPanel, SuiWidget* pDdWidget, WideString* pTexts, i32 textsCount, i32* pSelected, i32* pIsOpened)
{
    sui_widget_ignore_input(pCurrentPanel, pDdWidget);

    SuiDropDown* pSuiDropDown = (SuiDropDown*) pDdWidget->Data;

    SuiState state = sui_is_item_pressed(pDdWidget->Position, pDdWidget->Size, pDdWidget->Id);
    pDdWidget->State = state;
    if (state == SuiState_Clicked)
    {
        pSuiDropDown->IsOpened = !pSuiDropDown->IsOpened;

    }

    // memory_bind_current_arena();

    f32 fw = 0.0f, fh = 0.0f;
    for (i32 i = 0; i < textsCount; ++i)
    {
        WideString wst = pTexts[i];
        f32 w, h;
        sui_get_text_metrics(wst, &w, &h);

        if (h > fh)
            fh = h;
    }

    // memory_unbind_current_arena();

    pSuiDropDown->ItemHeight = sui_get_text_background_size(v2_new(fw, fh), v2_0()).Y;

    if (pSuiDropDown->IsOpened)
    {
        v3 pos = v3_sub_y(pDdWidget->Position, pDdWidget->Size.Height);
        pos.Z = sui_panel_get_header_position(pCurrentPanel).Z - 0.1;
        for (i32 i = 0; i < array_count(pSuiDropDown->SelectOptions); ++i)
        {
            WideString str = pSuiDropDown->SelectOptions[i];
            //pSuiDropDown->ItemHeight;
            v2 itemSize = v2_new(pDdWidget->Size.Width, pSuiDropDown->ItemHeight);

            if (sui_is_item_hovered(pos, itemSize))
            {
                pDdWidget->SubState = sui_is_item_pressed(pos, itemSize, pDdWidget->Id);
                pSuiDropDown->ActiveIndex = i;
                if (pDdWidget->SubState == SuiState_Clicked)
                {
                    pSuiDropDown->IsOpened = 0;
                    pSuiDropDown->SelectedIndex = i;
                }

                break;
            }

            pos = v3_sub_y(pos, pSuiDropDown->ItemHeight);
        }
    }

    *pIsOpened = pSuiDropDown->IsOpened;
    *pSelected = pSuiDropDown->SelectedIndex;

    if (pDdWidget->SubState == SuiState_Clicked)
        return pDdWidget->SubState;

    /*
      NOTE(typedef): Popup window need for this things
    */

    //GINFO("%s\n", sui_state_to_string(pDropdownWidget->State));

    return SuiState_None;
}

SuiState
sui_dropdown_l(WideString* pLabel, WideString* pTexts, i32 textsCount, i32* pSelected, i32* pIsOpened)
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();

    SuiWidget* pWidget = sui_panel_get_widget_by_label(pCurrentPanel, pLabel);
    if (pWidget == NULL)
        pWidget = _sui_dropdown_create(pCurrentPanel, pLabel, pTexts, textsCount, pSelected, pIsOpened);

    SuiState state = _sui_dropdown_process_input(pCurrentPanel, pWidget, pTexts, textsCount, pSelected, pIsOpened);

    _sui_widget_set_existance_flag(pWidget);

    return state;
}

SuiState
sui_dropdown(char* label, char** texts, i32 textsCount, i32* selected, i32* isOpened)
{
    WideString* pWideLabel = _sui_convert_utf8_to_wide_string(label);

    WideString* options = NULL;
    for (i32 i = 0; i < textsCount; ++i)
    {
        char* utf8 = texts[i];
        WideString* pOptionWide = _sui_convert_utf8_to_wide_string(utf8);
        array_push(options, *pOptionWide);
    }

    SuiState state = sui_dropdown_l(pWideLabel, options, textsCount, selected, isOpened);
    return state;
}

SuiWidget*
_sui_image_create(SuiPanel* pCurrentPanel, WideString* pLabel, v2 size, v4 color, i32 textureId)
{
    WidgetGuard(pLabel);
    //vguard(textureId >= 0);

    SuiStyle* pSuiStyle = GetStyle();
    SuiDrawData* pDrawData = GetDrawData();
    SuiInputData* pInputData = GetInputData();
    SuiStyleSizeItem imageStyleItem = sui_style_get_size(GetStyle(), SuiStyleItemType_Widget_Image);

    SuiWidget newImage = {
        .Label = pLabel,
        .Type = SuiWidgetType_Image,
        .Size = size,
        .FloatValues = color,
        .TextureId = textureId
    };

    SuiWidget* pImageWidget = _sui_panel_add_widget(pCurrentPanel, newImage);
    return pImageWidget;
}

SuiState
sui_image_l(WideString* pLabel, i64 textureId, v2 size, v4 color)
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();

    SuiWidget* pWidget = sui_panel_get_widget_by_label(pCurrentPanel, pLabel);
    if (pWidget == NULL)
        pWidget = _sui_image_create(pCurrentPanel, pLabel, size, color, textureId);

    SuiState state = _sui_base_widget_process_input(pCurrentPanel, pWidget);

    _sui_widget_set_existance_flag(pWidget);

    return state;
}

SuiState
sui_image(char* pLabel, i64 textureId, v2 size, v4 color)
{
    WideString* pWideLabel = _sui_convert_utf8_to_wide_string(pLabel);
    SuiState state = sui_image_l(pWideLabel, textureId, size, color);
    return state;
}

SuiWidget*
_sui_collapse_header_create(SuiPanel* pCurrentPanel, WideString* pLabel, i32* pIsOpen)
{
    WidgetGuard(pLabel);

    WideString labelValue = *pLabel;

    // DOCS(): CREATION stage
    v2 size = sui_get_text_metrics_v2(labelValue);
    v2 margin = GetStyle()->
        Items[SuiStyleItemType_Widget_CollapseHeader]
        .Offset.Margin;
    size = sui_get_text_background_size(size, margin);

    v3 pos = {};//sui_layout_process_next_position(&pCurrentPanel->LayoutData, size);
    size.Width = Max(size.Width, pCurrentPanel->Size.Width - (pos.X - pCurrentPanel->Position.X));

    SuiWidget newCollapseHdr = {
        .Type = SuiWidgetType_CollapseHeader,
        .Label = pLabel,
        .Size = size,
        .State = SuiState_None,
    };

    SuiWidget* pCollapseWidget =
        _sui_panel_add_widget(pCurrentPanel, newCollapseHdr);

    if (pCurrentPanel->Flags & SuiPanelFlags_LayoutExist)
    {
        SuiPanel* pPanel = sui_get_current_panel();
    }

    return pCollapseWidget;
}

SuiState
_sui_collapse_header_process_input(SuiPanel* pCurrentPanel, SuiWidget* pCollapseWidget, i32* pIsOpen)
{
    sui_widget_ignore_input(pCurrentPanel, pCollapseWidget);

    pCollapseWidget->State = sui_is_item_pressed(pCollapseWidget->Position, pCollapseWidget->Size, pCollapseWidget->Id);

    if (pCollapseWidget->State == SuiState_Hovered)
    {
        i32 result = sui_is_click_outside_visible_viewport(pCollapseWidget);
        result = result;
    }

    if (pCollapseWidget->State == SuiState_Clicked)
    {
        *pIsOpen = !(*pIsOpen);
    }

    if (IsKeyReleased(SuiKeys_Mouse_Left_Button))
    {
        pCollapseWidget->State = SuiState_None;
    }

    return pCollapseWidget->State;

}

SuiState
sui_collapse_header_l(WideString* pLabel, i32* pIsOpen)
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();

    SuiWidget* pWidget = sui_panel_get_widget_by_label(pCurrentPanel, pLabel);
    if (pWidget == NULL)
        pWidget = _sui_collapse_header_create(pCurrentPanel, pLabel, pIsOpen);

    SuiState state = _sui_collapse_header_process_input(pCurrentPanel, pWidget, pIsOpen);

    _sui_widget_set_existance_flag(pWidget);

    return state;
}

SuiState
sui_collapse_header(char* pLabel, i32* pIsOpen)
{
    WideString* pWideLabel = _sui_convert_utf8_to_wide_string(pLabel);
    SuiState state = sui_collapse_header_l(pWideLabel, pIsOpen);
    return state;
}

f32
sui_slider_get_slide_offset_x(f32 value, v2 range)
{
    SuiStyle* pSuiStyle = GetStyle();
    SuiStyleItem slideStyle =
        pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32_Slider];
    SuiStyleItem sliderStyle =
        pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32];

    v2 fullSliderSize = sliderStyle.Size.Min;
    v2 procent = slideStyle.Size.Min;
    v2 sliderSize = v2_new(fullSliderSize.Width  * procent.X,
                           fullSliderSize.Height * procent.Y);
    f32 maxSliderX = fullSliderSize.Width - sliderSize.Width;

    f32 diff = range.Max - range.Min;
    f32 onePointCostProc = 1.0f / diff;
    // 50 / (155 - 35) * size.X
    f32 procentRatio = (value - range.Min) * onePointCostProc;
    f32 offsetX = procentRatio * maxSliderX;

    /*
      .Size = {
      .Min = v2_new(0.1f, 1.0f),
      }
    */
    /*

      [0 .. sliderSize]
    */

    return offsetX;
}

v3
sui_slider_get_slide_position(v3 position, f32 offsetX)
{
    v3 slidePosition = v3_new(position.X + offsetX,
                              position.Y,
                              position.Z - 0.3f);
    return slidePosition;
}

v2
sui_slider_get_slide_size(v2 sliderSize)
{
    SuiStyleItem slideStyle =
        GetStyle()->Items[SuiStyleItemType_Widget_Slider_F32_Slider];
    f32 ratioFromFullWidthX = slideStyle.Size.Min.Width;
    v2 slideSize = v2_new(sliderSize.Width * ratioFromFullWidthX,
                          sliderSize.Height);
    return slideSize;
}

void
sui_checkbox_get_metrics(v3 widgetPos, v2 widgetSize, v3* checkPos, v2* checkSize)
{
    v2 size = v2_mulv(widgetSize, 0.55f);
    v3 pos  = v3_new(widgetPos.X + (widgetSize.X - size.X)/2, widgetPos.Y - (widgetSize.Y - size.Y)/2, widgetPos.Z-0.1f);

    *checkPos = pos;
    *checkSize = size;
}

v3
sui_input_get_text_position(WideString* text, v3 backgroundPos, v2 widgetSize)
{
    v3 pos = sui_align_get_text_position(
        text,
        v3_v2v(backgroundPos.XY, backgroundPos.Z - 0.1f), widgetSize,
        SuiVerticalAlignType_Center, SuiHorizontalAlignType_None,
        0.0f);
    return pos;
}
v2
sui_input_get_text_size(v2 backgroundSize)
{
    return v2_new(0.75f * backgroundSize.X, 0.75f * backgroundSize.Y);
}

/*
  #############################
  DOCS(typedef): Public Sui Functions For Back-end
  #############################
*/

SuiInstance*
sui_get_instance()
{
    return &gSuiInstance;
}

SuiDrawData*
sui_get_draw_data()
{
    return GetDrawData();
}

SuiInputData*
sui_get_input_data()
{
    return &gSuiInstance.InputData;
}

SuiStyle*
sui_get_style()
{
    return &gSuiInstance.Style;
}

/* SuiLayoutData* */
/* sui_get_layout_data() */
/* { */
/*     return GetLayoutData(); */
/* } */

SuiPanel*
sui_get_current_panel()
{
    i32 ind = gSuiInstance.PrevFrameCache.CurrentPanelInd;
    if (ind == -1)
    {
        return NULL;
    }

    SuiPanel* pSuiPanel = &gSuiInstance.PrevFrameCache.PanelsTable[ind].Value;
    vguard_not_null(pSuiPanel);

    return pSuiPanel;
}

void
sui_set_current_panel(i64 ind)
{
    i64 cap = table_capacity(gSuiInstance.PrevFrameCache.PanelsTable);
    i32 isCorrectInd = (ind >= 0 && ind <= cap);
    vguard(isCorrectInd && "Passsed incorrect ind to sui_set_current_panel()!");

    gSuiInstance.PrevFrameCache.CurrentPanelInd = ind;
}

SuiPanel*
sui_get_active_panel()
{
    i32 panelInd = gSuiInstance.PrevFrameCache.Panels[0];
    SuiPanel* pActivePanel = &gSuiInstance.PrevFrameCache.PanelsTable[panelInd].Value;
    return pActivePanel;
}

SuiPanel*
sui_get_panel_by_label(char* label)
{
    vguard_not_null(label);

    WideString labelAsWide = wide_string_utf8(label);

    for (size_t i = 0; i < table_capacity(gSuiInstance.PrevFrameCache.PanelsTable); ++i)
    {
        SuiPanelKeyValue keyValue = gSuiInstance.PrevFrameCache.PanelsTable[i];
        SuiPanel suiPanel = keyValue.Value;
        if (suiPanel.Label == NULL)
            continue;

        i32 equals = wide_string_equals(labelAsWide, *suiPanel.Label);
        if (equals)
        {
            return &gSuiInstance.PrevFrameCache.PanelsTable[i].Value;
        }
    }

    return NULL;
}

void
sui_get_panel_aabb(SuiPanel* pSuiPanel, v2* min, v2* max)
{
    vassert_not_null(pSuiPanel);
    *min = v2_sub_y(pSuiPanel->Position.XY, pSuiPanel->Size.Height);
    *max = v2_add_x(pSuiPanel->Position.XY, pSuiPanel->Size.Width);
}

void
sui_get_panel_aabb_w_hdr(SuiPanel* pSuiPanel, v2* min, v2* max)
{
    vassert_not_null(pSuiPanel);

    v2 xy = pSuiPanel->Position.XY;
    v2 ps = pSuiPanel->Size;
    *min = v2_sub_y(xy, ps.Height);

    v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
    *max = v2_new(xy.X + ps.Width, xy.Y - hdrSize.Height);
}

void
sui_get_current_panel_aabb(v2* min, v2* max)
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();
    vassert_not_null(pCurrentPanel);
    sui_get_panel_aabb(pCurrentPanel, min, max);
}


SuiWidget*
sui_get_current_widget()
{
    SuiPanel* pSuiPanel = sui_get_current_panel();
    i32 ind = array_count(pSuiPanel->aWidgets) - 1;
    SuiWidget* widget = &pSuiPanel->aWidgets[ind];
    return widget;
}


/*
  ######################################
  DOCS(typedef): Sui Internal Impl
  ######################################
*/

WideString*
sui_wide_string_raw(wchar* str, i32 length)
{
    memory_bind_current_arena();
    WideString ws = {
        .Buffer = str,
        .Length = length
    };
    WideString* ptr = wide_string_ptr(ws.Buffer, ws.Length);
    memory_unbind_current_arena();
    return ptr;
}

i32
_sui_panel_compare(const void* p1, const void* p2)
{
    i32 p1v = *((i32*) p1);
    i32 p2v = *((i32*) p2);

    i32 cap =
        table_capacity(gSuiInstance.PrevFrameCache.PanelsTable);
    if (p1v > cap || p2v > cap)
    {
        GWARNING("p1v > cap || p2v > cap!\n");
        return 0;
    }

    SuiPanel* pFirstPanel =
        &gSuiInstance.PrevFrameCache.PanelsTable[p1v].Value;
    SuiPanel* pSecondPanel =
        &gSuiInstance.PrevFrameCache.PanelsTable[p2v].Value;

    if (pFirstPanel->Position.Z < pSecondPanel->Position.Z)
    {
        return -1;
    }
    else if (pFirstPanel->Position.Z > pSecondPanel->Position.Z)
    {
        return 1;
    }

    return 0;
}

void
_sui_click_and_grab_state_machine(
    SuiInputData* pSuiInputData,
    SuiPanelId id,
    SuiState* pState,
    v3 pos,
    v2 size)
{
    // NOTE(): Only for panels
    SuiState state = *pState;

    //TODO reorder if and elif
    // DOCS(typedef): Some Header State Logic
    if (state != SuiState_Grabbed
        && state != SuiState_Clicked)
    {
        *pState = sui_is_item_pressed(pos, size, id);
    }
    else if (*pState == SuiState_Clicked)
    {
        *pState = SuiState_Grabbed;
        pSuiInputData->IsInGrabState = 1;
        pSuiInputData->GrabPosition = v2_v3(pos);
        pSuiInputData->GrabMousePosition = pSuiInputData->MousePosition;
    }

    if (IsKeyReleased(SuiKeys_Mouse_Left_Button))
    {
        *pState = SuiState_None;
        pSuiInputData->IsInGrabState = 0;
    }
}


/*

  DOCS(typedef): Thought process

  root (VS)
  | |  \       \    \  \
  a b  VS      HS    i  j
  | \ \   |  \
  c  d e  VS  h
  |
  f
  VS -> a, b
  VS -> VS -> c, d, e
  VS -> HS -> VS -> f
  VS -> HS -> h
  VS -> i, j

  Benefits:
  > Don't need T* Layouts

  How to find 'a' fast and easy?
  > Iterate throught tree ~~

  Should we return single Node or Array<Node>?
  > Having one Node, we can iterate to Root object
  > We can use Stack<Node> for current iteration state reserving

  How to build Stack<Node>?
  1) Recursive method that append all .Children of Child [SLOW, WE PICK THIS FOR NOW]

  struct SuiLayoutNode* node_get_children(struct SuiLayoutNode* current):

  iter0: node_get_children(Root) -> [ a ]
  iter1: node_get_children(Root) -> [ a, b ]
  iter2: node_get_children(Root) -> [ a, b, _recursion_0 ]
  _recursion_0 iter0: node_get_children(VS) -> [ c ]
  _recursion_0 iter1: node_get_children(VS) -> [ c, d ]
  _recursion_0 iter2: node_get_children(VS) -> [ c, d, e ]
  iter2: node_get_children(Root) -> [ a, b, c, d, e ]
  iter3: node_get_children(Root) -> [ a, b, c, d, e, _recursion_1 ]
  _recursion_1 iter0: node_get_children(HS) -> [ _recursion_2 ]
  _recursion_2 iter0: node_get_children(VS) -> [ f ]
  _recursion_1 iter0: node_get_children(HS) -> [ f ]
  _recursion_1 iter1: node_get_children(HS) -> [ f, h ]
  iter3: node_get_children(Root) -> [ a, b, c, d, e, f, h ]
  iter4: node_get_children(Root) -> [ a, b, c, d, e, f, h, i ]
  iter5: node_get_children(Root) -> [ a, b, c, d, e, f, h, i, j ]

  Ok it works for building stack


*/

// TODO(typedef): Revisit this to be sure, this works for already filled tree (on 2, 3, 4, ... frames)
/*
  DOCS(typedef): Thought process

  Possible solutions:
  1) Recreate tree from scratch, compare, replace if needed
  2)

  f0                                                        f1
  set all nodes as 'existable' -> remove all that is not -> repeat
  this is set up inside _sui_tree_recreate_from_stack()

*/

// DOCS: create nodes consistently, if it already exist ->
// go to next layout stackItem,
// then return index of current node for widget
i32
_sui_tree_create_nodes_for_layout_stack(SuiPanel* pPanel)
{
    // note: validate LaoutStack
    SuiWidgetLayoutType* pStack = pPanel->LayoutStack;
    i32 stackCnt = array_count(pStack);
    vassert(stackCnt > 0 && "Stack not empty!");

#if ENGINE_DEBUG == 1
    SuiPanelLayoutType stackOnStack[10] = {};
    memcpy(stackOnStack, pStack, Min(10, stackCnt) * sizeof(SuiPanelLayoutType));
#endif

    SuiLayoutNode* nodes = pPanel->aNodes;
    if (nodes == NULL)
    {
        // note: create root
        SuiLayoutNode rootNode = {
            .Parent = -1,
            .Children = NULL,
            .Type = SuiWidgetLayoutType_VerticalStack,
            .IsExist = 1,
            .WidgetId = -1
        };
        array_push(nodes, rootNode);
    }

    i32 currentInd = 0;
    for (i32 i = 1; i < stackCnt; ++i)
    {
        SuiWidgetLayoutType stackItem = pStack[i];

        i32 isAlreadyExist = 0;
        SuiLayoutNode* currentNode = &nodes[currentInd];
        i32 childrenCnt = array_count(currentNode->Children);
        for (i32 c = 0; c < childrenCnt; ++c)
        {
            i32 childInd = currentNode->Children[c];
            SuiLayoutNode* child = &nodes[childInd];

            if (child->Type == stackItem)
            {
                currentInd = childInd;
                isAlreadyExist = 1;
                break;
            }
        }

        if (isAlreadyExist)
            continue;

        SuiLayoutNode newNode = {
            .Parent = currentInd,
            .Children = NULL,
            .Type = stackItem,
            .IsExist = 1,
            .WidgetId = -1
        };
        i32 nodeInd = array_count(nodes);
        array_push(nodes, newNode);

        // DOCS(typedef): As before, we need reassign currentNode to actual pointer due to reallocation happend in nodes
        currentNode = &nodes[currentInd];
        array_push(currentNode->Children, nodeInd);

        currentInd = nodeInd;
    }

    pPanel->aNodes = nodes;

    return currentInd;
}

void
_sui_new_tree_append_node(SuiPanel* pPanel, SuiWidget* pWidget)
{
    i32 nodeInd = _sui_tree_create_nodes_for_layout_stack(pPanel);

    // DOCS: Check if widget node already exist
    SuiLayoutNode* currentNode = &pPanel->aNodes[nodeInd];
    i64 cnt = array_count(currentNode->Children);
    for (i64 n = 0; n < cnt; ++n)
    {
        i32 childInd = currentNode->Children[n];
        SuiLayoutNode* node = &pPanel->aNodes[childInd];
        if (node->WidgetId == pWidget->Id)
            return;
    }

    // DOCS: Create new widget node
    SuiLayoutNode widgetNode = {
        .IsExist = 1,
        .Parent = nodeInd,
        .WidgetId = pWidget->Id
    };
    i32 widgetInd = array_count(pPanel->aNodes);
    // note: pPanel->Nodes modified
    array_push(pPanel->aNodes, widgetNode);

    // note: We need this update because pointer to Nodes can be changed
    currentNode = &pPanel->aNodes[nodeInd];
    array_push(currentNode->Children, widgetInd);
}

#if 0
void
_sui_tree_append_node(struct SuiLayoutNode* pRoot, SuiWidget* pWidget, SuiLayoutType* pStack)
{
    i32 stackCnt = array_count(pStack);
    vassert(stackCnt > 0 && "Stack not empty!");
    vguard(pRoot->Type == pStack[0] && "Stack should be started from root->Type");

    struct SuiLayoutNode* current = pRoot;

    SuiLayoutType stackOnStack[10] = {};
    memcpy(stackOnStack, pStack, Min(10, stackCnt) * sizeof(SuiLayoutType));

    for (i32 i = 1; i < stackCnt; ++i)
    {
        SuiLayoutType stackItem = pStack[i];

        i32 isAlreadyExist = 0;
        i32 childrenCnt = array_count(current->Children);
        for (i32 c = 0; c < childrenCnt; ++c)
        {
            struct SuiLayoutNode* child = &current->Children[c];

            if (child->Parent != current)
            {
                GERROR("%ld %p %p\n", child->WidgetId, child->Parent, current);
                child->Parent = current;
            }

            if (child->Type == stackItem)
            {
                current = child;
                isAlreadyExist = 1;
                break;
            }
        }

        if (isAlreadyExist)
            continue;

        struct SuiLayoutNode newNode = {
            .Parent = current,
            .Type = stackItem,
            .IsExist = 1,
            .WidgetId = -1
        };

        array_push(current->Children, newNode);

        current = &current->Children[array_count(current->Children) - 1];
        vguard(current->Parent == newNode.Parent && "Parents equals!");
    }

    vguard(current->WidgetId == -1 && "Current is not widgetId");

    i32 ind = array_index_of(current->Children, item.WidgetId == pWidget->Id);
    if (ind != -1)
    {
        if (current->Children[ind].Parent != current)
        {
            struct SuiLayoutNode* child = &current->Children[ind];
            GERROR("%ld %p %p\n", child->WidgetId, child->Parent, current);
            child->Parent = current;
        }

        return;
    }

    struct SuiLayoutNode newNode = {
        .Parent = current,
        .IsExist = 1,
        .WidgetId = pWidget->Id
    };
    array_push(current->Children, newNode);


}
#endif

#if 0
void
_sui_tree_reset_existance(struct SuiLayoutNode* current)
{
    for (i32 c = 0; c < array_count(current->Children); ++c)
    {
        struct SuiLayoutNode* child = &current->Children[c];
        child->IsExist = 0;

        if (child->Children != NULL)
        {
            _sui_tree_reset_existance(child);
        }
    }
}

void
_sui_tree_free_children(struct SuiLayoutNode* current)
{
    if (current->Children == NULL)
        return;

    for (i32 c = 0; c < array_count(current->Children); ++c)
    {
        struct SuiLayoutNode child = current->Children[c];
        _sui_tree_free_children(&child);
    }

    array_free(current->Children);
}

void
_sui_tree_remove_old(struct SuiLayoutNode* current)
{
    for (i32 c = 0; c < array_count(current->Children); ++c)
    {
        struct SuiLayoutNode* child = &current->Children[c];
        if (child->IsExist)
        {
            _sui_tree_remove_old(child);
        }
        else
        {
            _sui_tree_free_children(child);
        }
    }
}
#endif

SuiLayoutNode*
_node_get(SuiPanel* pPanel, i32 ind)
{
    i32 isValid = (ind >= 0) && (ind < array_count(pPanel->aNodes));
    if (!isValid)
    {
        GERROR("%d (%ld)\n", ind, array_count(pPanel->aNodes));
        vassert(isValid && "Index wrong!");
    }

    SuiLayoutNode* pNode = &pPanel->aNodes[ind];
    return pNode;
}

// DOCS: pPanel->aNodes[0] = root
void
_print_node(SuiPanel* pPanel, SuiLayoutNode* pNode, i32 isRoot)
{
    if (isRoot)
    {
        const char* child_node_str = (pNode->WidgetId == -1) ? sui_widget_layout_type_to_string(pNode->Type) : "W";
        printf("_root_ [%s:%p] \n",
               child_node_str,
               pNode
            );

        return;
    }

    vassert(pNode->Parent != -1 && "_print_node: Parent node is not valid");

    SuiLayoutNode* pParentNode = _node_get(pPanel, pNode->Parent);
    vassert(sui_widget_layout_type_is_valid(pParentNode->Type) && "Laypout Node Parent is not valid!");

    if (pNode->WidgetId != -1)
    {
        printf("P[%s:%p] C[%s:%p] WId[",
               sui_widget_layout_type_to_string(pParentNode->Type),
               pParentNode,
               "W",
               pNode
            );

        i32 ind = array_index_of(pPanel->aWidgets, item.Id == pNode->WidgetId);
        if (ind == -1 || ind >= array_count(pPanel->aWidgets))
        {
            printf("\n");
            GERROR("%ld (WidgetId) is not found, or ind is %d >= array_count(pPanel->Widgets)\n", pNode->WidgetId, ind);
            vguard(0);
        }

        WideString ws = *pPanel->aWidgets[ind].Label;
        wide_string_print(ws);
        printf("]\n");

        return;
    }

    printf("P[%s:%p] C[%s:%p]\n",
           sui_widget_layout_type_to_string(pParentNode->Type),
           pParentNode,
           sui_widget_layout_type_to_string(pNode->Type),
           pNode
        );

}

void
_sui_tree_print(SuiPanel* pPanel)
{
    GINFO("\n");

    array_foreach_ptr(pPanel->aNodes, _print_node(pPanel, item, i == 0));
}

static const char*
_node_address_to_string(void* pAddress)
{
    static char buf[64];
    memset(buf, 0, 64*sizeof(char));

    size_t addr = (size_t) pAddress;
    u32 lowAddr = (u32)(u16) addr;
    size_t highAddr = addr >> 16;
    snprintf(buf, 64, "%p"BLUEM("%x"), (void*)highAddr, lowAddr);

    return buf;
}

void
_recursive_node_print(SuiPanel* pPanel, SuiLayoutNode* pNode)
{
    i32 isRoot = (pNode->Parent == -1);

    if (isRoot)
    {
        printf("__root__ [%s:%s]\n", sui_widget_layout_type_to_string(pNode->Type), _node_address_to_string(pNode));
    }
    else
    {
        SuiLayoutNode* pParent = _node_get(pPanel, pNode->Parent);

        if (pNode->WidgetId != -1)
        {
            printf("P[%s:%s]",
                   sui_widget_layout_type_to_string(pParent->Type),
                   _node_address_to_string(pParent));

            printf("C[%s:%s] Wid[", "W", _node_address_to_string(pNode));

            i32 ind = array_index_of(pPanel->aWidgets, item.Id == pNode->WidgetId);
            if (ind == -1 || ind >= array_count(pPanel->aWidgets))
            {
                printf("\n");
                GERROR("%ld (WidgetId) is not found, or ind is %d >= array_count(pPanel->Widgets)\n", pNode->WidgetId, ind);
                vguard(0);
            }

            WideString ws = *pPanel->aWidgets[ind].Label;
            wide_string_print(ws);
            printf("]\n");
        }
        else
        {
            printf("P[%s:%s] ",
                   sui_widget_layout_type_to_string(pParent->Type),
                   _node_address_to_string(pParent));

            printf("C[%s:%s]\n",
                   sui_widget_layout_type_to_string(pNode->Type),
                   _node_address_to_string(pNode));
        }

    }

    i64 i, count = array_count(pNode->Children);
    for (i = 0; i < count; ++i)
    {
        i32 ind = pNode->Children[i];
        SuiLayoutNode* pChildNode = _node_get(pPanel, ind);
        _recursive_node_print(pPanel, pChildNode);
    }
}

void
sui_new_tree_print(SuiPanel* pPanel)
{
    GINFO("\n");

    SuiLayoutNode* pRoot = pPanel->aNodes;
    if (!pRoot)
    {
        GWARNING("Root is NULL!\n");
        return;
    }

    _recursive_node_print(pPanel, pRoot);
}

SuiWidgetId
_sui_widget_get_id(WideString* label, SuiPanel* pSuiPanel)
{
    SuiWidgetId id = whash(*label) + whash(*pSuiPanel->Label);
    return id;
}

SuiWidget*
_sui_panel_add_widget(SuiPanel* pSuiPanel, SuiWidget suiWidget)
{
    SuiWidgetId widgetId = _sui_widget_get_id(suiWidget.Label, pSuiPanel);
    suiWidget.Id = widgetId;

    i32 ind = array_count(pSuiPanel->aWidgets);
    array_push(pSuiPanel->aWidgets, suiWidget);
    SuiWidget* pWidget = &pSuiPanel->aWidgets[ind];

    SuiInstance* pInstance = sui_get_instance();
    ++pInstance->PrevFrameCache.WidgetsCount;

    pSuiPanel->IsWidgetsModified = 1;

    return pWidget;
}


/*

  ###########################################################
  ###########################################################

  ##             DOCS(typedef): Sui back-end               ##

  ###########################################################
  ###########################################################

*/

static SuiFunctionBackend gSuiBackend = {};
static SuiDebugConfig gDebugConfig = {};

void
sui_backend_init(SuiBackendCreateInfo backendCreateInfo)
{
    sui_backend_validate_and_set(backendCreateInfo.Functions);
    gDebugConfig = backendCreateInfo.DebugConfig;
}

SuiDebugConfig*
sui_backend_get_debug_config_pointer()
{
    return &gDebugConfig;
}

void
sui_backend_validate_and_set(SuiFunctionBackend backend)
{
    vguard_not_null(backend.DrawRect);
    vguard_not_null(backend.DrawImage);
    vguard_not_null(backend.DrawText);
    vguard_not_null(backend.DrawItems);
    vguard_not_null(backend.DrawEmptyRect);
    vguard_not_null(backend.DrawCircle);
    vguard_not_null(backend.DrawLine);
    vguard_not_null(backend.Flush);
    vguard_not_null(backend.SetVisibleRect);

    gSuiBackend = backend;
}

void
sui_backend_draw_panel_header(SuiStyle* pSuiStyle, SuiPanel suiPanel)
{
    v2 hdrSize = sui_panel_get_header_size(&suiPanel);

    // DOCS(typedef): Draw header background
    v4 suiPanelHdrColor =
        sui_style_get_color_by_state(pSuiStyle,
                                     SuiStyleItemType_Panel_Header,
                                     suiPanel.HeaderState);
    v4 headerColor = v4_new(0.5, 0.5, 0.1, 1.0f);
    v2 hdrMin = sui_style_get_size(pSuiStyle,
                                   SuiStyleItemType_Panel_Header).Min;
    v3 headerPosition = sui_panel_get_header_position(&suiPanel);
    gSuiBackend.DrawRect(headerPosition, hdrSize, suiPanelHdrColor);

    // DOCS(typedef): Draw header text;
    SuiStyleItem hdrTxtStyle = pSuiStyle->Items[SuiStyleItemType_Panel_Header_Text];
    v4 suiPanelHdrLabelColor = hdrTxtStyle.Color.Background;
    if (gDebugConfig.IsFontDebug)
    {
        suiPanelHdrLabelColor = v4_new(1, 1, 1, gDebugConfig.IsFontDebug);
    }
    v3 textPos = sui_panel_get_header_text_position(&suiPanel);
    gSuiBackend.DrawText(suiPanel.Label->Buffer, suiPanel.Label->Length, 1, textPos, v3_new(0,0,0), hdrSize, suiPanelHdrLabelColor);

#define DEBUG_TEXT_MARGIN 1
#if DEBUG_TEXT_MARGIN == 2
    v2 labelSize = sui_get_text_metrics_v2(*suiPanel.Label);
    v2_print(labelSize);
    gSuiBackend.DrawRect(v3_v2v(textPos.XY, 0.0f), labelSize, v4_new(1,1,1,1));
#endif

    // DOCS(typedef): Draw close button
    //v2_print(headerSize);
    v2 clsBtnSize = sui_panel_get_close_button_size(hdrSize);
    v3 clsBtnPos = sui_panel_get_close_button_position(&suiPanel, clsBtnSize);
    //v2_print(clsBtnSize);
    //v3_print(clsBtnPos);
    v4 clsBtnColor = sui_style_get_color_by_state(
        pSuiStyle,
        SuiStyleItemType_Panel_Header_Close_Button,
        suiPanel.CloseButtonState);
    gSuiBackend.DrawCircle(clsBtnPos, clsBtnSize, clsBtnColor, 1.5f, 0.2f, 16);

    //v2_print(clsBtnMinSize);
    v4 clsBtnXColor = sui_style_get_color_by_state(
        pSuiStyle,
        SuiStyleItemType_Panel_Header_Close_Button_X,
        suiPanel.CloseButtonState);
    v2 clsBtnXSize =
        pSuiStyle->Items[SuiStyleItemType_Panel_Header_Close_Button_X]
        .Size.Min;
    f32 sx, sy, ex, ey, zv;
    sui_panel_get_close_button_x_coordinates(
        clsBtnSize, clsBtnPos, clsBtnXSize,
        &sx, &sy, &ex, &ey, &zv);

    gSuiBackend.DrawLine(v3_new(sx, sy, zv),
                         v3_new(ex, ey, zv),
                         2.0f,
                         clsBtnXColor);
    gSuiBackend.DrawLine(v3_new(sx, ey, zv),
                         v3_new(ex, sy, zv),
                         2.0f,
                         clsBtnXColor);
}

void
sui_backend_draw_panel_scroll_y(SuiStyle* pSuiStyle, SuiPanel* pSuiPanel)
{
    if (pSuiPanel->FullScrollYState == SuiState_None)
        return;

    SuiStyleItem scrollStyle = pSuiStyle->Items[SuiStyleItemType_Panel_Scroll_Y];
    SuiStyleItem scrollBtnStyle = pSuiStyle->Items[SuiStyleItemType_Panel_Scroll_Button_Y];

    v4 scrollColor = sui_style_get_color_by_state(pSuiStyle, SuiStyleItemType_Panel_Scroll_Y, pSuiPanel->ScrollYState);
    v3 scrollPosition = sui_panel_get_y_scroll_position(pSuiPanel);
    v2 scrollSize = sui_panel_get_y_scroll_size(pSuiPanel);
    gSuiBackend.DrawRect(scrollPosition, scrollSize, scrollColor);

    v4 scrollBtnColor = sui_style_get_color_by_state(
        pSuiStyle,
        SuiStyleItemType_Panel_Scroll_Button_Y,
        pSuiPanel->ScrollYState);
    v3 scrollBtnPos = sui_panel_get_y_scroll_button_position(pSuiPanel);
    v2 scrollBtnSize = sui_panel_get_y_scroll_button_size(pSuiPanel);
    gSuiBackend.DrawRect(scrollBtnPos, scrollBtnSize, scrollBtnColor);

}

void
sui_backend_draw_panel_scroll_x(SuiStyle* pSuiStyle, SuiPanel* pSuiPanel)
{
    if (pSuiPanel->ScrollXState == SuiState_None &&
        !sui_is_item_hovered(sui_panel_get_x_scroll_position(pSuiPanel), sui_panel_get_x_scroll_size(pSuiPanel)))
    {
        //return;
    }

    SuiStyleItem scrollStyle = pSuiStyle->Items[SuiStyleItemType_Panel_Scroll_X];
    SuiStyleItem scrollBtnStyle = pSuiStyle->Items[SuiStyleItemType_Panel_Scroll_Button_X];

    v4 scrollColor = sui_style_get_color_by_state(pSuiStyle, SuiStyleItemType_Panel_Scroll_X, pSuiPanel->ScrollXState);
    v3 scrollPos = sui_panel_get_x_scroll_position(pSuiPanel);
    v2 scrollSize = sui_panel_get_x_scroll_size(pSuiPanel);
    gSuiBackend.DrawRect(scrollPos, scrollSize, scrollColor);
    //v3_print(scrollPos);
    //v2_print(scrollSize);

    v4 scrollBtnColor = sui_style_get_color_by_state(
        pSuiStyle,
        SuiStyleItemType_Panel_Scroll_Button_X,
        pSuiPanel->ScrollXState);
    v3 scrollBtnPos = sui_panel_get_x_scroll_button_position(pSuiPanel);
    v2 scrollBtnSize = sui_panel_get_x_scroll_button_size(pSuiPanel);
    gSuiBackend.DrawRect(scrollBtnPos, scrollBtnSize, scrollBtnColor);
}

void
sui_backend_draw_widget_button(SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget btn)
{
    v4 color = sui_style_get_color_by_state(
        pSuiStyle, SuiStyleItemType_Widget_Button, btn.State);
    v2 margin = pSuiStyle->Items[SuiStyleItemType_Widget_Button].Offset.Margin;
    gSuiBackend.DrawRect(btn.Position, btn.Size, color);

#if 0
    v3 textPos = sui_get_text_position_inside(btn.Label, btn.Position, btn.Size, margin);
#else
    v3 textPos = sui_align_get_text_position(btn.Label, btn.Position, btn.Size, SuiVerticalAlignType_Center, SuiHorizontalAlignType_Center, 0.0f);
#endif
    gSuiBackend.DrawText(btn.Label->Buffer, btn.Label->Length, 1.0f, textPos, v3_new(0,0,0), v2_new(1000, 1000),	v4_new(1, 1, 1, gDebugConfig.IsFontDebug));
}

void
sui_backend_draw_widget_slider_f32(SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget sliderF32)
{
    v4 color = sui_style_get_color_by_state(
        pSuiStyle, SuiStyleItemType_Widget_Slider_F32, sliderF32.SubState);
    gSuiBackend.DrawRect(sliderF32.Position, sliderF32.Size, color);

    v2 minMax = v2_new(sliderF32.FloatValues.Y, sliderF32.FloatValues.Z);
    SuiStyleItem sliderStyle =
        pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32];
    SuiStyleItem slideStyle =
        pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32_Slider];
    f32 value = MinMaxV2(sliderF32.FloatValues.X, minMax);
    v2 size = sliderStyle.Size.Min;
    f32 offsetX = sui_slider_get_slide_offset_x(value, minMax);
    v3 slidePosition = sui_slider_get_slide_position(sliderF32.Position, offsetX);
    v2 slideSize = sui_slider_get_slide_size(size);
    v4 slideColor = sui_style_get_color_by_state(
        pSuiStyle, SuiStyleItemType_Widget_Slider_F32_Slider, sliderF32.State);
    gSuiBackend.DrawRect(slidePosition, slideSize, slideColor);

    v3 pos = v3_new(sliderF32.Position.X + sliderF32.Size.X, sliderF32.Position.Y, sliderF32.Position.Z);
    v3 textPos = sui_align_get_text_position(sliderF32.TempBuffer, pos, sliderF32.Size, SuiVerticalAlignType_Center, SuiHorizontalAlignType_None,   0.0f);
    gSuiBackend.DrawText(sliderF32.TempBuffer->Buffer, sliderF32.TempBuffer->Length, 1.0f, textPos, v3_new(0,0,0), v2_new(1000, 1000), v4_new(1, 1, 1, gDebugConfig.IsFontDebug));
}

void
sui_backend_draw_widget_checkbox(SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget checkbox)
{
    SuiStyleItem styleItem =
        pSuiStyle->Items[SuiStyleItemType_Widget_Checkbox];

    v4 color = sui_style_get_color_by_state(
        pSuiStyle, SuiStyleItemType_Widget_Checkbox, checkbox.State);
    gSuiBackend.DrawRect(checkbox.Position, checkbox.Size, styleItem.Color.Background);

    if (checkbox.Flag0 == 1)
    {
        v3 checkPos;
        v2 checkSize;
        sui_checkbox_get_metrics(checkbox.Position,
                                 checkbox.Size,
                                 &checkPos, &checkSize);
        gSuiBackend.DrawRect(checkPos, checkSize, styleItem.Color.Clicked);
    }

    v3 pos = v3_new(checkbox.Position.X + checkbox.Size.X, checkbox.Position.Y, checkbox.Position.Z);
    v3 textPos =
        sui_align_get_text_position(
            checkbox.Label,
            pos, checkbox.Size,
            SuiVerticalAlignType_Center, SuiHorizontalAlignType_None,
            0.0f);
    gSuiBackend.DrawText(checkbox.Label->Buffer, checkbox.Label->Length,1.0f,textPos, v3_new(0,0,0), v2_new(1000, 1000),v4_new(1, 1, 1, gDebugConfig.IsFontDebug));
}

void
sui_backend_draw_widget_text(SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget text)
{
    gSuiBackend.DrawText(text.TempBuffer->Buffer, text.TempBuffer->Length, 1.0f,v3_v2v(text.Position.XY, text.Position.Z - 0.3f),v3_new(0,0,0),v2_new(1000, 1000),v4_new(1, 1, 1, gDebugConfig.IsFontDebug));
}

void
sui_backend_draw_widget_input_string(SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget input)
{
    SuiStyleItem styleItem =
        pSuiStyle->Items[SuiStyleItemType_Widget_Input_String];

    v4 color = sui_style_get_color_by_state(
        pSuiStyle, SuiStyleItemType_Widget_Input_String, input.State);
    gSuiBackend.DrawRect(input.Position, input.Size, styleItem.Color.Background);

    SuiInputData* pSuiInputData = (&sui_get_instance()->InputData);
    const i32 MAGIC = 19;
    float marginX = 5.0f;
    i32 diffInd = 0;
    if (pSuiInputData->TextIndexPosition > MAGIC)
    {
        diffInd = pSuiInputData->TextIndexPosition - MAGIC;
    }

    if (pSuiInputData->FocusID == input.Id)
    {
        vguard(pSuiInputData->TextIndexPosition >= 0);

        f32 w, h;
        sui_get_text_metrics_ext(input.TempBuffer->Buffer, Min(pSuiInputData->TextIndexPosition, MAGIC), &w, &h);

        wchar indCh =
            input.TempBuffer->Buffer[pSuiInputData->TextIndexPosition];
        f32 xoffset = 0.0f;
        if (pSuiInputData->TextIndexPosition > 0)
            xoffset = sui_get_char_xoffset(indCh) - 1.0f;
        //GINFO("Char: %lc %f\n", indCh, xoffset);
        v3 inputPos = v3_new(input.Position.X + w + xoffset + marginX, input.Position.Y, input.Position.Z - 0.4f);

        gSuiBackend.DrawRect(inputPos, v2_new(2, input.Size.Y), v4_new(1, 0, 0, 1));

    }

    //GINFO("Length: %d\n", input.TempBuffer.Length);

    gSuiBackend.DrawText(input.TempBuffer->Buffer + diffInd, input.TempBuffer->Length, 1.0f, sui_input_get_text_position(input.TempBuffer, input.Position, input.Size), v3_new(0,0,0), input.Size, v4_new(1, 1, 1, gDebugConfig.IsFontDebug));
}

void
sui_backend_draw_widget_dropdown(SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget dropdown)
{
    SuiStyleItem styleItem =
        pSuiStyle->Items[SuiStyleItemType_Widget_Dropdown];

    v4 color = sui_style_get_color_by_state(
        pSuiStyle, SuiStyleItemType_Widget_Dropdown, dropdown.State);

    gSuiBackend.DrawRect(dropdown.Position, dropdown.Size, color);

    // NOTE: Arrow part
    f32 arrowXMargin = 10.0f;
    f32 arrowYMargin = 0.25f;
    v2 arrowSize = v2_mul(v2_new(0.07, 0.5), dropdown.Size);

    SuiDropDown* pSuiDropDown = (SuiDropDown*) dropdown.Data;
    v3 v0Pos, v1Pos, v2Pos;
    if (pSuiDropDown->IsOpened)
    {
        v0Pos = v3_new(
            dropdown.Position.X - arrowXMargin + dropdown.Size.X - arrowSize.X,
            dropdown.Position.Y - arrowYMargin * dropdown.Size.Y,
            dropdown.Position.Z - 0.1f);
        v1Pos = v3_new(
            v0Pos.X + arrowSize.X,
            v0Pos.Y,
            v0Pos.Z);
        v2Pos = v3_new(
            v0Pos.X + arrowSize.X / 2,
            v0Pos.Y - arrowSize.Y,
            v0Pos.Z);


    }
    else
    {
        v0Pos = v3_new(
            dropdown.Position.X - arrowXMargin + dropdown.Size.X - arrowSize.X,
            dropdown.Position.Y - arrowYMargin * dropdown.Size.Y - arrowSize.Y,
            dropdown.Position.Z - 0.1f);
        v1Pos = v3_new(
            v0Pos.X + arrowSize.X,
            v0Pos.Y,
            v0Pos.Z);
        v2Pos = v3_new(
            v0Pos.X + arrowSize.X / 2,
            v0Pos.Y + arrowSize.Y,
            v0Pos.Z);
    }

    v3 poss[4] = { v0Pos, v1Pos, v2Pos, v2Pos };
    v2 uvs[4] = {};
    gSuiBackend.DrawItems(poss, uvs, 4, 6, 0, v4_new(1,1,1,1), 0);

    // DOCS: Drawing label
    v2 textPadding = v2_new(10, 0);
    if (pSuiDropDown->SelectedIndex == -1)
    {
        v3 textPos = sui_get_text_position_inside(dropdown.Label, dropdown.Position, dropdown.Size, textPadding);
        gSuiBackend.DrawText(dropdown.Label->Buffer, dropdown.Label->Length, 1, textPos, v3_new(0,0,0), v2_new(1000,1000), v4_new(1,1,1,0));
    }
    else
    {
        WideString opt = pSuiDropDown->SelectOptions[pSuiDropDown->SelectedIndex];
        v3 textPos = sui_get_text_position_inside(&opt, dropdown.Position, dropdown.Size, textPadding);
        gSuiBackend.DrawText(opt.Buffer, opt.Length, 1, textPos, v3_new(0,0,0), v2_new(1000,1000), v4_new(1,1,1,0));
    }

    // DOCS: Drawing items
    if (pSuiDropDown->IsOpened)
    {
        v3 pos = v3_sub_y(dropdown.Position, dropdown.Size.Height);
        pos.Z = sui_panel_get_header_position(&suiPanel).Z - 0.1;
        for (i32 i = 0; i < array_count(pSuiDropDown->SelectOptions); ++i)
        {
            WideString str = pSuiDropDown->SelectOptions[i];
            GLOG("height: %d\n", pSuiDropDown->ItemHeight);
            v2 itemSize = v2_new(dropdown.Size.Width, pSuiDropDown->ItemHeight);
            v4 itemColor = color;
            if (pSuiDropDown->ActiveIndex == i)
            {
                itemColor = sui_style_get_color_by_state(
                    pSuiStyle,
                    SuiStyleItemType_Widget_Dropdown,
                    SuiState_Hovered);
            }

            gSuiBackend.DrawRect(pos, itemSize, itemColor);

            v3 itemPos = sui_get_text_position_inside(&str, pos, itemSize, textPadding);
            gSuiBackend.DrawText(str.Buffer, str.Length, 1, itemPos, v3_new(0,0,0), v2_new(1000, 1000), v4_new(1,1,1,0));

            pos = v3_sub_y(pos, pSuiDropDown->ItemHeight);
        }

        v2 borderSize = v2_add_y(dropdown.Size, pSuiDropDown->ItemHeight * array_count(pSuiDropDown->SelectOptions));
        gSuiBackend.DrawEmptyRect(v3_sub_z(dropdown.Position, 1.3f), borderSize, 1.0f, v4_new(1,1,1,1));
    }

}

void
sui_backend_draw_widget_image(SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget image)
{
    gSuiBackend.DrawImage(image.Position, image.Size, image.FloatValues, image.TextureId);
}

void
sui_backend_draw_widget_collapse_header(SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget collapseHdr)
{
    SuiStyleItem styleItem =
        pSuiStyle->Items[SuiStyleItemType_Widget_CollapseHeader];
    SuiStyleItem txtStyleItem =
        pSuiStyle->Items[SuiStyleItemType_Widget_CollapseHeader_Text];

    v4 color = sui_style_get_color_by_state(
        pSuiStyle, SuiStyleItemType_Widget_CollapseHeader, collapseHdr.State);

    gSuiBackend.DrawRect(collapseHdr.Position, collapseHdr.Size, color);

    v2 textMargin = v2_new(0,0);
    v3 textPos = sui_get_text_position_inside(collapseHdr.Label, collapseHdr.Position, collapseHdr.Size, textMargin);
    gSuiBackend.DrawText(collapseHdr.Label->Buffer, collapseHdr.Label->Length, 1, textPos, v3_new(0,0,0), v2_new(1000, 1000), txtStyleItem.Color.Background);
}

void
sui_backend_draw_widgets(SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget suiWidget)
{
    switch (suiWidget.Type)
    {
    case SuiWidgetType_Button:
        if (gDebugConfig.IsButtonDrawn)
            sui_backend_draw_widget_button(pSuiStyle, suiPanel, suiWidget);
        break;

    case SuiWidgetType_Slider_F32:
        if (gDebugConfig.IsSliderDrawn)
            sui_backend_draw_widget_slider_f32(pSuiStyle, suiPanel, suiWidget);
        break;

    case SuiWidgetType_Checkbox:
        if (gDebugConfig.IsCheckboxDrawn)
            sui_backend_draw_widget_checkbox(pSuiStyle, suiPanel, suiWidget);
        break;

    case SuiWidgetType_Text:
        if (gDebugConfig.IsTextDrawn)
            sui_backend_draw_widget_text(pSuiStyle, suiPanel, suiWidget);
        break;

    case SuiWidgetType_InputString:
        //sui_backend_draw_widget_input_string(renderer, pSuiStyle, suiPanel, suiWidget);
        break;

    case SuiWidgetType_Dropdown:
        sui_backend_draw_widget_dropdown(pSuiStyle, suiPanel, suiWidget);

    case SuiWidgetType_Image:
        if (gDebugConfig.IsImageDrawn)
            sui_backend_draw_widget_image(pSuiStyle, suiPanel, suiWidget);
        break;

    case SuiWidgetType_CollapseHeader:
        if (gDebugConfig.IsCollapseHeaderDebug)
            sui_backend_draw_widget_collapse_header(pSuiStyle, suiPanel, suiWidget);
        break;

    default:
        GERROR("No backend for widget of type: %d\n", suiWidget.Type);
        vguard(0);
        break;
    }
}

void
sui_panel_get_drawable_area(SuiPanel* pPanel, v2* pMin, v2* pMax)
{
    v2 dSize = sui_get_instance()->Monitor.DisplaySize;
    SuiStyleItem borderStyle = GetStyle()->Items[SuiStyleItemType_Panel_Border];

    v2 xy = pPanel->Position.XY;
    xy = v2_sub_yr(xy, dSize.Height);

    *pMin = v2_add(xy, v2_new(-borderStyle.Size.Min.X, 0.0f));
    *pMax = v2_add(xy, v2_add(pPanel->Size, borderStyle.Size.Min));
}

void
sui_backend_draw_panel(SuiPanel suiPanel, i32 isActive)
{
    SuiStyle* pSuiStyle = sui_get_style();
    SuiStyleItem panelStyle = pSuiStyle->Items[SuiStyleItemType_Panel];
    SuiStyleItem borderStyle = pSuiStyle->Items[SuiStyleItemType_Panel_Border];

    // NOTE Panels rectangle if is equal to full panelSize
    {
        v2 dSize = sui_get_instance()->Monitor.DisplaySize;
        /*
          DOCS(typedef):
          * v2_sub_yr() -> reversing y because in shader we have different coord system (flipped on Y-axis)
          * substract border size
          */
        //v2 min = v2_add(v2_sub_yr(suiPanel.Position.XY, dSize.Height), v2_new(-borderStyle.Size.Min.X, 0.0f));

        v2 min, max;
        sui_panel_get_drawable_area(&suiPanel, &min, &max);
        gSuiBackend.SetVisibleRect(min, max);
        //gSuiBackend.DrawRect(suiPanel.Position, suiPanel.Size, v4_new(1,1,1,1));

    }

    // DOCS(typedef): Draw panel window
    v4 suiPanelColor = sui_style_get_color_by_state(pSuiStyle, SuiStyleItemType_Panel, suiPanel.State);
    gSuiBackend.DrawRect(suiPanel.Position, suiPanel.Size, suiPanelColor);

    // DOCS(typedef): Draw panel border
    v3 borderPos;
    v2 borderSize;
    f32 borderThickness;
    v4 borderColor;
    sui_panel_border_get_all(&suiPanel, isActive, &borderPos, &borderSize, &borderThickness, &borderColor);
    gSuiBackend.DrawEmptyRect(borderPos, borderSize, borderThickness, borderColor);

    // DOCS(typedef): Draw header
    if ((suiPanel.Flags & SuiPanelFlags_WithOutHeader) == 0)
        sui_backend_draw_panel_header(pSuiStyle, suiPanel);

    // DOCS(typedef): Draw widgets
    DO_ONES(GINFO("Widgets Cnt: %d\n", array_count(suiPanel.aWidgets)););
    v4 btnColor = v4_new(0.1, 0.253, 0.134, 1);

    v2 panelOverflow = v2_sub(suiPanel.NeededSize, suiPanel.Size);
    if (panelOverflow.X >= 0.0f && (suiPanel.Flags & SuiPanelFlags_Scrollable_X))
    {
        // note: not working right now
        //sui_backend_draw_panel_scroll_x(pSuiStyle, &suiPanel);
    }
    if (panelOverflow.Y >= 0.0f && (suiPanel.Flags & SuiPanelFlags_Scrollable_Y))
    {
        sui_backend_draw_panel_scroll_y(pSuiStyle, &suiPanel);
    }

    // NOTE: Widgets have different visible rectangle,
    // note: why: for rendering hdr, scroll-bar, etc
    {
        v2 dSize = sui_get_instance()->Monitor.DisplaySize;
        v2 min = v2_add(v2_sub_yr(suiPanel.Position.XY, dSize.Height), v2_new(-borderStyle.Size.Min.X, 0.0f));

        v2 hdrSize = sui_panel_get_header_size(&suiPanel);
        if ((suiPanel.Flags & SuiPanelFlags_WithOutHeader) == 0)
        {
            min = v2_new(min.X, min.Y + hdrSize.Height);
        }

        v2 max;
        if ((suiPanel.Flags & SuiPanelFlags_Scrollable) && suiPanel.ScrollYState != SuiState_None)
        {
            max = v2_add(min, v2_sub_x(suiPanel.Size, sui_panel_get_y_scroll_size(&suiPanel).Width));
        }
        else
        {
            max = v2_add(min, suiPanel.Size);
        }

        if ((suiPanel.Flags & SuiPanelFlags_WithOutHeader) == 0)
        {
            max = v2_new(max.X, max.Y - hdrSize.Height);
        }

        gSuiBackend.SetVisibleRect(min, max);
    }

    // DOCS(typedef): Make scrollable area if overlow exist
    SuiStyleItem verticalStackItem =
        pSuiStyle->Items[SuiStyleItemType_LayoutType_VerticalStack];
    v2 layoutPad = verticalStackItem.Offset.Margin;
    v2 headerSize = sui_panel_get_header_size(&suiPanel);

    i32 breaked = 0;
    f32 fullHeight = headerSize.Y;
    // GINFO("Widgets Cnt: %ld\n", array_count(suiPanel.aWidgets));
    i64 widgetCount = array_count(suiPanel.aWidgets);
    for (i64 w = 0; w < widgetCount; ++w)
    {
        SuiWidget suiWidget = suiPanel.aWidgets[w];
        fullHeight += suiWidget.Size.Height + layoutPad.Y;

        if (0 && fullHeight > panelStyle.Size.Max.Height)
        {
            breaked = 1;
            break;
        }

        sui_backend_draw_widgets(pSuiStyle, suiPanel, suiWidget);
    }

}

void
_sui_backend_process_some_stuff()
{
    SuiInputData* pInput = GetInputData();
    /* SuiKeys_LeftArrow, */
    /* SuiKeys_RightArrow, */
    /* SuiKeys_UpArrow, */
    /* SuiKeys_DownArrow, */

    i32 isLeft = (pInput->KeyData[SuiKeys_LeftArrow].State == SuiKeyState_Pressed);
    i32 isRight = (pInput->KeyData[SuiKeys_RightArrow].State == SuiKeyState_Pressed);
    i32 isUp = (pInput->KeyData[SuiKeys_UpArrow].State == SuiKeyState_Pressed);
    i32 isDown = (pInput->KeyData[SuiKeys_DownArrow].State == SuiKeyState_Pressed);


    SuiPanel* pCurrentPanel = sui_get_active_panel();
    if (pCurrentPanel == NULL)
    {
        GINFO("Current panel is NULL!\n");
        return;
    }

    const f32 key_step = 100.0f;

    if (isLeft)
    {
        pCurrentPanel->Position.X -= key_step;
    }
    else if (isRight)
    {
        pCurrentPanel->Position.X += key_step;
    }

    if (isUp)
    {
        pCurrentPanel->Position.Y += key_step;
    }
    else if (isDown)
    {
        pCurrentPanel->Position.Y -= key_step;
    }
}

void
sui_backend_draw_ui()
{
    SuiDrawData* drawData = sui_get_draw_data();
    SuiInstance* suiInstance = sui_get_instance();
    //SuiLayoutData* suiLayoutData = sui_get_layout_data();

    SuiPanelKeyValue* panelsTable = suiInstance->PrevFrameCache.PanelsTable;
    i64 openedPanelsCount = 0; //;

    i64 i, count = array_count(suiInstance->PrevFrameCache.Panels);
    for (i = 0; i < count; ++i)
    {
        i32 ind = suiInstance->PrevFrameCache.Panels[i];
        SuiPanelKeyValue keyVal = panelsTable[ind];
        if (keyVal.Value.PanelState == SuiPanelState_Opened)
        {
            ++openedPanelsCount;
        }
    }

    //GINFO("PanelsCount: %ld\n", panelsCount);
    //gSuiBackend.DrawRect(v3_new(0, 0, 10000), v2_new(100, 100), v4_new(1, 0, 0, 1));

    if (openedPanelsCount <= 0)
    {
        //DOCS(typedef): Clear all submited data with that
        gSuiBackend.DrawRect(v3_new(0, 0, 10000), v2_new(100,100), v4_new(0,0,0,0));
    }
    else
    {
        i64 panelsCount = array_count(suiInstance->PrevFrameCache.Panels);
        for (i32 p = 0; p < panelsCount; ++p)
        {
            SuiPanel suiPanel =
                sui_panel_get_by_i_value(p);

            if (suiPanel.PanelState == SuiPanelState_Closed)
            {
                continue;
            }

#if 0
            wchar helperBuf[256] = {};
            memcpy(helperBuf, &suiPanel.Label->Buffer, MinMax(suiPanel.Label->Length, 0, 256));
            GERROR("Panel Label: %ls Z: %f\n", suiPanel.Label->Buffer, suiPanel.Position.X, suiPanel.Position.Y, suiPanel.Position.Z);
#endif

            sui_backend_draw_panel(suiPanel, (p == 0));
        }
    }

    gSuiBackend.Flush();

    _sui_backend_process_some_stuff();
}
