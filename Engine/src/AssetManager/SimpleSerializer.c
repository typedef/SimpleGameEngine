#include "SimpleSerializer.h"
#include "AssetManager/AssetManager.h"

#include <Core/SimpleStandardLibrary.h>
#include <Model/StaticModel.h>
#include <Utils/SimpleFormats.h>

void*
string_serialize(void* buffer, const char* pString)
{
    i64 len = string_length(pString);
    memcpy(buffer, &len, sizeof(i64));
    buffer += sizeof(i64);

    // DOCS(typedef): to make it cross-platform convert ws to utf-8
    size_t size = len * sizeof(char);
    memcpy(buffer, pString, size);
    buffer = buffer + size;

    return buffer;
}

void*
string_deserialize(void* buffer, char** ppString, i64* pLength)
{
    void* pRead = buffer;
    i64 len = *((i64*)pRead);
    pRead += sizeof(i64);

    char* str = string_copy(pRead, len);
    pRead = pRead + len * sizeof(char);

    *ppString = str;

    return pRead;
}

void*
wide_string_serialize(void* buffer, WideString ws)
{
    // NOTE(typedef): locale should be utf8
    memcpy(buffer, &ws.Length, sizeof(size_t));
    buffer += sizeof(size_t);

    // DOCS(typedef): to make it cross-platform convert ws to utf-8
    char* utf8 = wchar_as_char(ws.Buffer, ws.Length * sizeof(wchar));
    size_t elSize = wide_string_utf8_get_char_size(utf8);
    size_t len = wide_string_utf8_length(utf8);
    size_t size = len * elSize;
    memcpy(buffer, utf8, size);
    buffer = buffer + size;

    return buffer;
}

void*
wide_string_deserialize(void* buffer, WideString* pWs)
{
    size_t len = *((size_t*)buffer);
    buffer += sizeof(size_t);

    size_t utfLen = wide_string_utf8_length(buffer);
    size_t utfElSize = wide_string_utf8_get_char_size(buffer);
    size_t wholeSize = utfLen * utfElSize;
    string_copy(buffer, wholeSize);
    WideString ws = wide_string_utf8(buffer);
    *pWs = ws;
    buffer += wholeSize;

    return buffer;
}

void
simple_binary_static_model_serialize(StaticModel* pModel, void** ppSerializedBytes, size_t* pSize)
{
    // TODO revisit it later

    // DOCS calc size
    //                ModelType           Id            PathLength   NamelLength
    size_t baseSize = sizeof(ModelType) + sizeof(i64) + sizeof(i64) + sizeof(i64);
    //                IsVisible    MaterialId    NameLength    IndicesCount
    size_t meshHdr  = sizeof(i8) + sizeof(i64) + sizeof(i64) + sizeof(i64);

    size_t arraySizes = 0;
    array_foreach(pModel->aMeshes,
                  arraySizes += array_count(item.Indices) + array_count(item.Vertices));

    i64 meshesCount = array_count(pModel->aMeshes);
    size_t fullSize = baseSize + sizeof(i64) + meshesCount * meshHdr + arraySizes;

    // DOCS fill the buffer
    void* buffer = memory_allocate(fullSize);

    void* writePtr = buffer;
    memcpy(writePtr, &pModel->Base.Type, sizeof(ModelType));
    writePtr += sizeof(ModelType);
    memcpy(writePtr, &pModel->Base.Id, sizeof(i64));
    writePtr += sizeof(i64);
    writePtr = string_serialize(writePtr, pModel->Base.Path);
    writePtr = wide_string_serialize(writePtr, pModel->Base.Name);

    memcpy(writePtr, &meshesCount, sizeof(i64));
    writePtr += sizeof(i64);

    for (i32 i = 0; i < meshesCount; ++i)
    {
        StaticMesh mesh = pModel->aMeshes[i];

        memcpy(writePtr, &mesh.IsVisible, sizeof(i8));
        writePtr += sizeof(i8);

        memcpy(writePtr, &mesh.MaterialId, sizeof(i64));
        writePtr += sizeof(i64);

        writePtr = wide_string_serialize(writePtr, mesh.Name);

        i64 indicesCount = array_count(mesh.Indices);
        memcpy(writePtr, &indicesCount, sizeof(i64));
        size_t meshIndicesSize = indicesCount * sizeof(u32);
        memcpy(writePtr, mesh.Indices, meshIndicesSize);
        writePtr += meshIndicesSize;

        i64 verticesCount = array_count(mesh.Vertices);
        memcpy(writePtr, &verticesCount, sizeof(i64));
        size_t meshVerticesSize = verticesCount * sizeof(StaticMeshVertex);
        memcpy(writePtr, mesh.Vertices, meshVerticesSize);
        writePtr += meshVerticesSize;
    }

    *ppSerializedBytes = buffer;
    *pSize = fullSize;

}


void
simple_binary_static_model_deserialize(StaticModel* pModel, void* pSerializedBytes)
{
    StaticModel sModel = {};

    void* pRead = pSerializedBytes;
    sModel.Base.Type = *((ModelType*) pRead);
    pRead = pRead + sizeof(ModelType);

    sModel.Base.Id = *((i64*) pRead);
    pRead = pRead + sizeof(i64);

    // NOTE() Not sure we can use here compound literal
    pRead = string_deserialize(pRead, &sModel.Base.Path, &(i64){0});

    sModel.Base.Id = *((ModelType*) (pRead + sizeof(ModelType)));
    pRead = wide_string_deserialize(pRead, &sModel.Base.Name);

    i64 meshesCount = *((i64*)pRead);
    pRead += sizeof(i64);

    for (i32 m = 0; m < meshesCount; ++m)
    {
        StaticMesh mesh = {};
        mesh.IsVisible = *((i8*)pRead);
        pRead += sizeof(i8);

        mesh.MaterialId = *((i64*)pRead);
        pRead += sizeof(i64);

        pRead = wide_string_deserialize(pRead, &mesh.Name);

        i64 indicesCount = *((i64*)pRead);
        size_t indicesSize = indicesCount * sizeof(u32);
        pRead += sizeof(i64);
        mesh.Indices = memory_allocate(indicesSize);
        memcpy(mesh.Indices, pRead, indicesSize);
        pRead += indicesSize;

        i64 verticesCount = *((i64*)pRead);
        size_t verticesSize = verticesCount * sizeof(StaticMeshVertex);
        pRead += sizeof(i64);
        mesh.Vertices = memory_allocate(verticesSize);
        memcpy(mesh.Vertices, pRead, verticesSize);
        pRead += verticesSize;
    }

    *pModel = sModel;
}

i32
simple_binary_is_sbte(const char* path, char** pathToRead, char** newBinPath)
{
    const char* binaryDir = asset_texture("binary");

    char* nameWoExt = path_get_name_wo_extension(path);
    char* name = string_concat(nameWoExt, simple_formats_to_ext(SimpleFormatsType_SBTE));
    *newBinPath = path_combine(binaryDir, name);

    memory_free(nameWoExt);
    memory_free(name);

    if (path_is_file_exist(*newBinPath))
    {
        *pathToRead = *newBinPath;
        return 1;
    }
    else if (string_compare(path_get_extension(path), simple_formats_to_ext(SimpleFormatsType_SBTE)))
    {
        memory_free(newBinPath);
        *pathToRead = string(path);
        return 1;
    }

    *pathToRead = (char*) path;
    return 0;
}
