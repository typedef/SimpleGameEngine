#ifndef SIMPLE_SERIALIZER_H
#define SIMPLE_SERIALIZER_H

#include <Core/Types.h>

struct StaticModel;

void* string_serialize(void* buffer, const char* pString);
void* string_deserialize(void* buffer, char** ppString, i64* pLength);
void* wide_string_serialize(void* buffer, WideString ws);
void* wide_string_deserialize(void* buffer, WideString* pWs);
void simple_binary_static_model_serialize(struct StaticModel* model, void** ppSerializedBytes, size_t* pSize);
void simple_binary_static_model_deserialize(struct StaticModel* pModel, void* pSerializedBytes);

i32 simple_binary_is_sbte(const char* path, char** pathToRead, char** newBinPath);


#endif // SIMPLE_SERIALIZER_H
