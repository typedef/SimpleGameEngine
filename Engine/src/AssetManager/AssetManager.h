#ifndef ASSET_MANAGER_H
#define ASSET_MANAGER_H

#include "Model/DynamicModel.h"
#include <Core/Types.h>

#include <Model/StaticModel.h>
#include <Utils/Font.h>

typedef enum AssetGroup
{
    AssetGroup_Default = -1,
} AssetGroup;

typedef enum AssetType
{
    AssetType_None = 0,
    AssetType_Texture,
    AssetType_Texture_WithRect,
    AssetType_StaticModel,
    AssetType_StaticMesh,
    AssetType_DynamicModel,
    AssetType_GeneratedModel_Cube,
    AssetType_Material,
    AssetType_Font,
    AssetType_Count
} AssetType;

const char* asset_type_to_string(AssetType type);

typedef struct MapTable
{
    const char* Key;
    i64 Value;
} MapTable;

// TODO(bies): const char* -> WideString from the start
// GlobalPath is Application path, all path's in game is ascii friendly so,
// we don't need impliment WideStirng Path API here
typedef struct AssetManager
{
    i32 IsInitialized;
    i32 ID;
    AssetType Type;
    const char* Name;
    const char* GlobalPath;

    // NOTE: new one
    StaticModel* aStaticModels;
    MapTable* aModelsTable;

    VsaTexture* aTextures;
    MapTable* aTexturesTable;

    VsaFont* aFonts;
    MapTable* aFontsTable;

    MeshMaterial* aMaterials;
    MapTable* aMaterialsTable;

    DynamicModel* aDynamicModels;
    MapTable* aDynamicModelsTable;

    // TODO: DO not save it as raw data,
    // save metadata here about asset from disk,
    // real data (at least for models) handled inside ecs

} AssetManager;

void asset_manager_create();
void asset_manager_destroy();
StaticModel asset_manager_load_model(const char* diskPath);
DynamicModel asset_manager_load_dynamic_model(const char* diskPath);
i64 asset_manager_load_texture_id(const char* diskPath);
VsaTexture asset_manager_load_texture(const char* diskPath);
VsaTexture asset_manager_load_texture_by_id(i64 id);
VsaFont asset_manager_load_font(const char* diskPath);
i64 asset_manager_load_material_id(MeshMaterial* pMaterial);
MeshMaterial asset_manager_load_material_by_id(i64 matId);

VsaTexture asset_manager_get_texture(i64 textId);

/*
  #########################
  #DOCS(typedef): Easy Api#
  #########################
*/

/* void asset_manager_load_asset_file(AssetType type, const char* name); */
/* void asset_manager_unload_asset(Asset asset); */


#endif // ASSET_MANAGER_H
