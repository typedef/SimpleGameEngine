#include "AssetManager.h"
#include "Model/ModelBase.h"

#include <AssetManager/SimpleSerializer.h>
#include <Graphics/Vulkan/VulkanSimpleApiTypes.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Core/SimpleStandardLibrary.h>
#include <Utils/SimpleFormats.h>
#include <Utils/SimpleFormats.h>
#include <Utils/SimpleHelpers.h>
#include <Model/GeneratedModel.h>
#include <Model/StaticModel.h>
#include <Model/DynamicModel.h>
#include <Math/SimpleMath.h>

static AssetManager gAssetManager = {0};

#define AssetManagerGuard() vguard(gAssetManager.IsInitialized == 1 && "Asset Manager is not initialized!");

/* DOCS(typedef): BEGIN UTILS */
/* DOCS(typedef): END UTILS   */

const char*
asset_type_to_string(AssetType type)
{
    switch (type)
    {
    case AssetType_None: return "None";
    case AssetType_Texture: return "Texture";
    case AssetType_Texture_WithRect: return "Texture_WithRect";
    case AssetType_StaticModel: return "StaticModel";
    case AssetType_StaticMesh: return "StaticMesh";
    case AssetType_DynamicModel: return "DynamicModel";
    case AssetType_GeneratedModel_Cube: return "GeneratedModel_Cube";
    case AssetType_Material: return "Material";
    case AssetType_Font: return "Font";
    case AssetType_Count: return "Count";
    }

    vguard(0);
    return NULL;
}

void
_validate_directory(const char* dirName)
{
    char* assetDir = path_combine(path_get_current_directory(), dirName);
    if (path_is_directory_exist(assetDir))
        return;

    GERROR("Directory %s doesn't exist!\n", dirName);
    vguard(0 && "[AssetManager] directory doesn't exist!");
}

void
asset_manager_create()
{
    // note/todo: mb change assets -> Assets, hmm
    _validate_directory("assets");
    _validate_directory("Resources");

    char* assetDir = path_combine(path_get_current_directory(), "assets");
    if (!path_is_directory_exist(assetDir))
    {
        GERROR("Directory assets doesn't exist!\n");
        vguard(0 && "asssets directory doesn't exist!");
    }

    gAssetManager.IsInitialized = 1;

    array_reserve(gAssetManager.aStaticModels, 100);
    array_reserve(gAssetManager.aTextures, 100);
    array_reserve(gAssetManager.aMaterials, 100);
    array_reserve(gAssetManager.aFonts, 100);

    // DOCS(typedef): Loading default assets

    asset_manager_load_texture(resource_texture("white.png"));
    asset_manager_load_texture(resource_texture("white_texture.png"));

    MeshMaterial defaultMat = {
        .Name = string("Default Material"),
        .IsMetallicExist = 1,
        .Metallic = {
            .MetallicFactor = 0.0f,
            .RoughnessFactor = 0.0f,
            .BaseColor = v4_new(0.2, 0.2, 0.2, 1.0),
            .BaseColorTexture = -1,
            .MetallicRoughnessTexture = -1
        }
    };
    asset_manager_load_material_id(&defaultMat);

    asset_manager_load_font(resource_font("NotoSans.ttf"));
}

void
asset_manager_destroy()
{
    AssetManagerGuard();

    i64 modelsCnt = array_count(gAssetManager.aStaticModels);
    for (i64 i = 0; i < modelsCnt; ++i)
    {
        StaticModel* pModel = &gAssetManager.aStaticModels[i];
        static_model_destroy(pModel);
    }

    i64 texturesCnt = array_count(gAssetManager.aTextures);
    for (i64 i = 0; i < texturesCnt; ++i)
    {
        VsaTexture vsaTexture = gAssetManager.aTextures[i];
        vsa_texture_destroy(vsaTexture);
    }

    i64 fontsCnt = array_count(gAssetManager.aFonts);
    for (i64 i = 0; i < fontsCnt; ++i)
    {
        VsaFont vsaFont = gAssetManager.aFonts[i];
        vsa_font_info_destroy(vsaFont);
    }

    // NOTE: new
    array_free(gAssetManager.aStaticModels);
    table_free(gAssetManager.aModelsTable);

    array_free(gAssetManager.aTextures);
    table_free(gAssetManager.aTexturesTable);

    array_free(gAssetManager.aFonts);
    table_free(gAssetManager.aFontsTable);

    array_free(gAssetManager.aMaterials);
    table_free(gAssetManager.aMaterialsTable);

}

StaticModel
asset_manager_load_model(const char* diskPath)
{
    StaticModel sm = {};

    i64 ind = shash_geti(gAssetManager.aModelsTable, diskPath);
    if (ind == -1)
    {
        sm = static_model_load(diskPath);
        i64 ind = array_count(gAssetManager.aStaticModels);
        array_push(gAssetManager.aStaticModels, sm);
        shash_put(gAssetManager.aModelsTable, diskPath, ind);
    }

    ind = table_index(gAssetManager.aModelsTable);
    i64 id = gAssetManager.aModelsTable[ind].Value;
    sm = gAssetManager.aStaticModels[id];

    return sm;
}

DynamicModel
asset_manager_load_dynamic_model(const char* diskPath)
{
    DynamicModel dm = {};

    i64 ind = shash_geti(gAssetManager.aDynamicModelsTable, diskPath);
    if (ind == -1)
    {
        dm = dynamic_model_load(diskPath);
        i64 ind = array_count(gAssetManager.aDynamicModels);
        array_push(gAssetManager.aDynamicModels, dm);
        shash_put(gAssetManager.aDynamicModelsTable, diskPath, ind);
    }

    ind = table_index(gAssetManager.aDynamicModelsTable);
    i64 id = gAssetManager.aDynamicModelsTable[ind].Value;
    dm = gAssetManager.aDynamicModels[id];

    return dm;
}

i64
asset_manager_load_texture_id(const char* diskPath)
{
    i64 ind = shash_geti(gAssetManager.aTexturesTable, diskPath);
    if (ind == -1)
    {
        VsaTexture vt = vsa_texture_newf(diskPath);
        i64 ind = array_count(gAssetManager.aTextures);
        vt.Id = ind;
        array_push(gAssetManager.aTextures, vt);
        shash_put(gAssetManager.aTexturesTable, diskPath, ind);
    }

    ind = table_index(gAssetManager.aTexturesTable);
    i64 id = gAssetManager.aTexturesTable[ind].Value;
    return id;
}

VsaTexture
asset_manager_load_texture(const char* diskPath)
{
    VsaTexture vt = {};

    i64 id = asset_manager_load_texture_id(diskPath);
    vt = gAssetManager.aTextures[id];

    return vt;
}

VsaTexture
asset_manager_load_texture_by_id(i64 id)
{
    VsaTexture vt = gAssetManager.aTextures[id];
    return vt;
}

VsaFont
asset_manager_load_font(const char* diskPath)
{
    VsaFont vf = {};

    i64 ind = shash_geti(gAssetManager.aFontsTable, diskPath);
    if (ind == -1)
    {
        vf = vsa_font_info_create(diskPath, 32, 1);
        const char* texturePrefixPath = resource_texture("Generated_");
        char* nameWoExt = path_get_name_wo_extension(diskPath);
        char* finalTexturePath = string_concat3(texturePrefixPath, nameWoExt, ".png");

        VsaTexture vt =
            asset_manager_load_texture(finalTexturePath);
        vf.Atlas = vt;

        i64 id = array_count(gAssetManager.aFonts);
        array_push(gAssetManager.aFonts, vf);
        shash_put(gAssetManager.aFontsTable, diskPath, id);

        memory_free(nameWoExt);
        memory_free(finalTexturePath);
    }

    ind = table_index(gAssetManager.aFontsTable);
    i64 id = gAssetManager.aFontsTable[ind].Value;
    vf = gAssetManager.aFonts[id];
    return vf;
}

i64
asset_manager_load_material_id(MeshMaterial* pMaterial)
{
    vguard_not_null(pMaterial->Name);

    i64 ind = shash_geti(gAssetManager.aMaterialsTable, pMaterial->Name);
    if (ind == -1)
    {
        i64 id = array_count(gAssetManager.aMaterials);
        MeshMaterial value = *pMaterial;
        array_push(gAssetManager.aMaterials, value);
        shash_put(gAssetManager.aMaterialsTable, pMaterial->Name, id);
        return id;
    }

    ind = table_index(gAssetManager.aMaterialsTable);
    i64 id = gAssetManager.aMaterialsTable[ind].Value;
    return id;
}

MeshMaterial
asset_manager_load_material_by_id(i64 matId)
{
    MeshMaterial mat = gAssetManager.aMaterials[matId];
    return mat;
}

void
_validate_id(i64 id, i64 cnt)
{
    i32 isValid = ((id >= 0) && (id < cnt));
    if (!isValid)
    {
        GERROR("Id is not valid %d %d\n", id, cnt);
        vguard(isValid && "Id not valid!");
    }
}

#define ValidateId(id, arr)			\
    ({						\
        _validate_id(id, array_count(arr));	\
        arr[id];				\
    })

VsaTexture
asset_manager_get_texture(i64 textId)
{
    VsaTexture texture = ValidateId(textId, gAssetManager.aTextures);
    return texture;
}
