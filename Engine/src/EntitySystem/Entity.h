#ifndef ENTITY_H
#define ENTITY_H

#include <Core/Types.h>

typedef i32 EntityID;

typedef struct Entity
{
    EntityID ID;
    EntityID Parent;
    EntityID* Childs;
} Entity;

Entity entity_create(EntityID id, EntityID parent, EntityID* childs);

#endif
