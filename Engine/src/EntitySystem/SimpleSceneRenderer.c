#include "SimpleSceneRenderer.h"

#include <AssetManager/AssetManager.h>
#include <Graphics/Camera.h>
#include <Graphics/Vulkan/VsrCompositor.h>
#include <Graphics/Vulkan/Vsr2dCompositor.h>
#include <Graphics/Vulkan/Vsr3dCompositor.h>
#include <Graphics/Vulkan/Vsr2d.h>
#include <Graphics/Vulkan/Vsr3d.h>
#include <Editor/EditorViewport.h>
#include <Math/SimpleMath.h>
#include <UI/SuifBackend.h>
#include <Model/StaticModel.h>

#include "Components/Base/CameraComponent.h"
#include "Components/Base/TransformComponent.h"
#include "Components/3D/StaticModelComponent.h"


static Vsr3dCompositor* gpCompositor3D = NULL;
struct EditorViewport gEditorViewport = {};


SimpleSceneRenderer
simple_scene_renderer_new(SimpleSceneRendererSettings* pSettings)
{
    vguard_not_null(pSettings->pCamera);

    Camera* pCamera = pSettings->pCamera;

    Vsr3dSSetting vsr3dsSet = {
	.MaxVertices = I32M(1,000,000),
	.MaxInstances = 200,
	.MaxMaterialsCount = 200,
	.MaxTexture = 200,
    };
    Vsr3dDSetting vsr3ddSet = {
	.MaxVertices = I32T(100,000),
	.MaxInstances = 20,
	.MaxMaterialsCount = 20,
	.MaxTexture = 20,
    };
    Vsr3dCompositor comp = {};
    vsr_3d_compositor_create(&comp, &vsr3dsSet, &vsr3ddSet, pCamera);
    Vsr3dCompositor* pRenderCompositor =
	vsr_compositor_register("Compositor3d", &comp, sizeof(Vsr3dCompositor), (OnVsrDestroy) vsr_3d_compositor_destroy);
    gpCompositor3D = pRenderCompositor;

    Vsr2dCompositor* g2dCompositor = vsr_compositor_get("Compositor2d");
    Vsr2dSettings vsr2dSet = {
	.MaxVerticesCount = I32T(55, 000),
	.MaxInstancesCount = 7000,
	.MaxTexturesCount = 100,
	.pName = "Debug Renderer",
	.Font = asset_manager_load_font(resource_font("NotoSans.ttf")),
	.pCamera = pCamera
    };
    vsr_2d_compositor_create_world(g2dCompositor, &vsr2dSet);

    VsrVisibleRect vsrVisibleRect = {
	.Min = v2_new(0, 0),
	.Max = v2_new(2000, 2000),
    };
    vsr_2d_set_visible_rect(&g2dCompositor->WorldRender, vsrVisibleRect);

    SimpleSceneRenderer renderer = {
	.p2dCompositor = (void*) g2dCompositor,
	.p3dCompositor = (void*) gpCompositor3D,
    };

    return renderer;
}

void
simple_scene_renderer_destroy(SimpleSceneRenderer* pSimpleRenderer)
{
}

void
simple_scene_renderer_camera_updated(CameraComponent* pCamera)
{
    gpCompositor3D->pCamera = &pCamera->Base;
    vsr_3d_compositor_update_camera_ubo(gpCompositor3D);
}

void
simple_scene_renderer_add_static(SimpleSceneRenderer* pSimpleRenderer, TransformComponent* pTransform, StaticModelComponent* pModel)
{
    Vsr3dCompositor* pCompositor = (Vsr3dCompositor*) pSimpleRenderer->p3dCompositor;
    StaticModel* pStaticModel = (StaticModel*) &pModel->Model;

    vsr_3d_compositor_submit_model(pCompositor, pStaticModel, pTransform->Matrix);
}

void
simple_scene_renderer_flush_static(SimpleSceneRenderer* pSimpleRenderer)
{
    Vsr3dCompositor* pCompositor = (Vsr3dCompositor*) pSimpleRenderer->p3dCompositor;

    vsr_3d_compositor_flush(pCompositor);
}
