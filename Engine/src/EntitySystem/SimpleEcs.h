#ifndef SIMPLE_ECS_H
#define SIMPLE_ECS_H

#include <Core/Types.h>

/*garbase*/

/*
  DOCS(typedef):
  0 ... i64_max - valid id
  i64_min ... -1  - not valid id
  we can easily make id invalid by doing: valid_id * -1,
  this formula saves id prev value.
*/
typedef i64 ComponentId;
typedef i64 EntityId;

#define ComponentIdValid(id) ({ id >= EcsIdType_ComponentFirst && id <= EcsIdType_ComponentLast; })
#define ComponentIndexIdValid(ind) ({ ind >= 0 && ind <= (EcsIdType_ComponentLast - EcsIdType_ComponentFirst); })

/*
  NOTE(typedef): not the best validation, just cut some obvious
  wrong cases.
*/
#define EntityIdValid(id) ({ id >= EcsIdType_EntityFirst; })
#define EntityIndexIdValid(ind) ({ ind >= 0; })

typedef enum EcsIdType
{
    EcsIdType_InternalFirst = 0,
    EcsIdType_InternalLast = 511,
    EcsIdType_ComponentFirst = 512,
    EcsIdType_ComponentLast = 1024,
    EcsIdType_EntityFirst = 1025
} EcsIdType;

/*
  NOTE(typedef): this is array of components of one type.
  "TransformComponent|TransformComponent|TransformComponent|..."
  "ModelComponent|ModelComponent|ModelComponent|..."
*/
typedef struct ComponentRecord
{
    ComponentId Id;
    EntityId* Entities;
    size_t ComponentSize;
    void* Data;
} ComponentRecord;

/*
  NOTE(typedef):
  Links entity with component data.
  Id: If Id == -1 then entity deleted, deleted entities placed in Deleted,
  entityId can be checked for validity very fast:
     Records[entityId].Id != -1 ? Valid : Invalid.
  Data: IntHashTable for components
*/
typedef struct EntityRecord
{
    char* Name;
    EntityId Id;
    // todo: remove this, it should be global table for relation
    EntityId Parent;
    EntityId* Childs;
#define USE_BAD_OPTION 1
#if USE_BAD_OPTION == 1
    // NOTE(): Maybe we don't need this AT ALL, because query always gives us a components ids
    //NOTE/BUG(typedef): it is bad idea to have hash table here, it's atleast 63 elemens long, so we have huge amount of wasted data;
    struct { ComponentId Key; ComponentId Value; }* EntityData; //pointer to EntityRecord.Data

#else

    ComponentId* aComponentIds;

#endif
} EntityRecord;


/*
  NOTE(typedef):
  Deleted: when creating entities
  > if (EStorage.Deleted != NULL)
  >    new_entity_id = array_pop(EStorage.Deleted)

  EcsQueryResult queryResult = GetQuery(ModelComponent, TransformComponent);
  GetQuery - only for two or more components, for one component you should use
    ComponentRecord record = GetComponentRecord();
    SomeComponent* components = GetComponents();

  Examples:
  EcsQueryResult result;
  // O(N)
  for (i64 i = 0; i < array_count(world->EStorage.Records); ++i)
  {
     // O(difficult 1)
     i64 modelIndex = hash_geti(record.Data, ModelComponent);
     // O(difficult 1)
     i64 transformIndex = hash_geti(record.Data, TransformComponent);
     if (modelIndex != -1 && transformIndex != -1)
     {
       array_push(result.Data[i].Records, record.Data[modelIndex]);
       array_push(result.Data[i].Records, record.Data[transformIndex]);
     }
  }

  Optimize with ComponentStorage.ComponentRecord double linking:
  ComponentRecord will contain ComponentRecord.Entities, all entities that has
  this component.

  Example:
  EntityId* final_entities = NULL;
  ComponentId id = ids[i];
  ComponentRecord cRecord = world->CStorage.Record[GetComponentAsArrayIndex(id)];
  array_foreach(cRecord.Entities, array_push(final_entities, item););

  for (i64 i = 1; i < array_count(ids); ++i)
  {
     ComponentId id = ids[i];
     ComponentRecord cRecord = world->CStorage.Record[GetComponentAsArrayIndex(id)];
     array_foreach(cRecord.Entities, array_push(final_entities, item););

     ComponentId Id;
     size_t ComponentSize;
     size_t Count;
     size_t Capacity;
     void* Data;

  }
 */
typedef struct EntityStorage
{
    EntityId* Deleted;
    EntityRecord* Records; // Records[entity_id]
} EntityStorage;

typedef struct ComponentPair
{
    const char* Key;
    ComponentId Value;
} ComponentPair;

/*
  NOTE(typedef): all the data for entities component
*/
typedef struct ComponentStorage
{
    ComponentId LastId;
    ComponentPair* ComponentsToID;
    ComponentRecord* Records;
} ComponentStorage;

/*
  DOCS(typedef): EcsQuery will work as Archetype, it just give as
  custom archetype, ecs_get_query(TransformComponent, StaticModelComponent)
*/
typedef struct EcsQuery
{
    ComponentId* Ids;
    // BUG(typedef): bad code, better get EntityId* Entities;
    // guess we can deal with ids only, pointer can become invalid or smth
    EntityRecord* Entities;
} EcsQuery;

typedef struct EcsWorld
{
    u32 Id;
    i64 EntitiesCount;
    EntityStorage EStorage;
    ComponentStorage CStorage;
    EcsQuery* CachedQueries;
} EcsWorld;

struct CameraComponent;


/*
  DOCS(typedef):
  Here public api begins

  Some new things:
  OffsetArray - just array where original indices starts not from 0, but from some random value, then for using this array you should subtract (your_original_index - starting_point)

  Examples:
  This two things is/(should be) implemented as OffsetArray
  * EntityRecords* EntityToComponents; // to_get_0_id = entityId - EcsIdType_EntityFirst
  * ComponentRecord* Records;          // to_get_0_id = componentId - EcsIdType_ComponentFirst
*/

/*
  NOTE(typedef): simplified API
*/

#define GetEntityAsArrayIndex(entity) ({(entity) - EcsIdType_EntityFirst;})
#define GetEntityId(entity) ({(entity) + EcsIdType_EntityFirst;})
#define GetComponentAsArrayIndex(comp) ({(comp) - EcsIdType_ComponentFirst;})
#define GetComponentId(comp) ({(comp) + EcsIdType_ComponentFirst;})

#define GetComponent(entity, component)					\
    ({									\
        (component *) _ecs_world_entity_get_component(ecs_get_current_world(), entity, #component); \
    })
#define GetComponentExt(world, entity, component)			\
    ({									\
        (component *) _ecs_world_entity_get_component(world, entity, #component); \
    })
#define GetComponentById(entity, componentId)				\
    ({									\
        _ecs_world_entity_get_component_by_id(ecs_get_current_world(), entity, componentId); \
    })
#define GetComponentByIdExt(world, entity, componentId)	\
    ({							\
        _ecs_world_entity_get_component_by_id(world, entity, componentId); \
    })
#define GetComponentRecordData(component)		\
    ({							\
        (component*)					\
            _ecs_world_get_component_record_data(	\
                ecs_get_current_world(), #component);	\
    })
#define GetComponentRecord(component)					\
    ({ _ecs_world_get_component_record(ecs_get_current_world(), #component); })
#define HasComponent(entity, component)					\
    ({ _ecs_world_entity_has_component(ecs_get_current_world(), entity, #component); })
#define HasComponentExt(world, entity, component)			\
    ({ _ecs_world_entity_has_component(world, entity, #component); })
#define HasComponentById(entity, componentId)				\
    ({ _ecs_world_entity_has_component_by_id(ecs_get_current_world(), entity, componentId); })
#define HasComponentByIdExt(world, entity, componentId)			\
    ({ _ecs_world_entity_has_component_by_id(world, entity, componentId); })

#define ecs_world_create()			\
    _ecs_world_create()
#define ecs_world_destroy(world)		\
    _ecs_world_destroy(world)

/*
  NOTE(typedef): API with World
*/
#define ecs_world_register_component(world, component)	\
    _ecs_world_register_component(world, #component, sizeof(component))



#define ecs_world_entity_add_component_str(world, entity, compStr, data, size) \
    ({									\
        _ecs_world_entity_add_component_str(world,			\
                                            entity,			\
                                            compStr,			\
                                            &data,			\
                                            size);			\
    })
#define ecs_world_entity_add_component(world, entity, component, data)	\
    ({									\
        ecs_world_entity_add_component_str(world, entity, #component, data, sizeof(data)); \
    })

#define ecs_world_entity_remove_component(world, entity, component)	\
    _ecs_world_entity_remove_component(world, entity, #component)
#define ecs_world_get_component_record(world, component)	\
    _ecs_world_get_component_record(world, #component)
#define ecs_world_entity(world, name)		\
    _ecs_world_entity(world, name)
#define ecs_world_get_query(world, ...)			\
    _ecs_world_get_query(world, #__VA_ARGS__, 0)
#define ecs_world_get_query_w_cache(world, ...)		\
    _ecs_world_get_query(world, #__VA_ARGS__, 1)

/*
  NOTE(typedef): API without World
*/
#define ecs_register_component(component) \
    ecs_world_register_component(ecs_get_current_world(), component)
#define ecs_entity_add_component(entity, component, data)		\
    ecs_world_entity_add_component(ecs_get_current_world(), entity, component, data)
#define ecs_world_entity_remove_component(world, entity, component)	\
    _ecs_world_entity_remove_component(world, entity, #component)
#define ecs_entity(name)				\
    _ecs_world_entity(ecs_get_current_world(), name)
#define ecs_get_query(...)					\
    _ecs_world_get_query(ecs_get_current_world(), #__VA_ARGS__, 0)
#define ecs_get_query_w_cache(...)					\
    _ecs_world_get_query(ecs_get_current_world(), #__VA_ARGS__, 1)


/*
  NOTE(typedef):
  Helper function, adds additional functionality to Ecs
*/
EcsWorld* ecs_get_current_world();
void ecs_set_current_world(EcsWorld* world);
#define EcsComponentGetId(component) ({ ecs_component_get_id_by_name(#component); })
ComponentId ecs_component_get_id_by_name(const char* name);
ComponentId ecs_world_component_get_id_by_name(EcsWorld* world, const char* name);


/*
  NOTE(typedef):
  Internal functionality
*/
EcsWorld* _ecs_world_create();
void _ecs_world_destroy(EcsWorld* world);
i32 _ecs_world_register_component(EcsWorld* world, const char* componentName, size_t size);
void _ecs_world_entity_add_component_str(EcsWorld* pWorld, EntityId entity, const char* componentStr, void* data, size_t size);
i32 _ecs_world_entity_exist(EcsWorld* world, EntityId entity);
ComponentRecord* _ecs_world_entity_add_component(EcsWorld* world, EntityId entity, const char* componentName, ComponentId* componentId, EntityRecord** eRecordPtr);
void _ecs_world_entity_remove_component(EcsWorld* world, EntityId entity, const char* component);
ComponentRecord _ecs_world_get_component_record_by_id(EcsWorld* world, ComponentId componentId);
ComponentRecord _ecs_world_get_component_record(EcsWorld* world, const char* componentName);
void* _ecs_world_get_component_record_data(EcsWorld* world, const char* componentName);
void* _ecs_world_entity_get_component(EcsWorld* world, EntityId entity, const char* componentName);
void* _ecs_world_entity_get_component_by_id(EcsWorld* world, EntityId entity, ComponentId componentId);
i32 _ecs_world_entity_has_component(EcsWorld* world, EntityId entity, const char* component);
i32 _ecs_world_entity_has_component_by_id(EcsWorld* world, EntityId entity, ComponentId id);
i32 _ecs_world_entity_has_component_by_index_id(EcsWorld* world, EntityId entity, ComponentId componentAsIndex);
//TODO(typedef): refactor this
void _ecs_world_entity_set_component(EcsWorld* world, EntityId entity, const char* componentName, void* data);
void _ecs_world_entity_set_component_by_id(EcsWorld* world, EntityId entity, ComponentId componentId, void* data);

/*
  NOTE(typedef):
  _ecs_world_entity - returns just EntityId, because
  1. everything else { .Parent, .Childs } we don't need most of the time
  2. we can get it O(1) from OffsetArray
*/
EntityId _ecs_world_entity(EcsWorld* world, char* name);
EcsQuery _ecs_world_get_query(EcsWorld* world, const char* components, i32 needsToBeCached);

/*
  DOCS(typedef): Helpfull utils <-- stupid!!!
*/
struct CameraComponent* ecs_get_main_camera_component(EcsWorld* world);


/*
  NOTE(typedef):
  we can have one instance of EcsWorld:
  static EcsWorld CurrentWorld;

  then all api devides by 2 parts:
  1) Operates with current world:
     * ecs_add_component
  2)


*/


// rewrite all api with simple_ prefix refactor
EcsWorld* simple_ecs_world_create();


#endif // SIMPLE_ECS_H
