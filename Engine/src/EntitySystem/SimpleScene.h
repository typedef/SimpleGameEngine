#ifndef SIMPLE_SCENE_H
#define SIMPLE_SCENE_H

#include "SimpleSceneRenderer.h"
#include <EntitySystem/SimpleEcsTypes.h>
#include <Utils/SimpleEventDispatcher.h>
#include <EntitySystem/SimpleEcs.h>

struct EcsWorld;
struct CameraComponent;


typedef struct SimpleScene
{
    struct EcsWorld* pEcsWorld;
    i32 SelectedEntity;
    SimpleSceneRenderer SceneRenderer;
    EcsQuery MainQuery;
    SimpleEventDispatcher CameraDispatcher;
} SimpleScene;

SimpleScene simple_scene_new();
SimpleScene simple_scene_load();
void simple_scene_destroy(SimpleScene* pScene);
struct CameraComponent* simple_scene_get_main_camera(SimpleScene* pScene);
void simple_scene_set_main_camera(SimpleScene* pScene, struct CameraComponent* pCamera);
void simple_scene_camera_subscribe(SimpleScene* pScene, SimpleEventSubcribe subscribe);
void simple_scene_camera_unsubscribe(SimpleScene* pScene, const char* pName);
void simple_scene_update(SimpleScene* pScene);

#endif // SIMPLE_SCENE_H
