#include "SimpleEcs.h"
#include "EntitySystem/Components/Base/TransformComponent.h"

#include <Core/SimpleStandardLibrary.h>

static i32 WorldsCount = 0;
static EcsWorld* CurrentWorld = NULL;

EcsWorld*
_ecs_world_create()
{
    EcsWorld world = {
        .Id = WorldsCount,
        .EntitiesCount = 0,
        .EStorage = {
            .Deleted = NULL,
            .Records = NULL
        },
        .CStorage = {
            .LastId = (ComponentId) EcsIdType_ComponentFirst,
            .ComponentsToID = NULL,
            .Records = NULL
        },
        .CachedQueries = NULL
    };

    EcsWorld* result = memory_allocate_type(EcsWorld);
    memcpy(result, &world, sizeof(EcsWorld));

    //NOTE(typedef): mb remove this in future
    ecs_set_current_world(result);

    ++WorldsCount;
    return result;
}

void
_ecs_world_destroy(EcsWorld* world)
{
    array_foreach(world->EStorage.Records,
                  array_free(item.Childs);
                  hash_free(item.EntityData););
    array_free(world->EStorage.Deleted);
    array_free(world->EStorage.Records);

    array_foreach(world->CStorage.Records,
                  array_free(item.Entities);
                  array_free(item.Data););
    array_free(world->CStorage.Records);
    shash_free(world->CStorage.ComponentsToID);
    memory_free(world);
    ecs_set_current_world(NULL);
    --WorldsCount;
}

i32
_ecs_world_register_component(EcsWorld* world, const char* componentName, size_t size)
{
    //everything wrong world->CStorage.ComponentsToID is OffsetArray
    i64 alreadyRegistered = shash_geti(world->CStorage.ComponentsToID, componentName);
    if (alreadyRegistered != -1)
    {
        GWARNING("Component %s already exist!\n", componentName);
        vassert_break();
        return 0;
    }

    ComponentId componentId = world->CStorage.LastId;
    ComponentId componentArrayIndex = GetComponentAsArrayIndex(componentId);

    ComponentRecord cRecord = {
        .Id = componentId,
        .Entities = NULL,
        .ComponentSize = size,
        .Data = NULL
    };

    // GLOG("Component %s size %ld\n", componentName, size);
    vassert(componentArrayIndex == array_count(world->CStorage.Records)
            && "Component should be equal to size of array_count(CStorage.Records)!!!");
    array_push(world->CStorage.Records, cRecord);
    shash_put(world->CStorage.ComponentsToID, componentName, componentId);

    ++world->CStorage.LastId;
    return 1;
}

void
_ecs_world_entity_add_component_str(EcsWorld* pWorld, EntityId entity, const char* componentStr, void* data, size_t size)
{

    ComponentId componentId;
    EntityRecord* eRecord;
    ComponentRecord* pRecord =
        _ecs_world_entity_add_component(pWorld, entity, componentStr, &componentId, &eRecord);

    vguard(pRecord->ComponentSize == size && "Wrong data typed in ecs_entity_add_component");

    /* NOTE(typedef): Link Entity to ComponentRecord */
    array_push(pRecord->Entities, entity);
    i64 ind = array_count(pRecord->Data);
    // BUG: we should push content here
    array_push_void(pRecord->Data, data, pRecord->ComponentSize);

    TransformComponent* pTc = (TransformComponent*)pRecord->Data;

    /* NOTE(typedef): Link ComponentRecord->Data to Entity */
    /* void* dataPtr = record->Data + (array_count(record->Data) - 1) * sizeof(data);*/
    hash_put(eRecord->EntityData, componentId, ind);
}

i32
_ecs_world_entity_exist(EcsWorld* world, EntityId entity)
{
    vassert_not_null(world->EStorage.Records);

    i64 entityId = GetEntityAsArrayIndex(entity);
    i32 eRecordsCount = array_count(world->EStorage.Records);
    if (entityId >= eRecordsCount)
    {
        GERROR("Entity is outside of bounds Entity(%ld) >= ERecordsCount(%ld)\n", entityId, eRecordsCount);
        vassert(0 && "Should create entity first, wrong entity id!");
    }

    EntityRecord record = world->EStorage.Records[entityId];
    return record.Id != -1;
}

ComponentRecord*
_ecs_world_entity_add_component(EcsWorld* world, EntityId entity, const char* component, ComponentId* componentId, EntityRecord** eRecordPtr)
{
    /*
      NOTE(typedef):
      Checks if entity created/exist
    */
    i32 entityExist = _ecs_world_entity_exist(world, entity);
    if (!entityExist)
    {
        GWARNING("Entity %ld doesnt exist!\n", entity);
        vassert(0 && "Entity doesnt exist!");
    }

    /*
      NOTE(typedef): checks if component already exist in entity
    */
    ComponentId compId = GetComponentAsArrayIndex(
        ecs_component_get_id_by_name(component));
    *componentId = compId;

    EntityId entityIndex = GetEntityAsArrayIndex(entity);
    EntityRecord* eRecord = &world->EStorage.Records[entityIndex];
    if (eRecord->EntityData != NULL && hash_geti(eRecord->EntityData, compId) != -1)
    {
        GWARNING("Component %s already exist in entity %d!\n", component, entity);
        vassert(0 && "Component is already added!");
    }

    *eRecordPtr = eRecord;
    ComponentRecord* pCompRecord = &world->CStorage.Records[compId];

    /*
      DOCS(typedef): update cache query
    */
    // BUG(): adding entity with only TransformComponent records to
    // Archetype: TransformComponent, StaticModelComponent
    if (world->CachedQueries != NULL)
    {
        i32 qid, count = array_count(world->CachedQueries);
        for (qid = 0; qid < count; ++qid)
        {
            EcsQuery* ecsQuery = &world->CachedQueries[qid];

            i32 componentNotInQuery =
                (array_index_of(ecsQuery->Ids, item == compId) == -1);
            if (componentNotInQuery)
            {
                continue;
            }

            i32 allComponentsAcceptAddedExist = 1,
                compCount = array_count(ecsQuery->Ids);
            for (i32 c = 0; c < compCount; ++c)
            {
                //BUG ??? GetComponentId(ecsQuery->Ids[c])
                ComponentId componentQueryId = ecsQuery->Ids[c];
                vassert(ComponentIndexIdValid(componentQueryId) && "Not valid component!");
                i32 hasComponent =
                    _ecs_world_entity_has_component_by_index_id(world, entity, componentQueryId);

                if (!hasComponent && componentQueryId != compId)
                {
                    allComponentsAcceptAddedExist = 0;
                    break;
                }
            }

            if (!allComponentsAcceptAddedExist)
                continue;

            i32 entityDoesntExistInsideQuery =
                (array_index_of(ecsQuery->Entities, item.Id == entity) == -1);
            if (entityDoesntExistInsideQuery)
            {
                // update cache
                //GERROR("Update cache for entity: %d\n", entity);
                array_push(ecsQuery->Entities, *eRecord);
                break;
            }
        }
    }

    return pCompRecord;
}

void
_ecs_world_entity_remove_component(EcsWorld* world, EntityId entity, const char* component)
{
    vassert(0 && "Not implemented yet!");

    vassert_not_null(world);
    vassert_not_null(component);
    vassert(entity > -1 && "Wrong entity id!");

    /*
      NOTE(typedef):
      Checks if entity created/exist
    */
    i32 entityExist = _ecs_world_entity_exist(world, entity);
    if (!entityExist)
    {
        GWARNING("Entity %ld doesnt exist!\n", entity);
        vassert_break();
    }

    ComponentId componentId = ecs_component_get_id_by_name(component);
    ComponentId componentArrayIndex = GetComponentAsArrayIndex(componentId);
    ComponentRecord* record = &world->CStorage.Records[componentArrayIndex];

    if (record->Data == NULL)
    {
        GWARNING("Can't remove from record->Data because it's NULL!\n");
        return;
    }

}

ComponentRecord
_ecs_world_get_component_record(EcsWorld* world, const char* componentName)
{
    ComponentId componentId = ecs_component_get_id_by_name(componentName);
    return _ecs_world_get_component_record_by_id(world, componentId);
}
ComponentRecord
_ecs_world_get_component_record_by_id(EcsWorld* world, ComponentId componentId)
{
    ComponentRecord componentRecord =
        world->CStorage.Records[GetComponentAsArrayIndex(componentId)];
    return componentRecord;
}

void*
_ecs_world_get_component_record_data(EcsWorld* world, const char* componentName)
{
    ComponentId componentId = ecs_component_get_id_by_name(componentName);
    ComponentRecord componentRecord =
        world->CStorage.Records[GetComponentAsArrayIndex(componentId)];
    return componentRecord.Data;
}

void*
_ecs_world_entity_get_component(EcsWorld* world, EntityId entity, const char* componentName)
{
    ComponentId componentId = ecs_component_get_id_by_name(componentName);
    return _ecs_world_entity_get_component_by_id(world, entity, componentId);
}
void*
_ecs_world_entity_get_component_by_id(EcsWorld* world, EntityId entity, ComponentId componentId)
{
    EntityId entityAsIndex = GetEntityAsArrayIndex(entity);
    vassert(EntityIndexIdValid(entityAsIndex) && "Wrong entity index!");
    ComponentId componentAsIndex = GetComponentAsArrayIndex(componentId);
    EntityRecord* eRecord = &world->EStorage.Records[entityAsIndex];

    i64 index = hash_get(eRecord->EntityData, componentAsIndex);
    ComponentRecord cRec = world->CStorage.Records[componentAsIndex];
    void* linkData = (void*) (cRec.Data + index * cRec.ComponentSize);

    return linkData;
}

i32
_ecs_world_entity_has_component(EcsWorld* world, EntityId entity, const char* component)
{
    ComponentId componentId = ecs_component_get_id_by_name(component);
    return _ecs_world_entity_has_component_by_id(world, entity, componentId);
}

i32
_ecs_world_entity_has_component_by_id(EcsWorld* world, EntityId entity, ComponentId componentId)
{
    ComponentId componentAsIndex = GetComponentAsArrayIndex(componentId);
    i32 hasComponent =
        _ecs_world_entity_has_component_by_index_id(world, entity, componentAsIndex);
    return hasComponent;
}
i32
_ecs_world_entity_has_component_by_index_id(EcsWorld* world, EntityId entity, ComponentId componentAsIndex)
{
    EntityId entityAsIndex = GetEntityAsArrayIndex(entity);
    EntityRecord eRecord = world->EStorage.Records[entityAsIndex];
    i32 hasComponent = hash_geti(eRecord.EntityData, componentAsIndex) != -1;
    return hasComponent;
}

void
_ecs_world_entity_set_component(EcsWorld* world, EntityId entity, const char* componentName, void* data)
{
    ComponentId componentId = ecs_component_get_id_by_name(componentName);
    _ecs_world_entity_set_component_by_id(world, entity, componentId, data);
}
void
_ecs_world_entity_set_component_by_id(EcsWorld* world, EntityId entity, ComponentId componentId, void* data)
{
    //NOTE(typedef): not touching this
    vassert_break();
    /* EntityId entityAsIndex = GetEntityAsArrayIndex(entity); */
    /* EntityRecord* eRecord = &world->EStorage.Records[entityAsIndex]; */
    /* void* linkData = hash_get(eRecord->EntityData, componentId); */

    /* ComponentId componentIndex = GetComponentAsArrayIndex(componentId); */
    /* ComponentRecord cRecord = world->CStorage.Records[componentIndex]; */
    /* memcpy(linkData, data, cRecord.ComponentSize); */
}

EntityId
_ecs_world_entity(EcsWorld* world, char* name)
{
    EntityId entityId = array_count(world->EStorage.Records) + EcsIdType_EntityFirst;

    EntityRecord eRecord = {
        .Name = string(name),
        .Id = entityId,
        .Parent = -1,
        .Childs = NULL,
        .EntityData = NULL
    };

    array_push(world->EStorage.Records, eRecord);

    return entityId;
}

static ComponentId*
string_to_component_ids(EcsWorld* world, const char* components)
{
    ComponentId* ids = NULL;

    char** splitted = string_split((char*)components, ',');
    char** splittedTrimed = NULL;
    array_foreach_ptr(
        splitted,
        size_t newLen;
        i32 len = string_length(*item);
        char* trimed = string_trim_char(*item, len, &newLen, ' ');
        array_push(splittedTrimed, trimed);
        );
    array_foreach(
        splittedTrimed,
        ComponentId id = shash_get(world->CStorage.ComponentsToID, item);
        ComponentId indexId = GetComponentAsArrayIndex(id);
        array_push(ids, indexId);
        );

    /* array_foreach(ids, */
    /*		  printf("Id: %d\n", item);); */

    // TODO(typedef): deallocate memory
    array_foreach(splitted, memory_free(item););
    array_foreach(splittedTrimed, memory_free(item););
    array_free(splitted);
    array_free(splittedTrimed);

    return ids;
}

#define ECS_MEASURE_PERFORMANCE 0

EcsQuery
_ecs_world_get_query(EcsWorld* world, const char* components, i32 needsToBeCached)
{
#if ECS_MEASURE_PERFORMANCE == 1
    TimeState state;
    profiler_start(&state);
#endif

    ComponentId* compIds =
        string_to_component_ids(world, components);

    /*
      DOCS(typedef): ecs_get_query() from CachedQueries
    */
    if (world->CachedQueries)
    {
        array_foreach(
            world->CachedQueries,
            i32 isEqual = 1;
            for (i32 cid = 0; cid < array_count(item.Ids); ++cid)
            {
                ComponentId currId = item.Ids[cid];
                if (array_index_of(compIds, item == currId) == -1)
                {
                    isEqual = 0;
                    break;
                }
            }

            if (isEqual)
            {
                // GINFO("Returned cached!\n");
#if ECS_MEASURE_PERFORMANCE == 1
                profiler_end(&state);
                profiler_print(&state);
#endif

                return item;
            });
    }

#if ECS_MEASURE_PERFORMANCE == 1
    profiler_end(&state);
    profiler_print(&state);
#endif


    /*
      DOCS(typedef): This called once at the start of the application for
      Frequently Used Entities, then we add Query Data inside add_component
      function.

      Slow searching of components
    */
    EntityId* savedIds = NULL;
    array_foreach(compIds, array_push(savedIds, item););
    /*
      NOTE(typedef): sort by order 1 2 3 15 123
    */
    array_sort(
        compIds,
        array_count(world->CStorage.Records[item0].Data)
        >
        array_count(world->CStorage.Records[item1].Data));

    /* GWARNING("Comps: %s\n", components); */
    /* array_foreach( */
    /*	savedIds, */
    /*	printf("Id: %d, count: %d\n", */
    /*	       item, */
    /*	       array_count(world->CStorage.Records[item].Data));); */

    EntityRecord* entities = NULL;
    {

        ComponentRecord record = world->CStorage.Records[compIds[0]];

        for (i32 e = 0; e < array_count(record.Entities); ++e)
        {
            EntityId entity = record.Entities[e];

            i32 addEntity = 1;

            for (i32 id = 1; id < array_count(compIds); ++id)
            {
                ComponentId compId = compIds[id];
                record = world->CStorage.Records[compId];

                i32 isInsideOtherRecord = 0;
                for (i32 re = 0; re < array_count(record.Entities); ++re)
                {
                    EntityId recordEntity = record.Entities[re];
                    if (entity == recordEntity)
                    {
                        isInsideOtherRecord = 1;
                        break;
                    }
                }

                addEntity *= isInsideOtherRecord;
            }

            if (addEntity)
            {
                EntityRecord newRec =
                    world->EStorage.Records[GetEntityAsArrayIndex(entity)];
                array_push(entities, newRec);
            }
        }

    }

    array_free(compIds);

    EcsQuery query = (EcsQuery) {
        .Ids = savedIds,
        .Entities = entities,
    };

    //GINFO("Entities count: %d\n", array_count(query.Entities));

    if (needsToBeCached)
    {
        /*
          NOTE(typedef): we dont check for existance for a reason
        */
        array_push(world->CachedQueries, query);
    }

    return query;
}


/*
  NOTE(typedef):
  Helper function, adds additional functionality to Ecs
*/
EcsWorld*
ecs_get_current_world()
{
    return CurrentWorld;
}
void
ecs_set_current_world(EcsWorld* world)
{
    CurrentWorld = world;
}

ComponentId
ecs_component_get_id_by_name(const char* name)
{
    EcsWorld* world = ecs_get_current_world();
    ComponentId result = ecs_world_component_get_id_by_name(world, name);
    return result;
}

ComponentId
ecs_world_component_get_id_by_name(EcsWorld* world, const char* name)
{
    i32 result = shash_geti(world->CStorage.ComponentsToID, name);
    if (result == -1)
    {
        GERROR("Register component %s\n", name);
        vassert(result != -1 && "Can't find component in EcsWorld! Register component before usage.");
    }

    ComponentId componentId = shash_get(world->CStorage.ComponentsToID, name);
    return componentId;
}

/*
  DOCS(typedef): Helpfull utils
*/

#include "Components/Base/CameraComponent.h"

CameraComponent*
ecs_get_main_camera_component(EcsWorld* world)
{
    vassert_not_null(world);

    // NOTE(typedef): Getting Main Camera
    CameraComponent* mainCamera = NULL;
    CameraComponent* cameras = GetComponentRecordData(CameraComponent);
    array_foreach_ptr(cameras,
                      if (item->IsMainCamera == 1)
                      {
                          mainCamera = item;
                      });

    vassert_not_null(mainCamera);
    return mainCamera;
}
