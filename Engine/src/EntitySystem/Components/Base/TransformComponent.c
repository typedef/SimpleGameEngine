#include "TransformComponent.h"

#include <Math/SimpleMath.h>

TransformComponent
transform_component_new(v3 translation,
			v3 rotation,
			v3 scale)
{
    quat q = quat_pitch_yaw_roll(rotation.Pitch, rotation.Yaw, rotation.Roll);

    TransformComponent component = {
	.ShouldBeUpdated = 0,
	.Translation = translation,
	.Rotation    = rotation,
	.Scale       = scale,
	.Matrix      = quat_transform(translation, q, scale)
    };

    return component;
}
