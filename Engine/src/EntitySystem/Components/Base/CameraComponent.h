#ifndef CAMERA_COMPONENT_H
#define CAMERA_COMPONENT_H

#include <Graphics/Camera.h>
#include <Graphics/CameraType.h>

typedef enum CameraMoveDirectionType
{
    CameraMoveDirectionType_Forward,
    CameraMoveDirectionType_Backward,
    CameraMoveDirectionType_Right,
    CameraMoveDirectionType_Left,
    CameraMoveDirectionType_Up,
    CameraMoveDirectionType_Down
} CameraMoveDirectionType;

typedef struct CameraComponentSettings
{
    CameraType Type;
    f32 SensitivityHorizontal;
    f32 SensitivityVertical;
    f32 Yaw;   // x
    f32 Pitch; // y
    f32 PrevX;
    f32 PrevY;
    v3  Position;
    v3  Front;
    v3  Up;
    v3  Right;
} CameraComponentSettings;
CameraComponentSettings camera_component_settings_new(CameraType type);

typedef struct PerspectiveSettings
{
    f32 Near;
    f32 Far;
    f32 AspectRatio;
    // NOTE(typedef): in degree
    f32 Fov;
} PerspectiveSettings;

typedef struct OrthographicSettings
{
    //NOTE(typedef): Orthographic
    f32 Near;
    f32 Far;
    f32 Left;
    f32 Right;
    f32 Bot;
    f32 Top;
} OrthographicSettings;

typedef struct SpectatorSettings
{
    f32 Speed;
    f32 FasterMultiplier;
    f32 SlowerMultiplier;
    f32 Zoom;
} SpectatorSettings;

struct CameraComponent;

typedef struct CameraComponentCreateInfo
{
    Camera Base;
    i8 IsMainCamera;
    i8 IsCameraMoved;
    CameraComponentSettings Settings;
    PerspectiveSettings Perspective;
    OrthographicSettings Orthographic;
    SpectatorSettings Spectator;
} CameraComponentCreateInfo;

typedef struct CameraComponent
{
    Camera Base;
    // NOTE: Can chage this flag only on creation or through
    // simple_scene_set_main_camera function.
    i8 IsMainCamera;
    i8 IsCameraMoved;
    CameraComponentSettings Settings;
    PerspectiveSettings Perspective;
    OrthographicSettings Orthographic;
    SpectatorSettings Spectator;
} CameraComponent;

CameraComponent camera_component_new(CameraType type);
CameraComponent camera_component_new_ext(CameraComponentCreateInfo createInfo);

void camera_component_set_perspective(CameraComponent* camera);
void camera_component_set_orthograhic(CameraComponent* camera);
void camera_component_set_spectator(CameraComponent* camera);

void camera_component_move_speed(CameraComponent* camera, CameraMoveDirectionType direction, f32 timestep, f32 speed);
void camera_component_move(CameraComponent* camera, CameraMoveDirectionType direction, f32 timestep);
void camera_component_rotate(CameraComponent* camera, f64 mx, f64 my, f32 timestep);

void camera_component_update(CameraComponent* camera);

#endif // CAMERA_COMPONENT_H
