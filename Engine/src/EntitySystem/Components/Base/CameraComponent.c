#include "CameraComponent.h"

#include <Math/SimpleMath.h>
#include <Core/SimpleStandardLibrary.h>

CameraComponentSettings
camera_component_settings_new(CameraType type)
{
    CameraComponentSettings settings = (CameraComponentSettings) {
        .Type = type,
        .Position = v3_new(0, 0, 0),
        .Front = v3_new(0, 0, -1),
        .Up    = v3_new(0, 1,  0),
        .Right = v3_new(1, 0,  0),
        .Yaw = 0,
        .Pitch = 0,
        .PrevX = 0,
        .PrevY = 0,
        .SensitivityHorizontal = 0.35f,
        .SensitivityVertical = 0.45f
    };

    return settings;
}

CameraComponent
camera_component_new(CameraType type)
{
    CameraComponentCreateInfo createInfo = {
        .IsMainCamera = 0,
        .Base = camera_new_default(),
        .Settings = camera_component_settings_new(type)
    };

    CameraComponent camera =
        camera_component_new_ext(createInfo);

    return camera;
}

CameraComponent
camera_component_new_ext(CameraComponentCreateInfo createInfo)
{
    if (createInfo.Perspective.Near != 0
        && createInfo.Perspective.Far != 0
        && createInfo.Perspective.AspectRatio != 0
        && createInfo.Perspective.Fov != 0)
    {
        GWARNING("fov: %f\n", createInfo.Perspective.Fov);
        vguard(createInfo.Perspective.Fov > 10 && "Fov should be in radiance!");
        createInfo.Perspective.Fov = rad(createInfo.Perspective.Fov);
    }

    CameraComponent camera = {
        .IsMainCamera = createInfo.IsMainCamera,
        .IsCameraMoved = createInfo.IsCameraMoved,
        .Base = createInfo.Base,
        .Settings = createInfo.Settings,
        .Perspective = createInfo.Perspective,
        .Orthographic = createInfo.Orthographic,
        .Spectator = createInfo.Spectator
    };

    camera.Spectator.FasterMultiplier = 35.0f;
    camera.Spectator.SlowerMultiplier = 0.03f;

    camera.Base.View =
        m4_translate_identity(createInfo.Settings.Position);

    if (camera.Settings.Type & CameraType_Spectator)
    {
        camera_component_set_spectator(&camera);
        GINFO("[Camera component] Set to spectator!\n");
    }

    if (camera.Settings.Type & CameraType_Perspective)
    {
        camera_component_set_perspective(&camera);
        GINFO("[Camera component] Set to perspective!\n");
    }

    if (camera.Settings.Type & CameraType_Orthographic)
    {
        camera_component_set_orthograhic(&camera);
        GINFO("[Camera component] Set to orthograhic!\n");
    }

    return camera;
}

void
camera_component_set_perspective(CameraComponent* camera)
{
    camera->Settings.Type |=  CameraType_Perspective ;
    camera->Settings.Type &= ~CameraType_Orthographic;

    PerspectiveSettings set = camera->Perspective;
    m4 projection = perspective(set.Near, set.Far, set.AspectRatio, set.Fov);
    camera_update_projection((Camera*) &camera->Base, projection);
    // NOTE(typedef): mb camera_component_update()
}

void
camera_component_set_orthograhic(CameraComponent* camera)
{
    camera->Settings.Type |=  CameraType_Orthographic;
    camera->Settings.Type &= ~CameraType_Perspective ;

    OrthographicSettings set = camera->Orthographic;
    m4 projection = orthographic(set.Left, set.Right, set.Bot, set.Top, set.Near, set.Far);
    camera_update_projection((Camera*) &camera->Base, projection);
    // NOTE(typedef): mb camera_component_update()
}

void
camera_component_set_spectator(CameraComponent* camera)
{
    camera->Settings.Type |=  CameraType_Spectator;
    camera->Settings.Type &= ~CameraType_Arcball  ;
}

void
camera_component_update(CameraComponent* camera)
{
    CameraType type = camera->Settings.Type;
    Camera* base = (Camera*) camera;
    CameraComponentSettings set = camera->Settings;

    // NOTE(typedef): update view
    if (camera->IsCameraMoved)
    {
        if (type & CameraType_Arcball)
        {
            //base->View = view_get2(set.Position, set.Orientation);
            vassert_break();
        }
        else if (type == CameraType_PerspectiveSpectator)
        {
            base->View = m4_look_at(set.Position, v3_add(set.Position, set.Front), set.Up);
        }
        else if (type == CameraType_OrthographicSpectator)
        {
            base->View = m4_translate_identity(set.Position);
        }
        else
        {
            vassert_break();
        }

        // TODO: Remove this
        vguard(camera_is_valid(base) == 1);

        // NOTE(typedef): update projection
        base->ViewProjection = m4_mul(base->Projection, base->View);

        camera->IsCameraMoved = 0;
    }
}

void
camera_component_move_speed(CameraComponent* pCamera, CameraMoveDirectionType direction, f32 timestep, f32 speed)
{
    printf("camera_component_move_speed: %p \n", pCamera);

    CameraType type = pCamera->Settings.Type;
    if (type & CameraType_Spectator)
    {
        CameraComponentSettings* pSetting = &pCamera->Settings;
        SpectatorSettings set = pCamera->Spectator;
        f32 velocity = speed * timestep;
        pCamera->IsCameraMoved = 1;

        switch (direction)
        {
        case CameraMoveDirectionType_Forward:
            pSetting->Position = v3_add(pSetting->Position, v3_scale(pSetting->Front, velocity));
            break;
        case CameraMoveDirectionType_Backward:
            pSetting->Position = v3_sub(pSetting->Position, v3_scale(pSetting->Front, velocity));
            break;
        case CameraMoveDirectionType_Left:
            pSetting->Position = v3_sub(pSetting->Position, v3_scale(pSetting->Right, velocity));
            break;
        case CameraMoveDirectionType_Right:
            pSetting->Position = v3_add(pSetting->Position, v3_scale(pSetting->Right, velocity));
            break;

        case CameraMoveDirectionType_Up:
            pSetting->Position = v3_add(pSetting->Position, v3_new(0.0, 1.0f * velocity, 0.0f));
            break;
        case CameraMoveDirectionType_Down:
            pSetting->Position = v3_add(pSetting->Position, v3_new(0.0, -1.0f * velocity, 0.0f));
            break;

        default:
            pCamera->IsCameraMoved = 0;
            break;
        }
    }
    else if (type & CameraType_Arcball)
    {
        /* v3 forward = get_front_direction(camera); */
        /* v3 forwardDistance = v3_mulv(forward, 1/\* camera->Distance *\/); */
        /* camera->Position = v3_sub(camera->LookAtPoint, forwardDistance); */
    }
}

void
camera_component_move(CameraComponent* pCamera, CameraMoveDirectionType direction, f32 timestep)
{
    camera_component_move_speed(pCamera, direction, timestep, pCamera->Spectator.Speed);
}

void
camera_component_rotate(CameraComponent* camera, f64 mx, f64 my, f32 timestep)
{
    CameraComponentSettings* set = &camera->Settings;

    /*
      NOTE(typedef): spectator behaviour
    */
    if (set->PrevX == 0.0f && set->PrevY == 0.0f)
    {
        set->PrevX = mx;
        set->PrevY = my;

        return;
    }

    f64 dx = mx - set->PrevX;
    f64 dy = my - set->PrevY;
    //TODO(typedef): find more accurate value
    const f32 scv = 0.1f;
    set->Yaw   = dx * set->SensitivityHorizontal * timestep * scv;
    set->Pitch = dy * set->SensitivityVertical * timestep * scv;

    set->PrevX = mx;
    set->PrevY = my;

    // GINFO("[dx, dy] [%f %f]\n", dx, dy);
    // GINFO("[mx, my] [%f %f]\n", mx, my);
    // GINFO("[px, py] [%f %f]\n", set->PrevX, set->PrevY);
    if (!f32_equal(dx, 0.0f) || !f32_equal(dy, 0.0f))
    {
        set->Up = v3_new(0, 1, 0);
        //GINFO("Pitch %f Yaw %f\n", set->Pitch, set->Yaw);
        quat qpitch = quat_new(-set->Pitch, set->Right);
        quat qyaw = quat_new(-set->Yaw, set->Up);
        quat q = quat_mul(qpitch, qyaw);
        // q internally normalized
        set->Front = quat_rotate_v3(q, set->Front);
        set->Right = v3_cross(set->Front, set->Up);
        //GINFO("Up: %f %f %f\n", set->Up.X, set->Up.Y, set->Up.Z);
        //GINFO("Ri: %f %f %f\n", set->Right.X, set->Right.Y, set->Right.Z);

        camera->IsCameraMoved = 1;
    }
}
