#include "StaticModelComponent.h"

#include <Model/GeneratedModel.h>
#include <AssetManager/AssetManager.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Model/StaticModel.h>

StaticModelComponent
static_model_component_create(const char* path)
{
    StaticModelComponent result = {
	.Model = asset_manager_load_model(path)
    };

    return result;
}

StaticModelComponent
static_model_component_create_model(StaticModel model)
{
    StaticModelComponent result = {
	.Model = model
    };

    return result;
}

StaticModelComponent
static_model_component_create_cube()
{
    StaticModelComponent result = {
	.Model = static_model_cube_create()
    };

    return result;
}
