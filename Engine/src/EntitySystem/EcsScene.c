#include "EcsScene.h"

#include <Application/Application.h>
#include <AssetManager/AssetManager.h>
#include <EntitySystem/SimpleEcs.h>
#include <EntitySystem/Components/AllComponents.h>
#include <EntitySystem/EcsSerializer.h>
#include <Graphics/SimpleWindow.h>
#include <Core/SimpleStandardLibrary.h>

static EcsWorld* gWorld = NULL;
static EntityRecord* gSelectedEntity = NULL;
static Application* App = NULL;

#if 0
void
scene_create_default_camera(f32 aspectRatio)
{
    EntityId cameraEntity = ecs_world_entity(gWorld, "DefaultCamera");
    CameraComponent cameraComponent =
        camera_component_new(CameraType_PerspectiveSpectator);
    // this should be TransformComponent
    cameraComponent.Settings.Position = v3_new(0.0f, 2.5f, 5.5f);
    cameraComponent.Perspective = (PerspectiveSettings) {
        .Near = 0.01f,
        .Far = 4000.0f,
        .AspectRatio = aspectRatio,
        .Fov = rad(90),
    };
    cameraComponent.Spectator = (SpectatorSettings) {
        .Speed = 5.0f,
        .Zoom = 0
    };
    cameraComponent.IsMainCamera = 1;
    camera_component_set_perspective(&cameraComponent);

    ecs_entity_add_component(cameraEntity, CameraComponent, cameraComponent);
}

void
scene_create()
{
    gWorld = ecs_world_create();

    ecs_world_register_component(gWorld, CameraComponent);
    ecs_world_register_component(gWorld, TransformComponent);
    ecs_world_register_component(gWorld, StaticModelComponent);
    ecs_world_register_component(gWorld, SkyboxComponent);

    App = application_get();

#if LEGACY
    renderer_init();
    //TODO(typedef): replace with dots or plane
    scene_basic_create_grid(v4_new(0.3, 0.3, 0.3, 1.0), 0.001f, -15.0f, 15.0f, 0.2f);
    renderer2d_init(NULL);
    // TODO(typedef): CubemapComponent
    renderer3d_cubemap_init(NULL);
    renderer3d_model_init(NULL);
#endif
}

void
scene_save()
{
    GINFO("Save scene!\n");

    EcsWorld* world = scene_get_world();
    if (world == NULL)
    {
        GERROR("Can't save scene, EcsWorld is NULL!\n");
        return;
    }

    ecs_serializer_write((void*)world, asset_scene("DefaultScene.json"));
}

void
scene_load()
{
    GINFO("Scene Load !!!\n");

    const char* defaultPath = asset_scene("Default.json");
    i32 fileExists = path_is_file_exist(defaultPath);
    vguard(fileExists == 1);
    EcsWorld* ecsWorld = (EcsWorld*) ecs_serializer_read(defaultPath);

    //scene_update_pointer_main_camera();
    //viewport_register_layer(scene_get_main_camera());
    //viewport_set_active();
    //viewport_panel_disable_movement();

    ecs_get_query_w_cache(TransformComponent, StaticModelComponent);
}

EcsWorld*
scene_get_world()
{
    return gWorld;
}

void*
scene_get_selected_entity()
{
    return gSelectedEntity;
}
void
scene_set_selected_entity(void* selectedEntity)
{
    gSelectedEntity = (EntityRecord*) selectedEntity;
}

CameraComponent*
scene_get_main_camera()
{
    vassert_not_null(gWorld);

    // NOTE(typedef): Getting Main Camera
    CameraComponent* mainCamera = NULL;
    CameraComponent* cameras = GetComponentRecordData(CameraComponent);
    array_foreach_ptr(cameras,
                      if (item->IsMainCamera == 1)
                      {
                          mainCamera = item;
                      });

    vassert_not_null(mainCamera);
    return mainCamera;
}

void
scene_update_pointer_main_camera()
{
    //CameraComponent* camera = scene_get_main_camera();
    //renderer3d_model_set_camera(camera);
    //renderer2d_set_camera(camera);
    //renderer3d_cubemap_set_camera(camera);
}

u32
scene_get_skybox()
{
    ComponentId id = shash_get(gWorld->CStorage.ComponentsToID, "SkyboxComponent");
    ComponentId index = GetComponentAsArrayIndex(id);
    ComponentRecord cRec = gWorld->CStorage.Records[index];
    if (cRec.Data == NULL)
    {
        //GWARNING("No skybox provided!\n");
        return 0;
    }

    u32 skyboxId = (*(u32*)cRec.Data);
    return skyboxId;
}

void
scene_update()
{
    if (gWorld == NULL || App == NULL)
    {
        return;
    }

    framebuffer_bind(viewport_get_current_framebuffer());
    {
        renderer_clear(v4_new(0.1f, 0.1f, 0.1f, 1.0f));
        CameraComponent* mainCamera = scene_get_main_camera();
        camera_component_update(mainCamera);

        u32 skyboxId = scene_get_skybox();
        if (skyboxId != 0)
            renderer3d_cubemap_draw(skyboxId);

        scene_basic_render_grid();

        static v4 FrontColor = { 0, 0, 1, 1 };
        static v4 RightColor = { 1, 0, 0, 1 };
        static v4 UpColor    = { 0, 1, 0, 1 };
        scene_basic_render_camera_orientation(mainCamera, FrontColor, RightColor, UpColor);

        // TransformComponent, StaticModelComponent
        EcsQuery query = ecs_get_query(TransformComponent, StaticModelComponent);
        i32 i, count = array_count(query.Entities);
        //GINFO("Count: %d\n", count);
        for (i = 0; i < count; ++i)
        {
            EntityRecord* eRecord = &query.Entities[i];
            TransformComponent* tc = GetComponent(eRecord->Id, TransformComponent);
            StaticModelComponent* smc = GetComponent(eRecord->Id, StaticModelComponent);

            vassert_not_null(tc);
            vassert_not_null(smc);
            renderer3d_model_draw(smc->Model, tc->Matrix);
        }
    }
    framebuffer_unbind();

    framebuffer_transfer(viewport_get_current_framebuffer()->RendererId,
                         viewport_get_draw_framebuffer()->RendererId,
                         App->Window->Width,
                         App->Window->Height);

    renderer_flush(viewport_get_draw_framebuffer()->ColorAttachments[0]);
}


void
scene_destroy()
{
    ecs_world_destroy(gWorld);
}
#endif
