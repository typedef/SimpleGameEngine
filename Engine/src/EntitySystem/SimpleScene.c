#include "SimpleScene.h"

#include <Application/Application.h>
#include <Graphics/SimpleWindow.h>
#include <Math/SimpleMath.h>
#include <Core/SimpleStandardLibrary.h>

#include "SimpleEcs.h"
#include "SimpleSceneRenderer.h"
#include "Components/Base/CameraComponent.h"
#include "Components/Base/TransformComponent.h"
#include "Components/3D/StaticModelComponent.h"
#include "Utils/SimpleEventDispatcher.h"

CameraComponent _create_camera_component(SimpleWindow* pWindow);

SimpleScene
simple_scene_new()
{
    EcsWorld* pEcsWorld = ecs_world_create();

    ecs_world_register_component(pEcsWorld, CameraComponent);
    ecs_world_register_component(pEcsWorld, TransformComponent);
    ecs_world_register_component(pEcsWorld, StaticModelComponent);

    CameraComponent mainCamera = _create_camera_component(
        application_get_window());
    EntityId cameraEntityId = ecs_entity("MainCamera");
    ecs_entity_add_component(cameraEntityId, CameraComponent, mainCamera);

    SimpleScene scene = {
        .pEcsWorld = pEcsWorld,
        .CameraDispatcher = simple_event_dispatcher_new(),
        .SelectedEntity = -1,
    };

    CameraComponent* pCamera = simple_scene_get_main_camera(&scene);

    SimpleSceneRendererSettings renderSettings = {
        .pCamera = &pCamera->Base,
    };
    SimpleSceneRenderer ssr = simple_scene_renderer_new(&renderSettings);
    scene.SceneRenderer = ssr;

    return scene;
}

SimpleScene
simple_scene_load()
{
    SimpleScene result = {};
    vguard_not_impl();
    return result;
}

void
simple_scene_destroy(SimpleScene* pScene)
{
    ecs_world_destroy(pScene->pEcsWorld);
    simple_scene_renderer_destroy(&pScene->SceneRenderer);
}

CameraComponent*
simple_scene_get_main_camera(SimpleScene* pScene)
{
    EcsWorld* pEcsWorld = pScene->pEcsWorld;
    vassert_not_null(pEcsWorld);

    // NOTE(typedef): Getting Main Camera
    CameraComponent* mainCamera = NULL;
    CameraComponent* aCameras = (CameraComponent*) _ecs_world_get_component_record_data(
        pEcsWorld, ToString(CameraComponent));
    array_foreach_ptr(aCameras,
                      if (item->IsMainCamera == 1)
                      {
                          mainCamera = item;
                      });

    vassert_not_null(mainCamera);
    return mainCamera;
}

void
simple_scene_set_main_camera(SimpleScene* pScene, CameraComponent* pCamera)
{
    simple_scene_get_main_camera(pScene)->IsMainCamera = 0;

    pCamera->IsMainCamera = 1;

    // todo: needs more testing
    simple_event_dispatcher_dispatch(&pScene->CameraDispatcher, pCamera);
}

void
simple_scene_camera_subscribe(SimpleScene* pScene, SimpleEventSubcribe subscribe)
{
    simple_event_dispatcher_register(&pScene->CameraDispatcher, subscribe);
}
void
simple_scene_camera_unsubscribe(SimpleScene* pScene, const char* pName)
{
    simple_event_dispatcher_unregister(&pScene->CameraDispatcher, pName);
}

void
simple_scene_update(SimpleScene* pScene)
{
    static EcsQuery query = {};

    if (query.Ids == NULL)
    {
        query = ecs_get_query_w_cache(TransformComponent, StaticModelComponent);
    }

    static i32 flag = 0;
    if (flag)
        return;
    flag = 1;

    i64 i, count = array_count(query.Entities);
    for (i = 0; i < count; ++i)
    {
        EntityRecord rec = query.Entities[i];
        TransformComponent* pTransform = GetComponentExt(pScene->pEcsWorld, rec.Id, TransformComponent);
        StaticModelComponent* pStaticModel = GetComponentExt(pScene->pEcsWorld, rec.Id, StaticModelComponent);

        simple_scene_renderer_add_static(&pScene->SceneRenderer, pTransform, pStaticModel);
    }

    simple_scene_renderer_flush_static(&pScene->SceneRenderer);

}


/*DOCS:
  ###########################################
  ###          PRIVATE                    ###
  ###########################################*/
CameraComponent
_create_camera_component(SimpleWindow* pWindow)
{
    CameraComponentCreateInfo createInfo = {
        .IsMainCamera = 1,
        .IsCameraMoved = 1,

        .Settings = {
            .Type = CameraType_PerspectiveSpectator,
            .Position = v3_new(0.0f, 0.0f, 7.92f),
            .Front = v3_new(0, 0, -1),
            .Up    = v3_new(0, 1,  0),
            .Right = v3_new(1, 0,  0),
            .Yaw = 0,
            .Pitch = 0,
            .PrevX = 0,
            .PrevY = 0,
            .SensitivityHorizontal = 0.35f,
            .SensitivityVertical = 0.45f
        },

        .Perspective = {
            .Near = 0.001f,
            .Far = 50000.0f,
            .AspectRatio = pWindow->AspectRatio,
            // NOTE(typedef): in degree
            .Fov = 75.0f
        },

        .Spectator = {
            .Speed = 5.0f,
            .Zoom = 0
        }
    };

    CameraComponent cameraComponent =
        camera_component_new_ext(createInfo);
    camera_component_update(&cameraComponent);

    // TODO: Place it somewhere else (not in SimpleScene)
    /* EditorViewport gEditorViewport; */
    /* editor_viewport_create(&gEditorViewport, &cameraComponent, simple_scene_renderer_camera_updated); */
    /* gEditorViewport.OnUpdate = simple_scene_renderer_camera_updated(); */

    return cameraComponent;
}
