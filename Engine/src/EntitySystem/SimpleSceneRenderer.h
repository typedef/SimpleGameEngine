#ifndef SIMPLE_SCENE_RENDERER_H
#define SIMPLE_SCENE_RENDERER_H

struct Camera;
struct CameraComponent;
struct Vsr2dCompositor;
struct Vsr3dCompositor;
struct TransformComponent;
struct StaticModelComponent;

typedef struct SimpleSceneRendererSettings
{
    struct Camera* pCamera;
} SimpleSceneRendererSettings;

typedef struct SimpleSceneRenderer
{
    struct Vsr2dCompositor* p2dCompositor;
    struct Vsr3dComposito* p3dCompositor;
} SimpleSceneRenderer;

SimpleSceneRenderer simple_scene_renderer_new(SimpleSceneRendererSettings* pSettings);
void simple_scene_renderer_destroy(SimpleSceneRenderer* pSimpleRenderer);
void simple_scene_renderer_camera_updated(struct CameraComponent* pCamera);
void simple_scene_renderer_add_static(SimpleSceneRenderer* pSimpleRenderer, struct TransformComponent* pTransform, struct StaticModelComponent* pModel);
void simple_scene_renderer_flush_static(SimpleSceneRenderer* pSimpleRenderer);

#endif // SIMPLE_SCENE_RENDERER_H
