#ifndef ENGINE_H
#define ENGINE_H

#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"

#include "Application/Application.h"

#include "Event/Event.h"

#include "Core/SimpleSmallEcs.h"
#include "Core/Types.h"

// Utils
#include "Utils/WavReader.h"
#include "Utils/SoundComponent.h"
#include "Utils/Font.h"

// Graphics
#include "Graphics/SimpleWindow.h"
// Graphics -> Vulkan
#include "Graphics/Vulkan/VulkanSimpleApi.h"
#include "Graphics/Vulkan/Vsr2d.h"
#include "Graphics/Vulkan/Vsr3d.h"

// Input System
#include "InputSystem/SimpleInput.h"
#include "InputSystem/KeyBinding.h"

// Asset Manager
#include "AssetManager/AssetManager.h"

// Localization
#include "Localization/SimpleLocalization.h"
#include "Localization/SimpleLocalizationCodeGen.h"

// Simple Ui
#include "UI/Sui.h"

// Entity System
#include "EntitySystem/SimpleEcs.h"
#include "EntitySystem/EcsScene.h"
#include "EntitySystem/Components/AllComponents.h"
#include "EntitySystem/EcsSerializer.h"

// Editor
#include <Editor/EditorPanels.h>

// Models
#include "Model/ModelBase.h"
#include "Model/StaticModel.h"
#include "Model/GeneratedModel.h"
#include "Model/DynamicModel.h"

// Math
#include "Math/SimpleMath.h"
#include "Math/SimpleMathIO.h"

// NOTE(typedef): Helma part
#include "Helma/Helma.h"
#include "Helma/HelmaParserHelper.h"
#include "Helma/HelmaEnumGenerator.h"


#endif // ENGINE_H
