#ifndef APPLICATION_H
#define APPLICATION_H

#include <Core/Types.h>

#pragma clang diagnostic ignored "-Wvisibility"


struct Event;
struct SimpleWindow;

typedef struct Layer
{
    const char* Name;
    void (*OnAttach)();
    // NOTE(bies): Some layer want call other function after they been initialized in *_on_attach()
    void (*OnAttachFinished)();
    void (*OnBeforeUpdate)();
    void (*OnUpdate)(f32 timestep);
    void (*OnAfterUpdate)();
    void (*OnEvent)(struct Event* event);
    void (*OnDestoy)();
    void (*OnUIRender)();
} Layer;

typedef struct ApplicationSettings
{
    // DOCS(typedef): Width, Height, Name should go in a row
    i16 Width;
    i16 Height;
    const char* Name;
    i8 IsFrameRatePrinted;
    i8 IsInitGraphics;
    i8 IsVsync;
    i8 IsDebug;
    u8 UiFpsSyncRatio;

    i32 ArgumentsCount;
    char** Arguments;
} ApplicationSettings;

typedef struct RuntimeStatistics
{
    //DOCS(typedef): Frame time (Timestep) related
    f32 CurrentTime;
    f32 LastFrameTime;
    f32 Timestep;

    //DOCS(typedef): Second Counter
    f32 SecondElapsed;

    i32 FramesCount; //TODO()rename to fps

    // DOCS(typedef): Ui updates related
    u8 UiFpsSyncRatio;
    u8 FpsBeforeUiUpdate;
} RuntimeStatistics;

typedef struct Application
{
    /* TODO(typedef): Make this bit flag */
    i8 IsMinimized;
    i8 IsRunning;
    i8 IsGraphicsInit;
    i8 IsFrameRatePrinted;
    i8 IsVsync;
    char* EventTypeToString[32];
    Layer* Layers;
    struct SimpleWindow* Window;
    char** Arguments;

    RuntimeStatistics Stats;
} Application;

Application* application_get();
struct SimpleWindow* application_get_window();
void application_set_window(struct SimpleWindow* pSimpleWindow);
void application_push_layer(Layer layer);
void application_create(ApplicationSettings appSettings);
void application_start();
void application_on_event(struct Event* event);
void application_close();
void application_end();
void application_set_locale_utf8();

#endif
