#include "Application.h"

#include <locale.h>

#include <AssetManager/AssetManager.h>
#include <Event/Event.h>
#include <Graphics/SimpleWindow.h>
#include <Graphics/GraphicsLayer.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <InputSystem/KeyCodes.h>
#include <InputSystem/SimpleInput.h>
#include <UI/UserInterface.h>
#include <Core/SimpleStandardLibrary.h>


static Application gApplication = {};

Application*
application_get()
{
    return &gApplication;
}

SimpleWindow*
application_get_window()
{
    return gApplication.Window;
}

void
application_set_window(SimpleWindow* pSimpleWindow)
{
    gApplication.Window = pSimpleWindow;
}

void
application_push_layer(Layer layer)
{
    vguard_not_null(layer.Name);

    TimeState timeState = {};
    profiler_start(&timeState);

    array_push(gApplication.Layers, layer);
    if (layer.OnAttach != NULL)
    {
        layer.OnAttach();
    }

    profiler_end(&timeState);
    GSUCCESS("PushLayer %s took %s\n", layer.Name ? layer.Name : "DEFAULT LAYER", profiler_get_string(&timeState));
}

void
application_create(ApplicationSettings appSet)
{
    GINFO("Current Directory: %s\n", path_get_current_directory());

    application_set_locale_utf8();

    gApplication.IsMinimized = 0;
    gApplication.IsRunning = 1;
    gApplication.IsGraphicsInit = appSet.IsInitGraphics;
    gApplication.IsFrameRatePrinted = appSet.IsFrameRatePrinted;
    gApplication.IsVsync = appSet.IsVsync;
    gApplication.Layers = NULL;

    RuntimeStatistics runtimeStat = {
        .CurrentTime = 0,
        .LastFrameTime = 0,
        .Timestep = 0.0f,
        .SecondElapsed = 0.0f,
        .FramesCount = 0,

        .UiFpsSyncRatio = Min(appSet.UiFpsSyncRatio, 16),
    };

    gApplication.Stats = runtimeStat;

    gApplication.Arguments = NULL;
    array_reserve(gApplication.Arguments, appSet.ArgumentsCount);
    for (i32 i = 0; i < appSet.ArgumentsCount; ++i)
    {
        char* argument = appSet.Arguments[i];
        array_push(gApplication.Arguments, argument);
    }

    // TODO(typedef): Add this as new graphics layer
    if (appSet.IsInitGraphics)
    {
        struct GraphicsLayerSettings gls = {
            .IsDebug = appSet.IsDebug,
            .IsVsync = gApplication.IsVsync,

            .Width = appSet.Width,
            .Height = appSet.Height,
            .Name = appSet.Name
        };
        graphics_layer_create((const struct GraphicsLayerSettings*) &gls);
    }
}

force_inline void
application_update_statistics()
{
    gApplication.Stats.CurrentTime = (f32) window_get_short_time();
    gApplication.Stats.Timestep = gApplication.Stats.CurrentTime - gApplication.Stats.LastFrameTime;
    gApplication.Stats.LastFrameTime = gApplication.Stats.CurrentTime;
    gApplication.Stats.SecondElapsed += gApplication.Stats.Timestep;
}

void
_application_graphics_main_loop()
{
    i32 layersCount = array_count(gApplication.Layers);

    while (gApplication.IsRunning)
    {
        application_update_statistics();

        // HACK(typedef): Change this for better ..
        //if (gApplication.Stats.Timestep > 1)
        //    usleep(1000);

        if (!gApplication.IsMinimized)
        {
            for (i32 l = 0; l < layersCount; ++l)
            {
                Layer layer = gApplication.Layers[l];
                if (layer.OnBeforeUpdate != NULL)
                    layer.OnBeforeUpdate();
            }

            for (i32 l = 0; l < layersCount; ++l)
            {
                Layer layer = gApplication.Layers[l];

                if (layer.Name != NULL && layer.OnUpdate != NULL)
                {
                    layer.OnUpdate(gApplication.Stats.Timestep);
                }
            }

            for (i32 l = 0; l < layersCount; ++l)
            {
                Layer layer = gApplication.Layers[l];
                if (layer.OnAfterUpdate != NULL)
                    layer.OnAfterUpdate();
            }


        }

        if (gApplication.Stats.SecondElapsed >= 1.0f)
        {
            RuntimeStatistics stats = application_get()->Stats;
            if (gApplication.IsFrameRatePrinted)
                GINFO("delay[ms s]: %fms %fs, %d frames\n", 1000 * stats.Timestep, stats.Timestep, stats.FramesCount);

            gApplication.Stats.SecondElapsed = 0.0f;
            gApplication.Stats.FramesCount = 0;
        }

        window_on_update(gApplication.Window);

        ++gApplication.Stats.FramesCount;
    }
}

void
_mock_usleep(size_t usec)
{
#ifdef LINUX_PLATFORM
    usleep(usec);
#else
    HANDLE timer;
    LARGE_INTEGER ft;

    ft.QuadPart = -(10 * usec); // Convert to 100 nanosecond interval, negative value indicates relative time

    timer = CreateWaitableTimer(NULL, TRUE, NULL);
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
    WaitForSingleObject(timer, INFINITE);
    CloseHandle(timer);
#endif
}

void
_application_non_graphics_main_loop()
{
    while (gApplication.IsRunning)
    {
        /*
          NOTE/BUG(typedef): temp solution
        */
        _mock_usleep(1 * 1000);

        if (gApplication.IsMinimized)
        {
            continue;
        }

        for (i32 l = 0; l < array_count(gApplication.Layers); ++l)
        {
            Layer layer = gApplication.Layers[l];

            if (layer.OnUpdate != NULL)
            {
                layer.OnUpdate(gApplication.Stats.Timestep);
            }
        }

    }
}

void
application_start()
{
    f32 secondTimer = 0.0f;
    i32 framesCount = 0,
        layersCount = array_count(gApplication.Layers);

    gApplication.IsRunning = 1;

    for (i32 l = 0; l < layersCount; ++l)
    {
        Layer layer = gApplication.Layers[l];
        if (layer.OnAttachFinished)
        {
            layer.OnAttachFinished();
        }
    }

    if (gApplication.IsGraphicsInit)
    {
        _application_graphics_main_loop();
    }
    else
    {
        _application_non_graphics_main_loop();
    }


}

void
application_on_event(struct Event* event)
{
    Layer layer;
    if (event->Category == EventCategory_Window)
    {
        if (event->Type == EventType_WindowResized)
        {
            WindowResizedEvent* wevent = (WindowResizedEvent*) event;

            if (wevent->Width == 0 || wevent->Height == 0)
            {
                gApplication.IsMinimized = 1;
            }
            else
            {
                gApplication.Window->AspectRatio = (f32) gApplication.Window->Width / gApplication.Window->Height;
                //GINFO("AspectRatio: %f\n", gApplication.Window->AspectRatio);
            }

        }
        else if (event->Type == EventType_WindowMinimized)
        {
            gApplication.IsMinimized = 1;
        }
        else if (event->Type == EventType_WindowRestored)
        {
            gApplication.IsMinimized = 0;
        }
    }

    i32 layersCount = array_count(gApplication.Layers);
    for (i32 l = 0; l < layersCount; l++)
    {
        layer = gApplication.Layers[l];
        if (layer.OnEvent != NULL && event->IsHandled == 0)
        {
            layer.OnEvent(event);
        }
    }
}

void
application_close()
{
    gApplication.IsRunning = 0;
}

void
application_end()
{
    vsa_device_wait_idle();

    {
        i32 l, layersCount = array_count(gApplication.Layers);
        for (l = 0; l < layersCount; ++l)
        {
            GINFO("Name: %s\n", gApplication.Layers[l].Name);
        }
    }

    Layer layer;
    i32 l, layersCount = array_count(gApplication.Layers);
    for (l = (layersCount - 1); l >= 0; --l)
    {
        layer = gApplication.Layers[l];
        if (layer.OnDestoy != NULL)
        {
            if (layer.Name != NULL)
            {
                GDEBUG("Destoying layer: %s\n", layer.Name);
            }

            layer.OnDestoy();
        }
    }

    array_free(gApplication.Layers);
}

void
application_set_locale_utf8()
{
    setlocale(LC_ALL, ".UTF8");
}
