typedef enum SimpleTokenType
{
    SimpleTokenType_None = 0,

    /*
      DOCS(): Operations
    */
    SimpleTokenType_Assign,

    /*
      DOCS(): Base language stuff
    */
    SimpleTokenType_EndOfExpression,
    SimpleTokenType_Identifier,

    /* DOCS(): Square bracket */
    SimpleTokenType_SquareBracket_Open,
    SimpleTokenType_SquareBracket_Close,
    /* DOCS(): Curly brackets */
    SimpleTokenType_CurlyBracket_Open,
    SimpleTokenType_CurlyBracket_Close,
    /* DOCS(): Round brackets */
    SimpleTokenType_RoundBracket_Open,
    SimpleTokenType_RoundBracket_Close,

    /*DOCS(): Defaults: Star, ampersand */
    SimpleTokenType_Star,
    SimpleTokenType_Ampersand,
    SimpleTokenType_Comma,

    /*DOCS(): Literals */
    SimpleTokenType_Literal_I32,
    SimpleTokenType_Literal_F32,
    SimpleTokenType_Literal_F64,
    SimpleTokenType_Literal_Char,
    SimpleTokenType_Literal_String,

    /*
      DOCS(): Keywords
    */
    SimpleTokenType_KeyWord_Return,
    SimpleTokenType_KeyWord_Typedef,
    SimpleTokenType_KeyWord_Const,

    SimpleTokenType_KeyWord_Type_Void,
    SimpleTokenType_KeyWord_Type_I8,
    SimpleTokenType_KeyWord_Type_I32,
    SimpleTokenType_KeyWord_Type_U8,
    SimpleTokenType_KeyWord_Type_U32,
    SimpleTokenType_KeyWord_Type_F32,
    SimpleTokenType_KeyWord_Type_Bool,
    SimpleTokenType_KeyWord_Type_Char,
    /* DOCS(): Loops */
    SimpleTokenType_KeyWord_Loop_For,
    SimpleTokenType_KeyWord_Loop_While,
    SimpleTokenType_KeyWord_Loop_Do,
    SimpleTokenType_KeyWord_Loop_Break,
    /* DOCS(): Stucts */
    SimpleTokenType_KeyWord_Struct_Enum,
    SimpleTokenType_KeyWord_Struct_Union,
    SimpleTokenType_KeyWord_Struct_Struct,


    SimpleTokenType_Count
} SimpleTokenType;

force_inline i32
simple_token_type_is_keyword(SimpleTokenType type)
{
    return (type >= SimpleTokenType_KeyWord_Return && type <= SimpleTokenType_KeyWord_Struct_Struct);
}

const char*
simple_token_type_to_string(SimpleTokenType tokenType)
{
    switch (tokenType)
    {
    case SimpleTokenType_None:
	return "SimpleTokenType_None";

    /*
      DOCS(): Operations
    */
    case SimpleTokenType_Assign:
	return "SimpleTokenType_Assign";

    /*
      DOCS(): Base language stuff
    */
    case SimpleTokenType_EndOfExpression:
	return "SimpleTokenType_EndOfExpression";
    case SimpleTokenType_Identifier:
	return "SimpleTokenType_Identifier";

	/* DOCS(): Square bracket */
    case SimpleTokenType_SquareBracket_Open:
	return "[";
    case SimpleTokenType_SquareBracket_Close:
	return "]";
	/* DOCS(): Curly brackets */
    case SimpleTokenType_CurlyBracket_Open:
	return "{";
    case SimpleTokenType_CurlyBracket_Close:
	return "}";
	/* DOCS(): Round brackets */
    case SimpleTokenType_RoundBracket_Open:
	return "(";
    case SimpleTokenType_RoundBracket_Close:
	return ")";

	/*DOCS(): Defaults: Star, ampersand */
    case SimpleTokenType_Star:
	return "SimpleTokenType_Star";
    case SimpleTokenType_Ampersand:
	return "SimpleTokenType_Ampersand";
    case SimpleTokenType_Comma:
	return "SimpleTokenType_Comma";

	/*DOCS(): Literals */
    case SimpleTokenType_Literal_I32:
	return "SimpleTokenType_Literal_I32";
    case SimpleTokenType_Literal_F32:
	return "SimpleTokenType_Literal_F32";
    case SimpleTokenType_Literal_F64:
	return "SimpleTokenType_Literal_F64";
    case SimpleTokenType_Literal_Char:
	return "SimpleTokenType_Literal_Char";
    case SimpleTokenType_Literal_String:
	return "SimpleTokenType_Literal_String";

	/*DOCS(): Keywords */
    case SimpleTokenType_KeyWord_Return:
	return "return";
    case SimpleTokenType_KeyWord_Typedef:
	return "typedef";
    case SimpleTokenType_KeyWord_Const:
	return "const";

    case SimpleTokenType_KeyWord_Type_Void:
	return "void";
    case SimpleTokenType_KeyWord_Type_I8:
	return "i8";
    case SimpleTokenType_KeyWord_Type_I32:
	return "i32";
    case SimpleTokenType_KeyWord_Type_U8:
	return "u8";
    case SimpleTokenType_KeyWord_Type_U32:
	return "u32";
    case SimpleTokenType_KeyWord_Type_F32:
	return "f32";
    case SimpleTokenType_KeyWord_Type_Bool:
	return "bool";
    case SimpleTokenType_KeyWord_Type_Char:
	return "char";

	/* DOCS(): Loops */
    case SimpleTokenType_KeyWord_Loop_For:
	return "for";
    case SimpleTokenType_KeyWord_Loop_While:
	return "while";
    case SimpleTokenType_KeyWord_Loop_Do:
	return "do";
    case SimpleTokenType_KeyWord_Loop_Break:
	return "break";

	/* DOCS(): Stucts */
    case SimpleTokenType_KeyWord_Struct_Enum:
	return "enum";
    case SimpleTokenType_KeyWord_Struct_Union:
	return "union";
    case SimpleTokenType_KeyWord_Struct_Struct:
	return "struct";

    }

    vassert_break();
    return "";
}

typedef struct SimpleToken
{
    SimpleTokenType Type;
    union
    {
	const char* Source;
	i64 SourceInt;
	u64 SourceUint;
	f64 SourceDouble;
    };
} SimpleToken;

char*
parser_skip_empty(char* ptr)
{
    char pC = *ptr;
    while (pC == ' ' || pC == '\n' || pC == '\t' || pC == '\r')
    {
	++ptr;
	pC = *ptr;
    }
    return ptr;
}

char*
parser_skip_until_char(char* ptr, char ch)
{
    char* iter = ptr + 1;
    char c = *iter;

    while (c != ch)
    {
	if (c == '\0')
	    return NULL;
	++iter;
	c = *iter;
    }

    return iter;
}

i32
parser_is_keyword(char* ptr, const char** source, SimpleTokenType* tokenType, i32* tokenLength)
{
    for (i32 i = SimpleTokenType_KeyWord_Return;
	 i <= SimpleTokenType_KeyWord_Struct_Struct;
	 ++i)
    {
	const char* keywordStr = simple_token_type_to_string(i);
	i32 keyWordLength = string_length(keywordStr);
	if (string_compare_length(ptr, keywordStr, keyWordLength))
	{
	    *tokenType = (SimpleTokenType) i;
	    *tokenLength = keyWordLength;
	    *source = istring(keywordStr);
	    return 1;
	}
    }

    tokenType = NULL;
    return 0;
}

force_inline i32
parser_is_operation(char c)
{
    return c == '+' || c == '-' || c == '*'|| c == '/' || c == '%';
}
force_inline i32
parser_is_char_brackets(char c)
{
    return c == '(' || c == ')' || c == '['|| c == ']' || c == '{' || c == '}';
}

force_inline i32
parser_is_end_of_stream(char c)
{
    return c == '\0';
}

i32
parser_is_identifier(char* ptr, const char** identifier, SimpleTokenType* tokenType, i32* tokenLength)
{
    i32 isFirstChar = 1;
    char c = *ptr;
    char* start = ptr;

    while (!parser_is_end_of_stream(c)

	   /* DOCS(): Variable identifier */
	   && c != ' ' && c != ';' && c != ','
	   && c != '='
	   && !parser_is_operation(c)

	   /* DOCS(): Function, array identifier or */
	   && !parser_is_char_brackets(c)
	)
    {

	switch (c)
	{
	case 'a' ... 'z':
	case 'A' ... 'Z':
	    break;

	case '0' ... '9':
	case '_':
	    if (isFirstChar)
		return 0;
	    break;

	default:
	    return 0;
	}

	++ptr;
	c = *ptr;

	isFirstChar = 0;
    }

    if (ptr == start)
	return 0;

    i32 len = ptr - start;
    *identifier = (const char*) string_substring_length(start, len + 1, len);
    *tokenType = SimpleTokenType_Identifier;
    *tokenLength = len;

    return 1;
}

i32
parser_is_end_of_expression(char* ptr, const char** source, SimpleTokenType* tokenType, i32* tokenLength)
{
    char c = *ptr;
    if (c != ';')
    {
	return 0;
    }

    *source = ";";
    *tokenType = SimpleTokenType_EndOfExpression;
    *tokenLength = 1;

    return 1;
}

force_inline i32
parser_is_brackets(char* ptr, const char** source, SimpleTokenType* tokenType, i32* tokenLength)
{
    char c = *ptr;

    switch (c)
    {
    case '[':
	*source = "[";
	*tokenType = SimpleTokenType_SquareBracket_Open;
	*tokenLength = 1;
	return 1;

    case ']':
	*source = "]";
	*tokenType = SimpleTokenType_SquareBracket_Close;
	*tokenLength = 1;
	return 1;

    case '(':
	*source = "(";
	*tokenType = SimpleTokenType_RoundBracket_Open;
	*tokenLength = 1;
	return 1;

    case ')':
	*source = ")";
	*tokenType = SimpleTokenType_RoundBracket_Close;
	*tokenLength = 1;
	return 1;

    case '{':
	*source = "{";
	*tokenType = SimpleTokenType_CurlyBracket_Open;
	*tokenLength = 1;
	return 1;

    case '}':
	*source = "}";
	*tokenType = SimpleTokenType_CurlyBracket_Close;
	*tokenLength = 1;
	return 1;

    }

    return 0;
}

force_inline i32
parser_is_digit(char c)
{
    return c >= '0' && c <= '9';
}
force_inline i32
parser_is_digit_or_component(char c)
{
    return parser_is_digit(c) || c == '_' || c == '.' || c == 'f';
}

i32
parser_is_literal(char* ptr, const char** source, SimpleTokenType* tokenType, i32* tokenLength)
{
    char c = *ptr;

    switch (c)
    {
    case '\"':
    {
	char* iter = parser_skip_until_char(ptr, '\"');
	if (iter == NULL)
	    return 0;

	size_t len = iter - ptr - 1;
	GINFO("LENGRT: %d\n", len);
	vguard(len > 0 && len < 1024 && "String length might be 0 < len < 1024!");
	*source = string_copy(ptr+1, len);
	*tokenType = SimpleTokenType_Literal_String;
	*tokenLength = len + 2;

	return 1;
    }

    case '\'':
    {
	char tc = *(ptr + 2);
	if (tc != '\'')
	{
	    GERROR("Lexical error wrong character literal usage!\n");
	    return 0;
	}

	*source = string_copy(ptr + 1, 1);
	*tokenType = SimpleTokenType_Literal_Char;
	*tokenLength = 3;

	return 1;
    }

    case '0' ... '9':
    {
	SimpleTokenType literType = SimpleTokenType_Literal_I32;
	char* iter = ptr;
	char c = *iter;

	while (c != ';'
	       || c != ' '
	       && parser_is_digit_or_component(c))
	{

	    if (c == '.')
	    {
		literType = SimpleTokenType_Literal_F64;
	    }
	    else if (c == 'f')
	    {
		literType = SimpleTokenType_Literal_F32;
	    }

	    ++iter;
	    c = *iter;
	}

	i32 len = iter - ptr;

	*source = string_copy(ptr, len);
	*tokenType = literType;
	*tokenLength = len;

	return 1;
    }

    }

    return 0;
}

i32
parser_is_other(char* ptr, const char** source, SimpleTokenType* tokenType, i32* tokenLength)
{
    char c = *ptr;

    switch (c)
    {
    case '*':
	*source = istring("*");
	*tokenType = SimpleTokenType_Star;
	*tokenLength = 1;
	return 1;

    case '&':
	*source = istring("&");
	*tokenType = SimpleTokenType_Ampersand;
	*tokenLength = 1;
	return 1;

    case ',':
	*source = istring(",");
	*tokenType = SimpleTokenType_Comma;
	*tokenLength = 1;
	return 1;
    }

    return 0;
}

SimpleToken*
simple_compiler_lexer_parse(char* stream)
{
    SimpleToken* tokens = NULL;

#define AddToken(source, tokenType, tokenLength)	\
    {							\
	SimpleToken token = {				\
	    .Type = tokenType,				\
	    .Source = source				\
	};						\
	array_push(tokens, token);			\
	ptr += tokenLength;				\
	c = *ptr;					\
    }


    char* ptr = stream;
    char c = *ptr;
    while (c != '\0')
    {
	ptr = parser_skip_empty(ptr);

	SimpleTokenType tokenType;
	i32 tokenLength;
	const char* source = NULL;
	if (parser_is_keyword(ptr, &source, &tokenType, &tokenLength))
	{
	    //GINFO("Keyword: %s Length: %d\n", simple_token_type_to_string(tokenType), tokenLength);
	    AddToken(source, tokenType, tokenLength);
	}
	else if (parser_is_end_of_expression(ptr, &source, &tokenType, &tokenLength))
	{
	    AddToken(source, tokenType, tokenLength);
	}
	else if (parser_is_brackets(ptr, &source, &tokenType, &tokenLength))
	{
	    AddToken(source, tokenType, tokenLength);
	}
	else if (parser_is_literal(ptr, &source, &tokenType, &tokenLength))
	{
	    AddToken(source, tokenType, tokenLength);
	}
	else if (parser_is_identifier(ptr, &source, &tokenType, &tokenLength))
	{
	    //GINFO("Identifier: %s Length: %d\n", source, tokenLength);
	    AddToken(source, tokenType, tokenLength);
	}
	else if (parser_is_other(ptr, &source, &tokenType, &tokenLength))
	{
	    AddToken(source, tokenType, tokenLength);
	}
	else
	{
	    ++ptr;
	    c = *ptr;
	}

    }

    return tokens;
}

/*
  NOTE(typedef): needs to be called inside *_on_ui()
  requires #include <UI/Suif.h>
*/
void
sui_compiler_demo()
{
    static char* CodeInCDotAny = NULL;
    if (CodeInCDotAny == NULL)
    {
	CodeInCDotAny = file_read_string("/home/bies/Data/programming/C/__SimpleEngine/Projects/VulkanSandbox/src/CodeInC.any");
	vguard_not_null(CodeInCDotAny);
    }

    static i32 isAnotherPanelVisible = 1;
    static SuiPanelFlags woHdrFlag = SuiPanelFlags_WithOutHeader | SuiPanelFlags_Movable;
    static WideString compilerPanel = {
	.Buffer = L"Панель компилятора",
	.Length = 18
    };
    if (sui_panel_begin(&compilerPanel, &isAnotherPanelVisible, woHdrFlag))
    {

	static WideString buttonLabel = {
	    .Buffer = L"Произвести лексический анализ",
		.Length = 29
	};
	static SimpleToken* tokens = NULL;
	if (sui_button(&buttonLabel, v2_new(0,0)) == SuiState_Clicked)
	{
	    GINFO("Лексический анализ\n");
	    if (tokens != NULL)
	    {
		array_free(tokens);
	    }

	    tokens =
		simple_compiler_lexer_parse(CodeInCDotAny);
	    array_foreach(tokens,
			  GINFO("Keyword: %s\n",
				simple_token_type_to_string(item.Type)););
	}

	for (i32 i = 0; i < array_count(tokens); ++i)
	{
	    SimpleToken token = tokens[i];

	    if (simple_token_type_is_keyword(token.Type))
	    {
		static WideString keywordText = {
		    .Buffer = L"%d [KeyWord] Token: %s",
		    .Length = 19
		};

		sui_text(&keywordText,
			 i,
			 simple_token_type_to_string(token.Type));
	    }
	    else
	    {
		static WideString wText = {
		    .Buffer = L"%d Token: %s Source: %s",
		    .Length = 20
		};

		sui_text(&wText,
			 i,
			 simple_token_type_to_string(token.Type),
			 token.Source);
	    }
	}

    }
    sui_panel_end();

}
