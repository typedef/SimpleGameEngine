#include "GeneratedModel.h"

#include "ModelBase.h"
#include "StaticModel.h"
#include <Math/SimpleMath.h>
#include <Core/SimpleStandardLibrary.h>

static const f32 vp = 0.5f;
static const f32 dv = 2 * vp;

static v3 DefaultCubePositions[24] = {
    // Front
    //0             1          2          3
    {-vp,vp,vp}, {vp,vp,vp}, {vp,-vp,vp}, {-vp,-vp,vp},

    // Back
    //   4             5            6              7
    {-vp,vp,-vp}, {vp,vp,-vp}, {vp,-vp,-vp}, {-vp,-vp,-vp},

    // Left
    //   8             9             10            11
    {-vp,vp,-vp}, {-vp,vp,vp}, {-vp,-vp,vp}, {-vp,-vp,-vp},

    // Right
    //  12           14           13           15
    {vp,vp,-vp}, {vp,vp,vp}, {vp,-vp,vp}, {vp,-vp,-vp},
    //{ vp,vp,vp},{vp,-vp,-vp}, {vp,-vp,vp},  {vp,vp,-vp},

    // Up
    //   16           17           18           19
    {-vp,vp,-vp}, {vp,vp,-vp}, {vp,vp,vp}, {-vp,vp,vp},

    // Bottom
    //   20           21           22           23
    {-vp,-vp,-vp}, {vp,-vp,-vp}, {vp,-vp,vp}, {-vp,-vp,vp},

};

static v3 DefaultCubeNormals[24] = {
    // Front
    //0        1          2           3
    {0, 0, 1},{0, 0, 1}, {0, 0, 1},  {0, 0, 1},

    // Back
    //4         5           6            7
    {0, 0, -1},{0, 0, -1}, {0, 0, -1},  {0, 0, -1},

    // Left
    //8          9           10           11
    {-1, 0, 0},{-1, 0, 0}, {-1, 0, 0},  {-1, 0, 0},

    // Right
    //12       13         14          15
    {1, 0, 0},{1, 0, 0}, {1, 0, 0},  {1, 0, 0},

    // Up
    //16        17          18          19
    {0, 1, 0},{0, 1, 0}, {0, 1, 0},  {0, 1, 0},

    // Bottom
    //  20         21          22           23
    {0, -1, 0},{0, -1, 0}, {0, -1, 0},  {0, -1, 0},
};

static v2 DefaultCubeUV[4] = {
    { 0, 1 },{ 1, 0 }, { 1, 1 }, { 0, 0 },
};

static u32 DefaultCubeIndices[36] = {
    // Front - CCW
    0, 3, 2, 2, 1, 0,

    // Back - CW
    4, 5, 6, 6, 7, 4,

    // LEFT - CW
    8 , 11, 9 , 9 , 11, 10,

    // RIGHT - CCW
    12, 13, 15, 15, 13, 14,

    // UP - CCW
    16, 19, 18, 18, 17, 16,

    // BOTTOM - CW
    20, 21, 22, 22, 23, 20,
};

StaticModel
static_model_cube_create()
{
    StaticModel model = {
        .Base = {
            .Type = ModelType_Generated_Cube,
            .Name = wide_string(L"Cube"),
        },
        .aMeshes = NULL,
        .Aabb = (ModelAabb) {
            .Min = v3_new(-vp, -vp, -vp),
            .Max = v3_new(+vp, +vp, +vp),
        },
    };

    StaticMeshVertex* vertices = NULL;
    for (i32 v = 0; v < 24; ++v)
    {
        StaticMeshVertex vertex = {
            .Position = DefaultCubePositions[v],
            .Normal = DefaultCubeNormals[v],
            .Uv = DefaultCubeUV[v % 4]
        };
        array_push(vertices, vertex);
    }

    u32* indices = NULL;
    array_for(36, array_push(indices, DefaultCubeIndices[i]););

    StaticMesh mesh = {
        .IsVisible = 1,
        .Name = model.Base.Name,
        .Indices = indices,
        .Vertices = vertices,
        .MaterialId = 0
    };

    array_push(model.aMeshes, mesh);

    return model;
}
