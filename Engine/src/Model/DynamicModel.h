#ifndef DYNAMIC_MODEL_H
#define DYNAMIC_MODEL_H

#include "ModelBase.h"
#include <Core/Types.h>

typedef struct DynamicMeshVertex
{
    v3 Position;
    v3 Normal;
    v2 Uv;
    v4i Joint;
    v4 Weight;
} DynamicMeshVertex;

typedef struct DynamicMesh
{
    i32 IsVisible;
    i64 MaterialId;

    v3 Position;
    ModelAabb Borders;
    WideString Name;

    u32* Indices;
    DynamicMeshVertex* Vertices;
} DynamicMesh;

typedef struct DynamicModel
{
    struct ModelBase Base;
    DynamicMesh* aMeshes;
} DynamicModel;

DynamicModel dynamic_model_load(const char* path);
i64 dynamic_model_get_vertices_count(const DynamicModel* pStaticModel);

#endif // DYNAMIC_MODEL_H
