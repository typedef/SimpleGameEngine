#include "ModelBase.h"

#include <AssetManager/AssetManager.h>
#include <Math/SimpleMath.h>
#include <Core/SimpleStandardLibrary.h>

#pragma clang diagnostic ignored "-Wswitch"

char*
_get_path_from_incorrect_path(char* path, i32 length)
{
    char* result;
    if (string_index_of_string(path, "%20"))
        result = string_replace_string(path, length, "%20", 3, " ", 1);
    else
        result = string(path);

    return result;
}


VsaTextureId
mesh_texture_create(cgltf_texture* texture, const char* path)
{
    if (!texture
        || !path
        //|| !texture->image->buffer_view
        || !texture->image->uri)
    {
        return -1;
    }

    char* correctUri = _get_path_from_incorrect_path(texture->image->uri, string_length(texture->image->uri));
    char* finalPath = path_combine_directory_and_name(path, correctUri);

    if (path == NULL)
    {
        GERROR("Path is wrong: %s\n", path);
        vassert(0 && "Mesh Texture Path is NULL!");
        return -1;
    }

    i64 textureId = asset_manager_load_texture_id(finalPath);

    memory_free(correctUri);
    memory_free(finalPath);

    return textureId;
}

/*
  Allocates indices array, need to be free
*/
u32*
mesh_get_indices(cgltf_primitive primitive)
{
    u32* indices = NULL;
    array_reserve(indices, primitive.indices->count);

    {
        cgltf_accessor* indicesAccessor = primitive.indices;
        i32 trianglesCount = (i32) (primitive.indices->count / 3);

        /*
          TODO(typedef): Rewrite it
        */
        switch (indicesAccessor->component_type)
        {

        case cgltf_component_type_r_16u:
        {
            size_t size = sizeof(u16);
            u16* buffer =
                (u16*) indicesAccessor->buffer_view->buffer->data
                + indicesAccessor->buffer_view->offset / size
                + indicesAccessor->offset / size;
            i32 ind = 0;
            for (i32 c = 0; c < indicesAccessor->count; ++c)
            {
                indices[c] = (u32) buffer[ind];
                ++ind /* += (i32) (indicesAccessor->stride / size) */;
            }
            break;
        }

        case cgltf_component_type_r_32u:
        {
            size_t size = sizeof(u32);
            u32* buffer =
                (u32*) indicesAccessor->buffer_view->buffer->data
                + indicesAccessor->buffer_view->offset / size
                + indicesAccessor->offset / size;
            i32 ind = 0;
            for (i32 c = 0; c < indicesAccessor->count; ++c)
            {
                indices[c] = (u32) buffer[ind];
                ++ind;
            }
            break;
        }

        default:
            vassert(0 && "ComponentType Unknown!!!");
            break;
        }

#if 0
        for (i32 i = 0; i < indicesAccessor->count; ++i)
        {
            printf("%d ", indices[i]);
            if (i > 0 && i % 17 == 0)
                printf("\n");
        } printf("\n");
#endif

    }

    array_header(indices)->Count = primitive.indices->count;

    return indices;
}

PbrMetallic
material_pbr_metallic(cgltf_pbr_metallic_roughness pbrMetallic, const char* materialPath)
{
    cgltf_texture* baseColorTexture =
        pbrMetallic.base_color_texture.texture;
    cgltf_texture* metallicTexture =
        pbrMetallic.metallic_roughness_texture.texture;

    PbrMetallic metallic = {
        .BaseColorTexture = mesh_texture_create(baseColorTexture, materialPath),
        .MetallicRoughnessTexture = mesh_texture_create(metallicTexture, materialPath),
        .BaseColor = v4_array_w(pbrMetallic.base_color_factor, 1.0f),
        .MetallicFactor = pbrMetallic.metallic_factor,
        .RoughnessFactor = pbrMetallic.roughness_factor
    };

    return metallic;
}

PbrSpecular
material_pbr_specular(cgltf_specular specular, const char* materialPath)
{
    PbrSpecular pbrSpecular = {
        .Factor = specular.specular_factor,
        .ColorFactor = v3_new(specular.specular_color_factor[0], specular.specular_color_factor[1], specular.specular_color_factor[2]),
        .Texture = mesh_texture_create(specular.specular_texture.texture, materialPath),
        .ColorTexture = mesh_texture_create(specular.specular_color_texture.texture, materialPath)
    };

    return pbrSpecular;
}

PbrSpecularGlossiness
material_pbr_specular_glossiness(cgltf_pbr_specular_glossiness psg, const char* materialPath)
{
    PbrSpecularGlossiness pbrSpecularGlossiness = {
        .GlossinessFactor = psg.glossiness_factor,
        .SpecularFactor = v3_new(psg.specular_factor[0], psg.specular_factor[1], psg.specular_factor[2]),
        .DiffuseFactor = v4_new(psg.diffuse_factor[0], psg.diffuse_factor[1], psg.diffuse_factor[2], psg.diffuse_factor[3]),
        .DiffuseTexture = mesh_texture_create(psg.diffuse_texture.texture, materialPath),
        .SpecularGlossinessTexture = mesh_texture_create(psg.specular_glossiness_texture.texture, materialPath),
    };

    return pbrSpecularGlossiness;

}

PbrClearCoat
material_clear_coat(cgltf_clearcoat cc, const char* materialPath)
{
    PbrClearCoat pcc = {
        .ClearcoatFactor = cc.clearcoat_factor,
        .ClearcoatRoughnessFactor = cc.clearcoat_roughness_factor,
        .ClearcoatTexture = mesh_texture_create(cc.clearcoat_texture.texture, materialPath),
        .ClearcoatRoughnessTexture = mesh_texture_create(cc.clearcoat_roughness_texture.texture, materialPath),
        .ClearcoatNormalTexture = mesh_texture_create(cc.clearcoat_normal_texture.texture, materialPath)
    };

    return pcc;
}

PbrTransmission
material_transmission(cgltf_transmission transmission, const char* materialPath)
{
    PbrTransmission pbrTransmission = {
        .TransmissionFactor = transmission.transmission_factor,
        .TransmissionTexture = mesh_texture_create(
            transmission.transmission_texture.texture, materialPath)
    };

    return pbrTransmission;
}

PbrVolume
material_volume(cgltf_volume volume, const char* materialPath)
{
    PbrVolume pbrVolume = {
        .ThicknessFactor = volume.thickness_factor,
        .AttenuationDistance = volume.attenuation_distance,
        .AttenuationColor = v3_new(volume.attenuation_color[0], volume.attenuation_color[1], volume.attenuation_color[2]),
        .ThicknessTexture  = mesh_texture_create(volume.thickness_texture.texture, materialPath)
    };

    return pbrVolume;
}

PbrSheen
material_sheen(cgltf_sheen sheen, const char* materialPath)
{
    PbrSheen pbrSheen = {
        .SheenRoughnessFactor = sheen.sheen_roughness_factor,
        .SheenColorFactor = v3_new(sheen.sheen_color_factor[0], sheen.sheen_color_factor[1], sheen.sheen_color_factor[2]),
        .SheenColorTexture = mesh_texture_create(sheen.sheen_color_texture.texture, materialPath),
        .SheenRoughnessTexture = mesh_texture_create(sheen.sheen_roughness_texture.texture, materialPath),

    };

    return pbrSheen;
}

PbrEmissive
material_emissive(cgltf_emissive_strength emissiveStrength, cgltf_texture_view emissiveTexture, cgltf_float emissiveFactor[3], const char* materialPath)
{
    PbrEmissive pbrEmissive = {
        .Strength = emissiveStrength.emissive_strength,
        .Factor = v3_new(emissiveFactor[0], emissiveFactor[1], emissiveFactor[2]),
        .Texture = mesh_texture_create(emissiveTexture.texture, materialPath)
    };

    return pbrEmissive;
}

PbrIridescence
material_iridescence(cgltf_iridescence iridescence, const char* materialPath)
{
    PbrIridescence pbrIridescence = {
        .Factor = iridescence.iridescence_factor,
        .Ior = iridescence.iridescence_ior,
        .ThicknessMin = iridescence.iridescence_thickness_min,
        .ThicknessMax = iridescence.iridescence_thickness_max,
        .Texture = mesh_texture_create(
            iridescence.iridescence_texture.texture, materialPath),
        .ThicknessTexture = mesh_texture_create(
            iridescence.iridescence_thickness_texture.texture, materialPath),
    };

    return pbrIridescence;
}

PbrAlpha
material_alpha(cgltf_alpha_mode alphaMode, cgltf_float alphaCutoff, const char* materialPath)
{
    PbrAlphaModeType type;

    switch (alphaMode)
    {

    case cgltf_alpha_mode_opaque:
        type = PbrAlphaModeType_Opaque;
        break;

    case cgltf_alpha_mode_mask:
        type = PbrAlphaModeType_Mask;
        break;

    case cgltf_alpha_mode_blend:
        type = PbrAlphaModeType_Blend;
        break;

    }

    PbrAlpha pbrAlpha = {
        .Mode = type,
        .CutOff = alphaCutoff
    };

    return pbrAlpha;
}

v2*
model_base_get_v2_from_attribute(cgltf_attribute attribute, v2* resultArray)
{
    size_t itemSize = sizeof(f32);
    f32* buffer = (f32*) attribute.data->buffer_view->buffer->data
        + attribute.data->buffer_view->offset / itemSize
        + attribute.data->offset / itemSize;

    i32 attributesCount = (i32) (attribute.data->stride / itemSize);

    i32 ind = 0;
    for (i32 c = 0; c < attribute.data->count; ++c)
    {
        v2 v2Value = v2_new(buffer[ind + 0], buffer[ind + 1]);
        array_push(resultArray, v2Value);

        ind += attributesCount;
    }

    return resultArray;
}

v3*
model_base_get_v3_from_attribute(cgltf_attribute attribute, v3* resultArray)
{
    f32* buffer = (f32*) attribute.data->buffer_view->buffer->data
        + attribute.data->buffer_view->offset / sizeof(f32)
        + attribute.data->offset / sizeof(f32);

    i32 attributesCount = (i32) (attribute.data->stride / sizeof(f32));

    i32 ind = 0;
    for (i32 c = 0; c < attribute.data->count; ++c)
    {
        v3 v3Value = v3_new(buffer[ind + 0], buffer[ind + 1], buffer[ind + 2]);
        array_push(resultArray, v3Value);

        ind += attributesCount;
    }

    return resultArray;
}

v4*
model_base_get_v4_from_attribute(cgltf_attribute attribute, v4* resultArray)
{
    f32* buffer = (f32*) attribute.data->buffer_view->buffer->data
        + attribute.data->buffer_view->offset / sizeof(f32)
        + attribute.data->offset / sizeof(f32);

    i32 attributesCount = (i32) (attribute.data->stride / sizeof(f32));

    i32 ind = 0;
    for (i32 c = 0; c < attribute.data->count; ++c)
    {
        v4 v4Value = v4_new(buffer[ind + 0], buffer[ind + 1], buffer[ind + 2], buffer[ind + 3]);
        array_push(resultArray, v4Value);

        ind += attributesCount;
    }

    return resultArray;
}

v4i*
model_base_get_joints_from_attribute(cgltf_attribute attribute, v4i* resultArray)
{
    size_t itemSize = sizeof(u16);
    u16* buffer = (u16*) attribute.data->buffer_view->buffer->data
        + attribute.data->buffer_view->offset / itemSize
        + attribute.data->offset / itemSize;

    i32 attributesCount = (i32) (attribute.data->stride / itemSize);

    i32 ind = 0;
    for (i32 c = 0; c < attribute.data->count; ++c)
    {
        v4i v4v = v4i_new(buffer[ind + 0], buffer[ind + 1], buffer[ind + 2], buffer[ind + 3]);
        array_push(resultArray, v4v);

        ind += attributesCount;
    }

    return resultArray;
}

const char*
model_base_type_to_string(cgltf_type type)
{
    const char* cgltfTypeToStr[] = {
        [cgltf_type_invalid] = TO_STRING(cgltf_type_invalid),
        [cgltf_type_scalar] = TO_STRING(cgltf_type_scalar),
        [cgltf_type_vec2] = TO_STRING(cgltf_type_vec2),
        [cgltf_type_vec3] = TO_STRING(cgltf_type_vec3),
        [cgltf_type_vec4] = TO_STRING(cgltf_type_vec4),
        [cgltf_type_mat2] = TO_STRING(cgltf_type_mat2),
        [cgltf_type_mat3] = TO_STRING(cgltf_type_mat3),
        [cgltf_type_mat4] = TO_STRING(cgltf_type_mat4)
    };

    i8 isOk = (type >= cgltf_type_invalid || type <= cgltf_type_mat4);
    vguard(isOk && "Wrong argument to model_base_type_to_string!");

    return cgltfTypeToStr[type];
}

const char*
model_base_component_type_to_string(cgltf_component_type type)
{
    const char* cgltfComponentTypeToStr[] = {
        [cgltf_component_type_invalid] = TO_STRING(cgltf_component_type_invalid),
        [cgltf_component_type_r_8] = TO_STRING(cgltf_component_type_r_8),
        [cgltf_component_type_r_8u] = TO_STRING(cgltf_component_type_r_8u),
        [cgltf_component_type_r_16] = TO_STRING(cgltf_component_type_r_16),
        [cgltf_component_type_r_16u] = TO_STRING(cgltf_component_type_r_16u),
        [cgltf_component_type_r_32u] = TO_STRING(cgltf_component_type_r_32u),
        [cgltf_component_type_r_32f] = TO_STRING(cgltf_component_type_r_32f),
        [cgltf_component_type_max_enum] = TO_STRING(cgltf_component_type_max_enum)
    };

    i8 isOk = (type >= cgltf_component_type_invalid || type <= cgltf_component_type_max_enum);
    vguard(isOk && "Wrong argument to model_base_component_type_to_string!");

    return cgltfComponentTypeToStr[type];
}

const char*
model_base_result_to_string(cgltf_result result)
{

    const char* cgltfResultToStr[] = {
        [cgltf_result_success] = TO_STRING(cgltf_result_success),
        [cgltf_result_unknown_format] = TO_STRING(cgltf_result_unknown_format),
        [cgltf_result_invalid_json] = TO_STRING(cgltf_result_invalid_json),
        [cgltf_result_invalid_gltf] = TO_STRING(cgltf_result_invalid_gltf),
        [cgltf_result_invalid_options] = TO_STRING(cgltf_result_invalid_options),
        [cgltf_result_file_not_found] = TO_STRING(cgltf_result_file_not_found),
        [cgltf_result_io_error] = TO_STRING(cgltf_result_io_error),
        [cgltf_result_out_of_memory] = TO_STRING(cgltf_result_out_of_memory),
        [cgltf_result_legacy_gltf] = TO_STRING(cgltf_result_legacy_gltf),
    };

    i8 isOk = (result >= cgltf_result_success || result <= cgltf_result_legacy_gltf);
    vguard(isOk && "Wrong argument to model_base_result_to_string!");

    return cgltfResultToStr[result];
}

size_t
model_base_attribute_get_size(cgltf_component_type type)
{
    switch (type)
    {

    case cgltf_component_type_r_8 /* BYTE */: return sizeof(i8);
    case cgltf_component_type_r_8u /* UNSIGNED_BYTE */: return sizeof(u8);
    case cgltf_component_type_r_16 /* SHORT */: return sizeof(i16);
    case cgltf_component_type_r_16u /* UNSIGNED_SHORT */: return sizeof(u16);
    case cgltf_component_type_r_32u /* UNSIGNED_INT */: return sizeof(u32);
    case cgltf_component_type_r_32f /* FLOAT */: return sizeof(f32);

    default: vguard(0 && "default: model_base_attribute_get_size"); return 0;

    }
}

i32
model_base_attribute_get_count(cgltf_type type)
{
    switch (type)
    {

    case cgltf_type_scalar: return 1;
    case cgltf_type_vec2: return 2;
    case cgltf_type_vec3: return 3;
    case cgltf_type_vec4: return 4;
    case cgltf_type_mat2: return 4;
    case cgltf_type_mat3: return 9;
    case cgltf_type_mat4: return 16;

    default: vguard(0 && "default: model_base_attribute_get_count"); return 0;

    }
}

MeshMaterial
model_base_get_mesh_material(cgltf_material* pMat, const char* path, const char* meshName)
{
    MeshMaterial material = {};

    if (pMat->name)
    {
        material.Name = string(pMat->name);
    }
    else
    {
        material.Name = string_concat(meshName, "_Material");
    }
    vguard_not_null(material.Name);

    if (pMat->has_pbr_metallic_roughness)
    {
        material.IsMetallicExist = 1;
        material.Metallic =
            material_pbr_metallic(pMat->pbr_metallic_roughness, path);
    }

    if (pMat->has_specular)
    {
        material.IsSpecularExist = 1;
        material.Specular = material_pbr_specular(pMat->specular, path);
    }

    if (pMat->has_pbr_specular_glossiness)
    {
        material.IsSpecularGlossinessExist = 1;
        material.SpecularGlossiness =
            material_pbr_specular_glossiness(
                pMat->pbr_specular_glossiness, path);
    }

    if (pMat->has_clearcoat)
    {
        material.IsClearCoatExist = 1;
        material.ClearCoat =
            material_clear_coat(
                pMat->clearcoat, path);
    }

    if (pMat->has_transmission)
    {
        material.IsTransmissionExist = 1;
        material.Transmission =
            material_transmission(
                pMat->transmission, path);
    }

    if (pMat->has_volume)
    {
        material.IsVolumeExist = 1;
        material.Volume =
            material_volume(
                pMat->volume, path);
    }

    if (pMat->has_ior)
    {
        material.IsIorExist = 1;
        material.Ior = pMat->ior.ior;
    }

    if (pMat->has_sheen)
    {
        material.IsSheenExist = 1;
        material.Sheen =
            material_sheen(
                pMat->sheen, path);
    }

    if (pMat->has_emissive_strength)
    {
        material.IsVolumeExist = 1;
        material.Emissive =
            material_emissive(
                pMat->emissive_strength,
                pMat->emissive_texture,
                pMat->emissive_factor,
                path);
    }

    if (pMat->has_iridescence)
    {
        material.IsIridescenceExist = 1;
        material.Iridescence =
            material_iridescence(
                pMat->iridescence, path);
    }

    if (pMat->normal_texture.texture)
    {
        material.Normal = mesh_texture_create(pMat->normal_texture.texture, path);
    }

    if (pMat->occlusion_texture.texture)
    {
        material.Occlusion = mesh_texture_create(pMat->occlusion_texture.texture, path);
    }

    return material;
}
