#include "StaticModel.h"

#include <stdio.h>

#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Core/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>
#include <Math/SimpleMathIO.h>
#include <AssetManager/AssetManager.h>
#include <Utils/cgltf.h>


#pragma clang diagnostic ignored "-Wswitch"

StaticMesh
static_mesh_load(i32* isModelDownscaled, cgltf_mesh gltfMesh, const char* path)
{
    vassert_not_null(gltfMesh.name);

    StaticMesh mesh = {
        .Vertices = NULL,
        .Indices  = NULL,
        //.Material = (MeshMaterial) {0},
        .MaterialId = 0,
        .Name = wide_string_utf8(gltfMesh.name),
        .IsVisible = 1
    };

    // Copy From
    v3* meshPositions = NULL;
    v3* meshNormals = NULL;
    v2* meshUVs = NULL;

    GLOG("Mesh name: %s\n", path_get_name(path));

    for (i32 p = 0; p < gltfMesh.primitives_count; ++p)
    {
        cgltf_primitive primitive = gltfMesh.primitives[p];
        if (primitive.type != cgltf_primitive_type_triangles)
        {
            GWARNING("Not a triangle!\n");
            continue;
        }

        if (primitive.indices != NULL && mesh.Indices == NULL)
        {
            //mesh.IndicesCount = primitive.indices->count;
            mesh.Indices = mesh_get_indices(primitive);
        }

        /* Positions, Normals */
        for (i32 a = 0; a < primitive.attributes_count; ++a)
        {
            cgltf_attribute attribute = primitive.attributes[a];
            cgltf_type type = attribute.data->type;
            cgltf_component_type componentType = attribute.data->component_type;

            switch (attribute.type)
            {

            case cgltf_attribute_type_invalid:
            {
                GERROR("Invalid attribute!\n");
                break;
            }

            case cgltf_attribute_type_position:
            {
                //GINFO("Attribute_Position [ name %s, type: %s ]\n", attribute.name, "position", );
                meshPositions = model_base_get_v3_from_attribute(attribute, meshPositions);

                break;
            }

            case cgltf_attribute_type_normal:
            {
                meshNormals = model_base_get_v3_from_attribute(attribute, meshNormals);
                break;
            }

            case cgltf_attribute_type_tangent:
                GINFO("Attribute_Tangent [ name %s, type: %s ]\n", attribute.name, "tangent");
                break;

            case cgltf_attribute_type_texcoord:
            {
                meshUVs = model_base_get_v2_from_attribute(attribute, meshUVs);
                break;
            }

            default:
                /*
                  cgltf_attribute_type_invalid
                */
                GWARNING("Attribute_Default\n");
                break;

            }
        }

        // primitive.material == NULL for some reason
        if (!primitive.material)
            continue;

        MeshMaterial material =
            model_base_get_mesh_material(primitive.material, path, gltfMesh.name);
        mesh.MaterialId = asset_manager_load_material_id(&material);
        //mesh.Material = material;
    }

    i32 meshPositionsCount = array_count(meshPositions);
    i32 meshNormalsCount = array_count(meshNormals);
    if (meshPositionsCount != meshNormalsCount)
    {
        GERROR("PosCnt %d   NormalsCnt %d\n", meshPositionsCount, meshNormalsCount);
        vassert(meshPositionsCount == meshNormalsCount && "Positions cnt != Normals cnt!");
    }
    i32 isModelBeenDownscaled = *isModelDownscaled;
    i32 needsDownscale = (isModelBeenDownscaled == 1) || array_any_cond(meshPositions, item.X > 100.0f || item.Y > 100.0f || item.Z > 100.0f);
    if (!isModelBeenDownscaled && needsDownscale)
    {
        *isModelDownscaled = needsDownscale;
    }

    GINFO("meshPositionsCount: %d\n", meshPositionsCount);
    GINFO("meshNormalsCount: %d\n", meshNormalsCount);
    GINFO("Mesh UV COUNT: %ld\n", array_count(meshUVs));

    for (i32 i = 0; i < meshPositionsCount; ++i)
    {
        StaticMeshVertex vertex;
        //NOTE(typedef): this working weird
        if (0 && needsDownscale)
        {
            vertex.Position = v3_scale(meshPositions[i], 0.01f);
        }
        else
        {
            vertex.Position = meshPositions[i];
        }

        vertex.Normal = meshNormals[i];
        if (meshUVs != NULL)
        {
            vertex.Uv = meshUVs[i];
        }
        else
        {
            vertex.Uv = v2_new(0.0f, 0.0f);
        }

        array_push(mesh.Vertices, vertex);
    }

    GINFO("Mesh vertices cnt: %ld\n", array_count(mesh.Vertices));


    if (array_count(mesh.Vertices) <= 0)
    {
        vassert(0 && "Smth wrong with mesh!");
    }

    array_free(meshPositions);
    array_free(meshNormals);
    array_free(meshUVs);

    return mesh;
}

void
_print_cgltf_result(char* toPrint, cgltf_result result)
{
    char* results[] = {
        [cgltf_result_success] = TO_STRING(cgltf_result_success),
        [cgltf_result_unknown_format] = TO_STRING(cgltf_result_unknown_format),
        [cgltf_result_invalid_json] = TO_STRING(cgltf_result_invalid_json),
        [cgltf_result_invalid_gltf] = TO_STRING(cgltf_result_invalid_gltf),
        [cgltf_result_invalid_options] = TO_STRING(cgltf_result_invalid_options),
        [cgltf_result_file_not_found] = TO_STRING(cgltf_result_file_not_found),
        [cgltf_result_io_error] = TO_STRING(cgltf_result_io_error),
        [cgltf_result_out_of_memory] = TO_STRING(cgltf_result_out_of_memory),
        [cgltf_result_legacy_gltf] = TO_STRING(cgltf_result_legacy_gltf),
        [cgltf_result_max_enum] = TO_STRING(cgltf_result_max_enum),
    };

    GERROR("%s %d %s!\n", toPrint, result, results[result]);
}

StaticModel
static_model_load(const char* path)
{
    cgltf_options loadOptions = { 0 };
    cgltf_data* gltfData = NULL;
    cgltf_result loadResult = cgltf_parse_file(&loadOptions, path, &gltfData);
    if (loadResult != cgltf_result_success)
    {
        _print_cgltf_result("Model loading error: ", loadResult);
        vassert_break();
    }

    loadResult = cgltf_load_buffers(&loadOptions, gltfData, path);
    if (loadResult != cgltf_result_success)
    {
        GERROR("Loading buffers error!\n");
        vassert_break();
    }

    switch (gltfData->file_type)
    {
    case cgltf_file_type_glb:
        break;
    case cgltf_file_type_gltf:
        break;
    default:
        GERROR("StaticModel type incorrect!\n");
        vassert_break();
        break;
    }

    StaticModel model = {
        .Base = (struct ModelBase) {
            .Type = ModelType_Loaded,
            .Path = string(path),
            .Name = wide_string_utf8(path_get_name(path)),
        },
        .aMeshes = NULL,
    };


#if 1
    i32 meshesCount = gltfData->meshes_count;
    for (i32 m = 0; m < meshesCount; ++m)
    {
        cgltf_mesh gltfMesh = gltfData->meshes[m];
        i32 downScaled = 0;
        StaticMesh mesh = static_mesh_load(&downScaled, gltfMesh, path);
        array_push(model.aMeshes, mesh);
    }

#else

    for (i32 i = 0; i < gltfData->nodes_count; ++i)
    {
        cgltf_node node = gltfData->nodes[i];
        if (node.mesh != NULL)
        {
            cgltf_mesh gltfMesh = *node.mesh;
            i32 downScaled = 0;
            StaticMesh mesh = static_mesh_load(&downScaled, gltfMesh, path);
            array_push(model.Meshes, mesh);
        }
    }

#endif

    // note: Find aabb
    v3 min = model.aMeshes[0].Vertices[0].Position;
    v3 max = min;

    for (i64 m = 0; m < array_count(model.aMeshes); ++m)
    {
        StaticMesh mesh = model.aMeshes[m];

        for (i64 v = 0; v < array_count(mesh.Vertices); ++v)
        {
            StaticMeshVertex vertex = mesh.Vertices[v];
            v3 pos = vertex.Position;

            if (pos.X < min.X) min.X = pos.X;
            if (pos.Y < min.Y) min.Y = pos.Y;
            if (pos.Z < min.Z) min.Z = pos.Z;

            if (pos.X > max.X) max.X = pos.X;
            if (pos.Y > max.Y) max.Y = pos.Y;
            if (pos.Z > max.Z) max.Z = pos.Z;
        }
    }

    model.Aabb = (ModelAabb) {
        .Min = min,
        .Max = max,
    };

    GINFO("======     StaticModel Loading Info   ======\n");
    GINFO("Meshes Count: %ld\n", array_count(model.aMeshes));
    GINFO("Total vertices: %ld\n", static_model_get_vertices_count(&model));
    GINFO("======   StaticModel Loading Info End ======\n");

    cgltf_free(gltfData);

    return model;
}

void
static_model_destroy(const StaticModel* pStaticModel)
{
    array_foreach(pStaticModel->aMeshes,
                  wide_string_destroy(item.Name);
                  array_free(item.Vertices);
                  array_free(item.Indices);
        );

}

i64
static_model_get_vertices_count(const StaticModel* pStaticModel)
{
    i64 modelVerticesCount = 0;

    i64 i, count = array_count(pStaticModel->aMeshes);
    for (i = 0; i < count; ++i)
    {
        StaticMesh mesh = pStaticModel->aMeshes[i];
        modelVerticesCount += array_count(mesh.Vertices);
    }

    return modelVerticesCount;
}
