#ifndef STATIC_MODEL_H
#define STATIC_MODEL_H

#include "ModelBase.h"
#include <Core/Types.h>

typedef struct StaticMeshVertex
{
    v3 Position;
    v3 Normal;
    v2 Uv;
} StaticMeshVertex;

typedef struct StaticMesh
{
    i8 IsVisible;
    // TODO: Add
    i64 Id;
    i64 MaterialId;
    WideString Name;
    u32* Indices;
    StaticMeshVertex* Vertices;
} StaticMesh;

typedef struct StaticModel
{
    ModelAabb Aabb;
    ModelBase Base;
    StaticMesh* aMeshes;
} StaticModel;

StaticModel static_model_load(const char* path);
void static_model_destroy(const StaticModel* pStaticModel);
i64 static_model_get_vertices_count(const StaticModel* pStaticModel);

// idk
//StaticModel static_model_copy(StaticModel model);
StaticModel static_model_load_assimp(const char* path);

#endif // STATIC_MODEL_H
