#include "DynamicModel.h"

#include <AssetManager/AssetManager.h>
#include <Core/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>

#pragma clang diagnostic ignored "-Wswitch"

DynamicMesh
dynamic_mesh_load(i32* isModelDownscaled, const char* path, cgltf_mesh gltfMesh, cgltf_node gltfNode)
{
    vassert_not_null(gltfMesh.name);

    DynamicMesh mesh = {
        .Vertices = NULL,
        .Indices  = NULL,
        .MaterialId = 0,
        .Name = (gltfMesh.name != NULL) ? wide_string_utf8(gltfMesh.name) : (WideString) {},
        .IsVisible = 1
    };

    // Copy From
    v3* meshPositions = NULL;
    v3* meshNormals = NULL;
    v2* meshUVs = NULL;
    v4i* joints = NULL;
    v4* weights = NULL;

    GLOG("Mesh name: %s\n", path_get_name(path));

    for (size_t p = 0; p < gltfMesh.primitives_count; ++p)
    {
        cgltf_primitive primitive = gltfMesh.primitives[p];
        if (primitive.type != cgltf_primitive_type_triangles)
        {
            GWARNING("Not a triangle!\n");
            continue;
        }

        if (primitive.indices != NULL && mesh.Indices == NULL)
        {
            //mesh.IndicesCount = primitive.indices->count;
            mesh.Indices = mesh_get_indices(primitive);
        }

        /* Positions, Normals */
        for (size_t a = 0; a < primitive.attributes_count; ++a)
        {
            cgltf_attribute attribute = primitive.attributes[a];
            i32 cnt = attribute.data->count;
            cgltf_type type = attribute.data->type;
            cgltf_component_type componentType = attribute.data->component_type;

            switch (attribute.type)
            {

            case cgltf_attribute_type_invalid:
            {
                GERROR("Invalid attribute!\n");
                break;
            }

            case cgltf_attribute_type_position:
            {
                //GINFO("Attribute_Position [ name %s, type: %s ]\n", attribute.name, "position", );
                meshPositions = model_base_get_v3_from_attribute(attribute, meshPositions);

                break;
            }

            case cgltf_attribute_type_normal:
            {
                meshNormals = model_base_get_v3_from_attribute(attribute, meshNormals);
                break;
            }

            case cgltf_attribute_type_tangent:
                GINFO(RED5("Attribute_Tangent")" [ name %s, type: %s ]\n", attribute.name, "tangent");
                break;

            case cgltf_attribute_type_texcoord:
            {
                meshUVs = model_base_get_v2_from_attribute(attribute, meshUVs);
                break;
            }

            case cgltf_attribute_type_color:
            {
                GINFO("Attribute_Color [ name %s, type: %s ]\n", attribute.name, "color");
                break;
            }

            case cgltf_attribute_type_joints:
            {
                joints = model_base_get_joints_from_attribute(attribute, joints);

                //GINFO(RED5("Joints!")"%d %d %d %s %s\n", attributesCount, cnt, attribute.data->stride, model_base_type_to_string(type), model_base_component_type_to_string(componentType));
                break;
            }

            case cgltf_attribute_type_weights:
            {
                weights = model_base_get_v4_from_attribute(attribute, weights);

                //GINFO(RED5("Weights!")"%d %d %d %s %s\n", attributesCount, cnt, attribute.data->stride, model_base_type_to_string(type), model_base_component_type_to_string(componentType));
                break;
            }

            case cgltf_attribute_type_custom:
            {
                f32* buffer = (f32*) attribute.data->buffer_view->buffer->data
                    + attribute.data->buffer_view->offset / sizeof(f32)
                    + attribute.data->offset / sizeof(f32);
                i32 attributesCount = (i32) (attribute.data->stride / sizeof(f32));
                i32 cnt = attribute.data->count;
                GINFO(RED5("Custom!")"%d %d\n", attributesCount, cnt);
                break;
            }

            default:
                /*
                  cgltf_attribute_type_invalid
                */
                GWARNING("Attribute_Default\n");
                break;

            }
        }

        // primitive.material == NULL for some reason
        if (!primitive.material)
            continue;

        MeshMaterial material =
            model_base_get_mesh_material(primitive.material, path, gltfMesh.name);
        mesh.MaterialId = asset_manager_load_material_id(&material);

    }

    GINFO("meshPositions cnt: %d\n", array_count(meshPositions));
    GINFO("meshNormals cnt: %d\n", array_count(meshNormals));
    GINFO("Mesh UV cnt: %ld\n", array_count(meshUVs));
    GINFO("Joints cnt: %ld\n", array_count(joints));
    GINFO("Weights cnt: %ld\n", array_count(weights));

    i32 meshPositionsCount = array_count(meshPositions);
    i32 meshNormalsCount = array_count(meshNormals);
    if (meshPositionsCount != meshNormalsCount)
    {
        GERROR("PosCnt %d   NormalsCnt %d\n", meshPositionsCount, meshNormalsCount);
        vassert(meshPositionsCount == meshNormalsCount && "Positions cnt != Normals cnt!");
    }
    i32 isModelBeenDownscaled = *isModelDownscaled;
    i32 needsDownscale = (isModelBeenDownscaled == 1) || array_any_cond(meshPositions, item.X > 100.0f || item.Y > 100.0f || item.Z > 100.0f);
    if (!isModelBeenDownscaled && needsDownscale)
    {
        *isModelDownscaled = needsDownscale;
    }

    v2 defaultUv = {};
    for (i32 i = 0; i < meshPositionsCount; ++i)
    {
        DynamicMeshVertex vertex;
        //NOTE(typedef): this working weird
        if (0 && needsDownscale)
        {
            vertex.Position = v3_scale(meshPositions[i], 0.01f);
        }
        else
        {
            vertex.Position = meshPositions[i];
        }

        vertex.Normal = meshNormals[i];
        if (meshUVs != NULL)
        {
            vertex.Uv = meshUVs[i];
        }
        else
        {
            vertex.Uv = defaultUv;
        }

        vertex.Joint = joints[i];
        vertex.Weight = weights[i];

        array_push(mesh.Vertices, vertex);
    }

    GINFO("Mesh vertices cnt: %ld\n", array_count(mesh.Vertices));


    if (array_count(mesh.Vertices) <= 0)
    {
        vassert(0 && "Smth wrong with mesh!");
    }

    array_free(meshPositions);
    array_free(meshNormals);
    array_free(meshUVs);

    return mesh;
}

DynamicModel
dynamic_model_load(const char* path)
{
    DynamicModel model = {
        .Base = (struct ModelBase) {
            .Type = ModelType_Loaded,
            .Path = string(path),
            .Name = wide_string_utf8(path_get_name(path)),
        },
        .aMeshes = NULL,
    };

    cgltf_options loadOptions = {};
    cgltf_data* gltfData = NULL;
    cgltf_result loadResult = cgltf_parse_file(&loadOptions, path, &gltfData);
    if (loadResult != cgltf_result_success)
    {
        char* results[] = {
            [cgltf_result_success] = TO_STRING(cgltf_result_success),
            [cgltf_result_unknown_format] = TO_STRING(cgltf_result_unknown_format),
            [cgltf_result_invalid_json] = TO_STRING(cgltf_result_invalid_json),
            [cgltf_result_invalid_gltf] = TO_STRING(cgltf_result_invalid_gltf),
            [cgltf_result_invalid_options] = TO_STRING(cgltf_result_invalid_options),
            [cgltf_result_file_not_found] = TO_STRING(cgltf_result_file_not_found),
            [cgltf_result_io_error] = TO_STRING(cgltf_result_io_error),
            [cgltf_result_out_of_memory] = TO_STRING(cgltf_result_out_of_memory),
            [cgltf_result_legacy_gltf] = TO_STRING(cgltf_result_legacy_gltf),
            [cgltf_result_max_enum] = TO_STRING(cgltf_result_max_enum),
        };

        GERROR("Loading model error %d %s!\n", loadResult, results[loadResult]);
        vassert_break();
    }

    loadResult = cgltf_load_buffers(&loadOptions, gltfData, path);
    if (loadResult != cgltf_result_success)
    {
        GERROR("Loading buffers error!\n");
        vassert_break();
    }

    switch (gltfData->file_type)
    {
    case cgltf_file_type_glb:
        break;
    case cgltf_file_type_gltf:
        break;
    default:
        GERROR("DynamicModel type incorrect!\n");
        vassert_break();
        break;
    }


#if 0
    i32 meshesCount = gltfData->meshes_count;
    for (i32 m = 0; m < meshesCount; ++m)
    {
        cgltf_mesh gltfMesh = gltfData->meshes[m];
        i32 downScaled = 0;
        cgltf_node node;
        DynamicMesh mesh = dynamic_mesh_load(&downScaled, path, gltfMesh, node);
        array_push(model.aMeshes, mesh);
    }

#else

    // todo/note: we need cgltf_node for animations

    cgltf_animation* animations;
    cgltf_size animations_count;

    cgltf_animation* pAnimation = gltfData->animations;
    cgltf_size animationCount = gltfData->animations_count;


    for (size_t i = 0; i < gltfData->nodes_count; ++i)
    {
        cgltf_node node = gltfData->nodes[i];

        // cgltf_skin - skeletal, joints, bones
        // cgltf_animation ?

        if (node.mesh != NULL)
        {
            GINFO("Children count: %d\n", node.children_count);

            cgltf_mesh gltfMesh = *node.mesh;
            i32 downScaled = 0;
            DynamicMesh mesh = dynamic_mesh_load(&downScaled, path, gltfMesh, node);
            array_push(model.aMeshes, mesh);

        }
    }

    GINFO(RED5("alalla")"\n");
    GINFO("nodes_count: %ld %d\n", gltfData->nodes_count, array_count(model.aMeshes));

#endif

    printf("======     DynamicModel Loading Info   ======\n");
    printf("Meshes Count: %ld\n", array_count(model.aMeshes));
    printf("Total vertices: %ld\n", dynamic_model_get_vertices_count(&model));
    printf("======   DynamicModel Loading Info End ======\n");

    cgltf_free(gltfData);

    return model;
}

i64
dynamic_model_get_vertices_count(const DynamicModel* pDynamicModel)
{
    i64 modelVerticesCount = 0;

    for (i32 i = 0; i < array_count(pDynamicModel->aMeshes); ++i)
    {
        DynamicMesh mesh = pDynamicModel->aMeshes[i];
        modelVerticesCount += array_count(mesh.Vertices);
    }

    return modelVerticesCount;
}
