#ifndef MODEL_BASE_H
#define MODEL_BASE_H

#include <Utils/cgltf.h>
#include <Core/Types.h>
#include <Graphics/Vulkan/VulkanSimpleApiTypes.h>


/*
  ###################################
  # DOCS(typedef): Material Related #
  ###################################
*/

typedef enum TextureType
{
    TextureType_Diffuse = 0,
    TextureType_Specular,
    TextureType_Normal
} TextureType;

typedef struct PbrMetallic
{
    f32 MetallicFactor;
    f32 RoughnessFactor;
    v4 BaseColor;
    VsaTextureId BaseColorTexture;
    VsaTextureId MetallicRoughnessTexture;
} PbrMetallic;

typedef struct PbrSpecular
{
    f32 Factor;
    v3 ColorFactor;
    VsaTextureId Texture;
    VsaTextureId ColorTexture;
} PbrSpecular;

typedef struct PbrSpecularGlossiness
{
    f32 GlossinessFactor;
    v3 SpecularFactor;
    v4 DiffuseFactor;
    VsaTextureId DiffuseTexture;
    VsaTextureId SpecularGlossinessTexture;
} PbrSpecularGlossiness;

typedef struct PbrClearCoat
{
    f32 ClearcoatFactor;
    f32 ClearcoatRoughnessFactor;
    VsaTextureId ClearcoatTexture;
    VsaTextureId ClearcoatRoughnessTexture;
    VsaTextureId ClearcoatNormalTexture;
} PbrClearCoat;

typedef struct PbrTransmission
{
    f32 TransmissionFactor;
    VsaTextureId TransmissionTexture;
} PbrTransmission;

typedef struct PbrVolume
{
    f32 ThicknessFactor;
    f32 AttenuationDistance;
    v3 AttenuationColor;
    VsaTextureId ThicknessTexture;
} PbrVolume;

typedef struct PbrSheen
{
    f32 SheenRoughnessFactor;
    v3 SheenColorFactor;
    VsaTextureId SheenColorTexture;
    VsaTextureId SheenRoughnessTexture;
} PbrSheen;

typedef struct PbrEmissive
{
    f32 Strength;
    v3 Factor;
    VsaTextureId Texture;
} PbrEmissive;

typedef struct PbrIridescence
{
    f32 Factor;
    f32 Ior;
    f32 ThicknessMin;
    f32 ThicknessMax;
    VsaTextureId Texture;
    VsaTextureId ThicknessTexture;
} PbrIridescence;

typedef enum PbrAlphaModeType
{
    PbrAlphaModeType_Opaque = 0,
    PbrAlphaModeType_Mask,
    PbrAlphaModeType_Blend,
    PbrAlphaModeType_Count
} PbrAlphaModeType;

typedef struct PbrAlpha
{
    PbrAlphaModeType Mode;
    f32 CutOff;
} PbrAlpha;

typedef struct MeshMaterial
{
    const char* Name;

    i8 IsMetallicExist;
    i8 IsSpecularExist;
    i8 IsSpecularGlossinessExist;
    i8 IsClearCoatExist;
    i8 IsTransmissionExist;
    i8 IsVolumeExist;
    i8 IsSheenExist;
    i8 IsEmissiveExist;
    i8 IsIridescenceExist;
    i8 IsAlphaExist;
    i8 IsNormalExist;
    i8 IsExist;
    i8 IsOcclusionExist;
    i8 IsIorExist;

    i8 IsDoubleSided;
    i8 IsUnlit;

    PbrMetallic Metallic;
    PbrSpecular Specular;
    PbrSpecularGlossiness SpecularGlossiness;
    PbrClearCoat ClearCoat;
    PbrTransmission Transmission;
    PbrVolume Volume;
    PbrSheen Sheen;
    PbrEmissive Emissive;
    PbrIridescence Iridescence;
    PbrAlpha Alpha;

    VsaTextureId Normal;
    VsaTextureId Occlusion;

    f32 Ior;

    /* cgltf_specular specular; */
    /* cgltf_pbr_specular_glossiness pbr_specular_glossiness; */
    /* cgltf_clearcoat clearcoat; */
    /* cgltf_ior ior; */
    /* cgltf_sheen sheen; */
    /* cgltf_transmission transmission; */
    /* cgltf_volume volume; */
    /* cgltf_emissive_strength emissive_strength; */
    /* cgltf_iridescence iridescence; */
    /* cgltf_texture_view normal_texture; */
    /* cgltf_texture_view occlusion_texture; */
    /* cgltf_texture_view emissive_texture; */
    /* cgltf_float emissive_factor[3]; */
    /* cgltf_alpha_mode alpha_mode; */
    /* cgltf_float alpha_cutoff; */
    /* cgltf_bool double_sided; */
    /* cgltf_bool unlit; */
    /* cgltf_extras extras; */ // TODO
    /* cgltf_size extensions_count; */ // TODO
    /* cgltf_extension* extensions; */ // TODO
} MeshMaterial;

typedef struct MeshMaterialInfo
{
    // 16 byte
    Align(4) float MetallicFactor;
    Align(4) float RoughnessFactor;
    Align(4) float SpecularFactor;
    Align(4) float GlossinessFactor;

    // 16 byte
    Align(4) float ClearcoatFactor;
    Align(4) float ClearcoatRoughnessFactor;
    Align(4) float TransmissionFactor;
    Align(4) float ThicknessFactor;

    // 16 byte
    Align(4) float AttenuationDistance;
    Align(4) float SheenRoughnessFactor;
    Align(4) float EmissiveStrength;
    Align(4) float IridescenceFactor;

    // 16 byte
    Align(4) float ThicknessMin;
    Align(4) float ThicknessMax;
    Align(4) float CutOff;
    Align(4) float IridescenceIor;

    // 16 byte
    Align(4) int BaseColorTexture; // TODO THIS
    Align(4) int MetallicRoughnessTexture;
    Align(4) int SpecularTexture;
    Align(4) int ColorTexture;

    /* // 16 byte */
    Align(4) int DiffuseTexture;
    Align(4) int SpecularGlossinessTexture;
    Align(4) int ClearcoatTexture;
    Align(4) int ClearcoatRoughnessTexture;

    // 16 byte
    Align(4) int ClearcoatNormalTexture;
    Align(4) int TransmissionTexture;
    Align(4) int VolumeThicknessTexture;
    Align(4) int SheenColorTexture;

    // 16 byte
    Align(4) int SheenRoughnessTexture;
    Align(4) int EmissiveTexture;
    Align(4) int IridescenceTexture;
    Align(4) int IridescenceThicknessTexture;

    // 16 byte

    v4 ColorFactor;

    v3 SpecularGlossinessFactor;
    int Occlusion;

    v3 AttenuationColor;
    int Normal;

    v3 SheenColorFactor;
    int Mode;

    v3 EmissiveFactor;
    float Ior;

    v4 BaseColor;
    v4 DiffuseFactor;
} MeshMaterialInfo;

typedef enum ModelType
{
    ModelType_Loaded = 1 << 0,
    ModelType_Downscaled = 1 << 1,
    ModelType_Generated = 1 << 2,
    ModelType_Generated_Cube = ModelType_Generated | (1 << 3),
} ModelType;



char* _get_path_from_incorrect_path(char* path, i32 length);
VsaTextureId mesh_texture_create(cgltf_texture* texture, const char* path);
PbrMetallic material_pbr_metallic(cgltf_pbr_metallic_roughness pbrMetallic, const char* materialPath);
PbrSpecular material_pbr_specular(cgltf_specular specular, const char* materialPath);
PbrSpecularGlossiness material_pbr_specular_glossiness(cgltf_pbr_specular_glossiness psg, const char* materialPath);
PbrClearCoat material_clear_coat(cgltf_clearcoat cc, const char* materialPath);
PbrTransmission material_transmission(cgltf_transmission transmission, const char* materialPath);
PbrVolume material_volume(cgltf_volume volume, const char* materialPath);
PbrSheen material_sheen(cgltf_sheen sheen, const char* materialPath);
PbrEmissive material_emissive(cgltf_emissive_strength emissiveStrength, cgltf_texture_view emissiveTexture, cgltf_float emissiveFactor[3], const char* materialPath);
PbrIridescence material_iridescence(cgltf_iridescence iridescence, const char* materialPath);
PbrAlpha material_alpha(cgltf_alpha_mode alphaMode, cgltf_float alphaCutoff, const char* materialPath);


/*
  ##########################################
  # DOCS(typedef): Loading Data From cgltf #
  ##########################################
*/

u32* mesh_get_indices(cgltf_primitive primitive);


typedef struct ModelAabb
{
    v3 Min;
    v3 Max;
} ModelAabb;

typedef struct ModelBase
{
    ModelType Type;
    i64 Id;
    char* Path;
    WideString Name;
} ModelBase;

v2* model_base_get_v2_from_attribute(cgltf_attribute attribute, v2* resultArray);
v3* model_base_get_v3_from_attribute(cgltf_attribute attribute, v3* resultArray);
v4* model_base_get_v4_from_attribute(cgltf_attribute attribute, v4* resultArray);
v4i* model_base_get_joints_from_attribute(cgltf_attribute attribute, v4i* resultArray);
const char* model_base_type_to_string(cgltf_type type);
const char* model_base_component_type_to_string(cgltf_component_type type);
const char* model_base_result_to_string(cgltf_result result);
size_t model_base_attribute_get_size(cgltf_component_type type);
i32 model_base_attribute_get_count(cgltf_type type);

MeshMaterial model_base_get_mesh_material(cgltf_material* pMat, const char* path, const char* meshName);

#endif // MODEL_BASE_H
