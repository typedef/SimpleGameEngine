#include "Application/Application.h"

/*
  NOTE(bies):
  Starting main application with 50 Mb stack
*/

typedef struct UserApplicationSettings
{
    i32 Argc;
    char** Argv;
} UserApplicationSettings;

extern void create_user_application(UserApplicationSettings* appSettings);

void*
main_thread_function(void* data)
{
    UserApplicationSettings* pArgs = (UserApplicationSettings*) data;

#ifdef WINDOWS_PLATFORM
    UserApplicationSettings value = {};
    pArgs = &value;
#endif

    create_user_application(pArgs);

    application_start();
    application_end();

    return NULL;
}

int main(int argc, char** argv)
{
    size_t stackSize = MB(10);
    UserApplicationSettings uas = {.Argc = argc, .Argv = argv};
    SimpleThread mainThread = simple_thread_create(main_thread_function, stackSize, &uas);
    simple_thread_attach(&mainThread);

    return 0;
}
