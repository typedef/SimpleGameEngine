#include "JsonParser.h"

#if defined(IO_H)
#warning "IO_H DECLR"
#endif

#include <Core/SimpleStandardLibrary.h>

#pragma clang diagnostic ignored "-Wswitch"

const char*
json_value_type_to_string(JsonValueType type)
{

    switch (type)
    {
    case JsonValueType_Int: return "Int";
    case JsonValueType_Float: return "Float";
    case JsonValueType_Bool: return "Bool";
    case JsonValueType_Null: return "Null";
    case JsonValueType_String: return "String";
    case JsonValueType_IntArray: return "IntArray";
    case JsonValueType_FloatArray: return "FloatArray";
    case JsonValueType_StringArray: return "StringArray";
    case JsonValueType_ObjectArray: return "ObjectArray";
    case JsonValueType_Object: return "Object";
    }

    vassert_break();
    return "";
}

/*
  PUBLIC JSON UTILS
*/
JsonObject*
json_object_create()
{
    JsonObject* obj = memory_allocate_type(JsonObject);
    obj->Keys = NULL;
    obj->Values = NULL;
    return obj;
}

void
json_check_for_type(JsonValue value, JsonValueType type)
{
    if (value.Type != JsonValueType_IntArray)
    {
        GERROR("Value type: %d %s\n", value.Type,
               json_value_type_to_string(value.Type));
        vassert(value.Type == type && "Wrong data type!");
    }
}

void
json_object_destroy(JsonObject* obj)
{
    i32 i, count = array_count(obj->Values);
    for (i = 0; i < count; ++i)
    {
        JsonValue value = obj->Values[i];
        if (value.Type == JsonValueType_Object)
        {
            json_object_free((JsonObject*)value.Data);
        }
        else if (value.Type == JsonValueType_IntArray
                 || value.Type == JsonValueType_FloatArray
                 || value.Type == JsonValueType_StringArray)
        {
            array_free(value.Data);
        }
    }

    memory_free(obj);
}

static const char* TrueKeyWord;
static const char* FalseKeyWord;
static const char* NullKeyWord;

force_inline void
_register_keywords()
{
    TrueKeyWord = istring("true");
    FalseKeyWord = istring("false");
    NullKeyWord = istring("null");
}

force_inline void
_skip_spaces(char* stream)
{
    while (*stream == ' ' || *stream == '\n' || *stream == '\t' || *stream == '\r')
    {
        ++stream;
    }
}

i32
_json_is_array_of_type(JsonToken* tokens, JsonTokenType type, i32 index)
{
    i32 count = array_count(tokens);
    while (1)
    {
        JsonToken token = tokens[index];
        ++index;

        if (token.Type == JsonTokenType_ArrayCloseBracket)
        {
            return 1;
        }

        if (token.Type == JsonTokenType_Comma)
            continue;

        if (token.Type != type || index >= count)
            return 0;
    }
}

JsonValue
_json_create_array(JsonToken* tokens, JsonTokenType type, i32* index)
{
    JsonValue result;

    switch (type)
    {
    case JsonTokenType_String:
        result.Type = JsonValueType_StringArray;
        break;
    case JsonTokenType_IntNumber:
        result.Type = JsonValueType_Int;
        break;
    case JsonTokenType_FloatNumber:
        result.Type = JsonValueType_Float;
        break;
    }

    void* data = NULL;
    i32 i = *index;

    while (1)
    {
        JsonToken token = tokens[i];

        switch (token.Type)
        {
        case JsonTokenType_String:
        {
            char** strData = (char**) data;
            result.Type = JsonValueType_StringArray;
            array_push(strData, ((char*)(token.Data)));
            data = strData;
            break;
        }
        case JsonTokenType_IntNumber:
        {
            i32* iData = (i32*) data;
            result.Type = JsonValueType_IntArray;
            array_push(iData, void_to_i32(token.Data));
            data = iData;
            break;
        }
        case JsonTokenType_FloatNumber:
        {
            f32* fData = (f32*) data;
            result.Type = JsonValueType_FloatArray;
            array_push(fData, void_to_f32(token.Data));
            data = fData;
            break;
        }
        case JsonTokenType_Comma:
            ++i;
            continue;
        case JsonTokenType_ArrayCloseBracket:
            goto endLabel;
        default:
            assert(0 && "We don't support this type of arrays");
            break;
        }

        ++i;
    }

    //TODO (bies): delete this in the future
    assert(tokens[i].Type == JsonTokenType_ArrayCloseBracket);

endLabel:
    result.Data = data;
    *index = i;

    return result;
}

JsonValue
_json_create_object_array(JsonToken* tokens, i32* index)
{
    JsonValue result;
    result.Type = JsonValueType_ObjectArray;

    size_t* data = NULL;

    i32 i = *index;
    while (1)
    {
        JsonToken token = tokens[i];
        switch (token.Type)
        {
        case JsonTokenType_ObjectOpenBracket:
        {
            JsonObject* addr = json_parser_create_json_object(tokens, index);
            array_push(data, (size_t)addr);
            //i = *index;
            break;
        }
        case JsonTokenType_Comma:
            break;
        case JsonTokenType_ArrayCloseBracket:
            goto endLabel;
        default:
            assert(0 && "We don't support this type of arrays");
            break;
        }

        ++i;
    }

endLabel:
    vassert(tokens[i].Type == JsonTokenType_ArrayCloseBracket && "Not a ] token type!");
    result.Data = data;
    *index = i;

    return result;
}


JsonToken
_json_read_string_token(char* stream, i32* length)
{
    const i32 bufferSize = 256;
    char tempStringBuffer[bufferSize];
    i32 index = 0;

    memset(tempStringBuffer, '\0', bufferSize*sizeof(char));

    if (stream[1] == '\\')
    {
        JsonToken stringToken;
        stringToken.Type = JsonTokenType_String;
        stringToken.Data = "";
        *length = 0;
        return stringToken;
    }

    /*

      we are here
      |
      v
      "string content"

    */
    ++stream;

    while (*stream != '"' && *stream != '\0')
    {
        tempStringBuffer[index] = *stream;
        ++index;
        ++stream;
    }

    *length = index + 1;

    /*

      we are here (we ++ in main loop)
      |
      v
      "string content"

    */

    JsonToken stringToken;
    stringToken.Type = JsonTokenType_String;
    stringToken.Data = string(tempStringBuffer);

    return stringToken;
}

JsonToken
_json_read_number_token(char* stream, i32* length, i32 negativePart)
{
    /*
      0123.456
      ^
    */
    i32 isIntPart = 1;
    i32 intIndex = 0;
    i32 floatIndex = 0;
    char c = *stream;
    const i32 intBufferLength = 128;
    const i32 floatBufferLength = 128;
    char intBuffer[intBufferLength];
    char floatBuffer[floatBufferLength];

    memset(intBuffer  , '\0', intBufferLength   * sizeof(char));
    memset(floatBuffer, '\0', floatBufferLength * sizeof(char));

    while ((c >= '0' && c <= '9') || c == '.')
    {
        switch (c)
        {
        case '0': case '1': case '2':
        case '3': case '4': case '5':
        case '6': case '7': case '8':
        case '9':
        {
            if (isIntPart)
            {
                intBuffer[intIndex] = c;
                ++intIndex;
            }
            else
            {
                floatBuffer[floatIndex] = c;
                ++floatIndex;
            }
            break;
        }

        case '.':
        {
            if (!isIntPart)
            {
                assert(0 && "Two dots in number are not acceptable!");
            }

            floatBuffer[floatIndex] = '0';
            ++floatIndex;
            floatBuffer[floatIndex] = '.';
            ++floatIndex;

            isIntPart = 0;

            break;
        }
        }

        ++stream;
        c = *stream;
    }

    *length = intIndex - 1;

    f32 result = string_to_i32(intBuffer);

    if (!isIntPart)
    {
        f32 floatPart = string_to_f32(floatBuffer);
        result += floatPart;
        *length += floatIndex - 1;
    }

    result *= negativePart;

    JsonToken token;
    if (!isIntPart)
    {
        token.Data = memory_allocate_type(f32);
        *((f32*)token.Data) = result;
        token.Type = JsonTokenType_FloatNumber;
    }
    else
    {
        token.Data = memory_allocate_type(i32);
        *((i32*)token.Data) = result;
        token.Type = JsonTokenType_IntNumber;
    }

    return token;
}

i32
_json_check_key_word(char* stream, const char* keyWord)
{
    i32 i;
    char* ptr = stream;
    i32 keyWordLength = istring_length(keyWord);

    for (i = 0; i < keyWordLength; ++i)
    {
        if (*ptr != keyWord[i])
        {
            return 0;
        }

        ++ptr;
    }

    return 1;
}

i32
_json_next_bool(char* stream)
{
    char* ptr = stream;
    ++ptr;

    if (*ptr != 'r') return 0;
    if (*ptr != 'u') return 0;
    if (*ptr != 'e') return 0;

    return 1;
}

JsonToken*
_json_read_tokens(JsonParser* parser, char* stream)
{
    char c;
    JsonToken* tokens = NULL;

    while ((c = *stream) != '\0')
    {
        _skip_spaces(stream);

        switch (c)
        {
        case '{':
        {
            JsonToken token;
            token.Type = JsonTokenType_ObjectOpenBracket;
            token.Data = "{";
            array_push(tokens, token);
            break;
        }

        case '}':
        {
            JsonToken token;
            token.Type = JsonTokenType_ObjectCloseBracket;
            token.Data = "}";
            array_push(tokens, token);
            break;
        }

        case ',':
        {
            JsonToken token;
            token.Type = JsonTokenType_Comma;
            token.Data = ",";
            array_push(tokens, token);
            break;
        }

        case '"':
        {
            i32 length;
            JsonToken stringToken = _json_read_string_token(stream, &length);
            array_push(tokens, stringToken);

            stream += length;

            break;
        }

        case ':':
        {
            JsonToken keyValueToken;
            keyValueToken.Type = JsonTokenType_KeyValueSeparator;
            keyValueToken.Data = ":";
            array_push(tokens, keyValueToken);
            break;
        }

        case 't':
        {
            if (_json_check_key_word(stream, TrueKeyWord))
            {
                JsonToken trueValueToken;
                trueValueToken.Type = JsonTokenType_BoolTrue;
                trueValueToken.Data = (void*)TrueKeyWord;
                stream += 2;
                array_push(tokens, trueValueToken);
            }
            break;
        }
        case 'f':
        {
            if (_json_check_key_word(stream, FalseKeyWord))
            {
                JsonToken falseValueToken;
                falseValueToken.Type = JsonTokenType_BoolFalse;
                falseValueToken.Data = (void*)FalseKeyWord;
                stream += 3;
                array_push(tokens, falseValueToken);
            }
            break;
        }
        case 'n':
        {
            if (_json_check_key_word(stream, NullKeyWord))
            {
                JsonToken token;
                token.Type = JsonTokenType_Null;
                token.Data = (void*)NullKeyWord;
                stream += 3;
                array_push(tokens, token);
            }
            break;
        }

        case '[':
        {
            JsonToken token;
            token.Type = JsonTokenType_ArrayOpenBracket;
            token.Data = "[";
            array_push(tokens, token);
            break;
        }

        case ']':
        {
            JsonToken token;
            token.Type = JsonTokenType_ArrayCloseBracket;
            token.Data = "]";
            array_push(tokens, token);
            break;
        }

        case '0': case '1': case '2':
        case '3': case '4': case '5':
        case '6': case '7': case '8':
        case '9':
        {
            i32 length, negativePart = (*(stream - 1) == '-') ? -1 : 1;

            JsonToken token = _json_read_number_token(stream, &length, negativePart);
            array_push(tokens, token);
            stream += length;

            break;
        }

        }

        ++stream;
    }

    return tokens;
}

JsonValue _json_parse_value(JsonToken* tokens, i32* index);

JsonObject*
json_parser_create_json_object(JsonToken* tokens, i32* index)
{
    i32 i = *index,
        isAfterKey = 0,
        count = array_count(tokens);
    JsonToken token;
    JsonObject* object = memory_allocate_type(JsonObject);
    object->Keys = NULL;
    object->Values = NULL;

    while (1)
    {
        token = tokens[i];

        if (token.Type == JsonTokenType_ObjectCloseBracket || i >= count)
            break;

        if (!isAfterKey && token.Type == JsonTokenType_String)
        {
            array_push(object->Keys, token.Data);
            isAfterKey = 1;
        }
        else if (isAfterKey
                 && token.Type != JsonTokenType_KeyValueSeparator
                 && token.Type != JsonTokenType_Comma)
        {
            // json value can contains a json object
            JsonValue value = _json_parse_value(tokens, &i);
            array_push(object->Values, value);
            isAfterKey = 0;
        }

        ++i;
    }

    *index = i;

    return object;
}

JsonValue
_json_parse_value(JsonToken* tokens, i32* index)
{
    JsonValue result;
    i32 i = *index;
    JsonToken token = tokens[i];

    switch (token.Type)
    {
    case JsonTokenType_Null:
    {
        result.Type = JsonValueType_Null;
        result.Data = token.Data;
        break;
    }

    case JsonTokenType_String:
    {
        result.Type = JsonValueType_String;
        result.Data = token.Data;
        break;
    }

    case JsonTokenType_IntNumber:
    {
        result.Type = JsonValueType_Int;
        result.Data = token.Data;
        break;
    }

    case JsonTokenType_FloatNumber:
    {
        result.Type = JsonValueType_Float;
        result.Data = token.Data;
        break;
    }

    case JsonTokenType_BoolTrue:
    case JsonTokenType_BoolFalse:
    {
        result.Type = JsonValueType_Bool;
        result.Data = token.Data;
        break;
    }

    case JsonTokenType_ArrayOpenBracket:
    {
        ++i;

        if (_json_is_array_of_type(tokens, JsonTokenType_String, i))
        {
            result = _json_create_array(tokens, JsonTokenType_String, &i);
        }
        else if (_json_is_array_of_type(tokens, JsonTokenType_IntNumber, i))
        {
            result = _json_create_array(tokens, JsonTokenType_IntNumber, &i);
        }
        else if (_json_is_array_of_type(tokens, JsonTokenType_FloatNumber, i))
        {
            result = _json_create_array(tokens, JsonTokenType_FloatNumber, &i);
        }
        else // if (_json_is_array_of_type(tokens, JsonTokenType_Object, i))
        {
            result = _json_create_object_array(tokens, &i);
        }
        /* else */
        /* { */
        /*     assert(0 && "Not supported array!"); */
        /* } */

        break;
    }

    case JsonTokenType_ObjectOpenBracket:
    {
        result.Type = JsonValueType_Object;
        result.Data = json_parser_create_json_object(tokens, &i);
        assert(tokens[i].Type == JsonTokenType_ObjectCloseBracket);
        break;
    }

    }

    *index = i;

    return result;
}

void
json_parse_string(JsonParser* parser, char* string)
{
    _register_keywords();

    assert(string != NULL && "JsonParser can't parse NULL string!");

    i32 index = 0;
    parser->SourceContent = string;
    parser->Tokens = _json_read_tokens(parser, string);
    parser->Object = json_parser_create_json_object(parser->Tokens, &index);
}

void
json_parse_file(JsonParser* parser, const char* path)
{
    char* data = file_read_string(path);
    json_parse_string(parser, data);
}

void
json_destroy(JsonParser* parser)
{
    JsonObject* obj = parser->Object;

    json_object_destroy(obj);

    memory_free(parser->SourceContent);
    memory_free(parser->Tokens);

    parser->SourceContent = NULL;
    parser->Object = NULL;
    parser->Tokens = NULL;
}

/*
  TODO(typedef):

  Use this function, and then file_write():
  char* json_object_to_string(JsonObject* root, i32 startIndentationLevel)
 */
void
json_write_file(JsonParser* parser, const char* path)
{
    char* sb = NULL;
    i32 i, count = array_count(parser->Tokens),
        isInArray = 0;

    char intBuffer[32];
    char floatBuffer[32];
    i32 indentationLevel = 0;
    const char* tab4 = "    ";

    for (i = 0; i < count; ++i)
    {
        JsonToken token = parser->Tokens[i];

        if (token.Type == JsonTokenType_ObjectOpenBracket)
        {
            ++indentationLevel;
        }
        else if (token.Type == JsonTokenType_ObjectCloseBracket)
        {
            --indentationLevel;
        }

        if (token.Type == JsonTokenType_ArrayOpenBracket)
            isInArray = 1;
        else if (token.Type == JsonTokenType_ArrayCloseBracket)
            isInArray = 0;

        switch (token.Type)
        {
        case JsonTokenType_IntNumber:
        {
            memset(intBuffer, '\0', sizeof(intBuffer));
            i32 number = void_to_i32(token.Data);
            string_i32(intBuffer, number);
            string_builder_appends(sb, intBuffer);
            break;
        }

        case JsonTokenType_FloatNumber:
        {
            memset(floatBuffer, '\0', sizeof(floatBuffer));
            f32 number = void_to_f32(token.Data);
            string_f64(floatBuffer, number);
            string_builder_appends(sb, floatBuffer);
            break;
        }

        case JsonTokenType_KeyValueSeparator:
        {
            string_builder_appends(sb, token.Data);
            string_builder_appends(sb, " ");
            break;
        }

        case JsonTokenType_String:
        {
            string_builder_appends(sb, "\"");
            string_builder_appends(sb, token.Data);
            string_builder_appends(sb, "\"");

            break;
        }

        default:
        {
            string_builder_appends(sb, token.Data);
            break;
        }
        }

        if ((token.Type == JsonTokenType_Comma
             || token.Type == JsonTokenType_ObjectOpenBracket)
            && !isInArray)
        {
            string_builder_appends(sb, "\n");
            for (i32 i = 0; i < indentationLevel; i++)
                string_builder_appends(sb, tab4);
        }

        if (parser->Tokens[i + 1].Type == JsonTokenType_ObjectOpenBracket)
        {
            string_builder_appends(sb, "\n");
            for (i32 i = 0; i < indentationLevel; i++)
                string_builder_appends(sb, tab4);
        }
        else if (parser->Tokens[i + 1].Type == JsonTokenType_ObjectCloseBracket)
        {
            string_builder_appends(sb, "\n");
            for (i32 i = 0; i < (indentationLevel - 1); i++)
                string_builder_appends(sb, tab4);
        }
    }

    file_write_string(path, sb, string_builder_count(sb));
}

JsonValue
json_object_get_value(JsonObject* object, char* key, JsonValueType type)
{
    const char** keys = object->Keys;
    i32 i, count = array_count(keys);
    for (i = 0; i < count; ++i)
    {
        if (string_compare(keys[i], key))
        {
            return object->Values[i];
        }
    }

    assert(0 && "Expected key that exist in current object!");
    return (JsonValue) {};
}

#define ind(sb, indentation)			\
    ({						\
        const char* tab4 = "    ";		\
        for (i32 i = 0; i < indentation; ++i)	\
        {					\
            string_builder_appends(sb, tab4);	\
        }					\
    })

char*
json_object_to_string(JsonObject* root, i32 startIndentationLevel)
{
    char** keys = (char**)root->Keys;
    JsonValue* values = (JsonValue*)root->Values;

    vassert_not_null(keys);
    vassert_not_null(values);

    char* sb = NULL;
    i32 indentationLevel = startIndentationLevel;

    ind(sb, indentationLevel);

    string_builder_appends(sb, "{\n");
    ++indentationLevel;

    i32 keysCount = array_count(keys);
    for (i32 i = 0; i < keysCount; ++i)
    {
        char* key = keys[i];
        JsonValue value = values[i];

        ind(sb, indentationLevel);

        string_builder_appendf(sb, "\"%s\": ", key);

        switch (value.Type)
        {
        case JsonValueType_Int:
        {
            string_builder_appendf(sb, "%d", void_to_i32(value.Data));
            break;
        }
        case JsonValueType_Float:
        {
            string_builder_appendf(sb, "%f", void_to_f32(value.Data));
            break;
        }
        case JsonValueType_Null:
        case JsonValueType_Bool:
        {
            string_builder_appendf(sb, "%s", void_to_string(value.Data));
            break;
        }
        case JsonValueType_String:
        {
            string_builder_appendf(sb, "\"%s\"", void_to_string(value.Data));
            break;
        }
        case JsonValueType_IntArray:
        {
            i32* array = (i32*)value.Data;
            i32 count = array_count(array);
            string_builder_appendc(sb, '[');
            for (i32 i = 0; i < count; ++i)
            {
                if (i < (count - 1))
                {
                    string_builder_appendf(sb, "%d, ", array[i]);
                }
                else
                {
                    string_builder_appendf(sb, "%d", array[i]);
                }
            }
            string_builder_appendc(sb, ']');
            break;
        }
        case JsonValueType_FloatArray:
        {
            f32* array = (f32*)value.Data;
            i32 count = array_count(array);
            string_builder_appendc(sb, '[');
            for (i32 i = 0; i < count; ++i)
            {
                if (i < (count - 1))
                {
                    string_builder_appendf(sb, "%f, ", array[i]);
                }
                else
                {
                    string_builder_appendf(sb, "%f", array[i]);
                }
            }
            string_builder_appendc(sb, ']');
            break;
        }
        case JsonValueType_StringArray:
        {
            char** array = (char**) value.Data;
            i32 count = array_count(array);
            string_builder_appendc(sb, '[');
            for (i32 i = 0; i < count; ++i)
            {
                if (i < (count - 1))
                {
                    string_builder_appendf(sb, "\"%s\", ", array[i]);
                }
                else
                {
                    string_builder_appendf(sb, "\"%s\"", array[i]);
                }
            }
            string_builder_appendc(sb, ']');

            break;
        }
        case JsonValueType_Object:
        {
            JsonObject* obj = (JsonObject*) value.Data;
            if (obj->Keys != NULL && obj->Values != NULL)
            {
                char* objToStr = json_object_to_string(obj, indentationLevel);
                GINFO("Obj To Str[%s]:\n%s\n", key, objToStr);
                string_builder_appendf(sb, "\n%s", objToStr);
            }
            else
            {
                string_builder_appends(sb, " \"NULL\"");
            }

            break;
        }

        }

        if (i < (keysCount - 1))
        {
            string_builder_appends(sb, ",\n");
        }
        else
        {
            string_builder_appends(sb, "\n");
        }

        //if (string_compare(key, "ViewProjection"))
        //    GINFO("Key: %s, Sb: %s\n", key, sb);
        GINFO("Count: %lld Capacity: %lld \n", string_builder_count(sb), string_builder_capacity(sb));
        vguard(string_builder_count(sb) < string_builder_capacity(sb));
    }

    ind(sb, indentationLevel - 1);
    string_builder_appendc(sb, '}');

    return sb;
}

/*
  PUBLIC JSON UTILS
*/

static const char* g_TokensTypeToString[] = {
    "JsonTokenType_None",
    "JsonTokenType_KeyValueSeparator",
    "JsonTokenType_String",
    "JsonTokenType_IntNumber",
    "JsonTokenType_FloatNumber",
    "JsonTokenType_BoolTrue",
    "JsonTokenType_BoolFalse",
    "JsonTokenType_Null",
    "JsonTokenType_ObjectOpenBracket",
    "JsonTokenType_ObjectCloseBracket",
    "JsonTokenType_ArrayOpenBracket",
    "JsonTokenType_ArrayCloseBracket",
    "JsonTokenType_Comma"
};

const char*
json_token_to_string(JsonToken token)
{
    return g_TokensTypeToString[token.Type];
}

char*
json_tokens_to_string(JsonToken* tokens)
{
    i32 i;
    i32 count = array_count(tokens);
    char* sb = NULL;
    i32 indentation = 0;

    for (i = 0; i < count; ++i)
    {
        JsonToken token = tokens[i];

        switch (token.Type)
        {
        case JsonTokenType_ObjectOpenBracket:
        {
            if (i != 0)
                string_builder_appends(sb, "\n");
            ind(sb, indentation);
            string_builder_appends(sb, "{\n");
            ++indentation;
            break;
        }
        case JsonTokenType_ObjectCloseBracket:
        {
            --indentation;
            ind(sb, indentation);
            string_builder_appends(sb, "}");
            break;
        }
        case JsonTokenType_ArrayOpenBracket:
        {
            string_builder_appends(sb, "[ ");
            ++i;
            while (1)
            {
                token = tokens[i];

                if (token.Type == JsonTokenType_ArrayCloseBracket)
                {
                    break;
                }

                if (token.Type == JsonTokenType_Comma)
                {
                    ++i;
                    continue;
                }

                const char* tokenStr = NULL;// = json_token_to_string(token);

                switch (token.Type)
                {
                case JsonTokenType_String:
                {
                    tokenStr = token.Data;
                    break;
                }
                case JsonTokenType_IntNumber:
                {
                    tokenStr = "I32";
                    break;
                }
                case JsonTokenType_FloatNumber:
                {
                    tokenStr = "F32";
                    break;
                }
                }

                if (tokens[i + 1].Type != JsonTokenType_ArrayCloseBracket)
                {
                    string_builder_appendf(sb, "%s, ", tokenStr);
                }
                else
                {
                    string_builder_appendf(sb, "%s", tokenStr);
                }

                ++i;
            }

            string_builder_appends(sb, " ]");

            break;
        }
        case JsonTokenType_Comma:
        {
            string_builder_appends(sb, ",\n");
            break;
        }
        case JsonTokenType_KeyValueSeparator:
        {
            string_builder_appends(sb, ": ");
            break;
        }
        case JsonTokenType_String:
        {
            if (tokens[i - 1].Type != JsonTokenType_KeyValueSeparator)
                ind(sb, indentation);
            string_builder_appendf(sb, "\"%s\"", token.Data);
            break;
        }
        case JsonTokenType_IntNumber:
        {
            string_builder_appends(sb, "I32");
            break;
        }
        case JsonTokenType_FloatNumber:
        {
            string_builder_appends(sb, "F32");
            break;
        }
        default:
        {
            const char* tokenStr = json_token_to_string(token);
            string_builder_appendf(sb, "%s", tokenStr);
            break;
        }
        }

        if (tokens[i + 1].Type == JsonTokenType_ObjectCloseBracket)
        {
            string_builder_appends(sb, "\n");
        }
    }

    return sb;
}

void
json_object_free(JsonObject* obj)
{
    i32 i, count = array_count(obj->Values);
    for (i = 0; i < count; ++i)
    {
        JsonValue value = obj->Values[i];
        if (value.Type == JsonValueType_Object)
        {
            json_object_free((JsonObject*)value.Data);
        }
        else if (value.Type == JsonValueType_IntArray
                 || value.Type == JsonValueType_FloatArray
                 || value.Type == JsonValueType_StringArray)
        {
            array_free(value.Data);
        }
    }

    array_free(obj->Keys);
    array_free(obj->Values);

    memory_free(obj);
}

void
_json_print_value(JsonValue value)
{
    switch (value.Type)
    {
    case JsonValueType_Int:
    {
        GERROR("JsonValue: %d\n", void_to_i32(value.Data));
        break;
    }

    case JsonValueType_Float:
    {
        GERROR("JsonValue: %f\n", void_to_f32(value.Data));
        break;
    }

    case JsonValueType_Null:
    {
        GERROR("JsonValue: NULL\n");
        break;
    }

    case JsonValueType_Bool:
    case JsonValueType_String:
    {
        GERROR("JsonValue: %s\n", void_to_string(value.Data));
        break;
    }

    case JsonValueType_IntArray:
    case JsonValueType_FloatArray:
    case JsonValueType_StringArray:
    {
        GERROR("JsonValue: THIS IS ARRAY\n");
        break;
    }

    case JsonValueType_Object:
    {
        GERROR("JsonValue: THIS IS OBJECT\n");
        break;
    }
    }
}
