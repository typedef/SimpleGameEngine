#ifndef SIMPLE_FORMATS_H
#define SIMPLE_FORMATS_H

typedef enum SimpleFormatsType
{
    SimpleFormatsType_None = 0,

    // DOCS(typedef): Simple Binary Texture
    SimpleFormatsType_SBTE,
    // DOCS(typedef): Simple Binary Material
    SimpleFormatsType_SBMA,
    // DOCS(typedef): Simple Binary Model
    SimpleFormatsType_SBMO,

    SimpleFormatsType_Count
} SimpleFormats;

const char* simple_formats_to_string(SimpleFormats formats);
const char* simple_formats_to_ext(SimpleFormats formats);

#endif // SIMPLE_FORMATS_H
