#include "SimpleFormats.h"

#include <Core/Types.h>

const char*
simple_formats_to_string(SimpleFormats formats)
{
    switch (formats)
    {
    case SimpleFormatsType_None : return "None";
    case SimpleFormatsType_SBTE : return "SBTE";
    case SimpleFormatsType_SBMA : return "SBMA";
    case SimpleFormatsType_SBMO : return "SBMO";
    case SimpleFormatsType_Count: return "Count";

    default: vguard(0 && "simple_formats_to_string: formats is wrong!");
    }

    return "NULL";
}


const char*
simple_formats_to_ext(SimpleFormats formats)
{
    switch (formats)
    {
    case SimpleFormatsType_SBTE : return ".sbte";
    case SimpleFormatsType_SBMA : return ".sbma";
    case SimpleFormatsType_SBMO : return ".sbmo";

    default: vguard(0 && "simple_formats_to_string: formats is wrong!");
    }

    return "NULL";
}
