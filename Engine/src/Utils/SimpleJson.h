#ifndef SIMPLE_JSON_H
#define SIMPLE_JSON_H

#include <Core/Types.h>

typedef enum SimpleJsonValueType
{
    SimpleJsonValueType_None = 0,

    // DOCS: Values
    SimpleJsonValueType_Int,
    SimpleJsonValueType_Float,
    SimpleJsonValueType_Bool,

    // DOCS: Pointer's
    SimpleJsonValueType_Null,
    SimpleJsonValueType_String,
    //       Array
    SimpleJsonValueType_Array_Int,
    SimpleJsonValueType_Array_Float,
    SimpleJsonValueType_Array_Bool,
    SimpleJsonValueType_Array_String,
    SimpleJsonValueType_Array_Object,

    //       Object
    SimpleJsonValueType_Object,

    SimpleJsonValueType_Count,
} SimpleJsonValueType;

typedef union SimpleJsonValueUnion
{
    void* pData;
    i64 I64Value;
    f64 F64Value;
    i64 BoolValue;
} SimpleJsonValueUnion;

typedef struct SimpleJsonValue
{
    SimpleJsonValueType Type;
    SimpleJsonValueUnion Data;
} SimpleJsonValue;

typedef struct SimpleJsonObject
{
    const char* Key;
    SimpleJsonValue Value;
} SimpleJsonObject;

typedef struct SimpleJsonError
{
    i32 IsError;
    const char* Error;
} SimpleJsonError;

typedef struct SimpleJsonParseResult
{
    i64 EndIndex;
    SimpleJsonError Error;
} SimpleJsonParseResult;

typedef struct SimpleJson
{
    SimpleJsonObject* Root;
} SimpleJson;

typedef enum SimpleJsonWriteType
{
    SimpleJsonWriteType_Compressed = 0,
    SimpleJsonWriteType_Extended,
} SimpleJsonWriteType;

typedef struct SimpleJsonWriteSettings
{
    char* Path;
    SimpleJsonWriteType WriteType;
    i64 TabIndex;
} SimpleJsonWriteSettings;

SimpleJson simple_json_parse(char* inputText, size_t inputLength);
SimpleJson simple_json_parse_file(char* filePath);
void simple_json_write_file(SimpleJson* pJson, SimpleJsonWriteSettings set);

#endif // SIMPLE_JSON_H
