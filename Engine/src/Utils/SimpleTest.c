#include "SimpleTest.h"

#include <AssetManager/AssetManager.h>
#include <Core/SimpleStandardLibrary.h>

void
asset_manager_test_performance()
{

#if 0
    i32 id0, id1;
    i64 t0, t1;
    TimeState ts = {};

    profiler_start(&ts);
    id0 = asset_manager_load_asset_id(AssetType_Texture, asset_texture("binary/CesiumMan_img0.sbte"));
    profiler_end(&ts);
    profiler_print(&ts);
    t0 = profiler_get_microseconds(&ts);

    profiler_start(&ts);
    id1 = asset_manager_load_asset_id(AssetType_Texture, asset_model("CesiumMan/CesiumMan_img0.jpg"));
    profiler_end(&ts);
    profiler_print(&ts);
    t1 = profiler_get_microseconds(&ts);


    GINFO("t0 %lld t1 %lld\n", t0, t1);
    GINFO("ID0 %d ID1 %d\n", id0, id1);
#else
    GINFO("Not use this shit\n");
#endif
}
