#ifndef SIMPLE_FAST_JSON_H
#define SIMPLE_FAST_JSON_H

/*
  todo:
  * make it single_header_library

  About lib:

  Lib divided into two parts:
  * SimpleFastJson.h - front-end actual tokenizer, should be smth like jsmn
  fast tokenizing - main job
  * SimpleFastJsonBackend.h - back-end process tokens and outputs better api,
  for getting or setting keys/values, writing json to disk, get actual arrays,
  objects, strings

*/

/*
	#############################
  DOCS: ###       FRONT-END       ###
	#############################
*/

// DOCS: it is possible to define type yourself,
// if for some reasone your platform behave differently

#if !defined(sfj_i8)
typedef char sfj_i8;
#endif

#if !defined(sfj_u8)
typedef unsigned char sfj_u8;
#endif

#if !defined(sfj_i32)
typedef int sfj_i32;
#endif

#if !defined(sfj_u32)
typedef unsigned int sfj_u32;
#endif

#if !defined(sfj_i64)
typedef long long int sfj_i64;
#endif

#if !defined(sfj_u64)
typedef unsigned long long sfj_u64;
#endif

#if !defined(sfj_real)
typedef float sfj_real;
#endif

#if !defined(sfj_null)
#define sfj_null ((void*)0)
#endif

#define SFJ_DEBUG 1
#if SFJ_DEBUG == 1
#define sfj_assert_break() ({volatile int*_some_ptr=sfj_null;*_some_ptr=0;})
#endif

typedef enum SFJTokenType
{
    SFJTokenType_Undefined = 0,

    SFJTokenType_Int,
    SFJTokenType_Int_Exp,
    SFJTokenType_Float,
    SFJTokenType_Float_Exp,
    SFJTokenType_String_Ascii,
    SFJTokenType_String_Unicode,
    SFJTokenType_Bool_True,
    SFJTokenType_Bool_False,
    SFJTokenType_Null,
    SFJTokenType_Array,
    SFJTokenType_Object,

    SFJTokenType_Count,
} SFJTokenType;

const char* sfj_token_type_to_string(SFJTokenType type);

typedef struct SFJToken
{
    SFJTokenType TokenType;
    sfj_i32 Parent;
    union
    {
	sfj_i32 Start;
	sfj_i32 StartInd;
    };
    
    union
    {
	sfj_i32 End;
	sfj_i32 EndInd;
    };
} SFJToken;

typedef enum SFJErrorType
{
    SFJErrorType_None = 0,
    SFJErrorType_NotEnoughtMemory,
    SFJErrorType_NotValidJson,
    SFJErrorType_HexInvalid,
    SFJErrorType_EscapeCharNotAllowed,
    SFJErrorType_MistakeInParser,
    SFJErrorType_WrongStartCharacter,

    // WrongInputParams
    SFJErrorType_TokensArrayIsNotAllocated,
} SFJErrorType;

const char* sfj_error_type_to_string(SFJErrorType type);

typedef struct SFJSetting
{
    const char* pText;
    sfj_u64 TextLength;
    sfj_u64 TokensCount;
    SFJToken* aTokens;
    SFJErrorType ErrorType;
} SFJSetting;

void sfj_parse_json(SFJSetting* pSetting);
sfj_i32 sfj_get_i32();



/*
	#############################
  DOCS: ###        BACK-END       ###
	#############################
*/


#endif // SIMPLE_FAST_JSON_H
