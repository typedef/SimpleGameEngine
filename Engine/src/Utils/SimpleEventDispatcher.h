#ifndef SIMPLE_EVENT_DISPATCHER_H
#define SIMPLE_EVENT_DISPATCHER_H

typedef void* (*SubscribeDelegate)(void* pData);

typedef struct SimpleEventSubcribe
{
    char* pName;
    SubscribeDelegate Function;
} SimpleEventSubcribe;

typedef struct SimpleEventDispatcher
{
    SimpleEventSubcribe* aSubscribers;
} SimpleEventDispatcher;

SimpleEventDispatcher simple_event_dispatcher_new();
void simple_event_dispatcher_register(SimpleEventDispatcher* pDispatcher, SimpleEventSubcribe newSub);
void simple_event_dispatcher_unregister(SimpleEventDispatcher* pDispatcher, const char* pName);
void simple_event_dispatcher_dispatch(SimpleEventDispatcher* pDispatcher, void* pData);

#endif // SIMPLE_EVENT_DISPATCHER_H
