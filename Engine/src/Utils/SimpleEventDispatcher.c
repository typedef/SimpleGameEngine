#include "SimpleEventDispatcher.h"

#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>

SimpleEventDispatcher
simple_event_dispatcher_new()
{
    SimpleEventDispatcher ses = {};

    return ses;
}

void
simple_event_dispatcher_register(SimpleEventDispatcher* pDispatcher, SimpleEventSubcribe newSub)
{
    i64 ind = array_index_of(pDispatcher->aSubscribers,
                             string_compare(item.pName, newSub.pName));

    if (ind != -1)
    {
        // warn?
        return;
    }

    if (newSub.Function == NULL)
    {
        // warn?
        return;
    }

    array_push(pDispatcher->aSubscribers, newSub);
}

void
simple_event_dispatcher_unregister(SimpleEventDispatcher* pDispatcher, const char* pName)
{
    i64 ind = array_index_of(pDispatcher->aSubscribers,
                             string_compare(item.pName, pName));
    if (ind == -1)
    {
        // warn?
        return;
    }

    array_remove_at(pDispatcher->aSubscribers, ind);
}

void
simple_event_dispatcher_dispatch(SimpleEventDispatcher* pDispatcher, void* pData)
{
    i64 i, count = array_count(pDispatcher->aSubscribers);
    for (i = 0; i < count; ++i)
    {
        SimpleEventSubcribe sub = pDispatcher->aSubscribers[i];
        sub.Function(pData);
    }
}
