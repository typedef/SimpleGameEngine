#include "tui.h"

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <wchar.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>

#include <Core/Types.h>

#if defined(LINUX_PLATFORM)

#include <termios.h>

i32
tui_getkey()
{
    struct termios orig_term_attr;
    struct termios new_term_attr;

    /* set the terminal to raw mode */
    tcgetattr(fileno(stdin), &orig_term_attr);
    memcpy(&new_term_attr, &orig_term_attr, sizeof(struct termios));
    new_term_attr.c_lflag &= ~(ECHO|ICANON);
    new_term_attr.c_iflag &= ~(ICRNL);
    tcsetattr(fileno(stdin), TCSANOW, &new_term_attr);

    /* read a character from the stdin stream without blocking */
    /*   returns EOF (-1) if no character is available */
    i32 character = fgetc(stdin);

    /* restore the original terminal attributes */
    tcsetattr(fileno(stdin), TCSANOW, &orig_term_attr);

    return character;
}

void
tui_printxy(int y, int x, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    printf("\033[%d;%dH", y, x);
    vprintf(format, args);
    va_end(args);
    fflush(stdout);
}

void
tui_flush()
{
    fflush(stdout);
}

void
tui_clear()
{
    system("clear");
}


#elif defined(WINDOWS_PLATFORM)

#include <windows.h>
#include <stdarg.h>
#include <conio.h>

i32
tui_getkey()
{
#if BAD
    if (_kbhit())
    {
        i32 c = fgetc(stdin);
        return c;
    }

    return -1;

#elif BADBAD
    HANDLE terminalHandle = GetStdHandle(STD_INPUT_HANDLE);
    SetConsoleMode(terminalHandle, ENABLE_WINDOW_INPUT | ENABLE_PROCESSED_INPUT);

    INPUT_RECORD inputRecord = {};
    DWORD count = 0;
    BOOL isSuccess = ReadConsoleInput(terminalHandle, &inputRecord, 1, &count);
    (void)isSuccess;
    CHAR c = inputRecord.Event.KeyEvent.uChar.AsciiChar;
    return (c > 0) ? c : -1;
#else
    HANDLE terminalHandle = GetStdHandle(STD_INPUT_HANDLE);
    SetConsoleMode(terminalHandle, ENABLE_WINDOW_INPUT | ENABLE_PROCESSED_INPUT);

    DWORD count = 0;
    char c;
    //CONSOLE_READCONSOLE_CONTROL inputControl = 0;
    ReadConsole(terminalHandle, &c, 1, &count, NULL);
    return c > 0 ? c : -1;
#endif
}

static char* gBuffer = NULL;
static i32 gBufferWidth;
static i32 gBufferHeight;

void
tui_printxy(int y, int x, const char* format, ...)
{
    if (gBuffer == NULL)
    {
        static HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
        CONSOLE_SCREEN_BUFFER_INFO csbi;
        COORD topLeft = { 0, 0 };

        // Figure out the current width and height of the console window
        if (!GetConsoleScreenBufferInfo(hOut, &csbi)) {
            // TODO: Handle failure!
            abort();
        }

        gBufferWidth = csbi.dwSize.X;
        gBufferHeight = csbi.dwSize.Y;

        for (i32 i = 0; i < (gBufferWidth*gBufferHeight); ++i)
        {
            string_builder_appendc(gBuffer, '\0');
        }
        string_builder_header(gBuffer)->Count = 0
    }

    char* writePtr = (char*) (gBuffer + x + gBufferWidth * y);

    va_list valist;
    va_start(valist, format);
    vsprintf(writePtr, format, valist);
    va_end(valist);
}

void
tui_flush()
{
    COORD coord = (COORD) {.X = 0, .Y = 0};
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

    printf(gBuffer);
    fflush(stdout);

    gBuffer = NULL;
    gBufferWidth  = 0;
    gBufferHeight = 0;
}

void
tui_clear()
{

#if 0
    ShowCursor(FALSE);
    system("cls");
#else
    // Get the Win32 handle representing standard output.
    // This generally only has to be done once, so we make it static.
    static HANDLE hOut = INVALID_HANDLE_VALUE;
    if (hOut == INVALID_HANDLE_VALUE)
        hOut = GetStdHandle(STD_OUTPUT_HANDLE);

    CONSOLE_SCREEN_BUFFER_INFO csbi;
    COORD topLeft = { 0, 0 };

    // std::cout uses a buffer to batch writes to the underlying console.
    // We need to flush that to the console because we're circumventing
    // std::cout entirely; after we clear the console, we don't want
    // stale buffered text to randomly be written out.
    tui_flush();

    // Figure out the current width and height of the console window
    if (!GetConsoleScreenBufferInfo(hOut, &csbi)) {
        // TODO: Handle failure!
        abort();
    }
    DWORD length = csbi.dwSize.X * csbi.dwSize.Y;

    DWORD written;

    // Flood-fill the console with spaces to clear it
    FillConsoleOutputCharacter(hOut, TEXT(' '), length, topLeft, &written);

    // Reset the attributes of every character to the default.
    // This clears all background colour formatting, if any.
    FillConsoleOutputAttribute(hOut, csbi.wAttributes, length, topLeft, &written);

    // Move the cursor back to the top left for the next sequence of writes
    SetConsoleCursorPosition(hOut, topLeft);
#endif
}

#endif
