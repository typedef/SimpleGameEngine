#include "SimpleHelpers.h"

#include <Core/Types.h>
#include <Core/SimpleStandardLibrary.h>
#include <Deps/stb_image.h>


i32
simple_helper_read_texture(void** ppData, const char* path, i32* width, i32* height, i32* channels)
{
    stbi_uc* data = stbi_load(path, width, height, channels, 0);
    //GINFO("[Texture] W=%d H=%d C=%d\n", width, height, channels);
    if (data == NULL)
    {
        GERROR("Texture could not be loaded %s\n", path);
        return 0;
    }

    // DOCS(typedef): this is obviously very bad situation
    i32 w = *width, h = *height, c = *channels;
    vguard(w > 0 && "Width should be greater then 0!");
    vguard(h > 0 && "Height should be greater then 0!");
    vguard(c > 0 && "Channels should be greater then 0!");

    *ppData = data;

    return 1;
}

void
simple_helper_free_texture(void* pData)
{
    stbi_image_free(pData);
}
