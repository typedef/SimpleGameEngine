#include "WavReader.h"

#include <stdio.h>
#include <Core/SimpleStandardLibrary.h>


void
wav_reader_header_print(WavHeader rawHeader)
{
    GINFO("ChunkID: %0.4s\n", rawHeader.ChunkID);
    GINFO("ChunkSize: %u\n", rawHeader.FileSize);
    GINFO("Format: %0.4s\n", rawHeader.Format);
    GINFO("FmtChunkID: %0.4s\n", rawHeader.FmtChunkID);
    GINFO("FmtChunkSize: %u\n", rawHeader.FmtChunkSize);
    GINFO("AudioFormat: %u\n", rawHeader.AudioFormat);
    GINFO("ChannelsCount: %u\n", rawHeader.ChannelsCount);
    GINFO("SampleRate: %u\n", rawHeader.SampleRate);
    GINFO("ByteRate: %u\n", rawHeader.ByteRate);
    GINFO("BlockAlign: %u\n", rawHeader.BlockAlign);
    GINFO("BitsPerSample: %u\n", rawHeader.BitsPerSample);
}

WavFile
wav_reader_read(const char* path)
{
    WavFile result;

    FILE* file = fopen(path, "r");
    vassert(file && "Can't open FILE!!!");

    WavHeader rawHeader;
    fread(&rawHeader, sizeof(WavHeader), 1, file);
    wav_reader_header_print(rawHeader);

    WavChunk chunk;
    fread(&chunk, sizeof(WavChunk), 1, file);

    u8* data = (u8*) memory_allocate(chunk.Size * sizeof(u8));
    fread(data, sizeof(u8), chunk.Size, file);

    fclose(file);

    WavFile wav = {
        .Header = rawHeader,
        .Chunk = chunk,
        .Data = data
    };

    return wav;
}
void
wav_reader_write(const char* path, WavFile wavFile)
{
    FILE* wfile = fopen(path, "w");
    vassert(wfile && "Can't open FILE for write!!!");

    fwrite(&wavFile.Header, 1, sizeof(WavHeader), wfile);
    fwrite(&wavFile.Chunk, 1, sizeof(WavChunk), wfile);
    fwrite(wavFile.Data, 1, wavFile.Chunk.Size, wfile);

    fclose(wfile);
}

void
wav_reader_free(WavFile file)
{
    memory_free(file.Data);
}
