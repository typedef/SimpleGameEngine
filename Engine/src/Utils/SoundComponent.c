#include "SoundComponent.h"

#include <stdio.h>

#include <Math/SimpleMath.h>
#include <Core/SimpleStandardLibrary.h>
#include <Utils/WavReader.h>
#include <Deps/miniaudio.h>


typedef struct SoundConfig
{
    f32 Pitch;
    f32 Gain;
    i32 IsLooping;
    u32 Buffer;
    v3  Position;
    v3  Velocity;
} SoundConfig;

void
sound_engine_get_devices()
{

}

void
sound_engine_init()
{
    ma_engine CurrentEngine;
    ma_result result = ma_engine_init(NULL, &CurrentEngine);

    //ma_engine* pEngine, ma_data_source* pDataSource, ma_uint32 flags, ma_sound_group* pGroup, ma_sound* pSound;

    ma_sound sound;
    //WavFile wavFile = wav_reader_read(asset_sound("SimpleSound2.wav"));
    //ma_sound_init_from_data_source(&CurrentEngine, wavFile.Data, 0, NULL, &sound);

    //NOTE(typedef): use default device format, channels
    ma_device_config config = ma_device_config_init(ma_device_type_playback);
    config.playback.format   = ma_format_unknown;
    config.playback.channels = 0;
    config.sampleRate        = 0;
    config.dataCallback      = NULL;
    config.pUserData         = NULL;
    ma_device device;
    if (ma_device_init(NULL, &config, &device) != MA_SUCCESS)
    {
        vassert(0 && "Can't init device!");
    }
    ma_device_start(&device);     // The device is sleeping by default so you'll need to start it manually.
    ma_device_uninit(&device);    // This will stop the device so no need to do that

    //ma_engine* pEngine, const char* pFilePath, ma_node* pNode, ma_uint32 nodeInputBusIndex
    ma_sound sound1, sound2, sound3;
    result = ma_sound_init_from_file(&CurrentEngine, asset_sound("SimpleSound.wav"), 0, NULL, NULL, &sound1);
    result = ma_sound_init_from_file(&CurrentEngine, asset_sound("SimpleSound2.wav"), 0, NULL, NULL, &sound2);
    result = ma_sound_init_from_file(&CurrentEngine, asset_sound("SimpleSound3.mp3"), 0, NULL, NULL, &sound3);

    ma_result playSoundResult = ma_engine_play_sound(&CurrentEngine, asset_sound("SimpleSound2.wav"), NULL);
    ma_result playSoundResult2 = ma_engine_play_sound(&CurrentEngine, asset_sound("SimpleSound.wav"), NULL);
    ma_result playSoundResult3 = ma_engine_play_sound(&CurrentEngine, asset_sound("SimpleSound3.mp3"), NULL);
    if (playSoundResult == MA_SUCCESS)
    {
        getchar();
    }

//ma_sound_at_end()
    //ma_device_start() //ma_device_stop()
    //ma_sound_set_position()

}

u32
sound_engine_create_buffer(WavFile* wavFile)
{
    return 1;
}

u32
sound_engine_create_source(SoundConfig config)
{
    u32 source = 0;
    return source;
}

void
sound_engine_destroy()
{
}

SoundComponent
sound_component_new()
{
    sound_engine_init();
    SoundComponent sc = {

    };


    return sc;
}
