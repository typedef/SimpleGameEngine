#ifndef SIMPLE_HELPERS_H
#define SIMPLE_HELPERS_H

int simple_helper_read_texture(void** ppData, const char* path, int* width, int* height, int* channels);
void simple_helper_free_texture(void* pData);

#endif // SIMPLE_HELPERS_H
