#include "SimpleJson.h"

#include <Core/SimpleStandardLibrary.h>

#pragma clang diagnostic ignored "-Wswitch"


/*
  ###################################
  #####       Parser Part       #####
  ###################################
*/
typedef enum SimpleJsonTokenType
{
    SimpleJsonTokenType_None = 0,

    SimpleJsonTokenType_Int,
    SimpleJsonTokenType_Float,
    SimpleJsonTokenType_String,
    SimpleJsonTokenType_Bool,
    SimpleJsonTokenType_Null,

    SimpleJsonTokenType_KeyValueSeparator,
    SimpleJsonTokenType_Object_Open,
    SimpleJsonTokenType_Object_Close,
    SimpleJsonTokenType_Array_Open,
    SimpleJsonTokenType_Array_Close,
    SimpleJsonTokenType_Comma,

    SimpleJsonTokenType_Count,
} SimpleJsonTokenType;

const char*
simple_json_token_type_to_string(SimpleJsonTokenType type)
{
    switch (type)
    {
    case SimpleJsonTokenType_None: return "None";

    case SimpleJsonTokenType_Int: return "Int";
    case SimpleJsonTokenType_Float: return "Float";
    case SimpleJsonTokenType_String: return "String";
    case SimpleJsonTokenType_Bool: return "Bool";
    case SimpleJsonTokenType_Null: return "Null";

    case SimpleJsonTokenType_KeyValueSeparator: return "KeyValueSeparator";
    case SimpleJsonTokenType_Object_Open: return "Object_Open";
    case SimpleJsonTokenType_Object_Close: return "Object_Close";
    case SimpleJsonTokenType_Array_Open: return "Array_Open";
    case SimpleJsonTokenType_Array_Close: return "Array_Close";
    case SimpleJsonTokenType_Comma: return "Comma";

    case SimpleJsonTokenType_Count: return "Count";
    }

    vassert_break();
    return NULL;
}

typedef struct SimpleJsonToken
{
    SimpleJsonTokenType Type;
    SimpleJsonValueUnion Data;
} SimpleJsonToken;

force_inline char*
_skip_spaces(char* stream)
{
    char c = *stream;
    while (c == ' ' || c == '\n' || c == '\t' || c == '\r')
    {
        ++stream;
        c = *stream;
    }

    return stream;
}

force_inline char*
_get_single_liner_lexem(char** pPtr, char escapeChar, i32 firstOffset)
{
    i32 isLexemValid = 0;

    i32 length = 0;
    char* ptr = *pPtr;
    char* sptr = ptr + firstOffset;
    char* oth = sptr;
    char oc = *oth;

    while (oc != '\n' || oc != '\0')
    {
        if (oc == escapeChar)
        {
            isLexemValid = 1;
            break;
        }

        ++length;

        ++oth;
        oc = *oth;
    }

    if (!isLexemValid)
    {
        GERROR("SimpleJson Lexer: StringError\n");
        vguard(0);
    }

    char* astr = string_substring_length(sptr, 1024, length);
    *pPtr = oth;

    return astr;
}

force_inline i32
_get_number_lexem(char** pPtr, void* pData, i32* pIsFloat)
{
    i32 length = 0;
    i32 containsInt = 0;
    i32 containsFloat = 0;

    char* sptr = *pPtr;
    char* oth = sptr;
    char oc = *oth;

    while (1)
    {
        if (char_is_integer(oc))
        {
            containsInt = 1;
        }
        else if (oc == '.')
        {
            containsFloat = 1;
        }
        else
        {
            i32 isSign = (oc == '-') || (oc == '+');
            if (!isSign)
            {
                break;
            }
        }

        ++length;
        ++oth;
        oc = *oth;
    }

    if (!containsInt && !containsFloat)
    {
        GERROR("SimpleJson Lexer: StringError\n");
        vguard(0);
    }

    if (containsFloat)
    {
        f32 number = string_to_f32_length(sptr, length);
        *((f32*)pData) = number;
        *pIsFloat = 1;
    }
    else if (containsInt)
    {
        i32 number = string_to_i32_length(sptr, length);
        *((i32*)pData) = number;
        *pIsFloat = 0;
    }
    else
    {
        return 0;
    }

    char* astr = string_substring_length(sptr, 1024, length);
    *pPtr = oth;

    return 1;
}

SimpleJsonObject* simple_json_object_parse(SimpleJsonToken* aTokens, i64 tokensCount, SimpleJsonParseResult* pReturn);

SimpleJsonToken*
simple_json_lexer_proccess(char* inputText, size_t textLength)
{
    SimpleJsonToken* tokens = NULL;

    // NOTE: Flags
    char* ptr = inputText;
    char c = *ptr;

#define NextToken()				\
    ({						\
        ++ptr;					\
        c = *ptr;				\
    })

    while (c != '\0')
    {
        ptr = _skip_spaces(ptr);
        c = *ptr;

        switch (c)
        {

        case '{':
        {
            SimpleJsonToken type = {
                .Type = SimpleJsonTokenType_Object_Open,
                .Data = { .pData = "{" },
            };

            array_push(tokens, type);
            NextToken();

            break;
        }

        case '}':
        {
            SimpleJsonToken type = {
                .Type = SimpleJsonTokenType_Object_Close,
                .Data = { .pData = "}" },
            };

            array_push(tokens, type);
            NextToken();

            break;
        }

        case '[':
        {
            SimpleJsonToken type = {
                .Type = SimpleJsonTokenType_Array_Open,
                .Data = { .pData = "[" },
            };

            array_push(tokens, type);
            NextToken();

            break;
        }

        case ']':
        {
            SimpleJsonToken type = {
                .Type = SimpleJsonTokenType_Array_Close,
                .Data = { .pData = "]" },
            };

            array_push(tokens, type);
            NextToken();

            break;
        }

        case '-': case '+': case '.':
        case '0':
        case '1': case '2': case '3':
        case '4': case '5': case '6':
        case '7': case '8': case '9':
        {
            char buf[4];
            i32 isFloat;
            i32 isSuccess = _get_number_lexem(&ptr, buf, &isFloat);

            SimpleJsonToken type;
            if (isFloat)
            {
                type = (SimpleJsonToken) {
                    .Type = SimpleJsonTokenType_Float,
                    .Data = { .F64Value = *((f32*) (&buf[0])) },
                };
            }
            else
            {
                type = (SimpleJsonToken) {
                    .Type = SimpleJsonTokenType_Int,
                    .Data = { .I64Value = *((i32*) (&buf[0])) },
                };
            }

            array_push(tokens, type);

            continue;
        }

        case '"':
        {
            char* astr = _get_single_liner_lexem(&ptr, '\"', 1);

            SimpleJsonToken type = {
                .Type = SimpleJsonTokenType_String,
                .Data = { .pData = astr },
            };

            array_push(tokens, type);
            NextToken();

            break;
        }

        case ',':
        {
            NextToken();

            break;
        }

        case ':':
        {
            NextToken();

            break;
        }

        // note: bad hack because we can't parse 1.0e-03
        case 'e':
        {
            while (*ptr != ',' && *ptr != '\0')
                ++ptr;
            break;
        }

        case 't':
        case 'f':
        {
            if (string_compare_length(ptr, "true", 4))
            {
                SimpleJsonToken type = {
                    .Type = SimpleJsonTokenType_Bool,
                    .Data = { .BoolValue = 1 },
                };
                array_push(tokens, type);

                ptr = ptr + 4;
                break;
            }
            else if (string_compare_length(ptr, "false", 5))
            {
                SimpleJsonToken type = {
                    .Type = SimpleJsonTokenType_Bool,
                    .Data = { .BoolValue = 0 },
                };
                array_push(tokens, type);
                ptr = ptr + 5;

                break;
            }
        }

        case 'n':
        {
            if (string_compare_length(ptr, "null", 4))
            {
                SimpleJsonToken type = {
                    .Type = SimpleJsonTokenType_Null,
                    .Data = { .pData = NULL },
                };

                array_push(tokens, type);
                ptr = ptr + 4;

                break;
            }
        }

        default:
        {
            /* i32 isLowerLetter = (c >= 'a' && c <= 'z'); */
            /* i32 isUpperLetter = (c >= 'A' && c <= 'Z'); */

            /* if (isLowerLetter || isUpperLetter) */
            /* { */
            /*	char* astr = _get_single_liner_lexem(&ptr, ':', 0); */

            /*	SimpleJsonToken type = { */
            /*	    .Type = SimpleJsonTokenType_Key, */
            /*	    .Data = { */
            /*		.pData = astr */
            /*	    }, */
            /*	}; */

            /*	array_push(tokens, type); */
            /* } */

            continue;
        }

        }

    }

    return tokens;
}

SimpleJsonValue
_simple_json_array_parse(SimpleJsonToken* aTokens, i64 tokensCount, i64* pI)
{
    i32 isFirstTokenValid = (aTokens[0].Type == SimpleJsonTokenType_Array_Open);
    if (!isFirstTokenValid)
    {
        GERROR("Wrong token type: %s\n", simple_json_token_type_to_string(aTokens[0].Type));
        vguard(isFirstTokenValid && "First token should be Array_Open");
    }

    i64 iv = *pI;

    SimpleJsonValue arrValue = {};
    void* returnResult = NULL;

    i8* i8Arr = NULL;
    i64* i64Arr = NULL;
    f64* f64Arr = NULL;
    char** strArr = NULL;
    SimpleJsonObject** objArr = NULL;

    SimpleJsonTokenType type = SimpleJsonTokenType_None;

#define SingleTypeGuard()						\
    ({									\
        if (type != SimpleJsonTokenType_None)				\
        {								\
            GERROR("type: %s tokenType: %s\n", simple_json_token_type_to_string(type), simple_json_token_type_to_string(token.Type)); \
            vguard((type == token.Type) && "Not single type array!!!");	\
        }								\
    })

    i64 i = 0;
    for ( ; i < tokensCount; )
    {
        SimpleJsonToken token = aTokens[i];

        if (token.Type == SimpleJsonTokenType_Array_Close)
        {
            break;
        }

        switch (token.Type)
        {

        case SimpleJsonTokenType_Int:
        {
            SingleTypeGuard();

            type = token.Type;
            i64 val = token.Data.I64Value;
            array_push(i64Arr, val);

            ++i;

            break;
        }

        case SimpleJsonTokenType_Float:
        {
            SingleTypeGuard();

            type = token.Type;
            f64 val = token.Data.F64Value;
            array_push(f64Arr, val);

            ++i;

            break;
        }

        case SimpleJsonTokenType_String:
        {
            SingleTypeGuard();

            type = token.Type;
            char* val = (char*)token.Data.pData;
            array_push(strArr, val);

            ++i;

            break;
        }

        case SimpleJsonTokenType_Bool:
        {
            SingleTypeGuard();

            type = token.Type;
            i8 val = (i8) token.Data.BoolValue;
            array_push(i8Arr, val);

            ++i;

            break;
        }

        case SimpleJsonTokenType_Object_Open:
        {
            SingleTypeGuard();

            type = token.Type;

            SimpleJsonParseResult parseResult = {};
            SimpleJsonObject* pObj =
                simple_json_object_parse(&aTokens[i], tokensCount, &parseResult);

            array_push(objArr, pObj);

            i += parseResult.EndIndex;

            break;
        }

        default:
            ++i;
            break;

        }

    }

    *pI = iv + i + 1;

    if (i8Arr)
    {
        arrValue = (SimpleJsonValue) {
            .Type = SimpleJsonValueType_Array_Bool,
            .Data = { .pData = i8Arr }
        };
    }
    else if (i64Arr)
    {
        arrValue = (SimpleJsonValue) {
            .Type = SimpleJsonValueType_Array_Int,
            .Data = { .pData = i64Arr }
        };
    }
    else if (f64Arr)
    {
        arrValue = (SimpleJsonValue) {
            .Type = SimpleJsonValueType_Array_Float,
            .Data = { .pData = f64Arr }
        };
    }
    else if (strArr)
    {
        arrValue = (SimpleJsonValue) {
            .Type = SimpleJsonValueType_Array_String,
            .Data = { .pData = strArr }
        };
    }
    else if (objArr)
    {
        arrValue = (SimpleJsonValue) {
            .Type = SimpleJsonValueType_Array_Object,
            .Data = { .pData = objArr }
        };
    }

    return arrValue;
}

SimpleJsonObject*
simple_json_object_parse(SimpleJsonToken* aTokens, i64 tokensCount, SimpleJsonParseResult* pReturn)
{
    SimpleJsonObject* obj = NULL;

    i32 isFirstValid = (aTokens[0].Type == SimpleJsonTokenType_Object_Open);
    if (!isFirstValid)
    {
        pReturn->Error = (SimpleJsonError) {
            .IsError = 1,
            .Error = "Not valid character for json-object"
        };
        return obj;
    }

    i64 isEnd = 1;
    for (i64 i = 0; i < tokensCount; ++i)
    {
        SimpleJsonToken token = aTokens[i];
        if (token.Type == SimpleJsonTokenType_Object_Open)
        {
            --isEnd;
        }
        else if (token.Type == SimpleJsonTokenType_Object_Close)
        {
            ++isEnd;
        }

        if (isEnd == 1)
        {
            isEnd = i;
            break;
        }
    }

    pReturn->EndIndex = isEnd;

    for (i64 i = 1; i < isEnd; )
    {
        SimpleJsonTokenType type = aTokens[i].Type;

        const char* key;
        SimpleJsonValue value = {};

        SimpleJsonToken keyToken = aTokens[i];
        SimpleJsonToken valToken = aTokens[i+1];

        key = (const char*) keyToken.Data.pData;

        i32 skipParserPart = 0;

        switch (valToken.Type)
        {

        case SimpleJsonTokenType_Int:
        {
            value = (SimpleJsonValue) {
                .Type = SimpleJsonValueType_Int,
                .Data = valToken.Data
            };

            i += 2;
            break;
        }

        case SimpleJsonTokenType_Float:
        {
            value = (SimpleJsonValue) {
                .Type = SimpleJsonValueType_Float,
                .Data = valToken.Data
            };

            i += 2;
            break;
        }

        case SimpleJsonTokenType_String:
        {
            value = (SimpleJsonValue) {
                .Type = SimpleJsonValueType_String,
                .Data = valToken.Data
            };

            i += 2;
            break;
        }

        case SimpleJsonTokenType_Bool:
        {
            value = (SimpleJsonValue) {
                .Type = SimpleJsonValueType_Bool,
                .Data = valToken.Data
            };

            i += 2;
            break;
        }

        case SimpleJsonTokenType_Null:
        {
            value = (SimpleJsonValue) {
                .Type = SimpleJsonValueType_Null,
                .Data = valToken.Data
            };

            i += 2;
            break;
        }

        case SimpleJsonTokenType_Object_Open:
        {
            ++i;

            SimpleJsonParseResult parseResult = {};
            SimpleJsonObject* pObj = simple_json_object_parse(&aTokens[i], tokensCount, &parseResult);

            if (parseResult.Error.IsError)
            {
                GERROR("%s\n", parseResult.Error.Error);
                vguard(parseResult.Error.IsError && "Error while parsing nested json-obj!");
            }

            value = (SimpleJsonValue) {
                .Type = SimpleJsonValueType_Object,
                .Data = { .pData = pObj },
            };

            i += parseResult.EndIndex + 1;

            break;
        }

        case SimpleJsonTokenType_Array_Open:
        {
            ++i;
            value = _simple_json_array_parse(&aTokens[i], isEnd, &i);
            break;
        }

        default:
            ++i;
            skipParserPart = 1;
            break;

        }

        if (!skipParserPart)
            shash_put(obj, key, value);
    }

    return obj;
}

SimpleJson
simple_json_parse(char* inputText, size_t inputLength)
{
    vguard_not_null(inputText);
    vguard(inputLength > 0 && "Input length should be greater then 0!");

    SimpleJson result = {};

    SimpleJsonToken* aTokens = simple_json_lexer_proccess(inputText, inputLength);

    /*
      TODO:
      Ideas, allocate all void* Data for SimpleJsonValue, from one allocated memory

      if ('{') -> create_object()
      if ('[') -> create_array()

      obj0 = {
       .Keys = ["key0", "key1", "key2", "key3"],
       .Values = ["val0", "val1", obj1, arr0, arr1]
      };

      {
        key0: val0,
        key1: val1,
        key2: {
          key0: val0,
          key1: val1,
        },
        key3: [0,1],
        key4: [{key0:1}, {key1:2}],
      }

    */

    SimpleJsonParseResult parseResult = {};
    result.Root = simple_json_object_parse(aTokens, array_count(aTokens), &parseResult);

    if (parseResult.Error.IsError)
    {
        GERROR("%s\n", parseResult.Error.Error);
    }

    return result;
}

SimpleJson
simple_json_parse_file(char* filePath)
{
    size_t fileLength;
    char* fileContent = file_read_string_ext(filePath, &fileLength);
    SimpleJson result = simple_json_parse(fileContent, fileLength);
    return result;
}

char* _simple_json_object_to_string(SimpleJsonObject* pObj, SimpleJsonWriteSettings set);

char*
_simple_json_object_to_string(SimpleJsonObject* pObj, SimpleJsonWriteSettings set)
{
    char* sb = NULL;

    i32 tabLvl = set.TabIndex;
    const char* tab4 = "    ";

    // NOTE: Tab write
#define Tabify()						\
    {								\
        if (set.WriteType == SimpleJsonWriteType_Extended)	\
        {							\
            for (i64 t = 0; t < tabLvl; ++t)			\
            {							\
                string_builder_appends(sb, tab4);		\
            }							\
        }							\
    }

    Tabify();

    string_builder_appends(sb, "{");
    if (set.WriteType == SimpleJsonWriteType_Extended)
        string_builder_appends(sb, "\n");
    ++tabLvl;

    i64 cnt = table_capacity(pObj);

#define ArrayWriteToSb(formatString)					\
    {									\
        i64 arrcnt = array_count(arr) - 1;				\
        string_builder_appends(sb, "[");				\
        for (i64 ai = 0; ai <= arrcnt; ++ai)				\
        {								\
            string_builder_appendf(sb, formatString, arr[ai]);		\
                                                                        \
            if (ai != arrcnt)						\
            {								\
                string_builder_appends(sb, ",");			\
                if (set.WriteType == SimpleJsonWriteType_Extended)	\
                    string_builder_appends(sb, " ");			\
            }								\
        }								\
                                                                        \
        string_builder_appends(sb, "]");				\
        if (i != lastInd)						\
        {								\
            string_builder_appends(sb, ",");				\
        }								\
    }

    i64 lastInd = -1;
    for (i64 i = 0; i < cnt; ++i)
    {
        SimpleJsonObject obj = pObj[i];

        if (obj.Key == NULL)
        {
            continue;
        }

        lastInd = i;
    }

    vguard(lastInd != -1 && "Last index is -1!");

    for (i64 i = 0; i < cnt; ++i)
    {
        SimpleJsonObject obj = pObj[i];

        const char* key = obj.Key;
        SimpleJsonValue value = obj.Value;

        if (key == NULL)
        {
            continue;
        }

        // NOTE: Tab write
        if (set.WriteType == SimpleJsonWriteType_Extended)
        {
            Tabify();
        }

        string_builder_appendf(sb, "\"%s\":", key);
        if (set.WriteType == SimpleJsonWriteType_Extended)
            string_builder_appends(sb, " ");

        // value
        switch (value.Type)
        {

        case SimpleJsonValueType_Int:
        {
            string_builder_appendf(sb, "%d", value.Data.I64Value);

            if (i != lastInd)
            {
                string_builder_appends(sb, ",");
            }

            if (set.WriteType == SimpleJsonWriteType_Extended)
                string_builder_appends(sb, "\n");
            break;
        }

        case SimpleJsonValueType_Bool:
        {
            string_builder_appendf(sb, "%s", value.Data.BoolValue ? "true" : "false");

            if (i != lastInd)
            {
                string_builder_appends(sb, ",");
            }

            if (set.WriteType == SimpleJsonWriteType_Extended)
                string_builder_appends(sb, "\n");
            break;
        }

        case SimpleJsonValueType_Float:
        {
            string_builder_appendf(sb, "%f", value.Data.F64Value);

            if (i != lastInd)
            {
                string_builder_appends(sb, ",");
            }

            if (set.WriteType == SimpleJsonWriteType_Extended)
                string_builder_appends(sb, "\n");

            break;
        }

        case SimpleJsonValueType_String:
        {
            string_builder_appendf(sb, "\"%s\"", (char*)value.Data.pData);

            if (i != lastInd)
            {
                string_builder_appends(sb, ",");
            }

            if (set.WriteType == SimpleJsonWriteType_Extended)
                string_builder_appends(sb, "\n");

            break;
        }

        case SimpleJsonValueType_Array_Int:
        {
            i64* arr = (i64*) value.Data.pData;
            ArrayWriteToSb("%d");

            if (set.WriteType == SimpleJsonWriteType_Extended)
                string_builder_appends(sb, "\n");

            break;
        }

        case SimpleJsonValueType_Array_Float:
        {
            f64* arr = (f64*) value.Data.pData;
            ArrayWriteToSb("%f");

            if (set.WriteType == SimpleJsonWriteType_Extended)
                string_builder_appends(sb, "\n");

            break;
        }

        case SimpleJsonValueType_Array_Bool:
        {
            i64* arr = (i64*) value.Data.pData;

            i64 arrcnt = array_count(arr) - 1;
            string_builder_appends(sb, "[");
            for (i64 ai = 0; ai <= arrcnt; ++ai)
            {
                string_builder_appendf(sb, "%s", arr[ai] ? "true" : "false");

                if (ai != arrcnt)
                {
                    string_builder_appends(sb, ",");
                    if (set.WriteType == SimpleJsonWriteType_Extended)
                        string_builder_appends(sb, " ");

                }
            }

            string_builder_appends(sb, "]");

            if (i != lastInd)
            {
                string_builder_appends(sb, ",");
            }

            if (set.WriteType == SimpleJsonWriteType_Extended)
                string_builder_appends(sb, "\n");

            break;
        }

        case SimpleJsonValueType_Array_String:
        {
            char** arr = (char**) value.Data.pData;
            ArrayWriteToSb("%s");

            if (set.WriteType == SimpleJsonWriteType_Extended)
                string_builder_appends(sb, "\n");

            break;
        }

        case SimpleJsonValueType_Array_Object:
        {
            string_builder_appends(sb, "[");

            if (set.WriteType == SimpleJsonWriteType_Extended)
            {
                string_builder_appends(sb, "\n");
            }

            SimpleJsonWriteSettings innSet = {
                .TabIndex = tabLvl + 1,
                .WriteType = set.WriteType,
            };

            SimpleJsonObject** aObj = (SimpleJsonObject**) value.Data.pData;

            i64 arrObjCnt = array_count(aObj);
            for (i64 oi = 0; oi < arrObjCnt; ++oi)
            {
                SimpleJsonObject* pObj = aObj[oi];

                char* sbObj =
                    _simple_json_object_to_string(pObj, innSet);
                string_builder_appendf(sb, "%s", sbObj);

                if (set.WriteType == SimpleJsonWriteType_Extended)
                {
                    if (oi < (arrObjCnt - 1))
                    {
                        string_builder_appends(sb, ",\n");
                    }
                }
            }

            string_builder_appends(sb, "\n");
            Tabify();
            string_builder_appends(sb, "]");

            if (i != lastInd)
            {
                string_builder_appends(sb, ",");
            }

            if (set.WriteType == SimpleJsonWriteType_Extended)
            {
                string_builder_appends(sb, "\n");
            }

            break;
        }

        case SimpleJsonValueType_Object:
        {
            SimpleJsonWriteSettings settings = {
                .TabIndex = tabLvl,
                .WriteType = set.WriteType,
            };

            if (set.WriteType == SimpleJsonWriteType_Extended)
                string_builder_appends(sb, "\n");

            char* sbObj =
                _simple_json_object_to_string((SimpleJsonObject*) value.Data.pData, settings);

            string_builder_appendf(sb, "%s", sbObj);

            if (i != lastInd)
            {
                string_builder_appends(sb, ",");
            }

            if (set.WriteType == SimpleJsonWriteType_Extended)
                string_builder_appends(sb, "\n");

            break;
        }

        }

    }

    --tabLvl;
    if (set.WriteType == SimpleJsonWriteType_Extended)
    {
        for (i64 t = 0; t < tabLvl; ++t)
        {
            string_builder_appends(sb, tab4);
        }
    }

    string_builder_appends(sb, "}");

    return sb;
}

void
simple_json_write_file(SimpleJson* pJson, SimpleJsonWriteSettings set)
{
    char* sb = _simple_json_object_to_string(pJson->Root, set);
    file_write_string(set.Path, sb, string_builder_count(sb));
}
