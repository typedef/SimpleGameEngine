#include "Font.h"

#include <string.h>
#include <stdio.h>
#include <math.h>

#include <Deps/stb_truetype.h>
#include <Deps/stb_image_write.h>
#include <Core/SimpleStandardLibrary.h>


//TODO(typedef): rewrite this
void
stbi__vertical_flip(void *image, int w, int h, int bytes_per_pixel)
{
    int row;
    size_t bytes_per_row = (size_t)w * bytes_per_pixel;
    u8 temp[2048];
    u8 *bytes = (u8 *)image;

    for (row = 0; row < (h>>1); row++) {
        u8 *row0 = bytes + row*bytes_per_row;
        u8 *row1 = bytes + (h - row - 1)*bytes_per_row;
        // swap row0 with row1
        size_t bytes_left = bytes_per_row;
        while (bytes_left) {
            size_t bytes_copy = (bytes_left < sizeof(temp)) ? bytes_left : sizeof(temp);
            memcpy(temp, row0, bytes_copy);
            memcpy(row0, row1, bytes_copy);
            memcpy(row1, temp, bytes_copy);
            row0 += bytes_copy;
            row1 += bytes_copy;
            bytes_left -= bytes_copy;
        }
    }
}

static void
char_char_print(CharChar charchar)
{
    printf("\n\n    CharChar(%lc): UV(%f %f) Size(%f %f) Char()\n\n\n",
           charchar.Character,
           charchar.UV.X, charchar.UV.Y,
           charchar.Size.Width, charchar.Size.Height);
}

typedef struct GlyphProcessingSetting
{
    u8* bitmap;
    stbtt_fontinfo Info;

    f32 Scale;
    f32 pixelDistScale;

    i32 bitmapWidth;  //= 512,
    i32 bitmapHeight; //= 512,
    i32 lineHeight;   //= settings.FontSize;

    i32 padding;
    i32 onEdgeValue; /* DOCS: 0..255 */
    i32 globalXOffset;
    i32 globalYOffset;
    i32 maxHeight;
    i32 uvy;
    i32 globalMaxHeight;

    i32 i;

    CharChar* charChars;
} GlyphProcessingSetting;

void
_font_glyph_processing(GlyphProcessingSetting* pSet)
{
    i32 w, h, xoff, yoff;
    u8* singleCharacter = stbtt_GetCodepointSDF(&pSet->Info, pSet->Scale, pSet->i/*(i32)L'А'*/, pSet->padding, pSet->onEdgeValue, pSet->pixelDistScale, &w, &h, &xoff, &yoff);
    i32 offset = 0, roffset = 0;

    if ((pSet->globalXOffset + w) >= pSet->bitmapWidth)
    {
        pSet->globalXOffset = 0;
        pSet->globalYOffset += pSet->maxHeight * pSet->bitmapWidth;
        pSet->uvy += pSet->maxHeight;
        pSet->maxHeight = 0;
    }

    if (h > pSet->maxHeight)
    {
        pSet->maxHeight = h;
    }

    if (h > pSet->globalMaxHeight)
    {
        pSet->globalMaxHeight = h;
    }

    i32 ascent, descent, lineGap;
    stbtt_GetFontVMetrics(&pSet->Info, &ascent, &descent, &lineGap);

    CharChar charchar = {
        .UV = {
            .X = f32(pSet->globalXOffset) / pSet->bitmapWidth,
            .Y = f32(pSet->uvy) / pSet->bitmapHeight
        },
        .Size = {
            .Width  = f32(w) / pSet->bitmapWidth,
            .Height = f32(h) / pSet->bitmapHeight
        },
        .FullWidth = w,
        .FullHeight = h,
        .XOffset = xoff,
        .YOffset = h + yoff,
        .Character = pSet->i
    };
    /* printf("%lc: %d\n", i, xoff); */
    array_push(pSet->charChars, charchar);

    for (i32 j = 0; j < h; ++j)
    {
        memcpy(pSet->bitmap + pSet->globalXOffset + pSet->globalYOffset + offset, singleCharacter + roffset, w);
        offset += pSet->bitmapWidth;
        roffset += w;
    }

    stbtt_FreeSDF(singleCharacter, NULL);

    pSet->globalXOffset += w;

}

Font
font_generic_create(FontInfoSettings settings, i32 range0, i32 range1, i32* extRanges)
{
    vguard_not_null(extRanges);

    i32 bitmapWidth = 512,
        bitmapHeight = 512,
        lineHeight = settings.FontSize;

    u8* bytes = file_read_bytes(settings.Path);
    stbtt_fontinfo info;
    if (!stbtt_InitFont(&info, bytes, 0))
    {
        vassert(0 && "Font init failed!");
    }

    f32 scale = stbtt_ScaleForPixelHeight(&info, lineHeight);

    //char* word = "the quick brown fox";
    u8* bitmap = memory_allocate(bitmapWidth * bitmapHeight * sizeof(char));
    memset(bitmap, '\0', bitmapWidth * bitmapHeight * sizeof(char));

    f32 padding = 2;
    f32 onEdgeValue = 153 /*0..255*/;
    f32 pixelDistScale = f32(onEdgeValue) / padding;

    GlyphProcessingSetting set = {
        .bitmap = bitmap,
        .Info = info,
        .Scale = scale,
        .pixelDistScale = pixelDistScale,

        .bitmapWidth = 512,
        .bitmapHeight = 512,
        .lineHeight = settings.FontSize,
        .padding = padding,
        .onEdgeValue = onEdgeValue, /* 0..255 */
    };

    for (i32 i = (i32)range0; i <= (i32)range1; ++i)
    {
        set.i = i;
        _font_glyph_processing(&set);
    }

    for (i32 j = 0; j < array_count(extRanges); ++j)
    {
        set.i = extRanges[j];
        _font_glyph_processing(&set);
    }

    i32 maxYOffset = -1.0f;
    CharRecord* charTable = NULL;
    for (i32 i = 0; i < array_count(set.charChars); ++i)
    {
        CharChar charchar = set.charChars[i];
        if (charchar.YOffset > maxYOffset)
        {
            maxYOffset = charchar.YOffset;
        }
        hash_put(charTable, i32(charchar.Character), charchar);
    }

    Font fontInfo = {
        .Name = string(path_get_name(settings.Path)),
        .CharTable = charTable,
        .MaxHeight = set.globalMaxHeight,
        .MaxYOffset = maxYOffset,
        .BitmapWidth = bitmapWidth,
        .BitmapHeight = bitmapHeight,
        .Bitmap = bitmap,
        .FontSize = settings.FontSize,
        .Scale = 1.0f
    };

    if (settings.IsSaveBitmap)
    {
        stbi_write_png(settings.BitmapSavePath, bitmapWidth, bitmapHeight, 1, bitmap, bitmapWidth);
    }

    array_free(set.charChars);

    return fontInfo;
}

Font
font_cirilic_create(FontInfoSettings settings)
{
    i32 cirilicRange0 = (i32)L'А', cirilicRange1 = (i32)L'я';
    vassert(cirilicRange0 > 1000 && "i32 cirilicRange0 = (i32)L'А', where A is english A!");
    vassert(cirilicRange1 > cirilicRange0 && "wrong L'я' value!");

    i32* extRanges = NULL;
    array_push(extRanges, (i32)L'Ё'); array_push(extRanges, (i32)L'ё');
    array_push(extRanges, (i32)L'0'); array_push(extRanges, (i32)L'1');
    array_push(extRanges, (i32)L'2'); array_push(extRanges, (i32)L'3');
    array_push(extRanges, (i32)L'4'); array_push(extRanges, (i32)L'5');
    array_push(extRanges, (i32)L'6'); array_push(extRanges, (i32)L'7');
    array_push(extRanges, (i32)L'8'); array_push(extRanges, (i32)L'9');

    array_push(extRanges, (i32)L'.'); array_push(extRanges, (i32)L',');
    array_push(extRanges, (i32)L'/'); array_push(extRanges, (i32)L'?');
    array_push(extRanges, (i32)L'+'); array_push(extRanges, (i32)L'!');
    array_push(extRanges, (i32)L'-'); array_push(extRanges, (i32)L'_');
    array_push(extRanges, (i32)L'=');
    array_push(extRanges, (i32)L'('); array_push(extRanges, (i32)L')');
    array_push(extRanges, (i32)L'['); array_push(extRanges, (i32)L']');
    array_push(extRanges, (i32)L'{'); array_push(extRanges, (i32)L'}');
    array_push(extRanges, (i32)L':'); array_push(extRanges, (i32)L';');
    array_push(extRanges, (i32)L'*'); array_push(extRanges, (i32)L'%');
    array_push(extRanges, (i32)L'$'); array_push(extRanges, (i32)L'^');
    array_push(extRanges, (i32)L'"'); array_push(extRanges, (i32)L'\'');
    array_push(extRanges, (i32)L'\\'); array_push(extRanges, (i32)L'/');
    //array_push(extRanges, (i32)L'|');

    array_push(extRanges, (i32)L'A'); array_push(extRanges, (i32)L'a');
    array_push(extRanges, (i32)L'B'); array_push(extRanges, (i32)L'b');
    array_push(extRanges, (i32)L'C'); array_push(extRanges, (i32)L'c');
    array_push(extRanges, (i32)L'D'); array_push(extRanges, (i32)L'd');
    array_push(extRanges, (i32)L'E'); array_push(extRanges, (i32)L'e');
    array_push(extRanges, (i32)L'F'); array_push(extRanges, (i32)L'f');
    array_push(extRanges, (i32)L'G'); array_push(extRanges, (i32)L'g');
    array_push(extRanges, (i32)L'H'); array_push(extRanges, (i32)L'h');
    array_push(extRanges, (i32)L'I'); array_push(extRanges, (i32)L'i');
    array_push(extRanges, (i32)L'J'); array_push(extRanges, (i32)L'j');
    array_push(extRanges, (i32)L'K'); array_push(extRanges, (i32)L'k');
    array_push(extRanges, (i32)L'L'); array_push(extRanges, (i32)L'l');
    array_push(extRanges, (i32)L'M'); array_push(extRanges, (i32)L'm');
    array_push(extRanges, (i32)L'N'); array_push(extRanges, (i32)L'n');
    array_push(extRanges, (i32)L'O'); array_push(extRanges, (i32)L'o');
    array_push(extRanges, (i32)L'P'); array_push(extRanges, (i32)L'p');
    array_push(extRanges, (i32)L'Q'); array_push(extRanges, (i32)L'q');
    array_push(extRanges, (i32)L'R'); array_push(extRanges, (i32)L'r');
    array_push(extRanges, (i32)L'S'); array_push(extRanges, (i32)L's');
    array_push(extRanges, (i32)L'T'); array_push(extRanges, (i32)L't');
    array_push(extRanges, (i32)L'U'); array_push(extRanges, (i32)L'u');
    array_push(extRanges, (i32)L'V'); array_push(extRanges, (i32)L'v');
    array_push(extRanges, (i32)L'W'); array_push(extRanges, (i32)L'w');
    array_push(extRanges, (i32)L'X'); array_push(extRanges, (i32)L'x');
    array_push(extRanges, (i32)L'Y'); array_push(extRanges, (i32)L'y');
    array_push(extRanges, (i32)L'Z'); array_push(extRanges, (i32)L'z');


    Font cirilicFontInfo = font_generic_create(settings, cirilicRange0, cirilicRange1, extRanges);
    array_free(extRanges);

    return cirilicFontInfo;
}

void
font_destroy(Font font)
{
    memory_free(font.Name);
    table_free(font.CharTable);
    memory_free(font.Bitmap);
}

i32
font_get_space_width(const Font* font)
{
    f32 val = font->MaxHeight / 3.0f * font->Scale;
    return val;
}

void
font_get_string_metrics_ext(const Font* font, wchar* buffer, i32 length, f32* w, f32* h)
{
    vguard_not_null(font);

    f32 width  = 0.0f;
    f32 height = 0.0f;

    for (i32 i = 0; i < length; ++i)
    {
        wchar wc = buffer[i];

        CharChar value = hash_get(font->CharTable, (i32)wc);
        i32 tableInd = table_index(font->CharTable);

        if (wc == L'\n' || wc == '\n')
        {
            height += font->MaxHeight + value.YOffset;
        }
        else if (wc != ' ')
        {

            if (tableInd == -1)
            {
                wchar charBuf[2] = {
                    [0] = wc,
                    [1] = '\0'
                };
                GERROR("Can't find %d symbol in hash table!\n", (i32) wc);
                GERROR("i: %d ind: %d wci: %d char: %ls\n", i, tableInd, wc, charBuf);
                vguard(tableInd != -1 && "No char!");
            }

            width += value.FullWidth;
            f32 temp = value.FullHeight;// + value.YOffset;
            height = Max(height, temp);
        }
        else
        {
            f32 val = font_get_space_width(font);
            width  += val;
        }
    }

    *w = width;
    *h = height;
}

void
font_get_string_metrics(const Font* font, WideString str, f32* w, f32* h)
{
    vguard_not_null(font);
    font_get_string_metrics_ext(font, str.Buffer, str.Length, w, h);
}

void
font_get_utf8_string_metrics(const Font* font, char* utf8Str, f32* w, f32* h)
{
    vguard_not_null(font);
    vguard_not_null(utf8Str);

    WideString str = wide_string_utf8(utf8Str);

    font_get_string_metrics_ext(font, str.Buffer, str.Length, w, h);
}


f32
font_get_first_char_y_offset(const Font* font, WideString str)
{
    wchar wc = str.Buffer[0];
    CharChar value = hash_get(font->CharTable, (i32)wc);

    f32 yOffset = font->Scale * (value.FullHeight - value.YOffset);
    return value.YOffset;
}

void
font_get_first_char_y(const Font* font, WideString str, v3 position, f32* py0, f32* py1)
{
    wchar wc = str.Buffer[0];
    CharChar charChar = hash_get(font->CharTable, (i32)wc);
    f32 y0 = position.Y
        - font->Scale * (charChar.FullHeight + charChar.YOffset);
    f32 y1 = y0 + font->Scale * charChar.FullHeight;

    *py0 = y0;
    *py1 = y1;
}

f32
font_get_string_width(const Font* font, WideString str)
{
    f32 width, h;
    font_get_string_metrics(font, str, &width, &h);
    return width;
}

f32
font_get_string_height(const Font* font, WideString str)
{
    f32 w, height;
    font_get_string_metrics(font, str, &w, &height);
    return height;
}
