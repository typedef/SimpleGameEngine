#ifndef SIMPLE_CHUNK_H
#define SIMPLE_CHUNK_H

#include <Core/Types.h>

struct StaticModel;


typedef struct SimpleChunk
{
    i64 Id;

    i64* pModelIds;
    m4* pStaticTansforms;
    //i64* pMaterialIds; // Model = Meshes + Materials

    //SkinModels* pSkinModels;
    //m4* skinTransforms;
} SimpleChunk;

#endif // SIMPLE_CHUNK_H
