#ifndef EDITOR_VIEWPORT_H
#define EDITOR_VIEWPORT_H

#include <Core/Types.h>

struct CameraComponent;
struct Event;

typedef void (*OnCameraUpdateDelegate)(struct CameraComponent* pCameraComponent);

typedef struct SimpleEditorViewportSetting
{
    i32 IsCameraMovementEnabled;
    OnCameraUpdateDelegate OnCameraUpdate;
} SimpleEditorViewportSetting;

typedef struct EditorViewport
{
    i8 IsActive;
    i8 IsMovementEnabled;
    struct CameraComponent* pCamera;
    OnCameraUpdateDelegate OnUpdate;
} EditorViewport;

void editor_viewport_create(EditorViewport* pViewport, SimpleEditorViewportSetting set);
void editor_viewport_set_camera(EditorViewport* pViewport, struct CameraComponent* pCamera);
void editor_viewport_activate(EditorViewport* pViewport);
void editor_viewport_disable(EditorViewport* pViewport);
void editor_viewport_switch(EditorViewport* pViewport);
void editor_viewport_enable_movement(EditorViewport* pViewport);
void editor_viewport_disable_movement(EditorViewport* pViewport);
void editor_viewport_camera_update_fov(EditorViewport* pViewport, f32 fovDegree);

i32 editor_viewport_on_update(EditorViewport* pViewport, f32 timestep);
void editor_viewport_on_event(EditorViewport* pViewport, struct Event* event);


#endif // EDITOR_VIEWPORT_H
