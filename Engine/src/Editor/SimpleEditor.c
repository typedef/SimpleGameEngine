#include "SimpleEditor.h"
#include "EntitySystem/Components/Base/TransformComponent.h"
#include "EntitySystem/SimpleEcs.h"
#include "Graphics/Vulkan/Vsr2d.h"
#include "InputSystem/KeyCodes.h"
#include "Model/StaticModel.h"

#include <Utils/SimpleEventDispatcher.h>
#include <Core/SimpleStandardLibrary.h>

#include <Application/Application.h>
#include <Event/Event.h>
#include <Editor/EditorViewport.h>
#include <EntitySystem/SimpleScene.h>
#include <EntitySystem/Components/Base/CameraComponent.h>
#include <EntitySystem/Components/3D/StaticModelComponent.h>
#include <Math/SimpleMath.h>
#include <Math/SimpleObbHelper.h>
#include <Math/SimpleMathIO.h>
#include <InputSystem/SimpleInput.h>

/*
  DOCS: It should control

  We can register editor as layer

  editor_viewport
  editor_world


 */

static SimpleEditor gEditor = {};

static EditorViewport gEditorViewport = {};

void*
_set_viewport_camera_on_update(void* pData)
{
    GINFO("Heeeey camera changed!\n");
    CameraComponent* pCamera = (CameraComponent*) pData;
    editor_viewport_set_camera(&gEditorViewport, pCamera);
    return NULL;
}

void
simple_editor_on_attach()
{
    // NOTE/TODO: This pointer can be changed dynamically
    CameraComponent* pCamera = simple_scene_get_main_camera(gEditor.pScene);
    GINFO("simple_editor_on_attach: %p (%p)\n", pCamera, gEditor.pScene);
    SimpleEditorViewportSetting editorViewportSetting = {
        .IsCameraMovementEnabled = 1,
        .OnCameraUpdate = simple_scene_renderer_camera_updated,
    };
    editor_viewport_create(&gEditorViewport, editorViewportSetting);
    editor_viewport_set_camera(&gEditorViewport, pCamera);

    SimpleEventSubcribe subscribe = {
        .pName = "EditorViewport",
        .Function = _set_viewport_camera_on_update,
    };
    simple_scene_camera_subscribe(gEditor.pScene, subscribe);
}

void
simple_editor_on_attach_finished()
{
    //
}

void
_simple_editor_draw_selected(SimpleEditor* pEditor)
{

    if (pEditor->pScene->SelectedEntity < 0)
        return;

    i64 selectedEntity = GetEntityAsArrayIndex(pEditor->pScene->SelectedEntity);
    EntityStorage storage = pEditor->pScene->pEcsWorld->EStorage;
    EntityRecord entityRec = storage.Records[selectedEntity];

    TransformComponent* ptc = GetComponent(entityRec.Id, TransformComponent);
    StaticModelComponent* ptmc = GetComponent(entityRec.Id, StaticModelComponent);


    // note: draw selected box
    //pViewport->Callbacks.SubmitLine();
    ModelAabb aabb = ptmc->Model.Aabb;
    m4 transform = ptc->Matrix;
    f32 constSub = 0.008f;
    v3 contSubV3 = v3_new(constSub, constSub, constSub);
    v3 min = v3_sub(aabb.Min, contSubV3);
    v3 max = v3_add(aabb.Max, contSubV3);

    min = m4_mul_v3(transform, min);
    max = m4_mul_v3(transform, max);

    typedef struct LinePos
    {
        v3 P0;
        v3 P1;
    } LinePos;

    LinePos poss[] = {
        [0] = (LinePos) { .P0 = min, .P1 = v3_new(max.X, min.Y, min.Z) },
        [1] = (LinePos) { .P0 = min, .P1 = v3_new(min.X, max.Y, min.Z) },
        [2] = (LinePos) { .P0 = min, .P1 = v3_new(min.X, min.Y, max.Z) },

        [3] = (LinePos) { .P0 = max, .P1 = v3_new(min.X, max.Y, max.Z) },
        [4] = (LinePos) { .P0 = max, .P1 = v3_new(max.X, min.Y, max.Z) },
        [5] = (LinePos) { .P0 = max, .P1 = v3_new(max.X, max.Y, min.Z) },

        [6] = (LinePos) { .P0 = v3_new(min.X, min.Y, max.Z), .P1 = v3_new(max.X, min.Y, max.Z) },
        [7] = (LinePos) { .P0 = v3_new(min.X, min.Y, max.Z), .P1 = v3_new(min.X, max.Y, max.Z) },
        [8] = (LinePos) { .P0 = v3_new(min.X, max.Y, min.Z), .P1 = v3_new(min.X, max.Y, max.Z) },

        [9] = (LinePos) { .P0 = v3_new(max.X, min.Y, max.Z), .P1 = v3_new(max.X, min.Y, min.Z) },
        [10] = (LinePos) { .P0 = v3_new(max.X, max.Y, min.Z), .P1 = v3_new(min.X, max.Y, min.Z) },
        [11] = (LinePos) { .P0 = v3_new(max.X, max.Y, min.Z), .P1 = v3_new(max.X, min.Y, min.Z) },
    };

    i8 minAxisOnly = 0;
    i8 maxAxisOnly = 0;

    i32 startL = 0;
    i32 endL = ARRAY_COUNT(poss);
    if (minAxisOnly)
    {
        endL = 3;
    }
    else if (maxAxisOnly)
    {
        startL = 3;
        endL = 6;
    }

    for (i32 l = startL; l < endL; ++l)
    {
        LinePos linePos = poss[l];
        pEditor->Callbacks.SubmitLine(linePos.P0,
                                      linePos.P1,
                                      0.01f,
                                      v4_new(1, 0, 0, 1));
    }

    pEditor->Callbacks.Flush();

#if 1
    SimpleWindow* pWindow = application_get_window();
    CameraComponent* pCc = (CameraComponent*)gEditorViewport.pCamera;
    v3 cameraPos = pCc->Settings.Position;
    Camera camera = pCc->Base;

    pEditor->Callbacks.SetVisibleRect(v2_new(0,0), v2_new(5000, 5000));

    // note: let's start it here
    // note: rendering z only for now
    v3 convertToScreen;
    v3 objPosInClipSpace;
    {
        // note: obj -> clipspace
        v3 objPos = ptc->Translation;
        // this is wrong
        v3 objPosInCameraOffset = v3_sub(objPos, cameraPos);
        v3 objPosToClipSpace = m4_mul_v3(camera.ViewProjection, objPos);
        //v3_print(objPosToClipSpace);

        // note: clipspace -> screenspace for 2d projection
        // 2d camera
        Camera* p2dCamera = pEditor->Callbacks.Get2dCamera();
        m4 iproj = m4_inverse(p2dCamera->Projection);
        objPosInClipSpace = m4_mul_v3(iproj, objPosToClipSpace);
        //v2_print(*(v2*)&objPosInClipSpace);
        m4 iview = m4_inverse(p2dCamera->View);
        objPosInClipSpace = m4_mul_v3(iview, objPosInClipSpace);
        //v2_print(*(v2*)&objPosInClipSpace);

        u32 ww = pWindow->Width;
        u32 wh = pWindow->Height;
#if 1
        v2 screen = v2_new(
            objPosToClipSpace.X * ww / 2.0f + ww/2.0f,
            objPosToClipSpace.Y * wh / 2.0f + wh/2.0f);
#else
        v2 screen = v2_new();
#endif
        //v2_print(screen);
    }

    const i32 gizmoPointerLength = 250;

    v3 start = v3_addz(objPosInClipSpace, -objPosInClipSpace.Z + 1.0f);
    v3 end = v3_addx(start, gizmoPointerLength);

    pEditor->Callbacks.ScreenSubmitLine(start, end, 10.1, v4_new(0, 0, 1, 1));
    //note: for testing
    //pEditor->Callbacks.ScreenSubmitLine(v3_new(100,100,1), v3_new(250, 250, 1), 100.1, v4_new(0, 0, 1, 1));
    pEditor->Callbacks.ScreenFlush();
#endif

}

void
simple_editor_on_update(f32 timestep)
{
    editor_viewport_on_update(&gEditorViewport, timestep);
}

void
simple_editor_on_after_update()
{
    SimpleEditor* pEditor = &gEditor;
    _simple_editor_draw_selected(pEditor);
}

i32
_check_if_entity_collide(CameraComponent* pCamera, ModelAabb aabb, m4 modelTransform)
{
    i32 isCollide = 0;

    SimpleWindow* pWindow = application_get_window();

    v2 screenSize = v2_new(pWindow->Width, pWindow->Height);
    v2 mouse = input_get_cursor_position_v2();

    v3 rayOrigin;
    v3 rayDir;
    simple_obb_helper_ss_to_ray(mouse, screenSize, pCamera->Base.View, pCamera->Base.Projection, &rayOrigin, &rayDir);

    v3 mn = aabb.Min;
    v3 mx = aabb.Max;

    f32 intersectionDistance;
    isCollide = simple_obb_helper_is_collide(
        rayOrigin,
        rayDir,
        mn, mx, modelTransform,
        &intersectionDistance);

    return isCollide;
}

void
simple_editor_on_event(Event* pEvent)
{
    editor_viewport_on_event(&gEditorViewport, pEvent);

    // note: check selected
    i32 isMousePressed = (pEvent->Category == EventCategory_Mouse) && (pEvent->Type == EventType_MouseButtonPressed) && (((MouseButtonEvent*)pEvent)->MouseCode == MouseButtonType_Left);

    if (isMousePressed)
    {
        GINFO("MousePressed!\n");
        EcsQuery query = ecs_get_query(TransformComponent, StaticModelComponent);

        i64 entityId = -1;
        f32 nearZ = +99999.0f;

        i64 i, count = array_count(query.Entities);
        for (i = 0; i < count; ++i)
        {
            EntityRecord entityRecord = query.Entities[i];
            TransformComponent* pTransform = GetComponent(entityRecord.Id, TransformComponent);
            StaticModelComponent* pModel = GetComponent(entityRecord.Id, StaticModelComponent);

            i32 isCollide = _check_if_entity_collide(
                (CameraComponent*) gEditorViewport.pCamera,
                pModel->Model.Aabb,
                pTransform->Matrix);

            if (!isCollide)
                continue;

            // note: check z
            if (pTransform->Translation.Z < nearZ)
            {
                nearZ = pTransform->Translation.Z;
                entityId = entityRecord.Id;
                break;
            }

        }

        ((SimpleScene*)gEditor.pScene)->SelectedEntity = entityId;
    }
}

void
simple_editor_on_destroy()
{
}

void
simple_editor_on_ui()
{

}

void
simple_editor_register_layer(SimpleEditorSetting* pSetting)
{
    vguard_not_null(pSetting->pScene);
    vguard_not_null(pSetting->Callbacks.SubmitLine);
    vguard_not_null(pSetting->Callbacks.Flush);
    vguard_not_null(pSetting->Callbacks.ScreenSubmitLine);
    vguard_not_null(pSetting->Callbacks.ScreenFlush);
    vguard_not_null(pSetting->Callbacks.Get2dCamera);

    gEditor.pScene = pSetting->pScene;
    gEditor.Callbacks = pSetting->Callbacks;

    Layer layer = {
        .Name = "Editor Layer",
        .OnAttach = simple_editor_on_attach,
        .OnAttachFinished = simple_editor_on_attach_finished,
        .OnUpdate = simple_editor_on_update,
        .OnAfterUpdate = simple_editor_on_after_update,
        .OnEvent = simple_editor_on_event,
        .OnDestoy = simple_editor_on_destroy,
        .OnUIRender = simple_editor_on_ui
    };

    application_push_layer(layer);
}

 SimpleEditor*
simple_editor_get()
{
    return &gEditor;
}
