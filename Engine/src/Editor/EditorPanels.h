#ifndef EDITOR_PANELS_H
#define EDITOR_PANELS_H

struct CameraComponent;
struct EditorViewport;

void editor_panel_camera(struct CameraComponent* gCameraComponent, struct EditorViewport* pEditorViewport);

#endif // EDITOR_PANELS_H
