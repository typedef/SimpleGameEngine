#include "EditorViewport.h"

#include <Event/Event.h>
#include <EntitySystem/Components/Base/CameraComponent.h>
#include <InputSystem/SimpleInput.h>
#include <InputSystem/KeyCodes.h>
#include <Core/Types.h>
#include <Math/SimpleMath.h>
#include <Core/SimpleStandardLibrary.h>

#pragma clang diagnostic ignored "-Wswitch"

void
editor_viewport_create(struct EditorViewport* pViewport, SimpleEditorViewportSetting set)
{
    pViewport->IsMovementEnabled = set.IsCameraMovementEnabled;
    pViewport->OnUpdate = set.OnCameraUpdate;
}

void
editor_viewport_set_camera(EditorViewport* pViewport, CameraComponent* pCamera)
{
    pViewport->pCamera = pCamera;
}

void
editor_viewport_activate(EditorViewport* pViewport)
{
    pViewport->IsActive = 1;
    input_disable_cursor();
}

void
editor_viewport_disable(EditorViewport* pViewport)
{
    pViewport->IsActive = 0;
}

void
editor_viewport_switch(EditorViewport* pViewport)
{
    pViewport->IsActive = !pViewport->IsActive;
    if (pViewport->IsActive)
    {
        input_disable_cursor();
    }
    else
    {
        input_enable_cursor();
    }

    CameraComponent* pCamera = pViewport->pCamera;
    CameraComponentSettings* set = &pCamera->Settings;
    f64 x, y;
    input_get_cursor_position(&x, &y);
    set->PrevX = x;
    set->PrevY = y;
}

void
editor_viewport_enable_movement(EditorViewport* pViewport)
{
    pViewport->IsMovementEnabled = 1;
}

void
editor_viewport_disable_movement(EditorViewport* pViewport)
{
    pViewport->IsMovementEnabled = 0;
}

void
_editor_viewport_camera_update(EditorViewport* pViewport, struct CameraComponent* pCamera)
{
    pCamera->IsCameraMoved = 1;

    camera_component_update(pCamera);

    if (pViewport->OnUpdate)
        pViewport->OnUpdate(pCamera);
}

void
editor_viewport_camera_update_fov(EditorViewport* pViewport, f32 fovDegree)
{
    CameraComponent* pCamera = pViewport->pCamera;

    pCamera->Perspective.Fov = rad(fovDegree);
    m4 newProj = perspective(pCamera->Perspective.Near,  pCamera->Perspective.Far, pCamera->Perspective.AspectRatio, pCamera->Perspective.Fov);
    camera_update_projection((Camera*)pCamera, newProj);

    _editor_viewport_camera_update(pViewport, pCamera);
}

static i32
_editor_camera_update(EditorViewport* pViewport, f32 timestep)
{
    CameraComponent* pCamera = (CameraComponent*) pViewport->pCamera;
    f32 multiplier = 1.0f;

    if (input_is_key_pressed(KeyType_LeftShift))
    {
        multiplier = pCamera->Spectator.FasterMultiplier;
    }
    else if (input_is_key_pressed(KeyType_LeftAlt))
    {
        multiplier = pCamera->Spectator.SlowerMultiplier;
    }

    if (input_is_key_pressed(KeyType_W))
    {
        camera_component_move_speed(pCamera, CameraMoveDirectionType_Forward, timestep, pCamera->Spectator.Speed * multiplier);

    }
    else if (input_is_key_pressed(KeyType_S))
    {
        camera_component_move_speed(pCamera, CameraMoveDirectionType_Backward, timestep, pCamera->Spectator.Speed * multiplier);
    }

    if (input_is_key_pressed(KeyType_A))
    {
        camera_component_move_speed(pCamera, CameraMoveDirectionType_Left, timestep, pCamera->Spectator.Speed * multiplier);
    }
    else if (input_is_key_pressed(KeyType_D))
    {
        camera_component_move_speed(pCamera, CameraMoveDirectionType_Right, timestep, pCamera->Spectator.Speed * multiplier);
    }

    if (input_is_key_pressed(KeyType_Q))
    {
        camera_component_move_speed(pCamera, CameraMoveDirectionType_Down, timestep, pCamera->Spectator.Speed * multiplier);
    }
    else if (input_is_key_pressed(KeyType_E))
    {
        camera_component_move_speed(pCamera, CameraMoveDirectionType_Up, timestep, pCamera->Spectator.Speed * multiplier);
    }

    CameraComponentSettings* set = &pCamera->Settings;
    f64 x, y;
    input_get_cursor_position(&x, &y);
    if (!f32_equal_epsilon(set->PrevX, x, 0.1f) || !f32_equal_epsilon(set->PrevY, y, 0.1f))
    {
        //GINFO("Move Rotate: Prev { %f %f } New { %f %f }\n", set->PrevX, set->PrevY, x, y);
        camera_component_rotate(pCamera, x, y, timestep);
    }

    if (pCamera->IsCameraMoved)
    {
        //GINFO("Move Camera update\n");
        camera_component_update(pCamera);

        if (pViewport->OnUpdate)
            pViewport->OnUpdate(pCamera);

        return 1;
    }

    return 0;
}

i32
editor_viewport_on_update(EditorViewport* pViewport, f32 timestep)
{
    if (!pViewport->IsActive)
        return 0;

    i32 cameraUpdated = _editor_camera_update(pViewport, timestep);

    i32 isUpdated = cameraUpdated || 1 /*set here other flags*/;
    return 0;
}

void
editor_viewport_on_event(EditorViewport* pViewport, Event* event)
{
    switch (event->Category)
    {

    case EventCategory_Key:
    {
        KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
        if (keyEvent->Modificator == ModType_Control)
        {
            /* if (keyEvent->KeyCode == GizmoRotationKeyBinding) */
            /* { */
            /*	OperationType = ROTATE; */
            /*	event->IsHandled = 1; */
            /* } */
            /* else if (keyEvent->KeyCode == GizmoTranslationKeyBinding) */
            /* { */
            /*	OperationType = TRANSLATE; */
            /*	event->IsHandled = 1; */
            /* } */
            /* else if (keyEvent->KeyCode == GizmoScaleKeyBinding) */
            /* { */
            /*	OperationType = SCALE; */
            /*	event->IsHandled = 1; */
            /* } */
        }
        else if (keyEvent->KeyCode == KeyType_Space && keyEvent->Action == ActionType_Press)
        {
            editor_viewport_switch(pViewport);
        }

        break;
    }

    case EventCategory_Window:
    {
        if (event->Type == EventType_WindowResized)
        {
            WindowResizedEvent* wrEvent = (WindowResizedEvent*) event;
            //framebuffer_recreate(wrEvent->Width, wrEvent->Height);
            //renderer_set_viewport(wrEvent->Width, wrEvent->Height);
        }
        break;
    }

    }
}
