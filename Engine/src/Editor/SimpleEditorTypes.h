#ifndef SIMPLE_EDITOR_TYPES_H
#define SIMPLE_EDITOR_TYPES_H

#include <Core/Types.h>

typedef void (*SubmitLineDelegate)(v3 start, v3 end, f32 thickness, v4 color);
typedef void (*FlushDelegate)();
typedef void (*SetVisibleRectDelegate)(v2 min, v2 max);
typedef void* (*Get2dCamera)();

typedef struct SimpleEditorDrawCallbacks
{
    SubmitLineDelegate SubmitLine;
    FlushDelegate Flush;

    // note: screen renderer 2
    SubmitLineDelegate ScreenSubmitLine;
    FlushDelegate ScreenFlush;
    SetVisibleRectDelegate SetVisibleRect;
    Get2dCamera Get2dCamera;
} SimpleEditorDrawCallbacks;

#endif // SIMPLE_EDITOR_TYPES_H
