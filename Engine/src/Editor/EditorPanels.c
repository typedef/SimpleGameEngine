#include "EditorPanels.h"

#include <Application/Application.h>
#include <Core/SimpleStandardLibrary.h>
#include <UI/Sui.h>
#include <EntitySystem/Components/Base/CameraComponent.h>
#include <EntitySystem/Components/Base/TransformComponent.h>
#include <Editor/EditorViewport.h>
#include <Math/SimpleMath.h>

void
editor_panel_camera(struct CameraComponent* pCameraComponent, struct EditorViewport* pEditorViewport)
{
    static WideString ltr = {.Buffer = L"Позиция", .Length = 7};
    if (sui_panel_begin_l(&ltr, SuiPanelFlags_WithOutHeader))
    {
        Application* pApp = application_get();
        static f32 seconds = 0.0f;
        static f32 timestep = 1.0f;
        static SimpleTimerData timerData = { 0.0f, 0.15f };

        if (simple_timer_interval(&timerData, pApp->Stats.Timestep))
        {
            timestep = pApp->Stats.Timestep;
            simple_timer_reset(&timerData);
        }
        sui_text("Fps: %d, FrameTime: %f", (i32) (1 / timestep), 1000 * timestep);

        v3 pos = pCameraComponent->Settings.Position;
        sui_text("Камера: %0.2f %0.2f %0.2f", pos.X, pos.Y, pos.Z);

        f32 fov = deg(pCameraComponent->Perspective.Fov);
        if (sui_slider_f32("Fov", &fov, v2_new(30, 120)) == SuiState_Changed)
        {
            editor_viewport_camera_update_fov(pEditorViewport, fov);
        }

    }
    sui_panel_end();
}
