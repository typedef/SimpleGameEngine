#ifndef SIMPLE_EDITOR_H
#define SIMPLE_EDITOR_H

#include "SimpleEditorTypes.h"

struct SimpleScene;

typedef struct SimpleEditorSetting
{
    struct SimpleScene* pScene;
    SimpleEditorDrawCallbacks Callbacks;
} SimpleEditorSetting;

typedef struct SimpleEditor
{
    struct SimpleScene* pScene;
    SimpleEditorDrawCallbacks Callbacks;
} SimpleEditor;

void simple_editor_register_layer(SimpleEditorSetting* pSetting);
SimpleEditor* simple_editor_get();

#endif // SIMPLE_EDITOR_H
