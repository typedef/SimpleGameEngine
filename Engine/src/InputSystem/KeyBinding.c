#include "KeyBinding.h"

#include <InputSystem/KeyCodes.h>
#include <Core/SimpleStandardLibrary.h>

#pragma clang diagnostic ignored "-Wswitch"


static KeyBinding* RegisteredKeyBindings = NULL;
static BindingElement* InputStream = NULL;
static i32 BindingElementWasAdded = 0;

i8
binding_element_equals(BindingElement a, BindingElement b)
{
    i8 isEqual = a.KeyCode == b.KeyCode /* && a.Action == b.Action */ && a.Modificator && b.Modificator;
    return isEqual;
}

const char*
key_type_to_string(KeyType keyType)
{

    switch (keyType)
    {

    case KeyType_0: return "0";
    case KeyType_1: return "1";
    case KeyType_2: return "2";
    case KeyType_3: return "3";
    case KeyType_4: return "4";
    case KeyType_5: return "5";
    case KeyType_6: return "6";
    case KeyType_7: return "7";
    case KeyType_8: return "8";
    case KeyType_9: return "9";

    case KeyType_Semicolon: return ";";
    case KeyType_Equal    : return "=";

    case KeyType_A: return "A";
    case KeyType_B: return "B";
    case KeyType_C: return "C";
    case KeyType_D: return "D";
    case KeyType_E: return "E";
    case KeyType_F: return "F";
    case KeyType_G: return "G";
    case KeyType_H: return "H";
    case KeyType_I: return "I";
    case KeyType_J: return "J";
    case KeyType_K: return "K";
    case KeyType_L: return "L";
    case KeyType_M: return "M";
    case KeyType_N: return "N";
    case KeyType_O: return "O";
    case KeyType_P: return "P";
    case KeyType_Q: return "Q";
    case KeyType_R: return "R";
    case KeyType_S: return "S";
    case KeyType_T: return "T";
    case KeyType_U: return "U";
    case KeyType_V: return "V";
    case KeyType_W: return "W";
    case KeyType_X: return "X";
    case KeyType_Y: return "Y";
    case KeyType_Z: return "Z";

    case KeyType_LeftControl: return "Ctrl";
    case KeyType_LeftAlt    : return "Alt";

    }

    //GWARNING("Unknown keytype: %d\n", keyType);
    return "Unknown";
}

void
binding_element_format(char buf[64], BindingElement a)
{
    const char* modTxt = " ";

    if (a.Modificator == ModType_Control)
    {
        modTxt = "Ctrl";
    }
    else if (a.Modificator == ModType_Alt)
    {
        modTxt = "Alt";
    }
    else if (a.Modificator == ModType_Shift)
    {
        modTxt = "Shift";
    }

    string_format(buf, "<%s%s%s>",
                  modTxt,
                  modTxt[0] != ' ' ? "+" : " ",
                  key_type_to_string((KeyType)a.KeyCode));

}

void
binding_element_print(BindingElement a)
{
    char buf[64];
    binding_element_format(buf, a);
    printf("%s", buf);
}

static i32
minor_binding_equals(MinorBinding a, MinorBinding b)
{
    i32 i,
        aCount = array_count(a.Binds),
        bCount = array_count(b.Binds);

    if (aCount != bCount)
        return 0;

    for (i = 0; i < aCount; ++i)
    {
        BindingElement aBind = a.Binds[i];
        BindingElement bBind = b.Binds[i];

        if (!binding_element_equals(aBind, bBind))
            return 0;
    }

    return 1;
}

static i32
minor_binding_valid(MinorBinding minor)
{
    vassert(wide_string_is_valid(minor.Description));
    vassert_not_null(minor.Function);
    return 1;
}

MinorBinding
minor_binding_create(WideString description, KeyBindingCallbackDelegate function, BindingElement* binds)
{
    vassert(wide_string_is_valid(description));
    vassert_not_null(function);

    MinorBinding minor = {
        .Description = description,
        .Function = function,
        .Binds = binds,
    };

    return minor;
}

i8
key_binding_exist(KeyBinding aKeyBinding, KeyBindingCreationRecord record)
{
    if (!binding_element_equals(aKeyBinding.Major, record.Major))
    {
        return 0;
    }

    i32 i,
        aCount = array_count(aKeyBinding.Minors);

    for (i = 0; i < aCount; ++i)
    {
        MinorBinding minor = aKeyBinding.Minors[i];

        if (minor_binding_equals(minor, record.Minor))
        {
            return 1;
        }
    }

    return 0;
}

i8
key_binding_is_registered(KeyBindingCreationRecord newBind)
{
    i32 i, count = array_count(RegisteredKeyBindings);
    for (i = 0; i < count; ++i)
    {
        KeyBinding key = RegisteredKeyBindings[i];

        if (binding_element_equals(key.Major, newBind.Major))
        {
            array_foreach(
                key.Minors,
                if (minor_binding_equals(item, newBind.Minor))
                {
                    GWARNING("KeyBinding %ls was resitered as %ls\n",
                             newBind.Minor.Description.Buffer,
                             item.Description.Buffer);
                    return 1;
                });

            return 0;
        }
    }

    return 0;
}

void
key_binding_register(KeyBindingCreationRecord newBinding)
{
    minor_binding_valid(newBinding.Minor);

    if (key_binding_is_registered(newBinding))
    {
        return;
    }

    // not like that
    i32 i, count = array_count(RegisteredKeyBindings);
    for (i = 0; i < count; ++i)
    {
        KeyBinding* regBinding = &RegisteredKeyBindings[i];
        if (binding_element_equals(regBinding->Major, newBinding.Major))
        {
            array_push(regBinding->Minors, newBinding.Minor);
            return;
        }
    }

    MinorBinding* minors = NULL;
    array_push(minors, newBinding.Minor);

    KeyBinding addBinding = {
        .Major = newBinding.Major,
        .Minors = minors
    };

    array_push(RegisteredKeyBindings, addBinding);
}

void
key_bnding_add(BindingElement bindingElement)
{
    char buf[64];
    binding_element_format(buf, bindingElement);
    // GINFO("Add binding element %s!\n", buf);

    array_push(InputStream, bindingElement);
    BindingElementWasAdded = 1;
}

BindingElement*
key_binding_get()
{
    return InputStream;
}

void
key_binding_process()
{
    i32 inputStreamCount = array_count(InputStream);
    if (inputStreamCount <= 0 || !BindingElementWasAdded)
        return;

    BindingElementWasAdded = 0;

    BindingElement bindingElement = InputStream[0];

    // GWARNING("Count: %d\n", inputStreamCount);
    /* array_foreach(InputStream, */
    /*		  GWARNING("IStream[%d]", i); */
    /*		  binding_element_print(item); */
    /*		  printf("\n");); */

    for (i32 i = 0; i < inputStreamCount; ++i)
    {
        i32 matches = 0;

        for (i32 r = 0; r < array_count(RegisteredKeyBindings); ++r)
        {
            KeyBinding keyBinding = RegisteredKeyBindings[r];
            if (!binding_element_equals(bindingElement, keyBinding.Major))
            {
                continue;
            }

            ++matches;

            for (i32 m = 0; m < array_count(keyBinding.Minors); ++m)
            {
                MinorBinding minor = keyBinding.Minors[m];

                if (minor.Binds == NULL)
                {
                    // GINFO("Match keyBinding.Minors == NULL!\n");
                    array_pop_begin(InputStream);
                    minor.Function();
                    return;
                }

                if (i >= (inputStreamCount - 1))
                    continue;

                // GINFO("Match keyBinding.Minor.Binds != NULL!\n");

                BindingElement other = InputStream[1];
                i32 minorEqual = 1;

                for (i32 m = 0;
                     m < array_count(minor.Binds);
                     ++m)
                {
                    // what if we have IS: C-x C-o, Binds: C-x C-o C-o
                    // TODO(typedef): additional check for this situation
                    BindingElement item = minor.Binds[m];

                    if (!binding_element_equals(other, item))
                    {
                        minorEqual = 0;
                    }
                }

                if (minorEqual)
                {
                    // GERROR("Match!\n");
                    array_pop_begin(InputStream);
                    array_pop_begin(InputStream);
                    minor.Function();
                    return;
                }

            }
        }

        if (matches == 0)
        {
            array_pop_begin(InputStream);
        }
        if (array_count(InputStream) > 1)
        {
            array_pop_begin(InputStream);
            array_pop_begin(InputStream);
        }

    }

}
