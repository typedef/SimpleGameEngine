#!/bin/bash

cd ../../../

app="bin/Debug-linux-x86_64/AutoTest/AutoTest"
valgrind --leak-check=full --log-file="at-valgrind-result.txt" $app

#"bin/Debug-linux-x86_64/VulkanSandbox/VulkanSandbox"
#--appworkingdir ""
#--wireframe-overlay
