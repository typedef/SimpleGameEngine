#!/bin/bash

echo "Cleaning dependencies .."

cd ../..

dependenciesDirs[0]="cimgui"
dependenciesDirs[1]="glad"
dependenciesDirs[2]="GLFW"
dependenciesDirs[3]="cgltf"
dependenciesDirs[4]="stb"
dependenciesDirs[5]="MiniAudio"
dependenciesDirs[6]="assimp"

function file_delete_if_exist() {
    if [ -e $1 ]; then
	rm $1
    fi
}

function dir_delete_if_exist() {
    if [[ -d $1 ]]; then
	rm -rf $1
    fi
}

for dependencyDir in ${dependenciesDirs[*]}
do
    dir="Dependencies/${dependencyDir}/"
    dir_delete_if_exist "$dir/bin"
    file_delete_if_exist "$dir/${dependencyDir}.mk"
    file_delete_if_exist "$dir/${dependencyDir}.project"
    file_delete_if_exist "$dir/${dependencyDir}.txt"
    file_delete_if_exist "$dir/compile_flags.txt"
    file_delete_if_exist "$dir/Makefile"
done
