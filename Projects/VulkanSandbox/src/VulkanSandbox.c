#include "VulkanSandbox.h"

#include <Core/SimpleStandardLibrary.h>
#include <UI/Sui.h>
#include <UI/SuiDemo.h>
#include <AssetManager/AssetManager.h>
#include <Editor/EditorViewport.h>
#include <Graphics/Vulkan/VsrCompositor.h>
#include <Graphics/Vulkan/Vsr3dCompositor.h>
#include <Graphics/Vulkan/Vsr3dSky.h>
#include <Graphics/Vulkan/Vsr2dCompositor.h>
#include <Graphics/SimpleWindow.h>
#include <InputSystem/KeyCodes.h>
#include <Model/ModelBase.h>
#include <Math/SimpleMath.h>
#include <Model/GeneratedModel.h>
#include <UI/SuifBackend.h>

#include "Graphics/Vulkan/Vsr2d.h"
#include "InputSystem/SimpleInput.h"
#include "TempScene.h"
#include "Utils/SimpleJson.h"
#include "Core/Types.h"
#include <Math/SimpleObbHelper.h>

#include <EntitySystem/SimpleScene.h>

#pragma clang diagnostic ignored "-Wswitch"


static SimpleWindow* CurrentWindow = NULL;
//NOTE(typedef): Temporary
static EcsWorld* World = NULL;
static Vsr3dS gRender = {};
static Vsr3dCompositor* gpCompositor = NULL;
static Vsr2dCompositor* g2dCompositor = NULL;

// NOTE() Demo   related
//LightDataUbo gLightDataUbo;
static CameraComponent gCameraComponent;
static StaticModel gRetroPcModel;
static StaticModel gCube = {};
static StaticModel gGizmoPointer = {};
static StaticModel gRedCube = {};
static StaticModel gSheenChairModel = {};
static StaticModel gSkinnedModel = {};
static StaticModel gCubeModel;
static DynamicModel gBrainModel = {};
static DynamicModel gFoxModel = {};
static DynamicModel gSimpleMen = {};

static Vsr3dSky gVsrSky = {};

static EditorViewport gEditorViewport = {};

// NOTE: NOT TRASH
// NOTE: NOT TRASH
// NOTE: NOT TRASH

static SimpleScene gCurrentScene = {};

// NOTE: NOT TRASH
// NOTE: NOT TRASH
// NOTE: NOT TRASH


void _create_light_data();
static void _create_camera_component();

void _camera_updated(CameraComponent* pCamera);
void _on_scene_changed(struct CameraComponent* pCamera);

#include <stdlib.h>
#include <stdarg.h>
#include <wchar.h>

#include <Core/SimpleStandardLibrary.h>

void
vulkan_sandbox_on_attach()
{
    CurrentWindow = application_get_window();

    //editor_viewport_activate();

    _create_light_data();
    _create_camera_component();
    GINFO("\n\n\nMAX VERTEX BINDIN: %d\n\n\n", vsa_get_max_vertex_binding_in_buffer());

    Vsr3dSSetting vsr3dsSet = {
        .MaxVertices = I32M(1,000,000),
        .MaxInstances = 200,
        .MaxMaterialsCount = 200,
        .MaxTexture = 200,
    };
    Vsr3dDSetting vsr3ddSet = {
        .MaxVertices = I32T(100,000),
        .MaxInstances = 20,
        .MaxMaterialsCount = 20,
        .MaxTexture = 20,
    };
    Vsr3dCompositor localComp = {};
    vsr_3d_compositor_create(&localComp, &vsr3dsSet, &vsr3ddSet, &gCameraComponent.Base);
    gpCompositor = vsr_compositor_register("3d Renderer", &localComp, sizeof(Vsr3dCompositor), (OnVsrDestroy) vsr_3d_compositor_destroy);

    g2dCompositor = vsr_compositor_get("Compositor2d");
    Vsr2dSettings vsr2dSet = {
        .MaxVerticesCount = I32T(55, 000),
        .MaxInstancesCount = 7000,
        .MaxTexturesCount = 100,
        .pName = "Debug Renderer",
        .Font = asset_manager_load_font(resource_font("NotoSans.ttf")),
        .pCamera = &gCameraComponent.Base
    };
    vsr_2d_compositor_create_world(g2dCompositor, &vsr2dSet);
    VsrVisibleRect vsrVisibleRect = {
        .Min = v2_new(0, 0),
        .Max = v2_new(2000, 2000),
    };
    vsr_2d_set_visible_rect(&g2dCompositor->WorldRender, vsrVisibleRect);

    gCube = static_model_cube_create();

    gGizmoPointer = asset_manager_load_model(
        resource_model("GizmoPointer/Gizmo.gltf"));

    MeshMaterial redMat = {
        .Name = "Red Material",
        .IsMetallicExist = 1,
        .Metallic = (PbrMetallic) {
            .MetallicFactor = 0,
            .RoughnessFactor = 0,
            .BaseColor = v4_new(1, 0, 0, 1),
            .BaseColorTexture = 0,
            .MetallicRoughnessTexture = 0
        }
    };
    i64 matId = asset_manager_load_material_id(&redMat);
    StaticModel redCube = static_model_cube_create();
    redCube.aMeshes[0].MaterialId = matId;
    redCube.Base.Name = wide_string(L"Красный куб");

#if 1
    gRetroPcModel = asset_manager_load_model(asset_model("Retro computer/Retro computer(Low only).gltf"));

#endif

    gSimpleMen = asset_manager_load_dynamic_model(asset_model("SimpleMen/SimpleMen.gltf"));

    srand(time(NULL));
}

static v3 gRetroPcPos = {1, 1, 1};
static v3 gGizmoPointerPos = {-1, -3, 0};
static v3 gRay = {};

void
vulkan_sandbox_on_update(f32 timestep)
{
    editor_viewport_on_update(&gEditorViewport, timestep);

    m4 transform = m4_translate_identity(gRetroPcPos);

    // TODO: Write this as a function wo render calls
    // DOCS: Draw Aabb
    {
        v3 min = gRetroPcModel.Aabb.Min;
        //min = v3_add(gRetroPcModel.Aabb.Min, v3_new(-0.05f, 0.0f, 0.0f));
        v3 max = gRetroPcModel.Aabb.Max;

        min = m4_mul_v3(transform, min);
        max = m4_mul_v3(transform, max);

        typedef struct LinePos
        {
            v3 P0;
            v3 P1;
        } LinePos;

        LinePos poss[] = {
            [0] = (LinePos) { .P0 = min, .P1 = v3_new(max.X, min.Y, min.Z) },
            [1] = (LinePos) { .P0 = min, .P1 = v3_new(min.X, max.Y, min.Z) },
            [2] = (LinePos) { .P0 = min, .P1 = v3_new(min.X, min.Y, max.Z) },

            [3] = (LinePos) { .P0 = max, .P1 = v3_new(min.X, max.Y, max.Z) },
            [4] = (LinePos) { .P0 = max, .P1 = v3_new(max.X, min.Y, max.Z) },
            [5] = (LinePos) { .P0 = max, .P1 = v3_new(max.X, max.Y, min.Z) },

            [6] = (LinePos) { .P0 = v3_new(min.X, min.Y, max.Z), .P1 = v3_new(max.X, min.Y, max.Z) },
            [7] = (LinePos) { .P0 = v3_new(min.X, min.Y, max.Z), .P1 = v3_new(min.X, max.Y, max.Z) },
            [8] = (LinePos) { .P0 = v3_new(min.X, max.Y, min.Z), .P1 = v3_new(min.X, max.Y, max.Z) },

            [9] = (LinePos) { .P0 = v3_new(max.X, min.Y, max.Z), .P1 = v3_new(max.X, min.Y, min.Z) },
            [10] = (LinePos) { .P0 = v3_new(max.X, max.Y, min.Z), .P1 = v3_new(min.X, max.Y, min.Z) },
            [11] = (LinePos) { .P0 = v3_new(max.X, max.Y, min.Z), .P1 = v3_new(max.X, min.Y, min.Z) },
        };

        i8 minAxisOnly = 0;
        i8 maxAxisOnly = 0;

        i32 startL = 0;
        i32 endL = ARRAY_COUNT(poss);
        if (minAxisOnly)
        {
            endL = 3;
        }
        else if (maxAxisOnly)
        {
            startL = 3;
            endL = 6;
        }

        for (i32 l = startL; l < endL; ++l)
        {
            LinePos linePos = poss[l];
            vsr_2d_compositor_world_submit_line(&g2dCompositor->WorldRender,
                                                linePos.P0,
                                                linePos.P1,
                                                0.01f,
                                                v4_new(1, 0, 0, 1));
        }


        vsr_2d_compositor_flush_world(g2dCompositor);
    }

    DO_ONES(
        static VsrStats stats = {};

        vsr_3d_compositor_submit_model(gpCompositor, &gRetroPcModel, transform);
        vsr_3d_compositor_submit_model(gpCompositor, &gRedCube, m4_translate_identity(v3_new(1, -3, 0)));
        vsr_3d_compositor_submit_model(gpCompositor, &gGizmoPointer, m4_translate_identity(gGizmoPointerPos));

        vsr_3d_compositor_submit_dynamic_model(gpCompositor, &gSimpleMen, m4_translate_identity(v3_new(4,2,2)));
        stats = gpCompositor->Render.Base.Stats;
        vsr_3d_compositor_flush(gpCompositor);
        );


}

void
vulkan_sandbox_on_ui_render()
{
    //sui_demo_of_frontend_usage();
    static i32 isVisible = 1;
    if (sui_panel_begin("Позиция", SuiPanelFlags_WithOutHeader))
    {
        Application* pApp = application_get();
        static f32 seconds = 0.0f;
        static f32 timestep = 1.0f;
        static SimpleTimerData timerData = { 0.0f, 0.2f };

        if (simple_timer_interval(&timerData, pApp->Stats.Timestep))
        {
            timestep = pApp->Stats.Timestep;
        }
        sui_text("Fps: %d, FrameTime: %f", (i32) (1 / timestep), 1000 * timestep);

        v3 pos = gCameraComponent.Settings.Position;
        sui_text("Камера: %0.2f %0.2f %0.2f", pos.X, pos.Y, pos.Z);

        f32 fov = deg(gCameraComponent.Perspective.Fov);
        if (sui_slider_f32("Fov", &fov, v2_new(30, 120)) == SuiState_Changed)
        {
            editor_viewport_camera_update_fov(&gEditorViewport, fov);
        }


        v3 min = gRetroPcModel.Aabb.Min;
        v3 max = gRetroPcModel.Aabb.Max;
        sui_text("Aabb: (%0.2f.%0.2f.%0.2f %0.2f.%0.2f.%0.2f)", min.X, min.Y, min.Z, max.X, max.Y, max.Z);

        v2 mp = input_get_cursor_position_v2();
        mp.Y = CurrentWindow->Height - mp.Y;
        sui_text("Mouse Coord (%0.2f,%0.2f)", mp.X, CurrentWindow->Height - mp.Y);

        i32 isCollide = 0;
        if (input_is_mouse_pressed(MouseButtonType_Left))
        {
            v2 screenSize = v2_new(CurrentWindow->Width, CurrentWindow->Height);
            v2 mouse = input_get_cursor_position_v2();

            { // best opt
                v3 rayOrigin;
                v3 rayDir;
                simple_obb_helper_ss_to_ray(mouse, screenSize, gCameraComponent.Base.View, gCameraComponent.Base.Projection, &rayOrigin, &rayDir);

                m4 model = m4_translate_identity(gRetroPcPos);
                v3 mn = gRetroPcModel.Aabb.Min;
                v3 mx = gRetroPcModel.Aabb.Max;

                f32 intersectionDistance;
                isCollide = simple_obb_helper_is_collide(
                    rayOrigin,
                    rayDir,
                    mn, mx, model,
                    &intersectionDistance);
            }
        }

        sui_text("Projected Ray (%0.2f,%0.2f,%0.2f)", gRay.X, gRay.Y, gRay.Z);
        {
            v3 rp = v3_sub(pos, gRay);
            sui_text("Sub (%0.2f,%0.2f,%0.2f)", rp.X, rp.Y, rp.Z);
        }

        sui_text("Collide %d", isCollide);

        //sui_text("Instance cnt: %ld", array_count(gVsr3dRenderer.PassedRenderables));
    }
    sui_panel_end();

}

void
vulkan_sandbox_on_event(Event* event)
{
    editor_viewport_on_event(&gEditorViewport, event);

    switch (event->Category)
    {

    case EventCategory_Key:
    {
        if (event->Type != EventType_KeyPressed)
            break;

        KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
        if (keyEvent->KeyCode == KEY_ESCAPE)
        {
            application_close();
            event->IsHandled = 1;
        }

        break;
    }

    case EventCategory_Window:
    {
        if (event->Category == EventCategory_Window)
        {
            switch (event->Type)
            {

            case EventType_WindowShouldBeClosed:
                application_close();
                event->IsHandled = 1;
                break;

            case EventType_WindowResized:
            case EventType_WindowMaximized:
                GINFO("Resized or Maximized!\n");
                vsa_set_window_resized();
                break;
            }
        }

    }

    }
}

void
vulkan_sandbox_on_destroy()
{
}

void
_create_light_data()
{
    /* gLightDataUbo = (LightDataUbo) { */
    /*	.DirectionalLightsCount = 1, */
    /*	.DirectionalLights = { */
    /*	    [0] = { */
    /*		.Base = { */
    /*		    .Ambient = v3_new(0.75, 0.75, 0.75), */
    /*		    .Diffuse = v3_new(0.75, 0.75, 0.75), */
    /*		    .Specular = v3_new(0.75, 0.75, 0.75), */
    /*		}, */
    /*		.Color = v3_new(1, 1, 1), */
    /*		.Direction = v3_new(0, -1.0f, 0) */
    /*	    } */
    /*	}, */
    /*	.PointLightsCount = 0, */
    /*	.FlashLightsCount = 0 */

    /* }; */
}

void
_camera_updated(CameraComponent* pCamera)
{
    vsr_3d_compositor_update_camera_ubo(gpCompositor);
}

static void
_create_camera_component()
{
    CameraComponentCreateInfo createInfo = {
        .IsCameraMoved = 1,

        .Settings = {
            .Type = CameraType_PerspectiveSpectator,
            .Position = v3_new(0.0f, 0.0f, 7.92f),
            .Front = v3_new(0, 0, -1),
            .Up    = v3_new(0, 1,  0),
            .Right = v3_new(1, 0,  0),
            .Yaw = 0,
            .Pitch = 0,
            .PrevX = 0,
            .PrevY = 0,
            .SensitivityHorizontal = 0.35f,
            .SensitivityVertical = 0.45f
        },

        .Perspective = {
            .Near = 0.001f,
            .Far = 50000.0f,
            .AspectRatio = CurrentWindow->AspectRatio,
            // NOTE(typedef): in degree
            .Fov = 75.0f
        },

        .Spectator = {
            .Speed = 5.0f,
            .Zoom = 0
        }
    };

    gCameraComponent =
        camera_component_new_ext(createInfo);
    camera_component_update(&gCameraComponent);

    SimpleEditorViewportSetting editorViewportSet = {
        .IsCameraMovementEnabled = 1,
        .OnCameraUpdate = _camera_updated,
    };
    editor_viewport_create(&gEditorViewport, editorViewportSet);
    editor_viewport_set_camera(&gEditorViewport, &gCameraComponent);
}

void
some_ecs_stuff()
{
    { // Default Ecs Preparation
        //World = ecs_world_create();
        //ecs_world_register_component(World, CameraComponent);

        /* EntityId entity = ecs_world_entity(World, "MainCamera"); */
        /* ecs_world_entity_add_component(World, entity, CameraComponent, cameraComponent); */
    }
    //scene_load(&gAssetManager);

}
