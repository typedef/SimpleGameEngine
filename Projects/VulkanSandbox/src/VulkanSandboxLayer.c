#include <Core/SimpleStandardLibrary.h>
#include "VulkanSandbox.h"
#include <EntryPoint.h>

void
create_user_application(UserApplicationSettings* pSettings)
{
    application_set_locale_utf8();
    logger_init();
    logger_to_terminal();

    ApplicationSettings appSettings = {
#if defined(LINUX_PLATFORM)
        .Width = 2000,
        .Height = 1200,
#elif defined(WINDOWS_PLATFORM)
        .Width = 1000,
        .Height = 800,
#endif
        .IsInitGraphics = 1,
        //BUG(): weird if 1 - bug, 0 - ok
        .IsFrameRatePrinted = 0,
        .IsVsync = 1,
        // BUG:
        /*
          pCreateInfo->imageExtent (2001, 1200), which is outside the bounds returned by vkGetPhysicalDeviceSurfaceCapabilitiesKHR(): currentExtent = (2000,1200), minImageExtent = (2000,1200), maxImageExtent = (2000,1200). The Vulkan spec states: If a VkSwapchainPresentScalingCreateInfoEXT structure was not included in the pNext chain, or it is included and VkSwapchainPresentScalingCreateInfoEXT::scalingBehavior is zero then imageExtent must be between minImageExtent and maxImageExtent, inclusive, where minImageExtent and maxImageExtent are members of the VkSurfaceCapabilitiesKHR structure returned by vkGetPhysicalDeviceSurfaceCapabilitiesKHR for the surface
        */
        .IsDebug = 1,
        .UiFpsSyncRatio = 2,
        .Name = "VulkanSandbox",
        .ArgumentsCount = pSettings->Argc,
        .Arguments = pSettings->Argv,
    };
    application_create(appSettings);

    Layer vulkanSandboxLayer = {0};
    vulkanSandboxLayer.Name = "VulkanSandbox Layer";
    vulkanSandboxLayer.OnAttach = vulkan_sandbox_on_attach;
    vulkanSandboxLayer.OnUpdate = vulkan_sandbox_on_update;
    vulkanSandboxLayer.OnUIRender = vulkan_sandbox_on_ui_render;
    vulkanSandboxLayer.OnEvent = vulkan_sandbox_on_event;
    vulkanSandboxLayer.OnDestoy = vulkan_sandbox_on_destroy;

    application_push_layer(vulkanSandboxLayer);
}
