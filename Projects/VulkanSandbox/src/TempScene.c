#include "TempScene.h"
#include "AssetManager/AssetManager.h"

#include <Core/SimpleStandardLibrary.h>
#include <Model/StaticModel.h>
#include <Math/SimpleMath.h>

/*
  World
  Each chunk size is v2_new(250, 250)
  0: -124 <= x <= 125
     -124 <= y <= 125
  1: -250 <= x <= -125
     -124 <= y <= 125



      234
      105
      876









*/

#if 0

#include <time.h>
#include <stdlib.h>

void
_generate_inside_box(DemoModelCollection* pModelCollection, SimpleChunk* pChunks, v2 boundaryX, v2 boundaryY, f32 step)
{
    srand(time(NULL));

    f32 sy = boundaryY.V[0];
    f32 ey = boundaryY.V[1];
    f32 sx = boundaryX.V[0];
    f32 ex = boundaryX.V[1];

    for (f32 y = sy; y < ey; y += step)
    {
        for (f32 x = sx; x < ex; x += step)
        {
            i64 ind = Min((rand() % pModelCollection->Count), pModelCollection->Count);
            i64 modelId = pModelCollection->Ids[ind];

            StaticModel* pModel = asset_manager_get(AssetType_StaticModel, modelId);

            m4 t = m4_translate_identity(v3_new(x, 0, y));
            /* //array_push(pChunks[0].pStaticTansforms, t); */
            /* for (i32 i = 0; i < array_count(pModel->Meshes); ++i) */
            /* { */
            /* } */
            array_push(pChunks[0].pStaticTansforms, t);
            array_push(pChunks[0].pModelIds, modelId);

            /*
              struct ModelToDraw
              {
              i64 ModelId;
              m4 Transform;
              };

            */
        }
    }

    GINFO("Chunk info: %ld \n", array_count(pChunks[0].pStaticTansforms));
}

void
prepare_scene(DemoModelCollection* pModelCollection)
{
    static SimpleChunk chunk = {
        .Id = 0,
        .pModelIds = NULL,
        .pStaticTansforms = NULL
    };

    f32 v = 78.0f;
    f32 step = 5.0f;
    _generate_inside_box(pModelCollection, pModelCollection->Chunks, v2_new(-v, v), v2_new(-v, v), step);

}
#endif
