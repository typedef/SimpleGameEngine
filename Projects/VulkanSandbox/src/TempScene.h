#ifndef TEMP_SCENE_H
#define TEMP_SCENE_H

#include <Core/Types.h>
#include <Utils/SimpleChunk.h>


typedef struct DemoModelCollection
{
    i64 Ids[5];
    i64 Count;
    SimpleChunk Chunks[1];
} DemoModelCollection;

typedef struct DemoSimpleChunk
{
    SimpleChunk Chunk;
    v2 BoundaryX;
    v2 BoundaryY;
} DemoSimpleChunk;


void prepare_scene(DemoModelCollection* pModelCollection);

#endif // TEMP_SCENE_H
