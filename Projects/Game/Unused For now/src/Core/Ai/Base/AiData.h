#ifndef AI_DATA_H
#define AI_DATA_H

#include <EntitySystem/SimpleEcsTypes.h>

#include "Needs.h"

typedef struct AiData
{
    f32 WorldTimestep;
    Needs* Needs;
    EntityId CurrentEntityId;
    EntityId* RelatedEntityIds;
} AiData;

#endif // AI_DATA_H
