#include "SlvMapTest.h"
#include "Logic/Map/SlvMapItem.h"
#include <Utils/SimpleStandardLibrary.h>
#include <AutoTest/AutoTest.h>

#include <Logic/Map/SlvMap.h>

void
slv_map_create_test()
{
    SlvMap map = slv_map_new((SlvMapSetting) {
            .BlockSize = 100,
            .Height = 1,
            .Width = 1
        });

    i64 i, count = array_count(map.tBlocks);
    for (i = 0; i < count; ++i)
    {
        SlvBlock* aBlocks = map.tBlocks[i];
        i64 j, rowCount = array_count(aBlocks);
        for (j = 0; i < rowCount; ++i)
        {
            SlvBlock block = aBlocks[j];
            if (block.aMapItems != NULL)
            {
                GERROR("Not null for %d.%d\n", i,j);
                AutoTest_Condition(block.aMapItems == NULL);
                return;
            }
            else
            {
                GINFO("block is ok %d.%d\n", i, j);
            }
        }
    }

    SlvMapItem mapItem = {
        .Type = SlvMapItemType_Building,
        .Position = (v3) {7, 10, 0},
    };

    slv_map_set_item(&map, mapItem);

    SlvBlock block = map.tBlocks[0][0];
    SlvMapItem blockItem = block.aMapItems[0];
    v3 bpos = blockItem.Position;
    AutoTest_F32_Equal(bpos.X, 7);
    AutoTest_F32_Equal(bpos.Y, 10);

    slv_map_destroy(map);
}

void
slv_map_test()
{
    AutoTest_File();

    AutoTest_Func(slv_map_create_test());
}
