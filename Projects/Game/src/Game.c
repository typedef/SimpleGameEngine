#include "Game.h"

#include <Engine.h>

#include <Utils/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>
#include <EntitySystem/SimpleEcs.h>

#include <LogicAutoTests/SlvAutoTestRunner.h>

#define GAME_RUN_TEST 1

// Game Demo Related
//static GameWorld sGameWorld;

/*
  Slv - Simple Little Virtual


  SlvWorld - handle people, time.

  SlvCity - instead of 'Map'
  SlvCityItem (|SlvBuilding) - handle individual part of city:
  road, house, market, police station and other
  ?Road
  ?RoadItem (|PhysicalItem) - it can be physical object (PhysicalObject)
  ?Building

  SlvTime - handle date and time of game world.

  SlvPeople - handle internal/external human stuff (mind, body, etc)
  SlvMind - handle thought process and desicion making.
  SlvBody - handle body

*/


/* typedef struct SlvWorld */
/* { */

/* } SlvWorld; */

/* typedef struct SlvWorld */
/* { */

/* } SlvWorld; */


void
game_on_attach()
{

#if GAME_RUN_TEST == 1

    slv_auto_test_runner_process();

#else

#endif


    /* GameWorldSettings set = { */
    /*     .TimeMultiplier = 8, */
    /*     .StartDate = game_time_new_full(8, 27, 30, 31, 03, 1985), */
    /*     .pEcsWorld = ecs_world_create() */
    /* }; */
    /* sGameWorld = game_world_create(set); */

    /* visualizer_create(&sGameWorld); */

    /*
      Human Personality Matrix (HPM)

      Matrix that encodes personality:
      M = ( 0.10984f ... 0.916002f
      ...
      0.723356002f ... 0.200091200f
      )
    */



}

void
game_on_update(f32 timestep)
{
    //scene_update();
    /* game_world_update(&sGameWorld, timestep); */
    /* visualizer_render(timestep); */
}

void
game_on_ui_render()
{
    //visualizer_ui();
}

void
game_on_event(Event* event)
{
    //visualizer_on_event(event);

    switch (event->Category)
    {

    case EventCategory_Key:
    {
        if (event->Type != EventType_KeyPressed)
            break;

        KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
        if (keyEvent->KeyCode == KeyType_Escape)
        {
            application_close();
            event->IsHandled = 1;
        }

        break;
    }

    case EventCategory_Window:
    {
        if (event->Category == EventCategory_Window)
        {
            switch (event->Type)
            {

            case EventType_WindowShouldBeClosed:
                application_close();
                event->IsHandled = 1;
                break;
            }
        }
    }

    default:
        break;

    }

}

void
game_on_destroy()
{
    //game_world_destroy(sGameWorld);
    //visualizer_destroy();
}











void
trash()
{
    #if 0
    static bool mindVisible = true;
    if (igBegin("Mind", &mindVisible, ImGuiWindowFlags_None))
    {
        MindComponent* mind = GetComponentRecordData(MindComponent);
        igText("CurrentEntity: %d", mind->CurrentEntity);

        igText("Needs:");
        igText("Food: %0.3f", mind->Needs.Food);
        igText("Toilet: %0.3f", mind->Needs.Toilet);
        igText("Social: %0.3f", mind->Needs.Social);
        igText("Energy: %0.3f", mind->Needs.Energy);
        igText("Hygiene: %0.3f", mind->Needs.Hygiene);
        igText("Fun: %0.3f", mind->Needs.Fun);
        igText("Instincts: %0.3f", mind->Needs.Instincts);
        igText("Ambitions: %0.3f", mind->Needs.Ambitions);

        igText("[Current Module]");
        igText("Type: %s", module_type_to_string(mind->CurrentModule.Type));
        igText("CurrentActivity: %s", module_type_to_string(mind->CurrentModule.CurrentActivity));

    }
    igEnd();

    static bool timeVisible = true;
    if (igBegin("Time", &timeVisible, ImGuiWindowFlags_None))
    {
        char buf[65];
        game_time_to_string(buf, sGameWorld.CurrentDate);
        igText("TimeMultiplier: %d", sGameWorld.TimeMultiplier);
        igText("Time: %s", buf);
        igText("WorldTimestep: %0.3f ms", sGameWorld.WorldTimestep * 1000);
    }
    igEnd();
#endif

}


/*

  System Oriented Design

update()
{
    ai_system_update():
    for (i32 i = 0; i < minds.Count; ++i)
    {
        minds[i].Update();
    }

    optimization ...
    let playerFrustum = GetPlayerFrustum();
    worldBuildings = world_building_update(playerFrustum);
    assetsTable = assets_table_update(playerFrustum);
    asset_system_set_visibility(worldBuildings, assetsTable, playerFrustrum);

    // Optimized worldBuilding and assets, get only what is visible for player.
    physics_system_update() depends on RigidBodyComponent, SoftBodyComponent;
    animation_system_update() depends on AnimationComponent;
    graphics_system_update() depends on ModelComponent;


}


some_player_code()
{
    let model: Model = GetPlayerComponent<ModelComponent>();
    foreach (let mesh in model.Meshes)
    {
        if (mesh.IsLeg())
        {
            mesh.SetDamagedTexture();
        }
    }
}















 */
