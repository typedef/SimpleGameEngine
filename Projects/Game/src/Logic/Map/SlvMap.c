#include "SlvMap.h"

#include <Core/Ssl.h>

SlvMap
slv_map_new(SlvMapSetting slvMapSetting)
{
    i32 bs = slvMapSetting.BlockSize;
    i32 h = slvMapSetting.Height,
        w = slvMapSetting.Width;

    SlvMap map = {
        .Size = (v2i) {w, h},
        .BlockSize = bs,
        .tBlocks = NULL,
    };

    for (i32 y = 0; y < h; ++y)
    {
        SlvBlock* aBlocks = NULL;
        for (i32 ind = 0; ind < w; ++ind)
        {
            SlvBlock block = {};
            array_push(aBlocks, block);
        }

        array_push(map.tBlocks, aBlocks);
    }

    return map;
}

void
slv_map_destroy(SlvMap map)
{
    array_foreach(map.tBlocks, array_free(item));
    array_free(map.tBlocks);
}

v2
slv_map_size(SlvMap* pMap)
{
    i32 blockSize = pMap->BlockSize;

    return (v2) {
        .X = pMap->Size.X * blockSize,
        .Y = pMap->Size.Y * blockSize
    };
}

void
slv_map_set_item(SlvMap* pMap, SlvMapItem mapItem)
{
    if (!pMap->tBlocks)
    {
        GERROR("tBlocks is null\n");
        return;
    }

    // DOCS: get block x, y
    v3 pos = mapItem.Position;
    i32 blockSize = pMap->BlockSize;
    i32 x = (i32) pos.X, y = (i32) pos.Y;

    /*bs = 100 pos=50, 120, 0*/
    i32 r = y / blockSize;
    i32 c = x / blockSize;

    if (r >= pMap->Size.X || c >= pMap->Size.Y
        ||
        r < 0 || c < 0)
    {
        GERROR("slv_map_set_item mapItem outside the map\n");
        return;
    }

    SlvBlock* pBlock = &pMap->tBlocks[r][c];

    array_push(pBlock->aMapItems, mapItem);
}
