#include <Engine.h>
#include <EntryPoint.h>

#include "Game.h"

void
create_user_application(UserApplicationSettings* pSettings)
{
    ApplicationSettings appSettings = {
#if defined(LINUX_PLATFORM)
        .Width = 2000,
        .Height = 1200,
#elif defined(WINDOWS_PLATFORM)
        .Width = 1000,
        .Height = 800,
#endif
        .Name = "GameDemo",
        //.IsInitGraphics = 1,
        //.IsVsync = 1,
        .IsDebug = 1,
        //.UiFpsSyncRatio = 2
    };
    application_create(appSettings);

    Layer gameLayer = {};
    gameLayer.Name = "Game Demo Layer";
    gameLayer.OnAttach = game_on_attach;
    gameLayer.OnUpdate = game_on_update;
    gameLayer.OnUIRender = game_on_ui_render;
    gameLayer.OnEvent = game_on_event;
    gameLayer.OnDestoy = game_on_destroy;

    application_push_layer(gameLayer);
}
