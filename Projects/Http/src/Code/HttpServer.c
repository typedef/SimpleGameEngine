#include "HttpServer.h"

#include <Utils/SimpleStandardLibrary.h>

typedef enum HttpCode
{
    HttpCode_Success = 200,
    HttpCode_PageNotFound = 400,
    HttpCode_ServerError = 500,
} HttpCode;

typedef enum HttpContentType
{
    HttpContentType_Html = 0,
    HttpContentType_Count,
} HttpContentType;

typedef struct HttpResponse
{
    // HTTP/1.1
    const char* pVersion;
    // 200
    HttpCode Code;
    // OK
    const char* pStatus;
    // Server: *pServer
    const char* pServer;
    HttpContentType ContentType;
    char* pContent;
} HttpResponse;

typedef enum HttpMethod
{
    HttpMethod_None = 0,
    HttpMethod_Get,
    HttpMethod_Head,
    HttpMethod_Post,
} HttpMethod;

const char*
http_method_to_string(HttpMethod httpMethod)
{
    switch (httpMethod)
    {
    case HttpMethod_Get: return "GET";
    case HttpMethod_Head: return "HEAD";
    case HttpMethod_Post: return "POST";

    default: return NULL;
    }
}

typedef enum HttpContants
{
    HttpContants_MaxMethod = 4,
    HttpContants_MaxUriSize = 2048,
} HttpContants;

typedef struct HttpRequest
{
    char pMethod[HttpContants_MaxUriSize];
    char pUri[HttpContants_MaxUriSize];
} HttpRequest;

const char*
http_code_to_string(HttpCode httpCode)
{
    switch (httpCode)
    {
    case HttpCode_Success: return "200";
    case HttpCode_PageNotFound: return "400";
    case HttpCode_ServerError: return "500";
    }

    return "0";
}

const char*
http_code_to_status_string(HttpCode httpCode)
{
    if (httpCode == HttpCode_Success)
        return "OK";
    return "ERROR";
}

const char*
http_content_type_to_string(HttpContentType httpContentType)
{
    switch (httpContentType)
    {
    case HttpContentType_Html: return "text/html; charset=utf-8";

    case HttpContentType_Count:
        break;
    }

    return "text/html; charset=utf-8";
}

char*
http_response_to_string(HttpResponse res)
{
    char* sb = NULL;

    const char* pCode = http_code_to_string(res.Code);
    const char* pStatus = http_code_to_status_string(res.Code);
    const char* pContentType = http_content_type_to_string(res.ContentType);

    i64 contentLength = string_length(res.pContent);

    /*     "HTTP/1.1 200 OK\r\n" */
    string_builder_appendf(sb, "%s %s %s\r\n", res.pVersion, pCode, pStatus);
    /*     "Server: simple-c-server\r\n" */
    string_builder_appendf(sb, "Server: %s\r\n", res.pServer);
    /*     //"Version: HTTP/1.1\r\n" */
    /*     "Content-Type: text/html; charset=utf-8\r\n" */
    string_builder_appendf(sb, "Content-Type: %s\r\n", pContentType);
    /*     "Content-Length: 51\r\n\r\n" */
    string_builder_appendf(sb, "Content-Length: %d\r\n\r\n", contentLength);

    // DOCS: Content
    string_builder_appendf(sb, "%s", res.pContent);

    return sb;
}


void
_http_server_fatal_exit(const char* pMessage, Socket* pSocket)
{
    GERROR("%s: %s\n", pMessage, ip_to_user_string(pSocket->Address.sin_addr.s_addr));
    perror("Reason: ");
    socket_close(pSocket);
}

// todo: remove
void
_http_server_send_line(Socket* pClient, char* pStr, i32 length)
{
    socket_send(pClient, pStr, length);
}

void
http_server_create()
{
}

static char*
_http_server_parse_method(char* pRequest, HttpMethod* pHttpMethod)
{
    char* ptr = pRequest;

    char c = *ptr;
    switch (c)
    {
    case 'G':
        *pHttpMethod = HttpMethod_Get;
        ptr += 3; // GET\s^
        break;

    case 'H':
        *pHttpMethod = HttpMethod_Head;
        ptr += 4; // HEAD\s^
        break;

    case 'P':
        *pHttpMethod = HttpMethod_Post;
        ptr += 4; // POST\s^
        break;

    default:
        break;
    }

    return ptr;
}

typedef struct SimpleString
{
    char* pData;
    u64 Length;
} SimpleString;

SimpleString
simple_string_new(char* pPointer, u64 length)
{
    char* aStr = string_substring_range(pPointer, 0, length);

    SimpleString simpleString = {
        .pData = aStr,
        .Length = length,
    };

    return simpleString;
}

void
simple_string_destroy(SimpleString* pString)
{
    memory_free(pString->pData);
}

i64
simple_string_count_char(SimpleString* pString, char c)
{
    i64 cnt = 0;
    for (i64 i = 0; i < pString->Length; ++i)
    {
        char dc = pString->pData[i];
        if (dc == c)
        {
            ++cnt;
        }
    }

    return cnt;
}

// DOCS: Allocation performed only for array pointer
SimpleString*
simple_string_split_as_view(SimpleString* pString, char splitCharacter)
{
    SimpleString* aStringViews = NULL;

    if (string_index_of(pString->pData, splitCharacter) == -1)
    {
        array_push(aStringViews, *pString);
        return aStringViews;
    }

    i32 len = 0;
    char* ptr = pString->pData;

    for (i32 i = 0; i < pString->Length; ++i)
    {
        char c = pString->pData[i];
        /*
          01234567
          a=1&b=17
                 ^
        */
        if (c == splitCharacter || (i == (pString->Length - 1))) // len != 0
        {

            SimpleString simpleString = {
                .pData = ptr,
                .Length = len,
            };

            array_push(aStringViews, simpleString);

            len = 0;
            ptr = &pString->pData[i + 1];
        }

        ++len;
    }

    return aStringViews;
}

/* DOCS: Part of query string, one concrete pair of key and value */
typedef struct HttpQueryItem
{
    SimpleString Key;
    SimpleString Value;
} HttpQueryItem;

// DOCS: Contains Uri + QueryString as array of HttpQueryItem
typedef struct HttpQuery
{
    SimpleString FullUri;
    SimpleString UriOnly;
    HttpQueryItem* aQueryItems;
} HttpQuery;

static char*
_http_server_uri_parse_single(char* pRequest, SimpleString* pItem, char itemChar)
{
    char* ptr = pRequest;
    char* beg = ptr;

    i32 ind = 0;
    char c = *ptr;
    while (c != itemChar && c != ' ' && c != '\0')
    {
        ++ptr;
        c = *ptr;
    }

    u64 len = (ptr - beg);

    *pItem = (SimpleString) {
        .Length = len,
        .pData = beg,
    };

    if (*ptr == itemChar)
        ++ptr;

    return ptr;
}

static char*
_http_server_parse_uri(char* pRequest, HttpQuery* pHttpQuery)
{
    char* ptr = pRequest;
    char* beg = ptr;

    i64 argsInd = -1;
    char c = *ptr;
    while (c != '\0' && c != ' ')
    {
        if (c == '?')
        {
            argsInd = (u64) (ptr - beg);
        }

        ++ptr;
        c = *ptr;
    }

    u64 length = (u64) (ptr - beg);

    SimpleString fullUri = simple_string_new(beg, length);
    u64 uriOnlyLength = (argsInd != -1) ? argsInd : fullUri.Length;
    SimpleString uriOnly = {
        .pData = fullUri.pData,
        .Length = uriOnlyLength,
    };
    HttpQueryItem* aQueryItems = NULL;

    if (argsInd != -1 && *(ptr + 1) != ' ')
    {
        // DOCS: Next char after '?'
        char* query = beg + argsInd + 1;
        ptr = query;

        i32 ind = 0;
        char oc = *ptr;
        while (oc != ' ' && oc != '\0' && oc != '\n')
        {
            SimpleString key = {};
            SimpleString val = {};
            ptr = _http_server_uri_parse_single(ptr, &key, '=');
            ptr = _http_server_uri_parse_single(ptr, &val, '&');
            oc = *ptr;

            HttpQueryItem queryItem = {
                .Key = key,
                .Value = val,
            };

            array_push(aQueryItems, queryItem);
        }

    }

HttpServerParseUriEndLabel:
    *pHttpQuery = (HttpQuery) {
        .FullUri = fullUri,
        .UriOnly = uriOnly,
        .aQueryItems = aQueryItems,
    };

    return ptr;
}

static char*
_http_server_parse_header(char* pRequest)
{
    char* ptr = pRequest;

    HttpMethod httpMethod = {0};
    ptr = _http_server_parse_method(ptr, &httpMethod);
    if (httpMethod == 0)
    {
        // handle error
    }
    //printf("%s\n", ptr);

    HttpQuery httpQuery = {};
    ptr = _http_server_parse_uri(ptr+1, &httpQuery);
    printf("Full uri: %.*s\n", (i32)httpQuery.FullUri.Length, httpQuery.FullUri.pData);
    printf("Uri only: %.*s\n", (i32)httpQuery.UriOnly.Length, httpQuery.UriOnly.pData);

    //vguard(array_count(httpQuery.aQueryItems) > 0);
    array_foreach(httpQuery.aQueryItems, printf("Key: %.*s Value: %.*s\n", (i32)item.Key.Length, item.Key.pData, (i32)item.Value.Length, item.Value.pData););

    // DOCS: Free everything for debug only
    simple_string_destroy(&httpQuery.FullUri);
    array_free(httpQuery.aQueryItems);
    // SimpleString Uri;
    // HttpQueryItem* aQueryItems;

    return ptr;
}

void
http_server_start()
{
    GINFO("Http started\n");

    socket_set_debug_mode();

    // DOCS: Constants
    const i32 max_queue_clients = 10;
    const char* http_version = "HTTP/1.1";

    SocketSettings set = (SocketSettings) {
        .Type = SocketType_TCP,
        .IpType = IpType_V4,
        .Address = {
#if 1
            .Ip = ip_as_integer(127, 0, 0, 1),
#else
            .Ip = ip_as_integer(192, 168, 1, 61),
#endif
            .Port = 25080,
        }
    };

    Socket serverSocket = socket_new(set);
    socket_make_async(&serverSocket);

    i32 bindResult = socket_bind(&serverSocket);
    if (!bindResult)
    {
        _http_server_fatal_exit("Failed bind socket to addrs", &serverSocket);
        return;
    }
    else
    {
        GSUCCESS("Server started on endpoint %s:%d ...\n", ip_to_user_string(serverSocket.Address.sin_addr.s_addr), htons(serverSocket.Address.sin_port));
    }

    i32 listenResult = socket_listen(&serverSocket, max_queue_clients);
    if (!bindResult)
    {
        _http_server_fatal_exit("Failed listen socket to addrs", &serverSocket);
        return;
    }

    i32 serverRuning = 1;
    Socket* aClients = NULL;

    fd_set readSet;
    while (serverRuning)
    {
        FD_ZERO(&readSet);
        i32 serverDescriptor = serverSocket.Descriptor;

        FD_SET(serverDescriptor, &readSet);
        i32 maxDescriptor = serverDescriptor;
        array_foreach(aClients,
                      FD_SET(item.Descriptor, &readSet);
                      if (item.Descriptor > maxDescriptor)
                          maxDescriptor = item.Descriptor;
            );

        i32 selectResult = select(maxDescriptor + 1, &readSet, NULL, NULL, 0);
        if (!selectResult)
        {
            GERROR("Wtf!!!!\n");
        }

        if (FD_ISSET(serverDescriptor, &readSet))
        {
            GINFO("Some new client!\n");
            Socket clientSocket = socket_accept(&serverSocket);
            socket_make_async(&clientSocket);
            array_push(aClients, clientSocket);

            GINFO("Get new client[%d:%d]: %s:%d\n", clientSocket.Descriptor, array_count(aClients), ip_to_user_string(clientSocket.ServerAddress.sin_addr.s_addr), htons(clientSocket.ServerAddress.sin_port));
        }

        i32* toRemove = NULL;

        for (i32 i = 0; i < array_count(aClients); ++i)
        {
            Socket clientSocket = aClients[i];

            if (FD_ISSET(clientSocket.Descriptor, &readSet))
            {

#if 1
                // DOCS: MSS is 1500 bytes for ethernet
                char buf[KB(3)] = {};
                i32 recvResult = socket_recv(&clientSocket, buf, KB(3));
                if (recvResult < 0)
                {
                    GERROR("Recv error!\n");
                    perror("Reson: ");
                }
                else if (recvResult == 0)
                {
                    GINFO("Connection closed!\n");
                }
                else
                {
                    GINFO("Request:\n%s\n", buf);
                    /*
                      method, uri, version
                      GET      /    HTTP/1.1
                    */
                }
#endif

                char* parsedPtr = _http_server_parse_header((char*)buf);
                GINFO("Parsed:\n%s\n", parsedPtr);

                const char responseBody[] =
                    "<title>Test HTTP Server</title>\n"
                    "<h1>Test page</h1>\n";


                HttpCode httpCode = HttpCode_Success;

                HttpResponse response = {
                    .pVersion = http_version,
                    .Code = httpCode,
                    .pStatus = http_code_to_status_string(httpCode),
                    .pServer = "simple-c-http1.1-server",
                    .ContentType = HttpContentType_Html,
                    .pContent = (char*) responseBody,
                };

                // TODO: validate required fields
                // pVersion, Code, pStatus, pServer, COntentType, ContentLength, pContent

                char* responseStr = http_response_to_string(response);
                socket_send(&clientSocket, responseStr, string_length(responseStr));

                string_builder_free(responseStr);

                socket_close(&clientSocket);

                array_push(toRemove, clientSocket.Descriptor);
            }


            for (i32 r = 0; r < array_count(toRemove); ++r)
            {
                i32 toRemoveInd = toRemove[r];
                i32 clientInd = array_index_of(aClients,
                                               item.Descriptor == toRemoveInd);
                if (clientInd != -1)
                    array_remove_at(aClients, clientInd);
            }

            simple_timer_mssleep(1);
        }
    }
}
