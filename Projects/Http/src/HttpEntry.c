#include <Utils/Types.h>

#include <Code/HttpServer.h>

i32
main(i32 count, char** arguments)
{
    http_server_start();

    return 0;
}
