#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <Core/SimpleStandardLibrary.h>
#include <Math/SimpleMathIO.h>

#define USE_SWL 1

#if USE_SWL == 1
#include <unistd.h>

#define SWL_RENDERER
#include <Core/SimpleWindowLibrary.h>

#elif USE_SWL == 0

#include "Event/Event.h"
#include "InputSystem/KeyCodes.h"


#endif

#include <Deps/stb_image.h>


/*
  NOTE: Needs to have simple api as front
  as back we need Xlib, Win64 Api, glfw (as an option for Wayland)
*/


static i32 gAppRun = 1;


#if USE_SWL == 1

static SwlWindow gSwlWindow = {};

void
my_event_callback(SwlEvent* pEvent)
{
    if (pEvent->Type == SwlEventType_WindowResized)
    {
        swl_renderer_resize(&gSwlWindow);
        printf("Size: %d %d\n", gSwlWindow.Size.Width, gSwlWindow.Size.Height);
    }

    if (pEvent->Type == SwlEventType_MousePress
        || pEvent->Type == SwlEventType_MouseRelease)
    {
        SwlMousePressEvent* pMouseKey = (SwlMousePressEvent*) pEvent;
        printf("IsHandled: %d Type: %d Key: %d\n", pEvent->IsHandled, pEvent->Type, pMouseKey->Key);
    }

    if (pEvent->Type == SwlEventType_KeyPress)
    {
        SwlKeyPressEvent *pKeyPress = (SwlKeyPressEvent *)pEvent;

        if (pKeyPress->Key == SwlKey_Escape)
        {
            gAppRun = 0;
        }

        printf("SwlKey: %d %s\n", pKeyPress->Key, swl_key_to_string(pKeyPress->Key));

    }

    if (pEvent->Type == SwlEventType_KeyType)
    {
        SwlKeyTypeEvent* pType = (SwlKeyTypeEvent*) pEvent;

        printf("%d\n", pType->CodePoint);
    }
}

swl_f32
_random_float()
{
    srand(time(NULL));

#define MaxRandom 10
    static i8 notInit = 1;
    static i8 counter = 0;
    static swl_f32 aRandoms[MaxRandom] = {};
    if (notInit)
    {
#define rando() ((f32)(rand() % 10)) / 10
        f32 ns = 0.0f;
        for (i32 i = 0; i < MaxRandom; ++i)
        {
            f32 nv = rando();
            while (nv < 0.001f)
            {
                nv = rando();
            }

            aRandoms[i] = nv;
        }
        notInit = 0;
    }

    ++counter;
    if (counter >= MaxRandom)
        counter = 0;

    return aRandoms[counter];
}


i32
_swl_load_texture(const char* pPath, i32* pW, swl_i32* pH, swl_i32* pC, void** pData)
{
    swl_i32 width, height, channels;
    *pData = (void*) stbi_load(pPath, &width, &height, &channels, 0);
    if (*pData == NULL)
    {
        return 0;
    }

    *pW = width;
    *pH = height;
    *pC = (swl_i8)channels;
    //stbi_image_free(data);
    return 1;
}

swl_i32
main(void)
{
    swl_init((SwlInit) { .MemoryAllocate = malloc, .MemoryFree = free, });
    SwlWindowSettings set = {
        .pName = "Swl Window test",
        .Position = {.X = 200, .Y = 100},
        .Size = {
            .Width = 1920,//968,
            .Height = 1061,
            .MinHeight = 150,
            .MinWidth = 150
        },
    };
    swl_window_create(&gSwlWindow, set);

    swl_window_set_event_call(&gSwlWindow, my_event_callback);

    swl_renderer_create(&gSwlWindow);
    swl_renderer_set_swap_iterval(&gSwlWindow, 1);

#if 1
    i32 codePoints[] = {
        '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~'
    };

#else

    i32 codePoints[] = {
        'H', 'e', 'l', 'o'
    };

#endif

    SwlFontSettings fontSet = {
        .pFontPath = asset_font("NotoSans.ttf"),
        .FontSize = 68,
        .BitmapSize = (swl_v2) { 640, 640 },
        .aCodePoints = codePoints,
        .CodePointsCount = ARRAY_COUNT(codePoints),
    };
    swl_renderer_font_load(&fontSet);
    void* pBitmap = swl_renderer_font_get_bitmap();
    swl_i32 fontId = swl_renderer_texture_create(pBitmap, (swl_v2){fontSet.BitmapSize.X,fontSet.BitmapSize.Y}, 1);
    /* printf("Here\n\n\n"); */

    swl_i32 textureId;
    {
        swl_i32 w, h, c;
        void* pData = NULL;
        swl_i32 isSuccess = _swl_load_texture(
            asset_texture("crimson_stem_top.png"),
            &w, &h, &c, &pData);
        if (!isSuccess)
        {
            printf("Could not load texture \n");
        }
        textureId = swl_renderer_texture_create(pData, (swl_v2){w, h}, c);
    }

    srand(time(NULL));
    swl_f32 r = 0.0654f, g = 0.268f, b = 0.4523f,
        ri = 0.0531f, gi=0.0203f, bi=.103f;
    f64 timestep = 0.111116f;
    f64 prevTime = simple_timer_get_time();
    f32 x = 0;
    while (gAppRun)
    {
        f64 newTime = simple_timer_get_time();
        timestep = newTime - prevTime;
        prevTime = newTime;
        // printf("timestep: %f\n", timestep);

        r += timestep * ri;
        g += timestep * gi;
        b += timestep * bi;

        //printf("r: %f g: %f b: %f\n", r, g, b);

        swl_renderer_clear_color(&gSwlWindow, r, g, b, 1.0f);
        swl_update(&gSwlWindow);

        swl_renderer_draw_rectangle(
            &gSwlWindow,
            (swl_v3){250, 100, 20}, (swl_v2){200, 200},
            (swl_v4) { 0.01127f, 0.01345f, 0.0456f, 1.0f });

        swl_renderer_draw_rectangle(
            &gSwlWindow,
            (swl_v3){250, 500, 20}, (swl_v2){200, 200},
            (swl_v4) { 0.347f, 0.345f, 0.456f, 1.0f });

        swl_renderer_draw_texture(
            &gSwlWindow,
            (swl_v3){x, 100, 10.1}, (swl_v2){10*15, 10*16},
            (swl_v4) { 1.f, 1.f, 1.f, 1.0f },
            textureId);

        swl_renderer_draw_texture(
            &gSwlWindow,
            (swl_v3) {x, 500, 10}, (swl_v2){512, 512},
            (swl_v4) { 1.f, 1.f, 1.f, 1.0f },
            fontId);

        //wchar* text = L"Hello, world!";
        wchar* text = L"My name is 'typedef' and im here to program!";
        swl_renderer_draw_text(&gSwlWindow,
                               (swl_i32*)text, 44,
                               (swl_v3) { 550, 250, 0.21 },
                               (swl_v4) { 0.7, 1, 0.7, 1 });



        x += 75 * timestep;
        if (x < 0)
        {
        }
        if (x > gSwlWindow.Size.Width)
        {
            x = 0;

        }

        if (gSwlWindow.aKeys[SwlKey_A] == SwlAction_Press)
        {
            //printf("X: %f, timestep: %f\n", x, timestep);
        }
        if (gSwlWindow.aKeys[SwlKey_Space] == SwlAction_Press)
        {
            x = 0;
        }
        //printf("X: %f, timestep: %f\n", x, timestep);

        swl_renderer_flush(&gSwlWindow);

#define DoRndmf(fu) ({; fu;})

        static swl_i32 isPositiveTrend = 1;
        if (isPositiveTrend)
        {
            if (r > 0.7f)
                ri = 0;
            if (g > 0.7f)
                gi = 0;
            if (b > 0.7f)
                bi = 0;

            if (r > 0.7f && b > 0.7f && g > 0.7f)
            {
                f32 rv = _random_float();
                ri = -rv;
                rv = _random_float();
                gi = -rv;
                rv = _random_float();
                bi = -rv;

                f32 mv = r;
                if (g > mv)
                    mv = g;
                if (b > mv)
                    mv = b;

                r = g = b = mv;

                isPositiveTrend = 0;
                //printf("Negative trend: %0.16f %0.16f %0.16f\n", ri, gi, bi);
            }
        }
        else
        {
            const f32 rt = 0.0001f;
            if (r <= rt)
                ri = 0;
            if (g <= rt)
                gi = 0;
            if (b <= rt)
                bi = 0;

            if (r <= rt && b <= rt && g <= rt)
            {
                f32 rv = _random_float();
                ri = rv;
                rv = _random_float();
                gi = rv;
                rv = _random_float();
                bi = rv;

                f32 mv = r;
                if (g > mv)
                    mv = g;
                if (b > mv)
                    mv = b;

                r = g = b = mv;
                isPositiveTrend = 1;
                //printf("Positive trend: %0.16f %0.16f %0.16f\n", ri, gi, bi);
            }
        }

    }

    return 0;
}

#elif USE_SWL == 0

//#include "SvlDemo.h"


#endif // USE_SWL == 0
