#include "VlanNet.h"

#include <InputSystem/KeyCodes.h>
#include <Net/SimpleLan.h>
#include <Net/SimpleTap.h>

void
vlan_net_on_attach()
{
    /* simple_socket_get_host_ip(); */

    /* Application* pApp = application_get(); */
    /* SimpleLanSettings lanSet = { */
    /*	.Argc = array_count(pApp->Arguments), */
    /*	.Argv = pApp->Arguments */
    /* }; */
    /* simple_lan_create(lanSet); */
    GINFO("Simple Lan created!\n");
}

void
vlan_net_on_update(f32 timestep)
{

}

void
vlan_net_on_ui_render()
{
    static wchar buffer[] = L"Сетевой Интерфейс";
    WideString label = wide_string_news(buffer);
    printf("Label: \n");
    wide_string_print_line(label);
    static i32 isVisible = 1;
    if (sui_panel_begin(&label, &isVisible, SuiPanelFlags_Movable))
    {
	static wchar btnBuffer[] = L"Соединение";
	WideString connectBtn = wide_string_news(btnBuffer);
	sui_button(&connectBtn, v2_new(100, 50));
    }
    sui_panel_end();

}

void
vlan_net_on_event(Event* event)
{
    switch (event->Category)
    {

    case EventCategory_Key:
    {
	if (event->Type != EventType_KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KeyType_Escape)
	{
	    application_close();
	    event->IsHandled = 1;
	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == EventType_WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }
}

void
vlan_net_on_destroy()
{
}
