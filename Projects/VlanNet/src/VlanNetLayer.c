#include "VlanNet.h"
#include <EntryPoint.h>

void
create_user_application(UserApplicationSettings* pSettings)
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitGraphics = 1,
	.IsFrameRatePrinted = 0,
	.Name = "VlanNet",
	.ArgumentsCount = pSettings->Argc,
	.Arguments = pSettings->Argv,
    };
    application_create(appSettings);

    Layer vlanNetLayer = {0};
    vlanNetLayer.Name = "VlanNet Layer";
    vlanNetLayer.OnAttach = vlan_net_on_attach;
    vlanNetLayer.OnUpdate = vlan_net_on_update;
    vlanNetLayer.OnUIRender = vlan_net_on_ui_render;
    vlanNetLayer.OnEvent = vlan_net_on_event;
    vlanNetLayer.OnDestoy = vlan_net_on_destroy;

    application_push_layer(vlanNetLayer);
}
