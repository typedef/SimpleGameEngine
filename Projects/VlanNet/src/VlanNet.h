#ifndef VLAN_NET_H
#define VLAN_NET_H

#include <Engine.h>

void vlan_net_on_attach();
void vlan_net_on_attach_finished();
void vlan_net_on_update(f32 timestep);
void vlan_net_on_ui_render();
void vlan_net_on_event(Event* event);
void vlan_net_on_destroy();

#endif // VLAN_NET_H
