#include "SimpleTest.h"
#include <EntryPoint.h>

void
create_user_application(UserApplicationSettings* pSettings)
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitOpenGl = 1,
	.IsInitImGui = 1,
	.Name = "Simple Test",
    };
    application_create(appSettings);

    Layer simpleLayer;
    simpleLayer.Name = "Simple Test Layer";
    simpleLayer.OnAttach   = simple_test_on_attach;
    simpleLayer.OnUpdate   = simple_test_on_update;
    simpleLayer.OnUIRender = simple_test_on_ui;
    simpleLayer.OnEvent    = simple_test_on_event;
    simpleLayer.OnDestoy   = simple_test_on_destroy;

    application_push_layer(simpleLayer);
}
