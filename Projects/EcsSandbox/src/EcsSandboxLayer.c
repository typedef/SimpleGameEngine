#include "EcsSandbox.h"
#include <EntryPoint.h>

void
create_user_application(UserApplicationSettings* pSettings)
{
    application_set_locale_utf8();
    logger_init();
    logger_to_terminal();

    ApplicationSettings appSettings = {
#if defined(LINUX_PLATFORM)
	.Width = 2000,
	.Height = 1200,
#elif defined(WINDOWS_PLATFORM)
	.Width = 1000,
	.Height = 800,
#endif
	.IsInitGraphics = 1,
	//BUG(): weird if 1 - bug, 0 - ok
	.IsFrameRatePrinted = 0,
	.IsVsync = 1,
	.IsDebug = 1,
	.UiFpsSyncRatio = 2,
	.Name = "EcsSandbox",
	.ArgumentsCount = pSettings->Argc,
	.Arguments = pSettings->Argv,
    };
    application_create(appSettings);

    Layer ecsSandboxLayer = {0};
    ecsSandboxLayer.Name = "EcsSandbox Layer";
    ecsSandboxLayer.OnAttach = ecs_sandbox_on_attach;
    ecsSandboxLayer.OnUpdate = ecs_sandbox_on_update;
    ecsSandboxLayer.OnUIRender = ecs_sandbox_on_ui;
    ecsSandboxLayer.OnEvent = ecs_sandbox_on_event;
    ecsSandboxLayer.OnDestoy = ecs_sandbox_on_destroy;

    application_push_layer(ecsSandboxLayer);
}
