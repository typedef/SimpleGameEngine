#ifndef ECS_SANDBOX_H
#define ECS_SANDBOX_H

#include <Engine.h>

void ecs_sandbox_on_attach();
void ecs_sandbox_on_attach_finished();
void ecs_sandbox_on_update(f32 timestep);
void ecs_sandbox_on_ui();
void ecs_sandbox_on_event(Event* event);
void ecs_sandbox_on_destroy();

#endif // ECS_SANDBOX_H
