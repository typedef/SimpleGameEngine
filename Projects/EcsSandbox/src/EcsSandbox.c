#include "EcsSandbox.h"
#include "AssetManager/AssetManager.h"
#include "EntitySystem/Components/3D/StaticModelComponent.h"
#include "EntitySystem/SimpleEcs.h"
#include "Graphics/Vulkan/Vsr2d.h"
#include "Graphics/Vulkan/Vsr2dCompositor.h"
#include "InputSystem/SimpleInput.h"
#include "Model/GeneratedModel.h"
#include "UI/Sui.h"
#include "Utils/SimpleStandardLibrary.h"
#include "Utils/Types.h"

#include <Event/Event.h>
#include <EntitySystem/SimpleScene.h>
#include <Editor/SimpleEditor.h>

#include <EntitySystem/Components/Base/CameraComponent.h>

static SimpleWindow* pCurrentWindow = NULL;
static SimpleScene gCurrentScene = {};

void
_callback_submit_line(v3 start, v3 end, f32 thickness, v4 color)
{
    Vsr2dCompositor* pCompositor = (Vsr2dCompositor*) gCurrentScene.SceneRenderer.p2dCompositor;
    vsr_2d_compositor_world_submit_line(&pCompositor->WorldRender, start, end, thickness, color);
}

void
_callback_flush()
{
    Vsr2dCompositor* pCompositor = (Vsr2dCompositor*) gCurrentScene.SceneRenderer.p2dCompositor;
    vsr_2d_compositor_flush_world(pCompositor);
}

void
_callback_set_visible_rect(v2 min, v2 max)
{
    Vsr2dCompositor* pCompositor = (Vsr2dCompositor*) gCurrentScene.SceneRenderer.p2dCompositor;

    VsrVisibleRect rect = {
	.Min = min,
	.Max = max,
    };
    vsr_2d_set_visible_rect(&pCompositor->ScreenRender2, rect);
}

void
_callback_submit_line_on_screen(v3 start, v3 end, f32 thickness, v4 color)
{
    Vsr2dCompositor* pCompositor = (Vsr2dCompositor*) gCurrentScene.SceneRenderer.p2dCompositor;

    vsr_2d_compositor_submit_line(&pCompositor->ScreenRender2, start, end, thickness, color);
}

void
_callback_flush_screen()
{
    Vsr2dCompositor* pCompositor = (Vsr2dCompositor*) gCurrentScene.SceneRenderer.p2dCompositor;
    vsr_2d_compositor_flush_screen2(pCompositor);
}

void*
_callback_get_2d_camera()
{
    Vsr2dCompositor* pCompositor = (Vsr2dCompositor*) gCurrentScene.SceneRenderer.p2dCompositor;
    /* Camera* pCamera = (Camera*) pCompositor->ScreenRender2.pCamera; */
    /* m4_print(pCamera->View, "view"); */
    /* m4_print(pCamera->Projection, "proj"); */
    /* m4_print(pCamera->ViewProjection, "viewproj"); */

    return pCompositor->ScreenRender2.pCamera;
}

#include <Utils/miniaudio.h>

static v3 gTempPos;
// we should put sound engine on separate thread
void*
process_sounds(void* pData)
{
    static ma_engine gMaEngine;
    static ma_sound sound;

    ma_result maResult;
    ma_engine_config conf = ma_engine_config_init();
    maResult = ma_engine_init(&conf, &gMaEngine);
    if (maResult != MA_SUCCESS)
    {
	vassert_break();
    }

    maResult = ma_sound_init_from_file(&gMaEngine, asset_sound("SimpleSound.wav"), 0, NULL, NULL, &sound);
    if (maResult != MA_SUCCESS) {
	vassert_break();
    }

    ma_sound_set_looping(&sound, 1);
    ma_sound_set_position(&sound, 0, 0, 0);
    ma_sound_start(&sound);

    while (1)
    {
	//v3_print(gTempPos);
	ma_engine_listener_set_position(&gMaEngine, 0, gTempPos.X, gTempPos.Y, gTempPos.Z);
	simple_timer_mssleep(200);
	printf("No doing anything in the audio loop.\n");
    }

    return NULL;
}

void
ecs_sandbox_on_attach()
{
    //logger_to_file();
    //simple_thread_create(process_sounds, KB(5), NULL);
    //printf("Samurai!!!\n");

    pCurrentWindow = application_get_window();
    gCurrentScene = simple_scene_new();

    SimpleEditorSetting editorSetting = {
	.pScene = (void*) &gCurrentScene,
	.Callbacks = {
	    .SubmitLine = _callback_submit_line,
	    .Flush = _callback_flush,

	    .ScreenSubmitLine = _callback_submit_line_on_screen,
	    .ScreenFlush = _callback_flush_screen,

	    .SetVisibleRect = _callback_set_visible_rect,
	    .Get2dCamera = _callback_get_2d_camera,
	},
    };
    simple_editor_register_layer(&editorSetting);

    /*

      DOCS: Setup Scene

    */

    StaticModel cube = static_model_cube_create();


    WideString modelName = wide_string(L"Красный куб");
    MeshMaterial redMat = {
	.Name = "Red Material",
	.IsMetallicExist = 1,
	.Metallic = (PbrMetallic) {
	    .MetallicFactor = 0,
	    .RoughnessFactor = 0,
	    .BaseColor = v4_new(1, 0, 0, 1),
	    .BaseColorTexture = 0,
	    .MetallicRoughnessTexture = 0
	}
    };
    i64 matId = asset_manager_load_material_id(&redMat);
    StaticModel redCube = static_model_cube_create();
    redCube.aMeshes[0].MaterialId = matId;

    StaticModel gizmoPointer =  asset_manager_load_model(
	resource_model("GizmoPointer/Gizmo.gltf"));
    StaticModel retroPcModel = asset_manager_load_model(asset_model("Retro computer/Retro computer(Low only).gltf"));

    {
	EntityId entityId = ecs_entity("Red Cube Entity");
	TransformComponent tc = TransformComponent_Position(1, -3, 0);
	ecs_entity_add_component(entityId, TransformComponent, tc);
	StaticModelComponent smc = static_model_component_create_model(redCube);
	ecs_entity_add_component(entityId, StaticModelComponent, smc);
    }

    {
	EntityId entityId = ecs_entity("Cube Entity");
	TransformComponent tc = TransformComponent_Position(-4, 3, 0);
	ecs_entity_add_component(entityId, TransformComponent, tc);
	StaticModelComponent smc = static_model_component_create_model(cube);
	ecs_entity_add_component(entityId, StaticModelComponent, smc);
    }

    {
	EntityId entityId = ecs_entity("Retro Pc Entity");
	TransformComponent tc = TransformComponent_Position(0, 0, 0);
	ecs_entity_add_component(entityId, TransformComponent, tc);
	StaticModelComponent smc = static_model_component_create_model(retroPcModel);
	ecs_entity_add_component(entityId, StaticModelComponent, smc);
    }

    {
	EntityId entityId = ecs_entity("Gizmo Pointer Entity");
	TransformComponent tc = TransformComponent_Position(2, -1, 0);
	ecs_entity_add_component(entityId, TransformComponent, tc);
	StaticModelComponent smc = static_model_component_create_model(gizmoPointer);
	ecs_entity_add_component(entityId, StaticModelComponent, smc);
    }

    {
	EntityId newCameraEntity = ecs_entity("Second cam");
	CameraComponentCreateInfo createInfo = {
	    .IsMainCamera = 0,
	    .IsCameraMoved = 1,

	    .Settings = {
		.Type = CameraType_PerspectiveSpectator,
		.Position = v3_new(1.0f, .5f, 5.92f),
		.Front = v3_new(0, 0, -1),
		.Up    = v3_new(0, 1,  0),
		.Right = v3_new(1, 0,  0),
		.Yaw = 0,
		.Pitch = 0,
		.PrevX = 0,
		.PrevY = 0,
		.SensitivityHorizontal = 0.35f,
		.SensitivityVertical = 0.45f
	    },

	    .Perspective = {
		.Near = 0.001f,
		.Far = 50000.0f,
		.AspectRatio = pCurrentWindow->AspectRatio,
		// NOTE(typedef): in degree
		.Fov = 85.0f
	    },

	    .Spectator = {
		.Speed = 2.5f,
		.Zoom = 0
	    }
	};

	CameraComponent cam =
	    camera_component_new_ext(createInfo);
	camera_component_update(&cam);

	ecs_entity_add_component(newCameraEntity, CameraComponent, cam);
    }
}

void
ecs_sandbox_on_update(f32 timestep)
{
    simple_scene_update(&gCurrentScene);

    CameraComponent* pCc = (CameraComponent*)simple_scene_get_main_camera(&gCurrentScene);
    Camera* p2dCamera = &(pCc)->Base;
    // BUG: camera is not updating inside ecs
    gTempPos = pCc->Settings.Position;

    printf("%p\n", pCc);

    //v3_print(gTempPos);
    //m4_print(p2dCamera->View, "view");
    //m4_print(p2dCamera->Projection, "proj");
    //m4_print(p2dCamera->ViewProjection, "viewproj");

    static SimpleTimerData timer = {
	.WaitSeconds = 0.1,
    };

    if (!simple_timer_interval(&timer, timestep))
	return;

    if (input_is_key_pressed(KeyType_T))
    {
	GINFO("Pressed T!\n");

	ComponentRecord compRec =
	    _ecs_world_get_component_record(
		gCurrentScene.pEcsWorld, ToString(CameraComponent));
	CameraComponent* aCameras = (CameraComponent*) compRec.Data;

	i32 i, count = array_count(aCameras);
	for (i = 0; i < count; ++i)
	{
	    CameraComponent* pCamera = &aCameras[i];
	    if (!pCamera->IsMainCamera)
	    {
		simple_scene_set_main_camera(&gCurrentScene, pCamera);
	    }
	}

	array_foreach_ptr(aCameras,
			  if (!item->IsMainCamera)
			  {
			      simple_scene_set_main_camera(
				  &gCurrentScene, item);
			  }
	    );

    }
}

void
ecs_sandbox_on_ui()
{
    // note: can render gizmo from sui raw api, but need to write sui raw api first :)

    if (sui_panel_begin("Hello Panel", SuiPanelFlags_Movable))
    {
	sui_button("Do smth", v2_0());
    }

    sui_panel_end();
}

void
ecs_sandbox_on_event(Event* event)
{
    switch (event->Category)
    {

    case EventCategory_Key:
    {
	if (event->Type != EventType_KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;

	switch (keyEvent->KeyCode)
	{

	case KEY_ESCAPE:
	{
	    application_close();
	    event->IsHandled = 1;
	}
	break;

	case KeyType_Space:
	{
	}
	break;

	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == EventType_WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }
}

void
ecs_sandbox_on_destroy()
{
    simple_scene_destroy(&gCurrentScene);
}
