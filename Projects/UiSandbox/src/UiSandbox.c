#include "UiSandbox.h"
#include "UI/Sui.h"

#include <Core/SimpleStandardLibrary.h>
#include <InputSystem/KeyCodes.h>
#include <UI/SuiDemo.h>

void
ui_sandbox_on_attach()
{
    /* printf("Разрядность операционной системы - %ld бит\n", sizeof(void*) * 8); */
    /* vguard(0); */

    SimpleTime time;
    simple_time_now(&time);
    simple_time_print(&time);

    input_init(application_get()->Window->GlfwWindow);
    logger_set_flags(
#if 0
        LoggerFlags_PrintType
#else
        LoggerFlags_PrintType | LoggerFlags_PrintShortLine | LoggerFlags_PrintFile | LoggerFlags_PrintShortTime
#endif
        );
}

void
ui_sandbox_on_update(f32 timestep)
{
    static SimpleTimerData std = {.WaitSeconds=5};
    if (simple_timer_interval(&std, timestep))
    {
        //SuiInstance* pInstance = sui_get_instance();
        //SuiPanel* pPanel = sui_panel_get_by_i(0);
        //GINFO("id: %d \n", pPanel->Id);
    }
}

void
ui_sandbox_on_ui_render()
{
    sui_demo_of_frontend_usage();
}

void
ui_sandbox_on_event(Event* event)
{
    switch (event->Category)
    {

    case EventCategory_Key:
    {
        if (event->Type != EventType_KeyPressed)
            break;

        KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
        if (keyEvent->KeyCode == KeyType_Escape)
        {
            application_close();
            event->IsHandled = 1;
        }

        break;
    }

    case EventCategory_Window:
    {
        if (event->Type == EventType_WindowShouldBeClosed)
            application_close();
        break;
    }

    }

}

void
ui_sandbox_on_destroy()
{
}
