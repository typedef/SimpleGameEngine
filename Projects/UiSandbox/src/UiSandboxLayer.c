#include "UiSandbox.h"
#include <EntryPoint.h>

void
create_user_application(UserApplicationSettings* pSettings)
{
    logger_set_flags(LoggerFlags_Short);

    ApplicationSettings appSettings = {
#if defined(LINUX_PLATFORM)
	.Width = 2000,
	.Height = 1200,
#elif defined(WINDOWS_PLATFORM)
	.Width = 1000,
	.Height = 800,
#endif
	.IsVsync = 1,
	.IsInitGraphics = 1,
	.IsDebug = 1,
	.UiFpsSyncRatio = 1,
	.Name = "UiSandbox",
    };
    application_create(appSettings);

    Layer uiSandboxLayer = {
	.Name = "UiSandbox Layer",
	.OnAttach = ui_sandbox_on_attach,
	.OnUpdate = ui_sandbox_on_update,
	.OnUIRender = ui_sandbox_on_ui_render,
	.OnEvent = ui_sandbox_on_event,
	.OnDestoy = ui_sandbox_on_destroy
    };

    application_push_layer(uiSandboxLayer);
}
