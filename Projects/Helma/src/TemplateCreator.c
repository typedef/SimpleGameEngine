#include "TemplateCreator.h"

#include <stdio.h>

#include <Core/SimpleStandardLibrary.h>

static char* ScriptDirectory;

static char*
string_to_convention(const char* str, size_t length, char conventionChar)
{
    i32 upperCount = string_count_upper(str);
    if (upperCount <= 0)
    {
        vassert_break();
        return string_copy(str, length);
    }

    if (upperCount == 1)
    {
        return string_to_lower(str);
    }

    size_t newLength = length + upperCount - 1;
    char* nstr = memory_allocate(newLength * sizeof(char));
    memset(nstr, '\0', newLength * sizeof(char));
    char* ptr = (char*) nstr;
    char c;
    char* rptr = (char*)str;
    char* eptr = rptr + newLength;

    typedef enum LocalFlag {
        LocalFlag_IsFirst = 0,
        LocalFlag_IsSecondary = 1,
        LocalFlag_IsThird = 2
    } LocalFlag;

    LocalFlag flag = LocalFlag_IsFirst;
    while (rptr != eptr)
    {
        c = *rptr;

        if (char_is_upper(c) && flag != LocalFlag_IsThird)
        {
            if (flag == LocalFlag_IsFirst)
            {
                flag = LocalFlag_IsSecondary;
                *ptr = char_to_lower(c);
                ++rptr;
            }
            else
            {
                flag = LocalFlag_IsThird;
                *ptr = conventionChar;
            }
        }
        else
        {
            flag = LocalFlag_IsSecondary;
            *ptr = char_to_lower(c);
            ++rptr;
        }

        ++ptr;
    }

    return nstr;
}

static void
_write_file(char* templateFile, char* projectName, char* srcDir, char* contentToChange)
{
    GLOG("Write file !\n");
    i32 contentToChangeLength = string_length(contentToChange);
    GINFO("Final content: %*s\n", contentToChangeLength, contentToChange);

    if (string_compare(templateFile, "Templates/template-layer.c"))
    {
        char* fileName = string_concat(projectName, "Layer.c");
        char* srcFile = path_combine(srcDir, fileName);
        file_write_string(srcFile, contentToChange, contentToChangeLength);
    }
    else if (string_compare(templateFile, "Templates/template.h"))
    {
        char* fileName = string_concat(projectName, ".h");
        char* srcFile = path_combine(srcDir, fileName);
        file_write_string(srcFile, contentToChange, contentToChangeLength);

    }
    else if (string_compare(templateFile, "Templates/template.c"))
    {
        char* fileName = string_concat(projectName, ".c");
        char* srcFile = path_combine(srcDir, fileName);
        file_write_string(srcFile, contentToChange, contentToChangeLength);

    }
    else if (string_compare(templateFile, "Templates/template-project-premake.lua"))
    {
        char* projectNameSeparated = string_to_convention(projectName, string_length(projectName), '-');
        char* premakeFileName = string_concat3("premake-", projectNameSeparated, ".lua");

        file_write_string(premakeFileName, contentToChange, contentToChangeLength);

        memory_free(projectNameSeparated);
        memory_free(premakeFileName);
    }
    else if (string_index_of_string(templateFile, ".sh") != -1)
    {
        if (!path_is_directory_exist(ScriptDirectory))
        {
            GERROR("Directory !Exist%s\n", ScriptDirectory);
            return;
        }

        char* shPath;
        if (string_compare(templateFile, "Templates/template_rebuild_debug.sh"))
        {
            shPath = string_concat(ScriptDirectory, "/rebuild_debug.sh");
        }
        else if (string_compare(templateFile, "Templates/template_rebuild_release.sh"))
        {
            shPath = string_concat(ScriptDirectory, "/rebuild_release.sh");
        }
        else if (string_compare(templateFile, "Templates/template_generate_project.sh"))
        {
            shPath = string_concat(ScriptDirectory, "/generate_project.sh");
        }
        else if (string_compare(templateFile, "Templates/template_debug.sh"))
        {
            shPath = string_concat(ScriptDirectory, "/debug.sh");
        }
        else if (string_compare(templateFile, "Templates/template_call_clean.sh"))
        {
            shPath = string_concat(ScriptDirectory, "/call_clean.sh");
        }
        else if (string_compare(templateFile, "Templates/template_call_clean_all.sh"))
        {
            shPath = string_concat(ScriptDirectory, "/call_clean_all.sh");
        }
        else if (string_compare(templateFile, "Templates/template_build_release.sh"))
        {
            shPath = string_concat(ScriptDirectory, "/build_release.sh");
        }
        else if (string_compare(templateFile, "Templates/template_build_debug.sh"))
        {
            shPath = string_concat(ScriptDirectory, "/build_debug.sh");
        }
        else
        {
            vassert(0 && "Wront templateFile!");
        }

        file_write_string_exe(shPath, contentToChange, contentToChangeLength);

        memory_free(shPath);
    }
    else
    {
        GERROR("TemplateFile: %s\n", templateFile);
        vassert(0 && "Wrong template file!");
    }
}

static void
swap_subset_and_write_to_file(char* projectName, char* srcDir, char** templateFiles, char** stringsToChange, char** stringsToSwap)
{
    i32 i, count = array_count(templateFiles);
    for (i = 0; i < count; ++i)
    {
        char** stringsToDelete = NULL;
        char* fileContent = file_read_string(templateFiles[i]);
        char* contentToChange = fileContent;
        size_t stringsCount = array_count(stringsToChange);
        for (i32 s = 0; s < stringsCount; ++s)
        {
            char* stringToChange = stringsToChange[s];
            if (!contentToChange)
            {
                GINFO("Not Content to change!\n");
                i32 wasd = 1234;
            }
            char* stringToSwap = stringsToSwap[s];
            char* projectNameReplaced = string_replace_string(
                contentToChange, string_length(contentToChange),
                stringToChange, string_length(stringToChange),
                stringToSwap, string_length(stringToSwap));
            vassert_not_null(projectNameReplaced);

            array_push(stringsToDelete, projectNameReplaced);
            contentToChange = projectNameReplaced;
            GINFO("Final content: %*s\n", string_length(contentToChange), contentToChange);
        }

        _write_file(templateFiles[i], projectName, srcDir, contentToChange);

        memory_free(fileContent);

        for (i32 i = 0; i < array_count(stringsToDelete); ++i)
        {
            memory_free(stringsToDelete[i]);
        }
    }
}

static void
_generate_base_source_files(char* projectName, i32 projectNameLength, char* srcDir, char* projectNameSeparated, char* camelProjectName, char* projectNameSeparatedWeird)
{
    char** stringsToChange = NULL;
    array_push(stringsToChange, (char*)"${ProjectName}");          // "ProjectName"
    array_push(stringsToChange, (char*)"${ProjectNameConvLower}"); // "project_name"
    array_push(stringsToChange, (char*)"${ProjectNameConvUpper}"); // "PROJECT_NAME"
    array_push(stringsToChange, (char*)"${ProjectNameCamel}");     // "projectName"
    array_push(stringsToChange, (char*)"${ProjectNameConvWeird}"); // "project-name"
    char** stringsToSwap = NULL;
    array_push(stringsToSwap, string_copy(projectName, projectNameLength));
    array_push(stringsToSwap, projectNameSeparated);
    array_push(stringsToSwap, string_to_upper(projectNameSeparated));
    array_push(stringsToSwap, camelProjectName);
    array_push(stringsToSwap, projectNameSeparatedWeird);
    char** templateFiles = NULL;
    array_push(templateFiles, "Templates/template-layer.c");
    array_push(templateFiles, "Templates/template.h");
    array_push(templateFiles, "Templates/template.c");
    // NOTE(typedef): project generation premake5 file
    array_push(templateFiles, "Templates/template-project-premake.lua");
    // NOTE(typedef): project build files
    array_push(templateFiles, "Templates/template_rebuild_release.sh");
    array_push(templateFiles, "Templates/template_rebuild_debug.sh");
    array_push(templateFiles, "Templates/template_generate_project.sh");
    array_push(templateFiles, "Templates/template_debug.sh");
    array_push(templateFiles, "Templates/template_call_clean.sh");
    array_push(templateFiles, "Templates/template_call_clean_all.sh");
    array_push(templateFiles, "Templates/template_build_release.sh");
    array_push(templateFiles, "Templates/template_build_debug.sh");

    GLOG("Start changing content!\n");
    swap_subset_and_write_to_file(projectName, srcDir, templateFiles, stringsToChange, stringsToSwap);

    array_free(stringsToChange);
    array_free(templateFiles);
    array_free_w_item(stringsToSwap);
}

static void
project_source_file_create(ProjectInfo info, char* srcDir)
{
    char* projectName = (char*) info.ProjectName;
    i32 projectNameLength = string_length(info.ProjectName);

    char* projectNameSeparated = string_to_convention(projectName, projectNameLength, '_');
    if (0 && !string_compare(projectNameSeparated, "new_project"))
    {
        GERROR("Project Name Separated: %s\n", projectNameSeparated);
        vassert(0 && "Wrong convention string!");
    }
    char* projectNameSeparatedWeird = string_to_convention(projectName, projectNameLength, '-');
    char* camelProjectName = string(info.ProjectName);
    camelProjectName[0] = char_to_lower(camelProjectName[0]);

    _generate_base_source_files(projectName, projectNameLength, srcDir, projectNameSeparated, camelProjectName, projectNameSeparatedWeird);
}

i32
project_create(ProjectInfo info)
{
    /*
      NOTE(typedef):
      Creates directories: Projects/NewProject, Projects/NewProject/src
    */
    char* rootDir = path_combine("Projects/", info.ProjectName);
    if (!path_directory_create(rootDir)
        && !path_is_directory_exist(rootDir))
    {
        GINFO("Directory already exist!\n");
        memory_free(rootDir);
        return 0;
    }
    char* srcDir = path_combine(rootDir, "src");
    if (!path_directory_create(srcDir)
        && !path_is_directory_exist(srcDir))
    {
        GERROR("Can't create dir: %s\n", rootDir);
        return 0;
    }
    GINFO("Directories created!\n");

    ScriptDirectory = string_concat("Scripts/Linux/", info.ProjectName);
    if (!path_directory_create(ScriptDirectory)
        && !path_is_directory_exist(ScriptDirectory))
    {
        GERROR("Directory cannot be created for some reason: %s\n", ScriptDirectory);
        memory_free(ScriptDirectory);
        vassert_break();
        return 0;
    }

    /*
      NOTE(typedef):
      Creates NewProject.h NewProject.c NewProjectLayer.c
    */
    project_source_file_create(info, srcDir);

#if 0
    //change existing clean_main_projects_copycopy.sh
    char* txt = file_read_string("Scripts/Linux/clean_main_projects_copycopy.sh");
    char* ptr = txt;
    char* sptr = NULL;
    i32 ind;
    while (*ptr != '\0')
    {
        // projectsDirs
        if (string_compare(ptr, "projectsDirs["))
        {
            sptr = ptr;
            ptr += 13;
            if (*ptr != '*')
            {
                char* optr;
                for (optr = ptr; *optr != ']'; ++optr);
                ind = string_to_i32_length(ptr, optr - ptr);
            }
        }

        ++ptr;
    }

    while (*sptr != '\n')
    {
        ++sptr;
    }
    ++sptr;

    char* firstPart = string_copy(txt, sptr - txt);
    char fmt[128];
    string_format(fmt, "projectsDirs[%d]=\"%s\"", ind, info.ProjectName);
    char* secondPart = string_copy(txt, sptr - txt);
#endif

    memory_free(srcDir);
    memory_free(ScriptDirectory);

    return 1;
}
