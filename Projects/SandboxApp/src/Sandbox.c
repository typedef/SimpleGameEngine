#include "Sandbox.h"

#include <InputSystem/KeyCodes.h>
#include <Graphics/SimpleWindow.h>
#include <Math/SimpleMath.h>
#include <UI/Sui.h>

void
sandbox_on_attach()
{

    GINFO("server exit!\n");
    vguard(0);
}

void
sandbox_on_update(f32 timestep)
{
}

void
sandbox_on_ui_render()
{
}

void
sandbox_on_event(Event* event)
{
}

void
sandbox_on_destroy()
{
}
