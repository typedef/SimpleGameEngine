#include "Svl.h"
#include "Graphics/CameraType.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <dirent.h>
#include <string.h>

#include <Core/Types.h>
#define SVL_CRASH_ON_ERROR 1
#define SWL_X11_BACKEND 1
#define SWL_IMPLEMENTATION
#include <Core/SimpleWindowLibrary.h>
#include <Core/SimpleVulkanLibrary.h>
#include <Core/SimpleStandardLibrary.h>

#include <Math/SimpleMathIO.h>
#include <Event/Event.h>
#include <Graphics/SimpleWindow.h>
#include <InputSystem/KeyCodes.h>

#include <EntitySystem/Components/Base/CameraComponent.h>

#include <Math/SimpleMath.h>
#include <Graphics/Camera.h>


typedef struct DemoVertex
{
    v3 Position;
    v4 Color;
} DemoVertex;

static i32 gAppRun = 1;

void
my_event_callback(SwlEvent* pEvent)
{
    if (pEvent->Type == SwlEventType_WindowResized)
    {
    }

    if (pEvent->Type == SwlEventType_MousePress
        || pEvent->Type == SwlEventType_MouseRelease)
    {
        SwlMousePressEvent* pMouseKey = (SwlMousePressEvent*) pEvent;
        printf("IsHandled: %d Type: %d Key: %d\n", pEvent->IsHandled, pEvent->Type, pMouseKey->Key);
    }

    if (pEvent->Type == SwlEventType_KeyPress)
    {
        SwlKeyPressEvent *pKeyPress = (SwlKeyPressEvent *)pEvent;

        if (pKeyPress->Key == SwlKey_Escape)
        {
            gAppRun = 0;
        }

        printf("SwlKey: %d %s\n", pKeyPress->Key, swl_key_to_string(pKeyPress->Key));

    }

    if (pEvent->Type == SwlEventType_KeyType)
    {
        SwlKeyTypeEvent* pType = (SwlKeyTypeEvent*) pEvent;

        printf("%d\n", pType->CodePoint);
    }
}

static SwlWindow gSwlWindow = {};

void
svl_on_attach()
{
    GINFO("Use svl library ...\n");
    GINFO("Size: %ld\n", sizeof(gSwlWindow));

    swl_init((SwlInit)
             {
                 .MemoryAllocate = malloc,
                 .MemoryFree = free,
             });

    SwlWindowSettings set = {
        .pName = "New working window",
            .Size = {
                .Width=2000,
                .Height=1200,
                .MinHeight = 150,
                .MinWidth = 150
            },
            .Position = {100,100}
    };
    swl_window_create(&gSwlWindow, set);
    swl_window_set_event_call(&gSwlWindow, my_event_callback);

    SvlSettings svlSet = {
        .IsDebug = 1,
        .IsVsync = 1,
        .IsDevicePropsPrintable = 1,
        // in vulkan 1.1 we have problem with VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT
        .VulkanVersion = VK_MAKE_API_VERSION(0, 1, 3, 0),
        // note: it should be inlined with svl backend ptr
        .pWindowBackend = gSwlWindow.pBackend,
        .pName = "Demo app",
        .Extent = (SvlExtent) {
            .Width = gSwlWindow.Size.Width,
            .Height = gSwlWindow.Size.Height,
        },

        .Paths = {
            .pRootDirectory = path_get_current_directory()
        },

    };

    SvlInstance svlInstance = svl_create(&svlSet);
    if (svlInstance.IsInitialized)
    {
        printf("svlInstance is created successfully!\n");
    }
    else
    {
        printf(SvlTerminalRed("Error")"\n");
        return;
    }
    printf("SvlInstance.Size: %lu\n", sizeof(SvlInstance));

    SvlRenderPassSettings renderPassSet = {
        .Type = SvlRenderPassType_SwapChained,

        .Dependency = (VkSubpassDependency) {
            .srcSubpass = VK_SUBPASS_EXTERNAL,
            .dstSubpass = 0,
            .srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            .dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
            .dependencyFlags = 0,
        },

        .AttachmentsCount = 3,

        .aAttachments = {
            [0] = {
                .Type = SvlAttachmentType_Default,
                .Description = {
                    .format = svlInstance.Swapchain.SurfaceFormat.format,
                    .samples = VK_SAMPLE_COUNT_8_BIT,
                    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                    .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                    .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                },
                .Reference = {
                    //.attachment = 0, AUTO DUE TO INDEX IN ARRAY
                    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                }
            },

            [1] = {
                .Type = SvlAttachmentType_Depth,
                .Description = {
                    .format = VK_FORMAT_D32_SFLOAT,
                    .samples = VK_SAMPLE_COUNT_8_BIT,
                    .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                    .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
                },
                .Reference = {
                    .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
                }
            },

            [2] = {
                .Type = SvlAttachmentType_SwapChained,
                .Description = {
                    .format = svlInstance.Swapchain.SurfaceFormat.format,
                    .samples = VK_SAMPLE_COUNT_1_BIT,
                    .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                    .finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
                },
                .Reference = {
                    .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                }
            }
        }

    };

    SvlErrorType error;
    SvlRenderPass svlRenderPass =
        svl_render_pass_create(&svlInstance, renderPassSet, &error);

    // NOTE: renderer part
    // NOTE: renderer part
    // NOTE: renderer part

    SvlShaderStage aStages[] = {
        [0] = (SvlShaderStage) {
            .Type = SvlShaderType_Vertex,
            .ShaderSourcePath = resource_shader("Demo.vert"),
            .DescriptorsCount = 1,
            .aDescriptors = {
                [0] = {
                    .pName = "u_CameraUbo",
                    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                    .Count = 1,
                    .Size = sizeof(m4)
                },
            }
        },

        [1] = (SvlShaderStage) {
            .Type = SvlShaderType_Fragment,
            .ShaderSourcePath = resource_shader("Demo.frag"),
            .DescriptorsCount = 0,
            .aDescriptors = NULL,
        }
    };

    SvlShaderAttribute aAttributes[] = {
        [0] = {
            .Format = VK_FORMAT_R32G32B32_SFLOAT,
            .Offset = OffsetOf(DemoVertex, Position),
        },

        [1] = {
            .Format = VK_FORMAT_R32G32B32A32_SFLOAT,
            .Offset = OffsetOf(DemoVertex, Color),
        },
    };

    SvlPipelineSettings pipeSets = {
        .Flags = SvlPipelineFlags_DepthStencil | SvlPipelineFlags_BackFaceCulling,

        // DOCS: Render pipeline description
        .Stride = sizeof(DemoVertex),

        .StagesCount = ARRAY_COUNT(aStages),
        .aStages = aStages,

        .AttributesCount = ARRAY_COUNT(aAttributes),
        .aAttributes = aAttributes,

        .PolygonMode = VK_POLYGON_MODE_FILL,

        // DOCS: Some shader constants
        .MaxInstanceCount = 100,

    };

    SvlErrorType pipeError = 0;
    SvlPipeline* pSvlPipeline = svl_pipeline_create(&svlInstance, pipeSets, &pipeError);
    if (pSvlPipeline == NULL)
    {
        if (pipeError == SvlErrorType_Pipeline)
        {
            GERROR("Can't create SvlPipeline with svl_pipeline_create!\n");
            vguard(0);
        }

        vguard(0);
    }

    v2 p0 = v2_new(150, 10), size = v2_new(250, 200);
#if 1

    DemoVertex vertices[] = {
        [0] = (DemoVertex) {
            .Position = v3_new(p0.X, p0.Y + size.Y, 1),
            .Color = v4_new(1, 1, 1, 1),
        },
        [1] = (DemoVertex) {
            .Position = v3_new(p0.X, p0.Y, 1),
            .Color = v4_new(1, 1, 1, 1),
        },
        [2] = (DemoVertex) {
            .Position = v3_new(p0.X + size.X, p0.Y, 1),
            .Color = v4_new(1, 1, 1, 1),
        },
        [3] = (DemoVertex) {
            .Position = v3_new(p0.X + size.X, p0.Y + size.Y, 1),
            .Color = v4_new(1, 1, 1, 1),
        },
    };

#else

    DemoVertex vertices[] = {
        [0] = (DemoVertex) {
            .Position = v3_new(p0.X, p0.Y, 1),
            .Color = v4_new(1, 1, 1, 1),
        },
        [1] = (DemoVertex) {
            .Position = v3_new(p0.X, p0.Y + size.Y, 1),
            .Color = v4_new(1, 1, 1, 1),
        },
        [2] = (DemoVertex) {
            .Position = v3_new(p0.X + size.X, p0.Y + size.Y, 1),
            .Color = v4_new(1, 1, 1, 1),
        },
        [3] = (DemoVertex) {
            .Position = v3_new(p0.X + size.X, p0.Y, 1),
            .Color = v4_new(1, 1, 1, 1),
        },
    };

#endif

    svl_u32 indices[] = {
        0,1,2,2,3,0
        /* , */
        /* 4,5,6,6,7,4 */
    };

    // NOTE(typedef): For now we are not handling situation with "buffer exceeded event"
    SvlInstance* pInstance = &svlInstance;

    SvlErrorType verr = 0;
    SvlBufferSettings vset = {
        .Size = sizeof(vertices),
        .Usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
    };
    SvlBuffer vbuf = svl_buffer_create(pInstance, vset, &verr);
    svl_buffer_set_data(pInstance, &vbuf, &vertices, 0, sizeof(vertices));

    SvlErrorType ierr = 0;
    SvlBufferSettings iset = {
        .Size = sizeof(indices),
        .Usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
    };
    SvlBuffer ibuf = svl_buffer_create(pInstance, iset, &ierr);

    svl_buffer_set_data(pInstance, &ibuf, indices, 0, sizeof(indices));

    vguard_not_null(pSvlPipeline->Binding.aUniforms);
    SvlUniform* pCameraUniform = NULL;
    for (svl_i32 i = 0; i < 1/* svl_array_count(pSvlPipeline->Binding.aUniforms) */; ++i)
    {
        SvlUniform svlUniform = pSvlPipeline->Binding.aUniforms[i];
        if (string_compare(svlUniform.pName, "u_CameraUbo"))
        {
            pCameraUniform = &pSvlPipeline->Binding.aUniforms[i];
            break;
        }
    }

    vguard_not_null(pCameraUniform);

    // DOCS: Use camera uniform
#if 1
    CameraComponentCreateInfo createInfo = {
        .IsCameraMoved = 1,
        .Base = 0,

        .Settings = {
            .Type = CameraType_OrthographicSpectator,
            .Position = v3_new(0, 0, 1.0f),
            .Front = v3_new(0, 0, -1),
            .Up    = v3_new(0, 1,  0),
            .Right = v3_new(1, 0,  0),
            .Yaw = 0,
            .Pitch = 0,
            .PrevX = 0,
            .PrevY = 0,
            .SensitivityHorizontal = 0.35f,
            .SensitivityVertical = 0.45f
        },

        .Orthographic = {
            .Near = 0.1f,
            .Far = 1000.0f,
            .Left = 0,
            .Right = (f32) gSwlWindow.Size.Width,
            .Bot = 0,
            .Top = (f32) gSwlWindow.Size.Height
        },

        .Spectator = {
            .Speed = 5.0f,
            .Zoom = 0
        }
    };

#else

    CameraComponentCreateInfo createInfo = {
        .IsMainCamera = 1,
        .IsCameraMoved = 1,

        .Settings = {
            .Type = CameraType_PerspectiveSpectator,
            .Position = v3_new(0.0f, 0.0f, 7.92f),
            .Front = v3_new(0, 0, -1),
            .Up    = v3_new(0, 1,  0),
            .Right = v3_new(1, 0,  0),
            .Yaw = 0,
            .Pitch = 0,
            .PrevX = 0,
            .PrevY = 0,
            .SensitivityHorizontal = 0.35f,
            .SensitivityVertical = 0.45f
        },

        .Perspective = {
            .Near = 0.001f,
            .Far = 50000.0f,
            .AspectRatio = ((f32)gSwlWindow.Size.Width) / gSwlWindow.Size.Height,
            // NOTE(typedef): in degree
            .Fov = 75.0f
        },

        .Spectator = {
            .Speed = 5.0f,
            .Zoom = 0
        }
    };

#endif

    CameraComponent gCameraComponent =
        camera_component_new_ext(createInfo);
    camera_component_update(&gCameraComponent);
    m4_print(gCameraComponent.Base.ViewProjection, "viewproj");
    m4_print(m4_t(gCameraComponent.Base.ViewProjection), "viewproj");

    svl_uniform_set(pInstance, pSvlPipeline, pCameraUniform, &gCameraComponent.Base.ViewProjection, 0, sizeof(m4));

    pSvlPipeline->Vertex = vbuf;
    pSvlPipeline->Index = ibuf;
    pSvlPipeline->IndicesCount = 6;

    pInstance->aRenderPasses[0].apPipelines[0] = pSvlPipeline;
    SvlRenderPass* pRenderPass = &pInstance->aRenderPasses[0];

    SvlFrame frame = {};
    SvlFrame* pFrame = &frame;
    while (gAppRun)
    {
        svl_frame_start(pInstance, &frame);

        // NOTE: Biiig stuff, processing render pass

        /*
          DOCS(typedef): Recording Commands BEGIN
        */
        VkCommandBuffer cmdBuffer = pInstance->Cmd.Buffer;
        VkCommandBufferBeginInfo commandBufferBeginInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .pNext = NULL,
            .flags = 0,
            .pInheritanceInfo = NULL,
        };
        VkResult beginCommandBufferResult =
            vkBeginCommandBuffer(cmdBuffer, &commandBufferBeginInfo);
        //vkValidResult(beginCommandBufferResult, "Begin command buffer!");

        VkClearColorValue clearColor = {0.004321, 0.00806007, 0.007, 1};
        pInstance->aRenderPasses[0].ClearColor = clearColor;
        SvlErrorType renderPassUpdate = SvlErrorType_None;
        //svl_render_pass_update(pInstance, &pInstance->aRenderPasses[0], &frame, &renderPassUpdate);

        VkCommandBuffer cmd = pInstance->Cmd.Buffer;

        VkClearValue aClearValues[2] = {
            [0] = (VkClearValue) {
                .color = pRenderPass->ClearColor, //{ 0.0034f, 0.0037f, 0.0039f, 1 }
                //.color = { 0.0934f, 0.0937f, 0.0939f, 1 }
            },
            [1] = (VkClearValue) {
                .depthStencil = { 1.0f, 0.0f }
            }
        };

        VkFramebuffer currentFramebuffer = pRenderPass->aFramebuffers[pFrame->ImageIndex];
        VkExtent2D vkExtent = pInstance->Swapchain.Resolution;

        VkRenderPassBeginInfo renderPassBeginInfo = {
            .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            .pNext = NULL,
            .renderPass = pRenderPass->RenderPass,
            .framebuffer = currentFramebuffer,
            .renderArea = {
                .offset = { 0, 0 },
                .extent = vkExtent
            },
            .clearValueCount = 2,
            .pClearValues = aClearValues
        };
        vkCmdBeginRenderPass(cmd, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

        //TODO(typedef): set in pipeline
        VkViewport viewport = {
            .x = 0,
            .y = 0,
            .width = vkExtent.width,
            .height = vkExtent.height,
            .minDepth = 0,
            .maxDepth = 1
        };
        vkCmdSetViewport(cmd, 0, 1, &viewport);

        //TODO(typedef): set in pipeline
        VkRect2D scissor = {
            .offset = { 0, 0 },
            .extent = vkExtent
        };
        vkCmdSetScissor(cmd, 0, 1, &scissor);

        vkCmdBindPipeline(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pSvlPipeline->Pipeline);
        vkCmdBindDescriptorSets(cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, pSvlPipeline->PipelineLayout, 0, 1, &pSvlPipeline->Binding.DescriptorSet, 0, NULL);
//VkPrimitiveTopology VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST
        VkDeviceSize offsets[1] = { 0 };
        vkCmdBindVertexBuffers(cmd, 0, 1, &pSvlPipeline->Vertex.Gpu, offsets);
        vkCmdBindIndexBuffer(cmd, pSvlPipeline->Index.Gpu, 0, VK_INDEX_TYPE_UINT32);

        vkCmdDrawIndexed(cmd, pSvlPipeline->IndicesCount, 1, 0, 0, 0);
        //vkCmdDraw(cmd, 3, 1, 0, 0);

        vkCmdEndRenderPass(cmd);
        VkResult endCommandBufferResult = vkEndCommandBuffer(cmdBuffer);
        // vkValidResult(endCommandBufferResult, "Can't end command buffer!");

        svl_frame_end(pInstance, &frame);

        swl_update(&gSwlWindow);
        //usleep(50);
    }


}

void
svl_on_attach_finished()
{

}

void
svl_on_update(f32 timestep)
{
}

void
svl_on_ui_render()
{

}

void
svl_on_event(Event* event)
{
    switch (event->Category)
    {

    case EventCategory_Key:
    {
        if (event->Type != EventType_KeyPressed)
            break;

        KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
        if (keyEvent->KeyCode == KeyType_Escape)
        {
            //application_close();
            event->IsHandled = 1;
        }

        break;
    }

    case EventCategory_Window:
    {
        if (event->Type == EventType_WindowShouldBeClosed)
        {
            //application_close();
        }
        break;
    }

    }
}

void
svl_on_destroy()
{
}
