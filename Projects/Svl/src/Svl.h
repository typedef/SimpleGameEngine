#ifndef SVL_H
#define SVL_H

#include <Core/Types.h>

struct Event;

void svl_on_attach();
void svl_on_attach_finished();
void svl_on_update(f32 timestep);
void svl_on_ui_render();
void svl_on_event(struct Event* event);
void svl_on_destroy();

#endif // SVL_H
