#include "AutoTest_Math.h"

#include <AutoTest/AutoTest.h>
#include <Core/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>
#include <cglm/include/cglm/cglm.h>


void
perspective_creation_test()
{
    f32 near = 0.1f,
        far = 1000.0f,
        aspect = 1920.0f / 1080.0f,
        fov = 45.0f;

    m4 pers = perspective(near, far, aspect, fov);
    mat4 glmPers;
    glm_perspective(fov, aspect, near, far, glmPers);

    m4 glmAsM4 = m4_double_array(glmPers);
    glmAsM4.M11 *= -1; // NOTE: fix for vulkan

    AutoTest_M4_Equal(pers, glmAsM4);
    AutoTest_M4_Value(pers);
    AutoTest_M4_Value(glmAsM4);
}

void
inverse_test()
{
    v3 pos = v3_new(1.24f,1.76f,5.10f);
    v3 front = v3_new(0,0, -1);
    v3 up = v3_new(0,1, 0);
    m4 view = m4_look_at(pos, v3_add(pos, front), up);

    mat4 mat = {};
    for (i32 r = 0; r < 4; ++r)
    {
        for (i32 c = 0; c < 4; ++c)
        {
            mat[r][c] = view.M[r][c];
        }
    }

    AutoTest_M4_Equal(view, m4_double_array(mat));

    mat4 invMat;
    glm_mat4_inv(mat, invMat);

    m4 iview = m4_inverse(view);
    AutoTest_M4_Value(iview);
    AutoTest_M4_Value(m4_double_array(invMat));
    AutoTest_M4_Equal(iview, m4_double_array(invMat));

}

void
auto_test_math()
{
    AutoTest_File();

    AutoTest_Func(perspective_creation_test());
    AutoTest_Func(inverse_test());
}
