#include "AutoTest_SimpleFastJson.h"

#include <AutoTest/AutoTest.h>
#include <Core/SimpleStandardLibrary.h>
#include <Utils/SimpleFastJson.h>


typedef struct SimpleTestResult
{
    i32 TokensCount;
    i32 Error;
} SimpleTestResult;


void
_auto_test_correct_in_terminal_test(const char* pPath)
{
    size_t size = 0;
    char* pText = file_read_string_ext(pPath, &size);

    size_t arenaSize = 4*size;
    i32 tokensCnt = 2*size;
    SimpleArena* pArena = simple_arena_create(arenaSize);
    memory_set_arena(pArena);
    SFJToken* aTokens = memory_allocate(tokensCnt);
    // hello comments

    SFJSetting set = {
        .pText = pText,
        .TextLength = size,
        .TokensCount = tokensCnt,
        .aTokens = aTokens
    };

    TimeState ts = {};
    profiler_start(&ts);

    sfj_parse_json(&set);

    profiler_end(&ts);

    for (i32 i = 0; i < set.TokensCount; ++i)
    {
        SFJToken token = aTokens[i];
        /* if (token.TokenType == SFJTokenType_Object) */
        /*     continue; */

        //todo: not working for root object
        if (token.Start == -1 || token.End == -1)
        {
            vassert_break();
            continue;

        }

        GINFO("T:%s:%.*s\n", sfj_token_type_to_string(token.TokenType),
              token.End - token.Start,
              set.pText + token.Start);
    }

    simple_arena_destroy(pArena);

    AutoTest_String_Value("Time");
    AutoTest_String_Value(profiler_get_string(&ts));

    AutoTest_Condition(set.TokensCount == 4);
    AutoTest_I32_Value(set.TokensCount);
}

typedef struct SfjWriteResult
{
    sfj_i8 IsError;
    const char* pMessage;
    char* pStringBuilder;
} SfjWriteResult;

SfjWriteResult
sfj_write_obj(SFJToken* aTokens, sfj_u64 tokensCount, char* pJsonText)
{
    // TokensCount: obj->EndInd - obj->StartInd + 1

    SFJToken objectToken = aTokens[0];

    sfj_i32 isNotValidFirst = (objectToken.TokenType != SFJTokenType_Object);
    if (isNotValidFirst)
    {
        SfjWriteResult wr = {
            .IsError = 1,
            .pMessage = "First token is not Object",
        };

        return wr;
    }

    sfj_i32 tabIndex = 0;
    const char* pNewLine = "\n";
#define TabStr "    "
    char* tabs[] = {
        [0] = "",
        [1] = TabStr,
        [2] = TabStr TabStr,
        [3] = TabStr TabStr TabStr,
        [4] = TabStr TabStr TabStr TabStr,
        [5] = TabStr TabStr TabStr TabStr TabStr,
        [6] = TabStr TabStr TabStr TabStr TabStr TabStr,
    };

#define GetTab(ti) ({					\
            const char*t;				\
            if(ti>=ARRAY_COUNT(tabs)){t=tabs[6];}	\
            else{t=tabs[ti];};				\
            t;						\
        })

    char* sb = NULL;

    // note: start from next token, because 1st is obj
    for (sfj_i64 i = 1; i < objectToken.EndInd; ++i)
    {
        SFJToken keyToken   = aTokens[i];
        SFJToken valueToken = aTokens[i + 1];

        // note: Validate key
        sfj_i32 isNotValideKey = (keyToken.TokenType != SFJTokenType_String_Ascii || keyToken.TokenType != SFJTokenType_String_Unicode);
        if (isNotValideKey)
        {
            SfjWriteResult wr = {.IsError = 1, .pMessage = "KeyToken is not key token :(", .pStringBuilder = sb};
            return wr;
        }

        string_builder_appendf(sb, "");

        switch (valueToken.TokenType)
        {

        case SFJTokenType_Int:
        case SFJTokenType_Int_Exp:
        {
            break;
        }

        default:
            break;

        }
    }

    SfjWriteResult wr = {.pStringBuilder = sb};
    return wr;
}

void
sfj_write(SFJToken* aTokens, sfj_u64 tokensCount, char* pJsonText)
{

    char* sb = NULL;
    sfj_i32 tabIndex = 0;
    sfj_i32 objParent = -1, arrParent = -1;
    sfj_i8 isKey = 0, isArr = 0;
    const char* pNewLine = "\n";

#define TabStr "    "
    char* tabs[] = {
        [0] = "",
            [1] = TabStr,
            [2] = TabStr TabStr,
            [3] = TabStr TabStr TabStr,
            [4] = TabStr TabStr TabStr TabStr,
            [5] = TabStr TabStr TabStr TabStr TabStr,
            [6] = TabStr TabStr TabStr TabStr TabStr TabStr,
    };

#define GetTab(ti) ({					\
            const char*t;				\
            if(ti>=ARRAY_COUNT(tabs)){t=tabs[6];}	\
            else{t=tabs[ti];};				\
            t;						\
        })

    SimpleArena* pArena = simple_arena_create(tokensCount * 100);
    memory_set_arena(pArena);

    sfj_i64 lastInd = tokensCount - 1;
    for (sfj_i64 i = 0; i < tokensCount; ++i)
    {
        SFJToken token = aTokens[i];
        SFJTokenType tokenType = token.TokenType;
        const char *pTab = GetTab(tabIndex);

        switch (tokenType)
        {

        case SFJTokenType_Object:
        {
            if (i == 0)
            {
                string_builder_appendf(sb, "{%s", pNewLine);
                ++tabIndex;
            }
            else
            {
                ++tabIndex;
                string_builder_appendf(sb, "%s%s{%s", pNewLine, pTab, pNewLine);
            }

            objParent = i;
            isKey = 1;

            break;
        }

        case SFJTokenType_Array:
        {
            isArr = 1;
            arrParent = i;
            string_builder_appends(sb, "[");

            break;
        }


        case SFJTokenType_String_Ascii:
        case SFJTokenType_String_Unicode:
        {
            char* substr = string_substring_range(pJsonText, token.Start, token.End);
            if (isKey)
            {
                string_builder_appendf(sb, "%s \"%s\": ", pTab, substr);
                isKey = 0;
            }
            else
            {
                string_builder_appendf(sb, "\"%s\",%s", substr, pNewLine);
                isKey = 1;
            }

            break;
        }

        case SFJTokenType_Int:
        case SFJTokenType_Float:
        case SFJTokenType_Float_Exp:
        case SFJTokenType_Bool_True:
        case SFJTokenType_Bool_False:
        case SFJTokenType_Null:
        {
            char* substr = string_substring_range(pJsonText, token.Start, token.End);

            sfj_i32 nextInd = i + 1;
            if (nextInd >= lastInd)
                break;

            if (token.Parent == arrParent /* isInsideArr */)
            {
                SFJToken nextToken = aTokens[nextInd];
                if (nextToken.Parent == token.Parent)
                {
                    string_builder_appendf(sb, "%s, ", substr);
                }
                else
                {
                    string_builder_appendf(sb, "%s],%s", substr, pNewLine);
                    arrParent = -1;
                    isKey = 1;
                }
            }
            else
            {
                // DOCS: value of key:value pair
                string_builder_appendf(sb, "%s,%s", substr, pNewLine);
                isKey = 1;
            }

            break;
        }

        }

        //todo: we can set flag, and apply changes on next iter
        if (objParent != -1
            && token.Parent == objParent /*isInsideArray*/
            && aTokens[i + 1].Parent != token.Parent /*parent changed*/
            && aTokens[i + 1].Parent != arrParent /*And it is not in arr*/)
        {
            tabIndex = MinMax(tabIndex - 1, 0, 6);
            const char* newTab = GetTab(tabIndex);
            string_builder_appendf(sb, "%s}%s", newTab, pNewLine);
            objParent = -1;
        }

        // if (isKey) string_builder_appends(sb, "[^]");

    }

    file_write_string(asset("/json/write_some.json"), sb, string_builder_count(sb));

    simple_arena_destroy(pArena);
    memory_set_arena(NULL);
}

void
auto_test_sfj_simple_test()
{
    //Arena* pArena = arena_create(MB(5));
    //memory_set_arena(pArena);

    size_t length = 0;
    char* pText = file_read_string_ext(asset("/json/some.json"), &length);

    SFJToken aTokens[KB(10)] = {};
    SFJSetting set = {
        .pText = pText,
        .TextLength = length,
        .TokensCount = KB(10),
        .aTokens = aTokens
    };
    TimeState ts = {};
    profiler_start(&ts);
    sfj_parse_json(&set);
    profiler_end(&ts);

    AutoTest_String_Value("Time");
    AutoTest_String_Value(profiler_get_string(&ts));
    //arena_destroy(pArena);

    //AutoTest_Condition(sizeof(sfj_i64) == 8);
    //AutoTest_Condition(sizeof(sfj_u64) == 8);
    //AutoTest_Condition(sizeof(sfj_i32) == 4);

    AutoTest_Condition(set.TokensCount == 70);
    AutoTest_I32_Value(set.TokensCount);

    for (i32 i = 0; i < set.TokensCount; ++i)
    {
        SFJToken token = aTokens[i];
        /* if (token.TokenType == SFJTokenType_Object) */
        /*     continue; */

        //todo: not working for root object
        if (token.Start == -1 || token.End == -1)
        {
            vassert_break();
            continue;

        }

        /* GINFO("T:%s:%.*s\n", sfj_token_type_to_string(token.TokenType), */
        /*       token.End - token.Start, */
        /*       set.pText + token.Start); */
        //char* str = string_substring_range(set.pText, token.Start, token.End);
        //AutoTest_String_Value(str);
    }

    sfj_write(set.aTokens, set.TokensCount, pText);

}

#define JSMN_PARENT_LINKS
#include <Utils/Jsmn.c>
#include <stdarg.h>

static SimpleTestResult
_jsmn_perf_test_callback(char* json, size_t length, void* data, i32 tokensCount)
{
    jsmntok_t* aTokens = (jsmntok_t*) data;
    jsmn_parser jsmnParser = {};
    jsmn_init(&jsmnParser);
    int rtc = jsmn_parse(&jsmnParser, json, length, aTokens, tokensCount);
    return (SimpleTestResult) {};
}

/*
  note:
  with one token parsed wo simd it's around 3629 mb/s
*/
static SimpleTestResult
_gold_solution_perf_test_callback(char* json, size_t length, void* data, i32 tokensCount)
{
    // note: defines

#define NextToken(passedPtr, passedChar)	\
    ++pos;					\
    ++passedPtr;				\
    passedChar = *passedPtr;

#define SkipTokenByPtr(passedPtr, passedChar, skipPtr)	\
    passedPtr = (skipPtr);				\
    passedChar = *passedPtr;

#define SkipDigits(ptr, c, atLeastOne)		\
    while (c >= '0' && c <= '9' && c != '\0')	\
    {						\
        NextToken(ptr, c);			\
        atLeastOne = 1;				\
    }

    char* ptr = json;
    char c = *ptr;
    sfj_i32 pos = 0;
    sfj_i32 cur = 0;

    SFJToken* aTokens = (SFJToken*) data;
    SFJToken* ptrTokens = aTokens;

    while (c != '\0')
    {
        if (c == '{')
        {
            SFJToken token = {
                .Start = pos,
                .TokenType = SFJTokenType_Object,
            };

#if 1
            *ptrTokens = token;
            ++ptrTokens;
#else
            aTokens[cur] = token;
            ++cur;
#endif
        }

        switch (c)
        {
        case '{':
        {


            break;
        }

        /* case '[': */
        /* { */
        /*     SFJToken token = { */
        /*	.Start = pos, */
        /*	.TokenType = SFJTokenType_Array, */
        /*     }; */

        /*     aTokens[cur] = token; */
        /*     ++cur; */

        /*     break; */
        /* } */

        /* case '"': */
        /* { */
        /*     NextToken(ptr, c); */

        /*     sfj_i32 startPos = pos; */

        /*     while (c != '\0') */
        /*     { */
        /*	if (c == '"') */
        /*	{ */
        /*	    SFJToken token = { */
        /*		.TokenType = SFJTokenType_String_Ascii, */
        /*		.Start = startPos, */
        /*		.End = pos, */
        /*	    }; */

        /*	    aTokens[cur] = token; */
        /*	    ++cur; */

        /*	    break; */
        /*	} */

        /*	NextToken(ptr, c); */
        /*     } */

        /*     // not sure we need this */
        /*     NextToken(ptr, c); */

        /*     break; */
        /* } */


        }

        NextToken(ptr, c);
    }

    SimpleTestResult res = {};
    return res;
}

static SimpleTestResult
_sfj_perf_test_callback(char* json, size_t length, void* data, i32 tokensCount)
{
    SFJToken* aTokens = (SFJToken*) data;

    SFJSetting set = {
        .aTokens = aTokens,
        .pText = json,
        .TextLength = length,
        .TokensCount = tokensCount
    };

    sfj_parse_json(&set);

    SimpleTestResult res = {
        .TokensCount = set.TokensCount,
        .Error = set.ErrorType,
    };

    return res;
}

typedef struct SimplePerfTestResult
{
    i32 TokensCount;
    i32 IterCount;
    i32 Error;
    f32 BytesPerSecond;
    i64 MsForAllIterations;
    const char* Time;
} SimplePerfTestResult;

static SimplePerfTestResult
_perf_test(char* pFileContent, size_t length, void* aTokens, i32 tokensCount,
           SimpleTestResult (*_perf_callback)(char* json, size_t length, void* aTokens, i32 tokensCount))
{
    TimeState ts = {};
    profiler_start(&ts);

    SimpleTestResult testRes = {};
    i64 i, cnt = 100;
    for (i = 0; i < cnt; ++i)
    {
        testRes = _perf_callback(pFileContent, length, aTokens, tokensCount);
    }

    profiler_end(&ts);

    i64 ms = profiler_get_milliseconds(&ts);
    size_t mb = MB(1);
    f32 bigNum = (1.0f * cnt * length);
    f32 bytesPerSec = bigNum / (ms/1000.0f);
    GINFO("%f Mb/s (bytesPerSec: %f)\n", (bytesPerSec / mb), bytesPerSec);

    SimplePerfTestResult res = {
        .TokensCount = testRes.TokensCount,
        .IterCount = 100,
        .Error = testRes.Error,
        .BytesPerSecond = bytesPerSec,
        .MsForAllIterations = ms,
        .Time = profiler_get_string(&ts),
    };

    return res;
}

void
_auto_test_file_perf(const char* pPath, i32 isJsmn)
{
    size_t size;
    char* pFileContent = file_read_string_ext(pPath, &size);

    size_t arenaSize = 4*size;
    i32 tokensCnt = 2*size;

    SimplePerfTestResult res = {};

    SimpleArena* pArena = simple_arena_create(arenaSize);
    memory_set_arena(pArena);

    if (isJsmn == 1)
    {
        // it's for cityslots
        // 392.357422 Mb/s (bytesPerSec: 411416576.000000)
        // 404.873657 Mb/s (bytesPerSec: 424540800.000000)
        // 1 021 016 900

        jsmntok_t* aTokens = memory_allocate(tokensCnt * sizeof(jsmntok_t));
        res = _perf_test(pFileContent, size, aTokens, tokensCnt, _jsmn_perf_test_callback);
        simple_arena_destroy(pArena);
    }
    else if (isJsmn == 2)
    {
        SFJToken* aTokens = memory_allocate(tokensCnt);
        res = _perf_test(pFileContent, size, aTokens, tokensCnt, _gold_solution_perf_test_callback);
        simple_arena_destroy(pArena);

        /* for (i32 i = 0; i < tokensCnt; ++i) */
        /* { */
        /*     SFJToken token = aTokens[i]; */
        /* } */
    }
    else
    {
        // it's for cityslots

        // 845.376343 Mb/s (bytesPerSec: 886441344.000000)
        // 930.522461 Mb/s (bytesPerSec: 975723520.000000)
        // 928.422241 Mb/s (bytesPerSec: 973521280.000000)
        // 920.770325 Mb/s (bytesPerSec: 965497664.000000)
        // 1561.575684 Mb/s (bytesPerSec: 1637430784.000000)
        // 1566.983765 Mb/s (bytesPerSec: 1643101568.000000)
        // full toks working 1265.463745 Mb/s (bytesPerSec: 1326934912.000000)
        // 82 623 100 -> 572 537 400

        SFJToken* aTokens = memory_allocate(tokensCnt);
        res = _perf_test(pFileContent, size, aTokens, tokensCnt, _sfj_perf_test_callback);
        simple_arena_destroy(pArena);
    }


    char buf[128] = {};
    string_format(buf, "%f Mb/s (bytesPerSec: %f, t:%s)\n", (res.BytesPerSecond / MB(1)), res.BytesPerSecond, res.Time);
    char* abuf = string(buf);
    AutoTest_String_Value(abuf);

    if (isJsmn)
    {
        AutoTest_I32_Value(jsmn_get_i32());
    }
    else
    {
        AutoTest_String_Value("### SFJ ###");
        AutoTest_I32_Value(sfj_get_i32());
    }

}

void
auto_test_sfj_base_test()
{

    const i32 is_profile = 0;
    const i32 is_jsmn = 0;

#define FILE_CITY_SLOTS_180_MB 0
#define FILE_25_MB 0
#define SIMPLE_MEN_FILE_35_KB 1
#define NANO_JSON_FILE_476_B 0
#define TWITTER_JSON_FILE_617_KB 0


#if 0
#error "err"
#elif FILE_25_MB == 1
    // not working indecies not set for token
    const char* pPath = asset("/json/25mb.json");
#elif FILE_CITY_SLOTS_180_MB
    const char* pPath = asset("/json/citylots.json");
#elif SIMPLE_MEN_FILE_35_KB
    const char* pPath = asset("/json/SimpleMen.json");
#elif NANO_JSON_FILE_476_B
    const char* pPath = asset("/json/nanojsonc_example.json");
#elif TWITTER_JSON_FILE_617_KB
    // not working obj.End == -1
    const char* pPath = asset("/json/twitter.json");
#else
    const char* pPath = asset("/json/some.json");
#endif

    if (is_profile)
        _auto_test_file_perf(pPath, is_jsmn);
    else
        _auto_test_correct_in_terminal_test(pPath);

}

void
auto_test_sfj_test()
{
    AutoTest_File();

    // note: for correctness
    AutoTest_Func(auto_test_sfj_simple_test());

    //note: for perf test
    //AutoTest_Func(auto_test_sfj_base_test());
}
