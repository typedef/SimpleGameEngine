#include "AutoTest_SimpleJson.h"

#include <AutoTest/AutoTest.h>
#include <Utils/SimpleJson.h>
#include <Utils/SimpleJson.c>
#include <Core/SimpleStandardLibrary.h>


char*
_auto_test_get_file_path(const char* relativePath)
{
    char* path;

    const char* curr = path_get_current_directory();
    if (string_index_of_string(curr, "Projects") != -1)
    {
        path = path_combine("Data/SimpleJson/", relativePath);
        //GINFO("Global\n");
    }
    else
    {
        path = path_combine("Projects/AutoTest/Data/SimpleJson/", relativePath);
        //GINFO("Inside\n");
    }

    return path;
}



void
auto_test_success_test()
{
    AutoTest_Condition(1 == 1);
}

void
_auto_test_lexer_test_helper(SimpleJsonTokenType* aValid,
                             size_t validCount,
                             SimpleJsonToken* aResult,
                             size_t resultCount,
                             i32 printTokens)
{
    i32 isValid = 1;

    AutoTest_I32_Equal(validCount, resultCount);
    //AutoTest_Condition(tokensCount == validTokensCount);

    if (validCount == resultCount)
    {
        for (i32 i = 0; i < validCount; ++i)
        {
            SimpleJsonTokenType validToken = aValid[i];
            SimpleJsonTokenType token = aResult[i].Type;

            if (validToken != token)
            {
                isValid = 0;
                break;
            }
        }

        AutoTest_Condition(isValid);
    }

    if (printTokens)
    {
        array_foreach(aResult,
                      char* str = (char*) simple_json_token_type_to_string(item.Type);
                      AutoTest_String_Value(str);

                      if (item.Type == SimpleJsonTokenType_String)
                      {
                          char* strData = (char*) item.Data.pData;
                          AutoTest_String_Value(strData);
                      });
    }

}

void
auto_test_simple_json_lexer_proccess_test()
{
    char* path = _auto_test_get_file_path("object-nested-0.json");
    size_t inputLength;
    char* inputText = file_read_string_ext(path, &inputLength);

    AutoTest_Condition(inputLength > 0);

    SimpleJsonToken* tokens =
        simple_json_lexer_proccess((char*)inputText, inputLength);

    i64 tokensCount = array_count(tokens);
    AutoTest_I32_Value(tokensCount);

    SimpleJsonTokenType validTokens[] = {
        SimpleJsonTokenType_Object_Open,

        SimpleJsonTokenType_String,
        SimpleJsonTokenType_String,

        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Int,

        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Float,

        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Array_Open,
        SimpleJsonTokenType_Int,
        SimpleJsonTokenType_Int,
        SimpleJsonTokenType_Array_Close,

        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Object_Open,
        // "ke0": "val"
        SimpleJsonTokenType_String,
        SimpleJsonTokenType_String,
        // "key1": 125
        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Int,
        // "key2: [1,2]"
        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Array_Open,
        SimpleJsonTokenType_Int,
        SimpleJsonTokenType_Int,
        SimpleJsonTokenType_Array_Close,
        // }
        SimpleJsonTokenType_Object_Close,

        SimpleJsonTokenType_Object_Close,

    };

    i32 isValid = 1;
    i64 validTokensCount = ARRAY_COUNT(validTokens);

    AutoTest_I32_Equal(tokensCount, validTokensCount);
    //AutoTest_Condition(tokensCount == validTokensCount);

    _auto_test_lexer_test_helper(validTokens,
                                 validTokensCount,
                                 tokens,
                                 array_count(tokens),
                                 1);
}

void
auto_test_simple_json_object_lexer_test()
{
    char* path = _auto_test_get_file_path("object-nested-0.json");

    size_t fileLength = 0;
    char* fileContent = file_read_string_ext(path, &fileLength);
    GINFO("%s\n", path_get_current_directory());
    vguard_not_null(fileContent);

    i64 inputLen = string_length(fileContent);
    SimpleJsonToken* tokens =
        simple_json_lexer_proccess(fileContent, fileLength);

    SimpleJsonTokenType validTokens[] = {
        // {
        SimpleJsonTokenType_Object_Open,

        //"key0": "value0",
        SimpleJsonTokenType_String,
        SimpleJsonTokenType_String,

        // "key1": 123,
        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Int,

        // "key2": 3.14,
        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Float,

        // "key3": [0,1],
        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Array_Open,
        SimpleJsonTokenType_Int,
        SimpleJsonTokenType_Int,
        SimpleJsonTokenType_Array_Close,

        // "key4": { "key0": "val", "key1": 125, "key2": [1,2] }
        SimpleJsonTokenType_String,

        SimpleJsonTokenType_Object_Open,

        SimpleJsonTokenType_String,
        SimpleJsonTokenType_String,
        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Int,
        SimpleJsonTokenType_String,
        SimpleJsonTokenType_Array_Open,
        SimpleJsonTokenType_Int,
        SimpleJsonTokenType_Int,
        SimpleJsonTokenType_Array_Close,

        SimpleJsonTokenType_Object_Close,

        // }
        SimpleJsonTokenType_Object_Close,

    };

    _auto_test_lexer_test_helper(validTokens,
                                 ARRAY_COUNT(validTokens),
                                 tokens,
                                 array_count(tokens),
                                 0);

}

void
auto_test_parser_simple_test_wo_obj()
{
    char* path = _auto_test_get_file_path("simple-parser.json");
    SimpleJson json = simple_json_parse_file(path);

    SimpleJsonValue val0 = shash_get(json.Root, "key0");
    SimpleJsonValue val1 = shash_get(json.Root, "key1");
    SimpleJsonValue val2 = shash_get(json.Root, "key2");
    SimpleJsonValue val3 = shash_get(json.Root, "key3");
    SimpleJsonValue val4 = shash_get(json.Root, "key4");
    SimpleJsonValue val5 = shash_get(json.Root, "key5");
    char* valStr = (char*) (val5.Data.pData == NULL ? "NULL" : "!NULL");

    /*
      {
      key0: 123,
      key1: 3.14,
      key2: "Hello, world",
      key3: true,
      key4: false,
      key5: null,
      }
    */
    AutoTest_I32_Value(val0.Data.I64Value);
    AutoTest_F32_Value(val1.Data.F64Value);
    AutoTest_String_Value(val2.Data.pData);
    AutoTest_I32_Value(val3.Data.BoolValue);
    AutoTest_I32_Value(val4.Data.BoolValue);
    AutoTest_String_Value(valStr);

}

void
auto_test_array_parser_test()
{
    /*

      {
        key0: "value0",
        key1: 123,
        key2: 3.14,
        key3: [0, 1],
        key4: [3.14, 1.0123, 2.34],
        key5: [ "Hello, world!", "Lalalal", "Other line of string"],
        key6: [ true, false, true],
      }

    */

    char* path = _auto_test_get_file_path("array.json");
    SimpleJson json = simple_json_parse_file(path);

    SimpleJsonValue val0 = shash_get(json.Root, "key0");
    SimpleJsonValue val1 = shash_get(json.Root, "key1");
    SimpleJsonValue val2 = shash_get(json.Root, "key2");
    SimpleJsonValue val3 = shash_get(json.Root, "key3");
    SimpleJsonValue val4 = shash_get(json.Root, "key4");
    SimpleJsonValue val5 = shash_get(json.Root, "key5");
    SimpleJsonValue val6 = shash_get(json.Root, "key6");

    AutoTest_String_Value(val0.Data.pData);
    AutoTest_I32_Value(val1.Data.I64Value);
    AutoTest_F32_Value(val2.Data.F64Value);

    AutoTest_Condition(val3.Type == SimpleJsonValueType_Array_Int);
    if (val3.Type == SimpleJsonValueType_Array_Int)
    {
        i64* arr = (i64*) val3.Data.pData;
        //array_foreach(arr, AutoTest_I32_Value(item));
    }

    AutoTest_Condition(val4.Type == SimpleJsonValueType_Array_Float);
    if (val4.Type == SimpleJsonValueType_Array_Float)
    {
        f64* arr = (f64*) val4.Data.pData;
        array_foreach(arr, AutoTest_F32_Value(item));
    }
    AutoTest_Condition(array_count(val4.Data.pData) > 0);
    AutoTest_I32_Value(array_count(val4.Data.pData));

    AutoTest_Condition(val5.Type == SimpleJsonValueType_Array_String);
    if (val5.Type == SimpleJsonValueType_Array_String)
    {
        char** arr = (char**) val5.Data.pData;
        array_foreach(arr, AutoTest_String_Value(item));
    }
    AutoTest_Condition(array_count(val5.Data.pData) > 0);
    AutoTest_I32_Value(array_count(val5.Data.pData));

    AutoTest_Condition(val6.Type == SimpleJsonValueType_Array_Bool);
    if (val6.Type == SimpleJsonValueType_Array_Bool)
    {
        i8* arr = (i8*) val6.Data.pData;
        array_foreach(arr, AutoTest_I32_Value(item));
    }

}

// DOCS: Test that parser end parsing object with } symbol
void
auto_test_object_end()
{

    char* path = _auto_test_get_file_path("object-end.json");
    SimpleJson json = simple_json_parse_file(path);

    SimpleJsonValue val0 = shash_get(json.Root, "key0");
    AutoTest_String_Value(val0.Data.pData);
}

void
_auto_test_nested_obj_parser_test(char* path)
{
    SimpleJson json = simple_json_parse_file(path);

    SimpleJsonValue val0 = shash_get(json.Root, "key0");
    SimpleJsonValue val1 = shash_get(json.Root, "key1");
    SimpleJsonValue val2 = shash_get(json.Root, "key2");
    SimpleJsonValue val3 = shash_get(json.Root, "key3");
    SimpleJsonValue val4 = shash_get(json.Root, "key4");

    char* v0str = (char*) val0.Data.pData;
    AutoTest_String_Value(v0str);
    AutoTest_String_Equal(v0str, "value0");
    AutoTest_I32_Value(val1.Data.I64Value);
    AutoTest_I32_Equal(val1.Data.I64Value, 123);
    AutoTest_F32_Value(val2.Data.F64Value);
    AutoTest_F32_Equal(val2.Data.F64Value, 3.14);

    AutoTest_Condition(val3.Type == SimpleJsonValueType_Array_Int);
    if (val3.Type == SimpleJsonValueType_Array_Int)
    {
        i64* arr = (i64*)val3.Data.pData;
        AutoTest_Condition(arr[0] == 0);
        AutoTest_Condition(arr[1] == 1);
    }

    if (val4.Type == SimpleJsonValueType_Object)
    {
        SimpleJsonObject* nestObj = (SimpleJsonObject*) val4.Data.pData;
        SimpleJsonValue nv0 = shash_get(nestObj, "key0");
        SimpleJsonValue nv1 = shash_get(nestObj, "key1");
        SimpleJsonValue nv2 = shash_get(nestObj, "key2");

        AutoTest_String_Equal(nv0.Data.pData, "val");
        AutoTest_I32_Equal(nv1.Data.I64Value, 125);

        i64* arr = (i64*) nv2.Data.pData;
        AutoTest_I32_Equal(arr[0], 1);
        AutoTest_I32_Equal(arr[1], 2);

    }
}

void
auto_test_nested_obj_parser_test_0()
{
    /*
      {
        key0: "value0",
        key1: 123,
        key2: 3.14,
        key3: [0, 1],
        key4: {
            key0: "val",
            key1: 125,
            key2: [1,2]
        }
       }
    */

    char* path = _auto_test_get_file_path("object-nested-0.json");
    _auto_test_nested_obj_parser_test(path);

}

void
auto_test_nested_obj_parser_test_1()
{
    /*

      {
        key0: "value0",
        key1: 123,
        key3: [0, 1],
        key4: {
          key0: "val",
          key1: 125,
          key2: [1,2]
        },
        key2: 3.14,
      }

    */

    char* path = _auto_test_get_file_path("object-nested-1.json");
    _auto_test_nested_obj_parser_test(path);

}

void
auto_test_nested_obj_parser_test_2()
{
    /*

      {
         key4: {
           key0: "val",
           key1: 125,
           key2: [1,2]
         },
         key0: "value0",
         key1: 123,
         key3: [0, 1],
         key2: 3.14,
      }

    */

    char* path = _auto_test_get_file_path("object-nested-2.json");
    _auto_test_nested_obj_parser_test(path);

}

void
auto_test_arr_obj_test()
{
    char* path = _auto_test_get_file_path("array_obj.json");

    i32 isFileExist = path_is_file_exist(path);
    if (!isFileExist)
    {
        AutoTest_Condition(isFileExist);
        return;
    }

    SimpleJson json = simple_json_parse_file(path);

    SimpleJsonValue val0 = shash_get(json.Root, "key0");
    SimpleJsonValue val1 = shash_get(json.Root, "key1");

    AutoTest_I32_Value(val0.Data.I64Value);
    AutoTest_I32_Equal(val0.Data.I64Value, 1024);

    if (val1.Type == SimpleJsonValueType_Array_Object)
    {
        SimpleJsonObject** apObj = (SimpleJsonObject**) val1.Data.pData;

        SimpleJsonObject* pObj0 = apObj[0];
        SimpleJsonValue o1v0 = shash_get(pObj0, "key0");
        SimpleJsonValue o1v1 = shash_get(pObj0, "key1");

        AutoTest_Condition(o1v0.Type == SimpleJsonValueType_String);
        if (o1v0.Type == SimpleJsonValueType_String)
        {
            AutoTest_String_Equal(((char*)o1v0.Data.pData), "val0");
        }
        AutoTest_Condition(o1v1.Type == SimpleJsonValueType_String);
        if (o1v1.Type == SimpleJsonValueType_String)
        {
            AutoTest_String_Equal(((char*)o1v1.Data.pData), "val1");
        }

        SimpleJsonObject* pObj1 = apObj[1];

        SimpleJsonValue o2v0 = shash_get(pObj1, "key0");
        SimpleJsonValue o2v1 = shash_get(pObj1, "key1");

        AutoTest_Condition(o2v0.Type == SimpleJsonValueType_String);
        if (o2v0.Type == SimpleJsonValueType_String)
        {
            AutoTest_String_Equal(((char*)o2v0.Data.pData), "string");
        }
        AutoTest_Condition(o2v1.Type == SimpleJsonValueType_String);
        if (o2v1.Type == SimpleJsonValueType_String)
        {
            AutoTest_String_Equal(((char*)o2v1.Data.pData), "other str");
        }

    }

}

void
auto_test_arr_obj_with_utf8_test()
{
    char* path = asset("/json/twitter.json");

    i32 isFileExist = path_is_file_exist(path);
    if (!isFileExist)
    {
        AutoTest_Condition(isFileExist);
        return;
    }

    SimpleJson json = simple_json_parse_file(path);

    /* SimpleJsonValue val0 = shash_get(json.Root, "key0"); */
    /* SimpleJsonValue val1 = shash_get(json.Root, "key1"); */

    /* AutoTest_I32_Value(val0.Data.I64Value); */
    /* AutoTest_I32_Equal(val0.Data.I64Value, 1024); */

    /* if (val1.Type == SimpleJsonValueType_Array_Object) */
    /* { */
    /*	SimpleJsonObject** apObj = (SimpleJsonObject**) val1.Data.pData; */

    /*	SimpleJsonObject* pObj0 = apObj[0]; */
    /*	SimpleJsonValue o1v0 = shash_get(pObj0, "key0"); */
    /*	SimpleJsonValue o1v1 = shash_get(pObj0, "key1"); */

    /*	AutoTest_Condition(o1v0.Type == SimpleJsonValueType_String); */
    /*	if (o1v0.Type == SimpleJsonValueType_String) */
    /*	{ */
    /*	    AutoTest_String_Equal(((char*)o1v0.Data.pData), "val0"); */
    /*	} */
    /*	AutoTest_Condition(o1v1.Type == SimpleJsonValueType_String); */
    /*	if (o1v1.Type == SimpleJsonValueType_String) */
    /*	{ */
    /*	    AutoTest_String_Equal(((char*)o1v1.Data.pData), "val1"); */
    /*	} */

    /*	SimpleJsonObject* pObj1 = apObj[1]; */

    /*	SimpleJsonValue o2v0 = shash_get(pObj1, "key0"); */
    /*	SimpleJsonValue o2v1 = shash_get(pObj1, "key1"); */

    /*	AutoTest_Condition(o2v0.Type == SimpleJsonValueType_String); */
    /*	if (o2v0.Type == SimpleJsonValueType_String) */
    /*	{ */
    /*	    AutoTest_String_Equal(((char*)o2v0.Data.pData), "string"); */
    /*	} */
    /*	AutoTest_Condition(o2v1.Type == SimpleJsonValueType_String); */
    /*	if (o2v1.Type == SimpleJsonValueType_String) */
    /*	{ */
    /*	    AutoTest_String_Equal(((char*)o2v1.Data.pData), "other str"); */
    /*	} */

    /* } */

}


void
auto_test_simple_json_write()
{
    char* path = _auto_test_get_file_path("array_obj.json");
    SimpleJson json = simple_json_parse_file(path);

    SimpleJsonWriteSettings set = {
        .Path = "simple_test.json",
        .WriteType = SimpleJsonWriteType_Extended,
    };
    simple_json_write_file(&json, set);
}

void
auto_test_simple_json_test()
{
    AutoTest_File();

#if 0
    AutoTest_Func(auto_test_success_test());

    // DOCS: Lexer test's
    AutoTest_Func(auto_test_simple_json_lexer_proccess_test());
    AutoTest_Func(auto_test_simple_json_object_lexer_test());

    // DOCS: Parser test's
    AutoTest_Func(auto_test_parser_simple_test_wo_obj());

    AutoTest_Func(auto_test_array_parser_test());
    AutoTest_Func(auto_test_object_end());

    AutoTest_Func(auto_test_nested_obj_parser_test_0());
    AutoTest_Func(auto_test_nested_obj_parser_test_1());
    AutoTest_Func(auto_test_nested_obj_parser_test_2());

    AutoTest_Func(auto_test_arr_obj_test());

#endif

    AutoTest_Func(auto_test_arr_obj_with_utf8_test());

    // AutoTest_Func(auto_test_simple_json_write());
}
