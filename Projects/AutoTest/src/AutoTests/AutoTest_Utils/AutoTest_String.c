#include "AutoTest_String.h"

#include <AutoTest/AutoTest.h>
#include <Core/SimpleStandardLibrary.h>

void
string_length_test()
{
    AutoTest_Condition(string_length("string") == 6);
}

void
string_to_f32_test()
{
    f32 f32num = string_to_f32("3.14f");
    AutoTest_F32_Equal(f32num, 3.14);
}

void
auto_test_string()
{
    AutoTest_File();

    AutoTest_Func(string_length_test());

}
