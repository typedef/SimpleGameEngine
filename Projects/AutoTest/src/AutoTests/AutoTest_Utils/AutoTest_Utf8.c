#include "AutoTest_Utf8.h"

#include <AutoTest/AutoTest.h>
#include <Core/SimpleStandardLibrary.h>

void
single_char_check(u32 codepoint)
{
    char utf8Buf[62] = {};
    i32 bytesCnt = simple_core_utf8_encode(utf8Buf, codepoint);

    AutoTest_String_Value(utf8Buf);
    //printf("Encoded: %d %s(%d)\n", bytesCnt, utf8Buf, codepoint);

    i32 bc;
    u32 decoded = simple_core_utf8_decode(utf8Buf, &bc);
    //printf("Decoded: %d (%d bytes)\n", decoded, bc);

    AutoTest_I32_Equal(codepoint, decoded);
}

void
utf8_base_octet_test()
{
    u32 codepoint = 0x042F; // я - 2 byte
    single_char_check(codepoint);
    codepoint = (u32)'a'; // a - 1 byte
    single_char_check(codepoint);
    codepoint = 0x2000; // 3 byte
    single_char_check(codepoint);
    codepoint = 0x00010001; // 4 byte
    single_char_check(codepoint);
}

void
check_range(u32 start, u32 end)
{
    i32 result = 1;

    i32 iter = 0;

    for (i32 i = start; i < end; ++i)
    {
        char utf8Buf[32] = {};
        i32 bytesCnt = simple_core_utf8_encode(utf8Buf, i);
        i32 bc;
        u32 decoded = simple_core_utf8_decode(utf8Buf, &bc);

        i32 isSuccess = (i == decoded);
        if (!isSuccess)
        {
            result = 0;
            break;
        }

        ++iter;
    }

    AutoTest_I32_Equal(result, 1);
    AutoTest_I32_Value(iter); // 63487
}

void
utf8_1_byte_range()
{
    check_range(0, 0x7F);
}

void
utf8_2_byte_range()
{
    check_range(0x80, 0x7FF);
}

void
utf8_3_byte_range()
{
    check_range(0x800, 0xFFFF);
}

void
utf8_4_byte_range()
{
    check_range(0x10000, 0x10FFFF);
}

void
utf8_two_byte_phrase()
{
    u32 phrase[] = {
        // "Привет"
        0x41F, 0x440, 0x438, 0x432, 0x435, 0x442, // 6 * 2b
        // ", " // 2b
        0x002C, 0x0020,
        // "Мир"
        0x41C, 0x438, 0x440, // 3 * 2b
        // "!"
        0x0021 // 1b
    };

    char utf8Buf[64] = {};
    char* ptr = utf8Buf;

    for (i32 i = 0; i < ARRAY_COUNT(phrase); ++i)
    {
        u32 codepoint = phrase[i];
        i32 bytesCnt = simple_core_utf8_encode(ptr, codepoint);
        ptr += bytesCnt;
    }

    AutoTest_String_Value(utf8Buf);

    i64 len = simple_core_utf8_length(utf8Buf);
    size_t size = simple_core_utf8_size(utf8Buf);
    AutoTest_I32_Equal(len, 12);
    AutoTest_I32_Value(len);
    AutoTest_I32_Equal(size, 21);
    size_t sizeOfPrase = simple_core_utf8_unicode_size(phrase, ARRAY_COUNT(phrase));
    AutoTest_I32_Equal(sizeOfPrase, 21);
    AutoTest_I32_Value(size);

}

void
auto_test_utf8_test()
{
    AutoTest_File();
    AutoTest_Func(utf8_base_octet_test());

    // range test's
    AutoTest_Func(utf8_1_byte_range());
    AutoTest_Func(utf8_2_byte_range());
    AutoTest_Func(utf8_3_byte_range());
    AutoTest_Func(utf8_4_byte_range());

    AutoTest_Func(utf8_two_byte_phrase());

}
