void
register_key_bindings()
{
#if 0
    {
	BindingElement bind = {
	    .KeyCode = KeyType_S,
	    .Action = ActionType_Press,
	    .Modificator = ModType_Control
	};
	BindingElement* binds = NULL;
	array_push(binds, bind);

	KeyBindingCreationRecord saveRec = {
	    .Major = {
		.KeyCode = KeyType_X,
		.Action = ActionType_Press,
		.Modificator = ModType_Control
	    },
	    .Minor = minor_binding_create(wide_string(L"Сохранить сцену"),
					  scene_save,
					  binds)
	};

	key_binding_register(saveRec);
    }

    {
	BindingElement bind = {
	    .KeyCode = KeyType_O,
	    .Action = ActionType_Press,
	    .Modificator = ModType_Control
	};
	BindingElement* binds = NULL;
	array_push(binds, bind);

	KeyBindingCreationRecord openRec = {
	    .Major = {
		.KeyCode = KeyType_X,
		.Action = ActionType_Press,
		.Modificator = ModType_Control
	    },
	    .Minor = minor_binding_create(wide_string(L"Открыть сцену"),
					  scene_load,
					  binds)
	};

	key_binding_register(openRec);
    }

    {
	KeyBindingCreationRecord openRec = {
	    .Major = {
		.KeyCode = KeyType_O,
		.Action = ActionType_Press,
		.Modificator = ModType_Control
	    },
	    .Minor = minor_binding_create(wide_string(L"Тестирование одиночного keybinding'а"),
					  signle_key_binding_callback,
					  NULL)
	};

	key_binding_register(openRec);
    }
#endif
}


void
simple_key_binding_test()
{

}
