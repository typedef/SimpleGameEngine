#include "AutoTest_Ecs.h"

#include <AutoTest/AutoTest.h>
#include <AssetManager/AssetManager.h>
#include <Core/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>
#include <Model/StaticModel.h>

#include <EntitySystem/SimpleEcs.h>
#include <EntitySystem/SimpleEcsTypes.h>
#include <EntitySystem/Components/Base/CameraComponent.h>
#include <EntitySystem/Components/Base/TransformComponent.h>
#include <EntitySystem/Components/3D/StaticModelComponent.h>


typedef struct TempModelComponent
{
    i64* Pointer;
} TempModelComponent;

static EcsWorld*
_create_world()
{
    EcsWorld* pEcsWorld = ecs_world_create();
    ecs_world_register_component(pEcsWorld, CameraComponent);
    ecs_world_register_component(pEcsWorld, TransformComponent);
    ecs_world_register_component(pEcsWorld, TempModelComponent);

    {
        EntityId entityId = ecs_entity("Red Cube Entity");
        TransformComponent tc = TransformComponent_Position(1, -3, 0);
        ecs_entity_add_component(entityId, TransformComponent, tc);
        TempModelComponent tmc = {};
        ecs_entity_add_component(entityId, TempModelComponent, tmc);
    }

    {
        EntityId entityId = ecs_entity("Cube Entity");
        TransformComponent tc = TransformComponent_Position(1, 3, 0);
        ecs_entity_add_component(entityId, TransformComponent, tc);
        TempModelComponent tmc = {};
        ecs_entity_add_component(entityId, TempModelComponent, tmc);
    }

    {
        EntityId entityId = ecs_entity("Gizmo Pointer Entity");
        TransformComponent tc = TransformComponent_Position(2, -1, 0);
        ecs_entity_add_component(entityId, TransformComponent, tc);
        TempModelComponent tmc = {};
        ecs_entity_add_component(entityId, TempModelComponent, tmc);
    }

    return pEcsWorld;
}

void
_test_signle_entity(EntityId id, TransformComponent actualTc, TempModelComponent tmc)
{
    TransformComponent* pTc = GetComponent(id, TransformComponent);
    TempModelComponent* pTe = GetComponent(id, TempModelComponent);

    AutoTest_M4_Equal(pTc->Matrix, actualTc.Matrix);
    //AutoTest_M4_Value(pTc->Matrix);
    //AutoTest_M4_Value(actualTc.Matrix);
}


void
base_test()
{
    EcsWorld* pEcsWorld = _create_world();

    i64 cameraRegistered = shash_geti(pEcsWorld->CStorage.ComponentsToID, ToString(CameraComponent));
    AutoTest_Condition(cameraRegistered != -1);
    i64 transformRegistered = shash_geti(pEcsWorld->CStorage.ComponentsToID, ToString(TransformComponent));
    AutoTest_Condition(transformRegistered != -1);
    i64 tempModelComponentRegistered = shash_geti(pEcsWorld->CStorage.ComponentsToID, ToString(TempModelComponent));
    AutoTest_Condition(tempModelComponentRegistered != -1);

    EcsQuery query = ecs_get_query_w_cache(TransformComponent, TempModelComponent);

    i64 count = array_count(query.Entities);
    AutoTest_Condition(count == 3);

    // Check if comp's exist
    for (i64 i = 0; i < count; ++i)
    {
        EntityRecord rec = query.Entities[i];
        EntityId entity = rec.Id;

        AutoTest_I32_Value(entity);
        AutoTest_Condition(HasComponent(entity, TransformComponent));
        AutoTest_Condition(HasComponent(entity, TempModelComponent));

        //ecs_entity_get_component(rec.Id);
    }

    AutoTest_Condition(array_count(query.Entities) == 3);

    if (array_count(query.Entities) != 3)
        return;

#if 1
    { // NOTE: 1-st
        TransformComponent tc = TransformComponent_Position(1, -3, 0);
        TempModelComponent tmc = {};
        _test_signle_entity(query.Entities[0].Id, tc, tmc);
    }
#endif

    { // NOTE: 2-nd
        TransformComponent tc = TransformComponent_Position(1, 3, 0);
        TempModelComponent tmc = {};
        _test_signle_entity(query.Entities[1].Id, tc, tmc);
    }

#if 1
    { // NOTE: 3-rd
        EntityRecord thirdEntity = query.Entities[2];
        TransformComponent tc = TransformComponent_Position(2, -1, 0);
        TempModelComponent tmc = {};
        _test_signle_entity(query.Entities[2].Id, tc, tmc);
    }
#endif

}

void
auto_test_ecs_test()
{
    AutoTest_File();

    AutoTest_Func(base_test());
}
