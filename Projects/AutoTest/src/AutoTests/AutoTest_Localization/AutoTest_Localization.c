void
auto_test_localization_test()
{
#if IF_CODE_NOT_REFACTORED
    SimpleLocalization local =
	simple_localization_new(
	    asset_localization(
		/* "ru_RU.slf" */
		"file.slfb"
		));

    SimpleLocalizationData localData =
	simple_localization_load_data(local, asset_localization("ru_RU.slf"));

#if !defined(TEST_LOCAL)
    i32 l, localizedCount = array_count(localData.LocalizedValues);
    for (l = 0; l < localizedCount; ++l)
    {
	GINFO("[%d] %s\n", l, local.GroupNames[l]);
	WideString* wstr = localData.LocalizedValues[l];
	array_foreach(wstr,
		      GWARNING("[%d] ", i/* , sl.Keys[l][i] */);
		      wide_string_print_line(item);
	    );
    }
#endif

#if !defined(TEST_LOCAL)
    simple_localization_code_gen_generate(local, localData);
#endif

#if defined(TEST_LOCAL)
    simple_localization_set(local, localData);
    WideString wstr = simple_localization_get(LocalGroup_EngineUi, EngineUi_StopButton);
    GINFO("INFO!\n");
    wide_string_print_line(wstr);
#endif

#endif
}
