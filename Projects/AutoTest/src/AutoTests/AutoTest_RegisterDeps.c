#include "AutoTest_RegisterDeps.h"

#include <AssetManager/AssetManager.h>
#include <AutoTest/AutoTest.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Core/Types.h>

static i32 gAmInit = 1;
static i32 gVsaInit = 1;


static i32
asset_manager_init()
{
    if (!gAmInit)
        return 0;

    asset_manager_create();
    return 1;
}

static i32
vsa_init()
{
    if (!gVsaInit)
        return 0;

    VsaSettings vsaSettings = {
        .IsDebugEnabled = 1,
        .IsVSyncEnabled = 1,
        .SamplesCount = 8,
    };

    vsa_core_init(vsaSettings);

    return 1;
}

void
auto_test_register_deps()
{
    AutoTestDependency amDep = {
        .Name = "AM",
        .OnInitialize = asset_manager_init
    };
    i32 code = _auto_test_register_dependency(amDep);
    vguard(code == 1 && "Init AM!");

    AutoTestDependency vsaDep = {
        .Name = "Vsa",
        .OnInitialize = vsa_init
    };
    code = _auto_test_register_dependency(vsaDep);
    vguard(code == 1 && "Init Vsa!");


}
