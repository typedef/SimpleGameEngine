#include "TestRunner.h"

#include <AutoTest/AutoTest.h>
#include <AutoTest/AutoTestUi.h>

#include "AutoTest_RegisterDeps.h"

// Utils
#include "AutoTest_Core/AutoTest_Array.h"
#include "AutoTest_Utils/AutoTest_String.h"
#include "AutoTest_Utils/AutoTest_Math.h"
#include "AutoTest_Utils/AutoTest_SimpleJson.h"
#include "AutoTest_Utils/AutoTest_SimpleFastJson.h"
#include "AutoTest_Utils/AutoTest_Utf8.h"

#include "AutoTest_EntitySystem/AutoTest_Ecs.h"
#include "AutoTest_Core/AutoTest_SimpleSmallEcs.h"
#include "Other/DetDemo.h"

#include <stdlib.h>
void
test_runner_run()
{
    auto_test_init(MB(1));

    // todo: uncomment for engine test's
    // auto_test_register_deps();

    auto_test_array();
    //auto_test_sfj_test();

    /* auto_test_string(); */
    /* auto_test_math(); */
    //auto_test_simple_json_test();
    //auto_test_utf8_test();
    //det_demo_run();
    /* auto_test_ecs_test(); */

    //auto_test_simple_small_ecs_test();

    AutoTestGlobal* pAutoTestGlobal = auto_test_get();
    auto_test_ui_draw_file_list(pAutoTestGlobal->aFiles);
}
