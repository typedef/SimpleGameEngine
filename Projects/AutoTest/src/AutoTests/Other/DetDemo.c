#include "DetDemo.h"

#include <Core/SimpleStandardLibrary.h>

typedef enum DetKillingType
{
    DetKillingType_Stabbed = 0,
    DetKillingType_Count,
} DetKillingType;

const char*
det_killing_type_to_verb(DetKillingType type)
{
    switch (type)
    {
    case DetKillingType_Stabbed: return "stabbed";
    }

    return "";
}


void
det_demo_run()
{
    printf("hello\n");
    DetKillingType killingType = DetKillingType_Stabbed;
    const char* pKilledName = "Martyn";

    printf("%s was %s\n", pKilledName, det_killing_type_to_verb(killingType));
}
