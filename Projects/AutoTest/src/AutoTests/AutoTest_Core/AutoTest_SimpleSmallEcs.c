#include "AutoTest_SimpleSmallEcs.h"

#include <AutoTest/AutoTest.h>
#include <Core/SimpleSmallEcs.h>
#include <Core/Types.h>
#include <Math/SimpleMath.h>


typedef struct TransformComponent
{
    v3 Position;  // 3 * 4 = 12 a 16
    v3 Rotation;  // 3 * 4 = 12 a 16
    v3 Size    ;  // 3 * 4 = 12 a 16
                  //         36 a 48
    m4 Transform; // 16 * 4 = 48
                  // Total = 96 a 100
} TransformComponent;

typedef struct AComponent
{
    i64 I;
    i64 B;
    const char* Str;
} AComponent;

void
_set_component_test(SseWorld* pWorld, SseEntity sseEntity, const char* pCompName, void* pData, size_t size)
{
    sse_i32 notFailed = sse_entity_set_component(pWorld, sseEntity, pCompName, pData, size);
    AutoTest_I32_Value(notFailed);
}

void
_get_transform_component_test(SseWorld* pWorld, SseEntity sseEntity, TransformComponent tc)
{
    TransformComponent* pTc = sse_entity_get_component(pWorld, sseEntity, sse_ctos(TransformComponent));
    AutoTest_V3_Equal(pTc->Position, tc.Position);
    AutoTest_V3_Equal(pTc->Size, tc.Size);
    AutoTest_V3_Equal(pTc->Rotation, tc.Rotation);
    AutoTest_M4_Equal(pTc->Transform, tc.Transform);
}

void
_get_a_component_test(SseWorld* pWorld, SseEntity sseEntity, AComponent ac)
{
    AComponent* pA = sse_entity_get_component(pWorld, sseEntity, sse_ctos(AComponent));
    AutoTest_I32_Equal(pA->B, ac.B);
    AutoTest_I32_Equal(pA->I, ac.I);
    AutoTest_String_Equal((char*)pA->Str, (char*)ac.Str);
}

void
_get_component_test(SseWorld* pWorld, SseEntity sseEntity)
{
    {
        TransformComponent* pTc = sse_entity_get_component(pWorld, sseEntity, sse_ctos(TransformComponent));
        AutoTest_V3_Equal(pTc->Position, v3_new(0,0,1));
        AutoTest_V3_Equal(pTc->Size, v3_new(1,1,1));
        AutoTest_V3_Equal(pTc->Rotation, v3_new(0,0,0));
        m4 tr = m4_translate_identity(v3_new(2, -4, 7));
        AutoTest_M4_Equal(pTc->Transform, tr);
    }

    {
        AComponent* pA = sse_entity_get_component(pWorld, sseEntity, sse_ctos(AComponent));
        AutoTest_I32_Equal(pA->B, 1337);
        AutoTest_I32_Equal(pA->I, 10110);
        AutoTest_String_Equal((char*)pA->Str, "a_component_0");
    }

}

void
auto_test_sse_init()
{
    //AutoTest_I32_Value(sizeof(TransformComponent));

    SseWorld* pWorld = sse_world_create();
    AutoTest_Condition(pWorld != NULL);

    i32 isRegiteredComponent = sse_world_component_register(pWorld, sse_ctos(TransformComponent), sizeof(TransformComponent));
    i32 isRegistered = sse_world_is_component_registered_by_name(pWorld, sse_ctos(TransformComponent));

    AutoTest_Condition(isRegiteredComponent == isRegistered);
    AutoTest_I32_Equal(isRegistered, 1);

    SseComponentId tcId = sse_component_name_to_id(pWorld, sse_ctos(TransformComponent));

    i32 isRegisteredById = sse_world_is_component_registered_by_id(pWorld, tcId);
    AutoTest_I32_Equal(isRegisteredById, isRegistered);

    // check for duplication on comp regstration
    isRegiteredComponent = sse_world_component_register(pWorld, sse_ctos(TransformComponent), sizeof(TransformComponent));
    AutoTest_I32_Equal(isRegiteredComponent, 0);

    sse_world_component_register(pWorld, sse_ctos(AComponent), sizeof(AComponent));

    SseEntity sseEntity0 = sse_entity_create(pWorld);
    SseEntity sseEntity1 = sse_entity_create(pWorld);

    {
        TransformComponent tc = {
            .Position = v3_new(0,0,1),
            .Rotation = {},
            .Size = v3_new(1,1,1),
            .Transform = m4_translate_identity(v3_new(2,-4,7)),
        };
        AComponent ac = {.B = 1337, .I = 10110, .Str = "a_component_0"};

        _set_component_test(pWorld, sseEntity0, sse_ctos(TransformComponent), &tc, sizeof(tc));
        _set_component_test(pWorld, sseEntity0, sse_ctos(AComponent), &ac, sizeof(ac));

        _get_transform_component_test(pWorld, sseEntity0, tc);
        _get_a_component_test(pWorld, sseEntity0, ac);
    }

    {
        TransformComponent tc = {
            .Position = v3_new(0,0,1),
            .Rotation = {},
            .Size = v3_new(1,1,1),
            .Transform = m4_translate_identity(v3_new(2,-4,7)),
        };
        AComponent ac = {.B = 1337, .I = 10110, .Str = "a_component_0"};

        _set_component_test(pWorld, sseEntity1, sse_ctos(TransformComponent), &tc, sizeof(tc));
        _set_component_test(pWorld, sseEntity1, sse_ctos(AComponent), &ac, sizeof(ac));

        _get_transform_component_test(pWorld, sseEntity1, tc);
        _get_a_component_test(pWorld, sseEntity1, ac);

    }

    // note: test 0 entity again
    {
        TransformComponent tc = {
            .Position = v3_new(0,0,1),
            .Rotation = {},
            .Size = v3_new(1,1,1),
            .Transform = m4_translate_identity(v3_new(2,-4,7)),
        };
        AComponent ac = {.B = 1337, .I = 10110, .Str = "a_component_0"};

        _set_component_test(pWorld, sseEntity0, sse_ctos(TransformComponent), &tc, sizeof(tc));
        _set_component_test(pWorld, sseEntity0, sse_ctos(AComponent), &ac, sizeof(ac));

        _get_transform_component_test(pWorld, sseEntity0, tc);
        _get_a_component_test(pWorld, sseEntity0, ac);
    }


    sse_world_destroy(pWorld);
}

void
auto_test_simple_small_ecs_test()
{
    AutoTest_File();

    AutoTest_Func(auto_test_sse_init());
}
