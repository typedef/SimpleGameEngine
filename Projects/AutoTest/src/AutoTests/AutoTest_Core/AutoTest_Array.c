#include "AutoTest_Array.h"

#include <stdio.h>
#include <AutoTest/AutoTest.h>
#include <Core/SimpleStandardLibrary.h>


#define DefaultValue -57

void
auto_array_push()
{
    size_t arenaSize = KB(1);
    i32 arenaCount = arenaSize / sizeof(i32);
    SimpleArena* pArena = simple_arena_create(arenaSize);
    memset(pArena->Data, DefaultValue, arenaSize);
    memory_set_arena(pArena);

    const i32 count = 10;
    i32* pArr = NULL;

    array_reserve(pArr, count);
    for (i32 i = 0; i < count; ++i)
        pArr[i] = i;

    ArrayHeader* pHdr = array_header(pArr);
    // DOCS:                                           72 bytes
    size_t size = pHdr->Capacity * pHdr->ElementSize + sizeof(ArrayHeader);
    AutoTest_Condition(size == 72);
    AutoTest_I32_Value(pHdr->Capacity);
    AutoTest_I32_Value(pHdr->ElementSize);

    {
        // DOCS: Printing array with the size of 'count'
        void* aptr = pArena->Data + sizeof(ArrayHeader);
        for (i32 i = 0; i < count; ++i)
        {
            i32 i32v = *((i32*) (aptr) + i);
            //AutoTest_I32_Value(i32v);

            char buf[64] = {};
            snprintf(buf, 64, "ptr[%d] = %d", i, i32v);
            AutoTest_String_Value(buf);
        }
    }

    void* rptr = (void*) (pArena->Data + sizeof(ArrayHeader));
    size_t baseBytesOffset = sizeof(ArrayHeader) + count * sizeof(i32);
    for (i32 i = baseBytesOffset; i < (arenaSize-sizeof(ArrayHeader)); ++i)
    {
        i8 i8v = *((i8*) (rptr + i));
        char buf[64] = {};
        snprintf(buf, 64, "ptr[%d] = %d", i, i8v);
        AutoTest_String_Value(buf);
    }

    memory_set_arena(NULL);
    simple_arena_destroy(pArena);

}

void
auto_test_array()
{
    AutoTest_File();

    AutoTest_Func(auto_array_push());
}
