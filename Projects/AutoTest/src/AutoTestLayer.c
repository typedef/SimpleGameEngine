#include <Engine.h>
#include <EntryPoint.h>

#include <Application/Application.h>
#include <InputSystem/KeyCodes.h>
#include <Event/Event.h>
#include <Graphics/SimpleWindow.h>
#include <Core/SimpleStandardLibrary.h>
#include <Utils/tui.h>

#include "AutoTests/TestRunner.h"

void auto_test_layer_on_attach();
void auto_test_layer_on_update(f32 timestep);
void auto_test_layer_on_ui_render();
void auto_test_layer_on_event(struct Event* event);
void auto_test_layer_on_destroy();

void
create_user_application(UserApplicationSettings* pSettings)
{
    application_set_locale_utf8();
    logger_init();
    logger_to_terminal();

    ApplicationSettings appSettings = {
#if defined(LINUX_PLATFORM)
        .Width = 2000,
        .Height = 1200,
#elif defined(WINDOWS_PLATFORM)
        .Width = 1000,
        .Height = 800,
#endif
        .IsInitGraphics = 0,
        //BUG(): weird if 1 - bug, 0 - ok
        .IsFrameRatePrinted = 0,
        .IsVsync = 1,
        // BUG:
        /*
          pCreateInfo->imageExtent (2001, 1200), which is outside the bounds returned by vkGetPhysicalDeviceSurfaceCapabilitiesKHR(): currentExtent = (2000,1200), minImageExtent = (2000,1200), maxImageExtent = (2000,1200). The Vulkan spec states: If a VkSwapchainPresentScalingCreateInfoEXT structure was not included in the pNext chain, or it is included and VkSwapchainPresentScalingCreateInfoEXT::scalingBehavior is zero then imageExtent must be between minImageExtent and maxImageExtent, inclusive, where minImageExtent and maxImageExtent are members of the VkSurfaceCapabilitiesKHR structure returned by vkGetPhysicalDeviceSurfaceCapabilitiesKHR for the surface
        */
        .IsDebug = 1,
        .UiFpsSyncRatio = 2,
        .Name = "AutoTest's",
        .ArgumentsCount = pSettings->Argc,
        .Arguments = pSettings->Argv,
    };
    application_create(appSettings);

    Layer vulkanSandboxLayer = {
        .Name = "Auto Test Layer",
        .OnAttach = auto_test_layer_on_attach,
        .OnUpdate = auto_test_layer_on_update,
        .OnUIRender = auto_test_layer_on_ui_render,
        .OnEvent = auto_test_layer_on_event,
        .OnDestoy = auto_test_layer_on_destroy,
    };

    application_push_layer(vulkanSandboxLayer);

    test_runner_run();

}

void
auto_test_layer_on_attach()
{
}

void
auto_test_layer_on_update(f32 timestep)
{
}

void
auto_test_layer_on_ui_render()
{
}

void
auto_test_layer_on_event(struct Event* event)
{
    SimpleWindow* pCurrentWindow = application_get_window();

    switch (event->Category)
    {

    case EventCategory_Key:
    {
        if (event->Type != EventType_KeyPressed)
            break;

        KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
        if (keyEvent->KeyCode == KEY_ESCAPE)
        {
            application_close();
            event->IsHandled = 1;
        }

        break;
    }

    case EventCategory_Window:
    {
        if (event->Category == EventCategory_Window)
        {
            switch (event->Type)
            {

            case EventType_WindowShouldBeClosed:
                application_close();
                event->IsHandled = 1;
                break;
            }
        }

    }

    }

}

void
auto_test_layer_on_destroy()
{

}
