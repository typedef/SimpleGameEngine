#include "SimpleEditor.h"

int
main(int argc, char** apArgs)
{
    simple_editor_run(argc, apArgs);

    return 0;
}
