#include "SimpleEditor.h"

#define SWL_RENDERER
#include <Core/SimpleWindowLibrary.h>
#include <Core/Types.h>
#include <Deps/stb_image.h>
#include <Core/SimpleStandardLibrary.h>

// DOCS: Editor part
#include <SeEditor/SeEditor.h>

static i32 gAppRun = 1;
static SwlWindow gSwlWindow = {};

void
my_event_callback(SwlEvent* pEvent)
{
#define LogEnabled 0

#if LogEnabled == 1
#define log(f, ...) printf(f, __VA_ARGS__)
#else
#define log(f, ...)
#endif

    if (pEvent->Type == SwlEventType_MouseMove)
    {
        SwlMouseMoveEvent* pMmEvent = (SwlMouseMoveEvent*) pEvent;
        log("X,Y: %d %d\n", pMmEvent->X, pMmEvent->Y);
    }

    if (pEvent->Type == SwlEventType_WindowResized)
    {
        swl_renderer_resize(&gSwlWindow);
        log("Size: %d %d\n", gSwlWindow.Size.Width, gSwlWindow.Size.Height);
    }

    if (pEvent->Type == SwlEventType_MousePress
        || pEvent->Type == SwlEventType_MouseRelease)
    {
        SwlMousePressEvent* pMouseKey = (SwlMousePressEvent*) pEvent;
        log("IsHandled: %d Type: %d Key: %d\n", pEvent->IsHandled, pEvent->Type, pMouseKey->Key);
    }

    if (pEvent->Type == SwlEventType_KeyPress)
    {
        SwlKeyPressEvent* pKeyPress = (SwlKeyPressEvent *)pEvent;

        if (pKeyPress->Key == SwlKey_Escape)
        {
            gAppRun = 0;
        }

        log("SwlKey: %d %s\n", pKeyPress->Key, swl_key_to_string(pKeyPress->Key));

    }

    if (pEvent->Type == SwlEventType_KeyType)
    {
        SwlKeyTypeEvent* pType = (SwlKeyTypeEvent*) pEvent;
        log("%d\n", pType->CodePoint);
    }

    se_editor_event(pEvent);
}



#define SwlTextureInvalid (-1)

i32
swl_ext_load_texture(const char* pPath)
{
    swl_i32 textureId;

    swl_i32 w, h, c;
    void* pData = (void*) stbi_load(pPath, &w, &h, &c, 0);
    if (pData == NULL)
    {
        printf("Could not load texture \n");
        return SwlTextureInvalid;
    }

    textureId = swl_renderer_texture_create(pData, (swl_v2){w, h}, c);

    stbi_image_free(pData);

    return textureId;
}

void
se_b_draw_rect(v3 pos, v2 size, v4 color)
{
    swl_v4 swlColor = (swl_v4) { color.R, color.G, color.B, color.A };
    swl_renderer_draw_rectangle(
            &gSwlWindow,
            (swl_v3) { pos.X, pos.Y, pos.Z },
            (swl_v2) { size.X, size.Y },
            (swl_v4) { color.R, color.G, color.B, color.A });
}

f32
se_b_get_text_width(i32* pText, i32 length)
{
    return swl_renderer_get_text_width(&gSwlWindow, pText, length);
}

void
se_b_draw_text(i32*pText, i32 count, v3 pos, v4 color)
{
    swl_v4 swlColor = (swl_v4) { color.R, color.G, color.B, color.A };
    swl_renderer_draw_text(&gSwlWindow,
                           (swl_i32*)pText,
                           count,
                           (swl_v3) { pos.X, pos.Y, pos.Z },
                           swlColor);
}

void
simple_editor_run(int argCount, char** apArgs)
{
    swl_init((SwlInit) { .MemoryAllocate = malloc, .MemoryFree = free, });
    SwlWindowSettings set = {
        .pName = "Swl Window test",
        .Position = {.X = 200, .Y = 100},
        .Size = {
            .Width = 1920,//968,
            .Height = 1061,
            .MinHeight = 150,
            .MinWidth = 150
        },
    };
    swl_window_create(&gSwlWindow, set);

    swl_window_set_event_call(&gSwlWindow, my_event_callback);

    swl_renderer_create(&gSwlWindow);
    //swl_renderer_set_swap_iterval(&gSwlWindow, 1);

    i32 codePoints[] = {
        '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~',
        '$',
        L'А', L'Б', L'В', L'Г', L'Д', L'Е', L'Ж', L'З', L'И', L'Й', L'К', L'Л', L'М', L'Н', L'О', L'П', L'Р', L'С', L'Т', L'У', L'Ф', L'Х', L'Ц', L'Ч', L'Ш', L'Щ', L'Ъ', L'Ы', L'Ь', L'Э', L'Ю', L'Я', L'а', L'б', L'в', L'г', L'д', L'е', L'ж', L'з', L'и', L'й', L'к', L'л', L'м', L'н', L'о', L'п', L'р', L'с', L'т', L'у', L'ф', L'х', L'ц', L'ч', L'ш', L'щ', L'ъ', L'ы', L'ь', L'э', L'ю', L'я'
    };

    SwlFontSettings fontSet = {
        .pFontPath = asset_font("NotoSans.ttf"),
        .FontSize = 50,
        .Padding = 2,
        .OnEdgeValue = 180,
        .BitmapSize = (swl_v2) { 640, 640 },
        .aCodePoints = codePoints,
        .CodePointsCount = ARRAY_COUNT(codePoints),
    };
    swl_renderer_font_load(&fontSet);
    void* pBitmap = swl_renderer_font_get_bitmap();
    swl_i32 fontId = swl_renderer_texture_create(pBitmap, (swl_v2){fontSet.BitmapSize.X,fontSet.BitmapSize.Y}, 1);
    /* printf("Here\n\n\n"); */

    //i32 textureId = swl_ext_load_texture(asset_texture("crimson_stem_top.png"));

    se_editor_create((SeEditorSettings){
            .Draw = {
                .DrawRect = se_b_draw_rect,
                .DrawText = se_b_draw_text,
                .GetTextWidth = se_b_get_text_width,
            }
        });

    //memory_set_print(PrintAllocationSourceType_Terminal);

    while (gAppRun)
    {
        const f32 cv = 0.051f;
        swl_renderer_clear_color(&gSwlWindow, cv, cv, cv, 1.0f);
        swl_update(&gSwlWindow);

        swl_renderer_draw_rectangle(
            &gSwlWindow,
            (swl_v3) {250, 100, 20}, (swl_v2){200, 200},
            (swl_v4) { 0.01127f, 0.01345f, 0.0456f, 1.0f });

        {
            f32 demoX = 500;
            wchar* lText = L"Y: 0";

            swl_renderer_draw_text(
                &gSwlWindow,
                lText,
                5,
                (swl_v3) { demoX, 50, 1 },
                (swl_v4) {1,1,1,1});

            wchar* hText = L"Y: 1";
            swl_renderer_draw_text(
                &gSwlWindow,
                hText,
                5,
                (swl_v3) { demoX, 500, 1 },
                (swl_v4) {1,1,1,1});
        }

        /* swl_renderer_draw_texture( */
        /*     &gSwlWindow, */
        /*     (swl_v3) {100, 500, 10}, (swl_v2){512, 512}, */
        /*     (swl_v4) { 1.f, 1.f, 1.f, 1.0f }, */
        /*     textureId); */

        //wchar* text = L"Hello, world!";

        se_editor_update();

        if (gSwlWindow.aKeys[SwlKey_A] == SwlAction_Press)
        {
            //printf("X: %f, timestep: %f\n", x, timestep);
        }
        if (gSwlWindow.aKeys[SwlKey_Space] == SwlAction_Press)
        {
        }
        //printf("X: %f, timestep: %f\n", x, timestep);

        swl_renderer_flush(&gSwlWindow);
    }

    swl_window_destroy(&gSwlWindow);
}
