#include "SeCmd.h"

#include "SeBuffer.h"
#include <Core/SimpleStandardLibrary.h>


void
se_cmd_save_buffer(SeBuffer* pBuffer)
{
    if (!pBuffer->pPath)
    {
        // note: there is no file existed in the file system
        file_delete(pBuffer->pPath);
    }

    char* sb = NULL;

    i64 i, count = array_count(pBuffer->aRows);
    for (i = 0; i < count; ++i)
    {
        SeRow seRow = pBuffer->aRows[i];

        i32* pUnicodeStr = seRow.aData;

        for (i32 si = 0; si < array_count(pUnicodeStr); ++si)
        {
            i32 unicode = pUnicodeStr[si];
            char buf[4] = {};
            i32 length = simple_core_utf8_encode(buf, unicode);

            string_builder_appendsl(sb, buf, length);
        }

        string_builder_appendc(sb, '\n');
    }

    file_write_string("Temp.c", sb, string_builder_count(sb));
}

void
se_cmd_load_buffer(SeBuffer* pBuffer, const char* pPath)
{
    if (pBuffer->aRows != NULL)
    {
        array_foreach(pBuffer->aRows, array_free(item.aData));

        array_free(pBuffer->aRows);
    }

    size_t size;
    char* pBytes = (char*) file_read_bytes_ext("Temp.c", &size);

    SeRow* aRows = NULL;

#define AddRow()                                \
    ({                                          \
        SeRow seRow = {};                       \
        array_push(aRows, seRow);               \
        &aRows[array_count(aRows)-1];           \
    })

    SeRow* pRow = AddRow();
    //i32 strLen = simple_core_utf8_length(pBytes);

    for (i32 i = 0; i < size;)
    {
        i32 bytesCount;
        i32 codepoint = simple_core_utf8_decode(pBytes + i, &bytesCount);
        if (codepoint == '\n')
        {
            array_push(pRow->aData, (i32)'\n');
            //add row
            pRow = AddRow();
        }
        else
        {
            array_push(pRow->aData, codepoint);
        }

        i += bytesCount;
    }

#undef AddRow

    pBuffer->aRows = aRows;
}
