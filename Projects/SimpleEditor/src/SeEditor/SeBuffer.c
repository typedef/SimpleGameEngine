#include "SeBuffer.h"
#include "SeRow.h"

#include <Core/SimpleStandardLibrary.h>


SeBuffer
se_buffer_new(v2 size)
{
    SeBuffer seBuffer = {
        .aRows = NULL,
        .Size = size,
    };

    SeRow seRow = {
        .aData = NULL
    };
    array_push(seBuffer.aRows, seRow);

    return seBuffer;
}

void
se_buffer_move_up(SeBuffer* pBuffer)
{
    pBuffer->Row = MinMax(pBuffer->Row - 1, 0, array_count(pBuffer->aRows));
    pBuffer->Col = 0;
}

void
se_buffer_move_down(SeBuffer* pBuffer)
{
    pBuffer->Row = MinMax(pBuffer->Row + 1, 0, array_count(pBuffer->aRows));

    if (pBuffer->PrevCol == 0)
        pBuffer->PrevCol = -1;
    else
        pBuffer->PrevCol = pBuffer->Col;

    pBuffer->Col = 0;
}

void
se_buffer_move_left(SeBuffer* pBuffer)
{
    if (pBuffer->Col > 0)
        pBuffer->Col -= 1;
    else if ((pBuffer->Row - 1) >= 0)
    {
        --pBuffer->Row;
        pBuffer->Col = array_count(GetActiveRow(pBuffer)->aData);
    }
}

void
se_buffer_move_right(SeBuffer* pBuffer)
{
    i32 cnt = array_count(GetActiveRow(pBuffer)->aData);
    if (pBuffer->Col < cnt)
        pBuffer->Col += 1;
    else if ((pBuffer->Row + 1) < array_count(pBuffer->aRows))
    {
        ++pBuffer->Row;
        pBuffer->Col = 0;
    }
}

void
se_buffer_add_char(SeBuffer* pBuffer, i32 ch)
{
    // todo: set active row
    SeRow* pRow = GetActiveRow(pBuffer);
    se_row_push_at(pRow, &ch, 1, pBuffer->Col);

    pBuffer->Col += 1;

    //se_row_push_back(&pBuffer->aRows[0], &ch, 1);
    //{JW!QHarray_push(pBuffer->aData, ch);
}

void
se_buffer_remove_char(SeBuffer* pBuffer)
{
    if (!pBuffer || !pBuffer->aRows)
        return;

    //todo: validate row
    if (pBuffer->Col > 0)
    {
        SeRow* pRow = GetActiveRow(pBuffer);
        se_row_pop_at(pRow, 1, pBuffer->Col - 1);
        pBuffer->Col -= 1;
    }
    else
    {
        //  move up and delete current row

        if (pBuffer->Row > 0)
        {
            i32 currentRow = pBuffer->Row;
            SeRow* pCur = GetActiveRow(pBuffer);
            --pBuffer->Row;
            SeRow* pRow = GetActiveRow(pBuffer);

            // todo: remove this
            //pRow->aData;

            se_row_push_at(pRow, pCur->aData, array_count(pCur->aData), array_count(pRow->aData)-1);

            array_remove_at(pBuffer->aRows, currentRow);
        }

        /*
          todo:
          if {
          _ we havent upper-row -> do noth
          }
          else {
          _ copy current-row to upper-row at pos '\n'
          _ remove current-row
          }
        */
    }
    //se_row_pop_back(pRow, 1);

    /* ArrayHeader* pHdr = array_header(pBuffer->aData); */

    /* if (pHdr->Count == 0) */
    /*     return; */

    /* pHdr->Count = pHdr->Count - 1; */
}

void
se_buffer_add_line(SeBuffer* pBuffer, i32 index)
{
    if (index != -1)
    {
        printf("se_buffer_add_line index != -1, not impl.\n");
        return;
    }
    else
    {
        index = pBuffer->Row + 1;
    }

    SeRow newRow = {};

    if (index >= array_count(pBuffer->aRows))
    {
        array_push(pBuffer->aRows, newRow);
    }
    else
    {
        array_push_at(pBuffer->aRows, index, newRow);
    }
}
