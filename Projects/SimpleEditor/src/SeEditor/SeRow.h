#ifndef SE_ROW_H
#define SE_ROW_H

#include <Core/Types.h>


typedef struct SeRow
{
    i32* aData;
} SeRow;

void se_row_push_at(SeRow* pRow, void* pData, i32 dataCount, i32 atPos);
void se_row_pop_at(SeRow* pRow, i32 dataCount, i32 atPos);

#endif // SE_ROW_H
