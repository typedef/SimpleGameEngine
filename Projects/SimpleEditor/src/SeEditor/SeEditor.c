#include "SeEditor.h"
#include <SeEditor/SeBuffer.h>
#include <SeEditor/SeCmd.h>

#include <Core/SimpleStandardLibrary.h>
#include <Core/SimpleWindowLibrary.h>
#include <Math/SimpleMath.h>


static SeEditor gSeEditor = {};
static SeDraw gDraw = {};

void _se_editor_key_pressed(SwlKeyPressEvent* pEvent);
void _se_editor_key_typed(SwlKeyTypeEvent* pEvent);

void
se_editor_create(SeEditorSettings settings)
{
    //todo: asset on null

    gDraw = settings.Draw;
    gSeEditor.Mode = SeInteractiveMode_Input;

    SeBuffer seBuffer = se_buffer_new((v2){700, 700});
    array_push(gSeEditor.aBuffers, seBuffer);
}

void
se_editor_destroy()
{
}

#define Rgba255(r,g,b,a) (v4) { r / 255.0f, g / 255.0f, b/255.0f, a/255.0f}

void
se_editor_update()
{
    SeBuffer* pBuffer = se_get_active_buffer(&gSeEditor);

    SeRow* pRow = GetActiveRow(pBuffer);
    //printf("Count: %ld\n", array_count(seBuffer.aData));

    const f32 fontSize = 50;
    v3 bPos = (v3) { .X=650, .Y=250, .Z=1.21 };

    { // DOCS: Draw buffer background
        gDraw.DrawRect(bPos, pBuffer->Size, Rgba255(13, 14, 22, 255));
    }

    { // DOCS: Draw Cursor
        v4 cursorColor = (v4) { 216/255.0f, 95/255.0f, 0/255.0f, 1.0 };

        i32 cursorWidth = 2,
            cursorHeight = fontSize;
        v3 sPos = v3_add(bPos, v3_new(0, 0, -0.5));

        f32 width = gDraw.GetTextWidth(pRow->aData, pBuffer->Col);
        //printf("Width: %f %d\n", width, pBuffer->Col);
        sPos.X += width;
        sPos.Y += fontSize * pBuffer->Row;

        gDraw.DrawRect(sPos, (v2){ cursorWidth,cursorHeight }, cursorColor);
    }

    i32 renderNewLine = 1;

    // note: draw single line
    v2 contentSize = {};
    v3 rowStartPos = v3_add(bPos, v3_new(0, +fontSize, -0.5));
    i32 cnt = array_count(pBuffer->aRows);
    for (i32 i = 0; i < cnt; ++i)
    {
        SeRow seRow = pBuffer->aRows[i];
        i32* pArr = seRow.aData;
        i32 rowCnt = array_count(seRow.aData);

        gDraw.DrawText(pArr,
                       rowCnt,
                       rowStartPos,
                       (v4) { 0.7, 1, 0.7, 1 });

        if (renderNewLine && array_index_of(pArr, '\n') != -1)
        {
            v3 newLinePos = rowStartPos;
            newLinePos.X += gDraw.GetTextWidth(pArr, rowCnt);
            i32 singleNewLineChar = '$';
            gDraw.DrawText(&singleNewLineChar,
                           1,
                           newLinePos,
                           (v4) { 0.7, 0.7, 1., 1 });
        }

        rowStartPos.Y += fontSize;
        contentSize.Y += fontSize;

        if (contentSize.Y >= pBuffer->Size.Y ||
            contentSize.X >= pBuffer->Size.X)
            break;
    }
}

SeBuffer*
se_get_active_buffer(SeEditor* pEditor)
{
    return &pEditor->aBuffers[0];
}

void
_se_editor_event_press_input_mode(SwlKeyPressEvent* pEvent)
{
    SwlKey key = pEvent->Key;

    switch (key)
    {
    case SwlKey_Left:
    {
        SeBuffer* pBuffer = se_get_active_buffer(&gSeEditor);
        se_buffer_move_left(pBuffer);
        break;
    }

    case SwlKey_Right:
    {
        SeBuffer* pBuffer = se_get_active_buffer(&gSeEditor);
        se_buffer_move_right(pBuffer);
        break;
    }

    case SwlKey_Up:
    {
        se_buffer_move_up(se_get_active_buffer(&gSeEditor));
        break;
    }
    case SwlKey_Down:
    {
        se_buffer_move_down(se_get_active_buffer(&gSeEditor));
        break;
    }

    case SwlKey_Space:
        se_buffer_add_char(se_get_active_buffer(&gSeEditor), 32);
        break;

    case SwlKey_Enter:
    {
        SeBuffer* pBuf = se_get_active_buffer(&gSeEditor);
        se_buffer_add_line(pBuf, -1);
        se_buffer_move_down(pBuf);
        break;
    }

    case SwlKey_Backspace:
    {
        SeBuffer* pBuffer = se_get_active_buffer(&gSeEditor);
        se_buffer_remove_char(pBuffer);
        break;
    }

    case SwlKey_Delete:
    {
        break;
    }

    default:
        break;
    }
}

void
_se_editor_event_type_input_mode(SwlKeyTypeEvent* pEvent)
{
    u32 code = pEvent->CodePoint;

    // note: write to first buffer for now
    SeBuffer* pBuffer = se_get_active_buffer(&gSeEditor);
    se_buffer_add_char(pBuffer, code);
}

void
_se_editor_key_pressed(SwlKeyPressEvent* pEvent)
{
    SwlKey key = pEvent->Key;

    switch (key)
    {
    case SwlKey_Escape:
        se_editor_destroy();
        return;

    case SwlKey_F1:
    {
        se_cmd_save_buffer(se_get_active_buffer(&gSeEditor));
        break;
    }
    case SwlKey_F2:
    {
        se_cmd_load_buffer(se_get_active_buffer(&gSeEditor), NULL);
        break;
    }

    default:
        break;
    }

    if (gSeEditor.Mode == SeInteractiveMode_Input)
    {
        _se_editor_event_press_input_mode(pEvent);
    }

}

void
_se_editor_key_typed(SwlKeyTypeEvent* pEvent)
{
    if (gSeEditor.Mode == SeInteractiveMode_Input)
    {
        _se_editor_event_type_input_mode(pEvent);
    }
}

void
se_editor_event(struct SwlEvent* pEvent)
{
    switch (pEvent->Type)
    {
    case SwlEventType_KeyType:
        _se_editor_key_typed((SwlKeyTypeEvent*) pEvent);
        break;

    case SwlEventType_KeyPress:
    {
        _se_editor_key_pressed((SwlKeyPressEvent*) pEvent);
        break;
    }

    default:
        break;
    }

}
