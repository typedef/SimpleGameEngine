#include "SeRow.h"

#include <Core/SimpleStandardLibrary.h>

void*
_se_array_new_buffer(void* pArr, i32 initCount, i32 newCapacity)
{
    ArrayHeader* pHdr = memory_allocate(sizeof(ArrayHeader) + newCapacity * sizeof(i32));
    pHdr->Count = initCount;
    pHdr->Capacity = newCapacity;
    pHdr->Buffer = ((char*)pHdr) + sizeof(ArrayHeader);
    memset(pHdr->Buffer, 0, pHdr->Capacity * sizeof(i32));

    return pHdr->Buffer;
}

void*
_se_array_grow_back(void* pArr, i32 newCount)
{
    // DOCS: Create new array with header
    if (pArr == NULL)
    {
        i32* pNewArr = _se_array_new_buffer(pArr, 0, 1.5*newCount+1);
        return pNewArr;
    }

    // DOCS: Create new buffer with cap = 1.5*cnt+1
    ArrayHeader* pHdr = array_header(pArr);
    void* pNewArr = _se_array_new_buffer(pArr, pHdr->Count, 1.5*newCount+1);

    // DOCS: Copy old data
    memcpy(pNewArr, pArr, pHdr->Count * sizeof(i32));

    memory_free(pHdr);

    return pNewArr;
}

void
_se_array_push_at(SeRow* pRow, void* pData, i32 dataCount, i32 atPos)
{
    void* pArr = (void*) pRow->aData;

    i64 newCnt = array_count(pArr) + dataCount;
    if (pArr == NULL || newCnt >= array_capacity(pArr))
    {
        pRow->aData = _se_array_grow_back(pArr, newCnt);
        pArr = pRow->aData;
    }

    // DOCS: Copy prev values to new location
    i32 cnt = array_count(pArr);
    i32 itemsToCopy = (cnt - dataCount - 1);
    if (atPos != cnt && itemsToCopy > 0)
    {
        printf("HERE!\n");
        memcpy(pArr + (atPos + dataCount) * sizeof(i32),
                pArr + atPos * sizeof(i32),
                itemsToCopy * sizeof(i32));
    }

    // DOCS: Set new values
    memcpy(pArr + atPos * sizeof(i32),
           pData,
           dataCount * sizeof(i32));

    array_header(pArr)->Count += dataCount;

}


// DOCS: PUBLIC API

void
se_row_push_at(SeRow* pRow, void* pData, i32 dataCount, i32 atPos)
{
    _se_array_push_at(pRow, pData, dataCount, atPos);
}

void
_se_array_pop_at(SeRow* pRow, i32 dataCount, i32 atPos)
{
    void* pArr = (void*) pRow->aData;
    i32 cnt = array_count(pArr);

    if (atPos >= cnt || cnt == 0)
        return;

    if (dataCount >= cnt)
    {
        array_header(pArr)->Count = 0;
        memset(pArr, 0, array_capacity(pArr));
        return;
    }

    // cnt: 5
    // dataCount: 1
    // atPos: 2
    i32 itemsToCopy = (cnt - atPos - dataCount );
    if (itemsToCopy > 0)
    {
        i32* writePtr = (i32*) (pArr + atPos * sizeof(i32));
        i32* readPtr  = (i32*) (pArr + (atPos + dataCount) * sizeof(i32));
        memmove(writePtr, readPtr, itemsToCopy * sizeof(i32));

        /* memcpy(pArr + atPos * sizeof(i32), */
        /*        pArr + (atPos + dataCount) * sizeof(i32), */
        /*        itemsToCopy*sizeof(i32)); */
    }

    array_header(pArr)->Count -= dataCount;
}

void
se_row_pop_at(SeRow* pRow, i32 dataCount, i32 atPos)
{
    _se_array_pop_at(pRow, dataCount, atPos);
}
