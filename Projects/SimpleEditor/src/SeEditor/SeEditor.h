#ifndef SE_EDITOR_H
#define SE_EDITOR_H

#include <SeEditor/SeBuffer.h>


struct SwlEvent;

typedef struct SeDraw
{
    void (*DrawRect)(v3 pos, v2 size, v4 color);
    void (*DrawText)(i32* pText, i32 count, v3 pos, v4 color);
    float (*GetTextWidth)(i32* pText, i32 length);
} SeDraw;

typedef enum SeInteractiveMode
{
    SeInteractiveMode_Input = 0,
    SeInteractiveMode_Count,
} SeInteractiveMode;

typedef struct SeEditorSettings
{
    SeDraw Draw;
} SeEditorSettings;


typedef struct SeEditor
{
    SeBuffer* aBuffers;
    SeInteractiveMode Mode;
} SeEditor;

void se_editor_create(SeEditorSettings settings);
SeBuffer* se_get_active_buffer(SeEditor* pEditor);
void se_editor_update();
void se_editor_event(struct SwlEvent* pSwlEvent);
void se_editor_destroy();

#endif // SE_EDITOR_H
