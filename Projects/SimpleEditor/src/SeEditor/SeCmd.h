#ifndef SE_CMD_H
#define SE_CMD_H


struct SeBuffer;

void se_cmd_save_buffer(struct SeBuffer* pBuffer);
void se_cmd_load_buffer(struct SeBuffer* pBuffer, const char* pPath);

#endif // SE_CMD_H
