#ifndef SE_BUFFER_H
#define SE_BUFFER_H

#include <Core/Types.h>

#include "SeRow.h"


typedef struct SeBuffer
{
    i32 Id;
    i32 Row;
    i32 Col;
    i32 PrevCol;
    v2 Size;
    const char* pPath;
    SeRow* aRows;
} SeBuffer;

#define GetActiveRow(pBuffer) ((SeRow*)&pBuffer->aRows[pBuffer->Row])

SeBuffer se_buffer_new(v2 size);
void se_buffer_add_char(SeBuffer* pBuffer, i32 ch);
void se_buffer_remove_char(SeBuffer* pBuffer);

void se_buffer_move_left(SeBuffer* pBuffer);
void se_buffer_move_right(SeBuffer* pBuffer);
void se_buffer_move_up(SeBuffer* pBuffer);
void se_buffer_move_down(SeBuffer* pBuffer);

void se_buffer_add_line(SeBuffer* pBuffer, i32 index);


#endif // SE_BUFFER_H
