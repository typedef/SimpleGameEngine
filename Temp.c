#include <stdio.h>
#include <stdlib.h>

int
main()
{
    printf("Hello,cruel world!\n");

    int* pArr = malloc(4*10);
    for (int i = 0; i < 10; ++i)
    {
        pArr[i] = i;
    }

    free(pArr);

    return 0;
}
