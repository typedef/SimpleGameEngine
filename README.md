SkipSkipSkipSkip
# SimpleGameEngine
SimpleGameEngine - game engine written in c99, using vulkan gapi.
Currently not ready for use.

### Supported Platforms
* GNU/Linux
* Windows10

### Get Started

#### GNU/Linux

Install:
* install git, clang, premake5 through package manager
* install VulkanSDK

To start Engine:
* git clone https://codeberg.org/typedef/SimpleGameEngine
* cd SimpleGameEngine
* git clone https://github.com/vezone/Assets assets
* cd Scripts/Linux/VulkanSandbox
* [run script ] ./rebuild_debug.sh

#### Windows10
* Type in PowerShell (opened as root) .. Set-ExecutionPolicy -ExecutionPolicy Unrestricted
* cd Scripts/Windows/VulkanSandbox/
* powershell > generate.ps1

### Tools Dependencies
* git
* premake5
* clang, clangd
* VulkanSDK

### Specific Dependencies For GNU/Linux Platform
* codelite(debugger)

### Dependencies For Windows10 Platform
* VS2022 (builder and debugger)

### Libs Dependencies
* [cgltf](https://github.com/jkuhlmann/cgltf)
* [GLFW](https://github.com/glfw/glfw)
* [MiniAudio](https://github.com/mackron/miniaudio)
* [stb: stb_image, stb_image_resize, stb_image_write, stb_truetype](https://github.com/nothings/stb)

# Branches
main -> win-dev -> x
	 -> dev -> physics
			-> simple-render-compositor -> sky
			-> render
