#include "${ProjectName}.h"
#include <EntryPoint.h>

void
create_user_application(UserApplicationSettings* pSettings)
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitGraphics = 1,
	.IsFrameRatePrinted = 0,
	.Name = "${ProjectName}",
	.ArgumentsCount = pSettings->Argc,
	.Arguments = pSettings->Argv,
    };
    application_create(appSettings);

    Layer ${ProjectNameCamel}Layer = {0};
    ${ProjectNameCamel}Layer.Name = "${ProjectName} Layer";
    ${ProjectNameCamel}Layer.OnAttach = ${ProjectNameConvLower}_on_attach;
    ${ProjectNameCamel}Layer.OnUpdate = ${ProjectNameConvLower}_on_update;
    ${ProjectNameCamel}Layer.OnUIRender = ${ProjectNameConvLower}_on_ui_render;
    ${ProjectNameCamel}Layer.OnEvent = ${ProjectNameConvLower}_on_event;
    ${ProjectNameCamel}Layer.OnDestoy = ${ProjectNameConvLower}_on_destroy;

    application_push_layer(${ProjectNameCamel}Layer);
}
