#include "${ProjectName}.h"

#include <InputSystem/KeyCodes.h>

static SimpleWindow* CurrentWindow = NULL;

void
${ProjectNameConvLower}_on_attach()
{
    CurrentWindow = application_get_window();;
}

void
${ProjectNameConvLower}_on_attach_finished()
{

}

void
${ProjectNameConvLower}_on_update(f32 timestep)
{

}

void
${ProjectNameConvLower}_on_ui_render()
{

}

void
${ProjectNameConvLower}_on_event(Event* event)
{
    switch (event->Category)
    {

    case EventCategory_Key:
    {
	if (event->Type != EventType_KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KeyType_Escape)
	{
	    application_close();
	    event->IsHandled = 1;
	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == EventType_WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }
}

void
${ProjectNameConvLower}_on_destroy()
{
}
