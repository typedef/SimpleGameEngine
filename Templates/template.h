#ifndef ${ProjectNameConvUpper}_H
#define ${ProjectNameConvUpper}_H

#include <Engine.h>

void ${ProjectNameConvLower}_on_attach();
void ${ProjectNameConvLower}_on_attach_finished();
void ${ProjectNameConvLower}_on_update(f32 timestep);
void ${ProjectNameConvLower}_on_ui_render();
void ${ProjectNameConvLower}_on_event(Event* event);
void ${ProjectNameConvLower}_on_destroy();

#endif // ${ProjectNameConvUpper}_H
